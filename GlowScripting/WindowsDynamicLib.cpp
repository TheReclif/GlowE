#include "WindowsDynamicLib.h"

#ifndef LOAD_LIBRARY_SEARCH_APPLICATION_DIR
#define LOAD_LIBRARY_SEARCH_APPLICATION_DIR 0x00000200
#endif // LOAD_LIBRARY_SEARCH_APPLICATION_DIR

CppScript::WindowsDynamicLibrary::WindowsDynamicLibrary()
	: handle(nullptr)
{
}

CppScript::WindowsDynamicLibrary::~WindowsDynamicLibrary()
{
	unload();
}

bool CppScript::WindowsDynamicLibrary::load(const char* name)
{
	if (handle)
	{
		return false;
	}

	handle = LoadLibraryA(name);

	if (!handle)
	{
		return false;
	}

	return true;
}

void CppScript::WindowsDynamicLibrary::unload()
{
	if (handle)
	{
		FreeLibrary(handle);
		handle = nullptr;
	}
}

void* CppScript::WindowsDynamicLibrary::getMethod(const char* name) const
{
	if (handle)
	{
		return (void*)(GetProcAddress(handle, name));
	}

	return nullptr;
}

void* CppScript::WindowsDynamicLibrary::operator[](const char* name) const
{
	return getMethod(name);
}

bool CppScript::WindowsDynamicLibrary::operator==(const WindowsDynamicLibrary& right) const
{
	return handle == right.handle;
}

bool CppScript::WindowsDynamicLibrary::operator!=(const WindowsDynamicLibrary& right) const
{
	return handle != right.handle;
}

bool CppScript::WindowsDynamicLibrary::operator<(const WindowsDynamicLibrary& right) const
{
	return handle < right.handle;
}

bool CppScript::WindowsDynamicLibrary::operator>(const WindowsDynamicLibrary& right) const
{
	return handle > right.handle;
}
