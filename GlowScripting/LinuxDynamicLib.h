#pragma once
#ifndef GLOWE_LINUX_DYNAMICLIB_INCLUDED
#define GLOWE_LINUX_DYNAMICLIB_INCLUDED

#include "glowscripting_export.h"

#include "../GlowSystem/Utility.h"

#ifdef GLOWE_COMPILE_FOR_LINUX
#include <dlfcn.h>

namespace CppScript
{
	class GLOWSCRIPTING_EXPORT LinuxDynamicLibrary
	{
	private:
		void* library = nullptr;
	public:
		LinuxDynamicLibrary() = default;
		~LinuxDynamicLibrary();

		bool load(const char* name);
		void unload();

		void* getMethod(const char* name) const;
		void* operator[](const char* name) const;

		bool operator==(const LinuxDynamicLibrary& right) const;
		bool operator!=(const LinuxDynamicLibrary& right) const;
		bool operator<(const LinuxDynamicLibrary& right) const;
		bool operator>(const LinuxDynamicLibrary& right) const;
	};
}
#endif

#endif
