#include "LinuxDynamicLib.h"

CppScript::LinuxDynamicLibrary::~LinuxDynamicLibrary()
{
	unload();
}

bool CppScript::LinuxDynamicLibrary::load(const char* name)
{
	if (library)
	{
		return false;
	}

	library = dlopen(name, RTLD_NOW | RTLD_LOCAL);

	return library;
}

void CppScript::LinuxDynamicLibrary::unload()
{
	if (library)
	{
		dlclose(library);
		library = nullptr;
	}
}

void* CppScript::LinuxDynamicLibrary::getMethod(const char* name) const
{
	if (!library)
	{
		return nullptr;
	}

	return dlsym(library, name);
}

void* CppScript::LinuxDynamicLibrary::operator[](const char* name) const
{
	return getMethod(name);
}

bool CppScript::LinuxDynamicLibrary::operator==(const LinuxDynamicLibrary& right) const
{
	return library == right.library;
}

bool CppScript::LinuxDynamicLibrary::operator!=(const LinuxDynamicLibrary& right) const
{
	return library != right.library;
}

bool CppScript::LinuxDynamicLibrary::operator<(const LinuxDynamicLibrary& right) const
{
	return library < right.library;
}

bool CppScript::LinuxDynamicLibrary::operator>(const LinuxDynamicLibrary& right) const
{
	return library > right.library;
}
