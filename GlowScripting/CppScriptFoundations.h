#pragma once
#ifndef GLOWE_SCRIPTING_CPPSCRIPTFOUND_INCLUDED
#define GLOWE_SCRIPTING_CPPSCRIPTFOUND_INCLUDED

#include "../GlowSystem/MemoryMgr.h"
#include "../GlowSystem/String.h"

#ifdef GLOWE_COMPILE_FOR_WINDOWS
#include "WindowsDynamicLib.h"
#endif
#ifdef GLOWE_COMPILE_FOR_LINUX
#include "LinuxDynamicLib.h"
#endif

#ifdef GLOWE_COMPILE_FOR_WINDOWS
#ifdef CPPSCRIPT_DLL_NOCRT
#ifdef __GNUC__
#ifndef _VCRTIMP
#define _VCRTIMP
#endif // _VCRTIMP
#ifndef _In_
#define _In_
#endif // _In_
typedef void* EXCEPTION_DISPOSITION;

#define EntryPointName DllMainCRTStartup
#else
#define EntryPointName _DllMainCRTStartup
#endif

#ifdef _M_IX86
#define __sec_check_cookie_decl __fastcall
#else
#define __sec_check_cookie_decl __cdecl
#endif
#define CppScriptDll() \
extern "C" int _cdecl _purecall(void) \
{ \
	return 0; \
} \
 \
extern "C" int _cdecl __cxa_pure_virtual(void) \
{ \
	return 0; \
} \
 \
extern "C" _VCRTIMP EXCEPTION_DISPOSITION __cdecl __CxxFrameHandler3( \
	void*, \
	void*, \
	void*, \
	void* \
) \
{ \
	return EXCEPTION_DISPOSITION(); \
} \
 \
extern "C" void __cdecl __std_terminate() \
{ \
} \
 \
void __sec_check_cookie_decl __security_check_cookie(uintptr_t _StackCookie) \
{ \
} \
 \
extern "C" BOOL WINAPI EntryPointName( \
	HINSTANCE const instance, \
	DWORD     const reason, \
	LPVOID    const reserved \
) \
{ \
	return TRUE; \
}
#else
#define CppScriptDll() \
BOOL WINAPI DllMain(HINSTANCE, DWORD, LPVOID) \
{ \
	return TRUE; \
}
#endif
#else
#define CppScriptDll()
#endif

#ifdef GLOWE_COMPILE_FOR_WINDOWS
#define CPPSCRIPT_DLLEXPORT __declspec(dllexport)
#else
#define CPPSCRIPT_DLLEXPORT
#endif

//static_assert(std::is_base_of<::GLOWE::BaseScriptBehaviour, extendedClass>::value, #extendedClass " must be a child of GLOWE::BaseScriptBehaviour."); \

#define CppScriptExportExtends(name, extendedClass) \
static_assert(std::is_base_of<extendedClass, name>::value, #name " must be a child of " #extendedClass "."); \
extern "C" inline CPPSCRIPT_DLLEXPORT ::GLOWE::BaseScriptBehaviour* __stdcall create##name(void* temp) { return static_cast<::GLOWE::BaseScriptBehaviour*>(new (temp) name); } \
extern "C" inline CPPSCRIPT_DLLEXPORT unsigned long long __stdcall getSizeOf##name() { return sizeof(name); } \
//extern "C" CPPSCRIPT_DLLEXPORT void __stdcall destroy##name(name* arg) { arg->~name(); }

#define GlowScriptExport(name) CppScriptExportExtends(name, ::GLOWE::BaseScriptBehaviour)

namespace GLOWE
{
	class BaseScriptBehaviour;
}

namespace CppScript
{
#if defined(GLOWE_COMPILE_FOR_WINDOWS)
	using DynamicLibrary = WindowsDynamicLibrary;
#elif defined(GLOWE_COMPILE_FOR_LINUX)
	using DynamicLibrary = LinuxDynamicLibrary;
#endif
	class Script;

#define constStr() getCharArray() // Calls c_str() on String object.
#define CppScriptNew(type) GLOWE::getInstance<GLOWE::GlobalAllocator>().allocate<type>()
#define CppScriptDelete(object) GLOWE::getInstance<GLOWE::GlobalAllocator>().deallocate(object)
#define CppScriptAllocSpace GLOWE::getInstance<GLOWE::GlobalAllocator>().allocateSpace
#define CppScriptDealloc GLOWE::getInstance<GLOWE::GlobalAllocator>().deallocate

	using GLOWE::BaseScriptBehaviour;

	class BaseScriptSerializer
	{
	private:
		virtual void _serialize(const char* name, const void* data, const unsigned int dataSize) = 0;
		virtual const void* _deserialize(const char* name) const = 0;
	public:
		template<class T>
		void serialize(const char* name, const T& arg)
		{
			_serialize(name, &arg, sizeof(T));
		}

		template<class T>
		T deserialize(const char* name) const
		{
			return T(*((const T*)(_deserialize(name))));
		}
	};

	using CreateFunc = BaseScriptBehaviour* (__stdcall*)(void*);
	using GetSizeFunc = unsigned long long(__stdcall*)();
	using DestroyFunc = void(__stdcall*)(void*);

	template<class T>
	using Vector = GLOWE::Vector<T>;
	template<class T>
	using Set = GLOWE::Set<T>;
	template<class Key, class Value>
	using Map = GLOWE::Map<Key, Value>;

	using GLOWE::String;

	// Works like scripts group.
	class GLOWSCRIPTING_EXPORT ScriptContext
	{
	private:
		Set<Script*> scripts;
	public:
		ScriptContext() = default;
		~ScriptContext();

		void registerScript(Script* script);
		void unregisterScript(Script* script);

		bool reloadLibrary(const DynamicLibrary& oldLib, const DynamicLibrary& newLib) const;

		Script* createScript(const String& name, const DynamicLibrary& lib);
		Script* cloneScript(Script* script);
		void destroyScript(Script* script);
	};

	class GLOWSCRIPTING_EXPORT ScriptSerializer
		: public BaseScriptSerializer
	{
	private:
		Map<String, String> storedData;
	public:
		virtual void _serialize(const char* name, const void* data, const unsigned int dataSize) override;
		virtual const void* _deserialize(const char* name) const override;
	};

	class Script
	{
	private:
		BaseScriptBehaviour* behaviour;
		ScriptContext* associatedContext;
		const DynamicLibrary* associatedLibrary;
		String scriptName;
		void* trueMemory;
	public:
		GLOWSCRIPTING_EXPORT Script() = default;
		Script(const Script&) = delete;
		GLOWSCRIPTING_EXPORT ~Script();

		Script& operator=(const Script&) = delete;

		GLOWSCRIPTING_EXPORT BaseScriptBehaviour& operator*();
		GLOWSCRIPTING_EXPORT const BaseScriptBehaviour& operator*() const;
		GLOWSCRIPTING_EXPORT BaseScriptBehaviour* operator->();
		GLOWSCRIPTING_EXPORT const BaseScriptBehaviour* operator->() const;

		GLOWSCRIPTING_EXPORT BaseScriptBehaviour* getBehaviourPtr() const;

		template<class T>
		T* getBehaviourAs() const;

		friend class ScriptContext;
	};

	template<class T>
	inline T* Script::getBehaviourAs() const
	{
		return dynamic_cast<T*>(behaviour);
	}
}

#endif
