#pragma once
#ifndef GLOWE_WINDOWS_DYNAMICLIB_INCLUDED
#define GLOWE_WINDOWS_DYNAMICLIB_INCLUDED

#include "glowscripting_export.h"

#include "../GlowSystem/Utility.h"

#ifdef GLOWE_COMPILE_FOR_WINDOWS
namespace CppScript
{
	class GLOWSCRIPTING_EXPORT WindowsDynamicLibrary
	{
	private:
		HMODULE handle;
	public:
		WindowsDynamicLibrary();
		~WindowsDynamicLibrary();

		bool load(const char* name);
		void unload();

		void* getMethod(const char* name) const;
		void* operator[](const char* name) const;

		bool operator==(const WindowsDynamicLibrary& right) const;
		bool operator!=(const WindowsDynamicLibrary& right) const;
		bool operator<(const WindowsDynamicLibrary& right) const;
		bool operator>(const WindowsDynamicLibrary& right) const;
	};
}
#endif

#endif
