#pragma once
#ifndef GLOWE_SCRIPTING_BASESCRIPTBEHAVIOUR_INCLUDED
#define GLOWE_SCRIPTING_BASESCRIPTBEHAVIOUR_INCLUDED

#include "CppScriptFoundations.h"

namespace GLOWE
{
	class GLOWSCRIPTING_EXPORT BaseScriptBehaviour
	{
	public:
		virtual ~BaseScriptBehaviour() = default;

		virtual void serialize(CppScript::ScriptSerializer& handle) const {};
		virtual void deserialize(CppScript::ScriptSerializer& handle) {};
	};
}

#endif
