#include "CppScriptFoundations.h"
#include "BaseScriptBehaviour.h"

CppScript::ScriptContext::~ScriptContext()
{
	for (auto x : scripts)
	{
		CppScriptDelete(x);
	}
}

void CppScript::ScriptContext::registerScript(Script* script)
{
	scripts.insert(script);
}

void CppScript::ScriptContext::unregisterScript(Script* script)
{
	scripts.erase(script);
}

bool CppScript::ScriptContext::reloadLibrary(const DynamicLibrary& oldLib, const DynamicLibrary& newLib) const
{
	CreateFunc func = nullptr;
	GetSizeFunc sizeFunc = nullptr;
	//DestroyFunc destroyFunc = nullptr;
	ScriptSerializer serializer;

	for (auto x : scripts)
	{
		if (*(x->associatedLibrary) == oldLib)
		{
			//destroyFunc = (DestroyFunc)newLib[(u8"destroy" + x->scriptName).constStr()];

			(*x)->serialize(serializer);
			x->behaviour->~BaseScriptBehaviour();
			//destroyFunc(x->behaviour);
			CppScriptDealloc(x->trueMemory);
			func = (CreateFunc)newLib[(u8"create" + x->scriptName).constStr()];
			sizeFunc = (GetSizeFunc)newLib[(u8"getSizeOf" + x->scriptName).constStr()];
			if (func && sizeFunc)
			{
				void* temp = CppScriptAllocSpace(sizeFunc());
				x->behaviour = func(temp);
				x->trueMemory = temp;
				(*x)->deserialize(serializer);
				x->associatedLibrary = &newLib;
			}
			else
			{
				return false;
			}
		}
	}

	return true;
}

CppScript::Script* CppScript::ScriptContext::createScript(const String& name, const DynamicLibrary& lib)
{
	CreateFunc func = (CreateFunc)lib[(u8"create" + name).constStr()];
	GetSizeFunc sizeFunc = (GetSizeFunc)lib[(u8"getSizeOf" + name).constStr()];

	if (func && sizeFunc)
	{
		Script* temp = CppScriptNew(Script);
		void* tempMem = CppScriptAllocSpace(sizeFunc());
		temp->associatedContext = this;
		temp->associatedLibrary = &lib;
		temp->behaviour = func(tempMem);
		temp->trueMemory = tempMem;
		temp->scriptName = name;
		registerScript(temp);

		return temp;
	}

	return nullptr;
}

CppScript::Script* CppScript::ScriptContext::cloneScript(Script* script)
{
	if (script && scripts.count(script))
	{
		Script* temp = CppScriptNew(Script);
		CreateFunc func = nullptr;
		GetSizeFunc sizeFunc = nullptr;

		temp->associatedLibrary = script->associatedLibrary;
		func = (CreateFunc)temp->associatedLibrary->getMethod(("create" + script->scriptName).constStr());
		sizeFunc = (GetSizeFunc)temp->associatedLibrary->getMethod((u8"getSizeOf" + script->scriptName).constStr());
		if (!func || !sizeFunc)
		{
			CppScriptDelete(temp);
			return nullptr;
		}
		void* tempMem = CppScriptAllocSpace(sizeFunc());
		temp->associatedContext = this;
		temp->behaviour = func(tempMem);
		temp->trueMemory = tempMem;
		ScriptSerializer serializer;
		script->behaviour->serialize(serializer);
		temp->behaviour->deserialize(serializer);
		temp->scriptName = script->scriptName;
		registerScript(temp);

		return temp;
	}

	return nullptr;
}

void CppScript::ScriptContext::destroyScript(Script* script)
{
	if (script && scripts.count(script))
	{
		CppScriptDelete(script);
		unregisterScript(script);
	}
}

CppScript::Script::~Script()
{
	if (behaviour && trueMemory && associatedLibrary)
	{
		//DestroyFunc func = (DestroyFunc)(associatedLibrary->getMethod(("destroy" + scriptName).constStr()));
		//if (func)
		{
			//func(behaviour);
			behaviour->~BaseScriptBehaviour();
			CppScriptDealloc(trueMemory);
		}

		behaviour = nullptr;
	}
}

GLOWE::BaseScriptBehaviour& CppScript::Script::operator*()
{
	return *behaviour;
}

const GLOWE::BaseScriptBehaviour& CppScript::Script::operator*() const
{
	return *behaviour;
}

GLOWE::BaseScriptBehaviour* CppScript::Script::operator->()
{
	return behaviour;
}

const GLOWE::BaseScriptBehaviour* CppScript::Script::operator->() const
{
	return behaviour;
}

GLOWE::BaseScriptBehaviour* CppScript::Script::getBehaviourPtr() const
{
	return behaviour;
}

void CppScript::ScriptSerializer::_serialize(const char* name, const void* data, const unsigned int dataSize)
{
	storedData.emplace(std::piecewise_construct, std::forward_as_tuple(name), std::forward_as_tuple((const char*)data, dataSize));
}

const void* CppScript::ScriptSerializer::_deserialize(const char* name) const
{
	const auto it = storedData.find(name);
	if (it != storedData.end())
	{
		return it->second.constStr();
	}

	return nullptr;
}
