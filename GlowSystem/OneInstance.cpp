#include "OneInstance.h"
#include "MemoryMgr.h"

class GLOWSYSTEM_EXPORT GlobalAllocWrapper
{
private:
	GLOWE::GlobalAllocator& alloc;
	GLOWE::GlobalAllocatorMgr& allocMgr;
	bool& isDuringCleanUp;
public:
	GlobalAllocWrapper(GLOWE::GlobalAllocatorMgr& mgr, bool& flag)
		: alloc(mgr.getAllocator(std::this_thread::get_id())), allocMgr(mgr), isDuringCleanUp(flag)
	{
	}
	~GlobalAllocWrapper()
	{
		alloc.markAsFreed();
		isDuringCleanUp = allocMgr.checkIsDuringCleanup();
	}

	GLOWE::GlobalAllocator& getAlloc()
	{
		return alloc;
	}
};

template<>
GLOWE::GlobalAllocator& GLOWE::OneInstance<GLOWE::GlobalAllocator>::instance()
{
	static bool isDuringCleanup = false;
	static GlobalAllocatorMgr& subsystem = getInstance<GlobalAllocatorMgr>();
	thread_local GlobalAllocWrapper globalAlloc(subsystem, isDuringCleanup);
	return (!isDuringCleanup) ? globalAlloc.getAlloc() : subsystem.getAllocator(std::this_thread::get_id());
}
