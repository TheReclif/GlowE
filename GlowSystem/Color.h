#pragma once
#ifndef GLOW_COLOR_INCLUDED
#define GLOW_COLOR_INCLUDED

#include "Utility.h"
#include "NumberStorage.h"
#include "SerializableDataCreator.h"

namespace GLOWE
{
	/// @brief Class used to represent RGBA colors in floating point range from 0 to 1, both inclusive.
	class GLOWSYSTEM_EXPORT alignas(16) Color
		: public StaticBinarySerializableTag
	{
	private:
		struct GLOWSYSTEM_EXPORT alignas(16) ColorBase
		{
			float rgba[4];

			operator const Color&() const;
			operator const Float4&() const;
		};
	public:
		union
		{
			float rgba[4];
			float rgb[3];
			struct
			{
				float r, g, b, a;
			};
		};

		/// @brief Default constructor. Initializes rgba to zeros.
		constexpr Color()
			: rgba{ 0, 0, 0, 0 }
		{}
		/// @brief Initializes rgba from the argument.
		/// @param arr RGBA float array. Floats must be in [0, 1] range, otherwise using this color is an UB.
		constexpr Color(const float arr[4])
			: rgba{ arr[0], arr[1], arr[2], arr[3] }
		{}
		/// @brief Initializes rgba from the argument.
		/// @param arr Color constant. Mostly used internally when creating color from eg. Color::white. Floats must be in [0, 1] range, otherwise using this color is an UB.
		constexpr Color(const ColorBase& arg)
			: rgba{ arg.rgba[0], arg.rgba[1], arg.rgba[2], arg.rgba[3] }
		{}
		/// @brief Initializes colors from the arguments.
		/// @param rr Red
		/// @param gg Green
		/// @param bb Blue
		/// @param aa Alpha. 1 by default
		constexpr Color(const float rr, const float gg, const float bb, const float aa = 1.0f)
			: rgba{ rr, gg, bb, aa }
		{}
		constexpr Color(const Float4& arg)
			: rgba{ arg[0], arg[1], arg[2], arg[3] }
		{}

		/// @brief Implicit conversion to 4 element float array.
		operator Float4() const;

		bool operator==(const Color& other) const;
		bool operator!=(const Color& other) const;

		/// @brief Sets the color from RGBA
		/// @param rr Red
		/// @param gg Green
		/// @param bb Blue
		/// @param aa Alpha
		void setRGBA(const float rr, const float gg, const float bb, const float aa);
		/// @brief Sets the color from BGRA. Not to be confused with RGBA.
		/// @param bb Blue
		/// @param gg Green
		/// @param rr Red
		/// @param aa Alpha
		void setBGRA(const float bb, const float gg, const float rr, const float aa);

		/// @brief Sets RGBA from just 3 colors. Alpha is set to 1.
		/// @param rr Red
		/// @param gg Green
		/// @param bb Blue
		void setRGB(const float rr, const float gg, const float bb);
		/// @brief Sets BGRA from just 3 colors. Alpha is set to 1. Not to be confused with RGBA.
		/// @param bb Blue
		/// @param gg Green
		/// @param rr Red
		void setBGR(const float bb, const float gg, const float rr);

		void serialize(LoadSaveHandle& handle) const;
		void deserialize(LoadSaveHandle& handle);

		/// @brief Creates a Color from quantized RGBA values.
		/// @param r Red
		/// @param g Green
		/// @param b Blue
		/// @param a Alpha. 255 by default
		/// @return Color created from these values
		static constexpr Color from255RGBA(const std::uint8_t r, const std::uint8_t g, const std::uint8_t b, const std::uint8_t a = 255)
		{
			return Color(static_cast<float>(r) / 255.0f, static_cast<float>(g) / 255.0f, static_cast<float>(b) / 255.0f, static_cast<float>(a) / 255.0f);
		}

		GlowStaticSerialize(GlowBases(), GlowField(rgba));
	public:
		static constexpr ColorBase white = { 1.0f, 1.0f, 1.0f, 1.0f };
		/// @brief A color that is perfectly half way from black and white.
		static constexpr ColorBase grey = { 0.5f, 0.5f, 0.5f, 1.0f };
		static constexpr ColorBase black = { 0.0f, 0.0f, 0.0f, 1.0f };

		static constexpr ColorBase red = { 1.0f, 0.0f, 0.0f, 1.0f };
		static constexpr ColorBase green = { 0.0f, 1.0f, 0.0f, 1.0f };
		static constexpr ColorBase blue = { 0.0f, 0.0f, 1.0f, 1.0f };

		static constexpr ColorBase cyan = { 0.0f, 1.0f, 1.0f, 1.0f };
		static constexpr ColorBase magenta = { 1.0f, 0.0f, 1.0f, 1.0f };
		static constexpr ColorBase yellow = { 1.0f, 1.0f, 0.0f, 1.0f };
	};
}

#endif
