#pragma once
#ifndef FILE_IO_INCLUDED
#define FILE_IO_INCLUDED

#include <fstream>
#include <string>
#include <iostream>

#include "String.h"

//using namespace std;

namespace GLOWE
{
	// TODO: Conflict with Error.h
	/*class GLOWSYSTEM_EXPORT FileIOException
		: public StringException
	{
	public:
		using StringException::StringException;
	};*/

	class GLOWSYSTEM_EXPORT File
	{
	public:
		enum OpenFlag
		{
			In = std::ios_base::in,
			Out = std::ios_base::out,
			Append = std::ios_base::app,
			AtEnd = std::ios_base::ate,
			Binary = std::ios_base::binary,
			Truncate = std::ios_base::trunc
		};
	public:
		using FileStreamType = std::fstream;

		enum class FilePointer
		{
			Read,
			Write,
			Both
		};

		enum class MovePointerFlags
		{
			None = 0,
			Begin,
			End
		};
	public:
		class GLOWSYSTEM_EXPORT BOMInformation
		{
		public:
			enum class FileEncoding
			{
				ASCII = 0,
				Utf8,
				Utf16LittleEndian,
				Utf16BigEndian,
				Utf32LittleEndian,
				Utf32BigEndian
			};
		public:
			std::size_t bomSize;
			FileEncoding encoding;
		public:
			inline BOMInformation() noexcept
				: bomSize(0), encoding(FileEncoding::ASCII)
			{
			}

			~BOMInformation() noexcept = default;

			inline bool checkIsBOMPresent() const noexcept
			{
				return bomSize > 0;
			}

			inline std::size_t getBOMSize() const noexcept
			{
				return bomSize;
			}

			inline FileEncoding getFileEncoding() const noexcept
			{
				return encoding;
			}

			void checkBOM(std::istream& str);

			static String fileEncodingToString(const FileEncoding& arg);
		};
	private:
		FileStreamType filestream;

		std::ios_base::openmode usedOpenFlags;

		BOMInformation bomInfo;
	public:
		File();
		File(File&& arg) noexcept;
		~File();

		void open(const String& filename, const unsigned int flags, bool saveFlagsIfFail = false); //flags - fstream flags
		void close();

		void movePointer(const std::size_t newPos, const FilePointer whichPointer = FilePointer::Both, const MovePointerFlags flags = MovePointerFlags::None);

		std::size_t getPointerPosition(const FilePointer whichPointer);
		std::size_t getFileSize(); // For Read returns realFileSize - BOM size. To obtain real file's size you must query BOMInformation and add BOM size to file size.

		File& write(const void* ptr, const std::size_t howMany);
		File& read(void* ptr, const std::size_t howMany);

		template<class T>
		File& operator << (const T& t);

		template<class T>
		File& operator >> (T& t);

		FileStreamType& getStream();

		bool checkIsOpen() const;
		operator bool() const;

		std::ios_base::openmode getUsedOpenFlags() const;
		BOMInformation getBOMInfo() const;

		static String readAll(const String& filename, const bool binary = false);
	};

	template<class T>
	inline File & File::operator <<(const T& t)
	{
		filestream << t;
		return *this;
	}

	template<class T>
	inline File & File::operator >> (T & t)
	{
		filestream >> t;
		return *this;
	}
}

#endif
