#pragma once
#ifndef GLOW_VALUESCLASSES_INCLUDED
#define GLOW_VALUESCLASSES_INCLUDED

#include "Utility.h"
#include "String.h"
#include "Error.h"
#include "Regex.h"

namespace GLOWE
{
	class GLOWSYSTEM_EXPORT Value
	{
	public:
		class GLOWSYSTEM_EXPORT ValueConversionException
			: public Exception
		{
		public:
			ValueConversionException();
			virtual ~ValueConversionException() = default;
		};
	public:
		enum class ValueType
		{
			Float,
			FloatArray,
			Int,
			IntArray,
			UInt,
			UIntArray,
			Double,
			DoubleArray,
			Bool,
			BoolArray,
			String,
			StringArray
		};
	public:
		Value() = default;
		virtual ~Value() = default;

		virtual float asFloat() const = 0;
		virtual void asFloatArray(Vector<float>& out) const = 0;

		virtual int asInt() const = 0;
		virtual void asIntArray(Vector<int>& out) const = 0;

		virtual unsigned int asUInt() const = 0;
		virtual void asUIntArray(Vector<unsigned int>& out) const = 0;

		virtual double asDouble() const = 0;
		virtual void asDoubleArray(Vector<double>& out) const = 0;

		virtual String asString() const = 0;
		virtual void asString(String& out) const = 0;
		virtual void asStringArray(Vector<String>& out) const = 0;

		virtual bool asBool() const = 0;
		virtual void asBoolArray(Vector<bool>& out) const = 0;

		virtual void fromFloat(const float& in) = 0;
		virtual void fromFloatArray(const float* in, const size_t arraySize) = 0;
		virtual void fromFloatArray(const Vector<float>& in) = 0;

		virtual void fromInt(const int& in) = 0;
		virtual void fromIntArray(const int* in, const size_t arraySize) = 0;
		virtual void fromIntArray(const Vector<int>& in) = 0;

		virtual void fromUInt(const unsigned int& in) = 0;
		virtual void fromUIntArray(const unsigned int* in, const size_t arraySize) = 0;
		virtual void fromUIntArray(const Vector<unsigned int>& in) = 0;

		virtual void fromDouble(const double& in) = 0;
		virtual void fromDoubleArray(const double* in, const size_t arraySize) = 0;
		virtual void fromDoubleArray(const Vector<double>& in) = 0;

		virtual void fromBool(const bool& in) = 0;
		virtual void fromBoolArray(const bool* in, const size_t arraySize) = 0;
		virtual void fromBoolArray(const Vector<bool>& in) = 0;

		virtual void fromString(const String& in) = 0;
		virtual void fromStringArray(const String* in, const size_t arraySize) = 0;
		virtual void fromStringArray(const Vector<String>& in) = 0;

		virtual const ValueType getType() const = 0;

		// Warning:
		// Returns 0 for non-array types.
		// Example: it will return 0 for FloatValue, but will return true array size for FloatArrayValue.
		virtual const size_t getArraySize() const = 0;

		static String getTypename(const ValueType& valType);
	};

	class GLOWSYSTEM_EXPORT FloatValue
		: public Value
	{
	private:
		float data;
	public:
		FloatValue();
		FloatValue(const float& arg);
		virtual ~FloatValue() = default;

		virtual float asFloat() const override;
		virtual void asFloatArray(Vector<float>& out) const override;

		virtual int asInt() const override;
		virtual void asIntArray(Vector<int>& out) const override;

		virtual unsigned int asUInt() const override;
		virtual void asUIntArray(Vector<unsigned int>& out) const override;

		virtual double asDouble() const override;
		virtual void asDoubleArray(Vector<double>& out) const override;

		virtual String asString() const override;
		virtual void asString(String& out) const override;

		virtual bool asBool() const override;
		virtual void asBoolArray(Vector<bool>& out) const override;

		virtual void fromFloat(const float& in) override;
		virtual void fromFloatArray(const float* in, const size_t arraySize) override;
		virtual void fromFloatArray(const Vector<float>& in) override;

		virtual void fromInt(const int& in) override;
		virtual void fromIntArray(const int* in, const size_t arraySize) override;
		virtual void fromIntArray(const Vector<int>& in) override;

		virtual void fromUInt(const unsigned int& in) override;
		virtual void fromUIntArray(const unsigned int* in, const size_t arraySize) override;
		virtual void fromUIntArray(const Vector<unsigned int>& in) override;

		virtual void fromDouble(const double& in) override;
		virtual void fromDoubleArray(const double* in, const size_t arraySize) override;
		virtual void fromDoubleArray(const Vector<double>& in) override;

		virtual void fromBool(const bool& in) override;
		virtual void fromBoolArray(const bool* in, const size_t arraySize) override;
		virtual void fromBoolArray(const Vector<bool>& in) override;

		virtual void fromString(const String& in) override;

		virtual const ValueType getType() const override;

		virtual const size_t getArraySize() const override;

		virtual void asStringArray(Vector<String>& out) const override;
		virtual void fromStringArray(const String * in, const size_t arraySize) override;
		virtual void fromStringArray(const Vector<String>& in) override;
	};

	class GLOWSYSTEM_EXPORT FloatArrayValue
		: public Value
	{
	private:
		Vector<float> data;
	public:
		FloatArrayValue() = default;
		FloatArrayValue(const float* arg, const size_t arraySize);
		FloatArrayValue(const Vector<float>& arg);
		virtual ~FloatArrayValue() = default;

		virtual float asFloat() const override;
		virtual void asFloatArray(Vector<float>& out) const override;

		virtual int asInt() const override;
		virtual void asIntArray(Vector<int>& out) const override;

		virtual unsigned int asUInt() const override;
		virtual void asUIntArray(Vector<unsigned int>& out) const override;

		virtual double asDouble() const override;
		virtual void asDoubleArray(Vector<double>& out) const override;

		virtual String asString() const override;
		virtual void asString(String& out) const override;

		virtual bool asBool() const override;
		virtual void asBoolArray(Vector<bool>& out) const override;

		virtual void fromFloat(const float& in) override;
		virtual void fromFloatArray(const float* in, const size_t arraySize) override;
		virtual void fromFloatArray(const Vector<float>& in) override;

		virtual void fromInt(const int& in) override;
		virtual void fromIntArray(const int* in, const size_t arraySize) override;
		virtual void fromIntArray(const Vector<int>& in) override;

		virtual void fromUInt(const unsigned int& in) override;
		virtual void fromUIntArray(const unsigned int* in, const size_t arraySize) override;
		virtual void fromUIntArray(const Vector<unsigned int>& in) override;

		virtual void fromDouble(const double& in) override;
		virtual void fromDoubleArray(const double* in, const size_t arraySize) override;
		virtual void fromDoubleArray(const Vector<double>& in) override;

		virtual void fromBool(const bool& in) override;
		virtual void fromBoolArray(const bool* in, const size_t arraySize) override;
		virtual void fromBoolArray(const Vector<bool>& in) override;

		virtual void fromString(const String& in) override;

		virtual const ValueType getType() const override;

		virtual const size_t getArraySize() const override;

		virtual void asStringArray(Vector<String>& out) const override;
		virtual void fromStringArray(const String * in, const size_t arraySize) override;
		virtual void fromStringArray(const Vector<String>& in) override;
	};

	class GLOWSYSTEM_EXPORT IntValue
		: public Value
	{
	private:
		signed int data;
	public:
		IntValue();
		IntValue(const signed int& arg);
		virtual ~IntValue() = default;

		virtual float asFloat() const override;
		virtual void asFloatArray(Vector<float>& out) const override;

		virtual int asInt() const override;
		virtual void asIntArray(Vector<int>& out) const override;

		virtual unsigned int asUInt() const override;
		virtual void asUIntArray(Vector<unsigned int>& out) const override;

		virtual double asDouble() const override;
		virtual void asDoubleArray(Vector<double>& out) const override;

		virtual String asString() const override;
		virtual void asString(String& out) const override;

		virtual bool asBool() const override;
		virtual void asBoolArray(Vector<bool>& out) const override;

		virtual void fromFloat(const float& in) override;
		virtual void fromFloatArray(const float* in, const size_t arraySize) override;
		virtual void fromFloatArray(const Vector<float>& in) override;

		virtual void fromInt(const int& in) override;
		virtual void fromIntArray(const int* in, const size_t arraySize) override;
		virtual void fromIntArray(const Vector<int>& in) override;

		virtual void fromUInt(const unsigned int& in) override;
		virtual void fromUIntArray(const unsigned int* in, const size_t arraySize) override;
		virtual void fromUIntArray(const Vector<unsigned int>& in) override;

		virtual void fromDouble(const double& in) override;
		virtual void fromDoubleArray(const double* in, const size_t arraySize) override;
		virtual void fromDoubleArray(const Vector<double>& in) override;

		virtual void fromBool(const bool& in) override;
		virtual void fromBoolArray(const bool* in, const size_t arraySize) override;
		virtual void fromBoolArray(const Vector<bool>& in) override;

		virtual void fromString(const String& in) override;

		virtual const ValueType getType() const override;

		virtual const size_t getArraySize() const override;

		virtual void asStringArray(Vector<String>& out) const override;
		virtual void fromStringArray(const String * in, const size_t arraySize) override;
		virtual void fromStringArray(const Vector<String>& in) override;
	};

	class GLOWSYSTEM_EXPORT IntArrayValue
		: public Value
	{
	private:
		Vector<signed int> data;
	public:
		IntArrayValue() = default;
		IntArrayValue(const int* arg, const size_t arraySize);
		IntArrayValue(const Vector<signed int>& arg);
		virtual ~IntArrayValue() = default;

		virtual float asFloat() const override;
		virtual void asFloatArray(Vector<float>& out) const override;

		virtual int asInt() const override;
		virtual void asIntArray(Vector<int>& out) const override;

		virtual unsigned int asUInt() const override;
		virtual void asUIntArray(Vector<unsigned int>& out) const override;

		virtual double asDouble() const override;
		virtual void asDoubleArray(Vector<double>& out) const override;

		virtual String asString() const override;
		virtual void asString(String& out) const override;

		virtual bool asBool() const override;
		virtual void asBoolArray(Vector<bool>& out) const override;

		virtual void fromFloat(const float& in) override;
		virtual void fromFloatArray(const float* in, const size_t arraySize) override;
		virtual void fromFloatArray(const Vector<float>& in) override;

		virtual void fromInt(const int& in) override;
		virtual void fromIntArray(const int* in, const size_t arraySize) override;
		virtual void fromIntArray(const Vector<int>& in) override;

		virtual void fromUInt(const unsigned int& in) override;
		virtual void fromUIntArray(const unsigned int* in, const size_t arraySize) override;
		virtual void fromUIntArray(const Vector<unsigned int>& in) override;

		virtual void fromDouble(const double& in) override;
		virtual void fromDoubleArray(const double* in, const size_t arraySize) override;
		virtual void fromDoubleArray(const Vector<double>& in) override;

		virtual void fromBool(const bool& in) override;
		virtual void fromBoolArray(const bool* in, const size_t arraySize) override;
		virtual void fromBoolArray(const Vector<bool>& in) override;

		virtual void fromString(const String& in) override;

		virtual const ValueType getType() const override;

		virtual const size_t getArraySize() const override;

		virtual void asStringArray(Vector<String>& out) const override;
		virtual void fromStringArray(const String * in, const size_t arraySize) override;
		virtual void fromStringArray(const Vector<String>& in) override;
	};

	class GLOWSYSTEM_EXPORT UIntValue
		: public Value
	{
	private:
		unsigned int data;
	public:
		UIntValue();
		UIntValue(const signed int& arg);
		virtual ~UIntValue() = default;

		virtual float asFloat() const override;
		virtual void asFloatArray(Vector<float>& out) const override;

		virtual int asInt() const override;
		virtual void asIntArray(Vector<int>& out) const override;

		virtual unsigned int asUInt() const override;
		virtual void asUIntArray(Vector<unsigned int>& out) const override;

		virtual double asDouble() const override;
		virtual void asDoubleArray(Vector<double>& out) const override;

		virtual String asString() const override;
		virtual void asString(String& out) const override;

		virtual bool asBool() const override;
		virtual void asBoolArray(Vector<bool>& out) const override;

		virtual void fromFloat(const float& in) override;
		virtual void fromFloatArray(const float* in, const size_t arraySize) override;
		virtual void fromFloatArray(const Vector<float>& in) override;

		virtual void fromInt(const int& in) override;
		virtual void fromIntArray(const int* in, const size_t arraySize) override;
		virtual void fromIntArray(const Vector<int>& in) override;

		virtual void fromUInt(const unsigned int& in) override;
		virtual void fromUIntArray(const unsigned int* in, const size_t arraySize) override;
		virtual void fromUIntArray(const Vector<unsigned int>& in) override;

		virtual void fromDouble(const double& in) override;
		virtual void fromDoubleArray(const double* in, const size_t arraySize) override;
		virtual void fromDoubleArray(const Vector<double>& in) override;

		virtual void fromBool(const bool& in) override;
		virtual void fromBoolArray(const bool* in, const size_t arraySize) override;
		virtual void fromBoolArray(const Vector<bool>& in) override;

		virtual void fromString(const String& in) override;

		virtual const ValueType getType() const override;

		virtual const size_t getArraySize() const override;

		virtual void asStringArray(Vector<String>& out) const override;
		virtual void fromStringArray(const String * in, const size_t arraySize) override;
		virtual void fromStringArray(const Vector<String>& in) override;
	};

	class GLOWSYSTEM_EXPORT UIntArrayValue
		: public Value
	{
	private:
		Vector<unsigned int> data;
	public:
		UIntArrayValue() = default;
		UIntArrayValue(const unsigned int* arg, const size_t arraySize);
		UIntArrayValue(const Vector<unsigned int>& arg);
		virtual ~UIntArrayValue() = default;

		virtual float asFloat() const override;
		virtual void asFloatArray(Vector<float>& out) const override;

		virtual int asInt() const override;
		virtual void asIntArray(Vector<int>& out) const override;

		virtual unsigned int asUInt() const override;
		virtual void asUIntArray(Vector<unsigned int>& out) const override;

		virtual double asDouble() const override;
		virtual void asDoubleArray(Vector<double>& out) const override;

		virtual String asString() const override;
		virtual void asString(String& out) const override;

		virtual bool asBool() const override;
		virtual void asBoolArray(Vector<bool>& out) const override;

		virtual void fromFloat(const float& in) override;
		virtual void fromFloatArray(const float* in, const size_t arraySize) override;
		virtual void fromFloatArray(const Vector<float>& in) override;

		virtual void fromInt(const int& in) override;
		virtual void fromIntArray(const int* in, const size_t arraySize) override;
		virtual void fromIntArray(const Vector<int>& in) override;

		virtual void fromUInt(const unsigned int& in) override;
		virtual void fromUIntArray(const unsigned int* in, const size_t arraySize) override;
		virtual void fromUIntArray(const Vector<unsigned int>& in) override;

		virtual void fromDouble(const double& in) override;
		virtual void fromDoubleArray(const double* in, const size_t arraySize) override;
		virtual void fromDoubleArray(const Vector<double>& in) override;

		virtual void fromBool(const bool& in) override;
		virtual void fromBoolArray(const bool* in, const size_t arraySize) override;
		virtual void fromBoolArray(const Vector<bool>& in) override;

		virtual void fromString(const String& in) override;

		virtual const ValueType getType() const override;

		virtual const size_t getArraySize() const override;

		virtual void asStringArray(Vector<String>& out) const override;
		virtual void fromStringArray(const String * in, const size_t arraySize) override;
		virtual void fromStringArray(const Vector<String>& in) override;
	};

	class GLOWSYSTEM_EXPORT DoubleValue
		: public Value
	{
	private:
		double data;
	public:
		DoubleValue();
		DoubleValue(const double& arg);
		virtual ~DoubleValue() = default;

		virtual float asFloat() const override;
		virtual void asFloatArray(Vector<float>& out) const override;

		virtual int asInt() const override;
		virtual void asIntArray(Vector<int>& out) const override;

		virtual unsigned int asUInt() const override;
		virtual void asUIntArray(Vector<unsigned int>& out) const override;

		virtual double asDouble() const override;
		virtual void asDoubleArray(Vector<double>& out) const override;

		virtual String asString() const override;
		virtual void asString(String& out) const override;

		virtual bool asBool() const override;
		virtual void asBoolArray(Vector<bool>& out) const override;

		virtual void fromFloat(const float& in) override;
		virtual void fromFloatArray(const float* in, const size_t arraySize) override;
		virtual void fromFloatArray(const Vector<float>& in) override;

		virtual void fromInt(const int& in) override;
		virtual void fromIntArray(const int* in, const size_t arraySize) override;
		virtual void fromIntArray(const Vector<int>& in) override;

		virtual void fromUInt(const unsigned int& in) override;
		virtual void fromUIntArray(const unsigned int* in, const size_t arraySize) override;
		virtual void fromUIntArray(const Vector<unsigned int>& in) override;

		virtual void fromDouble(const double& in) override;
		virtual void fromDoubleArray(const double* in, const size_t arraySize) override;
		virtual void fromDoubleArray(const Vector<double>& in) override;

		virtual void fromBool(const bool& in) override;
		virtual void fromBoolArray(const bool* in, const size_t arraySize) override;
		virtual void fromBoolArray(const Vector<bool>& in) override;

		virtual void fromString(const String& in) override;

		virtual const ValueType getType() const override;

		virtual const size_t getArraySize() const override;

		virtual void asStringArray(Vector<String>& out) const override;
		virtual void fromStringArray(const String * in, const size_t arraySize) override;
		virtual void fromStringArray(const Vector<String>& in) override;
	};

	class GLOWSYSTEM_EXPORT DoubleArrayValue
		: public Value
	{
	private:
		Vector<double> data;
	public:
		DoubleArrayValue() = default;
		DoubleArrayValue(const double* arg, const size_t arraySize);
		DoubleArrayValue(const Vector<double>& arg);
		virtual ~DoubleArrayValue() = default;

		virtual float asFloat() const override;
		virtual void asFloatArray(Vector<float>& out) const override;

		virtual int asInt() const override;
		virtual void asIntArray(Vector<int>& out) const override;

		virtual unsigned int asUInt() const override;
		virtual void asUIntArray(Vector<unsigned int>& out) const override;

		virtual double asDouble() const override;
		virtual void asDoubleArray(Vector<double>& out) const override;

		virtual String asString() const override;
		virtual void asString(String& out) const override;

		virtual bool asBool() const override;
		virtual void asBoolArray(Vector<bool>& out) const override;

		virtual void fromFloat(const float& in) override;
		virtual void fromFloatArray(const float* in, const size_t arraySize) override;
		virtual void fromFloatArray(const Vector<float>& in) override;

		virtual void fromInt(const int& in) override;
		virtual void fromIntArray(const int* in, const size_t arraySize) override;
		virtual void fromIntArray(const Vector<int>& in) override;

		virtual void fromUInt(const unsigned int& in) override;
		virtual void fromUIntArray(const unsigned int* in, const size_t arraySize) override;
		virtual void fromUIntArray(const Vector<unsigned int>& in) override;

		virtual void fromDouble(const double& in) override;
		virtual void fromDoubleArray(const double* in, const size_t arraySize) override;
		virtual void fromDoubleArray(const Vector<double>& in) override;

		virtual void fromBool(const bool& in) override;
		virtual void fromBoolArray(const bool* in, const size_t arraySize) override;
		virtual void fromBoolArray(const Vector<bool>& in) override;

		virtual void fromString(const String& in) override;

		virtual const ValueType getType() const override;

		virtual const size_t getArraySize() const override;

		virtual void asStringArray(Vector<String>& out) const override;
		virtual void fromStringArray(const String * in, const size_t arraySize) override;
		virtual void fromStringArray(const Vector<String>& in) override;
	};

	class GLOWSYSTEM_EXPORT StringValue
		: public Value
	{
	private:
		String data;
	public:
		StringValue() = default;
		StringValue(const String& arg);
		virtual ~StringValue() = default;

		virtual float asFloat() const override;
		virtual void asFloatArray(Vector<float>& out) const override;

		virtual int asInt() const override;
		virtual void asIntArray(Vector<int>& out) const override;

		virtual unsigned int asUInt() const override;
		virtual void asUIntArray(Vector<unsigned int>& out) const override;

		virtual double asDouble() const override;
		virtual void asDoubleArray(Vector<double>& out) const override;

		virtual String asString() const override;
		virtual void asString(String& out) const override;

		virtual bool asBool() const override;
		virtual void asBoolArray(Vector<bool>& out) const override;

		virtual void fromFloat(const float& in) override;
		virtual void fromFloatArray(const float* in, const size_t arraySize) override;
		virtual void fromFloatArray(const Vector<float>& in) override;

		virtual void fromInt(const int& in) override;
		virtual void fromIntArray(const int* in, const size_t arraySize) override;
		virtual void fromIntArray(const Vector<int>& in) override;

		virtual void fromUInt(const unsigned int& in) override;
		virtual void fromUIntArray(const unsigned int* in, const size_t arraySize) override;
		virtual void fromUIntArray(const Vector<unsigned int>& in) override;

		virtual void fromDouble(const double& in) override;
		virtual void fromDoubleArray(const double* in, const size_t arraySize) override;
		virtual void fromDoubleArray(const Vector<double>& in) override;

		virtual void fromBool(const bool& in) override;
		virtual void fromBoolArray(const bool* in, const size_t arraySize) override;
		virtual void fromBoolArray(const Vector<bool>& in) override;

		virtual void fromString(const String& in) override;

		virtual const ValueType getType() const override;

		virtual const size_t getArraySize() const override;

		virtual void asStringArray(Vector<String>& out) const override;
		virtual void fromStringArray(const String * in, const size_t arraySize) override;
		virtual void fromStringArray(const Vector<String>& in) override;
	};

	class GLOWSYSTEM_EXPORT StringArrayValue
		: public Value
	{
	private:
		Vector<String> data;
	public:
		StringArrayValue() = default;
		StringArrayValue(const String* arg, const size_t arraySize);
		StringArrayValue(const Vector<String>& arg);
		virtual ~StringArrayValue() = default;

		virtual float asFloat() const override;
		virtual void asFloatArray(Vector<float>& out) const override;

		virtual int asInt() const override;
		virtual void asIntArray(Vector<int>& out) const override;

		virtual unsigned int asUInt() const override;
		virtual void asUIntArray(Vector<unsigned int>& out) const override;

		virtual double asDouble() const override;
		virtual void asDoubleArray(Vector<double>& out) const override;

		virtual String asString() const override;
		virtual void asString(String& out) const override;

		virtual bool asBool() const override;
		virtual void asBoolArray(Vector<bool>& out) const override;

		virtual void fromFloat(const float& in) override;
		virtual void fromFloatArray(const float* in, const size_t arraySize) override;
		virtual void fromFloatArray(const Vector<float>& in) override;

		virtual void fromInt(const int& in) override;
		virtual void fromIntArray(const int* in, const size_t arraySize) override;
		virtual void fromIntArray(const Vector<int>& in) override;

		virtual void fromUInt(const unsigned int& in) override;
		virtual void fromUIntArray(const unsigned int* in, const size_t arraySize) override;
		virtual void fromUIntArray(const Vector<unsigned int>& in) override;

		virtual void fromDouble(const double& in) override;
		virtual void fromDoubleArray(const double* in, const size_t arraySize) override;
		virtual void fromDoubleArray(const Vector<double>& in) override;

		virtual void fromBool(const bool& in) override;
		virtual void fromBoolArray(const bool* in, const size_t arraySize) override;
		virtual void fromBoolArray(const Vector<bool>& in) override;

		virtual void fromString(const String& in) override;

		virtual const ValueType getType() const override;

		virtual const size_t getArraySize() const override;

		virtual void asStringArray(Vector<String>& out) const override;
		virtual void fromStringArray(const String * in, const size_t arraySize) override;
		virtual void fromStringArray(const Vector<String>& in) override;
	};

	class GLOWSYSTEM_EXPORT BoolValue
		: public Value
	{
	private:
		bool data;
	public:
		BoolValue();
		BoolValue(const bool& arg);
		virtual ~BoolValue() = default;

		virtual float asFloat() const override;
		virtual void asFloatArray(Vector<float>& out) const override;

		virtual int asInt() const override;
		virtual void asIntArray(Vector<int>& out) const override;

		virtual unsigned int asUInt() const override;
		virtual void asUIntArray(Vector<unsigned int>& out) const override;

		virtual double asDouble() const override;
		virtual void asDoubleArray(Vector<double>& out) const override;

		virtual String asString() const override;
		virtual void asString(String& out) const override;

		virtual bool asBool() const override;
		virtual void asBoolArray(Vector<bool>& out) const override;

		virtual void fromFloat(const float& in) override;
		virtual void fromFloatArray(const float* in, const size_t arraySize) override;
		virtual void fromFloatArray(const Vector<float>& in) override;

		virtual void fromInt(const int& in) override;
		virtual void fromIntArray(const int* in, const size_t arraySize) override;
		virtual void fromIntArray(const Vector<int>& in) override;

		virtual void fromUInt(const unsigned int& in) override;
		virtual void fromUIntArray(const unsigned int* in, const size_t arraySize) override;
		virtual void fromUIntArray(const Vector<unsigned int>& in) override;

		virtual void fromDouble(const double& in) override;
		virtual void fromDoubleArray(const double* in, const size_t arraySize) override;
		virtual void fromDoubleArray(const Vector<double>& in) override;

		virtual void fromBool(const bool& in) override;
		virtual void fromBoolArray(const bool* in, const size_t arraySize) override;
		virtual void fromBoolArray(const Vector<bool>& in) override;

		virtual void fromString(const String& in) override;

		virtual const ValueType getType() const override;

		virtual const size_t getArraySize() const override;

		virtual void asStringArray(Vector<String>& out) const override;
		virtual void fromStringArray(const String * in, const size_t arraySize) override;
		virtual void fromStringArray(const Vector<String>& in) override;
	};

	class GLOWSYSTEM_EXPORT BoolArrayValue
		: public Value
	{
	private:
		Vector<bool> data;
	public:
		BoolArrayValue() = default;
		BoolArrayValue(const bool* arg, const size_t arraySize);
		BoolArrayValue(const Vector<bool>& arg);
		virtual ~BoolArrayValue() = default;

		virtual float asFloat() const override;
		virtual void asFloatArray(Vector<float>& out) const override;

		virtual int asInt() const override;
		virtual void asIntArray(Vector<int>& out) const override;

		virtual unsigned int asUInt() const override;
		virtual void asUIntArray(Vector<unsigned int>& out) const override;

		virtual double asDouble() const override;
		virtual void asDoubleArray(Vector<double>& out) const override;

		virtual String asString() const override;
		virtual void asString(String& out) const override;

		virtual bool asBool() const override;
		virtual void asBoolArray(Vector<bool>& out) const override;

		virtual void fromFloat(const float& in) override;
		virtual void fromFloatArray(const float* in, const size_t arraySize) override;
		virtual void fromFloatArray(const Vector<float>& in) override;

		virtual void fromInt(const int& in) override;
		virtual void fromIntArray(const int* in, const size_t arraySize) override;
		virtual void fromIntArray(const Vector<int>& in) override;

		virtual void fromUInt(const unsigned int& in) override;
		virtual void fromUIntArray(const unsigned int* in, const size_t arraySize) override;
		virtual void fromUIntArray(const Vector<unsigned int>& in) override;

		virtual void fromDouble(const double& in) override;
		virtual void fromDoubleArray(const double* in, const size_t arraySize) override;
		virtual void fromDoubleArray(const Vector<double>& in) override;

		virtual void fromBool(const bool& in) override;
		virtual void fromBoolArray(const bool* in, const size_t arraySize) override;
		virtual void fromBoolArray(const Vector<bool>& in) override;

		virtual void fromString(const String& in) override;

		virtual const ValueType getType() const override;

		virtual const size_t getArraySize() const override;

		virtual void asStringArray(Vector<String>& out) const override;
		virtual void fromStringArray(const String * in, const size_t arraySize) override;
		virtual void fromStringArray(const Vector<String>& in) override;
	};
}

#endif
