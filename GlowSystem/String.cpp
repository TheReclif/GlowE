#include "String.h"
#include "FileIO.h"
#include "UtfStringConverter.h"
#include "LoadSaveHandle.h"
#include "Error.h"

#include <bitset>
#include <charconv>

#include <unicode/utypes.h>
#include <unicode/uchar.h>
#include <unicode/locid.h>
#include <unicode/ustring.h>
#include <unicode/ucnv.h>
#include <unicode/unistr.h>
#include <unicode/ucasemap.h>

const std::size_t GLOWE::String::invalidPos = GLOWE::String::BasicString::npos;

class CaseMapService
	: public GLOWE::Subsystem
{
private:
	GLOWE::Mutex mutex;
	GLOWE::Map<GLOWE::String, UCaseMap*> caseMaps;
	UCaseMap* defaultLocaleCaseMap;
public:
	CaseMapService()
		: mutex(), caseMaps(), defaultLocaleCaseMap()
	{
		using namespace GLOWE;
		UErrorCode errCode = UErrorCode::U_ZERO_ERROR;
		defaultLocaleCaseMap = ucasemap_open(nullptr, 0, &errCode);
		if (errCode != UErrorCode::U_ZERO_ERROR)
		{
			throw StringException("Unable to open case map for the default locale: "_str + u_errorName(errCode));
		}
	}

	virtual ~CaseMapService() override
	{
		ucasemap_close(defaultLocaleCaseMap);
		for (const auto& x : caseMaps)
		{
			ucasemap_close(x.second);
		}
	}

	UCaseMap* getMap(const char* const name)
	{
		using namespace GLOWE;
		if (name == nullptr)
		{
			return defaultLocaleCaseMap;
		}

		DefaultLockGuard guard(mutex);

		const auto it = caseMaps.find(name);
		if (it != caseMaps.end())
		{
			return it->second;
		}

		UErrorCode errCode = UErrorCode::U_ZERO_ERROR;
		const auto caseMap = ucasemap_open(name, 0, &errCode);
		if (errCode != UErrorCode::U_ZERO_ERROR)
		{
			throw StringException("Unable to open case map for the \""_str + name + "\" locale: " + u_errorName(errCode));
		}
		return caseMaps.emplace(name, caseMap).first->second;
	}
};

unsigned char getBytesCountOfCharacter(char arg)
{
	if (!arg)
	{
		return 0;
	}

	unsigned char result = 0;

	while (arg & 128U)
	{
		arg <<= 1U;
		++result;
	}

	return std::max(result, static_cast<unsigned char>(1U));
}

GLOWE::String::String(const String & str)
	: baseString(str.baseString)
{
}

GLOWE::String::String(String && str) noexcept
	: baseString(std::move(str.baseString))
{
}

GLOWE::String::String(std::size_t strSize)
	: baseString(strSize, '\0')
{
}

GLOWE::String::String(const char * buffer, std::size_t buffSize)
	: baseString(buffer, buffSize)
{
}

GLOWE::String::String(const std::size_t howMany, const BasicChar ch)
	: baseString(howMany, ch)
{
}

GLOWE::String::String(const Utf8String & str)
	: baseString(str)
{
}

GLOWE::String::String(const Utf16String & str)
{
	Utf<8>::fromUtf16(str, baseString);
}

GLOWE::String::String(const Utf32String & str)
{
	Utf<8>::fromUtf32(str, baseString);
}

GLOWE::String::String(const WideString & str)
{
	Utf<8>::fromWide(str, baseString);
}

GLOWE::String::String(Utf8String && str)
	: baseString(std::forward<Utf8String>(str))
{
}

GLOWE::String::String(Utf16String && str)
{
	Utf<8>::fromUtf16(std::forward<Utf16String>(str), baseString);
}

GLOWE::String::String(Utf32String && str)
{
	Utf<8>::fromUtf32(std::forward<Utf32String>(str), baseString);
}

GLOWE::String::String(WideString && str)
{
	Utf<8>::fromWide(std::forward<WideString>(str), baseString);
}

GLOWE::String::String(const std::string & str)
	: baseString(str.c_str())
{
}

GLOWE::String::String(const std::wstring & str)
{
	Utf<8>::fromWide(str.c_str(), baseString);
}

GLOWE::String::String(std::string && str)
	: baseString(str.c_str())
{
}

GLOWE::String::String(std::wstring && str)
{
	Utf<8>::fromWide(WideString(str.c_str()), baseString);
}

GLOWE::String::String(const BasicChar ch)
	: baseString(1, ch)
{
}

GLOWE::String::String(const Utf16Char ch)
{
	char tempBuff[9] = "";
	static const CodecvtUtf8ToUtf16& f = FacetProvider<CodecvtUtf8ToUtf16>::getFacet();// = std::use_facet<CodecvtUtf8ToUtf16>(getInstance<Locale>().getLocale());
	std::mbstate_t mbstate = std::mbstate_t();
	const CodecvtUtf8ToUtf16::intern_type* temp;
	char* temp1;

	f.out(mbstate, GlowCodecvtWorkaroundChar16 (&ch), GlowCodecvtWorkaroundChar16(&ch + 1), temp, tempBuff, tempBuff + 9, temp1);
	baseString.reserve(temp1 - tempBuff);
	baseString.append(tempBuff, temp1 - tempBuff);
}

GLOWE::String::String(const Utf32Char ch)
{
	char tempBuff[9] = "";
	static const CodecvtUtf8ToUtf32& f = FacetProvider<CodecvtUtf8ToUtf32>::getFacet();// = std::use_facet<CodecvtUtf8ToUtf32>(getInstance<Locale>().getLocale());
	std::mbstate_t mbstate = std::mbstate_t();
	const CodecvtUtf8ToUtf32::intern_type* temp;
	char* temp1;

	f.out(mbstate, GlowCodecvtWorkaroundChar32(&ch), GlowCodecvtWorkaroundChar32(&ch + 1), temp, tempBuff, tempBuff + 9, temp1);
	baseString.reserve(temp1 - tempBuff);
	baseString.append(tempBuff, temp1 - tempBuff);
}

GLOWE::String::String(const BasicChar * str)
	: baseString(str)
{
}

GLOWE::String::String(const WideChar * str)
{
	Utf<8>::fromWide(str, baseString);
}

GLOWE::String::String(StringView str)
	: baseString(str)
{
}

void GLOWE::String::serialize(LoadSaveHandle& handle) const
{
	const UInt64 siz = baseString.size();
	handle << siz;
	if (siz > 0)
	{
		handle.write(baseString.c_str(), sizeof(BasicChar) * siz);
	}
}

void GLOWE::String::deserialize(LoadSaveHandle& handle)
{
	UInt64 siz{};
	handle >> siz;
	baseString.resize(siz);
	if (siz > 0)
	{
		handle.read(&baseString.front(), sizeof(BasicChar) * siz);
	}
}

GLOWE::Hash GLOWE::String::getHash() const
{
	return *this;
}

const char * GLOWE::String::getCharArray() const
{
	return baseString.c_str();
}

char * GLOWE::String::getCharArray()
{
	return &baseString[0];
}

GLOWE::String::BasicString & GLOWE::String::getString()
{
	return baseString;
}

const GLOWE::String::BasicString & GLOWE::String::getString() const
{
	return baseString;
}

GLOWE::String::BasicChar & GLOWE::String::getLastChar()
{
	return baseString.back();
}

const GLOWE::String::BasicChar & GLOWE::String::getLastChar() const
{
	return baseString.back();
}

GLOWE::String::BasicChar & GLOWE::String::getFirstChar()
{
	return baseString.front();
}

const GLOWE::String::BasicChar & GLOWE::String::getFirstChar() const
{
	return baseString.front();
}

GLOWE::String::BasicChar GLOWE::String::operator[](const unsigned int index) const
{
	return baseString[index];
}

GLOWE::String::BasicChar & GLOWE::String::operator[](const unsigned int index)
{
	return baseString[index];
}

GLOWE::String GLOWE::String::getToken(const char* delims, const unsigned int whichToken) const
{
	std::size_t pos = 0, posLast = baseString.find_first_of(delims), x = 0;

	while (x < whichToken && posLast != String::invalidPos)
	{
		pos = posLast + 1;
		posLast = baseString.find_first_of(delims, pos);
		++x;
	}

	return getSubstring(pos, posLast - pos);
}

unsigned int GLOWE::String::getTokensCount(const char * delims) const
{
	unsigned int x = 0;
	int pos = -1;

	do
	{
		pos = baseString.find_first_of(delims, pos + 1);
		++x;
	} 
	while (pos != String::invalidPos);

	return x;
}

GLOWE::Vector<GLOWE::String> GLOWE::String::tokenize(const char * delims) const
{
	Vector<String> result;

	int pos = -1;
	const auto begIt = baseString.begin(), endIt = baseString.end();

	do
	{
		const auto findPos = baseString.find_first_of(delims, pos + 1);
		result.emplace_back(begIt + (pos + 1), findPos != String::invalidPos ? (begIt + findPos) : endIt);
		pos = findPos != String::invalidPos ? findPos : std::numeric_limits<int>::min();
	} while (pos != std::numeric_limits<int>::min());

	return result;
}

std::size_t GLOWE::String::find(const BasicChar toFind, const std::size_t pos) const
{
	return baseString.find(toFind, pos);
}

std::size_t GLOWE::String::find(const String& toFind, const std::size_t pos) const
{
	return baseString.find(toFind.getString(), pos);
}

std::size_t GLOWE::String::rfind(const BasicChar toFind, const std::size_t pos) const
{
	return baseString.rfind(toFind, pos);
}

std::size_t GLOWE::String::rfind(const String& toFind, const std::size_t pos) const
{
	return baseString.rfind(toFind.getString(), pos);
}

GLOWE::String & GLOWE::String::replace(const std::size_t pos, const std::size_t howMany, const String& withWhat)
{
	baseString.replace(pos, howMany, withWhat.getString());
	return *this;
}

GLOWE::String & GLOWE::String::replace(const String & what, const String & withWhat)
{
	// TODO: Fix this.
	for (std::size_t findRes = baseString.find(what.getString()); 
		findRes != invalidPos; 
		findRes = baseString.find(what.getString()))
	{
		replace(findRes, what.getSize(), withWhat);
	}

	return *this;
}

void GLOWE::String::resize(const std::size_t newSize)
{
	baseString.resize(newSize);
}

void GLOWE::String::resize(const std::size_t newSize, const BasicChar ch)
{
	baseString.resize(newSize, ch);
}

void GLOWE::String::reserve(const std::size_t newSize)
{
	baseString.reserve(newSize);
}

static void toUppercase(GLOWE::String& out, const char* const locale)
{
	using namespace GLOWE;

	UErrorCode errCode = UErrorCode::U_ZERO_ERROR;
	const auto caseMap = getInstance<CaseMapService>().getMap(locale);
	int32_t buffSize = ucasemap_utf8ToUpper(caseMap, nullptr, 0, out.getCharArray(), -1, &errCode);
	if (errCode == UErrorCode::U_BUFFER_OVERFLOW_ERROR)
	{
		String temp(buffSize, '\0');
		errCode = UErrorCode::U_ZERO_ERROR;
		ucasemap_utf8ToUpper(caseMap, temp.getCharArray(), temp.getSize() + 1, out.getCharArray(), -1, &errCode);
		if (errCode != UErrorCode::U_ZERO_ERROR)
		{
			throw StringException("Lowercasing failed: "_str + u_errorName(errCode));
		}
		out = std::move(temp);
	}
	else
	{
		throw StringException("Lowercasing failed: "_str + u_errorName(errCode));
	}
}

void GLOWE::String::toUppercase(const char* const locale)
{
	::toUppercase(*this, locale);
}

GLOWE::String GLOWE::String::asUppercase(const char* const locale) const
{
	String result(*this);
	result.toUppercase(locale);
	return result;
}

static void toLowercase(GLOWE::String& out, const char* const locale)
{
	using namespace GLOWE;

	UErrorCode errCode = UErrorCode::U_ZERO_ERROR;
	const auto caseMap = getInstance<CaseMapService>().getMap(locale);
	int32_t buffSize = ucasemap_utf8ToLower(caseMap, nullptr, 0 , out.getCharArray(), -1, &errCode);
	if (errCode == UErrorCode::U_BUFFER_OVERFLOW_ERROR)
	{
		String temp(buffSize, '\0');
		errCode = UErrorCode::U_ZERO_ERROR;
		ucasemap_utf8ToLower(caseMap, temp.getCharArray(), temp.getSize() + 1, out.getCharArray(), -1, &errCode);
		if (errCode != UErrorCode::U_ZERO_ERROR)
		{
			throw StringException("Lowercasing failed: "_str + u_errorName(errCode));
		}
		out = std::move(temp);
	}
	else
	{
		throw StringException("Lowercasing failed: "_str + u_errorName(errCode));
	}
}

void GLOWE::String::toLowercase(const char* const locale)
{
	::toLowercase(*this, locale);
}

GLOWE::String GLOWE::String::asLowercase(const char* const locale) const
{
	String result(*this);
	result.toLowercase(locale);
	return result;
}

GLOWE::Utf32String GLOWE::String::toUtf32() const
{
	return Utf<32>::fromUtf8(baseString);
}

GLOWE::Utf16String GLOWE::String::toUtf16() const
{
	return Utf<16>::fromUtf8(baseString);
}

GLOWE::WideString GLOWE::String::toWide() const
{
	return Utf<8>::toWide(baseString);
}

std::size_t GLOWE::String::getSize() const
{
	return baseString.size();
}

std::size_t GLOWE::String::getCapacity() const
{
	return baseString.capacity();
}

void GLOWE::String::clear()
{
	baseString.clear();
}

GLOWE::String & GLOWE::String::append(const String & arg)
{
	baseString.append(arg.baseString);
	return *this;
}

GLOWE::String & GLOWE::String::append(const std::size_t howMany, const BasicChar ch)
{
	baseString.append(howMany, ch);
	return *this;
}

GLOWE::String & GLOWE::String::append(const char * buff, const std::size_t howMany)
{
	baseString.append(buff, buff + howMany);
	return *this;
}

GLOWE::String GLOWE::String::getSubstring(const std::size_t pos, const std::size_t length) const
{
	return baseString.substr(pos, length);
}

GLOWE::String & GLOWE::String::erase(const std::size_t pos, const std::size_t howMany)
{
	baseString.erase(pos, howMany);
	return (*this);
}

GLOWE::String & GLOWE::String::insert(const std::size_t pos, const String & str)
{
	baseString.insert(pos, str.getString());
	return *this;
}

GLOWE::String & GLOWE::String::insert(const std::size_t pos, const char * str)
{
	baseString.insert(pos, str);
	return *this;
}

GLOWE::String & GLOWE::String::insert(const std::size_t pos, const char* str, const std::size_t howMany)
{
	baseString.insert(pos, str, howMany);
	return *this;
}

GLOWE::String & GLOWE::String::insert(const std::size_t pos, const char ch)
{
	baseString.insert(pos, &ch, 1);
	return *this;
}

GLOWE::String& GLOWE::String::insert(const ConstIterator& it, const String& str)
{
	baseString.insert(it, str.begin(), str.end());
	return *this;
}

GLOWE::String& GLOWE::String::insert(const ConstIterator& it, const char* str)
{
	baseString.insert(it, str, str + std::strlen(str));
	return *this;
}

GLOWE::String& GLOWE::String::insert(const ConstIterator& it, const char* str, const std::size_t howMany)
{
	baseString.insert(it, str, str + howMany);
	return *this;
}

GLOWE::String& GLOWE::String::insert(const ConstIterator& it, const char ch)
{
	baseString.insert(it, ch);
	return *this;
}

GLOWE::String::Utf8Iterator GLOWE::String::utf8Begin() const
{
	return Utf8Iterator(getCharArray(), getBytesCountOfCharacter(*getCharArray()));
}

GLOWE::String::Utf8Iterator GLOWE::String::utf8End() const
{
	return Utf8Iterator(getCharArray() + getSize(), 0);
}

GLOWE::String::Iterator GLOWE::String::begin()
{
	return baseString.begin();
}

GLOWE::String::ConstIterator GLOWE::String::begin() const
{
	return baseString.begin();
}

GLOWE::String::ReverseIterator GLOWE::String::rbegin()
{
	return baseString.rbegin();
}

GLOWE::String::ConstReverseIterator GLOWE::String::rbegin() const
{
	return baseString.rbegin();
}

GLOWE::String::Iterator GLOWE::String::end()
{
	return baseString.end();
}

GLOWE::String::ConstIterator GLOWE::String::end() const
{
	return baseString.end();
}

GLOWE::String::ReverseIterator GLOWE::String::rend()
{
	return baseString.rend();
}

GLOWE::String::ConstReverseIterator GLOWE::String::rend() const
{
	return baseString.rend();
}

GLOWE::String::operator std::string() const
{
	return std::string(baseString.c_str());
}

GLOWE::String::operator std::wstring() const
{
	WideString temp;
	Utf<8>::toWide(baseString, temp);
	return std::wstring(temp.c_str());
}

GLOWE::String::operator const BasicString&() const
{
	return baseString;
}

GLOWE::String::operator GLOWE::StringView() const
{
	return { baseString.c_str(), baseString.size() };
}

GLOWE::String GLOWE::String::operator+(const String & right) const
{
	return String(baseString + right.baseString);
}

GLOWE::String GLOWE::String::operator+(const BasicChar * right) const
{
	return String(baseString + right);
}

GLOWE::String GLOWE::String::operator+(const BasicChar right) const
{
	return String(baseString + right);
}

GLOWE::String GLOWE::String::operator+(const Utf16Char right) const
{
	return *this + String(right);
}

GLOWE::String GLOWE::String::operator+(const Utf32Char right) const
{
	return *this + String(right);
}

GLOWE::String & GLOWE::String::operator+=(const String & right)
{
	baseString += right.baseString;
	return *this;
}

GLOWE::String & GLOWE::String::operator+=(const BasicChar * right)
{
	baseString += right;
	return *this;
}

GLOWE::String & GLOWE::String::operator+=(const BasicChar right)
{
	baseString += right;
	return *this;
}

GLOWE::String& GLOWE::String::operator+=(const Utf16Char right)
{
	return *this += String(right);
}

GLOWE::String& GLOWE::String::operator+=(const Utf32Char right)
{
	return *this += String(right);
}

GLOWE::String& GLOWE::String::operator=(const String& arg)
{
	baseString = arg.baseString;
	return *this;
}

GLOWE::String& GLOWE::String::operator=(String&& arg) noexcept
{
	baseString = std::move(arg.baseString);
	return *this;
}

GLOWE::String & GLOWE::String::operator=(const BasicChar * right)
{
	baseString = right;
	return *this;
}

GLOWE::String & GLOWE::String::operator=(const WideChar * right)
{
	Utf<8>::fromWide(right, baseString);
	return *this;
}

bool GLOWE::String::operator==(const String & right) const
{
	return baseString==right.baseString;
}

bool GLOWE::String::operator==(const BasicChar * right) const
{
	return baseString==right;
}

bool GLOWE::String::operator!=(const String & right) const
{
	return !(*this == right);
}

bool GLOWE::String::operator!=(const BasicChar * right) const
{
	return !(*this == right);
}

bool GLOWE::String::operator<(const String & right) const
{
	return baseString < right.baseString;
}

bool GLOWE::String::operator>(const String & right) const
{
	return baseString > right.baseString;
}

bool GLOWE::String::isEmpty() const
{
	return baseString.empty();
}

int GLOWE::String::toInt(const int base) const
{
	return std::strtol(baseString.c_str(), nullptr, base);
}

long GLOWE::String::toLong(const int base) const
{
	return std::strtol(baseString.c_str(), nullptr, base);
}

long long GLOWE::String::toLongLong(const int base) const
{
	return std::strtoll(baseString.c_str(), nullptr, base);
}

unsigned int GLOWE::String::toUInt(const int base) const
{
	return std::strtoul(baseString.c_str(), nullptr, base);
}

unsigned long GLOWE::String::toULong(const int base) const
{
	return std::strtoul(baseString.c_str(), nullptr, base);
}

unsigned long long GLOWE::String::toULongLong(const int base) const
{
	return std::strtoull(baseString.c_str(), nullptr, base);
}

float GLOWE::String::toFloat() const
{
	return std::strtof(baseString.c_str(), nullptr);
}

double GLOWE::String::toDouble() const
{
	return std::strtod(baseString.c_str(), nullptr);
}

long double GLOWE::String::toLongDouble() const
{
	return std::strtold(baseString.c_str(), nullptr);
}

bool GLOWE::String::toBool() const
{
	return static_cast<bool>(this->toDouble()) || baseString == u8"true";
}

GLOWE::String GLOWE::toString(const int arg)
{
	std::size_t tempSize = 0;
	tempSize = std::snprintf(nullptr, 0, u8"%d", arg);
	String temp(tempSize);
	std::snprintf(temp.getCharArray(), tempSize + 1, u8"%d", arg);
	return temp;
}

GLOWE::String GLOWE::toString(const long arg)
{
	std::size_t tempSize = 0;
	tempSize = std::snprintf(nullptr, 0, u8"%ld", arg);
	String temp(tempSize);
	std::snprintf(temp.getCharArray(), tempSize + 1, u8"%ld", arg);
	return temp;
}

GLOWE::String GLOWE::toString(const long long arg)
{
	std::size_t tempSize = 0;
	tempSize = std::snprintf(nullptr, 0, u8"%lld", arg);
	String temp(tempSize);
	std::snprintf(temp.getCharArray(), tempSize + 1, u8"%lld", arg);
	return temp;
}

GLOWE::String GLOWE::toString(const unsigned int arg)
{
	std::size_t tempSize = 0;
	tempSize = std::snprintf(nullptr, 0, u8"%u", arg);
	String temp(tempSize);
	std::snprintf(temp.getCharArray(), tempSize + 1, u8"%u", arg);
	return temp;
}

GLOWE::String GLOWE::toString(const unsigned long arg)
{
	std::size_t tempSize = 0;
	tempSize = std::snprintf(nullptr, 0, u8"%lu", arg);
	String temp(tempSize);
	std::snprintf(temp.getCharArray(), tempSize + 1, u8"%lu", arg);
	return temp;
}

GLOWE::String GLOWE::toString(const unsigned long long arg)
{
	std::size_t tempSize = 0;
	tempSize = std::snprintf(nullptr, 0, u8"%llu", arg);
	String temp(tempSize);
	std::snprintf(temp.getCharArray(), tempSize + 1, u8"%llu", arg);
	return temp;
}

GLOWE::String GLOWE::toString(const float arg)
{
	std::size_t tempSize = 0;
	tempSize = std::snprintf(nullptr, 0, u8"%f", arg);
	String temp(tempSize);
	std::snprintf(temp.getCharArray(), tempSize + 1, u8"%f", arg);
	return temp;
}

GLOWE::String GLOWE::toString(const double arg)
{
	std::size_t tempSize = 0;
	tempSize = std::snprintf(nullptr, 0, u8"%f", arg);
	String temp(tempSize);
	std::snprintf(temp.getCharArray(), tempSize + 1, u8"%f", arg);
	return temp;
}

GLOWE::String GLOWE::toString(const long double arg)
{
	std::size_t tempSize = 0;
	tempSize = std::snprintf(nullptr, 0, u8"%Lf", arg);
	String temp(tempSize);
	std::snprintf(temp.getCharArray(), tempSize + 1, u8"%Lf", arg);
	return temp;
}

GLOWE::String GLOWE::toString(const bool arg)
{
	if (arg)
	{
		return String(u8"true");
	}

	return String(u8"false");
}

GLOWE::String GLOWE::toString(const void* const arg)
{
	constexpr const char* printfFlags = u8"%tx";
	std::size_t tempSize = 0;
	tempSize = std::snprintf(nullptr, 0, printfFlags, reinterpret_cast<std::make_unsigned<std::ptrdiff_t>::type>(arg));
	String temp(tempSize);
	std::snprintf(temp.getCharArray(), tempSize + 1, printfFlags, reinterpret_cast<std::make_unsigned<std::ptrdiff_t>::type>(arg));
	return temp;
}

std::ostream & GLOWE::operator<<(std::ostream & str, const String & right)
{
	return str << right.getCharArray();
}

std::istream & GLOWE::operator >> (std::istream & str, String & right)
{
	return str >> right.getString();
}

GLOWE::String GLOWE::operator+(const String::BasicChar * left, const String & right)
{
	return left + right.getString();
}

GLOWE::String GLOWE::operator+(const String::WideChar * left, const String & right)
{
	return std::move(String(left) + String(right));
}

GLOWE::String GLOWE::operator""_str(const char * str, const std::size_t size)
{
	return String(str, size);
}

GLOWE::String GLOWE::operator""_str(const wchar_t * str, const std::size_t)
{
	return String(str);
}

int GLOWE::StringView::toInt(const int base) const
{
	int result{};
	std::from_chars(data(), data() + size(), result, base);
	return result;
}

unsigned int GLOWE::StringView::toUInt(const int base) const
{
	unsigned int result{};
	std::from_chars(data(), data() + size(), result, base);
	return result;
}

float GLOWE::StringView::toFloat() const
{
	float result{};
	std::from_chars(data(), data() + size(), result);
	return result;
}

GLOWE::String::Utf8Iterator GLOWE::String::Utf8Iterator::operator++()
{
	arr += bytes;
	bytes = getBytesCountOfCharacter(*arr);

	return *this;
}

GLOWE::String::Utf8Iterator GLOWE::String::Utf8Iterator::operator++(int)
{
	Utf8Iterator temp = *this;
	operator++();
	return temp;
}

GLOWE::String::Utf8Iterator GLOWE::String::Utf8Iterator::operator--()
{
	auto& it = --arr;
	auto temp = *it;

	while ((temp & 128U) && !(temp & 64U))
	{
		--it;
		temp = *it;
	}

	bytes = getBytesCountOfCharacter(temp);

	return *this;
}

GLOWE::String::Utf8Iterator GLOWE::String::Utf8Iterator::operator--(int)
{
	Utf8Iterator temp = *this;
	operator--();
	return temp;
}

const char* GLOWE::String::Utf8Iterator::operator*() const noexcept
{
	return arr;
}

bool GLOWE::String::Utf8Iterator::operator==(const Utf8Iterator& arg) const
{
	return arr == arg.arr;
}

bool GLOWE::String::Utf8Iterator::operator==(const char* arg) const
{
	return std::strlen(arg) == bytes && (std::strncmp(arr, arg, bytes) == 0); // TODO: Pojebane, ka�de strncmp pojebuje to, mimo �e bytes to 2 to mimo zgodno�ci dw�ch pierwszych bajt�w zwraca pojebane false.
}

bool GLOWE::String::Utf8Iterator::operator==(const String& arg) const
{
	return arg.getSize() == bytes && (std::strncmp(arr, arg.getCharArray(), bytes) == 0);
}

bool GLOWE::String::Utf8Iterator::operator!=(const Utf8Iterator& arg) const
{
	return arr != arg.arr;
}

bool GLOWE::String::Utf8Iterator::operator!=(const char* arg) const
{
	return std::strlen(arg) != bytes || (std::strncmp(arr, arg, bytes) != 0);
}

bool GLOWE::String::Utf8Iterator::operator!=(const String& arg) const
{
	return arg.getSize() != bytes || !(std::strncmp(arr, arg.getCharArray(), bytes) != 0);
}

unsigned int GLOWE::String::Utf8Iterator::getBytesCount() const
{
	return bytes;
}

unsigned int shiftUnusedBytes(unsigned char arg)
{
	if (arg & 128U)
	{
		unsigned int toUnshift = 0;

		do
		{
			++toUnshift;
			arg <<= 1;
		} while (arg & 128U);

		arg <<= 1;
		++toUnshift;
		arg >>= toUnshift;
	}

	return arg;
}

unsigned int getUsedBitsCount(unsigned char arg)
{
	unsigned int result = 8;
	if (arg & 128U)
	{
		do
		{
			--result;
			arg <<= 1;
		} while (arg & 128U);

		--result;
	}

	return result;
}

GLOWE::String::Utf32Char GLOWE::String::Utf8Iterator::asUtf32Char() const
{
	Utf32Char result = 0;

	for (unsigned int x = 0; x < bytes; ++x)
	{
		result <<= getUsedBitsCount(arr[x]);
		result |= shiftUnusedBytes(arr[x]);
	}

	return result;
}
