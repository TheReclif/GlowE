#pragma once
#ifndef GLOWE_MEMORY_MGR_INCLUDED
#define GLOWE_MEMORY_MGR_INCLUDED

#include "Utility.h"
#include "SubsystemsCore.h"
#include "ThreadClass.h"

//#define GLOWE_ENABLE_LEAK_DETECTION
//#define GLOWE_ENABLE_CORRUPTION_HELPER
//#define GLOWE_ENABLE_MEMORY_SIZE_USAGES_COUNTER
//#define GLOWE_DISABLE_CUSTOM_STL_CONTAINERS

namespace GLOWE
{
	constexpr auto KiloByte = 1024;
	constexpr auto MegaByte = 1024 * KiloByte;
	constexpr auto GigaByte = 1024 * MegaByte;
	constexpr auto DefaultBlockSize = MegaByte;
	constexpr auto DefaultPoolBlockSize = 96;
	constexpr auto DefaultPoolSize = 6000;
	constexpr auto DefaultVectorPoolBlockSize = sizeof(__m128);
	constexpr auto DefaultVectorPoolSize = 2 * KiloByte;
	constexpr auto MostRestrictiveAlignment = alignof(std::max_align_t) >= 4 ? alignof(std::max_align_t) : 4;
	constexpr auto DefaultPoolAlignment = std::max(DefaultVectorPoolBlockSize, MostRestrictiveAlignment);
	/// @brief After how many ticks the memory manager will clean trash.
	constexpr unsigned int whenToCleanMemoryTrash = 30;
	/// @brief After how many deallocations the memory allocator should try to free unused RB allocators.
	constexpr unsigned int whenToFreeUnusedAllocators = 5000;

	class GLOWSYSTEM_EXPORT MemoryLeakDetector
		: public Subsystem
	{
	private:
		mutable Mutex mutex;
		std::unordered_map<const void*, std::type_index> current, previous;
	public:
		void addElement(const void* const ptr, const std::type_index& type, const unsigned int reqSize);
		void removeElement(const void* const ptr);
		void checkIfDeletable(const void* const ptr) const;

		std::map<const void*, std::type_index> getPersistant();
	};

	class MemoryProfiler;

	class GLOWSYSTEM_EXPORT RBTMemoryAllocator
	{
	public:
		using SizeType = std::size_t;
	private:
		template<class T>
		using Function = std::function<T>;

		struct FreeHeader;
		struct ClaimedHeader;

		struct alignas(MostRestrictiveAlignment) ClaimedHeader
		{
			ClaimedHeader* prev, *next;
		};
		struct alignas(MostRestrictiveAlignment) FreeHeader
			: public ClaimedHeader
		{
			FreeHeader* left = nullptr, *right = nullptr, *parent = nullptr; // RB data. Red/black bit is stored in the least significant bit of parent.
			// These addresses should be perfectly aligned to MostRestrictiveAlignment, so at least 2 least significant bits can be used to store data. In our case: most right tells us if the block is free or claimed and the one on the left is a red black bit.
		};
		struct FittingBlockData
		{
			ClaimedHeader* usableMemory = nullptr;
			SizeType remainingMemory = 0;
			SizeType adjustment = 0;
			SizeType usedMemory = 0;
		};

		static_assert(alignof(FreeHeader) == MostRestrictiveAlignment, "Alignments must match.");
		static_assert(alignof(ClaimedHeader) == MostRestrictiveAlignment, "Alignments must match.");
		static_assert(sizeof(ClaimedHeader) < sizeof(FreeHeader), "It should not happen.");
	private:
		void* memory;
		FreeHeader* freeMemory;
		ClaimedHeader* trueMemoryBegin, *endOfMemory;
		SizeType totalMemory, usedMemory;
		unsigned int allocations;
		bool isOwningMemory;
	private:
		// Red-black tree methods.
		// true for black, false for red.
		static constexpr inline ClaimedHeader* cleanAddress(const ClaimedHeader* ptr)
		{
			// We must clean two least significant bits.
			return (ClaimedHeader*)(((uintptr_t)ptr) & (~3));
		}
		static constexpr inline SizeType calcSize(const ClaimedHeader* ptr)
		{
			return ((uintptr_t)cleanAddress(ptr->next)) - ((uintptr_t)ptr);
		}
		static constexpr inline bool checkRedness(FreeHeader* const ptr)
		{
			return ((uintptr_t)ptr) & 2;
		}
		static constexpr inline bool isFreeHeader(const ClaimedHeader* ptr)
		{
			return ((uintptr_t)ptr) & 1;
		}
		static constexpr inline FreeHeader* setRedness(FreeHeader* const ptr, const bool red)
		{
			return (FreeHeader*)((((uintptr_t)ptr) & ~2) | (((uintptr_t)red) << 1));
		}
		static constexpr inline ClaimedHeader* setIsFreeHeader(const ClaimedHeader* ptr, const bool isFreeHeader)
		{
			return (ClaimedHeader*)((((uintptr_t)ptr) & ~1) | ((uintptr_t)isFreeHeader));
		}
		static constexpr inline bool getRed(FreeHeader* const ptr)
		{
			return ptr && checkRedness(ptr->parent);
		}

		void removeFromRBTree(FreeHeader* block);
		void insertToRBTree(FreeHeader* block); // It also encodes parent to store the red-black bit.
		bool isBlockFitting(FreeHeader * block, const SizeType reqSize, const SizeType alignment, FittingBlockData& outputData) const;
		FreeHeader* findFittingBlock(const SizeType size, const SizeType alignment, FittingBlockData& outputFittingBlock) const;
		unsigned int dbgCheckFreeTreeSanity() const;
		// Error list:
		// 1: left child looped.
		// 2: right child looped.
		// 3: left child's parent is invalid.
		// 4: right child's parent is invalid.
		// 5: two consecutive reds.
		// 6: black node height is not the same in the whole tree.
		// 7: root is red.
		// 8: root's parent is not null.

		unsigned int dbgGetBlackHeight(FreeHeader* const head) const;
	public:
		explicit RBTMemoryAllocator(const SizeType memorySize = DefaultBlockSize);
		RBTMemoryAllocator(void* memoryToUse, const SizeType memorySize, const bool isOwning = false); // Ignore the third parameter.
		~RBTMemoryAllocator();

		void* allocate(const SizeType howMany, const SizeType alignment = MostRestrictiveAlignment);
		void deallocate(void* ptr);

		SizeType getUsedMemory() const;
		SizeType getTotalMemory() const;

		unsigned int getAllocationsCount() const;

		/// @brief Tells whether a given pointer is contained in the allocator's memory range.
		/// @param ptr Pointer to check
		/// @return true if the pointer is in the memory range, false otherwise
		bool isPointerInMemoryRange(const void* ptr) const;

		/// @brief Calculates the tree's nodes count.
		/// @return Nodes count
		unsigned int dbgCalcTreeNodesCount() const;
		/// @brief Performs various checks to ensure that the memory and its organization tree is consistent and error free. Will throw std::runtime_error if the error exists.
		/// @warning Empty if _DEBUG is not defined.
		void dbgCheckSanity() const;
		/// @brief Writes all the blocks (information about them) to std::cout.
		void dbgWriteAllBlocks() const;
		/// @brief Checks if the blocks' linked list is integral. Complexivity: 0(n).
		/// @return true if the list is OK, false otherwise
		bool dbgCheckListIntegrity() const;
	};

	class _GlobalAllocate;

	class GLOWSYSTEM_EXPORT PoolAllocator
	{
	private:
		void* mem;
		void** freeList;
		size_t size;
		unsigned int allocations;
		_GlobalAllocate* memoryOwner;
		void* endOfMem;
	public:
		PoolAllocator() = delete;
		PoolAllocator(_GlobalAllocate* creator);
		~PoolAllocator();

		// Returns pointer to newly allocated DefaultPoolBlockSize size memory.
		void* allocate();
		void deallocate(void* memPtr);

		bool isPointerInMemoryRange(const void* arg) const;

		friend class _GlobalAllocate;
	};

	class GLOWSYSTEM_EXPORT VectorAllocator
	{
	private:
		void** freeList;
		void* mem;
	public:
		VectorAllocator();
		~VectorAllocator();

		//Returns pointer to newly allocated DefaultVectorPoolBlockSize size memory
		void* allocate();
		void deallocate(void* memPtr);
	};

	class String;
	class GlobalAllocatorMgr;

	class _GlobalAllocate
		: private NoCopy
	{
	private:
		unsigned long long newBlockSize;
		unsigned int allocations;
		// These MUST use standard allocator.
		std::deque<RBTMemoryAllocator*> allocators;
		std::deque<PoolAllocator*> poolAllocators;

		GLOWSYSTEM_EXPORT void createNewAllocator();
		GLOWSYSTEM_EXPORT void createNewPool();

		GLOWSYSTEM_EXPORT void _deallocate(void* memHandle);
		GLOWSYSTEM_EXPORT void _deallocateOnlyRBTAllocators(void* memHandle);
	public:
		GLOWSYSTEM_EXPORT _GlobalAllocate();
		GLOWSYSTEM_EXPORT _GlobalAllocate(_GlobalAllocate&& arg) noexcept;
		
		GLOWSYSTEM_EXPORT virtual ~_GlobalAllocate();

		template<class T, class... CtorArgs>
		T* allocate(CtorArgs&& ...args);
		template<class T, int adjustment, class... CtorArgs>
		T* allocateWithAlignment(CtorArgs&& ...args);

		GLOWSYSTEM_EXPORT void* allocateSpace(const size_t size);
		GLOWSYSTEM_EXPORT void* allocateSpaceWithAlignment(const size_t size, const size_t adjustment);

		template<class T>
		bool deallocate(T* ptr); // Returns true if the deallocation happened, false otherwise.

		GLOWSYSTEM_EXPORT void increaseAllocatorsCount(const unsigned int newCount);
		/// @brief Frees allocators whose allocations' count reached 0. Should be called only by the owning thread.
		GLOWSYSTEM_EXPORT void freeUnusedAllocators();

		GLOWSYSTEM_EXPORT bool isPointerInMemoryRange(const void* ptr) const;

		// Does not count pool allocators' allocations.
		GLOWSYSTEM_EXPORT unsigned int getAllocationsCount() const;

		GLOWSYSTEM_EXPORT bool checkSanity() const;

		friend class MemoryProfiler;
		friend class PoolAllocator;
	};

	template<class T, class ...CtorArgs>
	inline T * _GlobalAllocate::allocate(CtorArgs&& ...args)
	{
		T* temp;

		for (auto& x : allocators)
		{
			temp = (T*)(x->allocate(sizeof(T)));
			if (temp != nullptr)
			{
				temp = new (temp) T(std::forward<CtorArgs>(args)...);
#ifdef GLOWE_ENABLE_LEAK_DETECTION
				getInstance<MemoryLeakDetector>().addElement(temp, typeid(T), sizeof(T));
#endif
				++allocations;
				return temp;
			}
		}

		createNewAllocator();

		return this->allocate<T, CtorArgs...>(std::forward<CtorArgs>(args)...);
	}

	template<class T, int adjustment, class ...CtorArgs>
	inline T * _GlobalAllocate::allocateWithAlignment(CtorArgs&& ...args)
	{
		T* temp;

		for (auto& x : allocators)
		{
			temp = (T*)(x->allocate(sizeof(T), adjustment));
			if (temp != nullptr)
			{
				temp = new (temp) T(std::forward<CtorArgs>(args)...);
#ifdef GLOWE_ENABLE_LEAK_DETECTION
				getInstance<MemoryLeakDetector>().addElement(temp, typeid(T), sizeof(T));
#endif
				++allocations;
				return temp;
			}
		}

		createNewAllocator();

		return this->allocateWithAlignment<T, adjustment, CtorArgs...>(std::forward<CtorArgs>(args)...);
	}

	template<>
	inline bool _GlobalAllocate::deallocate(void* ptr)
	{
		if (isPointerInMemoryRange(ptr))
		{
#ifdef GLOWE_ENABLE_LEAK_DETECTION
			getInstance<MemoryLeakDetector>().removeElement(ptr);
#endif
			this->_deallocate(static_cast<void*>(ptr));
			return true;
		}

		return false;
	}

	template<class T>
	inline bool _GlobalAllocate::deallocate(T * ptr)
	{
		if (isPointerInMemoryRange(ptr))
		{
			ptr->~T();
#ifdef GLOWE_ENABLE_LEAK_DETECTION
			getInstance<MemoryLeakDetector>().removeElement(ptr);
#endif
			this->_deallocate(static_cast<void*>(ptr));

			return true;
		}

		return false;
	}

	class GlobalAllocator
		: private NoCopy
	{
	private:
		Mutex pointersToDeallocMutex;
		std::vector<void*> pointersToDeallocate;
		std::thread::id creatorsThreadID;
		_GlobalAllocate allocator;
		GlobalAllocatorMgr* overseer;
		unsigned int blockRemovalTick;

		template<class T>
		bool _deallocate(T* ptr);
		
		GLOWSYSTEM_EXPORT void collectTrash();
		GLOWSYSTEM_EXPORT void collectTrashUnconditionally();
	public:
		GLOWSYSTEM_EXPORT GlobalAllocator();
		GLOWSYSTEM_EXPORT GlobalAllocator(GlobalAllocator&& arg) noexcept;
		GLOWSYSTEM_EXPORT ~GlobalAllocator() = default;

		template<class T, class... CtorArgs>
		T* allocate(CtorArgs&& ...args);
		template<class T, int adjustment, class... CtorArgs>
		T* allocateWithAlignment(CtorArgs&& ...args);

		GLOWSYSTEM_EXPORT void* allocateSpace(const size_t size);
		GLOWSYSTEM_EXPORT void* allocateSpaceWithAlignment(const size_t size, const size_t adjustment);

		template<class T>
		void deallocate(T* ptr); // Returns true if the deallocation happened, false otherwise.

		GLOWSYSTEM_EXPORT void addPointerToDeallocate(void* arg);

		GLOWSYSTEM_EXPORT bool isPointerInMemoryRange(const void* arg) const;

		GLOWSYSTEM_EXPORT void markAsFreed();

		GLOWSYSTEM_EXPORT bool checkSanity() const;

		GLOWSYSTEM_EXPORT unsigned int getAllocationsCount() const;

		friend class MemoryProfiler;
		friend class GlobalAllocatorMgr;
	};

	class GlobalAllocatorMgr
		: public Subsystem
	{
	private:
		std::map<std::thread::id, GlobalAllocator> occupiedAllocators;
		std::list<GlobalAllocator> freedAllocators;

		std::thread::id mainThreadId; // If the allocator corresponding to this thread id is marked as freed then we are free to asume that the program has come to an end.
		bool isDuringCleanup = false;

		mutable ReadWriteMutex<RecursiveMutex> mutex;
	public:
		GLOWSYSTEM_EXPORT GlobalAllocatorMgr() = default;
		virtual ~GlobalAllocatorMgr() = default;

		GLOWSYSTEM_EXPORT GlobalAllocator& getAllocator(const std::thread::id& threadID);

		GLOWSYSTEM_EXPORT void markAllocatorAsFreed(const std::thread::id& alloc);

		GLOWSYSTEM_EXPORT unsigned int getUsedAllocatorsCount() const;
		GLOWSYSTEM_EXPORT unsigned int getFreedAllocatorsCount() const;

		GLOWSYSTEM_EXPORT virtual int getUnloadOrder() const override;

		template<class T>
		void deallocate(T* ptr)
		{
			ReadWriteLock<RecursiveMutex> lock(mutex);
			lock.lockRead();
			for (auto& x : occupiedAllocators)
			{
				if (x.second.isPointerInMemoryRange(ptr))
				{
					ptr->~T();
					x.second.addPointerToDeallocate(ptr);
					return;
				}

				/*
				if (x.second._deallocate(ptr))
				{
					return;
				}
				*/
			}

			for (std::list<GlobalAllocator>::iterator it = freedAllocators.begin(), end = freedAllocators.end(); it != end; ++it)
			{
				if (it->_deallocate(ptr))
				{
					if (it->getAllocationsCount() == 0)
					{
						lock.unlock();
						lock.lockWrite();
						freedAllocators.erase(it);
					}
					return;
				}
			}
		}

		GLOWSYSTEM_EXPORT bool checkSanity() const;
		GLOWSYSTEM_EXPORT bool checkIsDuringCleanup() const; // Checks if the isDuringCleanup flag is set.

		friend class GlobalAllocator;
		friend class MemoryProfiler;
	};

	template<>
	GLOWSYSTEM_EXPORT void GlobalAllocatorMgr::deallocate(void* ptr);

	template<class T>
	inline bool GlobalAllocator::_deallocate(T * ptr)
	{
		return allocator.deallocate(ptr);
	}

	template<class T, class ...CtorArgs>
	inline T * GlobalAllocator::allocate(CtorArgs && ...args)
	{
		collectTrash();
#ifdef GLOWE_ENABLE_MEMORY_SIZE_USAGES_COUNTER
		getInstance<MemoryProfiler>().notifyUsage(sizeof(T));
#endif
		return allocator.allocate<T, CtorArgs...>(std::forward<CtorArgs>(args)...);
	}

	template<class T, int adjustment, class ...CtorArgs>
	inline T * GlobalAllocator::allocateWithAlignment(CtorArgs && ...args)
	{
		collectTrash();
#ifdef GLOWE_ENABLE_MEMORY_SIZE_USAGES_COUNTER
		getInstance<MemoryProfiler>().notifyUsage(sizeof(T));
#endif
		return allocator.allocate<T, adjustment, CtorArgs...>(std::forward<CtorArgs>(args)...);
	}

	template<class T>
	inline void GlobalAllocator::deallocate(T * ptr)
	{
		if (!_deallocate(ptr))
		{
			overseer->deallocate(ptr);
		}
		else
		{
			collectTrash();
			++blockRemovalTick;
			if (blockRemovalTick >= whenToFreeUnusedAllocators)
			{
				blockRemovalTick = 0;
				allocator.freeUnusedAllocators();
			}
		}
	}

	class GLOWSYSTEM_EXPORT FrameAllocator
	{
	public:
		static constexpr UInt64 defaultBlockSize = 32 * KiloByte;
	private:
		void* const memory, * const memEnd;
		Atomic<void*> currMemPos;
		const bool isOwningMemory, shouldBeClearedAtFrameEnd;
	public:
		FrameAllocator(const bool clearAtFrameEnd = true);
		FrameAllocator(const UInt64 size, const bool clearAtFrameEnd = true);
		FrameAllocator(void* const newMemory, const UInt64 size, const bool clearAtFrameEnd = true);
		~FrameAllocator();

		void* allocate(const UInt64 size); // May return nullptr.

		void clear(); // Must be called to flush memory.

		UInt64 getCapacity() const;
	};

#pragma region StdAllocator

	template<class T>
	class StdAllocator
	{
	public:
		template<class U>
		struct rebind
		{
			using other = GLOWE::template StdAllocator<U>;
		};

		using value_type = T;

		using pointer = T*;
		using const_pointer = const T*;

		using reference = T&;
		using const_reference = const T&;

		using size_type = std::size_t;
		using difference_type = std::ptrdiff_t;

		using propagate_on_container_move_assignment = std::true_type;
		using is_always_equal = std::true_type;
	public:
		StdAllocator() = default;
		StdAllocator(const StdAllocator& arg) = default;
		~StdAllocator() = default;

		template<class U>
		StdAllocator(const StdAllocator<U>&) {};

		T* allocate(size_type n);
		void deallocate(pointer p, size_type n);

		bool operator==(const StdAllocator& arg) const;
		bool operator!=(const StdAllocator&) const;
	};

	template<class T>
	inline T* StdAllocator<T>::allocate(size_type n)
	{
		return (pointer)getInstance<GlobalAllocator>().allocateSpaceWithAlignment(sizeof(value_type) * n, alignof(T));
	}
	template<class T>
	inline void StdAllocator<T>::deallocate(pointer p, size_type)
	{
		getInstance<GlobalAllocator>().deallocate(static_cast<void*>(p));
	}

	template<class T>
	inline bool StdAllocator<T>::operator==(const StdAllocator &) const
	{
		return true;
	}

	template<class T>
	inline bool StdAllocator<T>::operator!=(const StdAllocator &) const
	{
		return false;
	}

	class GLOWSYSTEM_EXPORT UniqueDeleter
	{
	public:
		template<class T>
		inline void operator()(T* ptr) const
		{
			getInstance<GlobalAllocator>().deallocate(ptr);
		}
	};
	template<class T>
	class Deletable final
		: public T
	{
	public:
		static_assert(std::has_virtual_destructor<T>::value, "T must have a virtual destructor. If you are unable to or don't want to add a virtual destructor to T, use TrivialUniquePtr and makeTrivialUniquePtr.");
		template <class... Args>
		inline Deletable(Args&&... args) : T(std::forward<Args>(args)...) {};
		virtual ~Deletable() = default;
		inline void* operator new (const size_t size) { return getInstance<GlobalAllocator>().allocateSpaceWithAlignment(size, alignof(T)); };
		inline void operator delete (void* const p) noexcept { getInstance<GlobalAllocator>().deallocate(p); };
	};

	template<class T>
	using SharedPtr = std::shared_ptr<T>;
	template<class T, class Deleter = std::default_delete<T>>
	using UniquePtr = std::unique_ptr<T, Deleter>;
	template<class T>
	using TrivialUniquePtr = UniquePtr<T, UniqueDeleter>;
	template<class T>
	using WeakPtr = std::weak_ptr<T>;

	template<class T, class Deleter>
	inline SharedPtr<T> uniqueToShared(UniquePtr<T, Deleter>&& unique)
	{
		return SharedPtr<T>(std::move(unique));
	}

	template<class T1, class T2>
	using Pair = std::pair<T1, T2>;

	template<class T, class... Args>
	inline SharedPtr<T> makeSharedPtr(Args&&... args)
	{
		return std::allocate_shared<T>(StdAllocator<T>(), std::forward<Args>(args)...);
	}

	template<class T, class... Args>
	inline UniquePtr<T> makeUniquePtr(Args&&... args)
	{
		return UniquePtr<T>(new Deletable<T>(std::forward<Args>(args)...));
	}
	template<class T, class... Args>
	inline TrivialUniquePtr<T> makeTrivialUniquePtr(Args&&... args)
	{
		return TrivialUniquePtr<T>(getInstance<GlobalAllocator>().allocate<T>(std::forward<Args>(args)...));
	}

	template<class T, class Func>
	inline std::function<void(T*)> makeObserverDeleter(Func&& func)
	{
		return [func](T * arg)
		{
			func(*arg);
			getInstance<GlobalAllocator>().deallocate<T>(arg);
		};
	}

	template<class T>
	using UniqueObserverPtr = std::unique_ptr<T, std::function<void(T*)>>;

	template<class T, class Func, class... Args>
	inline UniqueObserverPtr<T> makeUniqueObserverPtr(Func&& func, Args&& ... args)
	{
		return UniqueObserverPtr<T>(getInstance<GlobalAllocator>().allocate<T>(std::forward<Args...>(args...)), makeObserverDeleter(std::forward<Func>(func)));
	}

	template<class T, class Func, class... Args>
	inline SharedPtr<T> makeSharedObserverPtr(Func&& func, Args&& ... args)
	{
		return SharedPtr<T>(getInstance<GlobalAllocator>().allocate<T>(std::forward<Args...>(args...)), makeObserverDeleter(std::forward<Func>(func)));
	}

#ifndef GLOWE_DISABLE_CUSTOM_STL_CONTAINERS
	template<class T>
	using Vector = std::vector<T, GLOWE::template StdAllocator<T>>;

	template<class T>
	using List = std::list<T, GLOWE::template StdAllocator<T>>;

	template<class T>
	using ForwardList = std::forward_list<T, GLOWE::template StdAllocator<T>>;

	template<class T>
	using Deque = std::deque<T, GLOWE::template StdAllocator<T>>;

	template<class Key, class Value, class Compare = std::template less<Key>>
	using Map = std::map<Key, Value, Compare, GLOWE::template StdAllocator<std::template pair<const Key, Value>>>;

	template<class Key, class Value, class Hasher = std::template hash<Key>, class KeyEq = std::template equal_to<Key>>
	using UnorderedMap = std::unordered_map<Key, Value, Hasher, KeyEq, GLOWE::template StdAllocator<std::template pair<const Key, Value>>>;

	template<class Key, class Value, class Compare = std::template less<Key>>
	using Multimap = std::multimap<Key, Value, Compare, GLOWE::template StdAllocator<std::template pair<const Key, Value>>>;

	template<class Key, class Value, class Hasher = std::template hash<Key>, class KeyEq = std::template equal_to<Key>>
	using UnorderedMultimap = std::unordered_multimap<Key, Value, Hasher, KeyEq, GLOWE::template StdAllocator<std::template pair<const Key, Value>>>;

	template<class Value, class Compare = std::template less<Value>>
	using Set = std::set<Value, Compare, GLOWE::template StdAllocator<Value>>;

	template<class Value, class Hasher = std::template hash<Value>, class KeyEq = std::template equal_to<Value>>
	using UnorderedSet = std::unordered_set<Value, Hasher, KeyEq, GLOWE::template StdAllocator<Value>>;

	template<class Value, class Compare = std::less<Value>>
	using Multiset = std::multiset<Value, Compare, GLOWE::template StdAllocator<Value>>;

	template<class Value, class Hasher = std::template hash<Value>, class KeyEq = std::template equal_to<Value>>
	using UnorderedMultiset = std::unordered_multiset<Value, Hasher, KeyEq, GLOWE::template StdAllocator<Value>>;
#else
	template<class T>
	using Vector = std::vector<T>;

	template<class T>
	using List = std::list<T>;

	template<class T>
	using ForwardList = std::forward_list<T>;

	template<class T>
	using Deque = std::deque<T>;

	template<class Key, class Value, class Compare = std::template less<Key>>
	using Map = std::map<Key, Value, Compare>;

	template<class Key, class Value, class Hasher = std::template hash<Key>, class KeyEq = std::template equal_to<Key>>
	using UnorderedMap = std::unordered_map<Key, Value, Hasher, KeyEq>;

	template<class Key, class Value, class Compare = std::template less<Key>>
	using Multimap = std::multimap<Key, Value, Compare>;

	template<class Key, class Value, class Hasher = std::template hash<Key>, class KeyEq = std::template equal_to<Key>>
	using UnorderedMultimap = std::unordered_multimap<Key, Value, Hasher, KeyEq>;

	template<class Value, class Compare = std::template less<Value>>
	using Set = std::set<Value, Compare>;

	template<class Value, class Hasher = std::template hash<Value>, class KeyEq = std::template equal_to<Value>>
	using UnorderedSet = std::unordered_set<Value, Hasher, KeyEq>;

	template<class Value, class Compare = std::less<Value>>
	using Multiset = std::multiset<Value, Compare>;

	template<class Value, class Hasher = std::template hash<Value>, class KeyEq = std::template equal_to<Value>>
	using UnorderedMultiset = std::unordered_multiset<Value, Hasher, KeyEq>;
#endif
	template<class T, class Container = GLOWE::template Deque<T>>
	using Queue = std::queue<T, Container>;

	template<class T, class Container = Vector<T>, class Comp = std::less<T>>
	using PriorityQueue = std::priority_queue<T, Container, Comp>;

	template<class T, class Container = GLOWE::template Deque<T>>
	using Stack = std::stack<T, Container>;

#pragma endregion

	class GLOWSYSTEM_EXPORT ResizeableFrameAllocator
	{
	private:
		Mutex mutex;
		Atomic<unsigned int> size;
		List<FrameAllocator> allocs;
	private:
		void addNewAlloc(const UInt64 reqSize, unsigned int expectedSize);
	public:
		ResizeableFrameAllocator();
		ResizeableFrameAllocator(const UInt64 reqSize);
		~ResizeableFrameAllocator();

		void* allocate(const UInt64 reqSize); // Won't return nullptr.

		void clear(); // Must be called to flush memory. In addition, it will deallocate all blocks and create a new one, as big as their sizes sum.
	};

	class GLOWSYSTEM_EXPORT FrameAllocatorMgr
		: public Subsystem
	{
	private:
		Mutex mutex;
		Set<FrameAllocator*> frameAllocators;
		Set<ResizeableFrameAllocator*> resizeableFrameAllocators;
	private:
		void registerAllocator(FrameAllocator* allocator);
		void unregisterAllocator(FrameAllocator* allocator);

		void registerAllocator(ResizeableFrameAllocator* allocator);
		void unregisterAllocator(ResizeableFrameAllocator* allocator);
	public:
		void clearAllAllocators(); // Called at the end of frame.

		friend class FrameAllocator;
		friend class ResizeableFrameAllocator;
	};

	template<class T>
	class FrameResizableStdAllocator
	{
	public:
		ResizeableFrameAllocator* alloc;
	public:
		template<class U>
		struct rebind
		{
			using other = GLOWE::template FrameResizableStdAllocator<U>;
		};

		using value_type = T;

		using pointer = T*;
		using const_pointer = const T*;

		using reference = T&;
		using const_reference = const T&;

		using size_type = std::size_t;
		using difference_type = std::ptrdiff_t;

		using propagate_on_container_copy_assignment = std::true_type;
		using propagate_on_container_move_assignment = std::true_type;
		using propagate_on_container_swap = std::true_type;
		using is_always_equal = std::false_type;
	public:
		FrameResizableStdAllocator() = delete;
		FrameResizableStdAllocator(ResizeableFrameAllocator* const newAlloc) noexcept : alloc(newAlloc) {}
		FrameResizableStdAllocator(const FrameResizableStdAllocator& arg) noexcept : alloc(arg.alloc) {}
		FrameResizableStdAllocator(FrameResizableStdAllocator&& arg) noexcept : alloc(exchange(arg.alloc, nullptr)) {}
		~FrameResizableStdAllocator() noexcept = default;

		template<class U>
		FrameResizableStdAllocator(const FrameResizableStdAllocator<U>& arg) : alloc(arg.alloc) {}

		pointer allocate(size_type n)
		{
			return (pointer)alloc->allocate(sizeof(value_type) * n);
		}
		void deallocate(pointer ptr, size_type n)
		{
#ifdef GLOWE_ENABLE_CORRUPTION_HELPER
			std::memset(ptr, 0xFF, n * sizeof(value_type));
#endif
		}
	};

	template<class T, class U>
	bool operator==(const FrameResizableStdAllocator<T>& a, const FrameResizableStdAllocator<U>& b) { return a.alloc == b.alloc; }
	template<class T, class U>
	bool operator!=(const FrameResizableStdAllocator<T>& a, const FrameResizableStdAllocator<U>& b) { return a.alloc != b.alloc; }

	template<class T>
	using FrameAllocVector = std::vector<T, FrameResizableStdAllocator<T>>;

	class String;

	class GLOWSYSTEM_EXPORT MemoryProfiler
		: public Subsystem
	{
	private:
		std::unordered_map<UInt64, UInt64> memorySizesUsage;
		mutable std::mutex mutex;
	public:
		void notifyUsage(const UInt64 size);
		void writeAllSizeUsages() const;

		static void writeSizeUsages();
		static Map<std::thread::id, Vector<Pair<size_t, size_t>>> getAllMemoryUsages();
		static String getAllMemoryUsagesAsString();
		static void writeAllMemoryUsagesToStream(std::ostream& os);
	};
}

#endif