#include "UtfStringConverter.h"
#include "Error.h"

#include <unicode/utypes.h>
#include <unicode/uchar.h>
#include <unicode/locid.h>
#include <unicode/ustring.h>
#include <unicode/ucnv.h>
#include <unicode/unistr.h>

GLOWE::Locale::Locale()
	: loc("C")
{
	std::locale::global(loc);
}

GLOWE::Locale::~Locale()
{
	std::locale::global(std::locale::classic());
}

std::locale& GLOWE::Locale::getLocale()
{
	return loc;
}

const std::locale& GLOWE::Locale::getLocale() const
{
	return loc;
}

bool GLOWE::isPrintable(const String::Utf32Char character)
{
	return u_isprint(character);
}

bool GLOWE::isGraphical(const String::Utf32Char character)
{
	return u_isgraph(character);
}

bool GLOWE::isWhiteSpace(const String::Utf32Char character)
{
	return u_isspace(character);
}

bool GLOWE::isLineBreakPoint(const String::Utf32Char character)
{
	return u_isUWhiteSpace(character);
}

bool GLOWE::isUppercase(const String::Utf32Char character)
{
	return u_isupper(character);
}

bool GLOWE::isLowercase(const String::Utf32Char character)
{
	return u_islower(character);
}

bool GLOWE::isDigit(const String::Utf32Char character)
{
	return u_isdigit(character);
}

void GLOWE::Utf<8>::toUtf16(const Utf8String& in, Utf16String& out)
{
	static const CodecvtUtf8ToUtf16& f = FacetProvider<CodecvtUtf8ToUtf16>::getFacet();
	std::mbstate_t mbstate = std::mbstate_t();
	const char* temp;
	CodecvtUtf8ToUtf16::intern_type* temp1;

	out.resize(in.size());
	f.in(mbstate, &in[0], &in[in.size()], temp, GlowCodecvtWorkaroundChar16 & out[0], GlowCodecvtWorkaroundChar16 & out[out.size()], temp1);
	out.resize((char16_t*)temp1 - &out[0]);
}

void GLOWE::Utf<8>::toUtf32(const Utf8String& in, Utf32String& out)
{
	static const CodecvtUtf8ToUtf32& f = FacetProvider<CodecvtUtf8ToUtf32>::getFacet();
	std::mbstate_t mbstate = std::mbstate_t();
	const char* temp;
	CodecvtUtf8ToUtf32::intern_type* temp1;

	out.resize(in.size());
	f.in(mbstate, &in[0], &in[in.size()], temp, GlowCodecvtWorkaroundChar32 & out[0], GlowCodecvtWorkaroundChar32 & out[out.size()], temp1);
	out.resize((char32_t*)temp1 - &out[0]);
}

void GLOWE::Utf<8>::toWide(const Utf8String& in, WideString& out)
{
	// If the input string is empty we return immediately, otherwise we wouldn't get U_BUFFER_OVERFLOW_ERROR as a result of u_strFromUTF8.
	if (in.empty())
	{
		out.clear();
		return;
	}

	UErrorCode errCode = UErrorCode::U_ZERO_ERROR;
	int32_t uCharBuffSize = -1;
	u_strFromUTF8(nullptr, 0, &uCharBuffSize, in.c_str(), -1, &errCode);
	if (errCode == UErrorCode::U_BUFFER_OVERFLOW_ERROR)
	{
		Utf16String str(uCharBuffSize, '\0');
		errCode = UErrorCode::U_ZERO_ERROR;
		u_strFromUTF8(&str[0], str.size() + 1, nullptr, in.c_str(), -1, &errCode);
		if (errCode)
		{
			throw StringException("UTF-8 -> UTF-16 conversion error: "_str + u_errorName(errCode));
		}
		u_strToWCS(nullptr, 0, &uCharBuffSize, str.c_str(), -1, &errCode);
		if (errCode == UErrorCode::U_BUFFER_OVERFLOW_ERROR)
		{
			out.resize(uCharBuffSize);
			errCode = UErrorCode::U_ZERO_ERROR;
			u_strToWCS(&out[0], out.size() + 1, nullptr, str.c_str(), -1, &errCode);
			if (errCode)
			{
				out.resize(0);
				throw StringException("UTF-16 -> wide conversion error: "_str + u_errorName(errCode));
			}
		}
		else
		{
			throw StringException("UTF-16 -> wide conversion error: "_str + u_errorName(errCode));
		}
	}
	else
	{
		throw StringException("UTF-8 -> UTF-16 conversion error: "_str + u_errorName(errCode));
	}
}

GLOWE::Utf16String GLOWE::Utf<8>::toUtf16(const Utf8String& in)
{
	Utf16String temp;
	Utf<8>::toUtf16(in, temp);
	return std::move(temp);
}

GLOWE::Utf32String GLOWE::Utf<8>::toUtf32(const Utf8String& in)
{
	Utf32String temp;
	Utf<8>::toUtf32(in, temp);
	return std::move(temp);
}

GLOWE::WideString GLOWE::Utf<8>::toWide(const Utf8String& in)
{
	WideString temp;
	Utf<8>::toWide(in, temp);
	return std::move(temp);
}

void GLOWE::Utf<8>::fromUtf16(const Utf16String& in, Utf8String& out)
{
	static const CodecvtUtf8ToUtf16& f = FacetProvider<CodecvtUtf8ToUtf16>::getFacet();
	std::mbstate_t mbstate = std::mbstate_t();
	const CodecvtUtf8ToUtf16::intern_type* temp;
	char* temp1;

	out.resize(in.size() * f.max_length());
	f.out(mbstate, GlowCodecvtWorkaroundChar16 & in[0], GlowCodecvtWorkaroundChar16 & in[in.size()], temp, &out[0], &out[out.size()], temp1);
	out.resize(temp1 - &out[0]);
}

void GLOWE::Utf<8>::fromUtf32(const Utf32String& in, Utf8String& out)
{
	static const CodecvtUtf8ToUtf32& f = FacetProvider<CodecvtUtf8ToUtf32>::getFacet();
	std::mbstate_t mbstate = std::mbstate_t();
	const CodecvtUtf8ToUtf32::intern_type* temp;
	char* temp1;

	out.resize(in.size() * f.max_length());
	f.out(mbstate, GlowCodecvtWorkaroundChar32 & in[0], GlowCodecvtWorkaroundChar32 & in[in.size()], temp, &out[0], &out[out.size()], temp1);
	out.resize(temp1 - &out[0]);
}

void GLOWE::Utf<8>::fromWide(const WideString& in, Utf8String& out)
{
	// If the input string is empty we return immediately, otherwise we wouldn't get U_BUFFER_OVERFLOW_ERROR as a result of u_strFromUTF8.
	if (in.empty())
	{
		out.clear();
		return;
	}

	UErrorCode errCode = UErrorCode::U_ZERO_ERROR;
	int32_t uCharBuffSize = -1;
	u_strFromWCS(nullptr, 0, &uCharBuffSize, in.c_str(), -1, &errCode);
	if (errCode == UErrorCode::U_BUFFER_OVERFLOW_ERROR)
	{
		Utf16String str(uCharBuffSize, '\0');
		errCode = UErrorCode::U_ZERO_ERROR;
		u_strFromWCS(&str[0], str.size() + 1, nullptr, in.c_str(), -1, &errCode);
		if (errCode)
		{
			throw StringException("Wide -> UTF-16 conversion error: "_str + u_errorName(errCode));
		}
		u_strToUTF8(nullptr, 0, &uCharBuffSize, str.c_str(), -1, &errCode);
		if (errCode == UErrorCode::U_BUFFER_OVERFLOW_ERROR)
		{
			out.resize(uCharBuffSize);
			errCode = UErrorCode::U_ZERO_ERROR;
			u_strToUTF8(&out[0], out.size() + 1, nullptr, str.c_str(), -1, &errCode);
			if (errCode)
			{
				out.resize(0);
				throw StringException("UTF-16 -> UTF-8 conversion error: "_str + u_errorName(errCode));
			}
		}
		else
		{
			throw StringException("UTF-16 -> UTF-8 conversion error: "_str + u_errorName(errCode));
		}
	}
	else
	{
		throw StringException("Wide -> UTF-16 conversion error: "_str + u_errorName(errCode));
	}
}

GLOWE::Utf32String GLOWE::Utf<32>::fromUtf8(const Utf8String& in)
{
	Utf32String temp;
	Utf<32>::fromUtf8(in, temp);
	return std::move(temp);
}

GLOWE::Utf32String GLOWE::Utf<32>::fromUtf16(const Utf16String& in)
{
	Utf32String temp;
	Utf<32>::fromUtf16(in, temp);
	return std::move(temp);
}

GLOWE::Utf32String GLOWE::Utf<32>::fromWide(const WideString& in)
{
	Utf32String temp;
	Utf<32>::fromWide(in, temp);
	return std::move(temp);
}

GLOWE::Utf8String GLOWE::Utf<8>::fromUtf16(const Utf16String& in)
{
	Utf8String temp;
	Utf<8>::fromUtf16(in, temp);
	return std::move(temp);
}

GLOWE::Utf16String GLOWE::Utf<16>::fromUtf8(const Utf8String& in)
{
	Utf16String temp;
	Utf<16>::fromUtf8(in, temp);
	return std::move(temp);
}

GLOWE::Utf16String GLOWE::Utf<16>::fromUtf32(const Utf32String& in)
{
	Utf16String temp;
	Utf<16>::fromUtf32(in, temp);
	return std::move(temp);
}

GLOWE::Utf16String GLOWE::Utf<16>::fromWide(const WideString& in)
{
	Utf16String temp;
	Utf<16>::fromWide(in, temp);
	return std::move(temp);
}

GLOWE::Utf8String GLOWE::Utf<8>::fromUtf32(const Utf32String& in)
{
	Utf8String temp;
	Utf<8>::fromUtf32(in, temp);
	return std::move(temp);
}

GLOWE::Utf8String GLOWE::Utf<8>::fromWide(const WideString& in)
{
	Utf8String temp;
	Utf<8>::fromWide(in, temp);
	return std::move(temp);
}

void GLOWE::Utf<16>::toUtf8(const Utf16String& in, Utf8String& out)
{
	Utf<8>::fromUtf16(in, out);
}

void GLOWE::Utf<16>::toUtf32(const Utf16String& in, Utf32String& out)
{
	Utf8String temp;
	Utf<8>::fromUtf16(in, temp);
	Utf<8>::toUtf32(temp, out);
}

void GLOWE::Utf<16>::toWide(const Utf16String& in, WideString& out)
{
	Utf8String temp;
	Utf<8>::fromUtf16(in, temp);
	Utf<8>::toWide(temp, out);
}

GLOWE::Utf8String GLOWE::Utf<16>::toUtf8(const Utf16String& in)
{
	Utf8String temp;
	Utf<16>::toUtf8(in, temp);
	return std::move(temp);
}

GLOWE::Utf32String GLOWE::Utf<16>::toUtf32(const Utf16String& in)
{
	Utf32String temp;
	Utf<16>::toUtf32(in, temp);
	return std::move(temp);
}

GLOWE::WideString GLOWE::Utf<16>::toWide(const Utf16String& in)
{
	WideString temp;
	Utf<16>::toWide(in, temp);
	return std::move(temp);
}

void GLOWE::Utf<16>::fromUtf8(const Utf8String& in, Utf16String& out)
{
	Utf<8>::toUtf16(in, out);
}

void GLOWE::Utf<16>::fromUtf32(const Utf32String& in, Utf16String& out)
{
	Utf8String temp;
	Utf<8>::fromUtf32(in, temp);
	Utf<8>::toUtf16(temp, out);
}

void GLOWE::Utf<16>::fromWide(const WideString& in, Utf16String& out)
{
	Utf8String temp;
	Utf<8>::fromWide(in, temp);
	Utf<8>::toUtf16(temp, out);
}

void GLOWE::Utf<32>::toUtf8(const Utf32String& in, Utf8String& out)
{
	Utf<8>::fromUtf32(in, out);
}

void GLOWE::Utf<32>::toUtf16(const Utf32String& in, Utf16String& out)
{
	Utf<16>::fromUtf32(in, out);
}

void GLOWE::Utf<32>::toWide(const Utf32String& in, WideString& out)
{
	Utf8String temp;
	Utf<8>::fromUtf32(in, temp);
	Utf<8>::toWide(temp, out);
}

GLOWE::Utf8String GLOWE::Utf<32>::toUtf8(const Utf32String& in)
{
	Utf8String temp;
	Utf<32>::toUtf8(in, temp);
	return std::move(temp);
}

GLOWE::Utf16String GLOWE::Utf<32>::toUtf16(const Utf32String& in)
{
	Utf16String temp;
	Utf<32>::toUtf16(in, temp);
	return std::move(temp);
}

GLOWE::WideString GLOWE::Utf<32>::toWide(const Utf32String& in)
{
	WideString temp;
	Utf<32>::toWide(in, temp);
	return std::move(temp);
}

void GLOWE::Utf<32>::fromUtf8(const Utf8String& in, Utf32String& out)
{
	Utf<8>::toUtf32(in, out);
}

void GLOWE::Utf<32>::fromUtf16(const Utf16String& in, Utf32String& out)
{
	Utf<16>::toUtf32(in, out);
}

void GLOWE::Utf<32>::fromWide(const WideString& in, Utf32String& out)
{
	Utf8String temp;
	Utf<8>::fromWide(in, temp);
	Utf<8>::toUtf32(temp, out);
}
