#pragma once
#ifndef GLOWE_INL_PACKAGE_INCLUDED
#define GLOWE_INL_PACKAGE_INCLUDED

#include "Package.h"

inline bool GLOWE::PackageEntryRead::checkIsOpen() const
{
	return isOpen;
}

inline zip * GLOWE::Package::getRawZipFile()
{
	return file;
}

inline const GLOWE::String & GLOWE::Package::getPath() const
{
	return zipName;
}

inline bool GLOWE::Package::checkIsOpen() const
{
	return isOpen;
}

inline GLOWE::Package::OpenMode GLOWE::Package::getOpenMode() const
{
	return chosenOpenMode;
}

#endif
