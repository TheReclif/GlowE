#pragma once
#ifndef GLOWE_SYSTEM_UUID_INCLUDED
#define GLOWE_SYSTEM_UUID_INCLUDED

#include "Utility.h"
#include "String.h"
#include "SerializableDataCreator.h"
#include "LoadSaveHandle.h"
#include "Random.h"

namespace GLOWE
{
	class GLOWSYSTEM_EXPORT UUID
		: public BinarySerializable
	{
	private:
		UInt32 data0;
		UInt16 data1, data2, data3;
		UInt8 data4[6];
	private:
		UInt64 getSum() const; // Use only in operator <.
	public:
		UUID() = default;
		UUID(const UUID&) = default;
		~UUID() = default;

		bool operator==(const UUID& right) const;
		bool operator!=(const UUID& right) const;
		bool operator<(const UUID& right) const;
		bool operator>(const UUID& right) const;

		bool isNil() const;

		void generate();

		String toString() const;

		virtual void serialize(LoadSaveHandle& handle) const override;
		virtual void deserialize(LoadSaveHandle& handle) override;
	};
}

#endif
