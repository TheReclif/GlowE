#pragma once
#ifndef GLOWE_SYSTEM_OBJECT_INCLUDED
#define GLOWE_SYSTEM_OBJECT_INCLUDED

#include "OneInstance.h"
#include "MemoryMgr.h"
#include "ThreadClass.h"
#include "Random.h"

namespace GLOWE
{
	class GLOWSYSTEM_EXPORT Object
	{
	public:
		using InstanceId = unsigned int;
	private:
		InstanceId instanceId;
	public:
		Object();
		virtual ~Object();

		InstanceId getUniqueId() const;
	};

	class GLOWSYSTEM_EXPORT ObjectRegistry
		: public Lockable, public Subsystem
	{
	private:
		UnorderedMap<Object::InstanceId, Object*> takenIds;
		Random randomEngine;
	public:
		ObjectRegistry();

		Object::InstanceId registerObject(Object* const obj);
		void unregisterObject(const Object::InstanceId id);

		Object* getObject(const Object::InstanceId id) const;

		template<class T>
		T* getObjectAs(const Object::InstanceId id) const
		{
			return dynamic_cast<T*>(getObject(id));
		}
	};
}

#endif
