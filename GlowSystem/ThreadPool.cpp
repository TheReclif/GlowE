#include "ThreadPool.h"
#include "Package.h"
#include "Time.h"

GLOWE::ThreadPool::ThreadPool(const unsigned int workersCount)
	: shouldStop(false), tasks(), awaitingWorkers(0), workers(), conditional(), mutex()
{
	create(workersCount);
}

GLOWE::ThreadPool::~ThreadPool()
{
	destroy();
}

void GLOWE::ThreadPool::create(const unsigned int & workersCount)
{
	destroy();

	PackageMgr& packMgr = getInstance<PackageMgr>();
	unsigned int finalWorkersCount = workersCount;

	if (finalWorkersCount == 0)
	{
		finalWorkersCount = GLOWE::getCPUCoresCount();
	}
	
	auto lambda = [this](Thread::Pointer) -> void
	{
		MutexUniqueLock lock(mutex, std::defer_lock);
		while (true)
		{
			lock.lock();
			awaitingWorkers.fetch_add(1, std::memory_order::memory_order_relaxed);
			conditional.wait(lock, [this]() -> bool
			{ return shouldStop || !tasks.empty(); });
			awaitingWorkers.fetch_sub(1, std::memory_order::memory_order_relaxed);

			if (shouldStop)
			{
				return;
			}

			auto task = std::move(tasks.front());
			tasks.pop();
			lock.unlock();
			try
			{
				task();
			}
			catch (const Exception& e)
			{
				WarningThrow(false, "GLOWE::Exception caught in thread pool's thread. What: " + String(e.what()));
			}
			catch (const std::exception& e)
			{
				WarningThrow(false, "std::exception caught in thread pool's thread. What: " + String(e.what()));
			}
			catch (...)
			{
				WarningThrow(false, "Unknown error caught in thread pool's thread.");
			}
		}
	};

	workers.reserve(finalWorkersCount);
	for (unsigned int x = 0; x < finalWorkersCount; ++x)
	{
		workers.emplace_back(lambda);
		workers.back().setThreadDescription(L"Thread Pool Worker");
		packMgr.declareCustomerThread(workers.back().getThreadID());
	}
}

void GLOWE::ThreadPool::destroy()
{
	PackageMgr& packMgr = getInstance<PackageMgr>();

	{
		DefaultLockGuard guard(mutex);
		shouldStop = true;
		conditional.notify_all();
	}

	for (auto& x : workers)
	{
		x.stopThread();
		packMgr.undeclareCustomerThread(x.getThreadID());
	}

	workers.clear();
	shouldStop = false;
}

GLOWE::Mutex& GLOWE::ThreadPool::getTasksMutex()
{
	return mutex;
}

void GLOWE::ThreadPool::onLockEnd()
{
	conditional.notify_all();
}

GLOWE::Vector<GLOWE::Thread::Id> GLOWE::ThreadPool::getThreadIDs() const
{
	Vector<Thread::Id> result(workers.size());

	for (unsigned int x = 0; x < workers.size(); ++x)
	{
		result[x] = workers[x].getThreadID();
	}

	return result;
}

unsigned int GLOWE::ThreadPool::getThreadsCount() const
{
	return workers.size();
}

void GLOWE::ThreadPool::waitForRemainingTasks() const
{
	const unsigned int workersCount = workers.size();
	while (awaitingWorkers.load() != workersCount)
	{
		Clock::yieldTime();
	}
}
