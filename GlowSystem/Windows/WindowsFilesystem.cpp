#include "WindowsFilesystem.h"
#include "..\Error.h"

void ensureLongPath(GLOWE::WideString& wStr)
{
	if (wStr.size() > 259)
	{
		wStr = L"\\\\?\\" + wStr;
	}
}

std::size_t GLOWE::Windows::FilePath::prefixEnd() const
{
	std::size_t result{};

	if (path.getSize() > 2
		&& WindowsFilesystemUtility::isSeparator(path.getFirstChar())
		&& WindowsFilesystemUtility::isSeparator(path[1])
		&& !WindowsFilesystemUtility::isSeparator(path[2]))
	{
		result = 3;

		for (; result < path.getSize() && !WindowsFilesystemUtility::isSeparator(path[result]); ++result)
		{
		}
	}
	else
	{
		result = path.find(WindowsFilesystemUtility::colon);
		if (result == GLOWE::String::invalidPos)
		{
			result = 0;
		}
		else
		{
			++result;
		}
	}

	return result;
}

std::size_t GLOWE::Windows::FilePath::rootEnd() const
{
	size_t result = prefixEnd();
	if (result < path.getSize()
		&& WindowsFilesystemUtility::isSeparator(path[result]))
	{
		++result;
	}

	return result;
}

GLOWE::FileStatus GLOWE::Windows::Filesystem::getFileStatus(const FilePath & path)
{
	FileStatus result;
	result.exists = false;
	result.isSymlink = false;
	result.type = FileStatus::FileType::Unknown;

	if (!path.isEmpty())
	{
		String::WideString tempStr = Utf<8>::toWide(path.getString());
		ensureLongPath(tempStr);

		DWORD attribData;

		if ((attribData = GetFileAttributesW(tempStr.c_str())) > 0)
		{
			if (attribData != 0xFFFFFFFF)
			{
				result.exists = true;

				if (attribData & FILE_ATTRIBUTE_DIRECTORY)
				{
					result.type = FileStatus::FileType::Directory;
				}
				else if (attribData & FILE_ATTRIBUTE_NORMAL || attribData & FILE_ATTRIBUTE_ARCHIVE)
				{
					result.type = FileStatus::FileType::Regular;
				}

				if (result.type != FileStatus::FileType::Directory && attribData & FILE_ATTRIBUTE_REPARSE_POINT)
				{
					result.isSymlink = true;
				}
			}
		}
	}

	return result;
}

bool GLOWE::Windows::Filesystem::isExisting(const FilePath & path)
{
	return getFileStatus(path).exists;
}

bool GLOWE::Windows::Filesystem::isRegularFile(const FilePath& path)
{
	return getFileStatus(path).type == FileStatus::FileType::Regular;
}

bool GLOWE::Windows::Filesystem::isSymLink(const FilePath & path)
{
	return getFileStatus(path).isSymlink;
}

bool GLOWE::Windows::Filesystem::isDirectory(const FilePath & path)
{
	return getFileStatus(path).type == FileStatus::FileType::Directory;
}

bool GLOWE::Windows::Filesystem::isEmpty(const FilePath & path)
{
	return getFileSize(path) == 0;
}

bool GLOWE::Windows::Filesystem::isBlockFile(const FilePath &)
{
	// There is no such a file type like block file on Windows
	return false;
}

bool GLOWE::Windows::Filesystem::isCharFile(const FilePath &)
{
	// There is no such a file type like character file on Windows
	return false;
}

bool GLOWE::Windows::Filesystem::isSocket(const FilePath&)
{
	return false;
}

bool GLOWE::Windows::Filesystem::isFifo(const FilePath&)
{
	return false;
}

bool GLOWE::Windows::Filesystem::isOther(const FilePath & path)
{
	FileStatus temp = getFileStatus(path);
	return temp.exists
		&& !(temp.isSymlink)
		&& (temp.type != FileStatus::FileType::Directory)
		&& (temp.type != FileStatus::FileType::Regular);
}

GLOWE::Windows::Filesystem::FileSize GLOWE::Windows::Filesystem::getFileSize(const FilePath & path)
{
	String::WideString tempStr = Utf<8>::toWide(path.getString());
	ensureLongPath(tempStr);
	WIN32_FILE_ATTRIBUTE_DATA attribData;

	if (GetFileAttributesExW(tempStr.c_str(), GetFileExInfoStandard, &attribData))
	{
		ULARGE_INTEGER result;
		result.HighPart = attribData.nFileSizeHigh;
		result.LowPart = attribData.nFileSizeLow;
		return result.QuadPart;
	}

	return 0;
}

GLOWE::Windows::Filesystem::FileSize GLOWE::Windows::Filesystem::getFileSizeOnDisk(const FilePath& path)
{
	String::WideString tempStr = Utf<8>::toWide(path.getString());
	ensureLongPath(tempStr);
	
	ULARGE_INTEGER result;
	DWORD highPart;
	result.LowPart = GetCompressedFileSizeW(tempStr.c_str(), &highPart);

	if (result.LowPart == INVALID_FILE_SIZE && GetLastError() != NO_ERROR)
	{
		return 0;
	}
	
	result.HighPart = highPart;
	return result.QuadPart;
}

GLOWE::Windows::Filesystem::FileSize GLOWE::Windows::Filesystem::getFreeSpace(const FilePath & path)
{
	String::WideString tempStr = Utf<8>::toWide(path.getRootPath().getString());
	ensureLongPath(tempStr);
	ULARGE_INTEGER freeSpace;

	if (GetDiskFreeSpaceExW(tempStr.c_str(), &freeSpace, nullptr, nullptr))
	{
		return (FileSize)(freeSpace.QuadPart);
	}

	return 0;
}

void GLOWE::Windows::Filesystem::createDirectory(const FilePath & path)
{
	if (!isExisting(path) && !isDirectory(path))
	{
		String::WideString tempStr = Utf<8>::toWide(path.getString());
		ensureLongPath(tempStr);

		CreateDirectoryW(tempStr.c_str(), nullptr);
	}
}

void GLOWE::Windows::Filesystem::createSymlink(const FilePath& from, const FilePath& to)
{
	if (isExisting(from))
	{
		String::WideString tempFrom = Utf<8>::toWide(from.getString()), tempTo = Utf<8>::toWide(to.getString());
		ensureLongPath(tempFrom);
		ensureLongPath(tempTo);

		CreateSymbolicLinkW(tempTo.c_str(), tempFrom.c_str(), 0);
	}
}

void GLOWE::Windows::Filesystem::createDirectorySymlink(const FilePath& from, const FilePath& to)
{
	if (isExisting(from))
	{
		String::WideString tempFrom = Utf<8>::toWide(from.getString()), tempTo = Utf<8>::toWide(to.getString());
		ensureLongPath(tempFrom);
		ensureLongPath(tempTo);

		CreateSymbolicLinkW(tempTo.c_str(), tempFrom.c_str(), SYMBOLIC_LINK_FLAG_DIRECTORY);
	}
}

GLOWE::Windows::FilePath GLOWE::Windows::Filesystem::createAbsolutePath(const FilePath& path)
{
	FilePath result, relativePath = path.getRelativePath(), root = path.getRootPath();

	if (root.isEmpty())
	{
		// If it doesn't contain a root, we add current working directory for algorithm to work properly (it needs the whole, working version of an absolute path).
		relativePath = Filesystem::getCurrentWorkingDir().getString() + relativePath;
	}
	
	for (auto it = relativePath.begin(), end = relativePath.end(); it != end; ++it)
	{
		const auto stem = it->getStem();
		if (stem == "..")
		{
			result = result.getParentPath();
		}
		else if (stem == ".")
		{
			continue;
		}
		else
		{
			result.append(*it);
		}
	}

	root.append(result);
	return root;
}

GLOWE::FileTimestamp GLOWE::Windows::Filesystem::getTimeStamp(const FilePath& path)
{
	FileTimestamp result;
	if (!tryGetTimeStamp(path, result))
	{
		throw FilesystemException("Unable to get timestamp for \"" + path.getString() + "\"");
	}
	return result;
}

bool GLOWE::Windows::Filesystem::tryGetTimeStamp(const FilePath& path, FileTimestamp& output)
{
	FILETIME lastAccess, lastWrite, creation;

	String::WideString wStr = Utf<8>::toWide(path.getString());
	ensureLongPath(wStr);

	const HANDLE file = CreateFileW(wStr.c_str(), GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, nullptr);
	if (!file)
	{
		return false;
	}

	const auto result = GetFileTime(file, &creation, &lastAccess, &lastWrite);
	CloseHandle(file);
	if (result == 0)
	{
		return false;
	}

	ULARGE_INTEGER lorgInt;

	lorgInt.HighPart = creation.dwHighDateTime;
	lorgInt.LowPart = creation.dwLowDateTime;
	output.creation = Time(lorgInt.QuadPart);

	lorgInt.HighPart = lastAccess.dwHighDateTime;
	lorgInt.LowPart = lastAccess.dwLowDateTime;
	output.lastAccess = Time(lorgInt.QuadPart);

	lorgInt.HighPart = lastWrite.dwHighDateTime;
	lorgInt.LowPart = lastWrite.dwLowDateTime;
	output.lastWrite = Time(lorgInt.QuadPart);

	return true;
}

GLOWE::Windows::FilePath GLOWE::Windows::Filesystem::readSymlinkDestination(const FilePath & path)
{
	if (isExisting(path))
	{
		String::WideString tempPath = Utf<8>::toWide(path.getString());
		ensureLongPath(tempPath);

		HANDLE file = CreateFileW(tempPath.c_str(), GENERIC_READ, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

		if (file)
		{
			String::WideString tempStr;

			DWORD buffSize = GetFinalPathNameByHandleW(file, nullptr, 0, 0);
			tempStr.resize(buffSize + 1);
			GetFinalPathNameByHandleW(file, &tempStr.front(), buffSize, 0);
			CloseHandle(file);

			return String(tempStr);
		}
	}

	return FilePath();
}

void GLOWE::Windows::Filesystem::resize(const FilePath & path, const FileSize & newSize)
{
	if (isRegularFile(path))
	{
		String::WideString tempStr = Utf<8>::toWide(path.getString());
		ensureLongPath(tempStr);

		HANDLE file = CreateFileW(tempStr.c_str(), GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);

		if (file)
		{
			LARGE_INTEGER heLorg;
			heLorg.QuadPart = newSize;
			SetFilePointer(file, heLorg.LowPart, &heLorg.HighPart, FILE_BEGIN);
			SetEndOfFile(file);
			CloseHandle(file);
		}
	}
}

void GLOWE::Windows::Filesystem::copy(const FilePath& source, const FilePath& destination)
{
	if (isExisting(source))
	{
		String::WideString sourceTemp = Utf<8>::toWide(source.getString()), destTemp = Utf<8>::toWide(destination.getString());
		ensureLongPath(sourceTemp);
		ensureLongPath(destTemp);

		CopyFileExW(sourceTemp.c_str(), destTemp.c_str(), nullptr, nullptr, nullptr, 0);
	}
}

void GLOWE::Windows::Filesystem::copySymlink(const FilePath& source, const FilePath& destination)
{
	if (isExisting(source))
	{
		String::WideString sourceTemp = Utf<8>::toWide(source.getString()), destTemp = Utf<8>::toWide(destination.getString());
		ensureLongPath(sourceTemp);
		ensureLongPath(destTemp);

		CopyFileExW(sourceTemp.c_str(), destTemp.c_str(), nullptr, nullptr, nullptr, COPY_FILE_COPY_SYMLINK);
	}
}

void GLOWE::Windows::Filesystem::move(const FilePath & from, const FilePath & destination)
{
	String::WideString tempFrom = Utf<8>::toWide(from.getString()), tempDest = Utf<8>::toWide(destination.getString());
	ensureLongPath(tempFrom);
	ensureLongPath(tempDest);

	MoveFileW(tempFrom.c_str(), tempDest.c_str());
}

void GLOWE::Windows::Filesystem::rename(const FilePath & from, const FilePath & destination)
{
	move(from, destination);
}

void GLOWE::Windows::Filesystem::remove(const FilePath & path)
{
	if (isExisting(path))
	{
		String::WideString tempStr = Utf<8>::toWide(path.getString());
		ensureLongPath(tempStr);
		DeleteFileW(tempStr.c_str());
	}
}

void GLOWE::Windows::Filesystem::removeDirectory(const FilePath & path)
{
	if (isExisting(path))
	{
		String::WideString tempStr = Utf<8>::toWide(path.getString());
		ensureLongPath(tempStr);
		RemoveDirectoryW(tempStr.c_str());
	}
}

GLOWE::Windows::FilePath GLOWE::Windows::Filesystem::getTempPath()
{
	wchar_t buffer[MAX_PATH + 1] = L"";

	GetTempPathW(MAX_PATH + 1, buffer);

	return String(buffer);
}

GLOWE::Windows::FilePath GLOWE::Windows::Filesystem::getCurrentWorkingDir()
{
	DWORD desiredBufferSize = GetCurrentDirectoryW(0, nullptr);
	String::WideString buffer;
	buffer.resize(desiredBufferSize);

	GetCurrentDirectoryW(desiredBufferSize, &buffer.front());
	buffer.resize(desiredBufferSize - 1); // Delete \0 in the last (but still in array) position.

	return String(buffer);
}

void GLOWE::Windows::Filesystem::setCurrentWorkingDir(const FilePath & newDir)
{
	if (!newDir.isEmpty())
	{
		String::WideString tempStr = Utf<8>::toWide(newDir.getString());
		ensureLongPath(tempStr);
		SetCurrentDirectoryW(tempStr.c_str());
	}
}

GLOWE::Windows::FilesystemWatcher::FilesystemWatcher(const FilePath & path, const unsigned int options)
	: watcher(), directoryHandle(), observedPath(), lastChange(), options()
{
	beginObserving(path, options);
}

GLOWE::Windows::FilesystemWatcher::~FilesystemWatcher()
{
	stopObserving();
}

DWORD gloweOptionsToDWORD(const unsigned int options)
{
	using Option = GLOWE::Windows::FilesystemWatcher::Option;
	DWORD result = 0;
	if (options & Option::FileNameChange)
	{
		result |= FILE_NOTIFY_CHANGE_FILE_NAME;
	}
	if (options & Option::DirectoryNameChange)
	{
		result |= FILE_NOTIFY_CHANGE_DIR_NAME;
	}
	if (options & Option::AttributesChange)
	{
		result |= FILE_NOTIFY_CHANGE_ATTRIBUTES;
	}
	if (options & Option::SizeChange)
	{
		result |= FILE_NOTIFY_CHANGE_SIZE;
	}
	if (options & Option::LastWriteChange)
	{
		result |= FILE_NOTIFY_CHANGE_LAST_WRITE;
	}
	if (options & Option::SecurityChange)
	{
		result |= FILE_NOTIFY_CHANGE_SECURITY;
	}

	return result;
}

void GLOWE::Windows::FilesystemWatcher::beginObserving(const FilePath & path, const unsigned int ops)
{
	stopObserving();
	String::WideString wStr = Utf<8>::toWide(path.getString().getString());
	ensureLongPath(wStr);

	if (Filesystem::isExisting(path))
	{
		watcher = FindFirstChangeNotificationW(wStr.c_str(), FALSE, gloweOptionsToDWORD(ops));
		if (watcher == nullptr)
		{
			WarningThrow(false, u8"Unable to create ChangeNotification.");
			return;
		}
		
	}

	if (Filesystem::isDirectory(path))
	{
		directoryHandle = CreateFileW(wStr.c_str(), FILE_LIST_DIRECTORY, FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE, nullptr, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, nullptr);
		if (directoryHandle == nullptr)
		{
			WarningThrow(false, u8"Unable to CreateFileW for watched directory.");
			FindCloseChangeNotification(watcher);
			watcher = nullptr;
			return;
		}
	}

	options = ops;
	observedPath = path;
}

void GLOWE::Windows::FilesystemWatcher::stopObserving()
{
	if (watcher)
	{
		FindCloseChangeNotification(watcher);
		watcher = nullptr;
		observedPath = FilePath();
		options = 0;
	}
	if (directoryHandle)
	{
		CloseHandle(directoryHandle);
		directoryHandle = nullptr;
	}
}

bool GLOWE::Windows::FilesystemWatcher::observe(const Time & howLong)
{
	if (!watcher)
	{
		DWORD status = WaitForSingleObject(watcher, howLong.asMilliseconds());
		if (status == WAIT_OBJECT_0)
		{
			FindNextChangeNotification(watcher);

			if (directoryHandle)
			{
				FILE_NOTIFY_INFORMATION info[128];
				DWORD recvBytes = 0;

				if (ReadDirectoryChangesW(directoryHandle, info, sizeof(info), FALSE, options, &recvBytes, nullptr, nullptr) != 0)
				{
					switch (info[0].Action)
					{
					case FILE_ACTION_ADDED:
						lastChange.whatChanged = Change::Added;
						break;
					case FILE_ACTION_MODIFIED:
						lastChange.whatChanged = Change::Modified;
						break;
					case FILE_ACTION_REMOVED:
						lastChange.whatChanged = Change::Removed;
						break;
					case FILE_ACTION_RENAMED_NEW_NAME:
						lastChange.whatChanged = Change::Renamed;
						break;
					default:
						return false;
						break;
					}
					lastChange.filename = String(info[0].FileName);
				}
			}

			return true;
		}
	}

	return false;
}

GLOWE::Windows::FilesystemWatcher::ChangeInfo GLOWE::Windows::FilesystemWatcher::getLastChange() const
{
	return lastChange;
}

bool GLOWE::Windows::FilesystemWatcher::checkIsValid() const
{
	if (!watcher)
	{
		return false;
	}

	DWORD result = WaitForSingleObject(watcher, 0);
	return result != WAIT_ABANDONED && result != WAIT_FAILED;
}

const GLOWE::Windows::FilePath & GLOWE::Windows::FilesystemWatcher::getObservedPath() const
{
	return observedPath;
}

unsigned int GLOWE::Windows::FilesystemWatcher::getOptions() const
{
	return options;
}
