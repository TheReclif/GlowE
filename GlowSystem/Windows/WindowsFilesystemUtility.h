#pragma once
#ifndef GLOWE_WINDOWS_FILESYSTEMUTILITY_INCLUDED
#define GLOWE_WINDOWS_FILESYSTEMUTILITY_INCLUDED

#include "../Utility.h"
#include "../String.h"

#ifdef GLOWE_COMPILE_FOR_WINDOWS
namespace GLOWE
{
	class GLOWSYSTEM_EXPORT WindowsFilesystemUtility
	{
	public:
		static constexpr GLOWE::String::BasicChar period = '.';
		static constexpr GLOWE::String::BasicChar preferredSeparator = '\\';
		static constexpr GLOWE::String::BasicChar slash = '/';
		static constexpr GLOWE::String::BasicChar backslash = '\\';
		static constexpr GLOWE::String::BasicChar colon = ':';

		static inline bool isSeparator(const char& x)
		{
			return x == slash 
				|| x == backslash;
		}
	};
}
#endif

#endif
