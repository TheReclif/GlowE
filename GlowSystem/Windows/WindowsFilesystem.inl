#pragma once
#ifndef GLOWE_INLINE_FILESYSTEM_INCLUDED
#define GLOWE_INLINE_FILESYSTEM_INCLUDED

#include "WindowsFilesystem.h"

template<class PathType>
void GLOWE::Windows::FilePathIterator<PathType>::updateSubpath()
{
	String str;
	std::size_t prefEnd = fullPath->prefixEnd(), size = fullPath->getSize();

	if(size <= currentOffset)
	{ 
	}
	else if (currentOffset < prefEnd)
	{
		str = fullPath->path.getSubstring(0, prefEnd);
	}
	/*
	else if (currentOffset == prefEnd
		&& prefEnd < size
		&& WindowsFilesystemUtility::isSeparator(fullPath->path[prefEnd]))
	{
		str = WindowsFilesystemUtility::preferredSeparator;
	}
	*/
	else
	{
		size_t nextSlash = 0, nextThing = 0;

		while ((currentOffset + nextSlash < size) && (WindowsFilesystemUtility::isSeparator(fullPath->path[currentOffset + nextSlash])))
		{
			++nextSlash;
		}

		while ((currentOffset + nextSlash + nextThing < size) && (!WindowsFilesystemUtility::isSeparator(fullPath->path[currentOffset + nextSlash + nextThing])))
		{
			++nextThing;
		}

		if (nextThing > 0)
		{
			str = fullPath->path.getSubstring(currentOffset + nextSlash, nextThing);
		}
		else if (nextSlash > 0)
		{
			str = WindowsFilesystemUtility::period;
		}
	}

	currentElement = str;
}

template<class PathType>
inline GLOWE::Windows::FilePathIterator<PathType>::FilePathIterator()
	: fullPath(nullptr), currentOffset(0)
{
}

template<class PathType>
inline GLOWE::Windows::FilePathIterator<PathType>::FilePathIterator(const PathType & path, const std::size_t & offset)
	: fullPath(&path), currentOffset(offset)
{
	updateSubpath();
}

template<class PathType>
inline const PathType & GLOWE::Windows::FilePathIterator<PathType>::operator*() const
{
	return currentElement;
}

template<class PathType>
inline const PathType * GLOWE::Windows::FilePathIterator<PathType>::operator->() const
{
	return &currentElement;
}

template<class PathType>
GLOWE::Windows::FilePathIterator<PathType> & GLOWE::Windows::FilePathIterator<PathType>::operator++()
{
	std::size_t prefixEnd = fullPath->prefixEnd(), size = fullPath->path.getSize();

	if(currentOffset < prefixEnd)
	{ 
		currentOffset = prefixEnd + 1;
	}
	else if (currentOffset == prefixEnd && prefixEnd < size
		&& WindowsFilesystemUtility::isSeparator(fullPath->path[prefixEnd]))
	{
		do
		{ 
			++currentOffset;
		} while (currentOffset < size
			&& !(WindowsFilesystemUtility::isSeparator(fullPath->path[currentOffset])));
	}
	else
	{
		while(currentOffset < size
			&& (WindowsFilesystemUtility::isSeparator(fullPath->path[currentOffset])))
		{
			++currentOffset;
		}

		while (currentOffset < size
			&& !(WindowsFilesystemUtility::isSeparator(fullPath->path[currentOffset])))
		{
			++currentOffset;
		}
	}

	updateSubpath();

	return *this;
}

template<class PathType>
inline GLOWE::Windows::FilePathIterator<PathType> GLOWE::Windows::FilePathIterator<PathType>::operator++(int)
{
	FilePathIterator<PathType> it = *this;
	++(*this);
	return it;
}

template<class PathType>
inline GLOWE::Windows::FilePathIterator<PathType> & GLOWE::Windows::FilePathIterator<PathType>::operator--()
{
	const std::size_t offsetCopy = currentOffset;
	std::size_t backOffset = 0;

	currentOffset = 0;

	do
	{
		backOffset = currentOffset;
		++(*this);
	} 
	while (currentOffset < offsetCopy);

	currentOffset = backOffset;
	updateSubpath();

	return *this;
}

template<class PathType>
inline GLOWE::Windows::FilePathIterator<PathType> GLOWE::Windows::FilePathIterator<PathType>::operator--(int)
{
	FilePathIterator<PathType> it = *this;
	--(*this);
	return it;
}

template<class PathType>
inline bool GLOWE::Windows::FilePathIterator<PathType>::operator==(const FilePathIterator & right) const
{
	return (fullPath == right.fullPath) && (currentOffset == right.currentOffset);
}

template<class PathType>
inline bool GLOWE::Windows::FilePathIterator<PathType>::operator!=(const FilePathIterator & right) const
{
	return !(*this == right);
}

inline GLOWE::Windows::FilePath::FilePath(const String::BasicChar * charArray)
	: path(charArray)
{
	replacePreferredSeparators();
}

inline GLOWE::Windows::FilePath::FilePath(const String & arg)
	: path(arg)
{
	replacePreferredSeparators();
}

inline void GLOWE::Windows::FilePath::clear()
{
	path.clear();
}

inline GLOWE::Windows::FilePath::Iterator GLOWE::Windows::FilePath::begin() const
{
	return Iterator(*this, 0);
}

inline GLOWE::Windows::FilePath::Iterator GLOWE::Windows::FilePath::end() const
{
	return Iterator(*this, path.getSize());
}

inline void GLOWE::Windows::FilePath::append(const String & arg)
{
	GLOWE::String str(arg.begin(), arg.end());

	for (auto& x : str)
	{
		if (WindowsFilesystemUtility::isSeparator(x))
		{
			x = WindowsFilesystemUtility::preferredSeparator;
		}
	}

	const bool isLastColon = !str.isEmpty() ? str.getLastChar() == WindowsFilesystemUtility::colon : false,
		isPathLastCharSeparator = !path.isEmpty() ? WindowsFilesystemUtility::isSeparator(path.getLastChar()) : false,
		isStrFirstCharSeparator = !str.isEmpty() ? WindowsFilesystemUtility::isSeparator(str.getFirstChar()) : false;
	/*
	if (path.getSize() > 0 && !WindowsFilesystemUtility::isSeparator(path.getLastChar()))
	{
		path.append(1, WindowsFilesystemUtility::preferredSeparator);
	}
	*/

	if (isPathLastCharSeparator && isStrFirstCharSeparator)
	{
		path.resize(path.getSize() - 1);
	}
	if (!isPathLastCharSeparator && !isStrFirstCharSeparator && !isLastColon && !path.isEmpty())
	{
		path.append(1, WindowsFilesystemUtility::preferredSeparator);
	}

	if (//path.getSize() > 0 &&
		str.getSize() > 0
		&& isLastColon
		&& !WindowsFilesystemUtility::isSeparator(str.getLastChar())
		&& !isStrFirstCharSeparator)
	{
		str.append(1, WindowsFilesystemUtility::preferredSeparator);
	}

	path.append(str);
}

inline void GLOWE::Windows::FilePath::append(const FilePath & arg)
{
	append(arg.path);
}

inline GLOWE::Windows::FilePath GLOWE::Windows::FilePath::getFilename() const
{
	String::ConstReverseIterator revIt = path.rbegin();
	String::ConstReverseIterator pathEnd = path.rend();
	String result;

	for (; revIt != pathEnd && !(WindowsFilesystemUtility::isSeparator(*revIt)); ++revIt)
	{
		result += *(revIt);
	}

	return FilePath(String(result.rbegin(), result.rend()));
}

inline GLOWE::Windows::FilePath GLOWE::Windows::FilePath::getExtension() const
{
	String str = getFilename();
	std::size_t findResult = str.rfind(WindowsFilesystemUtility::period);

	return (findResult == String::invalidPos
		|| str.getSize() == 1
		|| (str.getSize() == 2 && str.getFirstChar() == WindowsFilesystemUtility::period && str[1] == WindowsFilesystemUtility::period)) ?
		FilePath() :
		FilePath(str.getSubstring(findResult));
}

inline bool GLOWE::Windows::FilePath::hasFilename() const
{
	return !(getFilename().isEmpty());
}

inline bool GLOWE::Windows::FilePath::hasExtension() const
{
	return !(getExtension().isEmpty());
}

inline bool GLOWE::Windows::FilePath::hasRootDirectory() const
{
	return !(getRootDirectory().isEmpty());
}

inline bool GLOWE::Windows::FilePath::hasRootName() const
{
	return !(getRootName().isEmpty());
}

inline bool GLOWE::Windows::FilePath::hasRootPath() const
{
	return !(getRootPath().isEmpty());
}

inline bool GLOWE::Windows::FilePath::hasStem() const
{
	return !(getStem().isEmpty());
}

inline bool GLOWE::Windows::FilePath::hasRelativePath() const
{
	return !(getRelativePath().isEmpty());
}

inline bool GLOWE::Windows::FilePath::hasParentPath() const
{
	return !(getParentPath().isEmpty());
}

inline bool GLOWE::Windows::FilePath::isEmpty() const
{
	return path.isEmpty();
}

inline bool GLOWE::Windows::FilePath::isAbsolute() const
{
	return hasRootName()
		&& hasRootDirectory();
}

inline bool GLOWE::Windows::FilePath::isRelative() const
{
	return !(isAbsolute());
}

inline void GLOWE::Windows::FilePath::removeFilename()
{
	if (!isEmpty() && begin() != --end())
	{
		std::size_t rtEnd = rootEnd(), x = path.getSize();

		while (rtEnd < x && !(WindowsFilesystemUtility::isSeparator(path[x - 1])))
		{
			--x;
		}

		while (rtEnd < x && (WindowsFilesystemUtility::isSeparator(path[x - 1])))
		{
			--x;
		}

		path.erase(x);
	}
}

inline void GLOWE::Windows::FilePath::removeExtension()
{
	replaceExtension();
}

inline void GLOWE::Windows::FilePath::replaceFilename(const FilePath & arg)
{
	removeFilename();
	append(arg);
}

inline void GLOWE::Windows::FilePath::replaceExtension(const FilePath & arg)
{
	//String temp = getParentPath().getString() + getStem().getString();
	FilePath temp(getParentPath());
	temp.append(getStem());

	if (!arg.isEmpty()
		&& !(arg.getString().getFirstChar() == WindowsFilesystemUtility::period))
	{
		temp.path.append(String(WindowsFilesystemUtility::period));
	}

	temp.path.append(arg.getString());

	*this = temp;
}

inline void GLOWE::Windows::FilePath::replacePreferredSeparators()
{
	for (auto& x : path)
	{
		if (WindowsFilesystemUtility::isSeparator(x))
		{
			x = WindowsFilesystemUtility::preferredSeparator;
		}
	}
}

inline bool GLOWE::Windows::FilePath::operator==(const FilePath & right) const
{
	return path == right.path;
}

inline bool GLOWE::Windows::FilePath::operator!=(const FilePath & right) const
{
	return path != right.path;
}

inline GLOWE::Windows::FilePath::operator const String&() const
{
	return getString();
}

inline const GLOWE::String& GLOWE::Windows::FilePath::getString() const
{
	return path;
}

inline std::size_t GLOWE::Windows::FilePath::getSize() const
{
	return path.getSize();
}

inline void GLOWE::Windows::FilePath::serialize(LoadSaveHandle & handle) const
{
	path.serialize(handle);
}

inline void GLOWE::Windows::FilePath::deserialize(LoadSaveHandle & handle)
{
	path.deserialize(handle);
}

inline GLOWE::Windows::FilePath GLOWE::Windows::FilePath::getRootDirectory() const
{
	size_t prefEnd = prefixEnd();
	if (prefEnd < path.getSize()
		&& WindowsFilesystemUtility::isSeparator(path[prefEnd]))
	{
		return FilePath(String(1, WindowsFilesystemUtility::preferredSeparator));
	}

	return FilePath();
}

inline GLOWE::Windows::FilePath GLOWE::Windows::FilePath::getRootName() const
{
	return FilePath(path.getSubstring(0, prefixEnd()));
}

inline GLOWE::Windows::FilePath GLOWE::Windows::FilePath::getRootPath() const
{
	return FilePath(String(path.getCharArray(), rootEnd()));
}

inline GLOWE::Windows::FilePath GLOWE::Windows::FilePath::getStem() const
{
	String filename = getFilename();
	filename.resize(filename.getSize() - getExtension().getString().getSize());

	return FilePath(filename);
}

inline GLOWE::Windows::FilePath GLOWE::Windows::FilePath::getRelativePath() const
{
	std::size_t rtEnd = rootEnd();

	while (rtEnd < path.getSize()
		&& WindowsFilesystemUtility::isSeparator(path[rtEnd]))
	{
		++rtEnd;
	}

	return FilePath(path.getSubstring(rtEnd));
}

inline GLOWE::Windows::FilePath GLOWE::Windows::FilePath::getParentPath() const
{
	FilePath result;
	if (!isEmpty())
	{
		Iterator itEnd = --end();
		for (Iterator x = begin(); x != itEnd; ++x)
		{
			result.append(*x);
			// The fuck is this?!?
			//result.append(WindowsFilesystemUtility::preferredSeparator);
		}
	}

	return result;
}

inline GLOWE::Windows::FileListing::FileListing(const FilePath & path)
{
	create(path);
}

inline GLOWE::Windows::FileListing::Iterator GLOWE::Windows::FileListing::begin()
{
	return files.begin();
}

inline GLOWE::Windows::FileListing::ConstIterator GLOWE::Windows::FileListing::begin() const
{
	return files.begin();
}

inline GLOWE::Windows::FileListing::ReverseIterator GLOWE::Windows::FileListing::rbegin()
{
	return files.rbegin();
}

inline GLOWE::Windows::FileListing::ConstReverseIterator GLOWE::Windows::FileListing::rbegin() const
{
	return files.rbegin();
}

inline GLOWE::Windows::FileListing::Iterator GLOWE::Windows::FileListing::end()
{
	return files.end();
}

inline GLOWE::Windows::FileListing::ConstIterator GLOWE::Windows::FileListing::end() const
{
	return files.end();
}

inline GLOWE::Windows::FileListing::ReverseIterator GLOWE::Windows::FileListing::rend()
{
	return files.rend();
}

inline GLOWE::Windows::FileListing::ConstReverseIterator GLOWE::Windows::FileListing::rend() const
{
	return files.rend();
}

inline void GLOWE::Windows::FileListing::create(const FilePath& path)
{
	files.clear();
	findPath = path;

	if (findPath.isEmpty())
	{
		return;
	}

	String::WideString tempStr = Utf<8>::toWide(findPath.getString());
	if (!path.hasExtension() || Filesystem::isDirectory(path))
	{
		tempStr.append(L"\\*");
	}
	if (tempStr.size() > 259)
	{
		tempStr = L"\\\\?\\" + tempStr;
	}
	
	WIN32_FIND_DATAW findData{};
	HANDLE findHandle = INVALID_HANDLE_VALUE;

	findHandle = FindFirstFileW(tempStr.c_str(), &findData);

	if (findHandle == INVALID_HANDLE_VALUE)
	{
		return;
	}

	do
	{
		files.push_back(String(findData.cFileName));
	} 
	while (FindNextFileW(findHandle, &findData) != 0);

	FindClose(findHandle);
}

inline GLOWE::Windows::FilePath GLOWE::Windows::FileListing::getOriginalPath() const
{
	return findPath;
}

inline std::size_t GLOWE::Windows::FileListing::getSize() const
{
	return files.size();
}

#endif
