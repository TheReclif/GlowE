#pragma once
#ifndef GLOWE_WINDOWS_FILESYSTEM_INCLUDED
#define GLOWE_WINDOWS_FILESYSTEM_INCLUDED

#include "..\Utility.h"
#include "..\Time.h"
#include "..\String.h"
#include "..\UtfStringConverter.h"
#include "..\FilesystemUtility.h"

#include "WindowsFilesystemUtility.h"

namespace GLOWE
{
	namespace Windows
	{
		template<class PathType>
		class FilePathIterator
		{
		public:
			using iterator_category = std::bidirectional_iterator_tag;
		private:
			const PathType* fullPath;
			PathType currentElement;
			std::size_t currentOffset;
		private:
			void updateSubpath();
		public:
			inline FilePathIterator();
			inline FilePathIterator(const PathType& path, const std::size_t& offset);

			FilePathIterator(const FilePathIterator&) = default;
			FilePathIterator(FilePathIterator&&) noexcept = default;

			FilePathIterator& operator=(const FilePathIterator&) = default;

			~FilePathIterator() = default;

			inline const PathType& operator*() const;
			inline const PathType* operator->() const;

			FilePathIterator& operator++();  //pre
			inline FilePathIterator operator++(int);    //post

			FilePathIterator& operator--();  //pre
			inline FilePathIterator operator--(int);    //post

			inline bool operator==(const FilePathIterator& right) const;
			inline bool operator!=(const FilePathIterator& right) const;
		};

		class GLOWSYSTEM_EXPORT FilePath
			: public BinarySerializable
		{
		public:
			using Iterator = typename GLOWE::Windows::FilePathIterator<FilePath>;
		private:
			String path;
		private:
			std::size_t prefixEnd() const;
			std::size_t rootEnd() const;
		public:
			inline FilePath() = default;
			inline FilePath(const FilePath&) = default;
			inline FilePath(FilePath&&) noexcept = default;

			inline FilePath& operator=(const FilePath&) = default;
			inline FilePath& operator=(FilePath&&) noexcept = default;

			inline FilePath(const String::BasicChar* charArray);
			inline FilePath(const String& arg);

			inline ~FilePath() = default;

			inline void clear();

			inline Iterator begin() const;
			inline Iterator end() const;

			inline void append(const String& arg);
			inline void append(const FilePath& arg);

			inline FilePath getFilename() const;
			inline FilePath getExtension() const;
			inline FilePath getRootDirectory() const;
			inline FilePath getRootName() const;
			inline FilePath getRootPath() const;
			inline FilePath getStem() const;
			inline FilePath getRelativePath() const;
			inline FilePath getParentPath() const;

			inline bool hasFilename() const;
			inline bool hasExtension() const;
			inline bool hasRootDirectory() const;
			inline bool hasRootName() const;
			inline bool hasRootPath() const;
			inline bool hasStem() const;
			inline bool hasRelativePath() const;
			inline bool hasParentPath() const;

			inline bool isEmpty() const;
			inline bool isAbsolute() const;
			inline bool isRelative() const;

			inline void removeFilename();
			inline void removeExtension();

			inline void replaceFilename(const FilePath& arg);
			inline void replaceExtension(const FilePath& arg = FilePath());
			inline void replacePreferredSeparators();

			inline bool operator==(const FilePath& right) const;
			inline bool operator!=(const FilePath& right) const;

			inline operator const String&() const;
			inline const String& getString() const;

			inline std::size_t getSize() const;

			virtual void serialize(LoadSaveHandle& handle) const override;
			virtual void deserialize(LoadSaveHandle& handle) override;

			template<class PathType>
			friend class FilePathIterator;
		};

		class GLOWSYSTEM_EXPORT FileListing
		{
		public:
			using Container = Vector<String>;

			using Iterator = Container::iterator;
			using ConstIterator = Container::const_iterator;

			using ReverseIterator = Container::reverse_iterator;
			using ConstReverseIterator = Container::const_reverse_iterator;
		private:
			Container files;
			FilePath findPath;
		public:
			FileListing() = default;
			inline explicit FileListing(const FilePath& path);
			FileListing(const FileListing&) = default;
			FileListing(FileListing&&) noexcept = default;

			~FileListing() = default;

			FileListing& operator=(const FileListing&) = default;
			FileListing& operator=(FileListing&&) noexcept = default;

			inline Iterator begin();
			inline ConstIterator begin() const;

			inline ReverseIterator rbegin();
			inline ConstReverseIterator rbegin() const;

			inline Iterator end();
			inline ConstIterator end() const;

			inline ReverseIterator rend();
			inline ConstReverseIterator rend() const;

			void create(const FilePath& path);

			inline FilePath getOriginalPath() const;

			inline std::size_t getSize() const;
		};

		class GLOWSYSTEM_EXPORT Filesystem
		{
		public:
			using FileSize = std::uintmax_t;
		public:
			Filesystem() = delete;
			~Filesystem() = delete;

			static FileStatus getFileStatus(const FilePath& path);

			static bool isExisting(const FilePath& path);
			static bool isRegularFile(const FilePath& path);
			static bool isSymLink(const FilePath& path);
			static bool isDirectory(const FilePath& path);
			static bool isEmpty(const FilePath& path);
			static bool isBlockFile(const FilePath&);
			static bool isCharFile(const FilePath&);
			static bool isSocket(const FilePath&);
			static bool isFifo(const FilePath&);
			static bool isOther(const FilePath& path);

			static FileSize getFileSize(const FilePath& path);
			static FileSize getFileSizeOnDisk(const FilePath& path);

			static FileSize getFreeSpace(const FilePath& path);

			static void createDirectory(const FilePath& path);
			static void createSymlink(const FilePath& from, const FilePath& to);
			static void createDirectorySymlink(const FilePath& from, const FilePath& to);

			// Will erase all "." and "..".
			static FilePath createAbsolutePath(const FilePath& path);
			// The path may contain "." and "..".
			static FilePath createRelativePath(const FilePath& from, const FilePath& to);

			static FileTimestamp getTimeStamp(const FilePath& path);
			static bool tryGetTimeStamp(const FilePath& path, FileTimestamp& output);

			static FilePath readSymlinkDestination(const FilePath& path);

			static void resize(const FilePath& path, const FileSize& newSize);

			static void copy(const FilePath& source, const FilePath& destination);
			static void copySymlink(const FilePath& source, const FilePath& destination);
			
			static void move(const FilePath& from, const FilePath& destination);

			static void rename(const FilePath& from, const FilePath& destination);

			static void remove(const FilePath& path);
			static void removeDirectory(const FilePath& path);

			//https://msdn.microsoft.com/en-us/library/windows/desktop/aa364992(v=vs.85).aspx
			static FilePath getTempPath();
			static FilePath getCurrentWorkingDir();

			static void setCurrentWorkingDir(const FilePath& newDir);
		};

		class GLOWSYSTEM_EXPORT FilesystemWatcher
		{
		public:
			enum Option
			{
				FileNameChange = 1 << 0,
				DirectoryNameChange = 1 << 1,
				AttributesChange = 1 << 2,
				SizeChange = 1 << 3,
				LastWriteChange = 1 << 4,
				SecurityChange = 1 << 5
			};

			enum class Change
			{
				Added,
				Removed,
				Modified,
				Renamed
			};

			class GLOWSYSTEM_EXPORT ChangeInfo
			{
			public:
				Change whatChanged;
				FilePath filename;
			};
		private:
			HANDLE watcher, directoryHandle;
			FilePath observedPath;
			ChangeInfo lastChange;
			unsigned int options;
		public:
			FilesystemWatcher() = default;
			FilesystemWatcher(const FilesystemWatcher&) = delete;
			FilesystemWatcher(const FilePath& path, const unsigned int options);
			~FilesystemWatcher();

			void beginObserving(const FilePath& path, const unsigned int ops);
			void stopObserving();

			bool observe(const Time& howLong = Time::ZeroTime);

			ChangeInfo getLastChange() const;
			bool checkIsValid() const;
			const FilePath& getObservedPath() const;
			unsigned int getOptions() const;
		};

#include "WindowsFilesystem.inl"
	}
}

#endif
