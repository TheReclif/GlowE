#include "ThreadClass.h"

GLOWE::Thread::Thread()
	: threadHandle(), shouldStop(false), thisPtr(this), stopCallback(nullptr)
{
}

GLOWE::Thread::Thread(Thread && arg) noexcept
	: threadHandle(std::move(arg.threadHandle)), shouldStop(arg.shouldStop.load()), thisPtr(std::move(arg.thisPtr)), stopCallback(nullptr)
{
}

GLOWE::Thread::~Thread()
{
	stopThread();
}

void GLOWE::Thread::setThreadDescription(const wchar_t* name)
{
	if (threadHandle.joinable())
	{
#ifdef GLOWE_COMPILE_FOR_WINDOWS
		SetThreadDescription(threadHandle.native_handle(), name);
#endif
	}
}

void GLOWE::Thread::setStopCallback(const std::function<void(Thread&)>& stopFunc)
{
	stopCallback = stopFunc;
}

void GLOWE::Thread::stopThread()
{
	if (checkIsRunning())
	{
		shouldStop.store(true);
		if (stopCallback)
		{
			stopCallback(*this);
		}
		threadHandle.join();
	}
}

void GLOWE::Thread::detachThread()
{
	if (checkIsRunning())
	{
		threadHandle.detach();
	}
}

void GLOWE::Thread::joinThread()
{
	if (checkIsRunning())
	{
		threadHandle.join();
	}
}

bool GLOWE::Thread::checkIsRunning() const
{
	return threadHandle.joinable();
}

bool GLOWE::Thread::shouldThreadStop() const
{
	return shouldStop.load();
}

GLOWE::Thread::Id GLOWE::Thread::getThreadID() const
{
	return threadHandle.get_id();
}

GLOWE::Thread::Id GLOWE::Thread::getCurrentThreadID()
{
	return std::this_thread::get_id();
}

GLOWE::RecursiveMutex& GLOWE::Lockable::getMutex() const
{
	return mutex;
}

GLOWE::Lockable::operator GLOWE::RecursiveMutex&() const
{
	return mutex;
}
