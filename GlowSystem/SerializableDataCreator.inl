#pragma once
#ifndef GLOWE_SERIALIZABLEDATACREATOR_INL_INCLUDED
#define GLOWE_SERIALIZABLEDATACREATOR_INL_INCLUDED

#include "SerializableDataCreator.h"

inline void GLOWE::Hidden::SerializableDetail::serializeAll(Variables&)
{
	// Empty on purpose.
}

template<class T, class... Args>
inline void GLOWE::Hidden::SerializableDetail::serializeAll(Variables& variables, const String& name, const T& arg, Args&&... args)
{
	variables.emplaceOrAssign(name, serializeValue(arg));
	serializeAll(variables, std::forward<Args>(args)...);
}

template<class... Args>
inline void GLOWE::Hidden::SerializableDetail::serializeAll(Variables& variables, const std::pair<std::function<void(Variables&)>, std::function<void(const Variables&, const ObjectResolveEnvironment&)>>& funcs, Args&&... args)
{
	funcs.first(variables);
	serializeAll(variables, std::forward<Args>(args)...);
}

inline void GLOWE::Hidden::SerializableDetail::deserializeAll(const Variables&, const ObjectResolveEnvironment&)
{
	// Empty on purpose.
}

template<class T, class... Args>
inline void GLOWE::Hidden::SerializableDetail::deserializeAll(const Variables& variables, const ObjectResolveEnvironment& resolveEnv, const String& name, T& arg, Args&&... args)
{
	const auto it = variables.find(name);
	if (it != variables.end())
	{
		deserializeValue(it->second.get(), arg, resolveEnv);
	}
	deserializeAll(variables, resolveEnv, std::forward<Args>(args)...);
}

template<class... Args>
inline void GLOWE::Hidden::SerializableDetail::deserializeAll(const Variables& variables, const ObjectResolveEnvironment& resolveEnv, const std::pair<std::function<void(Variables&)>, std::function<void(const Variables&, const ObjectResolveEnvironment&)>>& funcs, Args&&... args)
{
	funcs.second(variables, resolveEnv);
	deserializeAll(variables, resolveEnv, std::forward<Args>(args)...);
}

inline void GLOWE::Hidden::processAllAccessors(Map<String, UniquePtr<BaseAccessor>>&)
{
	// Empty on purpose.
}

template<class T, class... Args>
inline void GLOWE::Hidden::processAllAccessors(Map<String, UniquePtr<BaseAccessor>>& accessors, const String& name, T& variable, Args&&... args)
{
	accessors.emplace(name, makeUniquePtr<SimpleAccessor<T>>(variable));
	processAllAccessors(accessors, std::forward<Args>(args)...);
}

template<class T, GLOWE::UInt64 ArraySize, class... Args>
inline void GLOWE::Hidden::processAllAccessors(Map<String, UniquePtr<BaseAccessor>>& accessors, const String& name, T(&variable)[ArraySize], Args&&... args)
{
	accessors.emplace(name, makeUniquePtr<ArrayOfSimplesAccessor<T>>(variable));
	processAllAccessors(accessors, std::forward<Args>(args)...);
}

template<class T, GLOWE::UInt64 ArraySize, class ...Args>
void GLOWE::Hidden::processAllAccessors(Map<String, UniquePtr<BaseAccessor>>& accessors, const String& name, Array<T, ArraySize>& variable, Args&& ...args)
{
	accessors.emplace(name, makeUniquePtr<ArrayOfSimplesAccessor<T>>(variable));
	processAllAccessors(accessors, std::forward<Args>(args)...);
}

template<class T, class... Args>
inline void GLOWE::Hidden::processAllAccessors(Map<String, UniquePtr<BaseAccessor>>& accessors, const String& name, Vector<T>& variable, Args&&... args)
{
	accessors.emplace(name, makeUniquePtr<ArrayOfSimplesAccessor<T>>(variable));
	processAllAccessors(accessors, std::forward<Args>(args)...);
}

template<class ...Args>
void GLOWE::Hidden::processAllAccessors(Map<String, UniquePtr<BaseAccessor>>& accessors, const std::pair<std::function<void(Serializable::Variables&)>, std::function<void(const Serializable::Variables&, const ObjectResolveEnvironment&)>>&, Args&& ...args)
{
	processAllAccessors(accessors, std::forward<Args>(args)...);
}

template<class T, class U, class ...Bases>
void GLOWE::Hidden::serializeBases(const T& arg, Serializable::Variables& variables, TypeTuple<U, Bases...>)
{
	arg.U::serialize(variables);
	serializeBases(arg, variables, TypeTuple<Bases...>());
}

template<class T, class U, class ...Bases>
void GLOWE::Hidden::deserializeBases(T& arg, const Serializable::Variables& variables, const ObjectResolveEnvironment& resolveEnv, TypeTuple<U, Bases...>)
{
	arg.U::deserialize(variables, resolveEnv);
	deserializeBases(arg, variables, resolveEnv, TypeTuple<Bases...>());
}

#endif
