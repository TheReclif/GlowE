#pragma once
#ifndef TIME_H_INCLUDED
#define TIME_H_INCLUDED

#include "Utility.h"
#include "SerializableDataCreator.h"

namespace GLOWE
{
	class GLOWSYSTEM_EXPORT Time
	{
	private:
		Int64 microseconds;							   //Czas w mikrosekundach
	public:
		Time();
		Time(const Time&) = default;
		Time(const Int64& timeInMicroseconds);    //Jako argument podaje si� czas w mikrosekundach
		~Time() = default;

		GlowStaticSerialize(GlowBases(), GlowField(microseconds));

		float asSeconds() const;                   //Zwraca czas w sekundach
		long asMilliseconds() const;               //Zwraca czas w milisekundach
		Int64 asMicroseconds() const;              //Zwraca czas w mikrosekundach (domy�lnej jednostce)

		//Operatory liczbowe
		Time operator+(const Time& right) const;
		Time& operator+=(const Time& right);
		Time operator-(const Time& right) const;
		Time& operator-=(const Time& right);
		Time operator*(const Time& right) const;
		Time& operator*=(const Time& right);
		Time operator/(const Time& right) const;
		Time& operator/=(const Time& right);

		//Operatory logiczne
		bool operator==(const Time& right) const;
		bool operator!=(const Time& right) const;
		bool operator==(long long right) const;
		bool operator!=(long long right) const;
		bool operator>(const Time& right) const;
		bool operator<(const Time& right) const;
		bool operator>=(const Time& right) const;
		bool operator<=(const Time& right) const;

		static Time fromSeconds(float arg);
		static Time fromMilliseconds(unsigned long long arg);
		static Time fromMicroseconds(unsigned long long arg);

		static const Time ZeroTime;
	};

	class GLOWSYSTEM_EXPORT Clock
	{
	protected:
		Time startTime;

		Time getCurrentTime() const;
	public:
		Clock();
		Time getAndReset();
		void reset();
		Time getElapsedTime() const;

		static void waitForGivenTime(const Time& time);
		static void yieldTime();
	};

	class GLOWSYSTEM_EXPORT PausableClock
	{
	private:
		Clock actualInstance;
		Clock prevInstance;
		Time pausedTime = Time::ZeroTime;
		bool isPaused = false;
	public:
		void reset();
		void pause();
		void unpause();
		Time getElapsedTime();
		bool getIsPaused();
		long long getElapsedTimeAsMicroseconds();
		float getElapsedTimeAsSeconds();
		long getElapsedTimeAsMilliseconds();
	};
}

#endif