#include "Debug.h"

#include "String.h"

void GLOWE::debugPrint(const GLOWE::String& msg)
{
#ifdef _MSC_VER
	OutputDebugStringW(msg.toWide().c_str());
#else
	std::clog << msg.getString();
#endif
}

void GLOWE::debugPrintln(const GLOWE::String& msg)
{
	debugPrint(msg + "\n");
}
