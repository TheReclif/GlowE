#include "Utility.h"

#include "MemoryMgr.h"

#if defined(free) && defined(_MSC_VER)
#pragma push_macro("free")
#undef free
#include <cds/init.h>
#include <cds/gc/hp.h>
#pragma pop_macro("free")
#else
#include <cds/init.h>
#include <cds/gc/hp.h>
#endif

struct GLOWE::ConcurrentGC::GC
{
	cds::gc::HP gc;
};

GLOWE::ConcurrentGC::ConcurrentGC()
{
	// Ensure memory allocator is created first and destroyed later.
	getInstance<GlobalAllocator>();
	cds::Initialize();
	cds::gc::HP::set_memory_allocator([](size_t size) -> void*
	{
		return getInstance<GlobalAllocator>().allocateSpace(size);
	}, [](void* p)
	{
		getInstance<GlobalAllocator>().deallocate(p);
	});
	// Required to happen AFTER cds::Initialize()
	gc = std::make_unique<GC>();
}

GLOWE::ConcurrentGC::~ConcurrentGC()
{
	gc.reset();
	cds::Terminate();
}

void GLOWE::ConcurrentGC::attachThread() const
{
	cds::threading::Manager::attachThread();
}

void GLOWE::ConcurrentGC::detachThread() const
{
	cds::threading::Manager::detachThread();
}
