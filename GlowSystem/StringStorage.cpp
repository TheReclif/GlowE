#include "StringStorage.h"

void GLOWE::StringStorage::add(Hash hsh, const String& str)
{
	strings.insert(std::make_pair(hsh, str));
}

void GLOWE::StringStorage::remove(Hash hsh)
{
	strings.erase(hsh);
}

GLOWE::String& GLOWE::StringStorage::get(Hash hsh)
{
	return strings[hsh];
}

const GLOWE::String & GLOWE::StringStorage::get(Hash hsh) const
{
	return strings.at(hsh);
}
