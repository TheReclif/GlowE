#pragma once
#ifndef GLOWE_SYSTEM_BINARYSERIALIZABLE_INCLUDED
#define GLOWE_SYSTEM_BINARYSERIALIZABLE_INCLUDED

#include "glowsystem_export.h"

namespace GLOWE
{
	class LoadSaveHandle;

	// Must implement serialize and deserialize just like BinarySerializable, but is cheaper to use (no virtual calls).
	class GLOWSYSTEM_EXPORT StaticBinarySerializableTag {};

	class GLOWSYSTEM_EXPORT BinarySerializable
	{
	public:
		virtual ~BinarySerializable() = default;

		virtual void serialize(LoadSaveHandle& handle) const = 0;
		virtual void deserialize(LoadSaveHandle& handle) = 0;
	};
}

#endif
