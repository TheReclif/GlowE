#pragma once
#ifndef GLOWE_SYSTEM_LOADSAVEHANDLE_INCLUDED
#define GLOWE_SYSTEM_LOADSAVEHANDLE_INCLUDED

#include "Utility.h"
#include "String.h"

namespace GLOWE
{
	class File;
	class Package;
	class PackageEntryRead;

	class GLOWSYSTEM_EXPORT LoadSaveHandle
	{
	public:
		virtual ~LoadSaveHandle() = default;

		virtual void read(void* data, const unsigned long long dataSize) = 0;
		virtual void write(const void* data, const unsigned long long dataSize) = 0;

		virtual void skipBytes(const unsigned long long howMany) = 0;

		virtual unsigned long long getDataSize() = 0;

		template<class T>
		T read()
		{
			char result[sizeof(T)];
			std::memset(result, 0, sizeof(T));
			read((void*)result, sizeof(T));
			return *((T*)result);
		}
		template<class T>
		void write(const T& data)
		{
			write(&data, sizeof(T));
		}

		template<class T>
		typename std::enable_if<!std::is_base_of<BinarySerializable, T>::value && !std::is_base_of<StaticBinarySerializableTag, T>::value, LoadSaveHandle>::type& operator<<(const T& data)
		{
			write(data);
			return *this;
		}
		template<class T>
		typename std::enable_if<std::is_base_of<BinarySerializable, T>::value || std::is_base_of<StaticBinarySerializableTag, T>::value, LoadSaveHandle>::type& operator<<(const T& data)
		{
			data.serialize(*this);
			return *this;
		}

		template<class T>
		typename std::enable_if<!std::is_base_of<BinarySerializable, T>::value && !std::is_base_of<StaticBinarySerializableTag, T>::value, LoadSaveHandle>::type& operator>>(T& data)
		{
			data = read<T>();
			return *this;
		}
		template<class T>
		typename std::enable_if<std::is_base_of<BinarySerializable, T>::value || std::is_base_of<StaticBinarySerializableTag, T>::value, LoadSaveHandle>::type& operator>>(T& data)
		{
			data.deserialize(*this);
			return *this;
		}
	};

	class GLOWSYSTEM_EXPORT FileLoadSaveHandle
		: public LoadSaveHandle, public NoCopy
	{
	private:
		File& file;
	public:
		FileLoadSaveHandle() = delete;
		FileLoadSaveHandle(File& arg);
		virtual ~FileLoadSaveHandle() = default;

		virtual void read(void* data, const unsigned long long dataSize) override;
		virtual void write(const void* data, const unsigned long long dataSize) override;

		virtual void skipBytes(const unsigned long long howMany) override;

		virtual unsigned long long getDataSize() override;
	};

	class GLOWSYSTEM_EXPORT MemoryLoadSaveHandle
		: public LoadSaveHandle, public NoCopy
	{
	private:
		const void* memory;
		unsigned long long dataSize, readPos, writePos;
	public:
		MemoryLoadSaveHandle() = delete;
		MemoryLoadSaveHandle(const void* mem, const unsigned long long size);
		virtual ~MemoryLoadSaveHandle() = default;

		virtual void read(void* data, const unsigned long long size) override;
		virtual void write(const void* data, const unsigned long long size) override;

		virtual void skipBytes(const unsigned long long howMany) override;

		virtual unsigned long long getDataSize() override;

		const void* getReadPtr() const;
		const void* getWritePtr() const;
	};

	class GLOWSYSTEM_EXPORT PackageLoadSaveHandle
		: public LoadSaveHandle, public NoCopy
	{
	private:
		union
		{
			PackageEntryRead* file;
			Package* package;
		};

		String writeData, namefile;
		const bool canWrite;
	public:
		PackageLoadSaveHandle() = delete;
		explicit PackageLoadSaveHandle(PackageEntryRead& arg);
		PackageLoadSaveHandle(Package& arg, const String& filename);
		virtual ~PackageLoadSaveHandle();

		virtual void read(void* data, const unsigned long long size) override;
		virtual void write(const void* data, const unsigned long long size) override;

		virtual void skipBytes(const unsigned long long howMany) override;

		virtual unsigned long long getDataSize() override;
	};

	class GLOWSYSTEM_EXPORT StringLoadSaveHandle
		: public LoadSaveHandle
	{
	private:
		String str;
		bool isWriteable;
		size_t readPos;
	public:
		StringLoadSaveHandle(); // Write mode.
		explicit StringLoadSaveHandle(const String& arg); // Read mode.
		explicit StringLoadSaveHandle(String&& arg); // Read mode.
		virtual ~StringLoadSaveHandle() = default;

		virtual void read(void* data, const unsigned long long size) override;
		virtual void write(const void* data, const unsigned long long size) override;

		virtual void skipBytes(const unsigned long long howMany) override;

		virtual unsigned long long getDataSize() override;

		const String& getString() const;
		String&& consumeString(); // Gets rvalue of the string.
	};
}

#endif
