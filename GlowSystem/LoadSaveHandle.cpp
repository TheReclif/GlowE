#include "LoadSaveHandle.h"
#include "FileIO.h"
#include "Package.h"

GLOWE::FileLoadSaveHandle::FileLoadSaveHandle(File & arg)
	: file(arg)
{
}

void GLOWE::FileLoadSaveHandle::read(void * data, const unsigned long long dataSize)
{
	file.read(data, dataSize);
}

void GLOWE::FileLoadSaveHandle::write(const void * data, const unsigned long long dataSize)
{
	file.write(data, dataSize);
}

void GLOWE::FileLoadSaveHandle::skipBytes(const unsigned long long howMany)
{
	file.movePointer(file.getPointerPosition(File::FilePointer::Read) + howMany);
}

unsigned long long GLOWE::FileLoadSaveHandle::getDataSize()
{
	return file.getFileSize();
}

GLOWE::MemoryLoadSaveHandle::MemoryLoadSaveHandle(const void * mem, const unsigned long long size)
	: memory(mem), dataSize(size), readPos(0ULL), writePos(0ULL)
{
}

void GLOWE::MemoryLoadSaveHandle::read(void * data, const unsigned long long size)
{
	if ((readPos + size) <= dataSize)
	{
		std::memcpy(data, ((const char*)memory) + readPos, size);
		readPos += size;
	}
}

void GLOWE::MemoryLoadSaveHandle::write(const void * data, const unsigned long long size)
{
	if ((writePos + size) <= dataSize)
	{
		std::memcpy(((char*)memory) + writePos, data, size);
		writePos += size;
	}
}

void GLOWE::MemoryLoadSaveHandle::skipBytes(const unsigned long long howMany)
{
	readPos += std::min(howMany, dataSize - readPos);
}

unsigned long long GLOWE::MemoryLoadSaveHandle::getDataSize()
{
	return dataSize;
}

const void* GLOWE::MemoryLoadSaveHandle::getReadPtr() const
{
	return static_cast<const void*>(static_cast<const char*>(memory) + readPos);
}

const void* GLOWE::MemoryLoadSaveHandle::getWritePtr() const
{
	return static_cast<const void*>(static_cast<const char*>(memory) + writePos);
}

GLOWE::PackageLoadSaveHandle::PackageLoadSaveHandle(PackageEntryRead & arg)
	: file(&arg), writeData(), canWrite(false), namefile()
{
}

GLOWE::PackageLoadSaveHandle::PackageLoadSaveHandle(Package & arg, const String& filename)
	: package(&arg), writeData(), canWrite(true), namefile(filename)
{
}

GLOWE::PackageLoadSaveHandle::~PackageLoadSaveHandle()
{
	if (canWrite)
	{
		package->addFile(namefile, writeData.getCharArray(), writeData.getSize());
	}
}

void GLOWE::PackageLoadSaveHandle::read(void * data, const unsigned long long size)
{
	if (!canWrite)
	{
		file->read(data, size);
	}
}

void GLOWE::PackageLoadSaveHandle::write(const void* data, const unsigned long long size)
{
	if (canWrite)
	{
		writeData.append((const char*)data, size);
	}
}

void GLOWE::PackageLoadSaveHandle::skipBytes(const unsigned long long howMany)
{
	if (!canWrite)
	{
		Vector<char> bytes(howMany);
		read(bytes.data(), howMany);
	}
}

unsigned long long GLOWE::PackageLoadSaveHandle::getDataSize()
{
	if (!canWrite)
	{
		return file->getSize();
	}

	return 0;
}

GLOWE::StringLoadSaveHandle::StringLoadSaveHandle()
	: str(), isWriteable(true), readPos(0)
{
}

GLOWE::StringLoadSaveHandle::StringLoadSaveHandle(const String & arg)
	: str(arg), isWriteable(false), readPos(0)
{
}

GLOWE::StringLoadSaveHandle::StringLoadSaveHandle(String&& arg)
	: str(arg), isWriteable(false), readPos(0)
{
}

void GLOWE::StringLoadSaveHandle::read(void* data, const unsigned long long size)
{
	if (!isWriteable)
	{
		if (str.getSize() - readPos >= size)
		{
			std::memcpy(data, &str[readPos], size);
			readPos += size;
		}
	}
}

void GLOWE::StringLoadSaveHandle::write(const void* data, const unsigned long long size)
{
	if (isWriteable)
	{
		str.append(static_cast<const char*>(data), size);
	}
}

void GLOWE::StringLoadSaveHandle::skipBytes(const unsigned long long howMany)
{
	if (!isWriteable)
	{
		readPos += std::min(howMany, static_cast<unsigned long long>(str.getSize() - readPos));
	}
}

unsigned long long GLOWE::StringLoadSaveHandle::getDataSize()
{
	if (!isWriteable)
	{
		return str.getSize();
	}

	return 0;
}

const GLOWE::String & GLOWE::StringLoadSaveHandle::getString() const
{
	return str;
}

GLOWE::String&& GLOWE::StringLoadSaveHandle::consumeString()
{
	return std::move(str);
}
