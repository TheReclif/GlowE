#include "Hash.h"
#include "String.h"
#include "FileIO.h"
#include "LoadSaveHandle.h"

static GLOWE::UInt32 sdbmHash(const char* str, const std::size_t size) noexcept
{
	GLOWE::UInt32 hash = 0;
	const char* const end = str + size;

	while (str != end)
	{
		hash = *str + (hash << 6) + (hash << 16) - hash;
		++str;
	}

	return hash;
}

static GLOWE::UInt32 reverseSdbmHash(const char* str, const std::size_t strLen) noexcept
{
	GLOWE::UInt32 hash = 0;
	const char* end = str;
	str += strLen;

	while (str-- > end)
	{
		hash = *str + (hash << 6) + (hash << 16) - hash;
	}

	return hash;
}

GLOWE::Hash::Hash(const String& str)
{
	hash(str);
}

GLOWE::Hash::Hash(const char * arg)
{
	hash(arg);
}

GLOWE::Hash::Hash(const char * buffer, const std::size_t buffSize)
{
	hash(buffer, buffSize);
}

void GLOWE::Hash::hash(const String& str)
{
	firstPart = sdbmHash(str.getCharArray(), str.getSize());
	secondPart = reverseSdbmHash(str.getCharArray(), str.getSize());
}

void GLOWE::Hash::hash(const char * arg)
{
	hash(arg, std::strlen(arg));
}

void GLOWE::Hash::hash(const char* buffer, const std::size_t buffSize) noexcept
{
	firstPart = sdbmHash(buffer, buffSize);
	secondPart = reverseSdbmHash(buffer, buffSize);
}

bool GLOWE::Hash::isHashed() const
{
	return data != 0;
}

GLOWE::Hash& GLOWE::Hash::operator=(const String& str)
{
	hash(str);
	return *this;
}

GLOWE::Hash & GLOWE::Hash::operator=(const char * str)
{
	hash(str);
	return *this;
}

bool GLOWE::Hash::operator==(const Hash& right) const
{
	return data == right.data;
}

bool GLOWE::Hash::operator!=(const Hash & right) const
{
	return data != right.data;
}

bool GLOWE::Hash::operator<(const Hash & right) const
{
	return data < right.data;
}

bool GLOWE::Hash::operator>(const Hash & right) const
{
	return data > right.data;
}

GLOWE::Hash::operator GLOWE::String() const
{
	return toString(data);
}

GLOWE::String GLOWE::Hash::asNumberString() const
{
	return (String)(*this);
}

void GLOWE::Hash::fromNumberString(const String & arg)
{
	data = arg.toULongLong();
}

void GLOWE::Hash::serialize(LoadSaveHandle & handle) const
{
	handle << data;
}

void GLOWE::Hash::deserialize(LoadSaveHandle & handle)
{
	handle >> data;
}

GLOWE::Hash GLOWE::Hash::combine(const Hash& a, const Hash& b)
{
	UInt64 temp = a.data;
	temp ^= b.data + 0x9e3779b9 + (a.data << 6) + (a.data >> 2);
	return temp;
}

bool GLOWE::Hashable::operator==(const Hashable & right) const
{
	return getHash() == right.getHash();
}

bool GLOWE::Hashable::operator!=(const Hashable & right) const
{
	return getHash() != right.getHash();
}

#ifdef GLOWE_DEBUG
GLOWE::Hash GLOWSYSTEM_EXPORT GLOWE::debugHash(const char* str)
{
	return Hash(str);
}

GLOWE::Hash GLOWSYSTEM_EXPORT GLOWE::debugHashBuffer(const char* str, const unsigned int size)
{
	return Hash(str, size);
}
#endif
