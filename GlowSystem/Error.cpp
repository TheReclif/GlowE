#include "Error.h"
#include "ThreadClass.h"
#ifdef GLOWE_COMPILE_FOR_WINDOWS
#include "Windows/StackWalker.h"
#endif

void GLOWE::ErrorMgr::writeErrorDesc()
{
	Lockable::Guard guard(*this);
	switch (errorType)
	{
	case ErrorType::Warning:
		stdErrorLogMgr << u8"[WARNING]";
		getInstance<LogMgr>() << u8"[WARNING]" << failureDesc << failurePlace;
#ifdef _MSC_VER
		OutputDebugStringA(u8"[WARNING]\n");
#endif
		break;
	case ErrorType::TotalFailure:
		isFailure = true;

		getInstance<LogMgr>() << u8"[ERROR]" << failureDesc << failurePlace;
		getInstance<LogMgr>() << u8"Aborting..." << u8"Please check the error log.";

		stdErrorLogMgr << u8"[ERROR] Aborting...";

		//messageBox(u8"An error has occured.\nAdditional info might be in the errorlog", MsgBoxType::MsgBoxError);
#ifdef _MSC_VER
		OutputDebugStringA(u8"[ERROR]\n");
#endif
		break;
	default:
		stdErrorLogMgr << u8"[INFO]";
		getInstance<LogMgr>() << u8"[INFO]" << failureDesc << failurePlace;
#ifdef _MSC_VER
		OutputDebugStringA(u8"[INFO]\n");
#endif
		break;
	}

	const String finalBacktrace = u8"Backtrace:\n" + getBacktrace(4);
	stdErrorLogMgr << failureDesc;
	stdErrorLogMgr << failurePlace;
	stdErrorLogMgr << finalBacktrace;
#ifdef _MSC_VER
	OutputDebugStringA(failureDesc.getCharArray());
	OutputDebugStringA(failurePlace.getCharArray());
	OutputDebugStringA(finalBacktrace.getCharArray());
#endif
}

GLOWE::ErrorMgr::ErrorMgr()
	: isFailure(false), errorType(), stdErrorLogMgr("", false)
{
	time_t currentTime = time(nullptr);
	wchar_t date[241];
	date[240] = L'\0';
#ifdef _MSC_VER
	tm timeStruct;

	localtime_s(&timeStruct, &currentTime);

	wcsftime(date, 240, L"%F-errorlog.log", &timeStruct);
#else
	tm* timeStruct;

	timeStruct = localtime(&currentTime);

	wcsftime(date, 240, L"%F-errorlog.log", timeStruct);
#endif

	//stdErrorLogMgrLogMgr(String(date), false);
	stdErrorLogMgr.setEndMsg(u8"End of errorlog.");
	stdErrorLogMgr.setFilename(date);
	getInstance<LogMgr>() << u8"Error manager created.";

	stdErrorLogMgr.openFile();
}

void GLOWE::ErrorMgr::throwError(const String& desc, const String& place, const ErrorType failureType)
{
	Lockable::Guard guard(*this);
	errorType = failureType;
	failureDesc = desc;
	failurePlace = place;
	writeErrorDesc();
	getInstance<LogMgr>().flush();
	if(errorType == ErrorType::TotalFailure)
		throw GLOWE_ERRORCODE_TOTALFAILURE;
}

GLOWE::ErrorMgr::ErrorType GLOWE::ErrorMgr::getErrorType()
{
	return errorType;
}

bool GLOWE::ErrorMgr::checkIsFailure()
{
	return isFailure;
}

GLOWE::ErrorInfo::ErrorInfo(String descArg, String placeArg)
	: place(placeArg), desc(descArg)
{
}

GLOWE::String GLOWE::ErrorInfo::getErrorPlace()
{
	return place;
}

GLOWE::String GLOWE::ErrorInfo::getErrorDesc()
{
	return desc;
}

#ifdef GLOWE_COMPILE_FOR_WINDOWS
class GlowStackWalker
	: public StackWalker
{
public:
	GLOWE::String* output = nullptr;
	unsigned int entriesToOmit = 2;
public:
	virtual void OnOutput(LPCSTR szText) override
	{
		if (!output)
			return;

		if (entriesToOmit == 0)
		{
			GLOWE::String& out = *output;
			out += szText;
		}
		else
		{
			--entriesToOmit;
		}
	}
	virtual void OnSymInit(LPCSTR, DWORD, LPCSTR) override {};
	virtual void OnLoadModule(LPCSTR,
		LPCSTR,
		DWORD64,
		DWORD,
		DWORD,
		LPCSTR,
		LPCSTR,
		ULONGLONG) override {};
};

#endif

GLOWE::Exception::Exception()
	: exceptText("Unknown exception"), backtrace(GLOWE::getBacktrace(3))
{
}

GLOWE::Exception::Exception(const char* const arg)
	: exceptText(arg), backtrace(GLOWE::getBacktrace(3))
{
}

const char* GLOWE::Exception::what() const noexcept
{
	return exceptText;
}

const char* GLOWE::Exception::getBacktrace() const noexcept
{
	return backtrace.getCharArray();
}

const GLOWE::String& GLOWE::Exception::getBacktraceStr() const noexcept
{
	return backtrace;
}

GLOWE::String GLOWE::getBacktrace(const unsigned int entriesToOmit)
{
	String result;
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	static GlowStackWalker stackWalker;
#endif
	static Mutex mutex;
	DefaultLockGuard lock(mutex);

#ifdef GLOWE_COMPILE_FOR_WINDOWS
	stackWalker.entriesToOmit = entriesToOmit;
	stackWalker.output = &result;
	stackWalker.ShowCallstack();
#endif

	return result;
}
