#pragma once
#ifndef GLOWE_HASH_INCLUDED
#define GLOWE_HASH_INCLUDED

#include <cstdint>
#include <cstddef>

#include "BinarySerializable.h"
#include "Utility.h"

// Creates a "constexpr Hash variableName" with hash that equals the hashed "text" argument.
#define GlowCompileTimeHash(variableName, text) \
struct variableName##__LINE__ { static constexpr char const* str() { return text; }; }; \
const ::GLOWE::Hash variableName = ::GLOWE::Hash(::GLOWE::Hidden::_ExplodeToCompileTimeString<variableName##__LINE__>::result::sdbmHash(), ::GLOWE::Hidden::_ReverseExplodeToCompileTimeString<variableName##__LINE__>::result::sdbmHash());

namespace GLOWE
{
	namespace Hidden
	{
		constexpr unsigned int constexprStrLen(char const* str, unsigned int count = 0)
		{
			return ('\0' == str[0]) ? count : constexprStrLen(str + 1, count + 1);
		}

		template<uint32_t hash, unsigned int remainingChars, char elem, char... arr>
		class __ConstSdbmHash
		{
		public:
			static constexpr uint32_t result = __ConstSdbmHash<elem + (hash << 6) + (hash << 16) - hash, remainingChars - 1, arr...>::result;
		};

		template<uint32_t hash, char elem>
		class __ConstSdbmHash<hash, 1, elem>
		{
		public:
			static constexpr uint32_t result = elem + (hash << 6) + (hash << 16) - hash;
		};

		template<unsigned int arrSize, char... arr>
		class _ConstSdbmHash
		{
		public:
			static constexpr uint32_t result = __ConstSdbmHash<0, arrSize, arr...>::result;
		};

		template<char... arr>
		class _CompileTimeString
		{
		public:
			static constexpr uint32_t sdbmHash()
			{
				return _ConstSdbmHash<sizeof...(arr), arr...>::result;
			}
		};

		template<class Provider, unsigned int arrSize, char... arr>
		class _ExplodeImpl
		{
		public:
			using result = typename _ExplodeImpl<Provider, arrSize - 1, Provider::str()[arrSize - 1], arr...>::result;
		};

		template<class Provider, char... arr>
		class _ExplodeImpl<Provider, 0, arr...>
		{
		public:
			using result = _CompileTimeString<arr...>;
		};

		template<class Provider>
		class _ExplodeToCompileTimeString
		{
		public:
			using result = typename _ExplodeImpl<Provider, constexprStrLen(Provider::str())>::result;
		};

		template<class Provider, unsigned int iteration, unsigned int arrSize, char... arr>
		class _ReverseExplodeImpl
		{
		public:
			using result = typename _ReverseExplodeImpl<Provider, iteration + 1, arrSize, Provider::str()[iteration - 1], arr...>::result;
		};

		template<class Provider, unsigned int iteration, char... arr>
		class _ReverseExplodeImpl<Provider, iteration, iteration, arr...>
		{
		public:
			using result = _CompileTimeString<Provider::str()[iteration - 1], arr...>;
		};

		template<class Provider>
		class _ReverseExplodeToCompileTimeString
		{
		public:
			using result = typename _ReverseExplodeImpl<Provider, 1, constexprStrLen(Provider::str())>::result;
		};
	}

	class String;

	using Hash32 = std::uint32_t;
	using UInt64 = std::uint64_t;

	class GLOWSYSTEM_EXPORT Hash
		: public StaticBinarySerializableTag
	{
	public:
		union
		{
			UInt64 data;
			struct
			{
				Hash32 firstPart, secondPart;
			};
		};
	public:
		constexpr Hash() : data(0) {};
		constexpr Hash(const Hash32 first, const Hash32 second) : firstPart(first), secondPart(second) {};
		constexpr Hash(const UInt64 arg) : data(arg) {};
		inline Hash(const Hash& arg) noexcept : data(arg.data) {};
		inline Hash(Hash&& arg) noexcept : data(exchange(arg.data, static_cast<UInt64>(0))) {};
		Hash(const String& str);
		Hash(const char* arg);
		Hash(const char* buffer, const std::size_t buffSize);
		~Hash() = default;

		inline Hash& operator=(const Hash& arg) noexcept { data = arg.data; return *this; };
		inline Hash& operator=(Hash&& arg) noexcept { data = exchange(arg.data, static_cast<UInt64>(0)); return *this; };

		void hash(const String& str);
		void hash(const char* arg);
		void hash(const char* buffer, const std::size_t buffSize) noexcept;

		bool isHashed() const;

		Hash& operator=(const String& str);
		Hash& operator=(const char* str);

		bool operator==(const Hash& right) const;
		bool operator!=(const Hash& right) const;
		bool operator<(const Hash& right) const;
		bool operator>(const Hash& right) const;

		operator GLOWE::String() const;

		String asNumberString() const;
		void fromNumberString(const String& arg);

		void serialize(LoadSaveHandle& handle) const;
		void deserialize(LoadSaveHandle& handle);

		static Hash combine(const Hash& a, const Hash& b);
	};

#ifdef GLOWE_DEBUG
	Hash GLOWSYSTEM_EXPORT debugHash(const char* str);
	Hash GLOWSYSTEM_EXPORT debugHashBuffer(const char* str, const unsigned int size);
#endif

	class GLOWSYSTEM_EXPORT Hashable
	{
	public:
		Hashable() = default;
		virtual ~Hashable() = default;

		virtual Hash getHash() const = 0;

		virtual bool operator==(const Hashable& right) const;
		virtual bool operator!=(const Hashable& right) const;
	};
}

namespace std
{
	template<>
	class hash<GLOWE::Hash>
	{
	public:
		inline size_t operator()(const GLOWE::Hash& arg) const noexcept
		{
			return std::hash<decltype(GLOWE::Hash::data)>{}(arg.data);
		}
	};
}

#endif