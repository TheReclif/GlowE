#include "UUID.h"

#define HexCase(Num, x) \
case Num: \
	result.insert(0, #x, 1); \
	break;
#define HexCaseSame(x) HexCase(x, x)

template<class Type>
GLOWE::String decToHex(const Type arg)
{
	using namespace GLOWE;
	String result;

	Type temp = arg, temp2 = 0;

	while (temp != 0)
	{
		temp2 = temp % 16;
		temp /= 16;

		switch (temp2)
		{
			HexCaseSame(0);
			HexCaseSame(1);
			HexCaseSame(2);
			HexCaseSame(3);
			HexCaseSame(4);
			HexCaseSame(5);
			HexCaseSame(6);
			HexCaseSame(7);
			HexCaseSame(8);
			HexCaseSame(9);
			HexCase(10, a);
			HexCase(11, b);
			HexCase(12, c);
			HexCase(13, d);
			HexCase(14, e);
			HexCase(15, f);
		}
	}

	return result;
}

GLOWE::UInt64 GLOWE::UUID::getSum() const
{
	UInt64 temp = data0;
	temp += (UInt32)(data1) + (UInt32)(data2) + (UInt32)(data3);
	temp += (UInt32)(data4[0]) + (UInt32)(data4[1]) + (UInt32)(data4[2]) + (UInt32)(data4[3]) + (UInt32)(data4[4]) + (UInt32)(data4[5]);
	return temp;
}

bool GLOWE::UUID::operator==(const UUID & right) const
{
	if (data0 != right.data0 
		|| data1 != right.data1 
		|| data2 != right.data2 
		|| data3 != right.data3)
	{
		return false;
	}

	for (unsigned int x = 0; x < 6; ++x)
	{
		if (data4[x] != right.data4[x])
		{
			return false;
		}
	}

	return true;
}

bool GLOWE::UUID::operator!=(const UUID & right) const
{
	return !((*this) == right);
}

bool GLOWE::UUID::operator<(const UUID & right) const
{
	return getSum() < right.getSum();
}

bool GLOWE::UUID::operator>(const UUID & right) const
{
	return getSum() > right.getSum();
}

bool GLOWE::UUID::isNil() const
{
	for (unsigned int x = 0; x < 6; ++x)
	{
		if (data4[x] != 0)
		{
			return false;
		}
	}

	return data0 == 0
		&& data1 == 0 
		&& data2 == 0 
		&& data3 == 0;
}

void GLOWE::UUID::generate()
{
	static Random random;
	constexpr unsigned int maxUInt8 = std::numeric_limits<UInt8>::max();
	data0 = (random.randomUInt(0, maxUInt8) << 24) | (random.randomUInt(0, maxUInt8) << 16) | (random.randomUInt(0, maxUInt8) << 8) | (random.randomUInt(0, maxUInt8));
	data1 = (random.randomUInt(0, maxUInt8) << 8) | (random.randomUInt(0, maxUInt8));
	data2 = random.randomUInt(0x4000, 0x4FFF);
	data3 = (random.randomUInt(0, maxUInt8) << 8) | (random.randomUInt(0, maxUInt8));

	for (unsigned int x = 0; x < 6; ++x)
	{
		data4[x] = random.randomUInt(0, maxUInt8);
	}
}

GLOWE::String GLOWE::UUID::toString() const
{
	String result = decToHex(data0) + '-';
	result += decToHex(data1) + '-';
	result += decToHex(data2) + '-';
	result += decToHex(data3) + '-';
	
	for (unsigned int x = 0; x < 6; ++x)
	{
		result += decToHex(data4[x]);
	}

	return result;
}

void GLOWE::UUID::serialize(LoadSaveHandle& handle) const
{
	handle << data0 << data1 << data2 << data3;
	for (unsigned int x = 0; x < 6; ++x)
	{
		handle << data4[x];
	}
}

void GLOWE::UUID::deserialize(LoadSaveHandle& handle)
{
	handle >> data0 >> data1 >> data2 >> data3;
	for (unsigned int x = 0; x < 6; ++x)
	{
		handle >> data4[x];
	}
}
