#include "Object.h"

GLOWE::Object::Object()
	: instanceId(getInstance<ObjectRegistry>().registerObject(this))
{
}

GLOWE::Object::~Object()
{
	getInstance<ObjectRegistry>().unregisterObject(instanceId);
}

GLOWE::Object::InstanceId GLOWE::Object::getUniqueId() const
{
	return instanceId;
}

GLOWE::ObjectRegistry::ObjectRegistry()
	: takenIds(), randomEngine()
{
}

GLOWE::Object::InstanceId GLOWE::ObjectRegistry::registerObject(Object* const obj)
{
	Lockable::Guard lock(*this);
	Object::InstanceId result;
	do
	{
		result = randomEngine.randomUInt();
	} while (takenIds.count(result) > 0);

	takenIds.emplace(result, obj);
	return result;
}

void GLOWE::ObjectRegistry::unregisterObject(const Object::InstanceId id)
{
	Lockable::Guard lock(*this);
	takenIds.erase(id);
}

GLOWE::Object* GLOWE::ObjectRegistry::getObject(const Object::InstanceId id) const
{
	const auto it = takenIds.find(id);
	if (it != takenIds.end())
	{
		return it->second;
	}

	return nullptr;
}
