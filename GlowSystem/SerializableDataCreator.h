#pragma once
#ifndef GLOWE_SYSTEM_SERIALIZABLE_INCLUDED
#define GLOWE_SYSTEM_SERIALIZABLE_INCLUDED

#include "Values.h"
#include "String.h"
#include "MemoryMgr.h"
#include "Object.h"
#include "LoadSaveHandle.h"
#include "Accessors.h"

#define GlowSerializeCustomAccessors(bases, ...) \
virtual void serialize(GLOWE::Serializable::Variables& variables) const override \
{ \
	GLOWE::Hidden::serializeBases(*this, variables, GLOWE::Hidden::TypeTuple<bases>()); \
	GLOWE::Hidden::SerializableDetail::serializeAll(variables, ##__VA_ARGS__); \
} \
virtual void deserialize(const GLOWE::Serializable::Variables& variables, const GLOWE::ObjectResolveEnvironment& resolveEnv) override \
{ \
	GLOWE::Hidden::deserializeBases(*this, variables, resolveEnv, GLOWE::Hidden::TypeTuple<bases>()); \
	GLOWE::Hidden::SerializableDetail::deserializeAll(variables, resolveEnv, ##__VA_ARGS__); \
}

#define GlowSerialize(bases, ...) \
GlowSerializeCustomAccessors(bases, __VA_ARGS__) \
virtual GLOWE::Map<GLOWE::String, GLOWE::UniquePtr<GLOWE::BaseAccessor>> getAccessors() override \
{ \
	GLOWE::Map<GLOWE::String, GLOWE::UniquePtr<GLOWE::BaseAccessor>> result; \
	GLOWE::Hidden::processAllAccessors(result, ##__VA_ARGS__); \
	return result; \
}

#define GlowStaticSerialize(bases, ...) \
void serialize(GLOWE::Serializable::Variables& variables) const \
{ \
	GLOWE::Hidden::serializeBases(*this, variables, GLOWE::Hidden::TypeTuple<bases>()); \
	GLOWE::Hidden::SerializableDetail::serializeAll(variables, ##__VA_ARGS__); \
} \
void deserialize(const GLOWE::Serializable::Variables& variables, const GLOWE::ObjectResolveEnvironment& resolveEnv) \
{ \
	GLOWE::Hidden::deserializeBases(*this, variables, resolveEnv, GLOWE::Hidden::TypeTuple<bases>()); \
	GLOWE::Hidden::SerializableDetail::deserializeAll(variables, resolveEnv, ##__VA_ARGS__); \
}

#define GlowField(field) GLOWE::String(#field), field

#define GlowGetSetField(field, setter, getter) GLOWE::String(#field), std::pair<std::function<decltype(field)()>, std::function<void(const decltype(field)&)>>([this] -> decltype(field){ return getter(); }, [this](const decltype(field)& arg){ const_cast<std::remove_cv<std::remove_reference<decltype(*this)>::type>::type&>(*this).setter(arg); })

#define GlowCustomField(serializer, deserializer) std::pair<std::function<void(GLOWE::Serializable::Variables&)>, std::function<void(const GLOWE::Serializable::Variables&, const GLOWE::ObjectResolveEnvironment&)>>([this](GLOWE::Serializable::Variables& vars) { serializer(vars); }, [this](const GLOWE::Serializable::Variables& vars, const GLOWE::ObjectResolveEnvironment& resolveEnv) { const_cast<std::remove_cv<std::remove_reference<decltype(*this)>::type>::type&>(*this).deserializer(vars, resolveEnv); })

#define GlowBases(...) __VA_ARGS__

namespace GLOWE
{
	class GLOWSYSTEM_EXPORT ObjectResolveEnvironment
	{
	public:
		virtual ~ObjectResolveEnvironment() = default;

		virtual Object* getObject(const Object::InstanceId id) const = 0;

		template<class T>
		T* getObjectAs(const Object::InstanceId id) const
		{
			return dynamic_cast<T*>(getObject(id));
		}
	};

	namespace Hidden
	{
		class GLOWSYSTEM_EXPORT SerializableValueBase
			: private NoCopy, public BinarySerializable
		{
		public:
			enum class Type
			{
				Bool,
				UInt,
				Int,
				UInt64,
				Int64,
				Float,
				Double,
				String,
				BoolArray,
				UIntArray,
				IntArray,
				FloatArray,
				DoubleArray,
				StringArray,
				Variables,
				VariablesArray,
				Map
			};
		public:
			virtual ~SerializableValueBase() = default;

			virtual UniquePtr<SerializableValueBase> clone() const = 0;
			virtual Type getType() const = 0;

			static UniquePtr<SerializableValueBase> createFromEnum(const Type& type);
		};

		class GLOWSYSTEM_EXPORT GlobalObjectResolveEnvironment
			: public ObjectResolveEnvironment
		{
		public:
			virtual ~GlobalObjectResolveEnvironment() = default;

			virtual Object* getObject(const Object::InstanceId id) const override;
		};

		class GLOWSYSTEM_EXPORT LocalObjectResolveEnvironment
			: public ObjectResolveEnvironment
		{
		private:
			Map<Object::InstanceId, Object*> objs;
		public:
			virtual ~LocalObjectResolveEnvironment() = default;

			// Returns false if the object was already existing in the map. So far should mark the data as broken and abort loading.
			bool registerObject(const Object::InstanceId id, Object* const obj);

			virtual Object* getObject(const Object::InstanceId id) const override;
		};
	}

	class GameObject;

	class GLOWSYSTEM_EXPORT Serializable
	{
	public:
		struct GLOWSYSTEM_EXPORT EditorUILayoutElem
		{
			enum class Type
			{
				Field,
				Header,
				Button,
				Custom
			};

			struct FieldData
			{
				String name;
				UniquePtr<BaseAccessor> value;
			};
			struct ButtonData
			{
				String name;
				std::function<void()> onClickCallback;
			};
			struct CustomData
			{
				std::function<void(GLOWE::GameObject&)> onCreate;
			};

			union
			{
				FieldData fieldData;
				String headerName;
				ButtonData buttonData;
				CustomData customData;
			};

			Type type;

			EditorUILayoutElem() = delete;
			EditorUILayoutElem(EditorUILayoutElem&& arg) noexcept;
			EditorUILayoutElem(const Type newType);
			~EditorUILayoutElem();
		};
	public:
		/// @brief Used to store variables for serialization purposes.
		class GLOWSYSTEM_EXPORT Variables
		{
		public:
			using Container = Map<String, UniquePtr<Hidden::SerializableValueBase>>;

			/// @brief Thrown when an invalid get method was used.
			class GLOWSYSTEM_EXPORT TypeDoesNotMatchException
				: public Exception
			{
			private:
				String reason;
			public:
				TypeDoesNotMatchException(const String& typeRequested, const String& requestedName);

				virtual const char* what() const noexcept override;
			};
		private:
			Map<String, UniquePtr<Hidden::SerializableValueBase>> value;
		public:
			Variables() = default;
			Variables(const Variables&) = delete;
			Variables(Variables&&) = default;

			Variables& operator=(const Variables&) = delete;
			Variables& operator=(Variables&&) = default;

			template<class T>
			Container::iterator emplaceOrAssign(const String& a, T&& b)
			{
				const auto it = value.find(a);
				if (it != value.end())
				{
					it->second = std::forward<T>(b);
					return it;
				}

				return value.emplace(a, std::forward<T>(b)).first;
			}
			UniquePtr<Hidden::SerializableValueBase>& operator[](const String& name);

			void set(const String& name, const bool val);
			void set(const String& name, const float val);
			void set(const String& name, const double val);
			void set(const String& name, const UInt32 val);
			void set(const String& name, const Int32 val);
			void set(const String& name, const UInt64 val);
			void set(const String& name, const Int64 val);
			void set(const String& name, const String& val);
			void set(const String& name, const Variables& val);
			void set(const String& name, Variables&& val);
			void set(const String& name, const Vector<Variables>& val);
			void set(const String& name, Vector<Variables>&& val);

			template<class T>
			void fromSerializable(const String& name, const T& arg)
			{
				Variables vars;
				arg.serialize(vars);
				set(name, std::move(vars));
			}

			template<class T>
			void fromSerializables(const String& name, const T* const arr, const unsigned int size)
			{
				Vector<Variables> vars(size);
				for (unsigned int x = 0; x < size; ++x)
				{
					arr[x].serialize(vars[x]);
				}
				set(name, std::move(vars));
			}

			template<class T>
			void fromSerializables(const String& name, const Vector<T>& arr)
			{
				fromSerializables(name, arr.data(), arr.size());
			}

			bool getBool(const String& name) const;
			float getFloat(const String& name) const;
			double getDouble(const String& name) const;
			UInt32 getUInt(const String& name) const;
			Int32 getInt(const String& name) const;
			UInt64 getUInt64(const String& name) const;
			Int64 getInt64(const String& name) const;
			const String& getString(const String& name) const;
			const Variables& getVariables(const String& name) const;
			Variables& getVariables(const String& name);
			const Vector<Variables>& getVariablesArray(const String& name) const;
			Vector<Variables>& getVariablesArray(const String& name);

			Variables clone() const;

			void clear();

			void erase(const String& name);

			bool checkIsPresent(const String& name) const;

			Container::const_iterator begin() const;
			Container::iterator begin();

			Container::const_iterator end() const;
			Container::iterator end();

			Container::const_iterator find(const String& arg) const;

			std::size_t size() const;
		};
	public:
		virtual ~Serializable() = default;

		virtual void serialize(Variables& variables) const = 0;
		virtual void deserialize(const Variables& variables, const ObjectResolveEnvironment& resolveEnv) = 0;
		virtual Map<String, UniquePtr<BaseAccessor>> getAccessors() = 0;
		virtual Vector<EditorUILayoutElem> getVisibleLayout();
	};

	namespace Hidden
	{
		template<class... Ts>
		struct TypeTuple {};

		template<class T>
		void serializeBases(const T&, Serializable::Variables&, TypeTuple<>) {};
		template<class T, class U, class ...Bases>
		void serializeBases(const T& arg, Serializable::Variables& variables, TypeTuple<U, Bases...>);

		template<class T>
		void deserializeBases(T&, const Serializable::Variables&, const ObjectResolveEnvironment&, TypeTuple<>) {};
		template<class T, class U, class ...Bases>
		void deserializeBases(T& arg, const Serializable::Variables& variables, const ObjectResolveEnvironment& resolveEnv, TypeTuple<U, Bases...>);
		
		GLOWSYSTEM_EXPORT void serializeMapSerializableValue(LoadSaveHandle& handle, const String& name, const UniquePtr<SerializableValueBase>& ptr);
		GLOWSYSTEM_EXPORT void deserializeMapSerializableValue(LoadSaveHandle& handle, Serializable::Variables& vars);

		template<class T>
		class SerializableValueClonable
			: public SerializableValueBase
		{
		public:
			virtual UniquePtr<SerializableValueBase> clone() const override
			{
				return makeUniquePtr<T>(static_cast<const T&>(*this).value);
			}
		};

		class GLOWSYSTEM_EXPORT BoolSerializableValue
			: public SerializableValueClonable<BoolSerializableValue>
		{
		public:
			bool value;
		public:
			BoolSerializableValue() = default;
			BoolSerializableValue(const bool arg)
				: value(arg)
			{}
			virtual Type getType() const override
			{
				return Type::Bool;
			}
			virtual void serialize(LoadSaveHandle& handle) const override
			{
				handle << value;
			}
			virtual void deserialize(LoadSaveHandle& handle) override
			{
				handle >> value;
			}
		};

		class GLOWSYSTEM_EXPORT UIntSerializableValue
			: public SerializableValueClonable<UIntSerializableValue>
		{
		public:
			unsigned int value;
		public:
			UIntSerializableValue() = default;
			UIntSerializableValue(const unsigned int arg)
				: value(arg)
			{}
			virtual Type getType() const override
			{
				return Type::UInt;
			}
			virtual void serialize(LoadSaveHandle& handle) const override
			{
				handle << value;
			}
			virtual void deserialize(LoadSaveHandle& handle) override
			{
				handle >> value;
			}
		};

		class GLOWSYSTEM_EXPORT IntSerializableValue
			: public SerializableValueClonable<IntSerializableValue>
		{
		public:
			int value;
		public:
			IntSerializableValue() = default;
			IntSerializableValue(const int arg)
				: value(arg)
			{}
			virtual Type getType() const override
			{
				return Type::Int;
			}
			virtual void serialize(LoadSaveHandle& handle) const override
			{
				handle << value;
			}
			virtual void deserialize(LoadSaveHandle& handle) override
			{
				handle >> value;
			}
		};

		class GLOWSYSTEM_EXPORT UInt64SerializableValue
			: public SerializableValueClonable<UInt64SerializableValue>
		{
		public:
			UInt64 value;
		public:
			UInt64SerializableValue() = default;
			UInt64SerializableValue(const UInt64 arg)
				: value(arg)
			{}
			virtual Type getType() const override
			{
				return Type::UInt64;
			}
			virtual void serialize(LoadSaveHandle& handle) const override
			{
				handle << value;
			}
			virtual void deserialize(LoadSaveHandle& handle) override
			{
				handle >> value;
			}
		};

		class GLOWSYSTEM_EXPORT Int64SerializableValue
			: public SerializableValueClonable<Int64SerializableValue>
		{
		public:
			Int64 value;
		public:
			Int64SerializableValue() = default;
			Int64SerializableValue(const Int64 arg)
				: value(arg)
			{}
			virtual Type getType() const override
			{
				return Type::Int64;
			}
			virtual void serialize(LoadSaveHandle& handle) const override
			{
				handle << value;
			}
			virtual void deserialize(LoadSaveHandle& handle) override
			{
				handle >> value;
			}
		};

		class GLOWSYSTEM_EXPORT FloatSerializableValue
			: public SerializableValueClonable<FloatSerializableValue>
		{
		public:
			float value;
		public:
			FloatSerializableValue() = default;
			FloatSerializableValue(const float arg)
				: value(arg)
			{}
			virtual Type getType() const override
			{
				return Type::Float;
			}
			virtual void serialize(LoadSaveHandle& handle) const override
			{
				handle << value;
			}
			virtual void deserialize(LoadSaveHandle& handle) override
			{
				handle >> value;
			}
		};

		class GLOWSYSTEM_EXPORT DoubleSerializableValue
			: public SerializableValueClonable<DoubleSerializableValue>
		{
		public:
			double value;
		public:
			DoubleSerializableValue() = default;
			DoubleSerializableValue(const double arg)
				: value(arg)
			{}
			virtual Type getType() const override
			{
				return Type::Double;
			}
			virtual void serialize(LoadSaveHandle& handle) const override
			{
				handle << value;
			}
			virtual void deserialize(LoadSaveHandle& handle) override
			{
				handle >> value;
			}
		};

		class GLOWSYSTEM_EXPORT StringSerializableValue
			: public SerializableValueClonable<StringSerializableValue>
		{
		public:
			String value;
		public:
			StringSerializableValue() = default;
			StringSerializableValue(const String& arg)
				: value(arg)
			{}
			virtual Type getType() const override
			{
				return Type::String;
			}
			virtual void serialize(LoadSaveHandle& handle) const override
			{
				handle << value;
			}
			virtual void deserialize(LoadSaveHandle& handle) override
			{
				handle >> value;
			}
		};

		class GLOWSYSTEM_EXPORT BoolArraySerializableValue
			: public SerializableValueClonable<BoolArraySerializableValue>
		{
		public:
			Vector<bool> value;
		public:
			BoolArraySerializableValue() = default;
			BoolArraySerializableValue(const Vector<bool>& arg)
				: value(arg)
			{}
			virtual Type getType() const override
			{
				return Type::BoolArray;
			}
			virtual void serialize(LoadSaveHandle& handle) const override
			{
				handle << static_cast<UInt32>(value.size());
				for (const auto& x : value)
				{
					handle << x;
				}
			}
			virtual void deserialize(LoadSaveHandle& handle) override
			{
				UInt32 size{};
				handle >> size;
				value.resize(size);
				const unsigned int valueSize = static_cast<unsigned int>(value.size());
				for (unsigned int x = 0; x < valueSize; ++x)
				{
					bool temp{};
					handle >> temp;
					value[x] = temp;
				}
			}
		};

		class GLOWSYSTEM_EXPORT UIntArraySerializableValue
			: public SerializableValueClonable<UIntArraySerializableValue>
		{
		public:
			Vector<unsigned int> value;
		public:
			UIntArraySerializableValue() = default;
			UIntArraySerializableValue(const Vector<unsigned int>& arg)
				: value(arg)
			{}
			virtual Type getType() const override
			{
				return Type::UIntArray;
			}
			virtual void serialize(LoadSaveHandle& handle) const override
			{
				handle << static_cast<UInt32>(value.size());
				for (const auto& x : value)
				{
					handle << x;
				}
			}
			virtual void deserialize(LoadSaveHandle& handle) override
			{
				UInt32 size{};
				handle >> size;
				value.resize(size);
				for (auto& x : value)
				{
					handle >> x;
				}
			}
		};

		class GLOWSYSTEM_EXPORT IntArraySerializableValue
			: public SerializableValueClonable<IntArraySerializableValue>
		{
		public:
			Vector<int> value;
		public:
			IntArraySerializableValue() = default;
			IntArraySerializableValue(const Vector<int>& arg)
				: value(arg)
			{}
			virtual Type getType() const override
			{
				return Type::IntArray;
			}
			virtual void serialize(LoadSaveHandle& handle) const override
			{
				handle << static_cast<UInt32>(value.size());
				for (const auto& x : value)
				{
					handle << x;
				}
			}
			virtual void deserialize(LoadSaveHandle& handle) override
			{
				UInt32 size{};
				handle >> size;
				value.resize(size);
				for (auto& x : value)
				{
					handle >> x;
				}
			}
		};

		class GLOWSYSTEM_EXPORT FloatArraySerializableValue
			: public SerializableValueClonable<FloatArraySerializableValue>
		{
		public:
			Vector<float> value;
		public:
			FloatArraySerializableValue() = default;
			FloatArraySerializableValue(const Vector<float>& arg)
				: value(arg)
			{}
			virtual Type getType() const override
			{
				return Type::FloatArray;
			}
			virtual void serialize(LoadSaveHandle& handle) const override
			{
				handle << static_cast<UInt32>(value.size());
				for (const auto& x : value)
				{
					handle << x;
				}
			}
			virtual void deserialize(LoadSaveHandle& handle) override
			{
				UInt32 size{};
				handle >> size;
				value.resize(size);
				for (auto& x : value)
				{
					handle >> x;
				}
			}
		};

		class GLOWSYSTEM_EXPORT DoubleArraySerializableValue
			: public SerializableValueClonable<DoubleArraySerializableValue>
		{
		public:
			Vector<double> value;
		public:
			DoubleArraySerializableValue() = default;
			DoubleArraySerializableValue(const Vector<double>& arg)
				: value(arg)
			{}
			virtual Type getType() const override
			{
				return Type::DoubleArray;
			}
			virtual void serialize(LoadSaveHandle& handle) const override
			{
				handle << static_cast<UInt32>(value.size());
				for (const auto& x : value)
				{
					handle << x;
				}
			}
			virtual void deserialize(LoadSaveHandle& handle) override
			{
				UInt32 size{};
				handle >> size;
				value.resize(size);
				for (auto& x : value)
				{
					handle >> x;
				}
			}
		};

		class GLOWSYSTEM_EXPORT StringArraySerializableValue
			: public SerializableValueClonable<StringArraySerializableValue>
		{
		public:
			Vector<String> value;
		public:
			StringArraySerializableValue() = default;
			StringArraySerializableValue(const Vector<String>& arg)
				: value(arg)
			{}
			virtual Type getType() const override
			{
				return Type::StringArray;
			}
			virtual void serialize(LoadSaveHandle& handle) const override
			{
				handle << static_cast<UInt32>(value.size());
				for (const auto& x : value)
				{
					handle << x;
				}
			}
			virtual void deserialize(LoadSaveHandle& handle) override
			{
				UInt32 size{};
				handle >> size;
				value.resize(size);
				for (auto& x : value)
				{
					handle >> x;
				}
			}
		};

		class GLOWSYSTEM_EXPORT VariablesSerializableValue
			: public SerializableValueBase
		{
		public:
			Serializable::Variables value;
		public:
			VariablesSerializableValue() = default;
			VariablesSerializableValue(Serializable::Variables&& arg) noexcept;

			VariablesSerializableValue& operator=(Serializable::Variables&& arg) noexcept;

			virtual UniquePtr<SerializableValueBase> clone() const override;
			virtual Type getType() const override;
			virtual void serialize(LoadSaveHandle& handle) const override;
			virtual void deserialize(LoadSaveHandle& handle) override;
		};

		class GLOWSYSTEM_EXPORT VariablesArraySerializableValue
			: public SerializableValueBase
		{
		public:
			Vector<Serializable::Variables> value;
		public:
			VariablesArraySerializableValue() = default;
			VariablesArraySerializableValue(Vector<Serializable::Variables>&& arg);

			VariablesArraySerializableValue& operator=(Vector<Serializable::Variables>&& arg);

			virtual UniquePtr<SerializableValueBase> clone() const override;
			virtual Type getType() const override;
			virtual void serialize(LoadSaveHandle& handle) const override;
			virtual void deserialize(LoadSaveHandle& handle) override;
		};

		class GLOWSYSTEM_EXPORT MapSerializableValue
			: public SerializableValueBase
		{
		public:
			Vector<Pair<UniquePtr<SerializableValueBase>, UniquePtr<SerializableValueBase>>> value;
		public:
			virtual UniquePtr<SerializableValueBase> clone() const override
			{
				UniquePtr<MapSerializableValue> result = makeUniquePtr<MapSerializableValue>();

				for (const auto& x : value)
				{
					result->value.emplace_back(x.first->clone(), x.second->clone());
				}

				return result;
			}
			virtual Type getType() const override
			{
				return Type::Map;
			}
			virtual void serialize(LoadSaveHandle& handle) const override;
			virtual void deserialize(LoadSaveHandle& handle) override;
		};

		namespace SerializableDetail
		{
			using Variables = Serializable::Variables;

			inline UniquePtr<SerializableValueBase> serializeValue(const bool& value)
			{
				return makeUniquePtr<BoolSerializableValue>(value);
			}
			inline void deserializeValue(const SerializableValueBase* serVal, bool& value, const ObjectResolveEnvironment&)
			{
				const BoolSerializableValue* const ptr = dynamic_cast<const BoolSerializableValue*>(serVal);
				if (ptr)
				{
					value = ptr->value;
				}
			}

			template<class T>
			inline typename std::enable_if<std::is_integral<T>::value && std::is_unsigned<T>::value && (sizeof(T) <= sizeof(unsigned int)), UniquePtr<SerializableValueBase>>::type serializeValue(const T& value)
			{
				return makeUniquePtr<UIntSerializableValue>(value);
			}
			template<class T>
			inline typename std::enable_if<std::is_integral<T>::value && std::is_unsigned<T>::value && (sizeof(T) <= sizeof(unsigned int))>::type deserializeValue(const SerializableValueBase* serVal, T& value, const ObjectResolveEnvironment&)
			{
				const UIntSerializableValue* const ptr = dynamic_cast<const UIntSerializableValue*>(serVal);
				if (ptr)
				{
					value = ptr->value;
				}
			}

			template<class T>
			inline typename std::enable_if<std::is_integral<T>::value && !std::is_unsigned<T>::value && (sizeof(T) <= sizeof(int)), UniquePtr<SerializableValueBase>>::type serializeValue(const T& value)
			{
				return makeUniquePtr<IntSerializableValue>(value);
			}
			template<class T>
			inline typename std::enable_if<std::is_integral<T>::value && !std::is_unsigned<T>::value && (sizeof(T) <= sizeof(int))>::type deserializeValue(const SerializableValueBase* serVal, T& value, const ObjectResolveEnvironment&)
			{
				const IntSerializableValue* const ptr = dynamic_cast<const IntSerializableValue*>(serVal);
				if (ptr)
				{
					value = ptr->value;
				}
			}

			inline UniquePtr<SerializableValueBase> serializeValue(const float& value)
			{
				return makeUniquePtr<FloatSerializableValue>(value);
			}
			inline void deserializeValue(const SerializableValueBase* serVal, float& value, const ObjectResolveEnvironment&)
			{
				const FloatSerializableValue* const ptr = dynamic_cast<const FloatSerializableValue*>(serVal);
				if (ptr)
				{
					value = ptr->value;
				}
			}

			inline UniquePtr<SerializableValueBase> serializeValue(const double& value)
			{
				return makeUniquePtr<DoubleSerializableValue>(value);
			}
			inline void deserializeValue(const SerializableValueBase* serVal, double& value, const ObjectResolveEnvironment&)
			{
				const DoubleSerializableValue* const ptr = dynamic_cast<const DoubleSerializableValue*>(serVal);
				if (ptr)
				{
					value = ptr->value;
				}
			}

			inline UniquePtr<SerializableValueBase> serializeValue(const String& value)
			{
				return makeUniquePtr<StringSerializableValue>(value);
			}		
			inline void deserializeValue(const SerializableValueBase* serVal, String& value, const ObjectResolveEnvironment&)
			{
				const StringSerializableValue* const ptr = dynamic_cast<const StringSerializableValue*>(serVal);
				if (ptr)
				{
					value = ptr->value;
				}
			}
			
			inline UniquePtr<SerializableValueBase> serializeValue(const Vector<unsigned int>& value)
			{
				return makeUniquePtr<UIntArraySerializableValue>(value);
			}
			inline void deserializeValue(const SerializableValueBase* serVal, Vector<unsigned int>& value, const ObjectResolveEnvironment&)
			{
				const UIntArraySerializableValue* const ptrVal = dynamic_cast<const UIntArraySerializableValue*>(serVal);
				if (ptrVal)
				{
					value = ptrVal->value;
				}
			}

			inline UniquePtr<SerializableValueBase> serializeValue(const Vector<int>& value)
			{
				return makeUniquePtr<IntArraySerializableValue>(value);
			}
			inline void deserializeValue(const SerializableValueBase* serVal, Vector<int>& value, const ObjectResolveEnvironment&)
			{
				const IntArraySerializableValue* const ptrVal = dynamic_cast<const IntArraySerializableValue*>(serVal);
				if (ptrVal)
				{
					value = ptrVal->value;
				}
			}

			inline UniquePtr<SerializableValueBase> serializeValue(const Vector<float>& value)
			{
				return makeUniquePtr<FloatArraySerializableValue>(value);
			}
			inline void deserializeValue(const SerializableValueBase* serVal, Vector<float>& value, const ObjectResolveEnvironment&)
			{
				const FloatArraySerializableValue* const ptrVal = dynamic_cast<const FloatArraySerializableValue*>(serVal);
				if (ptrVal)
				{
					value = ptrVal->value;
				}
			}

			inline UniquePtr<SerializableValueBase> serializeValue(const Vector<double>& value)
			{
				return makeUniquePtr<DoubleArraySerializableValue>(value);
			}
			inline void deserializeValue(const SerializableValueBase* serVal, Vector<double>& value, const ObjectResolveEnvironment&)
			{
				const DoubleArraySerializableValue* const ptrVal = dynamic_cast<const DoubleArraySerializableValue*>(serVal);
				if (ptrVal)
				{
					value = ptrVal->value;
				}
			}

			inline UniquePtr<SerializableValueBase> serializeValue(const Vector<String>& value)
			{
				return makeUniquePtr<StringArraySerializableValue>(value);
			}
			inline void deserializeValue(const SerializableValueBase* serVal, Vector<String>& value, const ObjectResolveEnvironment&)
			{
				const StringArraySerializableValue* const ptrVal = dynamic_cast<const StringArraySerializableValue*>(serVal);
				if (ptrVal)
				{
					value = ptrVal->value;
				}
			}

			inline UniquePtr<SerializableValueBase> serializeValue(const UInt64 value)
			{
				return makeUniquePtr<UInt64SerializableValue>(value);
			}
			inline void deserializeValue(const SerializableValueBase* serVal, UInt64& value, const ObjectResolveEnvironment&)
			{
				const UInt64SerializableValue* const ptrVal = dynamic_cast<const UInt64SerializableValue*>(serVal);
				if (ptrVal)
				{
					value = ptrVal->value;
				}
			}

			inline UniquePtr<SerializableValueBase> serializeValue(const Hash& value)
			{
				return serializeValue(value.data);
			}
			inline void deserializeValue(const SerializableValueBase* serVal, Hash& value, const ObjectResolveEnvironment& resolveEnv)
			{
				deserializeValue(serVal, value.data, resolveEnv);
			}

			inline UniquePtr<SerializableValueBase> serializeValue(const Int64 value)
			{
				return makeUniquePtr<Int64SerializableValue>(value);
			}
			inline void deserializeValue(const SerializableValueBase* serVal, Int64& value, const ObjectResolveEnvironment&)
			{
				const Int64SerializableValue* const ptrVal = dynamic_cast<const Int64SerializableValue*>(serVal);
				if (ptrVal)
				{
					value = ptrVal->value;
				}
			}

			GLOWSYSTEM_EXPORT UniquePtr<SerializableValueBase> serializeValue(const Variables& value);
			GLOWSYSTEM_EXPORT void deserializeValue(const SerializableValueBase* serVal, Variables& value, const ObjectResolveEnvironment&);

			GLOWSYSTEM_EXPORT UniquePtr<SerializableValueBase> serializeValue(const Vector<Variables>& value);
			GLOWSYSTEM_EXPORT void deserializeValue(const SerializableValueBase* serVal, Vector<Variables>& value, const ObjectResolveEnvironment&);

			template<class T, unsigned int ArrSize>
			UniquePtr<SerializableValueBase> serializeValue(const T(&value)[ArrSize])
			{
				Vector<T> temp(ArrSize);
				std::memcpy(temp.data(), value, sizeof(T) * ArrSize);
				return serializeValue(temp);
			}
			template<class T, unsigned int ArrSize>
			void deserializeValue(const SerializableValueBase* serVal, T (&value)[ArrSize], const ObjectResolveEnvironment& resolveEnv)
			{
				Vector<T> temp;
				temp.reserve(ArrSize);
				deserializeValue(serVal, temp, resolveEnv);
				temp.resize(ArrSize);
				std::memcpy(value, temp.data(), sizeof(T) * ArrSize);
			}

			template<class T, std::size_t ArrSize>
			UniquePtr<SerializableValueBase> serializeValue(const std::array<T, ArrSize>& value)
			{
				Vector<T> temp(ArrSize);
				std::memcpy(temp.data(), value.data(), sizeof(T) * ArrSize);
				return serializeValue(temp);
			}
			template<class T, std::size_t ArrSize>
			void deserializeValue(const SerializableValueBase* serVal, std::array<T, ArrSize>& value, const ObjectResolveEnvironment& resolveEnv)
			{
				Vector<T> temp;
				temp.reserve(ArrSize);
				deserializeValue(serVal, temp, resolveEnv);
				temp.resize(ArrSize);
				std::memcpy(value.data(), temp.data(), sizeof(T) * ArrSize);
			}

			// For serializing everything except Serializables and Object pointers.
			template<class T, std::size_t ArrSize1, std::size_t ArrSize2, class = typename std::enable_if<!std::is_base_of<Object, T>::value>::type>
			UniquePtr<SerializableValueBase> serializeValue(const std::array<std::array<T, ArrSize1>, ArrSize2>& value)
			{
				Vector<T> temp(ArrSize1 * ArrSize2);
				std::memcpy(temp.data(), value.data(), sizeof(T) * ArrSize1 * ArrSize2);
				return serializeValue(temp);
			}
			template<class T, std::size_t ArrSize1, std::size_t ArrSize2, class = typename std::enable_if<!std::is_base_of<Object, T>::value>::type>
			void deserializeValue(const SerializableValueBase* serVal, std::array<std::array<T, ArrSize1>, ArrSize2>& value, const ObjectResolveEnvironment& resolveEnv)
			{
				Vector<T> temp;
				temp.reserve(ArrSize1 * ArrSize2);
				deserializeValue(serVal, temp, resolveEnv);
				temp.resize(ArrSize1 * ArrSize2);
				std::memcpy(value.data(), temp.data(), sizeof(T) * ArrSize1 * ArrSize2);
			}

			template<class T, class = typename std::enable_if<std::is_base_of<GLOWE::Object, T>::value>::type>
			UniquePtr<SerializableValueBase> serializeValue(const T* const arg)
			{
				if (arg)
				{
					return serializeValue(arg->getUniqueId());
				}
				return nullptr;
			}
			template<class T, class = typename std::enable_if<std::is_base_of<GLOWE::Object, T>::value>::type>
			void deserializeValue(const SerializableValueBase* serVal, T*& value, const ObjectResolveEnvironment& resolveEnv)
			{
				Object::InstanceId tempId{};
				deserializeValue(serVal, tempId, resolveEnv);
				value = resolveEnv.getObjectAs<T>(tempId);
			}

			template<class T, class = typename std::enable_if<std::is_base_of<GLOWE::Serializable, T>::value>::type>
			UniquePtr<SerializableValueBase> serializeValue(const Vector<T>& arg)
			{
				UniquePtr<VariablesArraySerializableValue> result = makeUniquePtr<VariablesArraySerializableValue>();

				unsigned int i = 0;
				for (const auto& x : arg)
				{
					Variables temp;
					x.serialize(temp);
					result->value.emplace_back(std::move(temp));
				}

				return result;
			}
			template<class T, class = typename std::enable_if<std::is_base_of<GLOWE::Serializable, T>::value>::type>
			void deserializeValue(const SerializableValueBase* serVal, Vector<T>& value, const ObjectResolveEnvironment& resolveEnv)
			{
				const VariablesArraySerializableValue* const val = dynamic_cast<const VariablesArraySerializableValue*>(serVal);
				value.resize(val->value.size());
				for (unsigned int x = 0, size = value.size(); x < size; ++x)
				{
					value[x].deserialize(val->value[x], resolveEnv);
				}
			}

			template<class T>
			UniquePtr<SerializableValueBase> serializeValue(const std::pair<std::function<T()>, std::function<void(const T&)>>& funcs)
			{
				return serializeValue(funcs.first());
			}
			template<class T>
			void deserializeValue(const SerializableValueBase* serVal, const std::pair<std::function<T()>, std::function<void(const T&)>>& funcs, const ObjectResolveEnvironment& resolveEnv)
			{
				T temp;
				deserializeValue(serVal, temp, resolveEnv);
				funcs.second(temp);
			}

			template<class Key, class Value, class Compare>
			UniquePtr<SerializableValueBase> serializeValue(const Map<Key, Value, Compare>& value)
			{
				UniquePtr<MapSerializableValue> result = makeUniquePtr<MapSerializableValue>();

				for (const auto& x : value)
				{
					result->value.emplace_back(serializeValue(x.first), serializeValue(x.second));
				}

				return result;
			}
			template<class Key, class Value, class Compare>
			void deserializeValue(const SerializableValueBase* serVal, Map<Key, Value, Compare>& value, const ObjectResolveEnvironment& resolveEnv)
			{
				const MapSerializableValue* const ptr = dynamic_cast<const MapSerializableValue*>(serVal);
				if (ptr)
				{
					for (const auto& x : ptr->value)
					{
						Key key;
						deserializeValue(x.first, key, resolveEnv);
						deserializeValue(x.second, value[key], resolveEnv);
					}
				}
			}

			template<class Key, class Value, class Hasher, class KeyEq>
			UniquePtr<SerializableValueBase> serializeValue(const UnorderedMap<Key, Value, Hasher, KeyEq>& value)
			{
				UniquePtr<MapSerializableValue> result = makeUniquePtr<MapSerializableValue>();

				for (const auto& x : value)
				{
					result->value.emplace_back(serializeValue(x.first), serializeValue(x.second));
				}

				return result;
			}
			template<class Key, class Value, class Hasher, class KeyEq>
			void deserializeValue(const SerializableValueBase* serVal, UnorderedMap<Key, Value, Hasher, KeyEq>& value, const ObjectResolveEnvironment& resolveEnv)
			{
				const MapSerializableValue* const ptr = dynamic_cast<const MapSerializableValue*>(serVal);
				if (ptr)
				{
					for (const auto& x : ptr->value)
					{
						value.emplace(x.first->clone(), x.second->clone());
					}
				}
			}

			template<class T>
			SFINAE::TypedCheck<UniquePtr<SerializableValueBase>, decltype(std::template declval<T>().serialize(SFINAE::template declref<Serializable::Variables>()))> serializeValue(const T& value)
			{
				Serializable::Variables result;
				value.serialize(result);
				return makeUniquePtr<VariablesSerializableValue>(std::move(result));
			}
			template<class T>
			SFINAE::VoidCheck<decltype(std::template declval<T>().deserialize(std::template declval<const Serializable::Variables>(), std::template declval<const ObjectResolveEnvironment>()))> deserializeValue(const SerializableValueBase* serVal, T& value, const ObjectResolveEnvironment& resolveEnv)
			{
				value.deserialize(dynamic_cast<const VariablesSerializableValue&>(*serVal).value, resolveEnv);
			}

			template<class T, class = typename std::enable_if<std::is_enum<T>::value>::type>
			UniquePtr<SerializableValueBase> serializeValue(const T& value)
			{
				return serializeValue(static_cast<typename std::underlying_type<T>::type>(value));
			}
			template<class T, class = typename std::enable_if<std::is_enum<T>::value>::type>
			void deserializeValue(const SerializableValueBase* serVal, T& value, const ObjectResolveEnvironment& resolveEnv)
			{
				using UnderType = typename std::underlying_type<T>::type;
				UnderType temp{};
				deserializeValue(serVal, temp, resolveEnv);
				value = static_cast<T>(temp);
			}

			GLOWSYSTEM_EXPORT void serializeAll(Variables& variables);
			template<class T, class... Args>
			void serializeAll(Variables& variables, const String& name, const T& arg, Args&&... args);
			template<class... Args>
			void serializeAll(Variables& variables, const std::pair<std::function<void(Variables&)>, std::function<void(const Variables&, const ObjectResolveEnvironment&)>>& funcs, Args&&... args);

			GLOWSYSTEM_EXPORT void deserializeAll(const Variables& variables, const ObjectResolveEnvironment& resolveEnv);
			template<class T, class... Args>
			void deserializeAll(const Variables& variables, const ObjectResolveEnvironment& resolveEnv, const String& name, T& arg, Args&&... args);
			template<class... Args>
			void deserializeAll(const Variables& variables, const ObjectResolveEnvironment& resolveEnv, const std::pair<std::function<void(Variables&)>, std::function<void(const Variables&, const ObjectResolveEnvironment&)>>& funcs, Args&&... args);
		};

		GLOWSYSTEM_EXPORT void processAllAccessors(Map<String, UniquePtr<BaseAccessor>>&);
		template<class T, class... Args>
		void processAllAccessors(Map<String, UniquePtr<BaseAccessor>>& accessors, const String& name, T& variable, Args&&... args);
		template<class T, UInt64 ArraySize, class... Args>
		void processAllAccessors(Map<String, UniquePtr<BaseAccessor>>& accessors, const String& name, T (&variable)[ArraySize], Args&&... args);
		template<class T, UInt64 ArraySize, class... Args>
		void processAllAccessors(Map<String, UniquePtr<BaseAccessor>>& accessors, const String& name, Array<T, ArraySize>& variable, Args&&... args);
		template<class T, class... Args>
		void processAllAccessors(Map<String, UniquePtr<BaseAccessor>>& accessors, const String& name, Vector<T>& variable, Args&&... args);
		template<class... Args>
		void processAllAccessors(Map<String, UniquePtr<BaseAccessor>>& accessors, const std::pair<std::function<void(Serializable::Variables&)>, std::function<void(const Serializable::Variables&, const ObjectResolveEnvironment&)>>& funcs, Args&&... args);
	}
}

#include "SerializableDataCreator.inl"

#endif
