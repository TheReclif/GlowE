#pragma once
#ifndef GLOW_FILEPARSER_INCLUDED
#define GLOW_FILEPARSER_INCLUDED

#include "Utility.h"
#include "Values.h"

namespace GLOWE
{
	class GLOWSYSTEM_EXPORT FileParser
	{
	public:
		using ValuePtr = SharedPtr<Value>;
		using Section = Map<String, ValuePtr>;

		static const GLOWE::String globalSectionName;
	private:
		Map<String, Section> sections;
	public:
		FileParser();
		FileParser(const FileParser&) = delete;
		FileParser(const String& filename);
		~FileParser() = default;

		void parseFromFile(const String& filename);
		void parseToFile(const String& filename) const;

		void parseToString(String& buffOut) const;
		void parseFromString(const String& buffIn);

		void setValue(const String& name, const ValuePtr& arg, const String& section = globalSectionName);
		ValuePtr getValue(const String& name, const String& section = globalSectionName) const;

		bool doesValueExist(const String& name, const String& section = globalSectionName) const;
		bool doesSectionExist(const String& section) const;

		Section& getSection(const String& sectionName);
		const Section& getSection(const String& sectionName) const;
	};
}

#endif
