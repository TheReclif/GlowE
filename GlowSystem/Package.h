#pragma once
#ifndef GLOWE_SYSTEM_PACKAGE_INCLUDED
#define GLOWE_SYSTEM_PACKAGE_INCLUDED

#include "Utility.h"
#include "Error.h"
#include "String.h"
#include "ThreadClass.h"

struct zip_file;
struct zip;
struct zip_source;

namespace GLOWE
{
	class Package;

	class GLOWSYSTEM_EXPORT PackageEntryRead
	{
	private:
		zip_file* entry;
		Package* usedPackage;
		UInt64 size;

		String usedFilename;

		bool isOpen;
	public:
		PackageEntryRead();
		~PackageEntryRead();

		void open(Package& package, const String& filename, const String& password = String());
		void close();

		void read(void* data, const unsigned long long howManyBytes);
		void readAll(String& buff);

		UInt64 getSize() const;

		inline bool checkIsOpen() const;
	};

	class GLOWSYSTEM_EXPORT Package
	{
	public:
		enum OpenMode
		{
			None = 0,

			Read,
			Write
		};
	private:
		zip* file;

		OpenMode chosenOpenMode;

		String zipName;

		ForwardList<String> objectsToAdd;

		UniquePtr<String> memoryBuffer;
		zip_source* memorySource;

		bool isOpen;
	public:
		Package();
		Package(const String& path, const Package::OpenMode& openMode);
		~Package();

		void open(const String& path, const Package::OpenMode& openMode);
		void openFromMemory(const void* data, const UInt64 dataSize); // Read only.
		void openFromMemory(const String& data); // Read only.
		void close();
		void applyChanges();
		void reopen();
		void discardChanges();

		void addDirectory(const String& dirname);
		void deleteFile(const String& filename);
		void renameFile(const String& oldName, const String& newName);

		void addFile(const String& filename, const char* buffer, const UInt64 buffLen);
		void addFileFromFile(const String& filenameInZip, const String& filenameOnDisk, const UInt64 len = 0);

		bool checkFileForExistence(const String& filename) const;

		Vector<String> listAllFiles() const;

		inline zip* getRawZipFile();

		inline const String& getPath() const;
		inline bool checkIsOpen() const;
		inline OpenMode getOpenMode() const;

		void readFile(const String& filename, String& buff, const String& password = String());
	};

	class GLOWSYSTEM_EXPORT PackageMgr
		: public Subsystem
	{
	private:
		Map<Thread::Id, Map<String, Package>> packages;
		Mutex mutex;
	public:
		PackageMgr() = default;
		virtual ~PackageMgr() = default;

		void closeAllPackagesForFilename(const String& filename);

		Package& getPackage(const String& pathHash);

		// Not neccesary to call, just optimizes things.
		void declareCustomerThread(const Thread::Id& id = Thread::Id());
		void undeclareCustomerThread(const Thread::Id& id = Thread::Id());
	};

#include "Package.inl"
}

#endif