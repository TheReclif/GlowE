#include "MemoryMgr.h"
#include "Utility.h"
#include "Error.h"
#include "String.h"

//#define RBMEM_CHECKSANITY

using std::swap;

template<class T, class Y>
inline constexpr T* addPointers(T* const arg, const Y byHowMany)
{
	return (T*)(((uintptr_t)arg) + ((uintptr_t)byHowMany));
}

template<class T, class Y>
inline constexpr T* subPointers(T* const arg, const Y byHowMany)
{
	return (T*)(((uintptr_t)arg) - ((uintptr_t)byHowMany));
}

GLOWE::RBTMemoryAllocator::SizeType GLOWE::RBTMemoryAllocator::getUsedMemory() const
{
	return usedMemory;
}

GLOWE::RBTMemoryAllocator::SizeType GLOWE::RBTMemoryAllocator::getTotalMemory() const
{
	return totalMemory;
}

unsigned int GLOWE::RBTMemoryAllocator::getAllocationsCount() const
{
	return allocations;
}

bool GLOWE::RBTMemoryAllocator::isPointerInMemoryRange(const void * ptr) const
{
	return (ptr >= trueMemoryBegin) && (ptr < endOfMemory);
}

unsigned int GLOWE::RBTMemoryAllocator::dbgCalcTreeNodesCount() const
{
	static Function<void(FreeHeader*, const Function<void(FreeHeader*)>&)> traverseBlock = [](FreeHeader* block, const Function<void(FreeHeader*)>& func)
	{
		if (!block)
		{
			return;
		}

		traverseBlock(block->left, func);
		func(block);
		traverseBlock(block->right, func);
	};

	unsigned int count = 0;

	traverseBlock(freeMemory, [&count](FreeHeader*)
	{
		++count;
	});

	return count;
}

void GLOWE::RBTMemoryAllocator::dbgCheckSanity() const
{
	auto temp = dbgCheckFreeTreeSanity();
	if (temp)
	{
		throw temp;
	}
}

void GLOWE::RBTMemoryAllocator::removeFromRBTree(FreeHeader * block)
{
	if (!block)
	{
		return;
	}

	bool childRed = true, toRemoveRed = checkRedness(block->parent);
	FreeHeader* replacement = nullptr, *replaceParent = nullptr;

	if (block->left && block->right)
	{
		// Two children.
		FreeHeader* succ = block->right;

		while (succ->left)
		{
			succ = succ->left;
		}

		FreeHeader* tempGarbage = nullptr;
		FreeHeader*& toRemoveInParent = cleanAddress(block->parent) ? (((FreeHeader*)cleanAddress(block->parent))->left == block ? ((FreeHeader*)cleanAddress(block->parent))->left : ((FreeHeader*)cleanAddress(block->parent))->right) : tempGarbage;

		if (succ == block->right)
		{
			succ->left = block->left;
			block->left->parent = setRedness(succ, checkRedness(block->left->parent));
			block->left = nullptr;

			const bool tempRed = checkRedness(succ->parent);
			succ->parent = block->parent;
			toRemoveInParent = succ;
			block->parent = setRedness(succ, tempRed);

			block->right = succ->right;
			succ->right = block;
			if (block->right)
			{
				block->right->parent = setRedness(block, checkRedness(block->right->parent));
			}
		}
		else
		{
			FreeHeader*& succInParent = ((FreeHeader*)cleanAddress(succ->parent))->left == succ ? ((FreeHeader*)cleanAddress(succ->parent))->left : ((FreeHeader*)cleanAddress(succ->parent))->right;

			block->left->parent = setRedness(succ, checkRedness(block->left->parent));
			block->right->parent = setRedness(succ, checkRedness(block->right->parent));
			if (succ->right)
			{
				succ->right->parent = setRedness(block, checkRedness(succ->right->parent));
			}

			swap(block->left, succ->left);
			swap(block->right, succ->right);
			swap(block->parent, succ->parent); // Also the reddness is swaped.

			toRemoveInParent = succ;
			succInParent = block;
		}

		if (block == freeMemory)
		{
			freeMemory = succ;
		}

		removeFromRBTree(block);
		return;
	}
	else
		if (block->left || block->right)
		{
			// One child.
			FreeHeader* child = block->left ? block->left : block->right;
			replacement = child;

			if (block == freeMemory)
			{
				// No parent.
				freeMemory = child;
			}
			else
			{
				FreeHeader*& toRemoveInParent = ((FreeHeader*)cleanAddress(block->parent))->left == block ? ((FreeHeader*)cleanAddress(block->parent))->left : ((FreeHeader*)cleanAddress(block->parent))->right;
				toRemoveInParent = child;
			}

			child->parent = setRedness(block->parent, checkRedness(child->parent));
		}
		else
		{
			// An orphan.
			if (block == freeMemory)
			{
				freeMemory = nullptr;
				return;
			}
			else
			{
				FreeHeader*& toRemoveInParent = ((FreeHeader*)cleanAddress(block->parent))->left == block ? ((FreeHeader*)cleanAddress(block->parent))->left : ((FreeHeader*)cleanAddress(block->parent))->right;
				toRemoveInParent = nullptr;
			}
		}

	replaceParent = (FreeHeader*)cleanAddress(block->parent);
	childRed = replacement ? checkRedness(replacement->parent) : false;

	if (childRed || toRemoveRed)
	{
		if (replacement)
		{
			replacement->parent = setRedness(replacement->parent, false);
		}
	}
	else
	{
		// Double black.
		FreeHeader* doubleBlack = replacement, *dbParent = replaceParent, *sibling = doubleBlack, *tempGarbage = nullptr;

		while (doubleBlack != freeMemory)
		{
			sibling = dbParent->left == doubleBlack ? dbParent->right : dbParent->left;

			if (!getRed(sibling))
			{
				if (sibling && (getRed(sibling->left) || getRed(sibling->right)))
				{
					FreeHeader* redNode = getRed(sibling->left) ? sibling->left : sibling->right;
					FreeHeader*& dbParentInParent = cleanAddress(dbParent->parent) ? (((FreeHeader*)cleanAddress(dbParent->parent))->left == dbParent ? ((FreeHeader*)cleanAddress(dbParent->parent))->left : ((FreeHeader*)cleanAddress(dbParent->parent))->right) : tempGarbage;

					// https://www.geeksforgeeks.org/red-black-tree-set-3-delete-2/
					if (dbParent->left == sibling)
					{
						if (sibling->left == redNode)
						{
							// Left left.
							// Mirror of right right.
							dbParent->left = sibling->right;
							if (sibling->right)
							{
								sibling->right->parent = setRedness(dbParent, checkRedness(sibling->right->parent));
							}
							sibling->right = dbParent;
							const bool tempRed = checkRedness(sibling->parent);
							sibling->parent = dbParent->parent;
							dbParent->parent = setRedness(sibling, tempRed);
							dbParentInParent = sibling;
							sibling->left->parent = setRedness(sibling, false);

							if (sibling->parent == nullptr)
							{
								freeMemory = sibling;
							}
						}
						else
						{
							// Left right.
							// First rotation.
							FreeHeader* thing = sibling->right;
							sibling->right = thing->left;
							if (sibling->right)
							{
								sibling->right->parent = setRedness(sibling, checkRedness(sibling->right->parent));
							}
							thing->left = sibling;
							thing->parent = setRedness(dbParent, false);
							(dbParent->left == sibling ? dbParent->left : dbParent->right) = thing;
							sibling->parent = setRedness(thing, false);

							// Second rotation.
							thing->parent = dbParent->parent;
							dbParentInParent = thing;
							dbParent->parent = setRedness(thing, false);
							dbParent->left = thing->right;
							if (dbParent->left)
							{
								dbParent->left->parent = setRedness(dbParent, checkRedness(dbParent->left->parent));
							}
							thing->right = dbParent;

							if (thing->parent == nullptr)
							{
								freeMemory = thing;
							}
						}
					}
					else
					{
						if (sibling->right == redNode || getRed(sibling->right))
						{
							// Right right.
							dbParent->right = sibling->left;
							if (sibling->left)
							{
								sibling->left->parent = setRedness(dbParent, checkRedness(sibling->left->parent));
							}
							sibling->left = dbParent;
							const bool tempRed = checkRedness(sibling->parent);
							sibling->parent = dbParent->parent;
							dbParent->parent = setRedness(sibling, tempRed);
							dbParentInParent = sibling;
							sibling->right->parent = setRedness(sibling, false);

							if (sibling->parent == nullptr)
							{
								freeMemory = sibling;
							}
						}
						else
						{
							// Right left.
							// First rotation.
							FreeHeader* thing = sibling->left;
							sibling->left = thing->right;
							if (sibling->left)
							{
								sibling->left->parent = setRedness(sibling, checkRedness(sibling->left->parent));
							}
							thing->right = sibling;
							thing->parent = setRedness(dbParent, false);
							(dbParent->left == sibling ? dbParent->left : dbParent->right) = thing;
							sibling->parent = setRedness(thing, false);

							// Second rotation.
							thing->parent = dbParent->parent;
							dbParentInParent = thing;
							dbParent->parent = setRedness(thing, false);
							dbParent->right = thing->left;
							if (dbParent->right)
							{
								dbParent->right->parent = setRedness(dbParent, checkRedness(dbParent->right->parent));
							}
							thing->left = dbParent;

							if (thing->parent == nullptr)
							{
								freeMemory = thing;
							}
						}
					}

					// Restructurization done, no two consecutive reds principle restored.
					break;
				}
				else
				{
					// Both are black.
					if (sibling)
					{
						sibling->parent = setRedness(sibling->parent, true);
					}
					if (checkRedness(dbParent->parent))
					{
						// We can color red parent to black, and therefore, stop the recursion.
						dbParent->parent = setRedness(dbParent->parent, false);
						break;
					}
					doubleBlack = dbParent;
					dbParent = (FreeHeader*)cleanAddress(doubleBlack->parent);
				}
			}
			else
			{
				FreeHeader*& dbParentInParent = cleanAddress(dbParent->parent) ? ((FreeHeader*)cleanAddress(dbParent->parent))->left == dbParent ? ((FreeHeader*)cleanAddress(dbParent->parent))->left : ((FreeHeader*)cleanAddress(dbParent->parent))->right : tempGarbage;

				// Sibling is red.
				if (((FreeHeader*)cleanAddress(sibling->parent))->right == sibling)
				{
					// Right rotate.
					dbParent->right = sibling->left;
					if (sibling->left)
					{
						sibling->left->parent = setRedness(dbParent, checkRedness(sibling->left->parent));
					}
					sibling->left = dbParent;
					const bool tempRed = checkRedness(sibling->parent);
					sibling->parent = dbParent->parent;
					dbParent->parent = setRedness(sibling, tempRed);
					dbParentInParent = sibling;
					sibling->right->parent = setRedness(sibling, false);
				}
				else
				{
					// Left rotate.
					dbParent->left = sibling->right;
					if (sibling->right)
					{
						sibling->right->parent = setRedness(dbParent, checkRedness(sibling->right->parent));
					}
					sibling->right = dbParent;
					const bool tempRed = checkRedness(sibling->parent);
					sibling->parent = dbParent->parent;
					dbParent->parent = setRedness(sibling, tempRed);
					dbParentInParent = sibling;
					sibling->left->parent = setRedness(sibling, false);
				}

				if (sibling->parent == nullptr)
				{
					freeMemory = sibling;
				}
			}
		}
	}
}

void GLOWE::RBTMemoryAllocator::insertToRBTree(FreeHeader * block)
{
	if (freeMemory == nullptr)
	{
		freeMemory = block;
		freeMemory->parent = setRedness(nullptr, false);
		return;
	}

	FreeHeader* ptr = freeMemory;
	const SizeType blockSize = calcSize(block);

	while (true)
	{
		if (blockSize < calcSize(ptr))
		{
			if (ptr->left)
			{
				ptr = ptr->left;
			}
			else
			{
				ptr->left = block;
				block->parent = setRedness(ptr, true);
				ptr = block;
				break;
			}
		}
		else
		{
			if (ptr->right)
			{
				ptr = ptr->right;
			}
			else
			{
				ptr->right = block;
				block->parent = setRedness(ptr, true);
				ptr = block;
				break;
			}
		}
	}

	while (true)
	{
		if (ptr == freeMemory)
		{
			ptr->parent = setRedness(ptr->parent, false);
		}
		else
			if (getRed((FreeHeader*)cleanAddress(ptr->parent)))
			{
				// Recolor the tree.
				FreeHeader* parent = (FreeHeader*)cleanAddress(ptr->parent), *grandparent = (FreeHeader*)cleanAddress(parent->parent);

				if (!grandparent)
				{
					break;
				}

				FreeHeader* uncle = grandparent->left == parent ? grandparent->right : grandparent->left;

				if (getRed(uncle))
				{
					parent->parent = setRedness(parent->parent, false);
					uncle->parent = setRedness(uncle->parent, false);
					grandparent->parent = setRedness(grandparent->parent, true);

					ptr = grandparent;
					continue;
				}
				else
				{
					auto leftLeft = [this](FreeHeader* __restrict ptr, FreeHeader* __restrict parent, FreeHeader* __restrict grandparent)
					{
						grandparent->left = parent->right;
						if (grandparent->left)
						{
							grandparent->left->parent = setRedness(grandparent, checkRedness(grandparent->left->parent));
						}

						const bool tempRed = checkRedness(parent->parent);
						parent->parent = grandparent->parent;
						if (cleanAddress(parent->parent))
						{
							(((FreeHeader*)cleanAddress(parent->parent))->left == grandparent ? ((FreeHeader*)cleanAddress(parent->parent))->left : ((FreeHeader*)cleanAddress(parent->parent))->right) = parent;
						}
						else
						{
							freeMemory = parent;
						}
						grandparent->parent = setRedness(parent, tempRed);

						parent->right = grandparent;
					};
					auto rightRight = [this](FreeHeader* __restrict ptr, FreeHeader* __restrict parent, FreeHeader* __restrict grandparent)
					{
						grandparent->right = parent->left;
						if (grandparent->right)
						{
							grandparent->right->parent = setRedness(grandparent, checkRedness(grandparent->right->parent));
						}

						const bool tempRed = checkRedness(parent->parent);
						parent->parent = grandparent->parent;
						if (cleanAddress(parent->parent))
						{
							(((FreeHeader*)cleanAddress(parent->parent))->left == grandparent ? ((FreeHeader*)cleanAddress(parent->parent))->left : ((FreeHeader*)cleanAddress(parent->parent))->right) = parent;
						}
						else
						{
							freeMemory = parent;
						}
						grandparent->parent = setRedness(parent, tempRed);

						parent->left = grandparent;
					};

					if (grandparent && parent == grandparent->left)
					{
						if (ptr == parent->left)
						{
							// Left left.
							leftLeft(ptr, parent, grandparent);
						}
						else
						{
							// Left right.
							parent->right = ptr->left;
							if (parent->right)
							{
								parent->right->parent = setRedness(parent, checkRedness(parent->right->parent));
							}
							ptr->left = parent;
							ptr->parent = setRedness(grandparent, checkRedness(ptr->parent));
							parent->parent = setRedness(ptr, checkRedness(parent->parent));
							(grandparent->left == parent ? grandparent->left : grandparent->right) = ptr;
							leftLeft(parent, ptr, grandparent);
						}
					}
					else
					{
						if (ptr == parent->left)
						{
							// Right left.
							parent->left = ptr->right;
							if (parent->left)
							{
								parent->left->parent = setRedness(parent, checkRedness(parent->left->parent));
							}
							ptr->right = parent;
							ptr->parent = setRedness(grandparent, checkRedness(ptr->parent));
							parent->parent = setRedness(ptr, checkRedness(parent->parent));
							if (grandparent)
							{
								(grandparent->left == parent ? grandparent->left : grandparent->right) = ptr;
							}
							rightRight(parent, ptr, grandparent);
						}
						else
						{
							// Right right.
							rightRight(ptr, parent, grandparent);
						}
					}

					// Restructurization complete.
				}
			}
		break;
	}
}

inline bool GLOWE::RBTMemoryAllocator::isBlockFitting(FreeHeader * block, const SizeType reqSize, const SizeType alignment, FittingBlockData& outputData) const
{
	// Assumption: two free blocks cannot be next to each other.
	if (!block)
	{
		return false;
	}
	void* ptr = addPointers(block, sizeof(ClaimedHeader));
	SizeType space = calcSize(block) - sizeof(ClaimedHeader);
	const SizeType startingSpace = space;
	FittingBlockData& result = outputData;

	const SizeType requiredSize = ((reqSize % MostRestrictiveAlignment) == 0) ? reqSize : reqSize + (MostRestrictiveAlignment - (reqSize % MostRestrictiveAlignment));

	// We don't check if it's possible to attach any remaining memory to existing block. Also we don't check if it's possible to create new free block out of remaining memory.
	// That's because remaining memory, assuming we're unable to allocate new free blocks out of it, is attached to already existing claimed blocks.
	// There is only one case to check - whether prev is nullptr.
	if (block->prev == nullptr)
	{
		if (space >= sizeof(FreeHeader))
		{
			space -= sizeof(FreeHeader);
			ptr = addPointers(ptr, sizeof(FreeHeader));
		}
		else
		{
			return false;
		}
	}

	if (!std::align(alignment, requiredSize, ptr, space))
	{
		return false;
	}

	result.adjustment = ((SizeType)subPointers(ptr, block)) - sizeof(ClaimedHeader);
	result.usableMemory = (ClaimedHeader*)subPointers(ptr, sizeof(ClaimedHeader));
	result.remainingMemory = startingSpace - result.adjustment - requiredSize;
	result.usedMemory = requiredSize;

	return true;
}

GLOWE::RBTMemoryAllocator::FreeHeader * GLOWE::RBTMemoryAllocator::findFittingBlock(const SizeType size, const SizeType alignment, FittingBlockData& outputFittingBlock) const
{
	FreeHeader* searchNode = freeMemory, *chosenBlock = nullptr;
	FittingBlockData isFittingData, &chosenData = outputFittingBlock;
	chosenData.remainingMemory = (~0ULL);

	if (isBlockFitting(freeMemory, size, alignment, isFittingData))
	{
		chosenBlock = freeMemory;
		chosenData = isFittingData;
	}

	while (searchNode && (searchNode->left || searchNode->right))
	{
		if (isBlockFitting(searchNode->left, size, alignment, isFittingData))
		{
			chosenData = isFittingData;
			searchNode = chosenBlock = searchNode->left;
		}
		else
		if (isBlockFitting(searchNode->right, size, alignment, isFittingData))
		{
			chosenData = isFittingData;
			searchNode = chosenBlock = searchNode->right;
		}
		else
		{
			searchNode = searchNode->right;
		}
	}

	return chosenBlock;
}

unsigned int GLOWE::RBTMemoryAllocator::dbgCheckFreeTreeSanity() const
{
	unsigned int e = 0, count = 0;

	// Used to be static.
	Function<void(FreeHeader*, const Function<void(FreeHeader*)>&)> traverseBlock = [&e, &traverseBlock](FreeHeader* block, const Function<void(FreeHeader*)>& func)
	{
		if (!block)
		{
			return;
		}

		if (block->left == block)
		{
			e = 1;
			throw 0;
		}
		if (block->right == block)
		{
			e = 2;
			throw 0;
		}
		traverseBlock(block->left, func);
		func(block);
		traverseBlock(block->right, func);
	};

	if (getRed(freeMemory))
	{
		return 7;
	}

	if (freeMemory && (freeMemory->parent != nullptr))
	{
		return 8;
	}

	try
	{
		traverseBlock(freeMemory, [this, &e, &count](FreeHeader* block)
		{
			if (block->left)
			{
				if (cleanAddress(block->left->parent) != block)
				{
					e = 3;
					throw 0;
				}
			}

			if (block->right)
			{
				if (cleanAddress(block->right->parent) != block)
				{
					e = 4;
					throw 0;
				}
			}

			if (checkRedness(block->parent))
			{
				if (getRed(block->left) || getRed(block->right))
				{
					e = 5;
					throw 0;
				}
			}

			if ((block->left && !(block->right)) || (block->right && !(block->left)) || (!(block->left) && !(block->right)))
			{
				if (count != 0)
				{
					if (dbgGetBlackHeight(block) != count)
					{
						std::cout << "block's height: " << dbgGetBlackHeight(block) << " count: " << count << std::endl;
						e = 6;
						throw 0;
					}
				}
				else
				{
					count = dbgGetBlackHeight(block);
				}
			}
		});
	}
	catch (...)
	{
	}

	return e;
}

unsigned int GLOWE::RBTMemoryAllocator::dbgGetBlackHeight(FreeHeader * const head) const
{
	FreeHeader* search = head;
	unsigned int counter = 1;

	while (search->parent)
	{
		if (!checkRedness(search->parent))
		{
			++counter;
		}
		search = (FreeHeader*)cleanAddress(search->parent);
	}

	return counter;
}

void GLOWE::RBTMemoryAllocator::dbgWriteAllBlocks() const
{
	ClaimedHeader* search = trueMemoryBegin;

	std::cout << "Blocks dump:" << std::endl;
	std::cout << "FreeHeader: " << calcSize(search) << std::endl;
	search = cleanAddress(search->next);

	for (; search != cleanAddress(endOfMemory); search = cleanAddress(search->next))
	{
		if (isFreeHeader(cleanAddress(search->prev)->next))
		{
			std::cout << "FreeHeader: ";
		}
		else
		{
			std::cout << "ClaimedHeader: ";
		}
		std::cout << calcSize(search) << std::endl;
	}

	std::cout << std::endl;
}

bool GLOWE::RBTMemoryAllocator::dbgCheckListIntegrity() const
{
	ClaimedHeader* search = cleanAddress(trueMemoryBegin);

	while (search->next != endOfMemory)
	{
		if (cleanAddress(cleanAddress(search->next)->prev) != search)
		{
			return false;
		}
		search = cleanAddress(search->next);
	}

	return true;
}

GLOWE::RBTMemoryAllocator::RBTMemoryAllocator(const SizeType memorySize)
	: GLOWE::RBTMemoryAllocator(operator new(memorySize + MostRestrictiveAlignment), memorySize + MostRestrictiveAlignment, true)
{
}

GLOWE::RBTMemoryAllocator::RBTMemoryAllocator(void* memoryToUse, const SizeType memorySize, const bool isOwning)
	: memory(memoryToUse), endOfMemory((ClaimedHeader*)addPointers(memoryToUse, memorySize)), totalMemory(0), freeMemory(nullptr), usedMemory(0), allocations(0), isOwningMemory(isOwning), trueMemoryBegin(nullptr)
{
	SizeType tempSize = memorySize;
	void* temp = memoryToUse;
	if (std::align(MostRestrictiveAlignment, memorySize - MostRestrictiveAlignment, temp, tempSize))
	{
		freeMemory = new (temp) FreeHeader;
		freeMemory->left = nullptr;
		freeMemory->right = nullptr;
		freeMemory->parent = nullptr;
		freeMemory->next = endOfMemory;
		freeMemory->prev = nullptr;
		trueMemoryBegin = freeMemory;
		totalMemory = calcSize(freeMemory);
	}
	else
	{
		throw std::bad_alloc();
	}
}

GLOWE::RBTMemoryAllocator::~RBTMemoryAllocator()
{
#ifdef RBMEM_LEAKERROR
	if (allocations > 0)
	{
		std::cout << "Memory leak detected. Allocations: " << allocations << std::endl;
		throw Exception("Memory leak detected."); // Will call std::terminate.
	}
#endif

	if (isOwningMemory)
	{
		operator delete(memory);
	}
}

void * GLOWE::RBTMemoryAllocator::allocate(const SizeType howMany, const SizeType alignment)
{
	// Assumption: two free blocks cannot be next to each other.

#ifdef RBMEM_CHECKSANITY
	if (!dbgCheckListIntegrity())
	{
		throw 8;
	}
#endif

	FittingBlockData fittingBlockData;
	FreeHeader* fittingBlock = findFittingBlock((howMany >= sizeof(FreeHeader) - sizeof(ClaimedHeader) ? howMany : sizeof(FreeHeader) - sizeof(ClaimedHeader)), alignment, fittingBlockData);

	if (!fittingBlock)
	{
		return nullptr;
	}

	removeFromRBTree(fittingBlock);
#ifdef RBMEM_CHECKSANITY
	dbgCheckSanity();
#endif

	ClaimedHeader headInfo = *fittingBlock;
	ClaimedHeader* const claimedBlock = new (fittingBlockData.usableMemory) ClaimedHeader;

	// Check the adjustment and remaining memory size.
	const SizeType adjustment = fittingBlockData.adjustment, remainingMemory = fittingBlockData.remainingMemory;

	// First we take care of back memory.
	if (remainingMemory >= sizeof(FreeHeader))
	{
		// Create new free block.
		//FreeHeader* newBlock = new (subPointers(cleanAddress(headInfo.next), remainingMemory)) FreeHeader;
		FreeHeader* newBlock = new (addPointers(claimedBlock, sizeof(ClaimedHeader) + fittingBlockData.usedMemory)) FreeHeader;
		newBlock->next = headInfo.next;
		if (headInfo.next != endOfMemory)
		{
			cleanAddress(headInfo.next)->prev = setIsFreeHeader(newBlock, true);
		}
		newBlock->prev = setIsFreeHeader(claimedBlock, false);
		claimedBlock->next = setIsFreeHeader(newBlock, true);
		insertToRBTree(newBlock);
#ifdef RBMEM_CHECKSANITY
		dbgCheckSanity();
#endif
	}
	else
	{
		// Add the remaining memory to the claimed block.
		claimedBlock->next = headInfo.next;
		if (headInfo.next != endOfMemory)
		{
			cleanAddress(headInfo.next)->prev = setIsFreeHeader(claimedBlock, false);
		}
	}

	// Take care of forward memory.
	if (adjustment >= sizeof(FreeHeader))
	{
		// Create new free block.
		FreeHeader* newBlock = new (fittingBlock) FreeHeader;
		newBlock->next = setIsFreeHeader(claimedBlock, false);
		newBlock->prev = headInfo.prev;
		claimedBlock->prev = setIsFreeHeader(newBlock, true);
		if (headInfo.prev)
		{
			cleanAddress(headInfo.prev)->next = setIsFreeHeader(newBlock, true);
		}
		insertToRBTree(newBlock);
#ifdef RBMEM_CHECKSANITY
		dbgCheckSanity();
#endif
	}
	else
	{
		// Add the remaining memory to previous block.
		claimedBlock->prev = headInfo.prev;
		if (headInfo.prev)
		{
			cleanAddress(headInfo.prev)->next = setIsFreeHeader(claimedBlock, false);
		}
	}

	++allocations;
	usedMemory += calcSize(claimedBlock);

#ifdef RBMEM_CHECKSANITY
	if (!dbgCheckListIntegrity())
	{
		throw 8;
	}
#endif

	//dbgWriteAllBlocks();
	return addPointers(claimedBlock, sizeof(ClaimedHeader));
}

void GLOWE::RBTMemoryAllocator::deallocate(void* ptr)
{
	if (!ptr)
	{
		return;
	}

#ifdef RBMEM_CHECKSANITY
	if (!dbgCheckListIntegrity())
	{
		throw 8;
	}
#endif

	ClaimedHeader* claimedBlock = (ClaimedHeader*)subPointers(ptr, sizeof(ClaimedHeader));
	ClaimedHeader headInfo = *claimedBlock;
	usedMemory -= calcSize(claimedBlock);

	FreeHeader* newBlock = (FreeHeader*)claimedBlock;

	// Merge adjacent blocks if possible and add the new block to the RB tree.
	// Prevoius block (the one on left).
	if (isFreeHeader(headInfo.prev) && cleanAddress(headInfo.prev) != nullptr) // If it doesn't work then try adding "&& cleanAddress(headInfo.prev) != nullptr".
	{
		newBlock = (FreeHeader*)cleanAddress(headInfo.prev);
		removeFromRBTree(newBlock);
#ifdef RBMEM_CHECKSANITY
		dbgCheckSanity();
#endif
		newBlock->next = headInfo.next;
	}

	// Next block (the one on right).
	if (isFreeHeader(headInfo.next) && headInfo.next != endOfMemory)
	{
		FreeHeader* tempHead = (FreeHeader*)cleanAddress(headInfo.next);
		removeFromRBTree(tempHead);
#ifdef RBMEM_CHECKSANITY
		dbgCheckSanity();
#endif
		newBlock->next = cleanAddress(headInfo.next)->next;
	}

	if (newBlock->prev)
	{
		cleanAddress(newBlock->prev)->next = setIsFreeHeader(newBlock, true);
	}
	if (newBlock->next != endOfMemory)
	{
		cleanAddress(newBlock->next)->prev = setIsFreeHeader(newBlock, true);
	}
	// Merging complete.

	newBlock->left = newBlock->right = newBlock->parent = nullptr;
	insertToRBTree(newBlock);
#ifdef RBMEM_CHECKSANITY
	dbgCheckSanity();
#endif

#ifdef RBMEM_CHECKSANITY
	if (!dbgCheckListIntegrity())
	{
		throw 8;
	}
#endif

	--allocations;
}

void GLOWE::_GlobalAllocate::createNewAllocator()
{
	allocators.push_back(new GLOWE::RBTMemoryAllocator(newBlockSize *= 2));
}

void GLOWE::_GlobalAllocate::createNewPool()
{
	poolAllocators.push_back(new PoolAllocator(this));
}

GLOWE::_GlobalAllocate::_GlobalAllocate()
	: allocators(), poolAllocators(), allocations(0), newBlockSize(DefaultBlockSize / 2)
{
}

GLOWE::_GlobalAllocate::_GlobalAllocate(_GlobalAllocate && arg) noexcept
	: newBlockSize(exchange(arg.newBlockSize, 0ULL)), allocations(exchange(arg.allocations, 0U)), allocators(std::move(arg.allocators)), poolAllocators(std::move(arg.poolAllocators))
{
	for (auto& x : poolAllocators)
	{
		x->memoryOwner = this;
	}
}

GLOWE::_GlobalAllocate::~_GlobalAllocate()
{
	for (auto& x : poolAllocators)
	{
		delete x;
	}

	for (auto& x : allocators)
	{
		delete x;
	}
}

void* GLOWE::_GlobalAllocate::allocateSpace(const size_t size)
{
	void* temp;

	if (size <= DefaultPoolBlockSize)
	{
		for (auto& x : poolAllocators)
		{
			temp = x->allocate();
			if (temp)
			{
				++allocations;
#ifdef GLOWE_ENABLE_LEAK_DETECTION
				getInstance<MemoryLeakDetector>().addElement(temp, typeid(void), size);
#endif
				return temp;
			}
		}

		createNewPool();
	}
	else
	{
		for (auto& x : allocators)
		{
			temp = x->allocate(size);
			if (temp != nullptr)
			{
				++allocations;
#ifdef GLOWE_ENABLE_LEAK_DETECTION
				getInstance<MemoryLeakDetector>().addElement(temp, typeid(void), size);
#endif
				return temp;
			}
		}

		createNewAllocator();
	}

	return this->allocateSpace(size);
}

void* GLOWE::_GlobalAllocate::allocateSpaceWithAlignment(const size_t size, const size_t adjustment)
{
	void* temp;

	if (size <= DefaultPoolBlockSize && adjustment <= DefaultPoolAlignment)
	{
		for (auto& x : poolAllocators)
		{
			temp = x->allocate();
			if (temp)
			{
				++allocations;
#ifdef GLOWE_ENABLE_LEAK_DETECTION
				getInstance<MemoryLeakDetector>().addElement(temp, typeid(void), size);
#endif
				return temp;
			}
		}

		createNewPool();
	}
	else
	{
		for (auto& x : allocators)
		{
			temp = x->allocate(size, adjustment);
			if (temp != nullptr)
			{
				++allocations;
#ifdef GLOWE_ENABLE_LEAK_DETECTION
				getInstance<MemoryLeakDetector>().addElement(temp, typeid(void), size);
#endif
				return temp;
			}
		}

		createNewAllocator();
	}

	return this->allocateSpaceWithAlignment(size, adjustment);
}

void GLOWE::_GlobalAllocate::_deallocate(void* memHandle)
{
	for (auto& x : poolAllocators)
	{
		if (x->isPointerInMemoryRange(memHandle))
		{
			x->deallocate(memHandle);
			--allocations;
			return;
		}
	}

	for (auto x : allocators)
	{
		if (x->isPointerInMemoryRange(memHandle))
		{
			x->deallocate(memHandle);
			--allocations;
			return;
		}
	}
}

void GLOWE::_GlobalAllocate::_deallocateOnlyRBTAllocators(void * memHandle)
{
	for (auto x : allocators)
	{
		if (x->isPointerInMemoryRange(memHandle))
		{
			x->deallocate(memHandle);
			--allocations;
			return;
		}
	}
}

void GLOWE::_GlobalAllocate::freeUnusedAllocators()
{
	unsigned int removed = 1;

	for (auto it = allocators.begin(); it != allocators.end(); )
	{
		if ((*it)->getAllocationsCount() == 0)
		{
			delete (*it);
			it = allocators.erase(it);
			removed <<= 1;
		}
		else
		{
			++it;
		}
	}

	newBlockSize /= removed;
}

void GLOWE::_GlobalAllocate::increaseAllocatorsCount(const unsigned int newCount)
{
	while (allocators.size() < newCount)
	{
		createNewAllocator();
	}
}

bool GLOWE::_GlobalAllocate::isPointerInMemoryRange(const void* ptr) const
{
	for (const auto& x : allocators)
	{
		if (x->isPointerInMemoryRange(ptr))
		{
			return true;
		}
	}

	return false;
}

unsigned int GLOWE::_GlobalAllocate::getAllocationsCount() const
{
	return allocations - poolAllocators.size();
}

bool GLOWE::_GlobalAllocate::checkSanity() const
{
	try
	{
		for (const auto& x : allocators)
		{
			x->dbgCheckSanity();
		}
	}
	catch (...)
	{
		return false;
	}

	return true;
}

void GLOWE::GlobalAllocator::collectTrash()
{
	if (pointersToDeallocate.size() >= whenToCleanMemoryTrash)
	{
		collectTrashUnconditionally();
	}
}

void GLOWE::GlobalAllocator::collectTrashUnconditionally()
{
	DefaultLockGuard lock(pointersToDeallocMutex);
	for (const auto x : pointersToDeallocate)
	{
		_deallocate(x);
	}

	pointersToDeallocate.clear();
}

GLOWE::GlobalAllocator::GlobalAllocator()
	: allocator(), pointersToDeallocate(), overseer(&getInstance<GlobalAllocatorMgr>()), creatorsThreadID(std::this_thread::get_id()), blockRemovalTick(0)
{
	pointersToDeallocate.reserve(64);
}

GLOWE::GlobalAllocator::GlobalAllocator(GlobalAllocator && arg) noexcept
	: creatorsThreadID(GLOWE::exchange(arg.creatorsThreadID, std::thread::id())), pointersToDeallocate(std::move(arg.pointersToDeallocate)), allocator(std::move(arg.allocator)), overseer(GLOWE::exchange(arg.overseer, nullptr)), blockRemovalTick(0)
{
}

void * GLOWE::GlobalAllocator::allocateSpace(const size_t size)
{
	collectTrash();
#ifdef GLOWE_ENABLE_MEMORY_SIZE_USAGES_COUNTER
	getInstance<MemoryProfiler>().notifyUsage(size);
#endif
	return allocator.allocateSpace(size);
}

void * GLOWE::GlobalAllocator::allocateSpaceWithAlignment(const size_t size, const size_t adjustment)
{
	collectTrash();
#ifdef GLOWE_ENABLE_MEMORY_SIZE_USAGES_COUNTER
	getInstance<MemoryProfiler>().notifyUsage(size);
#endif
	return allocator.allocateSpaceWithAlignment(size, adjustment);
}

void GLOWE::GlobalAllocator::addPointerToDeallocate(void* arg)
{
#ifdef GLOWE_ENABLE_LEAK_DETECTION
	getInstance<MemoryLeakDetector>().checkIfDeletable(arg);
#endif
	DefaultLockGuard lock(pointersToDeallocMutex);
	pointersToDeallocate.push_back(arg);
}

bool GLOWE::GlobalAllocator::isPointerInMemoryRange(const void* arg) const
{
	return allocator.isPointerInMemoryRange(arg);
}

void GLOWE::GlobalAllocator::markAsFreed()
{
	overseer->markAllocatorAsFreed(creatorsThreadID);
}

bool GLOWE::GlobalAllocator::checkSanity() const
{
	return allocator.checkSanity();
}

unsigned int GLOWE::GlobalAllocator::getAllocationsCount() const
{
	return allocator.getAllocationsCount();
}

inline size_t alignForwardAdjustment(const void* address, size_t alignment)
{
	size_t adjustment = alignment - (reinterpret_cast<uintptr_t>(address) & static_cast<uintptr_t>(alignment - 1));

	if (adjustment == alignment)
		return 0; //already aligned

	return adjustment;
}

GLOWE::PoolAllocator::PoolAllocator(_GlobalAllocate* creator)
	: mem(creator->allocateSpaceWithAlignment(DefaultPoolBlockSize * DefaultPoolSize, DefaultPoolAlignment)), freeList((void**)mem), size(DefaultPoolBlockSize * DefaultPoolSize), memoryOwner(creator), endOfMem(addPointers(mem, size)), allocations(0)
{
	void** p = freeList;

	//Initialize free blocks list
	for (size_t i = 0; i < DefaultPoolSize - 1; ++i)
	{
		*p = addPointers(p, DefaultPoolBlockSize);
		p = (void**)*p;
	}

	*p = nullptr;
}

GLOWE::PoolAllocator::~PoolAllocator()
{
	memoryOwner->_deallocateOnlyRBTAllocators(mem);
}

void* GLOWE::PoolAllocator::allocate()
{
	if (freeList == nullptr)
		return nullptr;

	void* p = freeList;

	freeList = (void**)(*freeList);
	++allocations;

	return p;
}

void GLOWE::PoolAllocator::deallocate(void* memPtr)
{
	if (memPtr)
	{
		*((void**)memPtr) = freeList;

		freeList = (void**)memPtr;

		--allocations;
	}
}

bool GLOWE::PoolAllocator::isPointerInMemoryRange(const void * arg) const
{
	return arg >= mem && arg < endOfMem;
}

GLOWE::VectorAllocator::VectorAllocator()
	: freeList(nullptr), mem(getInstance<GlobalAllocator>().allocateSpaceWithAlignment(DefaultVectorPoolBlockSize*DefaultVectorPoolSize, alignof(__m128)))
{
	static_assert(alignof(__m128) == 16, "alignof(__m128) must be 16");

	uint8_t adjustment = alignForwardAdjustment(mem, alignof(__m128));

	freeList = (void**)addPointers(mem, adjustment);

	size_t numObjects = (DefaultPoolBlockSize*DefaultPoolSize - adjustment) / DefaultPoolBlockSize;

	void** p = freeList;

	//Initialize free blocks list
	for (size_t i = 0; i < numObjects - 1; ++i)
	{
		*p = addPointers(p, DefaultVectorPoolBlockSize);
		p = (void**)*p;
	}

	*p = nullptr;
}

GLOWE::VectorAllocator::~VectorAllocator()
{
	getInstance<GlobalAllocator>().deallocate(mem);
}

void* GLOWE::VectorAllocator::allocate()
{
	if (freeList == nullptr)
		return nullptr;

	void* p = freeList;

	freeList = (void**)(*freeList);

	return p;
}

void GLOWE::VectorAllocator::deallocate(void* memPtr)
{
	*((void**)memPtr) = freeList;

	freeList = (void**)memPtr;
}

void GLOWE::MemoryProfiler::notifyUsage(const UInt64 size)
{
	std::lock_guard<std::mutex> guard(mutex);
	++memorySizesUsage[size];
}

void GLOWE::MemoryProfiler::writeAllSizeUsages() const
{
	std::map<UInt64, UInt64> sortedUsages;
	{
		std::lock_guard<std::mutex> guard(mutex);

		for (const auto& x : memorySizesUsage)
		{
			sortedUsages.emplace(x.second, x.first);
		}
	}

#ifdef _MSC_VER
	OutputDebugStringA(u8"Memory sizes requested (sorted by amount):\n");
#else
	std::clog << u8"Memory sizes requested (sorted by amount):\n";
#endif

	for (const auto& x : sortedUsages)
	{
#ifdef _MSC_VER
		OutputDebugStringA((toString(x.second) + "B requested " + toString(x.first) + " times.\n").getCharArray());
#else
		std::clog << (toString(x.second) + "B requested " + toString(x.first) + " times.\n").getCharArray();
#endif
	}
}

void GLOWE::MemoryProfiler::writeSizeUsages()
{
	getInstance<MemoryProfiler>().writeAllSizeUsages();
}

GLOWE::Map<std::thread::id, GLOWE::Vector<GLOWE::Pair<size_t, size_t>>> GLOWE::MemoryProfiler::getAllMemoryUsages()
{
	Map<std::thread::id, Vector<Pair<size_t, size_t>>> result;
	GlobalAllocatorMgr& alloc = getInstance<GlobalAllocatorMgr>();
	RWReadLock<RecursiveMutex> lock(alloc.mutex);

	for (const auto& x : alloc.occupiedAllocators)
	{
		Vector<Pair<size_t, size_t>>& vec = (*result.emplace(x.first, Vector<Pair<size_t, size_t>>()).first).second;
		vec.reserve(x.second.allocator.allocators.size());
	}
	{
		Vector<Pair<size_t, size_t>>& vec = (*result.emplace(std::thread::id(), Vector<Pair<size_t, size_t>>()).first).second;
		vec.reserve(alloc.freedAllocators.size());
	}

	for (const auto& x : alloc.occupiedAllocators)
	{
		Vector<Pair<size_t, size_t>>& vec = result.at(x.first);
		for (const auto& y : x.second.allocator.allocators)
		{
			vec.emplace_back(y->getUsedMemory(), y->getTotalMemory());
		}
	}

	Vector<Pair<size_t, size_t>>& vec = result.at(std::thread::id());
	for (const auto& x : alloc.freedAllocators)
	{
		for (const auto& y : x.allocator.allocators)
		{
			vec.emplace_back(y->getUsedMemory(), y->getTotalMemory());
		}
	}

	return result;
}

GLOWE::String GLOWE::MemoryProfiler::getAllMemoryUsagesAsString()
{
	OStringStream result;

	writeAllMemoryUsagesToStream(result);

	return result.str();
}

void GLOWE::MemoryProfiler::writeAllMemoryUsagesToStream(std::ostream & os)
{
	Map<std::thread::id, Vector<Pair<size_t, size_t>>> map(getAllMemoryUsages());

	for (const auto& x : map)
	{
		os << x.first << ":\n";
		for (const auto& y : x.second)
		{
			os << '\t' << y.first << " / " << y.second << " bytes\n";
		}
	}
}

GLOWE::GlobalAllocator & GLOWE::GlobalAllocatorMgr::getAllocator(const std::thread::id & threadID)
{
	ReadWriteLock<RecursiveMutex> lock(mutex);
	lock.lockRead();
	if (occupiedAllocators.count(threadID) == 0)
	{
		lock.unlock();
		lock.lockWrite();
		if (occupiedAllocators.empty())
		{
			mainThreadId = threadID;
		}
		GlobalAllocator* result;
		if (freedAllocators.empty())
		{
			result = &(*occupiedAllocators.emplace(std::piecewise_construct, std::forward_as_tuple(threadID), std::forward_as_tuple()).first).second;
		}
		else
		{
			result = &(*occupiedAllocators.emplace(std::piecewise_construct, std::forward_as_tuple(threadID), std::forward_as_tuple(std::move(freedAllocators.front()))).first).second;
			freedAllocators.pop_front();
		}
		return *result;
	}
	else
	{
		return occupiedAllocators.at(threadID);
	}
}

void GLOWE::GlobalAllocatorMgr::markAllocatorAsFreed(const std::thread::id & alloc)
{
	ReadWriteLock<RecursiveMutex> lock(mutex);
	lock.lockRead();
	const std::map<std::thread::id, GlobalAllocator>::iterator it = occupiedAllocators.find(alloc);
	if (it != occupiedAllocators.end())
	{
		lock.unlock();
		lock.lockWrite();
		if (alloc == mainThreadId)
		{
			isDuringCleanup = true;
		}

		if (it->second.getAllocationsCount() != 0)
		{
			freedAllocators.push_back(std::move(it->second));
		}
		occupiedAllocators.erase(it);
	}
}

unsigned int GLOWE::GlobalAllocatorMgr::getUsedAllocatorsCount() const
{
	RWReadLock<RecursiveMutex> lock(mutex);
	return occupiedAllocators.size();
}

unsigned int GLOWE::GlobalAllocatorMgr::getFreedAllocatorsCount() const
{
	RWReadLock<RecursiveMutex> lock(mutex);
	return freedAllocators.size();
}

int GLOWE::GlobalAllocatorMgr::getUnloadOrder() const
{
	return std::numeric_limits<int>::min();
}

bool GLOWE::GlobalAllocatorMgr::checkSanity() const
{
	RWReadLock<RecursiveMutex> lock(mutex);
	for (const auto& x : occupiedAllocators)
	{
		if (!x.second.checkSanity())
		{
			return false;
		}
	}

	for (const auto& x : freedAllocators)
	{
		if (!x.checkSanity())
		{
			return false;
		}
	}

	return true;
}

template<>
void GLOWE::GlobalAllocatorMgr::deallocate(void* ptr)
{
	ReadWriteLock<RecursiveMutex> lock(mutex);
	lock.lockRead();
	for (auto& x : occupiedAllocators)
	{
		if (x.second.isPointerInMemoryRange(ptr))
		{
			x.second.addPointerToDeallocate(ptr);
			return;
		}
	}

	for (std::list<GlobalAllocator>::iterator it = freedAllocators.begin(), end = freedAllocators.end(); it != end; ++it)
	{
		if (it->_deallocate(ptr))
		{
			if (it->getAllocationsCount() == 0)
			{
				lock.unlock();
				lock.lockWrite();
				freedAllocators.erase(it);
			}
			return;
		}
	}
}

bool GLOWE::GlobalAllocatorMgr::checkIsDuringCleanup() const
{
	return isDuringCleanup;
}

GLOWE::FrameAllocator::FrameAllocator(const bool clearAtFrameEnd)
	: memory(getInstance<GlobalAllocator>().allocateSpace(defaultBlockSize)), memEnd(static_cast<char*>(memory) + defaultBlockSize), currMemPos(memory), isOwningMemory(true), shouldBeClearedAtFrameEnd(clearAtFrameEnd)
{
	if (clearAtFrameEnd)
	{
		getInstance<FrameAllocatorMgr>().registerAllocator(this);
	}
}

GLOWE::FrameAllocator::FrameAllocator(const UInt64 size, const bool clearAtFrameEnd)
	: memory(getInstance<GlobalAllocator>().allocateSpace(size > 0 ? size : defaultBlockSize)), memEnd(static_cast<char*>(memory) + (size > 0 ? size : defaultBlockSize)), currMemPos(memory), isOwningMemory(true), shouldBeClearedAtFrameEnd(clearAtFrameEnd)
{
	if (clearAtFrameEnd)
	{
		getInstance<FrameAllocatorMgr>().registerAllocator(this);
	}
}

GLOWE::FrameAllocator::FrameAllocator(void* const newMemory, const UInt64 size, const bool clearAtFrameEnd)
	: memory(newMemory), memEnd(static_cast<char*>(memory) + size), currMemPos(memory), isOwningMemory(false), shouldBeClearedAtFrameEnd(clearAtFrameEnd)
{
	if (clearAtFrameEnd)
	{
		getInstance<FrameAllocatorMgr>().registerAllocator(this);
	}
}

GLOWE::FrameAllocator::~FrameAllocator()
{
	if (shouldBeClearedAtFrameEnd)
	{
		getInstance<FrameAllocatorMgr>().unregisterAllocator(this);
	}

	if (isOwningMemory)
	{
		getInstance<GlobalAllocator>().deallocate(memory);
	}
}

void* GLOWE::FrameAllocator::allocate(const UInt64 size)
{
	void* expected = currMemPos.load(std::memory_order::memory_order_acquire), *newPtr{};
	do
	{
		newPtr = static_cast<char*>(expected) + size;
		if (newPtr > memEnd)
		{
			return nullptr;
		}
	} while (!currMemPos.compare_exchange_weak(expected, newPtr, std::memory_order::memory_order_acq_rel));

	return expected;
}

void GLOWE::FrameAllocator::clear()
{
	currMemPos.store(memory, std::memory_order::memory_order_release);
#ifdef GLOWE_ENABLE_CORRUPTION_HELPER
	std::memset(memory, 0xFA, getCapacity());
#endif
}

GLOWE::UInt64 GLOWE::FrameAllocator::getCapacity() const
{
	return reinterpret_cast<UInt64>(memEnd) - reinterpret_cast<UInt64>(memory);
}

void GLOWE::FrameAllocatorMgr::registerAllocator(FrameAllocator* allocator)
{
	DefaultLockGuard guard(mutex);
	frameAllocators.emplace(allocator);
}

void GLOWE::FrameAllocatorMgr::unregisterAllocator(FrameAllocator* allocator)
{
	DefaultLockGuard guard(mutex);
	frameAllocators.erase(allocator);
}

void GLOWE::FrameAllocatorMgr::registerAllocator(ResizeableFrameAllocator* allocator)
{
	DefaultLockGuard guard(mutex);
	resizeableFrameAllocators.emplace(allocator);
}

void GLOWE::FrameAllocatorMgr::unregisterAllocator(ResizeableFrameAllocator* allocator)
{
	DefaultLockGuard guard(mutex);
	resizeableFrameAllocators.erase(allocator);
}

void GLOWE::FrameAllocatorMgr::clearAllAllocators()
{
	DefaultLockGuard guard(mutex);
	for (auto& x : frameAllocators)
	{
		x->clear();
	}
	for (auto& x : resizeableFrameAllocators)
	{
		x->clear();
	}
}

void GLOWE::ResizeableFrameAllocator::addNewAlloc(const UInt64 reqSize, unsigned int expectedSize)
{
	DefaultLockGuard guard(mutex);
	allocs.emplace_back(reqSize <= FrameAllocator::defaultBlockSize ? FrameAllocator::defaultBlockSize : reqSize + FrameAllocator::defaultBlockSize, false);
	const unsigned int temp = expectedSize + 1;
	if (!size.compare_exchange_weak(expectedSize, temp, std::memory_order::memory_order_release, std::memory_order::memory_order_relaxed))
	{
		allocs.pop_back();
	}
}

GLOWE::ResizeableFrameAllocator::ResizeableFrameAllocator()
	: size(1)
{
	allocs.emplace_back(false);
	getInstance<FrameAllocatorMgr>().registerAllocator(this);
}

GLOWE::ResizeableFrameAllocator::ResizeableFrameAllocator(const UInt64 reqSize)
	: size(1)
{
	allocs.emplace_back(reqSize, false);
	getInstance<FrameAllocatorMgr>().registerAllocator(this);
}

GLOWE::ResizeableFrameAllocator::~ResizeableFrameAllocator()
{
	getInstance<FrameAllocatorMgr>().unregisterAllocator(this);
}

void* GLOWE::ResizeableFrameAllocator::allocate(const UInt64 reqSize)
{
	void* result = nullptr;
	
	const unsigned int expectedSize = size.load(std::memory_order::memory_order_acquire);
	auto it = allocs.begin();
	for (unsigned int x = 0; x < expectedSize; ++x, ++it)
	{
		result = it->allocate(reqSize);
		if (result)
		{
			return result;
		}
	}

	addNewAlloc(reqSize, expectedSize);

	return allocate(reqSize);
}

void GLOWE::ResizeableFrameAllocator::clear()
{
	if (size > 1)
	{
		size.store(0, std::memory_order::memory_order_relaxed);
		DefaultLockGuard guard(mutex);
		unsigned int newSize = 0;
		for (const auto& x : allocs)
		{
			newSize += x.getCapacity();
		}
		allocs.clear();
		allocs.emplace_back(newSize, false);
		size.store(1, std::memory_order::memory_order_release);
	}
#ifndef GLOWE_ENABLE_CORRUPTION_HELPER
	else
#endif
	{
		allocs.begin()->clear();
	}
}

void GLOWE::MemoryLeakDetector::addElement(const void* const ptr, const std::type_index& type, const unsigned int reqSize)
{
#ifdef GLOWE_ENABLE_LEAK_DETECTION
	DefaultLockGuard guard(mutex);
	current.emplace(ptr, type);
#endif
}

void GLOWE::MemoryLeakDetector::removeElement(const void* const ptr)
{
#ifdef GLOWE_ENABLE_LEAK_DETECTION
	DefaultLockGuard guard(mutex);
	const auto it = current.find(ptr);
	if (it == current.end())
	{
		throw Exception("Access violation - duplicate deallocation");
	}
	current.erase(it);
#endif
}

void GLOWE::MemoryLeakDetector::checkIfDeletable(const void* const ptr) const
{
#ifdef GLOWE_ENABLE_LEAK_DETECTION
	DefaultLockGuard guard(mutex);
	if (current.count(ptr) == 0)
	{
		throw Exception("Access violation - duplicate deallocation");
	}
#endif
}

std::map<const void*, std::type_index> GLOWE::MemoryLeakDetector::getPersistant()
{
	std::map<const void*, std::type_index> result;
#ifdef GLOWE_ENABLE_LEAK_DETECTION
	DefaultLockGuard guard(mutex);

	for (const auto& x : previous)
	{
		if (current.count(x.first) == 1)
		{
			result.emplace(x);
		}
	}

	previous = current;

#endif
	return result;
}
