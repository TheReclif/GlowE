#include "Values.h"

GLOWE::String GLOWE::Value::getTypename(const ValueType & valType)
{
#define SwitchCaseTypename(x) \
case ValueType::x: \
{ \
	return String(GlowU8Stringify(x)); \
} \
break;

	switch (valType)
	{
		SwitchCaseTypename(Float);
		SwitchCaseTypename(FloatArray);
		SwitchCaseTypename(Int);
		SwitchCaseTypename(IntArray);
		SwitchCaseTypename(UInt);
		SwitchCaseTypename(UIntArray);
		SwitchCaseTypename(Double);
		SwitchCaseTypename(DoubleArray);
		SwitchCaseTypename(Bool);
		SwitchCaseTypename(BoolArray);
		SwitchCaseTypename(String);
		SwitchCaseTypename(StringArray);
	}

	return String(u8"Unspecified");

#undef SwitchCaseTypename
}

GLOWE::FloatValue::FloatValue()
	: data(0.0f)
{
}

GLOWE::FloatValue::FloatValue(const float & arg)
	: data(arg)
{
}

float GLOWE::FloatValue::asFloat() const
{
	return data;
}

void GLOWE::FloatValue::asFloatArray(Vector<float>& out) const
{
	out.clear();
	out.resize(1, data);
}

int GLOWE::FloatValue::asInt() const
{
	return static_cast<int>(data);
}

void GLOWE::FloatValue::asIntArray(Vector<int>& out) const
{
	out.clear();
	out.resize(1, static_cast<int>(data));
}

unsigned int GLOWE::FloatValue::asUInt() const
{
	return static_cast<unsigned int>(data);
}

void GLOWE::FloatValue::asUIntArray(Vector<unsigned int>& out) const
{
	out.clear();
	out.resize(1, static_cast<unsigned int>(data));
}

double GLOWE::FloatValue::asDouble() const
{
	return static_cast<double>(data);
}

void GLOWE::FloatValue::asDoubleArray(Vector<double>& out) const
{
	out.clear();
	out.resize(1, static_cast<double>(data));
}

GLOWE::String GLOWE::FloatValue::asString() const
{
	return toString(data);
}

void GLOWE::FloatValue::asString(String & out) const
{
	out = toString(data);
}

bool GLOWE::FloatValue::asBool() const
{
	return static_cast<bool>(data);
}

void GLOWE::FloatValue::asBoolArray(Vector<bool>& out) const
{
	out.clear();
	out.resize(1, static_cast<bool>(data));
}

void GLOWE::FloatValue::fromFloat(const float & in)
{
	data = in;
}

void GLOWE::FloatValue::fromFloatArray(const float * in, const size_t arraySize)
{
	data = *in;
}

void GLOWE::FloatValue::fromFloatArray(const Vector<float>& in)
{
	data = in.front();
}

void GLOWE::FloatValue::fromInt(const int & in)
{
	data = static_cast<float>(in);
}

void GLOWE::FloatValue::fromIntArray(const int * in, const size_t arraySize)
{
	data = static_cast<float>(*in);
}

void GLOWE::FloatValue::fromIntArray(const Vector<int>& in)
{
	data = static_cast<float>(in.front());
}

void GLOWE::FloatValue::fromUInt(const unsigned int & in)
{
	data = static_cast<float>(in);
}

void GLOWE::FloatValue::fromUIntArray(const unsigned int * in, const size_t arraySize)
{
	data = static_cast<float>(*in);
}

void GLOWE::FloatValue::fromUIntArray(const Vector<unsigned int>& in)
{
	data = static_cast<float>(in.front());
}

void GLOWE::FloatValue::fromDouble(const double & in)
{
	data = static_cast<float>(in);
}

void GLOWE::FloatValue::fromDoubleArray(const double * in, const size_t arraySize)
{
	data = static_cast<float>(*in);
}

void GLOWE::FloatValue::fromDoubleArray(const Vector<double>& in)
{
	data = static_cast<float>(in.front());
}

void GLOWE::FloatValue::fromBool(const bool & in)
{
	data = static_cast<float>(in);
}

void GLOWE::FloatValue::fromBoolArray(const bool * in, const size_t arraySize)
{
	data = static_cast<float>(*in);
}

void GLOWE::FloatValue::fromBoolArray(const Vector<bool>& in)
{
	data = static_cast<float>(in.front());
}

void GLOWE::FloatValue::fromString(const String & in)
{
	data = in.toFloat();
}

const GLOWE::Value::ValueType GLOWE::FloatValue::getType() const
{
	return Value::ValueType::Float;
}

const size_t GLOWE::FloatValue::getArraySize() const
{
	return 0;
}

void GLOWE::FloatValue::asStringArray(Vector<String>& out) const
{
	out.resize(1);
	out.front() = toString(data);
}

void GLOWE::FloatValue::fromStringArray(const String * in, const size_t arraySize)
{
	if (arraySize > 0)
	{
		data = in[0].toFloat();
	}
}

void GLOWE::FloatValue::fromStringArray(const Vector<String>& in)
{
	if (in.size() > 0)
	{
		data = in.front().toFloat();
	}
}

GLOWE::FloatArrayValue::FloatArrayValue(const float* arg, const size_t arraySize)
	: data(arraySize)
{
	for(size_t x = 0; x<data.size(); ++x)
	{
		data[x] = arg[x];
	}
}

GLOWE::FloatArrayValue::FloatArrayValue(const Vector<float>& arg)
	: data(arg)
{
}

float GLOWE::FloatArrayValue::asFloat() const
{
	return !data.empty() ? data.front() : 0.0f;
}

void GLOWE::FloatArrayValue::asFloatArray(Vector<float>& out) const
{
	out.assign(data.begin(), data.end());
}

int GLOWE::FloatArrayValue::asInt() const
{
	return !data.empty() ? static_cast<int>(data.front()) : 0;
}

void GLOWE::FloatArrayValue::asIntArray(Vector<int>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<int>(data[x]);
	}
}

unsigned int GLOWE::FloatArrayValue::asUInt() const
{
	return !data.empty() ? static_cast<unsigned int>(data.front()) : 0U;
}

void GLOWE::FloatArrayValue::asUIntArray(Vector<unsigned int>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<unsigned int>(data[x]);
	}
}

double GLOWE::FloatArrayValue::asDouble() const
{
	return !data.empty() ? static_cast<double>(data.front()) : 0.0;
}

void GLOWE::FloatArrayValue::asDoubleArray(Vector<double>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<double>(data[x]);
	}
}

GLOWE::String GLOWE::FloatArrayValue::asString() const
{
	String result;

	for(const auto& x : data)
	{
		result += toString(x);
		result.getString() += u8", ";
	}

	return result.getString().substr(0, result.getSize() - 2);
}

void GLOWE::FloatArrayValue::asString(String& out) const
{
	out.getString().clear();

	for (const auto& x : data)
	{
		out += toString(x);
		out.getString() += u8", ";
	}

	out = out.getString().substr(0, out.getSize() - 2);
}

bool GLOWE::FloatArrayValue::asBool() const
{
	return !data.empty() ? static_cast<bool>(data.front()) : false;
}

void GLOWE::FloatArrayValue::asBoolArray(Vector<bool>& out) const
{
	out.resize(data.size());
	for(size_t x = 0; x<data.size(); ++x)
	{
		out[x] = static_cast<bool>(data[x]);
	}
}

void GLOWE::FloatArrayValue::fromFloat(const float& in)
{
	data.clear();
	data.resize(1, in);
}

void GLOWE::FloatArrayValue::fromFloatArray(const float* in, const size_t arraySize)
{
	data = Vector<float>(in, in + arraySize);
}

void GLOWE::FloatArrayValue::fromFloatArray(const Vector<float>& in)
{
	data.assign(in.begin(), in.end());
}

void GLOWE::FloatArrayValue::fromInt(const int& in)
{
	data.resize(1);
	data.front() = static_cast<float>(in);
}

void GLOWE::FloatArrayValue::fromIntArray(const int* in, const size_t arraySize)
{
	data.resize(arraySize);
	for(size_t x = 0; x<arraySize; ++x)
	{
		data[x] = static_cast<float>(in[x]);
	}
}

void GLOWE::FloatArrayValue::fromIntArray(const Vector<int>& in)
{
	data.resize(in.size());
	for(size_t x = 0; x<in.size(); ++x)
	{
		data[x] = static_cast<float>(in[x]);
	}
}

void GLOWE::FloatArrayValue::fromUInt(const unsigned int& in)
{
	data.resize(1);
	data.front() = static_cast<float>(in);
}

void GLOWE::FloatArrayValue::fromUIntArray(const unsigned int* in, const size_t arraySize)
{
	data.resize(arraySize);
	for(size_t x = 0; x<arraySize; ++x)
	{
		data[x] = static_cast<float>(in[x]);
	}
}

void GLOWE::FloatArrayValue::fromUIntArray(const Vector<unsigned int>& in)
{
	data.resize(in.size());
	for(size_t x = 0; x<in.size(); ++x)
	{
		data[x] = static_cast<float>(in[x]);
	}
}

void GLOWE::FloatArrayValue::fromDouble(const double& in)
{
	data.resize(1);
	data.front() = static_cast<float>(in);
}

void GLOWE::FloatArrayValue::fromDoubleArray(const double* in, const size_t arraySize)
{
	data.resize(arraySize);
	for(size_t x = 0; x<arraySize; ++x)
	{
		data[x] = static_cast<float>(in[x]);
	}
}

void GLOWE::FloatArrayValue::fromDoubleArray(const Vector<double>& in)
{
	data.resize(in.size());
	for(size_t x = 0; x<in.size(); ++x)
	{
		data[x] = static_cast<float>(in[x]);
	}
}

void GLOWE::FloatArrayValue::fromBool(const bool& in)
{
	data.resize(1);
	data.front() = static_cast<float>(in);
}

void GLOWE::FloatArrayValue::fromBoolArray(const bool* in, const size_t arraySize)
{
	data.resize(arraySize);
	for(size_t x = 0; x<arraySize; ++x)
	{
		data[x] = static_cast<float>(in[x]);
	}
}

void GLOWE::FloatArrayValue::fromBoolArray(const Vector<bool>& in)
{
	data.resize(in.size());
	for(size_t x = 0; x<in.size(); ++x)
	{
		data[x] = static_cast<float>(in[x]);
	}
}

void GLOWE::FloatArrayValue::fromString(const String& in)
{
	data.clear();

	const char possibleBeginBrackets[4] = { '<', '[', '(', '{' };
	const char possibleEndBrackets[4] = { '>', ']', ')', '}' };

	const char possibleSeparators[6] = { '.', ',', ';', '|', '/', '\\' };

	bool isNumberBeingProcessed = false;
	bool wasSeparatorAdded = false;
	String tempNumber;

	char chosenSeparator = '\0';
	const char* tempCharPtr = nullptr;
	short chosenBracketID = -1;

	for (const auto& x : in.getString())
	{
		if (std::isblank(static_cast<char>(x), getInstance<Locale>().getLocale()))
		{
			// Leave all of the blank spaces
			if (isNumberBeingProcessed)
			{
				isNumberBeingProcessed = false;
				data.push_back(tempNumber.toFloat());
				tempNumber.getString().clear();
				wasSeparatorAdded = false;
			}
		}
		else if (std::isdigit(static_cast<char>(x), getInstance<Locale>().getLocale()))
		{
			// If it's a digit - add it to our temporary String
			isNumberBeingProcessed = true;
			tempNumber.getString() += x;
		}
		else if (((tempCharPtr = std::find(possibleBeginBrackets, possibleBeginBrackets + 4, x)) != possibleBeginBrackets + 4))
		{
			// Check the beggining bracket
			if (chosenBracketID == (-1))
			{
				chosenBracketID = tempCharPtr - possibleBeginBrackets;
			}
			else
			{
				WarningThrow(false, String(u8"Unexpected token."));
				return;
			}
		}
		else if (((tempCharPtr = std::find(possibleEndBrackets, possibleEndBrackets + 4, x)) != possibleEndBrackets + 4))
		{
			// Check the ending bracket
			if (chosenBracketID != (-1))
			{
				if (x == possibleEndBrackets[chosenBracketID])
				{
					if (isNumberBeingProcessed)
					{
						isNumberBeingProcessed = false;
						data.push_back(tempNumber.toFloat());
						tempNumber.getString().clear();
						wasSeparatorAdded = false;
					}

					return;
				}
				else
				{
					WarningThrow(false, String(u8"Ending bracket does not match the beggining bracket."));
					return;
				}
			}
		}
		else if (x == '.')
		{
			if (isNumberBeingProcessed && !(wasSeparatorAdded))
			{
				tempNumber.getString() += x;
				wasSeparatorAdded = true;
			}
			else
			{
				WarningThrow(false, String(u8"Unexpected token."));
				return;
			}
		}
		else if (x == '-')
		{
			if (!isNumberBeingProcessed)
			{
				isNumberBeingProcessed = true;
				tempNumber.getString() += x;
			}
			else
			{
				WarningThrow(false, String(u8"Unexpected token."));
				return;
			}
		}
		else if (((tempCharPtr = std::find(possibleSeparators, possibleSeparators + 6, x)) != possibleSeparators + 6))
		{
			if (chosenSeparator == '\0')
			{
				chosenSeparator = x;
			}

			if (x != chosenSeparator)
			{
				WarningThrow(false, String(u8"Separators mismatch."));
				return;
			}

			if (isNumberBeingProcessed)
			{
				isNumberBeingProcessed = false;
				data.push_back(tempNumber.toFloat());
				tempNumber.getString().clear();
				wasSeparatorAdded = false;
			}
		}
		else
		{
			WarningThrow(false, String(u8"Unexpected token."));
			return;
		}
	}

	if (isNumberBeingProcessed)
	{
		isNumberBeingProcessed = false;
		data.push_back(tempNumber.toFloat());
		tempNumber.getString().clear();
		wasSeparatorAdded = false;
	}
}

const GLOWE::Value::ValueType GLOWE::FloatArrayValue::getType() const
{
	return Value::ValueType::FloatArray;
}

const size_t GLOWE::FloatArrayValue::getArraySize() const
{
	return data.size();
}

void GLOWE::FloatArrayValue::asStringArray(Vector<String>& out) const
{
	out.resize(data.size());

	for (unsigned int x = 0; x < data.size(); ++x)
	{
		out[x] = toString(data[x]);
	}
}

void GLOWE::FloatArrayValue::fromStringArray(const String * in, const size_t arraySize)
{
	data.resize(arraySize);

	for (unsigned int x = 0; x < arraySize; ++x)
	{
		data[x] = in[x].toFloat();
	}
}

void GLOWE::FloatArrayValue::fromStringArray(const Vector<String>& in)
{
	data.resize(in.size());

	for (unsigned int x = 0; x < in.size(); ++x)
	{
		data[x] = in[x].toFloat();
	}
}

GLOWE::IntValue::IntValue()
	: data(0)
{
}

GLOWE::IntValue::IntValue(const signed int & arg)
	: data(arg)
{
}

float GLOWE::IntValue::asFloat() const
{
	return static_cast<float>(data);
}

void GLOWE::IntValue::asFloatArray(Vector<float>& out) const
{
	out.clear();
	out.resize(1, static_cast<float>(data));
}

int GLOWE::IntValue::asInt() const
{
	return data;
}

void GLOWE::IntValue::asIntArray(Vector<int>& out) const
{
	out.clear();
	out.resize(1, data);
}

unsigned int GLOWE::IntValue::asUInt() const
{
	return static_cast<unsigned int>(data);
}

void GLOWE::IntValue::asUIntArray(Vector<unsigned int>& out) const
{
	out.clear();
	out.resize(1, static_cast<unsigned int>(data));
}

double GLOWE::IntValue::asDouble() const
{
	return static_cast<double>(data);
}

void GLOWE::IntValue::asDoubleArray(Vector<double>& out) const
{
	out.clear();
	out.resize(1, static_cast<double>(data));
}

GLOWE::String GLOWE::IntValue::asString() const
{
	return toString(data);
}

void GLOWE::IntValue::asString(String & out) const
{
	out = toString(data);
}

bool GLOWE::IntValue::asBool() const
{
	return static_cast<bool>(data);
}

void GLOWE::IntValue::asBoolArray(Vector<bool>& out) const
{
	out.clear();
	out.resize(1, static_cast<bool>(data));
}

void GLOWE::IntValue::fromFloat(const float & in)
{
	data = static_cast<signed int>(in);
}

void GLOWE::IntValue::fromFloatArray(const float * in, const size_t arraySize)
{
	data = static_cast<signed int>(*in);
}

void GLOWE::IntValue::fromFloatArray(const Vector<float>& in)
{
	data = static_cast<signed int>(in.front());
}

void GLOWE::IntValue::fromInt(const int & in)
{
	data = in;
}

void GLOWE::IntValue::fromIntArray(const int * in, const size_t arraySize)
{
	data = *in;
}

void GLOWE::IntValue::fromIntArray(const Vector<int>& in)
{
	data = in.front();
}

void GLOWE::IntValue::fromUInt(const unsigned int & in)
{
	data = static_cast<signed int>(in);
}

void GLOWE::IntValue::fromUIntArray(const unsigned int * in, const size_t arraySize)
{
	data = static_cast<signed int>(*in);
}

void GLOWE::IntValue::fromUIntArray(const Vector<unsigned int>& in)
{
	data = static_cast<signed int>(in.front());
}

void GLOWE::IntValue::fromDouble(const double & in)
{
	data = static_cast<signed int>(in);
}

void GLOWE::IntValue::fromDoubleArray(const double * in, const size_t arraySize)
{
	data = static_cast<signed int>(*in);
}

void GLOWE::IntValue::fromDoubleArray(const Vector<double>& in)
{
	data = static_cast<signed int>(in.front());
}

void GLOWE::IntValue::fromBool(const bool & in)
{
	data = static_cast<signed int>(in);
}

void GLOWE::IntValue::fromBoolArray(const bool * in, const size_t arraySize)
{
	data = static_cast<signed int>(*in);
}

void GLOWE::IntValue::fromBoolArray(const Vector<bool>& in)
{
	data = static_cast<signed int>(in.front());
}

void GLOWE::IntValue::fromString(const String & in)
{
	data = in.toInt();
}

const GLOWE::Value::ValueType GLOWE::IntValue::getType() const
{
	return Value::ValueType::Int;
}

const size_t GLOWE::IntValue::getArraySize() const
{
	return 0;
}

void GLOWE::IntValue::asStringArray(Vector<String>& out) const
{
	out.resize(1);
	out.front() = toString(data);
}

void GLOWE::IntValue::fromStringArray(const String * in, const size_t arraySize)
{
	if (arraySize > 0)
	{
		data = in[0].toInt();
	}
}

void GLOWE::IntValue::fromStringArray(const Vector<String>& in)
{
	if (in.size() > 0)
	{
		data = in.front().toInt();
	}
}

GLOWE::IntArrayValue::IntArrayValue(const int * arg, const size_t arraySize)
	: data(arraySize)
{
	for (size_t x = 0; x<data.size(); ++x)
	{
		data[x] = arg[x];
	}
}

GLOWE::IntArrayValue::IntArrayValue(const Vector<signed int>& arg)
	: data(arg)
{

}

float GLOWE::IntArrayValue::asFloat() const
{
	return !data.empty() ? static_cast<float>(data.front()) : 0.0f;
}

void GLOWE::IntArrayValue::asFloatArray(Vector<float>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<float>(data[x]);
	}
}

int GLOWE::IntArrayValue::asInt() const
{
	return !data.empty() ? data.front() : 0;
}

void GLOWE::IntArrayValue::asIntArray(Vector<int>& out) const
{
	out.assign(data.begin(), data.end());
}

unsigned int GLOWE::IntArrayValue::asUInt() const
{
	return !data.empty() ? static_cast<unsigned int>(data.front()) : 0U;
}

void GLOWE::IntArrayValue::asUIntArray(Vector<unsigned int>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<unsigned int>(data[x]);
	}
}

double GLOWE::IntArrayValue::asDouble() const
{
	return !data.empty() ? static_cast<double>(data.front()) : 0.0;
}

void GLOWE::IntArrayValue::asDoubleArray(Vector<double>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<double>(data[x]);
	}
}

GLOWE::String GLOWE::IntArrayValue::asString() const
{
	String result;

	for (const auto& x : data)
	{
		result += toString(x);
		result.getString() += u8", ";
	}

	return result.getString().substr(0, result.getSize() - 2);
}

void GLOWE::IntArrayValue::asString(String & out) const
{
	out.getString().clear();

	for (const auto& x : data)
	{
		out += toString(x);
		out.getString() += u8", ";
	}

	out = out.getString().substr(0, out.getSize() - 2);
}

bool GLOWE::IntArrayValue::asBool() const
{
	return !data.empty() ? static_cast<bool>(data.front()) : false;
}

void GLOWE::IntArrayValue::asBoolArray(Vector<bool>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<bool>(data[x]);
	}
}

void GLOWE::IntArrayValue::fromFloat(const float & in)
{
	data.clear();
	data.resize(1, static_cast<signed int>(in));
}

void GLOWE::IntArrayValue::fromFloatArray(const float * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<signed int>(in[x]);
	}
}

void GLOWE::IntArrayValue::fromFloatArray(const Vector<float>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<signed int>(in[x]);
	}
}

void GLOWE::IntArrayValue::fromInt(const int & in)
{
	data.clear();
	data.resize(1, in);
}

void GLOWE::IntArrayValue::fromIntArray(const int * in, const size_t arraySize)
{
	data = Vector<signed int>(in, in + arraySize);
}

void GLOWE::IntArrayValue::fromIntArray(const Vector<int>& in)
{
	data.assign(in.begin(), in.end());
}

void GLOWE::IntArrayValue::fromUInt(const unsigned int & in)
{
	data.clear();
	data.resize(1, static_cast<signed int>(in));
}

void GLOWE::IntArrayValue::fromUIntArray(const unsigned int * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<signed int>(in[x]);
	}
}

void GLOWE::IntArrayValue::fromUIntArray(const Vector<unsigned int>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<signed int>(in[x]);
	}
}

void GLOWE::IntArrayValue::fromDouble(const double & in)
{
	data.clear();
	data.resize(1, static_cast<signed int>(in));
}

void GLOWE::IntArrayValue::fromDoubleArray(const double * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<signed int>(in[x]);
	}
}

void GLOWE::IntArrayValue::fromDoubleArray(const Vector<double>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<signed int>(in[x]);
	}
}

void GLOWE::IntArrayValue::fromBool(const bool & in)
{
	data.clear();
	data.resize(1, static_cast<signed int>(in));
}

void GLOWE::IntArrayValue::fromBoolArray(const bool * in,const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<signed int>(in[x]);
	}
}

void GLOWE::IntArrayValue::fromBoolArray(const Vector<bool>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<signed int>(in[x]);
	}
}

void GLOWE::IntArrayValue::fromString(const String & in)
{
	data.clear();

	const char possibleBeginBrackets[4] = { '<', '[', '(', '{' };
	const char possibleEndBrackets[4] = { '>', ']', ')', '}' };

	const char possibleSeparators[6] = { '.', ',', ';', '|', '/', '\\' };

	bool isNumberBeingProcessed = false;
	bool wasSeparatorAdded = false;
	String tempNumber;

	char chosenSeparator = '\0';
	const char* tempCharPtr = nullptr;
	short chosenBracketID = -1;

	for (const auto& x : in.getString())
	{
		if (std::isblank(static_cast<char>(x), getInstance<Locale>().getLocale()))
		{
			// Leave all of the blank spaces
			if (isNumberBeingProcessed)
			{
				isNumberBeingProcessed = false;
				data.push_back(tempNumber.toInt());
				tempNumber.getString().clear();
				wasSeparatorAdded = false;
			}
		}
		else if (std::isdigit(static_cast<char>(x), getInstance<Locale>().getLocale()))
		{
			// If it's a digit - add it to our temporary String
			isNumberBeingProcessed = true;
			tempNumber.getString() += x;
		}
		else if (((tempCharPtr = std::find(possibleBeginBrackets, possibleBeginBrackets + 4, x)) != possibleBeginBrackets + 4))
		{
			// Check the beggining bracket
			if (chosenBracketID == (-1))
			{
				chosenBracketID = tempCharPtr - possibleBeginBrackets;
			}
			else
			{
				WarningThrow(false, String(u8"Unexpected token."));
				return;
			}
		}
		else if (((tempCharPtr = std::find(possibleEndBrackets, possibleEndBrackets + 4, x)) != possibleEndBrackets + 4))
		{
			// Check the ending bracket
			if (chosenBracketID != (-1))
			{
				if (x == possibleEndBrackets[chosenBracketID])
				{
					if (isNumberBeingProcessed)
					{
						isNumberBeingProcessed = false;
						data.push_back(tempNumber.toInt());
						tempNumber.getString().clear();
						wasSeparatorAdded = false;
					}

					return;
				}
				else
				{
					WarningThrow(false, String(u8"Ending bracket does not match the beggining bracket."));
					return;
				}
			}
		}
		else if (x == '-')
		{
			if (!isNumberBeingProcessed)
			{
				isNumberBeingProcessed = true;
				tempNumber.getString() += x;
			}
			else
			{
				WarningThrow(false, String(u8"Unexpected token."));
				return;
			}
		}
		else if (((tempCharPtr = std::find(possibleSeparators, possibleSeparators + 6, x)) != possibleSeparators + 6))
		{
			if (chosenSeparator == '\0')
			{
				chosenSeparator = x;
			}

			if (x != chosenSeparator)
			{
				WarningThrow(false, String(u8"Separators mismatch."));
				return;
			}

			if (isNumberBeingProcessed)
			{
				isNumberBeingProcessed = false;
				data.push_back(tempNumber.toInt());
				tempNumber.getString().clear();
				wasSeparatorAdded = false;
			}
		}
		else
		{
			WarningThrow(false, String(u8"Unexpected token."));
			return;
		}
	}

	if (isNumberBeingProcessed)
	{
		isNumberBeingProcessed = false;
		data.push_back(tempNumber.toInt());
		tempNumber.getString().clear();
		wasSeparatorAdded = false;
	}
}

const GLOWE::Value::ValueType GLOWE::IntArrayValue::getType() const
{
	return Value::ValueType::IntArray;
}

const size_t GLOWE::IntArrayValue::getArraySize() const
{
	return data.size();
}

void GLOWE::IntArrayValue::asStringArray(Vector<String>& out) const
{
	out.resize(data.size());

	for (unsigned int x = 0; x < data.size(); ++x)
	{
		out[x] = toString(data[x]);
	}
}

void GLOWE::IntArrayValue::fromStringArray(const String * in, const size_t arraySize)
{
	data.resize(arraySize);

	for (unsigned int x = 0; x < arraySize; ++x)
	{
		data[x] = in[x].toInt();
	}
}

void GLOWE::IntArrayValue::fromStringArray(const Vector<String>& in)
{
	data.resize(in.size());

	for (unsigned int x = 0; x < in.size(); ++x)
	{
		data[x] = in[x].toInt();
	}
}

GLOWE::UIntValue::UIntValue()
	: data(0U)
{
}

GLOWE::UIntValue::UIntValue(const signed int & arg)
	: data(arg)
{
}

float GLOWE::UIntValue::asFloat() const
{
	return static_cast<float>(data);
}

void GLOWE::UIntValue::asFloatArray(Vector<float>& out) const
{
	out.clear();
	out.resize(1, static_cast<float>(data));
}

int GLOWE::UIntValue::asInt() const
{
	return static_cast<int>(data);
}

void GLOWE::UIntValue::asIntArray(Vector<int>& out) const
{
	out.clear();
	out.resize(1, static_cast<int>(data));
}

unsigned int GLOWE::UIntValue::asUInt() const
{
	return data;
}

void GLOWE::UIntValue::asUIntArray(Vector<unsigned int>& out) const
{
	out.clear();
	out.resize(1, data);
}

double GLOWE::UIntValue::asDouble() const
{
	return static_cast<double>(data);
}

void GLOWE::UIntValue::asDoubleArray(Vector<double>& out) const
{
	out.clear();
	out.resize(1, static_cast<double>(data));
}

GLOWE::String GLOWE::UIntValue::asString() const
{
	return toString(data);
}

void GLOWE::UIntValue::asString(String & out) const
{
	out = toString(data);
}

bool GLOWE::UIntValue::asBool() const
{
	return static_cast<bool>(data);
}

void GLOWE::UIntValue::asBoolArray(Vector<bool>& out) const
{
	out.clear();
	out.resize(1, static_cast<bool>(data));
}

void GLOWE::UIntValue::fromFloat(const float & in)
{
	data = static_cast<unsigned int>(in);
}

void GLOWE::UIntValue::fromFloatArray(const float * in, const size_t arraySize)
{
	data = static_cast<unsigned int>(*in);
}

void GLOWE::UIntValue::fromFloatArray(const Vector<float>& in)
{
	data = static_cast<unsigned int>(in.front());
}

void GLOWE::UIntValue::fromInt(const int & in)
{
	data = static_cast<unsigned int>(in);
}

void GLOWE::UIntValue::fromIntArray(const int * in, const size_t arraySize)
{
	data = static_cast<unsigned int>(*in);
}

void GLOWE::UIntValue::fromIntArray(const Vector<int>& in)
{
	data = static_cast<unsigned int>(in.front());
}

void GLOWE::UIntValue::fromUInt(const unsigned int & in)
{
	data = in;
}

void GLOWE::UIntValue::fromUIntArray(const unsigned int * in, const size_t arraySize)
{
	data = *in;
}

void GLOWE::UIntValue::fromUIntArray(const Vector<unsigned int>& in)
{
	data = in.front();
}

void GLOWE::UIntValue::fromDouble(const double & in)
{
	data = static_cast<unsigned int>(in);
}

void GLOWE::UIntValue::fromDoubleArray(const double * in, const size_t arraySize)
{
	data = static_cast<unsigned int>(*in);
}

void GLOWE::UIntValue::fromDoubleArray(const Vector<double>& in)
{
	data = static_cast<unsigned int>(in.front());
}

void GLOWE::UIntValue::fromBool(const bool & in)
{
	data = static_cast<unsigned int>(in);
}

void GLOWE::UIntValue::fromBoolArray(const bool * in, const size_t arraySize)
{
	data = static_cast<unsigned int>(*in);
}

void GLOWE::UIntValue::fromBoolArray(const Vector<bool>& in)
{
	data = static_cast<unsigned int>(in.front());
}

void GLOWE::UIntValue::fromString(const String & in)
{
	data = in.toUInt();
}

const GLOWE::Value::ValueType GLOWE::UIntValue::getType() const
{
	return Value::ValueType::UInt;
}

const size_t GLOWE::UIntValue::getArraySize() const
{
	return 0;
}

void GLOWE::UIntValue::asStringArray(Vector<String>& out) const
{
	out.resize(1);
	out.front() = toString(data);
}

void GLOWE::UIntValue::fromStringArray(const String * in, const size_t arraySize)
{
	if (arraySize > 0)
	{
		data = in[0].toUInt();
	}
}

void GLOWE::UIntValue::fromStringArray(const Vector<String>& in)
{
	if (in.size() > 0)
	{
		data = in.front().toUInt();
	}
}

GLOWE::UIntArrayValue::UIntArrayValue(const unsigned int * arg, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = arg[x];
	}
}

GLOWE::UIntArrayValue::UIntArrayValue(const Vector<unsigned int>& arg)
	: data(arg.begin(), arg.end())
{
}

float GLOWE::UIntArrayValue::asFloat() const
{
	return !data.empty() ? static_cast<float>(data.front()) : 0.0f;
}

void GLOWE::UIntArrayValue::asFloatArray(Vector<float>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<float>(data[x]);
	}
}

int GLOWE::UIntArrayValue::asInt() const
{
	return !data.empty() ? static_cast<int>(data.front()) : 0;
}

void GLOWE::UIntArrayValue::asIntArray(Vector<int>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<int>(data[x]);
	}
}

unsigned int GLOWE::UIntArrayValue::asUInt() const
{
	return !data.empty() ? data.front() : 0U;
}

void GLOWE::UIntArrayValue::asUIntArray(Vector<unsigned int>& out) const
{
	out.assign(data.begin(), data.end());
}

double GLOWE::UIntArrayValue::asDouble() const
{
	return !data.empty() ? static_cast<float>(data.front()) : 0.0;
}

void GLOWE::UIntArrayValue::asDoubleArray(Vector<double>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<double>(data[x]);
	}
}

GLOWE::String GLOWE::UIntArrayValue::asString() const
{
	String result;

	for (const auto& x : data)
	{
		result += toString(x);
		result.getString() += u8", ";
	}

	return result.getString().substr(0, result.getSize() - 2);
}

void GLOWE::UIntArrayValue::asString(String & out) const
{
	out.getString().clear();

	for (const auto& x : data)
	{
		out += toString(x);
		out.getString() += u8", ";
	}

	out = out.getString().substr(0, out.getSize() - 2);
}

bool GLOWE::UIntArrayValue::asBool() const
{
	return !data.empty() ? static_cast<bool>(data.front()) : false;
}

void GLOWE::UIntArrayValue::asBoolArray(Vector<bool>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<bool>(data[x]);
	}
}

void GLOWE::UIntArrayValue::fromFloat(const float & in)
{
	data.clear();
	data.resize(1, static_cast<unsigned int>(in));
}

void GLOWE::UIntArrayValue::fromFloatArray(const float * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<unsigned int>(in[x]);
	}
}

void GLOWE::UIntArrayValue::fromFloatArray(const Vector<float>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<unsigned int>(in[x]);
	}
}

void GLOWE::UIntArrayValue::fromInt(const int & in)
{
	data.clear();
	data.resize(1, static_cast<unsigned int>(in));
}

void GLOWE::UIntArrayValue::fromIntArray(const int * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<unsigned int>(in[x]);
	}
}

void GLOWE::UIntArrayValue::fromIntArray(const Vector<int>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<unsigned int>(in[x]);
	}
}

void GLOWE::UIntArrayValue::fromUInt(const unsigned int & in)
{
	data.clear();
	data.resize(1, in);
}

void GLOWE::UIntArrayValue::fromUIntArray(const unsigned int * in, const size_t arraySize)
{
	data = Vector<unsigned int>(in, in + arraySize);
}

void GLOWE::UIntArrayValue::fromUIntArray(const Vector<unsigned int>& in)
{
	data.assign(in.begin(), in.end());
}

void GLOWE::UIntArrayValue::fromDouble(const double & in)
{
	data.clear();
	data.resize(1, static_cast<unsigned int>(in));
}

void GLOWE::UIntArrayValue::fromDoubleArray(const double * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<unsigned int>(in[x]);
	}
}

void GLOWE::UIntArrayValue::fromDoubleArray(const Vector<double>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<unsigned int>(in[x]);
	}
}

void GLOWE::UIntArrayValue::fromBool(const bool & in)
{
	data.clear();
	data.resize(1, static_cast<unsigned int>(in));
}

void GLOWE::UIntArrayValue::fromBoolArray(const bool * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<unsigned int>(in[x]);
	}
}

void GLOWE::UIntArrayValue::fromBoolArray(const Vector<bool>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<unsigned int>(in[x]);
	}
}

void GLOWE::UIntArrayValue::fromString(const String & in)
{
	data.clear();

	const char possibleBeginBrackets[4] = { '<', '[', '(', '{' };
	const char possibleEndBrackets[4] = { '>', ']', ')', '}' };

	const char possibleSeparators[6] = { '.', ',', ';', '|', '/', '\\' };

	bool isNumberBeingProcessed = false;
	bool wasSeparatorAdded = false;
	String tempNumber;

	char chosenSeparator = '\0';
	const char* tempCharPtr = nullptr;
	short chosenBracketID = -1;

	for (const auto& x : in.getString())
	{
		if (std::isblank(static_cast<char>(x), getInstance<Locale>().getLocale()))
		{
			// Leave all of the blank spaces
			if (isNumberBeingProcessed)
			{
				isNumberBeingProcessed = false;
				data.push_back(tempNumber.toInt());
				tempNumber.getString().clear();
				wasSeparatorAdded = false;
			}
		}
		else if (std::isdigit(static_cast<char>(x), getInstance<Locale>().getLocale()))
		{
			// If it's a digit - add it to our temporary String
			isNumberBeingProcessed = true;
			tempNumber.getString() += x;
		}
		else if (((tempCharPtr = std::find(possibleBeginBrackets, possibleBeginBrackets + 4, x)) != possibleBeginBrackets + 4))
		{
			// Check the beggining bracket
			if (chosenBracketID == (-1))
			{
				chosenBracketID = tempCharPtr - possibleBeginBrackets;
			}
			else
			{
				WarningThrow(false, String(u8"Unexpected token."));
				return;
			}
		}
		else if (((tempCharPtr = std::find(possibleEndBrackets, possibleEndBrackets + 4, x)) != possibleEndBrackets + 4))
		{
			// Check the ending bracket
			if (chosenBracketID != (-1))
			{
				if (x == possibleEndBrackets[chosenBracketID])
				{
					if (isNumberBeingProcessed)
					{
						isNumberBeingProcessed = false;
						data.push_back(tempNumber.toInt());
						tempNumber.getString().clear();
						wasSeparatorAdded = false;
					}

					return;
				}
				else
				{
					WarningThrow(false, String(u8"Ending bracket does not match the beggining bracket."));
					return;
				}
			}
		}
		else if (((tempCharPtr = std::find(possibleSeparators, possibleSeparators + 6, x)) != possibleSeparators + 6))
		{
			if (chosenSeparator == '\0')
			{
				chosenSeparator = x;
			}

			if (x != chosenSeparator)
			{
				WarningThrow(false, String(u8"Separators mismatch."));
				return;
			}

			if (isNumberBeingProcessed)
			{
				isNumberBeingProcessed = false;
				data.push_back(tempNumber.toInt());
				tempNumber.getString().clear();
				wasSeparatorAdded = false;
			}
		}
		else
		{
			WarningThrow(false, String(u8"Unexpected token."));
			return;
		}
	}

	if (isNumberBeingProcessed)
	{
		isNumberBeingProcessed = false;
		data.push_back(tempNumber.toInt());
		tempNumber.getString().clear();
		wasSeparatorAdded = false;
	}
}

const GLOWE::Value::ValueType GLOWE::UIntArrayValue::getType() const
{
	return Value::ValueType::UIntArray;
}

const size_t GLOWE::UIntArrayValue::getArraySize() const
{
	return data.size();
}

void GLOWE::UIntArrayValue::asStringArray(Vector<String>& out) const
{
	out.resize(data.size());

	for (unsigned int x = 0; x < data.size(); ++x)
	{
		out[x] = toString(data[x]);
	}
}

void GLOWE::UIntArrayValue::fromStringArray(const String * in, const size_t arraySize)
{
	data.resize(arraySize);

	for (unsigned int x = 0; x < arraySize; ++x)
	{
		data[x] = in[x].toUInt();
	}
}

void GLOWE::UIntArrayValue::fromStringArray(const Vector<String>& in)
{
	data.resize(in.size());

	for (unsigned int x = 0; x < in.size(); ++x)
	{
		data[x] = in[x].toUInt();
	}
}

GLOWE::DoubleValue::DoubleValue()
	: data(0.0)
{
}

GLOWE::DoubleValue::DoubleValue(const double & arg)
	: data(arg)
{
}

float GLOWE::DoubleValue::asFloat() const
{
	return static_cast<float>(data);
}

void GLOWE::DoubleValue::asFloatArray(Vector<float>& out) const
{
	out.clear();
	out.resize(1, static_cast<float>(data));
}

int GLOWE::DoubleValue::asInt() const
{
	return static_cast<int>(data);
}

void GLOWE::DoubleValue::asIntArray(Vector<int>& out) const
{
	out.clear();
	out.resize(1, static_cast<int>(data));
}

unsigned int GLOWE::DoubleValue::asUInt() const
{
	return static_cast<unsigned int>(data);
}

void GLOWE::DoubleValue::asUIntArray(Vector<unsigned int>& out) const
{
	out.clear();
	out.resize(1, static_cast<unsigned int>(data));
}

double GLOWE::DoubleValue::asDouble() const
{
	return data;
}

void GLOWE::DoubleValue::asDoubleArray(Vector<double>& out) const
{
	out.clear();
	out.resize(1, data);
}

GLOWE::String GLOWE::DoubleValue::asString() const
{
	return toString(data);
}

void GLOWE::DoubleValue::asString(String & out) const
{
	out = toString(data);
}

bool GLOWE::DoubleValue::asBool() const
{
	return static_cast<bool>(data);
}

void GLOWE::DoubleValue::asBoolArray(Vector<bool>& out) const
{
	out.clear();
	out.resize(1, static_cast<bool>(data));
}

void GLOWE::DoubleValue::fromFloat(const float & in)
{
	data = static_cast<double>(in);
}

void GLOWE::DoubleValue::fromFloatArray(const float * in, const size_t arraySize)
{
	data = static_cast<double>(*in);
}

void GLOWE::DoubleValue::fromFloatArray(const Vector<float>& in)
{
	data = static_cast<double>(in.front());
}

void GLOWE::DoubleValue::fromInt(const int & in)
{
	data = static_cast<double>(in);
}

void GLOWE::DoubleValue::fromIntArray(const int * in, const size_t arraySize)
{
	data = static_cast<double>(*in);
}

void GLOWE::DoubleValue::fromIntArray(const Vector<int>& in)
{
	data = static_cast<double>(in.front());
}

void GLOWE::DoubleValue::fromUInt(const unsigned int & in)
{
	data = static_cast<double>(in);
}

void GLOWE::DoubleValue::fromUIntArray(const unsigned int * in, const size_t arraySize)
{
	data = static_cast<double>(*in);
}

void GLOWE::DoubleValue::fromUIntArray(const Vector<unsigned int>& in)
{
	data = static_cast<double>(in.front());
}

void GLOWE::DoubleValue::fromDouble(const double & in)
{
	data = in;
}

void GLOWE::DoubleValue::fromDoubleArray(const double * in, const size_t arraySize)
{
	data = *in;
}

void GLOWE::DoubleValue::fromDoubleArray(const Vector<double>& in)
{
	data = in.front();
}

void GLOWE::DoubleValue::fromBool(const bool & in)
{
	data = static_cast<double>(in);
}

void GLOWE::DoubleValue::fromBoolArray(const bool * in, const size_t arraySize)
{
	data = static_cast<double>(*in);
}

void GLOWE::DoubleValue::fromBoolArray(const Vector<bool>& in)
{
	data = static_cast<double>(in.front());
}

void GLOWE::DoubleValue::fromString(const String & in)
{
	data = in.toDouble();
}

const GLOWE::Value::ValueType GLOWE::DoubleValue::getType() const
{
	return Value::ValueType::Double;
}

const size_t GLOWE::DoubleValue::getArraySize() const
{
	return 0;
}

void GLOWE::DoubleValue::asStringArray(Vector<String>& out) const
{
	out.resize(1);
	out.front() = toString(data);
}

void GLOWE::DoubleValue::fromStringArray(const String * in, const size_t arraySize)
{
	if (arraySize > 0)
	{
		data = in[0].toDouble();
	}
}

void GLOWE::DoubleValue::fromStringArray(const Vector<String>& in)
{
	if (in.size() > 0)
	{
		data = in.front().toDouble();
	}
}

GLOWE::DoubleArrayValue::DoubleArrayValue(const double * arg, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = arg[x];
	}
}

GLOWE::DoubleArrayValue::DoubleArrayValue(const Vector<double>& arg)
	: data(arg.begin(), arg.end())
{
}

float GLOWE::DoubleArrayValue::asFloat() const
{
	return static_cast<float>(data.front());
}

void GLOWE::DoubleArrayValue::asFloatArray(Vector<float>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<float>(data[x]);
	}
}

int GLOWE::DoubleArrayValue::asInt() const
{
	return static_cast<int>(data.front());
}

void GLOWE::DoubleArrayValue::asIntArray(Vector<int>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<int>(data[x]);
	}
}

unsigned int GLOWE::DoubleArrayValue::asUInt() const
{
	return static_cast<unsigned int>(data.front());
}

void GLOWE::DoubleArrayValue::asUIntArray(Vector<unsigned int>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<unsigned int>(data[x]);
	}
}

double GLOWE::DoubleArrayValue::asDouble() const
{
	return data.front();
}

void GLOWE::DoubleArrayValue::asDoubleArray(Vector<double>& out) const
{
	out.assign(data.begin(), data.end());
}

GLOWE::String GLOWE::DoubleArrayValue::asString() const
{
	String result;

	for (const auto& x : data)
	{
		result += toString(x);
		result.getString() += u8", ";
	}

	return result.getString().substr(0, result.getSize() - 2);
}

void GLOWE::DoubleArrayValue::asString(String & out) const
{
	out.getString().clear();

	for (const auto& x : data)
	{
		out += toString(x);
		out.getString() += u8", ";
	}

	out = out.getString().substr(0, out.getSize() - 2);
}

bool GLOWE::DoubleArrayValue::asBool() const
{
	return static_cast<bool>(data.front());
}

void GLOWE::DoubleArrayValue::asBoolArray(Vector<bool>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<bool>(data[x]);
	}
}

void GLOWE::DoubleArrayValue::fromFloat(const float & in)
{
	data.clear();
	data.resize(1, static_cast<double>(in));
}

void GLOWE::DoubleArrayValue::fromFloatArray(const float * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<double>(in[x]);
	}
}

void GLOWE::DoubleArrayValue::fromFloatArray(const Vector<float>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<double>(in[x]);
	}
}

void GLOWE::DoubleArrayValue::fromInt(const int & in)
{
	data.clear();
	data.resize(1, static_cast<double>(in));
}

void GLOWE::DoubleArrayValue::fromIntArray(const int * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<double>(in[x]);
	}
}

void GLOWE::DoubleArrayValue::fromIntArray(const Vector<int>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<double>(in[x]);
	}
}

void GLOWE::DoubleArrayValue::fromUInt(const unsigned int & in)
{
	data.clear();
	data.resize(1, static_cast<double>(in));
}

void GLOWE::DoubleArrayValue::fromUIntArray(const unsigned int * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<double>(in[x]);
	}
}

void GLOWE::DoubleArrayValue::fromUIntArray(const Vector<unsigned int>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<double>(in[x]);
	}
}

void GLOWE::DoubleArrayValue::fromDouble(const double & in)
{
	data.clear();
	data.resize(1, in);
}

void GLOWE::DoubleArrayValue::fromDoubleArray(const double * in, const size_t arraySize)
{
	data = Vector<double>(in, in + arraySize);
}

void GLOWE::DoubleArrayValue::fromDoubleArray(const Vector<double>& in)
{
	data.assign(in.begin(), in.end());
}

void GLOWE::DoubleArrayValue::fromBool(const bool & in)
{
	data.clear();
	data.resize(1, static_cast<double>(in));
}

void GLOWE::DoubleArrayValue::fromBoolArray(const bool * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<double>(in[x]);
	}
}

void GLOWE::DoubleArrayValue::fromBoolArray(const Vector<bool>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<double>(in[x]);
	}
}

void GLOWE::DoubleArrayValue::fromString(const String & in)
{
	data.clear();

	const char possibleBeginBrackets[4] = { '<', '[', '(', '{' };
	const char possibleEndBrackets[4] = { '>', ']', ')', '}' };

	const char possibleSeparators[6] = { '.', ',', ';', '|', '/', '\\' };

	bool isNumberBeingProcessed = false;
	bool wasSeparatorAdded = false;
	String tempNumber;

	char chosenSeparator = '\0';
	const char* tempCharPtr = nullptr;
	short chosenBracketID = -1;

	for (const auto& x : in.getString())
	{
		if (std::isblank(static_cast<char>(x), getInstance<Locale>().getLocale()))
		{
			// Leave all of the blank spaces
			if (isNumberBeingProcessed)
			{
				isNumberBeingProcessed = false;
				data.push_back(tempNumber.toDouble());
				tempNumber.getString().clear();
				wasSeparatorAdded = false;
			}
		}
		else if (std::isdigit(static_cast<char>(x), getInstance<Locale>().getLocale()))
		{
			// If it's a digit - add it to our temporary String
			isNumberBeingProcessed = true;
			tempNumber.getString() += x;
		}
		else if (((tempCharPtr = std::find(possibleBeginBrackets, possibleBeginBrackets + 4, x)) != possibleBeginBrackets + 4))
		{
			// Check the beggining bracket
			if (chosenBracketID == (-1))
			{
				chosenBracketID = tempCharPtr - possibleBeginBrackets;
			}
			else
			{
				WarningThrow(false, String(u8"Unexpected token."));
				return;
			}
		}
		else if (((tempCharPtr = std::find(possibleEndBrackets, possibleEndBrackets + 4, x)) != possibleEndBrackets + 4))
		{
			// Check the ending bracket
			if (chosenBracketID != (-1))
			{
				if (x == possibleEndBrackets[chosenBracketID])
				{
					if (isNumberBeingProcessed)
					{
						isNumberBeingProcessed = false;
						data.push_back(tempNumber.toDouble());
						tempNumber.getString().clear();
						wasSeparatorAdded = false;
					}

					return;
				}
				else
				{
					WarningThrow(false, String(u8"Ending bracket does not match the beggining bracket."));
					return;
				}
			}
		}
		else if (x == '.')
		{
			if (isNumberBeingProcessed && !(wasSeparatorAdded))
			{
				tempNumber.getString() += x;
				wasSeparatorAdded = true;
			}
			else
			{
				WarningThrow(false, String(u8"Unexpected token."));
				return;
			}
		}
		else if (x == '-')
		{
			if (!isNumberBeingProcessed)
			{
				isNumberBeingProcessed = true;
				tempNumber.getString() += x;
			}
			else
			{
				WarningThrow(false, String(u8"Unexpected token."));
				return;
			}
		}
		else if (((tempCharPtr = std::find(possibleSeparators, possibleSeparators + 6, x)) != possibleSeparators + 6))
		{
			if (chosenSeparator == '\0')
			{
				chosenSeparator = x;
			}

			if (x != chosenSeparator)
			{
				WarningThrow(false, String(u8"Separators mismatch."));
				return;
			}

			if (isNumberBeingProcessed)
			{
				isNumberBeingProcessed = false;
				data.push_back(tempNumber.toDouble());
				tempNumber.getString().clear();
				wasSeparatorAdded = false;
			}
		}
		else
		{
			WarningThrow(false, String(u8"Unexpected token."));
			return;
		}
	}

	if (isNumberBeingProcessed)
	{
		isNumberBeingProcessed = false;
		data.push_back(tempNumber.toDouble());
		tempNumber.getString().clear();
		wasSeparatorAdded = false;
	}
}

const GLOWE::Value::ValueType GLOWE::DoubleArrayValue::getType() const
{
	return Value::ValueType::DoubleArray;
}

const size_t GLOWE::DoubleArrayValue::getArraySize() const
{
	return data.size();
}

void GLOWE::DoubleArrayValue::asStringArray(Vector<String>& out) const
{
	out.resize(data.size());

	for (unsigned int x = 0; x < data.size(); ++x)
	{
		out[x] = toString(data[x]);
	}
}

void GLOWE::DoubleArrayValue::fromStringArray(const String * in, const size_t arraySize)
{
	data.resize(arraySize);

	for (unsigned int x = 0; x < arraySize; ++x)
	{
		data[x] = in[x].toDouble();
	}
}

void GLOWE::DoubleArrayValue::fromStringArray(const Vector<String>& in)
{
	data.resize(in.size());

	for (unsigned int x = 0; x < in.size(); ++x)
	{
		data[x] = in[x].toDouble();
	}
}

GLOWE::StringValue::StringValue(const String & arg)
	: data(arg)
{
}

float GLOWE::StringValue::asFloat() const
{
	return data.toFloat();
}

void GLOWE::StringValue::asFloatArray(Vector<float>& out) const
{
	FloatArrayValue temp;
	temp.fromString(data);
	temp.asFloatArray(out);
}

int GLOWE::StringValue::asInt() const
{
	return data.toInt();
}

void GLOWE::StringValue::asIntArray(Vector<int>& out) const
{
	IntArrayValue temp;
	temp.fromString(data);
	temp.asIntArray(out);
}

unsigned int GLOWE::StringValue::asUInt() const
{
	return data.toUInt();
}

void GLOWE::StringValue::asUIntArray(Vector<unsigned int>& out) const
{
	UIntArrayValue temp;
	temp.fromString(data);
	temp.asUIntArray(out);
}

double GLOWE::StringValue::asDouble() const
{
	return data.toDouble();
}

void GLOWE::StringValue::asDoubleArray(Vector<double>& out) const
{
	DoubleArrayValue temp;
	temp.fromString(data);
	temp.asDoubleArray(out);
}

GLOWE::String GLOWE::StringValue::asString() const
{
	return data;
}

void GLOWE::StringValue::asString(String& out) const
{
	out = data;
}

bool GLOWE::StringValue::asBool() const
{
	return data.toBool();
}

void GLOWE::StringValue::asBoolArray(Vector<bool>& out) const
{
	BoolArrayValue temp;
	temp.fromString(data);
	temp.asBoolArray(out);
}

void GLOWE::StringValue::fromFloat(const float & in)
{
	data = toString(in);
}

void GLOWE::StringValue::fromFloatArray(const float * in, const size_t arraySize)
{
	FloatArrayValue temp;
	temp.fromFloatArray(in, arraySize);
	data = temp.asString();
}

void GLOWE::StringValue::fromFloatArray(const Vector<float>& in)
{
	FloatArrayValue temp(in);
	data = temp.asString();
}

void GLOWE::StringValue::fromInt(const int & in)
{
	data = toString(in);
}

void GLOWE::StringValue::fromIntArray(const int * in, const size_t arraySize)
{
	IntArrayValue temp;
	temp.fromIntArray(in, arraySize);
	data = temp.asString();
}

void GLOWE::StringValue::fromIntArray(const Vector<int>& in)
{
	IntArrayValue temp(in);
	data = temp.asString();
}

void GLOWE::StringValue::fromUInt(const unsigned int & in)
{
	data = toString(in);
}

void GLOWE::StringValue::fromUIntArray(const unsigned int * in, const size_t arraySize)
{
	UIntArrayValue temp;
	temp.fromUIntArray(in, arraySize);
	data = temp.asString();
}

void GLOWE::StringValue::fromUIntArray(const Vector<unsigned int>& in)
{
	UIntArrayValue temp(in);
	data = temp.asString();
}

void GLOWE::StringValue::fromDouble(const double & in)
{
	data = toString(in);
}

void GLOWE::StringValue::fromDoubleArray(const double * in, const size_t arraySize)
{
	DoubleArrayValue temp;
	temp.fromDoubleArray(in, arraySize);
	data = temp.asString();
}

void GLOWE::StringValue::fromDoubleArray(const Vector<double>& in)
{
	DoubleArrayValue temp(in);
	data = temp.asString();
}

void GLOWE::StringValue::fromBool(const bool & in)
{
	data = toString(in);
}

void GLOWE::StringValue::fromBoolArray(const bool * in, const size_t arraySize)
{
	BoolArrayValue temp;
	temp.fromBoolArray(in, arraySize);
	data = temp.asString();
}

void GLOWE::StringValue::fromBoolArray(const Vector<bool>& in)
{
	BoolArrayValue temp(in);
	data = temp.asString();
}

GLOWE::BoolValue::BoolValue()
	: data(false)
{
}

GLOWE::BoolValue::BoolValue(const bool& arg)
	: data(arg)
{
}

float GLOWE::BoolValue::asFloat() const
{
	return static_cast<float>(data);
}

void GLOWE::BoolValue::asFloatArray(Vector<float>& out) const
{
	out.clear();
	out.resize(1, static_cast<float>(data));
}

int GLOWE::BoolValue::asInt() const
{
	return static_cast<int>(data);
}

void GLOWE::BoolValue::asIntArray(Vector<int>& out) const
{
	out.clear();
	out.resize(1, static_cast<int>(data));
}

unsigned int GLOWE::BoolValue::asUInt() const
{
	return static_cast<unsigned int>(data);
}

void GLOWE::BoolValue::asUIntArray(Vector<unsigned int>& out) const
{
	out.clear();
	out.resize(1, static_cast<unsigned int>(data));
}

double GLOWE::BoolValue::asDouble() const
{
	return static_cast<double>(data);
}

void GLOWE::BoolValue::asDoubleArray(Vector<double>& out) const
{
	out.clear();
	out.resize(1, static_cast<double>(data));
}

GLOWE::String GLOWE::BoolValue::asString() const
{
	return toString(data);
}

void GLOWE::BoolValue::asString(String& out) const
{
	out = toString(data);
}

bool GLOWE::BoolValue::asBool() const
{
	return data;
}

void GLOWE::BoolValue::asBoolArray(Vector<bool>& out) const
{
	out.clear();
	out.resize(1, data);
}

void GLOWE::BoolValue::fromFloat(const float& in)
{
	data = static_cast<bool>(in);
}

void GLOWE::BoolValue::fromFloatArray(const float* in, const size_t arraySize)
{
	data = static_cast<bool>(*in);
}

void GLOWE::BoolValue::fromFloatArray(const Vector<float>& in)
{
	data = static_cast<bool>(in.front());
}

void GLOWE::BoolValue::fromInt(const int& in)
{
	data = static_cast<bool>(in);
}

void GLOWE::BoolValue::fromIntArray(const int* in, const size_t arraySize)
{
	data = static_cast<bool>(*in);
}

void GLOWE::BoolValue::fromIntArray(const Vector<int>& in)
{
	data = static_cast<bool>(in.front());
}

void GLOWE::BoolValue::fromUInt(const unsigned int& in)
{
	data = static_cast<bool>(in);
}

void GLOWE::BoolValue::fromUIntArray(const unsigned int* in, const size_t arraySize)
{
	data = static_cast<bool>(*in);
}

void GLOWE::BoolValue::fromUIntArray(const Vector<unsigned int>& in)
{
	data = static_cast<bool>(in.front());
}

void GLOWE::BoolValue::fromDouble(const double& in)
{
	data = static_cast<bool>(in);
}

void GLOWE::BoolValue::fromDoubleArray(const double* in, const size_t arraySize)
{
	data = static_cast<bool>(*in);
}

void GLOWE::BoolValue::fromDoubleArray(const Vector<double>& in)
{
	data = static_cast<bool>(in.front());
}

void GLOWE::BoolValue::fromBool(const bool& in)
{
	data = in;
}

void GLOWE::BoolValue::fromBoolArray(const bool* in, const size_t arraySize)
{
	data = *in;
}

void GLOWE::BoolValue::fromBoolArray(const Vector<bool>& in)
{
	data = in.front();
}

void GLOWE::BoolValue::fromString(const String& in)
{
	data = in.toBool();
}

const GLOWE::Value::ValueType GLOWE::BoolValue::getType() const
{
	return Value::ValueType::Bool;
}

const size_t GLOWE::BoolValue::getArraySize() const
{
	return 0;
}

void GLOWE::BoolValue::asStringArray(Vector<String>& out) const
{
	out.resize(1);
	out.front() = toString(data);
}

void GLOWE::BoolValue::fromStringArray(const String * in, const size_t arraySize)
{
	if (arraySize > 0)
	{
		data = in[0].toBool();
	}
}

void GLOWE::BoolValue::fromStringArray(const Vector<String>& in)
{
	if (in.size() > 0)
	{
		data = in.front().toBool();
	}
}

GLOWE::BoolArrayValue::BoolArrayValue(const bool * arg, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = arg[x];
	}
}

GLOWE::BoolArrayValue::BoolArrayValue(const Vector<bool>& arg)
	: data(arg.begin(), arg.end())
{
}

float GLOWE::BoolArrayValue::asFloat() const
{
	return static_cast<float>(data.front());
}

void GLOWE::BoolArrayValue::asFloatArray(Vector<float>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<float>(data[x]);
	}
}

int GLOWE::BoolArrayValue::asInt() const
{
	return static_cast<int>(data.front());
}

void GLOWE::BoolArrayValue::asIntArray(Vector<int>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<int>(data[x]);
	}
}

unsigned int GLOWE::BoolArrayValue::asUInt() const
{
	return static_cast<unsigned int>(data.front());
}

void GLOWE::BoolArrayValue::asUIntArray(Vector<unsigned int>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<unsigned int>(data[x]);
	}
}

double GLOWE::BoolArrayValue::asDouble() const
{
	return static_cast<double>(data.front());
}

void GLOWE::BoolArrayValue::asDoubleArray(Vector<double>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = static_cast<double>(data[x]);
	}
}

GLOWE::String GLOWE::BoolArrayValue::asString() const
{
	String result;

	for (const auto& x : data)
	{
		result += toString(x);
		result.getString() += u8", ";
	}

	return result.getString().substr(0, result.getSize() - 2);
}

void GLOWE::BoolArrayValue::asString(String & out) const
{
	out.getString().clear();

	for (const auto& x : data)
	{
		out += toString(x);
		out.getString() += u8", ";
	}

	out = out.getString().substr(0, out.getSize() - 2);
}

bool GLOWE::BoolArrayValue::asBool() const
{
	return data.front();
}

void GLOWE::BoolArrayValue::asBoolArray(Vector<bool>& out) const
{
	out.assign(data.begin(), data.end());
}

void GLOWE::BoolArrayValue::fromFloat(const float & in)
{
	data.clear();
	data.resize(1, static_cast<bool>(in));
}

void GLOWE::BoolArrayValue::fromFloatArray(const float * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<bool>(in[x]);
	}
}

void GLOWE::BoolArrayValue::fromFloatArray(const Vector<float>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<bool>(in[x]);
	}
}

void GLOWE::BoolArrayValue::fromInt(const int & in)
{
	data.clear();
	data.resize(1, static_cast<bool>(in));
}

void GLOWE::BoolArrayValue::fromIntArray(const int * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<bool>(in[x]);
	}
}

void GLOWE::BoolArrayValue::fromIntArray(const Vector<int>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<bool>(in[x]);
	}
}

void GLOWE::BoolArrayValue::fromUInt(const unsigned int & in)
{
	data.clear();
	data.resize(1, static_cast<bool>(in));
}

void GLOWE::BoolArrayValue::fromUIntArray(const unsigned int * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<bool>(in[x]);
	}
}

void GLOWE::BoolArrayValue::fromUIntArray(const Vector<unsigned int>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<bool>(in[x]);
	}
}

void GLOWE::BoolArrayValue::fromDouble(const double & in)
{
	data.clear();
	data.resize(1, static_cast<bool>(in));
}

void GLOWE::BoolArrayValue::fromDoubleArray(const double * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = static_cast<bool>(in[x]);
	}
}

void GLOWE::BoolArrayValue::fromDoubleArray(const Vector<double>& in)
{
	data.resize(in.size());
	for (size_t x = 0; x < in.size(); ++x)
	{
		data[x] = static_cast<bool>(in[x]);
	}
}

void GLOWE::BoolArrayValue::fromBool(const bool & in)
{
	data.clear();
	data.resize(1, in);
}

void GLOWE::BoolArrayValue::fromBoolArray(const bool * in, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = in[x];
	}
}

void GLOWE::BoolArrayValue::fromBoolArray(const Vector<bool>& in)
{
	data.assign(in.begin(), in.end());
}

void GLOWE::BoolArrayValue::fromString(const String & in)
{
	data.clear();

	const char possibleBeginBrackets[4] = { '<', '[', '(', '{' };
	const char possibleEndBrackets[4] = { '>', ']', ')', '}' };

	const char possibleSeparators[6] = { '.', ',', ';', '|', '/', '\\' };

	bool isValueBeingProcessed = false;
	bool wasSeparatorAdded = false;
	String tempValue;

	char chosenSeparator = '\0';
	const char* tempCharPtr = nullptr;
	short chosenBracketID = -1;

	for (const auto& x : in.getString())
	{
		if (std::isblank(static_cast<char>(x), getInstance<Locale>().getLocale()))
		{
			if (isValueBeingProcessed)
			{
				isValueBeingProcessed = false;
				data.push_back(tempValue.toBool());
				tempValue.getString().clear();
				wasSeparatorAdded = false;
			}
		}
		else if(std::isdigit(static_cast<char>(x), getInstance<Locale>().getLocale()))
		{
			isValueBeingProcessed = true;
			tempValue.getString() += x;
		}
		else if (((tempCharPtr = std::find(possibleBeginBrackets, possibleBeginBrackets + 4, x)) != possibleBeginBrackets + 4))
		{
			// Check the beggining bracket
			if (chosenBracketID == (-1))
			{
				chosenBracketID = tempCharPtr - possibleBeginBrackets;
			}
			else
			{
				WarningThrow(false, String(u8"Unexpected token."));
				return;
			}
		}
		else if (((tempCharPtr = std::find(possibleEndBrackets, possibleEndBrackets + 4, x)) != possibleEndBrackets + 4))
		{
			// Check the ending bracket
			if (chosenBracketID != (-1))
			{
				if (x == possibleEndBrackets[chosenBracketID])
				{
					if (isValueBeingProcessed)
					{
						isValueBeingProcessed = false;
						data.push_back(tempValue.toBool());
						tempValue.getString().clear();
						wasSeparatorAdded = false;
					}

					return;
				}
				else
				{
					WarningThrow(false, String(u8"Ending bracket does not match the beggining bracket."));
					return;
				}
			}
		}
		else if (((tempCharPtr = std::find(possibleSeparators, possibleSeparators + 6, x)) != possibleSeparators + 6))
		{
			if (chosenSeparator == '\0')
			{
				chosenSeparator = x;
			}

			if (x != chosenSeparator)
			{
				WarningThrow(false, String(u8"Separators mismatch."));
				return;
			}

			if (isValueBeingProcessed)
			{
				isValueBeingProcessed = false;
				data.push_back(tempValue.toBool());
				tempValue.getString().clear();
				wasSeparatorAdded = false;
			}
		}
		else
		{
			switch (x)
			{
			case 'f':
			case 'a':
			case 'l':
			case 's':
			case 'e':
			case 't':
			case 'r':
			case 'u':
				{
					isValueBeingProcessed = true;
					tempValue.getString().push_back(x);
				}
				break;
			default:
				{
					WarningThrow(false, String(u8"Unexpected token."));
					return;
				}
			}
		}
	}

	if (isValueBeingProcessed)
	{
		isValueBeingProcessed = false;
		data.push_back(tempValue.toBool());
		tempValue.getString().clear();
		wasSeparatorAdded = false;
	}
}

const GLOWE::Value::ValueType GLOWE::BoolArrayValue::getType() const
{
	return Value::ValueType::BoolArray;
}

const size_t GLOWE::BoolArrayValue::getArraySize() const
{
	return data.size();
}

void GLOWE::BoolArrayValue::asStringArray(Vector<String>& out) const
{
	out.resize(data.size());

	for (unsigned int x = 0; x < data.size(); ++x)
	{
		out[x] = toString(data[x]);
	}
}

void GLOWE::BoolArrayValue::fromStringArray(const String * in, const size_t arraySize)
{
	data.resize(arraySize);

	for (unsigned int x = 0; x < arraySize; ++x)
	{
		data[x] = in[x].toBool();
	}
}

void GLOWE::BoolArrayValue::fromStringArray(const Vector<String>& in)
{
	data.resize(in.size());

	for (unsigned int x = 0; x < in.size(); ++x)
	{
		data[x] = in[x].toBool();
	}
}

void GLOWE::StringValue::fromString(const String & in)
{
	data = in;
}

const GLOWE::Value::ValueType GLOWE::StringValue::getType() const
{
	return Value::ValueType::String;
}

const size_t GLOWE::StringValue::getArraySize() const
{
	return data.getSize();
}

void GLOWE::StringValue::asStringArray(Vector<String>& out) const
{
	out.resize(1);
	out.front() = data;
}

void GLOWE::StringValue::fromStringArray(const String * in, const size_t arraySize)
{
	if (arraySize > 0)
	{
		data.clear();

		for (unsigned int x = 0; x < arraySize; ++x)
		{
			data += in[x];
		}
	}
}

void GLOWE::StringValue::fromStringArray(const Vector<String>& in)
{
	if (in.size() > 0)
	{
		data.clear();

		for (const auto& x : in)
		{
			data += x;
		}
	}
}

GLOWE::StringArrayValue::StringArrayValue(const String * arg, const size_t arraySize)
{
	data.resize(arraySize);
	for (size_t x = 0; x < arraySize; ++x)
	{
		data[x] = arg[x];
	}
}

GLOWE::StringArrayValue::StringArrayValue(const Vector<String>& arg)
	: data(arg.begin(), arg.end())
{
}

float GLOWE::StringArrayValue::asFloat() const
{
	if (!data.empty())
	{
		return data.front().toFloat();
	}

	return 0.0f;
}

void GLOWE::StringArrayValue::asFloatArray(Vector<float>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = data[x].toFloat();
	}
}

int GLOWE::StringArrayValue::asInt() const
{
	if (!data.empty())
	{
		return data.front().toInt();
	}

	return 0;
}

void GLOWE::StringArrayValue::asIntArray(Vector<int>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = data[x].toInt();
	}
}

unsigned int GLOWE::StringArrayValue::asUInt() const
{
	if (!data.empty())
	{
		return data.front().toUInt();
	}

	return 0U;
}

void GLOWE::StringArrayValue::asUIntArray(Vector<unsigned int>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = data[x].toUInt();
	}
}

double GLOWE::StringArrayValue::asDouble() const
{
	if (!data.empty())
	{
		return data.front().toDouble();
	}

	return 0.0;
}

void GLOWE::StringArrayValue::asDoubleArray(Vector<double>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = data[x].toDouble();
	}
}

GLOWE::String GLOWE::StringArrayValue::asString() const
{
	String result = u8"[";

	for (const auto& x : data)
	{
		result += u8"\"\"";
		result += x;
		result += u8"\"\", ";
	}

	result += u8"]";

	return result;
}

void GLOWE::StringArrayValue::asString(String & out) const
{
	out = u8"[";

	for (const auto& x : data)
	{
		out += u8"\"\"";
		out += x;
		out += u8"\"\", ";
	}

	out += u8"]";
}

bool GLOWE::StringArrayValue::asBool() const
{
	if (!data.empty())
	{
		return data.front().toBool();
	}

	return false;
}

void GLOWE::StringArrayValue::asBoolArray(Vector<bool>& out) const
{
	out.resize(data.size());
	for (size_t x = 0; x < data.size(); ++x)
	{
		out[x] = data[x].toBool();
	}
}

void GLOWE::StringArrayValue::fromFloat(const float & in)
{
	data.resize(1);
	data.front() = toString(in);
}

void GLOWE::StringArrayValue::fromFloatArray(const float * in, const size_t arraySize)
{
	data.resize(arraySize);

	for (unsigned int x = 0; x < arraySize; ++x)
	{
		data[x] = toString(in[x]);
	}
}

void GLOWE::StringArrayValue::fromFloatArray(const Vector<float>& in)
{
	data.resize(in.size());

	for (unsigned int x = 0; x < in.size(); ++x)
	{
		data[x] = toString(in[x]);
	}
}

void GLOWE::StringArrayValue::fromInt(const int & in)
{
	data.resize(1);
	data.front() = toString(in);
}

void GLOWE::StringArrayValue::fromIntArray(const int * in, const size_t arraySize)
{
	data.resize(arraySize);

	for (unsigned int x = 0; x < arraySize; ++x)
	{
		data[x] = toString(in[x]);
	}
}

void GLOWE::StringArrayValue::fromIntArray(const Vector<int>& in)
{
	data.resize(in.size());

	for (unsigned int x = 0; x < in.size(); ++x)
	{
		data[x] = toString(in[x]);
	}
}

void GLOWE::StringArrayValue::fromUInt(const unsigned int & in)
{
	data.resize(1);
	data.front() = toString(in);
}

void GLOWE::StringArrayValue::fromUIntArray(const unsigned int * in, const size_t arraySize)
{
	data.resize(arraySize);

	for (unsigned int x = 0; x < arraySize; ++x)
	{
		data[x] = toString(in[x]);
	}
}

void GLOWE::StringArrayValue::fromUIntArray(const Vector<unsigned int>& in)
{
	data.resize(in.size());

	for (unsigned int x = 0; x < in.size(); ++x)
	{
		data[x] = toString(in[x]);
	}
}

void GLOWE::StringArrayValue::fromDouble(const double & in)
{
	data.resize(1);
	data.front() = toString(in);
}

void GLOWE::StringArrayValue::fromDoubleArray(const double * in, const size_t arraySize)
{
	data.resize(arraySize);

	for (unsigned int x = 0; x < arraySize; ++x)
	{
		data[x] = toString(in[x]);
	}
}

void GLOWE::StringArrayValue::fromDoubleArray(const Vector<double>& in)
{
	data.resize(in.size());

	for (unsigned int x = 0; x < in.size(); ++x)
	{
		data[x] = toString(in[x]);
	}
}

void GLOWE::StringArrayValue::fromBool(const bool & in)
{
	data.resize(1);
	data.front() = toString(in);
}

void GLOWE::StringArrayValue::fromBoolArray(const bool * in, const size_t arraySize)
{
	data.resize(arraySize);

	for (unsigned int x = 0; x < arraySize; ++x)
	{
		data[x] = toString(in[x]);
	}
}

void GLOWE::StringArrayValue::fromBoolArray(const Vector<bool>& in)
{
	data.resize(in.size());

	for (unsigned int x = 0; x < in.size(); ++x)
	{
		data[x] = toString(in[x]);
	}
}

void GLOWE::StringArrayValue::fromString(const String & in)
{
	data.clear();

	static Regex regex(u8"\"\"((?:\\\\\" | [^\"])*)\"\"");
	RegexMatch regexMatch;

	String::ConstIterator it = in.begin();

	while (std::regex_match(it, in.end(), regexMatch, regex))
	{
		it = regexMatch[0].second;
		data.emplace_back(regexMatch[1].first, it);
	}
}

const GLOWE::Value::ValueType GLOWE::StringArrayValue::getType() const
{
	return ValueType::StringArray;
}

const size_t GLOWE::StringArrayValue::getArraySize() const
{
	return data.size();
}

void GLOWE::StringArrayValue::asStringArray(Vector<String>& out) const
{
	out = data;
}

void GLOWE::StringArrayValue::fromStringArray(const String * in, const size_t arraySize)
{
	data = Vector<String>(in, in + arraySize);
}

void GLOWE::StringArrayValue::fromStringArray(const Vector<String>& in)
{
	data = in;
}

GLOWE::Value::ValueConversionException::ValueConversionException()
	: Exception(u8"Value conversion error.")
{
}
