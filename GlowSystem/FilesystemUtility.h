#pragma once
#ifndef GLOWE_FILESYSTEMUTILITY_INCLUDED
#define GLOWE_FILESYSTEMUTILITY_INCLUDED

#include "Time.h"
#include "Error.h"

namespace GLOWE
{
	class GLOWSYSTEM_EXPORT FileStatus
	{
	public:
		enum class FileType
		{
			Unknown = 0,
			Directory,
			Regular,
			Block,
			Character,
			Socket,
			Fifo
		};

		FileType type : 6;
		bool exists : 1;
		bool isSymlink : 1;
	};

	class GLOWSYSTEM_EXPORT FilesystemException
		: public StringException
	{
	public:
		using StringException::StringException;
	};

	struct GLOWSYSTEM_EXPORT FileTimestamp
	{
		Time lastWrite, lastAccess, creation;
	};
}

#endif
