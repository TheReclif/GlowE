#pragma once
#ifndef GLOWE_THREADCLASS_INCLUDED
#define GLOWE_THREADCLASS_INCLUDED

#include "Utility.h"
//#include "Time.h"

namespace GLOWE
{
	using Mutex = std::mutex;
	using RecursiveMutex = std::recursive_mutex;

	using ConditionalVariable = std::condition_variable;
	using ConditionalVariableAny = std::condition_variable_any;

	template<class T>
	using Atomic = std::atomic<T>;

	template<class T>
	using Promise = std::promise<T>;

	// NOT ATOMIC COPYING/MOVING!
	template<class T>
	class AtomicCopyable
		: public Atomic<T>
	{
	public:
		AtomicCopyable() noexcept = default;
		AtomicCopyable(const Atomic<T>& arg) // NOT ATOMIC!
			: Atomic<T>(arg.load()) {};
		AtomicCopyable(const AtomicCopyable& arg) // NOT ATOMIC!
			: Atomic<T>(arg.load()) {};
		constexpr AtomicCopyable(T desired)
			: Atomic<T>(desired) {};
		AtomicCopyable(Atomic<T>&& arg) // NOT ATOMIC!
			: Atomic<T>(arg.exchange(T())) {};
		AtomicCopyable(AtomicCopyable&& arg) // NOT ATOMIC!
			: Atomic<T>(arg.exchange(T())) {};

		AtomicCopyable& operator=(const Atomic<T>& other)
		{
			store(other.load());
			return *this;
		}
		AtomicCopyable& operator=(Atomic<T>&& other)
		{
			store(other.load());
			return *this;
		}
		AtomicCopyable& operator=(const AtomicCopyable& other)
		{
			store(other.load());
			return *this;
		}
		AtomicCopyable& operator=(AtomicCopyable&& other)
		{
			store(other.load());
			return *this;
		}
	};

	template<class MutexType>
	using UniqueLock = std::unique_lock<MutexType>;
	template<class MutexType>
	using LockGuard = std::lock_guard<MutexType>;

	using DefaultUniqueLock = UniqueLock<Mutex>;
	using DefaultLockGuard = LockGuard<Mutex>;

	using DefaultUniqueRecursLock = UniqueLock<RecursiveMutex>;
	using DefaultRecursLockGuard = LockGuard<RecursiveMutex>;

	class GLOWSYSTEM_EXPORT Lockable
		: private NoCopy
	{
	public:
		using UniqueLock = DefaultUniqueRecursLock;
		using Guard = DefaultRecursLockGuard;
	private:
		mutable RecursiveMutex mutex;
	public:
		RecursiveMutex& getMutex() const;

		operator GLOWE::RecursiveMutex&() const;
	};

	template<class MutexType>
	class ReadWriteMutex
	{
	private:
		bool writeFlag;
		unsigned int readersActive, writersWaiting;
		MutexType mutex;
		ConditionalVariableAny cv;
	public:
		ReadWriteMutex();
		~ReadWriteMutex() = default;

		void lockRead();
		void lockWrite();

		void unlockRead();
		void unlockWrite();
	};

	template<class MutexType>
	class ReadWriteLock
	{
	private:
		unsigned char lockType; // 0 - no lock, 1 - read lock, 2 - write lock.
		ReadWriteMutex<MutexType>& mutex;
	public:
		ReadWriteLock(ReadWriteMutex<MutexType>& arg);
		~ReadWriteLock();

		void lockRead();
		void lockWrite();

		void unlock();

		unsigned char getLockType() const; // 0 - no lock, 1 - read lock, 2 - write lock.
	};

	template<class MutexType>
	class RWReadLock
	{
	private:
		ReadWriteLock<MutexType> lk;
	public:
		RWReadLock(ReadWriteMutex<MutexType>& arg);
		~RWReadLock() = default;

		void lock();
		void unlock();
	};

	template<class MutexType>
	class RWWriteLock
	{
	private:
		ReadWriteLock<MutexType> lk;
	public:
		RWWriteLock(ReadWriteMutex<MutexType>& arg);
		~RWWriteLock() = default;

		void lock();
		void unlock();
	};

	class GLOWSYSTEM_EXPORT Thread
	{
	public:
		using Id = std::thread::id;
		using Pointer = Thread*;
	private:
		std::thread threadHandle;

		std::function<void(Thread&)> stopCallback;
		std::atomic<bool> shouldStop;
		Pointer thisPtr;
	public:
		Thread();
		explicit Thread(const Thread&) = delete;
		Thread(Thread&& arg) noexcept;
		template<class Func, class ...Args>
		explicit Thread(Func&& threadFunc, Args&&... args);
		~Thread();

		void setThreadDescription(const wchar_t* name);
		void setStopCallback(const std::function<void(Thread&)>& stopFunc);

		template<class Func, class ...Args>
		void beginThread(Func&& threadFunc, Args&& ... args);

		void stopThread();
		/// @brief Detaches the running thread if it's joinable. Will not call the stop callback.
		void detachThread();
		/// @brief Joins the thread if it's joinable. Will not call the stop callback.
		void joinThread();

		bool checkIsRunning() const;
		bool shouldThreadStop() const;

		Id getThreadID() const;
		static Id getCurrentThreadID();
	};

	template<class Func, class ...Args>
	inline Thread::Thread(Func&& threadFunc, Args&& ...args)
		: Thread()
	{
		static_assert(!(std::is_same<Func, Thread>::value), "Func cannot be Thread.");

		beginThread(std::forward<Func>(threadFunc), std::forward<Args>(args)...);
	}

	// Warning: Func must have at least one argument: Thread::Pointer.
	template<class Func, class ...Args>
	inline void Thread::beginThread(Func&& threadFunc, Args&& ... args)
	{
		if (checkIsRunning())
		{
			return;
		}

		shouldStop = false;

		static_assert(!(std::is_same<Func, Thread>::value), "Func cannot be Thread.");

		threadHandle = std::move(std::thread(std::forward<Func>(threadFunc), thisPtr, std::forward<Args>(args)...));
	}
}

#include "ThreadClass.inl"

#endif