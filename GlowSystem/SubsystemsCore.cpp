#include "SubsystemsCore.h"

int GLOWE::Subsystem::getUnloadOrder() const
{
    return 0;
}

GLOWE::SubsystemsCore& GLOWE::SubsystemsCore::getGlobalCore()
{
	static SubsystemsCore core;
	return core;
}

GLOWE::SubsystemsCore::~SubsystemsCore()
{
	cleanUp();
}

void GLOWE::SubsystemsCore::cleanUp()
{
#if defined(_DEBUG) && defined(_MSC_VER)
	OutputDebugStringA(">>> Unloading subsystems:\n");
#endif
	for (auto& x : subsystemsOrder)
	{
		while (!x.second.empty())
		{
#if defined(_DEBUG) && defined(_MSC_VER)
			for (const auto& y : subsystems)
			{
				if (y.second == x.second.top().get())
				{
					char tempArr[1024] = "";
					sprintf_s(tempArr, "%i", x.first);
					OutputDebugStringA(tempArr);
					OutputDebugStringA(" : ");
					OutputDebugStringA(y.first.name());
					OutputDebugStringA("\n");
					break;
				}
			}
#endif
			x.second.pop();
		}
	}

#if defined(_DEBUG) && defined(_MSC_VER)
	_CrtDumpMemoryLeaks();
#endif
}
