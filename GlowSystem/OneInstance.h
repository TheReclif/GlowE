#pragma once
#ifndef GLOWE_ONE_INSTANCE_INCLUDED
#define GLOWE_ONE_INSTANCE_INCLUDED

#include "BuildTargetInfos.h"
#include "SubsystemsCore.h"

#include <memory>

#ifndef _In_
#define _In_
#endif
#ifndef _In_opt_
#define _In_opt_
#endif
#ifndef _Out_
#define _Out_
#endif

namespace GLOWE
{
	template<class T>
	class OneInstance
	{
	public:
		OneInstance() = delete;
		OneInstance(const OneInstance<T>&) = delete;
		OneInstance(OneInstance<T>&&) = delete;
		~OneInstance() = delete;

		static T& instance();
	};

	template<class T>
	T & OneInstance<T>::instance()
	{
		static T& subsystem = SubsystemsCore::getGlobalCore().getSubsystem<T>();
		return subsystem;
	}

	class GlobalAllocator;
	template<>
	GLOWSYSTEM_EXPORT GlobalAllocator& OneInstance<GlobalAllocator>::instance();

	template<class T>
	inline T& getInstance()
	{
		return OneInstance<T>::instance();
	}
}

#undef GlowDllOneInstance

#endif