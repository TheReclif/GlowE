#include "Time.h"

const GLOWE::Time GLOWE::Time::ZeroTime = GLOWE::Time(0);

GLOWE::Time::Time()
	: microseconds(ZeroTime.asMicroseconds())
{
}

GLOWE::Time::Time(const Int64& timeInMicroseconds)
	: microseconds(timeInMicroseconds)
{
}

float GLOWE::Time::asSeconds() const
{
	return (microseconds / 1000000.0f);
}

long GLOWE::Time::asMilliseconds() const
{
	return (microseconds / 1000);
}

GLOWE::Int64 GLOWE::Time::asMicroseconds() const
{
	return microseconds;
}

GLOWE::Time GLOWE::Time::operator+(const Time& right) const
{
	Time converter(microseconds + right.asMicroseconds());
	return converter;
}

GLOWE::Time& GLOWE::Time::operator+=(const Time& right)
{
	microseconds += right.microseconds;
	return *this;
}

GLOWE::Time GLOWE::Time::operator-(const Time& right) const
{
	Time converter(microseconds - right.asMicroseconds());
	return converter;
}

GLOWE::Time& GLOWE::Time::operator-=(const Time& right)
{
	microseconds -= right.microseconds;
	return *this;
}

GLOWE::Time GLOWE::Time::operator*(const Time& right) const
{
	Time converter(microseconds*right.asMicroseconds());
	return converter;
}

GLOWE::Time& GLOWE::Time::operator*=(const Time& right)
{
	microseconds *= right.microseconds;
	return *this;
}

GLOWE::Time GLOWE::Time::operator/(const Time& right) const
{
	Time converter(microseconds / right.asMicroseconds());
	return converter;
}

GLOWE::Time& GLOWE::Time::operator/=(const Time& right)
{
	microseconds /= right.microseconds;
	return *this;
}

bool GLOWE::Time::operator==(const Time& right) const
{
	return (microseconds == right.microseconds);
}

bool GLOWE::Time::operator!=(const Time& right) const
{
	return (microseconds != right.microseconds);
}

bool GLOWE::Time::operator==(long long right) const
{
	return (microseconds == right);
}

bool GLOWE::Time::operator!=(long long right) const
{
	return (microseconds != right);
}

bool GLOWE::Time::operator>(const Time& right) const
{
	return (microseconds>right.microseconds);
}

bool GLOWE::Time::operator<(const Time& right) const
{
	return (microseconds<right.microseconds);
}

bool GLOWE::Time::operator>=(const Time& right) const
{
	return (microseconds >= right.microseconds);
}

bool GLOWE::Time::operator<=(const Time& right) const
{
	return (microseconds <= right.microseconds);
}

GLOWE::Time GLOWE::Time::fromSeconds(float arg)
{
	return Time(static_cast<Int64>(arg * 1000000.0f));
}

GLOWE::Time GLOWE::Time::fromMilliseconds(unsigned long long arg)
{
	return Time(arg * 1000);
}

GLOWE::Time GLOWE::Time::fromMicroseconds(unsigned long long arg)
{
	return Time(arg);
}

GLOWE::Time GLOWE::Clock::getCurrentTime() const
{
#ifndef GLOWE_COMPILE_FOR_WINDOWS
	return std::chrono::high_resolution_clock::now().time_since_epoch().count() / 1000LL;
#else
	LARGE_INTEGER freq, zuza;
	if (!QueryPerformanceFrequency(&freq) || !QueryPerformanceCounter(&zuza))
	{
		return Time();
	}

	return Time::fromMicroseconds(zuza.QuadPart / (freq.QuadPart / 1000000));
#endif
}

GLOWE::Clock::Clock()
	: startTime(getCurrentTime())
{
}

GLOWE::Time GLOWE::Clock::getAndReset()
{
	Time converter(getElapsedTime());
	reset();
	return converter;
}

void GLOWE::Clock::reset()
{
	startTime = getCurrentTime();
}

GLOWE::Time GLOWE::Clock::getElapsedTime() const
{
	return getCurrentTime() - startTime;
}

void GLOWE::Clock::waitForGivenTime(const Time& time)
{
	std::this_thread::sleep_for(std::chrono::microseconds(time.asMicroseconds()));
}

void GLOWE::Clock::yieldTime()
{
	std::this_thread::yield();
}

void GLOWE::PausableClock::reset()
{
	actualInstance.reset();
	prevInstance.reset();
	pausedTime = Time::ZeroTime;
	isPaused = false;
}

void GLOWE::PausableClock::pause()
{
	if (!isPaused)
	{
		isPaused = true;
		prevInstance.reset();
	}
}

void GLOWE::PausableClock::unpause()
{
	if (isPaused)
	{
		isPaused = false;
		pausedTime += prevInstance.getAndReset();
	}
}

GLOWE::Time GLOWE::PausableClock::getElapsedTime()
{
	if (isPaused)
	{
		return (actualInstance.getElapsedTime() - pausedTime) + prevInstance.getElapsedTime();
	}

	return (actualInstance.getElapsedTime() - pausedTime);
}

bool GLOWE::PausableClock::getIsPaused()
{
	return isPaused;
}

long long GLOWE::PausableClock::getElapsedTimeAsMicroseconds()
{
	return getElapsedTime().asMicroseconds();
}

float GLOWE::PausableClock::getElapsedTimeAsSeconds()
{
	return getElapsedTime().asSeconds();
}

long GLOWE::PausableClock::getElapsedTimeAsMilliseconds()
{
	return getElapsedTime().asMilliseconds();
}
