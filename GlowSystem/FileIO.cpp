#include "FileIO.h"
#include "Error.h"

GLOWE::File::File()
{
}

GLOWE::File::File(File && arg) noexcept
	: filestream(std::move(arg.filestream)), usedOpenFlags(std::move(arg.usedOpenFlags)), bomInfo(std::move(arg.bomInfo))
{
	arg.filestream = FileStreamType();
	arg.usedOpenFlags = std::ios_base::openmode();
	arg.bomInfo = BOMInformation();
}

GLOWE::File::~File()
{
	this->close();
}

void GLOWE::File::open(const GLOWE::String& filename, const unsigned int flags, bool saveFlagsIfFail)
{
	filestream.open(filename.getCharArray(), static_cast<std::ios_base::openmode>(flags));

	if (!checkIsOpen())
	{
		if (saveFlagsIfFail)
		{
			usedOpenFlags = static_cast<std::ios_base::openmode>(flags);
		}

		return;
	}

	if (flags & OpenFlag::In)
	{
		bomInfo.checkBOM(filestream);
		filestream.seekg(bomInfo.getBOMSize());
	}

	usedOpenFlags = static_cast<std::ios_base::openmode>(flags);
}

void GLOWE::File::close()
{
	if (checkIsOpen())
	{
		filestream.close();
	}
}

void GLOWE::File::movePointer(const std::size_t newPos, const FilePointer whichPointer, const MovePointerFlags flags)
{
	if ((usedOpenFlags & std::ios_base::in) && (whichPointer != FilePointer::Write))
	{
		switch (flags)
		{
		case MovePointerFlags::Begin:
		{
			filestream.seekg(bomInfo.getBOMSize(), std::ios_base::beg);
		}
		break;
		case MovePointerFlags::End:
		{
			filestream.seekg(0, std::ios_base::end);
		}
		break;
		default:
		{
			filestream.seekg(newPos + bomInfo.getBOMSize());
		}
		break;
		}
	}

	if ((usedOpenFlags & std::ios_base::out) && (whichPointer != FilePointer::Read))
	{
		switch (flags)
		{
		case MovePointerFlags::Begin:
		{
			filestream.seekp(0, std::ios_base::beg);
		}
		break;
		case MovePointerFlags::End:
		{
			filestream.seekp(0, std::ios_base::end);
		}
		break;
		default:
		{
			filestream.seekp(newPos);
		}
		break;
		}
	}
}

std::size_t GLOWE::File::getPointerPosition(const FilePointer whichPointer)
{
	switch (whichPointer)
	{
	case FilePointer::Read:
		return (std::size_t)(filestream.tellg()) - bomInfo.getBOMSize();
		break;
	case FilePointer::Write:
		return filestream.tellp();
		break;
	default:
		return 0;
		break;
	}
}

std::size_t GLOWE::File::getFileSize()
{
	std::size_t result = 0;
	if (checkIsOpen())
	{
		if (usedOpenFlags & std::ios::in)
		{
			const std::size_t oldPos = getPointerPosition(FilePointer::Read);
			movePointer(0, FilePointer::Read, MovePointerFlags::End);
			result = getPointerPosition(FilePointer::Read);
			movePointer(oldPos, FilePointer::Read);
		}
		else
		{
			const std::size_t oldPos = getPointerPosition(FilePointer::Write);
			movePointer(0, FilePointer::Write, MovePointerFlags::End);
			result = getPointerPosition(FilePointer::Write);
			movePointer(oldPos, FilePointer::Write);
		}
	}

	return result;
}

GLOWE::File & GLOWE::File::write(const void* ptr, const std::size_t howMany)
{
	filestream.write((const char*)ptr, howMany);
	return (*this);
}

GLOWE::File & GLOWE::File::read(void* ptr, const std::size_t howMany)
{
	filestream.read((char*)ptr, howMany);
	return (*this);
}

GLOWE::File::FileStreamType & GLOWE::File::getStream()
{
	return filestream;
}

bool GLOWE::File::checkIsOpen() const
{
	return filestream.is_open();
}

GLOWE::File::operator bool() const
{
	return filestream.is_open() && (!filestream.eof());
}

std::ios_base::openmode GLOWE::File::getUsedOpenFlags() const
{
	return usedOpenFlags;
}

GLOWE::File::BOMInformation GLOWE::File::getBOMInfo() const
{
	return bomInfo;
}

GLOWE::String GLOWE::File::readAll(const String& filename, const bool binary)
{
	File file;
	file.open(filename, File::In | (binary ? File::Binary : 0));
	if (!file.checkIsOpen())
	{
		//throw FileIOException("Unable to open file " + filename);
		throw StringException("Unable to open file " + filename);
	}
	String result(file.getFileSize());
	file.read(result.getCharArray(), result.getSize());
	return result;
}

void GLOWE::File::BOMInformation::checkBOM(std::istream & str)
{
	bomSize = 0;
	encoding = FileEncoding::ASCII;

	size_t oldPos = str.tellg();
	str.seekg(0);
	char a = str.get(), b = str.get(), c = str.get();

	if (a == (char)0xEF && b == (char)0xBB && c == (char)0xBF)
	{
		encoding = FileEncoding::Utf8;
		bomSize = 3;
	}
	else if ((a == (char)0xFE && b == (char)0xFF)
		|| (a == (char)0xFF && b == (char)0xFE))
	{
		if (a == (char)0xFE)
		{
			encoding = FileEncoding::Utf16BigEndian;
		}
		else
		{
			encoding = FileEncoding::Utf16LittleEndian;
		}

		bomSize = 2;
	}
	else
	{
		char d = str.get();
		if ((a == (char)0x00 && b == (char)0x00 && c == (char)0xFE && d == (char)0xFF)
			|| (a == (char)0xFF && b == (char)0xFE && c == (char)0x00 && d == (char)0x00))
		{
			if (a == (char)0x00)
			{
				encoding = FileEncoding::Utf32BigEndian;
			}
			else
			{
				encoding = FileEncoding::Utf32LittleEndian;
			}

			bomSize = 4;
		}
	}

	str.seekg(oldPos);
}

GLOWE::String GLOWE::File::BOMInformation::fileEncodingToString(const FileEncoding & arg)
{
#define FEncCase(FilEnc) \
case FileEncoding::FilEnc: \
	return GlowU8Stringify(FilEnc); \
	break;

	switch (arg)
	{
		FEncCase(ASCII);
		FEncCase(Utf8);
		FEncCase(Utf16LittleEndian);
		FEncCase(Utf16BigEndian);
		FEncCase(Utf32LittleEndian);
		FEncCase(Utf32BigEndian);
	}

	return String();

#undef FEncCase
}
