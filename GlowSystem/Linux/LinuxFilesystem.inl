#pragma once
#ifndef GLOWE_LINUX_FILESYSTEM_INL_INCLUDED
#define GLOWE_LINUX_FILESYSTEM_INL_INCLUDED

#include "LinuxFilesystem.h"

#include <fnmatch.h>
#include <dirent.h>

// TODO: Remove mocks and review (append).

template<class PathType>
void GLOWE::Linux::FilePathIterator<PathType>::updateSubpath()
{
	String str;
	std::size_t prefEnd = fullPath->prefixEnd(), size = fullPath->getSize();

	if (size <= currentOffset)
	{
	}
	else if (currentOffset < prefEnd)
	{
		str = fullPath->path.getSubstring(0, prefEnd);
	}
	/*
	else if (currentOffset == prefEnd
		&& prefEnd < size
		&& LinuxFilesystemUtility::isSeparator(fullPath->path[prefEnd]))
	{
		str = LinuxFilesystemUtility::preferredSeparator;
	}
	*/
	else
	{
		size_t nextSlash = 0, nextThing = 0;

		while ((currentOffset + nextSlash < size) && (LinuxFilesystemUtility::isSeparator(fullPath->path[currentOffset + nextSlash])))
		{
			++nextSlash;
		}

		while ((currentOffset + nextSlash + nextThing < size) && (!LinuxFilesystemUtility::isSeparator(fullPath->path[currentOffset + nextSlash + nextThing])))
		{
			++nextThing;
		}

		if (nextThing > 0)
		{
			str = fullPath->path.getSubstring(currentOffset + nextSlash, nextThing);
		}
		else if (nextSlash > 0)
		{
			str = LinuxFilesystemUtility::period;
		}
	}

	currentElement = str;
}

template<class PathType>
inline GLOWE::Linux::FilePathIterator<PathType>::FilePathIterator()
	: fullPath(nullptr), currentOffset(0)
{
}

template<class PathType>
inline GLOWE::Linux::FilePathIterator<PathType>::FilePathIterator(const PathType& path, const std::size_t& offset)
	: fullPath(&path), currentOffset(offset)
{
	updateSubpath();
}

template<class PathType>
inline const PathType& GLOWE::Linux::FilePathIterator<PathType>::operator*() const
{
	return currentElement;
}

template<class PathType>
inline const PathType* GLOWE::Linux::FilePathIterator<PathType>::operator->() const
{
	return &currentElement;
}

template<class PathType>
GLOWE::Linux::FilePathIterator<PathType>& GLOWE::Linux::FilePathIterator<PathType>::operator++()
{
	std::size_t prefixEnd = fullPath->prefixEnd(), size = fullPath->path.getSize();

	if (currentOffset < prefixEnd)
	{
		currentOffset = prefixEnd + 1;
	}
	else if (currentOffset == prefixEnd && prefixEnd < size
		&& LinuxFilesystemUtility::isSeparator(fullPath->path[prefixEnd]))
	{
		do
		{
			++currentOffset;
		} while (currentOffset < size
			&& !(LinuxFilesystemUtility::isSeparator(fullPath->path[currentOffset])));
	}
	else
	{
		while (currentOffset < size
			&& (LinuxFilesystemUtility::isSeparator(fullPath->path[currentOffset])))
		{
			++currentOffset;
		}

		while (currentOffset < size
			&& !(LinuxFilesystemUtility::isSeparator(fullPath->path[currentOffset])))
		{
			++currentOffset;
		}
	}

	updateSubpath();

	return *this;
}

template<class PathType>
inline GLOWE::Linux::FilePathIterator<PathType> GLOWE::Linux::FilePathIterator<PathType>::operator++(int)
{
	FilePathIterator<PathType> it = *this;
	++(*this);
	return it;
}

template<class PathType>
inline GLOWE::Linux::FilePathIterator<PathType>& GLOWE::Linux::FilePathIterator<PathType>::operator--()
{
	const std::size_t offsetCopy = currentOffset;
	std::size_t backOffset = 0;

	currentOffset = 0;

	do
	{
		backOffset = currentOffset;
		++(*this);
	} while (currentOffset < offsetCopy);

	currentOffset = backOffset;
	updateSubpath();

	return *this;
}

template<class PathType>
inline GLOWE::Linux::FilePathIterator<PathType> GLOWE::Linux::FilePathIterator<PathType>::operator--(int)
{
	FilePathIterator<PathType> it = *this;
	--(*this);
	return it;
}

template<class PathType>
inline bool GLOWE::Linux::FilePathIterator<PathType>::operator==(const FilePathIterator& right) const
{
	return (fullPath == right.fullPath) && (currentOffset == right.currentOffset);
}

template<class PathType>
inline bool GLOWE::Linux::FilePathIterator<PathType>::operator!=(const FilePathIterator& right) const
{
	return !(*this == right);
}

inline GLOWE::Linux::FilePath::FilePath(const String::BasicChar* charArray)
	: path(charArray)
{
	replacePreferredSeparators();
}

inline GLOWE::Linux::FilePath::FilePath(const String& arg)
	: path(arg)
{
	replacePreferredSeparators();
}

inline void GLOWE::Linux::FilePath::clear()
{
	path.clear();
}

inline GLOWE::Linux::FilePath::Iterator GLOWE::Linux::FilePath::begin() const
{
	return Iterator(*this, 0);
}

inline GLOWE::Linux::FilePath::Iterator GLOWE::Linux::FilePath::end() const
{
	return Iterator(*this, path.getSize());
}

inline void GLOWE::Linux::FilePath::append(const String& arg)
{
	GLOWE::String str(arg.begin(), arg.end());

	for (auto& x : str)
	{
		if (LinuxFilesystemUtility::isSeparator(x))
		{
			x = LinuxFilesystemUtility::preferredSeparator;
		}
	}

	const bool isLastColon = false,
		isPathLastCharSeparator = !path.isEmpty() ? LinuxFilesystemUtility::isSeparator(path.getLastChar()) : false,
		isStrFirstCharSeparator = !str.isEmpty() ? LinuxFilesystemUtility::isSeparator(str.getFirstChar()) : false;
	/*
	if (path.getSize() > 0 && !LinuxFilesystemUtility::isSeparator(path.getLastChar()))
	{
		path.append(1, LinuxFilesystemUtility::preferredSeparator);
	}
	*/

	if (isPathLastCharSeparator && isStrFirstCharSeparator)
	{
		path.resize(path.getSize() - 1);
	}
	if (!isPathLastCharSeparator && !isStrFirstCharSeparator && !isLastColon && !path.isEmpty())
	{
		path.append(1, LinuxFilesystemUtility::preferredSeparator);
	}

	if (//path.getSize() > 0 &&
		str.getSize() > 0
		&& isLastColon
		&& !LinuxFilesystemUtility::isSeparator(str.getLastChar())
		&& !isStrFirstCharSeparator)
	{
		str.append(1, LinuxFilesystemUtility::preferredSeparator);
	}

	path.append(str);
}

inline void GLOWE::Linux::FilePath::append(const FilePath& arg)
{
	append(arg.path);
}

inline GLOWE::Linux::FilePath GLOWE::Linux::FilePath::getFilename() const
{
	String::ConstReverseIterator revIt = path.rbegin();
	String::ConstReverseIterator pathEnd = path.rend();
	String result;

	for (; revIt != pathEnd && !(LinuxFilesystemUtility::isSeparator(*revIt)); ++revIt)
	{
		result += *(revIt);
	}

	return FilePath(String(result.rbegin(), result.rend()));
}

inline GLOWE::Linux::FilePath GLOWE::Linux::FilePath::getExtension() const
{
	String str = getFilename();
	std::size_t findResult = str.rfind(LinuxFilesystemUtility::period);

	return (findResult == String::invalidPos
		|| str.getSize() == 1
		|| (str.getSize() == 2 && str.getFirstChar() == LinuxFilesystemUtility::period && str[1] == LinuxFilesystemUtility::period)) ?
		FilePath() :
		FilePath(str.getSubstring(findResult));
}

inline bool GLOWE::Linux::FilePath::hasFilename() const
{
	return !(getFilename().isEmpty());
}

inline bool GLOWE::Linux::FilePath::hasExtension() const
{
	return !(getExtension().isEmpty());
}

inline bool GLOWE::Linux::FilePath::hasRootDirectory() const
{
	return !(getRootDirectory().isEmpty());
}

inline bool GLOWE::Linux::FilePath::hasRootName() const
{
	return !(getRootName().isEmpty());
}

inline bool GLOWE::Linux::FilePath::hasRootPath() const
{
	return !(getRootPath().isEmpty());
}

inline bool GLOWE::Linux::FilePath::hasStem() const
{
	return !(getStem().isEmpty());
}

inline bool GLOWE::Linux::FilePath::hasRelativePath() const
{
	return !(getRelativePath().isEmpty());
}

inline bool GLOWE::Linux::FilePath::hasParentPath() const
{
	return !(getParentPath().isEmpty());
}

inline bool GLOWE::Linux::FilePath::isEmpty() const
{
	return path.isEmpty();
}

inline bool GLOWE::Linux::FilePath::isAbsolute() const
{
	return hasRootName()
		&& hasRootDirectory();
}

inline bool GLOWE::Linux::FilePath::isRelative() const
{
	return !(isAbsolute());
}

inline void GLOWE::Linux::FilePath::removeFilename()
{
	if (!isEmpty() && begin() != --end())
	{
		std::size_t rtEnd = rootEnd(), x = path.getSize();

		while (rtEnd < x && !(LinuxFilesystemUtility::isSeparator(path[x - 1])))
		{
			--x;
		}

		while (rtEnd < x && (LinuxFilesystemUtility::isSeparator(path[x - 1])))
		{
			--x;
		}

		path.erase(x);
	}
}

inline void GLOWE::Linux::FilePath::removeExtension()
{
	replaceExtension();
}

inline void GLOWE::Linux::FilePath::replaceFilename(const FilePath& arg)
{
	removeFilename();
	append(arg);
}

inline void GLOWE::Linux::FilePath::replaceExtension(const FilePath& arg)
{
	//String temp = getParentPath().getString() + getStem().getString();
	FilePath temp(getParentPath());
	temp.append(getStem());

	if (!arg.isEmpty()
		&& !(arg.getString().getFirstChar() == LinuxFilesystemUtility::period))
	{
		temp.path.append(String(LinuxFilesystemUtility::period));
	}

	temp.path.append(arg.getString());

	*this = temp;
}

inline void GLOWE::Linux::FilePath::replacePreferredSeparators()
{
	for (auto& x : path)
	{
		if (LinuxFilesystemUtility::isSeparator(x))
		{
			x = LinuxFilesystemUtility::preferredSeparator;
		}
	}
}

inline bool GLOWE::Linux::FilePath::operator==(const FilePath& right) const
{
	return path == right.path;
}

inline bool GLOWE::Linux::FilePath::operator!=(const FilePath& right) const
{
	return path != right.path;
}

inline GLOWE::Linux::FilePath::operator const String& () const
{
	return getString();
}

inline const GLOWE::String& GLOWE::Linux::FilePath::getString() const
{
	return path;
}

inline std::size_t GLOWE::Linux::FilePath::getSize() const
{
	return path.getSize();
}

inline void GLOWE::Linux::FilePath::serialize(LoadSaveHandle& handle) const
{
	path.serialize(handle);
}

inline void GLOWE::Linux::FilePath::deserialize(LoadSaveHandle& handle)
{
	path.deserialize(handle);
}

inline GLOWE::Linux::FilePath GLOWE::Linux::FilePath::getRootDirectory() const
{
	size_t prefEnd = prefixEnd();
	if (prefEnd < path.getSize()
		&& LinuxFilesystemUtility::isSeparator(path[prefEnd]))
	{
		return FilePath(String(1, LinuxFilesystemUtility::preferredSeparator));
	}

	return FilePath();
}

inline GLOWE::Linux::FilePath GLOWE::Linux::FilePath::getRootName() const
{
	return FilePath(path.getSubstring(0, prefixEnd()));
}

inline GLOWE::Linux::FilePath GLOWE::Linux::FilePath::getRootPath() const
{
	return FilePath(String(path.getCharArray(), rootEnd()));
}

inline GLOWE::Linux::FilePath GLOWE::Linux::FilePath::getStem() const
{
	String filename = getFilename();
	filename.resize(filename.getSize() - getExtension().getString().getSize());

	return FilePath(filename);
}

inline GLOWE::Linux::FilePath GLOWE::Linux::FilePath::getRelativePath() const
{
	std::size_t rtEnd = rootEnd();

	while (rtEnd < path.getSize()
		&& LinuxFilesystemUtility::isSeparator(path[rtEnd]))
	{
		++rtEnd;
	}

	return FilePath(path.getSubstring(rtEnd));
}

inline GLOWE::Linux::FilePath GLOWE::Linux::FilePath::getParentPath() const
{
	FilePath result;
	if (!isEmpty())
	{
		Iterator itEnd = --end();
		for (Iterator x = begin(); x != itEnd; ++x)
		{
			result.append(*x);
			// The fuck is this?!?
			//result.append(LinuxFilesystemUtility::preferredSeparator);
		}
	}

	return result;
}

inline GLOWE::Linux::FileListing::FileListing(const FilePath& path)
{
	create(path);
}

inline GLOWE::Linux::FileListing::Iterator GLOWE::Linux::FileListing::begin()
{
	return files.begin();
}

inline GLOWE::Linux::FileListing::ConstIterator GLOWE::Linux::FileListing::begin() const
{
	return files.begin();
}

inline GLOWE::Linux::FileListing::ReverseIterator GLOWE::Linux::FileListing::rbegin()
{
	return files.rbegin();
}

inline GLOWE::Linux::FileListing::ConstReverseIterator GLOWE::Linux::FileListing::rbegin() const
{
	return files.rbegin();
}

inline GLOWE::Linux::FileListing::Iterator GLOWE::Linux::FileListing::end()
{
	return files.end();
}

inline GLOWE::Linux::FileListing::ConstIterator GLOWE::Linux::FileListing::end() const
{
	return files.end();
}

inline GLOWE::Linux::FileListing::ReverseIterator GLOWE::Linux::FileListing::rend()
{
	return files.rend();
}

inline GLOWE::Linux::FileListing::ConstReverseIterator GLOWE::Linux::FileListing::rend() const
{
	return files.rend();
}

inline void GLOWE::Linux::FileListing::create(const FilePath& path)
{
	files.clear();
	findPath = path;

	if (findPath.isEmpty())
	{
		return;
	}

	const String toFind = path.getFilename();
	String where = path.getStem();
	if (!path.hasExtension())
	{
		where.append("/*");
	}

	const auto dirp = opendir(where.getCharArray());
	dirent* dp = nullptr;
	while ((dp = readdir(dirp)) != nullptr)
	{
		if (fnmatch(toFind.getCharArray(), dp->d_name, FNM_NOESCAPE) == 0)
		{
			files.emplace_back(dp->d_name);
		}
	}
	closedir(dirp);
}

inline GLOWE::Linux::FilePath GLOWE::Linux::FileListing::getOriginalPath() const
{
	return findPath;
}

inline std::size_t GLOWE::Linux::FileListing::getSize() const
{
	return files.size();
}

#endif
