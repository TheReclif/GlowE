#include "LinuxFilesystem.h"
#include "../Error.h"

#include <unistd.h>
#include <fcntl.h>
#include <sys/statvfs.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <sys/select.h>
#include <sys/inotify.h>

// TODO: Fix the whole file.

std::size_t GLOWE::Linux::FilePath::prefixEnd() const
{
	std::size_t result = 0;

	if (path.getSize() > 2
		&& LinuxFilesystemUtility::isSeparator(path.getFirstChar()))
	{
		result = 1;
	}

	return result;
}

std::size_t GLOWE::Linux::FilePath::rootEnd() const
{
	size_t result = prefixEnd();
	if (result < path.getSize()
		&& LinuxFilesystemUtility::isSeparator(path[result]))
	{
		++result;
	}

	return result;
}

GLOWE::FileStatus GLOWE::Linux::Filesystem::getFileStatus(const FilePath& path)
{
	FileStatus result;
	result.exists = false;
	result.isSymlink = false;
	result.type = FileStatus::FileType::Unknown;

	if (!path.isEmpty())
	{
		struct stat fileAttrib;
		if (stat(path.getString().getCharArray(), &fileAttrib) < 0)
		{
			return result;
		}

		result.exists = true;
		struct stat buff;
		result.isSymlink = (lstat(path.getString().getCharArray(), &buff) == 0);

		switch (fileAttrib.st_mode & S_IFMT)
		{
		case S_IFSOCK:
			result.type = FileStatus::FileType::Socket;
			break;
		case S_IFREG:
			result.type = FileStatus::FileType::Regular;
			break;
		case S_IFBLK:
			result.type = FileStatus::FileType::Block;
			break;
		case S_IFDIR:
			result.type = FileStatus::FileType::Directory;
			break;
		case S_IFCHR:
			result.type = FileStatus::FileType::Character;
			break;
		case S_IFIFO:
			result.type = FileStatus::FileType::Fifo;
			break;
		}
	}

	return result;
}

bool GLOWE::Linux::Filesystem::isExisting(const FilePath& path)
{
	return getFileStatus(path).exists;
}

bool GLOWE::Linux::Filesystem::isRegularFile(const FilePath& path)
{
	return getFileStatus(path).type == FileStatus::FileType::Regular;
}

bool GLOWE::Linux::Filesystem::isSymLink(const FilePath& path)
{
	return getFileStatus(path).isSymlink;
}

bool GLOWE::Linux::Filesystem::isDirectory(const FilePath& path)
{
	return getFileStatus(path).type == FileStatus::FileType::Directory;
}

bool GLOWE::Linux::Filesystem::isEmpty(const FilePath& path)
{
	return getFileSize(path) == 0;
}

bool GLOWE::Linux::Filesystem::isBlockFile(const FilePath& path)
{
	return getFileStatus(path).type == FileStatus::FileType::Block;
}

bool GLOWE::Linux::Filesystem::isCharFile(const FilePath& path)
{
	return getFileStatus(path).type == FileStatus::FileType::Character;
}

bool GLOWE::Linux::Filesystem::isSocket(const FilePath& path)
{
	return getFileStatus(path).type == FileStatus::FileType::Socket;
}

bool GLOWE::Linux::Filesystem::isFifo(const FilePath& path)
{
	return getFileStatus(path).type == FileStatus::FileType::Fifo;
}

bool GLOWE::Linux::Filesystem::isOther(const FilePath& path)
{
	FileStatus temp = getFileStatus(path);
	return temp.exists
		&& !(temp.isSymlink)
		&& (temp.type != FileStatus::FileType::Directory)
		&& (temp.type != FileStatus::FileType::Regular);
}

GLOWE::Linux::Filesystem::FileSize GLOWE::Linux::Filesystem::getFileSize(const FilePath& path)
{
	struct stat64 fileAttr;

	if (stat64(path.getString().getCharArray(), &fileAttr) >= 0)
	{
		return fileAttr.st_size;
	}

	return 0;
}

GLOWE::Linux::Filesystem::FileSize GLOWE::Linux::Filesystem::getFreeSpace(const FilePath& path)
{
	struct statvfs64 diskInfo;

	if (statvfs64(path.getString().getCharArray(), &diskInfo) >= 0)
	{
		return diskInfo.f_bsize * diskInfo.f_bfree;
	}

	return 0;
}

void GLOWE::Linux::Filesystem::createDirectory(const FilePath& path)
{
	if (!isExisting(path) && !isDirectory(path))
	{
		mkdir(path.getString().getCharArray(), 0);
	}
}

void GLOWE::Linux::Filesystem::createSymlink(const FilePath& from, const FilePath& to)
{
	if (isExisting(from))
	{
		symlink(to.getString().getCharArray(), from.getString().getCharArray());
	}
}

void GLOWE::Linux::Filesystem::createDirectorySymlink(const FilePath& from, const FilePath& to)
{
	// TODO: Check
	createSymlink(from, to);
}

GLOWE::Linux::FilePath GLOWE::Linux::Filesystem::createAbsolutePath(const FilePath& path)
{
	FilePath result, relativePath = path.getRelativePath(), root = path.getRootPath();

	if (root.isEmpty())
	{
		// If it doesn't contain a root, we add current working directory for algorithm to work properly (it needs the whole, working version of an absolute path).
		relativePath = Filesystem::getCurrentWorkingDir().getString() + relativePath;
	}

	for (auto it = relativePath.begin(), end = relativePath.end(); it != end; ++it)
	{
		const auto stem = it->getStem();
		if (stem == "..")
		{
			result = result.getParentPath();
		}
		else if (stem == ".")
		{
			continue;
		}
		else
		{
			result.append(*it);
		}
	}

	root.append(result);
	return root;
}

GLOWE::FileTimestamp GLOWE::Linux::Filesystem::getTimeStamp(const FilePath& path)
{
	FileTimestamp result;
	if (!tryGetTimeStamp(path, result))
	{
		throw FilesystemException("Unable to get timestamp for \"" + path.getString() + "\"");
	}
	return result;
}

static GLOWE::Time timestampToTime(const struct statx_timestamp& stamp)
{
	using namespace GLOWE;
	return static_cast<UInt64>((stamp.tv_sec * static_cast<Int64>(1e6)) + (static_cast<Int64>(stamp.tv_nsec) / static_cast<Int64>(1e3)));
}

bool GLOWE::Linux::Filesystem::tryGetTimeStamp(const FilePath& path, FileTimestamp& output)
{
	struct statx times;
	// https://man7.org/linux/man-pages/man2/statx.2.html
	if (statx(AT_FDCWD, path.getString().getCharArray(), 0, STATX_BTIME | STATX_ATIME | STATX_MTIME, &times) != 0)
	{
		return false;
	}

	output.creation = timestampToTime(times.stx_btime);
	output.lastAccess = timestampToTime(times.stx_atime);
	output.lastWrite = timestampToTime(times.stx_mtime);

	return true;
}

GLOWE::Linux::FilePath GLOWE::Linux::Filesystem::readSymlinkDestination(const FilePath& path)
{
	if (isExisting(path))
	{
		char* const realPath = realpath(path.getString().getCharArray(), nullptr);
		if (realPath)
		{
			FilePath result(realPath);
			free(realPath);
			return result;
		}
	}

	return FilePath();
}

void GLOWE::Linux::Filesystem::resize(const FilePath& path, const FileSize& newSize)
{
	if (isRegularFile(path))
	{
		truncate64(path.getString().getCharArray(), newSize);
	}
}

void GLOWE::Linux::Filesystem::copy(const FilePath& source, const FilePath& destination)
{
	if (isExisting(source))
	{
		const auto fileSize = getFileSize(source);
		const int src = open64(source.getString().getCharArray(), O_RDONLY);
		if (src <= 0)
		{
			return;
		}
		const int dest = open64(destination.getString().getCharArray(), O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
		if (dest <= 0)
		{
			close(src);
			return;
		}

		off64_t tempOff = 0;
		sendfile64(src, dest, &tempOff, fileSize);

		close(src);
		close(dest);
	}
}

void GLOWE::Linux::Filesystem::copySymlink(const FilePath& source, const FilePath& destination)
{
	if (isExisting(source))
	{
		const auto realPath = readSymlinkDestination(source);
		createSymlink(destination, realPath);
	}
}

void GLOWE::Linux::Filesystem::move(const FilePath& from, const FilePath& destination)
{
	std::rename(from.getString().getCharArray(), destination.getString().getCharArray());
}

void GLOWE::Linux::Filesystem::rename(const FilePath& from, const FilePath& destination)
{
	move(from, destination);
}

void GLOWE::Linux::Filesystem::remove(const FilePath& path)
{
	if (isExisting(path))
	{
		std::remove(path.getString().getCharArray());
	}
}

void GLOWE::Linux::Filesystem::removeDirectory(const FilePath& path)
{
	if (isExisting(path))
	{
		rmdir(path.getString().getCharArray());
	}
}

GLOWE::Linux::FilePath GLOWE::Linux::Filesystem::getTempPath()
{
	return "/temp";
}

GLOWE::Linux::FilePath GLOWE::Linux::Filesystem::getCurrentWorkingDir()
{
	char* const buff = getcwd(nullptr, 0);
	if (buff)
	{
		String result(buff);
		free(buff);
		return result;
	}

	return "";
}

void GLOWE::Linux::Filesystem::setCurrentWorkingDir(const FilePath& newDir)
{
	if (!newDir.isEmpty())
	{
		chdir(newDir.getString().getCharArray());
	}
}

GLOWE::Linux::FilesystemWatcher::FilesystemWatcher()
	: watchInterface(inotify_init1(IN_NONBLOCK)), watcher(-1), observedPath(), lastChange(), options()
{
}

GLOWE::Linux::FilesystemWatcher::FilesystemWatcher(const FilePath& path, const unsigned int options)
	: FilesystemWatcher()
{
	beginObserving(path, options);
}

GLOWE::Linux::FilesystemWatcher::~FilesystemWatcher()
{
	stopObserving();
	close(watcher);
}

uint32_t gloweOptionsToMask(const unsigned int options)
{
	using Option = GLOWE::Linux::FilesystemWatcher::Option;
	uint32_t result = 0;
	if (options & Option::FileNameChange)
	{
		result |= IN_MOVE | IN_CREATE | IN_DELETE | IN_MODIFY;
	}
	if (options & Option::DirectoryNameChange)
	{
		result |= IN_MOVE_SELF;
	}
	if (options & Option::AttributesChange)
	{
		result |= IN_ATTRIB;
	}
	if (options & Option::SizeChange)
	{
		result |= IN_MODIFY;
	}
	if (options & Option::LastWriteChange)
	{
		result |= IN_MODIFY;
	}
	if (options & Option::SecurityChange)
	{
		result |= IN_ATTRIB;
	}

	return result;
}

void GLOWE::Linux::FilesystemWatcher::beginObserving(const FilePath& path, const unsigned int ops)
{
	stopObserving();

	watcher = inotify_add_watch(watchInterface, path.getString().getCharArray(), gloweOptionsToMask(ops));
}

void GLOWE::Linux::FilesystemWatcher::stopObserving()
{
	if (watcher >= 0)
	{
		inotify_rm_watch(watchInterface, watcher);
		watcher = -1;
	}
}

bool GLOWE::Linux::FilesystemWatcher::observe(const Time& howLong)
{
	if (watcher < 0)
	{
		return false;
	}

	if (read(watcher, nullptr, 0) & IN_IGNORED)
	{
		stopObserving();
		return false;
	}

	fd_set set;
	FD_ZERO(&set);
	FD_SET(watcher, &set);
	timeval tempTimeout;

	tempTimeout.tv_sec = howLong.asMicroseconds() / 1000000;
	tempTimeout.tv_usec = howLong.asMicroseconds() % 1000000;

	if (select(watcher + 1, &set, nullptr, nullptr, &tempTimeout) > 0)
	{
		char buff[sizeof(struct inotify_event) + NAME_MAX + 1];
		if (read(watcher, &buff, sizeof(struct inotify_event) + NAME_MAX + 1) == 0)
		{
			inotify_event* const e = reinterpret_cast<inotify_event*>(buff);
			if (e->mask & IN_CREATE)
			{
				lastChange.whatChanged = Change::Added;
			}
			else
			if (e->mask & IN_MOVED_FROM || e->mask & IN_DELETE)
			{
				lastChange.whatChanged = Change::Removed;
			}
			else
			if (e->mask & IN_MOVED_TO)
			{
				lastChange.whatChanged = Change::Renamed;
			}
			else
			if (e->mask & IN_MODIFY)
			{
				lastChange.whatChanged = Change::Modified;
			}
			lastChange.filename = e->name;
		}
		else
		{
			stopObserving();
			return false;
		}

		return true;
	}

	return false;
}

GLOWE::Linux::FilesystemWatcher::ChangeInfo GLOWE::Linux::FilesystemWatcher::getLastChange() const
{
	return lastChange;
}

bool GLOWE::Linux::FilesystemWatcher::checkIsValid() const
{
	if (watcher < 0)
	{
		return false;
	}

	return read(watcher, nullptr, 0) == 0;
}

const GLOWE::Linux::FilePath& GLOWE::Linux::FilesystemWatcher::getObservedPath() const
{
	return observedPath;
}

unsigned int GLOWE::Linux::FilesystemWatcher::getOptions() const
{
	return options;
}
