#pragma once
#ifndef GLOWE_LINUX_FILESYSTEMUTILITY_INCLUDED
#define GLOWE_LINUX_FILESYSTEMUTILITY_INCLUDED

#include "glowsystem_export.h"

#include "../String.h"

#ifdef GLOWE_COMPILE_FOR_LINUX
namespace GLOWE
{
	class GLOWSYSTEM_EXPORT LinuxFilesystemUtility
	{
	public:
		static constexpr GLOWE::String::BasicChar period = '.';
		static constexpr GLOWE::String::BasicChar preferredSeparator = '/';
		static constexpr GLOWE::String::BasicChar slash = '/';
		static constexpr GLOWE::String::BasicChar backslash = '\\';

		static inline bool isSeparator(const char& x)
		{
			return x == slash
				|| x == backslash;
		}
	};
}
#endif

#endif