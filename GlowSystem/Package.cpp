#include "Package.h"
#include "Filesystem.h"

#include <zip.h>

GLOWE::Package::Package()
	: file(nullptr), memorySource(nullptr), chosenOpenMode(Package::OpenMode::None), isOpen(false)
{
}

GLOWE::Package::Package(const String & path, const Package::OpenMode& openMode)
	: file(nullptr), memorySource(nullptr), chosenOpenMode(Package::OpenMode::None), isOpen(false)
{
	open(path, openMode);
}

GLOWE::Package::~Package()
{
	close();
}

void GLOWE::Package::open(const String& path, const Package::OpenMode& openMode)
{
	if (isOpen)
	{
		return;
	}

	String finalPath = path;
	if (Filesystem::isSymLink(path))
	{
		finalPath = Filesystem::readSymlinkDestination(path);
	}

	int errorCode = 0;

	if (openMode == Package::OpenMode::Read)
	{
		//Read

		file = zip_open(finalPath.getCharArray(), ZIP_RDONLY, &errorCode);

		if (file == nullptr)
		{
			//OneInstance<LogMgr>::instance() << u8"Can't open archive in read mode." << u8"Archive: " + path << u8"Zip error code: " + toString(errorCode);
			//ErrorThrow(false);
			//WarningThrow(false, u8"Can't open archive in read mode. Archive: " + path + u8"\nZip error code: " + toString(errorCode) + u8".");
			return;
		}
	}
	else
	{
		//Write

		file = zip_open(finalPath.getCharArray(), ZIP_CREATE, &errorCode);

		if (file == nullptr)
		{
			//OneInstance<LogMgr>::instance() << u8"Can't open archive in write mode." << u8"Archive: " + path << u8"Zip error code: " + toString(errorCode);
			//ErrorThrow(false);
			//WarningThrow(false, u8"Can't open archive in write mode. Archive: " + path + u8"\nZip error code: " + toString(errorCode) + u8".");
			return;
		}
	}

	chosenOpenMode = openMode;
	isOpen = true;
	zipName = path;
}

void GLOWE::Package::openFromMemory(const void* data, const UInt64 dataSize)
{
	if (isOpen)
	{
		return;
	}

	memoryBuffer = makeUniquePtr<String>(static_cast<const char*>(data), dataSize);

	zip_error_t err;
	memorySource = zip_source_buffer_create(memoryBuffer->getCharArray(), memoryBuffer->getSize(), false, &err);
	if (memorySource)
	{
		file = zip_open_from_source(memorySource, ZIP_RDONLY, &err);
		if (file)
		{
			chosenOpenMode = OpenMode::Read;
			isOpen = true;
		}
		else
		{
			zip_source_close(memorySource);
			memorySource = nullptr;
			memoryBuffer.release();

			//OneInstance<LogMgr>::instance() << u8"Can't open archive from memory. Zip error code: " + toString(err.zip_err);
			//ErrorThrow(false);
			//WarningThrow(false, u8"Can't open archive from memory. Zip error code: " + toString(err.zip_err) + ". System error code: " + toString(zip_get_error(file)->sys_err));
			return;
		}
	}
	else
	{
		//OneInstance<LogMgr>::instance() << u8"Can't open archive from memory. Zip error code: " + toString(err.zip_err);
		//ErrorThrow(false);
		//WarningThrow(false, u8"Can't open archive from memory. Zip error code: " + toString(err.zip_err) + ". System error code: " + toString(zip_get_error(file)->sys_err));
		return;
	}
}

void GLOWE::Package::openFromMemory(const String& data)
{
	openFromMemory(data.getCharArray(), data.getSize());
}

void GLOWE::Package::close()
{
	if (isOpen)
	{
		if (zip_close(file) == -1)
		{
			//WarningThrow(false, u8"Can't close the zip file. Zip error code: " + toString(zip_get_error(file)->zip_err) + ". System error code: " + toString(zip_get_error(file)->sys_err));
		}
		file = nullptr;
		isOpen = false;
		zipName.clear();
		chosenOpenMode = OpenMode::None;
		objectsToAdd.clear();
		if (memorySource)
		{
			zip_source_close(memorySource);
			memorySource = nullptr;
		}
		memoryBuffer.reset();
	}
}

void GLOWE::Package::applyChanges()
{
	reopen();
}

void GLOWE::Package::reopen()
{
	if (isOpen && !(memoryBuffer))
	{
		String temp = zipName;
		OpenMode tempMode = chosenOpenMode;

		close();
		open(temp, tempMode);
	}
}

void GLOWE::Package::discardChanges()
{
	zip_discard(file);

	file = nullptr;
	isOpen = false;
	zipName = String();
	chosenOpenMode = OpenMode::None;
	objectsToAdd.clear();
	if (memorySource)
	{
		zip_source_close(memorySource);
		memorySource = nullptr;
	}
	memoryBuffer.release();
}

void GLOWE::Package::addDirectory(const String& dirname)
{
	if (dirname.getString().empty() || !isOpen || chosenOpenMode != OpenMode::Write)
	{
		return;
	}

	zip_add_dir(file, dirname.getCharArray());
}

void GLOWE::Package::deleteFile(const String& filename)
{
	if (filename.getString().empty() || !isOpen || chosenOpenMode != OpenMode::Write)
	{
		return;
	}

	auto tempID = zip_name_locate(file, filename.getCharArray(), 0);
	if (tempID == -1)
	{
		//WarningThrow(false, u8"File " + filename + u8" could not be located in the archive.");
		return;
	}

	zip_delete(file, tempID);
}

void GLOWE::Package::renameFile(const String& oldName, const String& newName)
{
	auto tempID = zip_name_locate(file, oldName.getCharArray(), 0);

	if (tempID == -1)
	{
		//WarningThrow(false, u8"File " + oldName + u8" could not be located in the archive.");
		return;
	}

	zip_file_rename(file, tempID, newName.getCharArray(), 0);
}

void GLOWE::Package::addFile(const String& filename, const char * buffer, const UInt64 buffLen)
{
	if (!isOpen || filename.getString().empty() || buffer == nullptr || buffLen == 0 || chosenOpenMode != OpenMode::Write)
	{
		return;
	}

	objectsToAdd.emplace_front(buffer, buffLen);

	zip_source_t* source = zip_source_buffer(file, objectsToAdd.front().getCharArray(), objectsToAdd.front().getSize(), 0);

	if (source == nullptr)
	{
		//WarningThrow(false, u8"Unable to create a new package source.");
		return;
	}

	zip_int64_t temp = zip_file_add(file, filename.getCharArray(), source, ZIP_FL_ENC_GUESS | ZIP_FL_OVERWRITE);

	if (temp == -1)
	{
		//WarningThrow(false, u8"Unable to add a new file to package.");
		return;
	}
}

void GLOWE::Package::addFileFromFile(const String& filenameInZip, const String& filenameOnDisk, const UInt64 len)
{
	if (!isOpen || filenameInZip.getString().empty() || filenameOnDisk.getString().empty()||chosenOpenMode!=OpenMode::Write)
	{
		return;
	}

	zip_source_t* source = zip_source_file(file, filenameOnDisk.getCharArray(), 0, len);

	if (source == nullptr)
	{
		//WarningThrow(false, u8"Unable to create a source from file.");
		return;
	}

	zip_int64_t temp = zip_file_add(file, filenameInZip.getCharArray(), source, ZIP_FL_ENC_GUESS | ZIP_FL_OVERWRITE);

	if (temp == -1)
	{
		//WarningThrow(false, u8"Unable to add a new file to package.");
		return;
	}
}

bool GLOWE::Package::checkFileForExistence(const String& filename) const
{
	if (!file || filename.getString().empty())
	{
		return false;
	}

	const auto result = zip_name_locate(file, filename.getCharArray(), 0);

	return !(result == (-1));
}

GLOWE::Vector<GLOWE::String> GLOWE::Package::listAllFiles() const
{
	if (!file)
	{
		return Vector<String>();
	}

	const UInt64 entries = zip_get_num_entries(file, ZIP_FL_UNCHANGED);
	Vector<String> result(entries);

	for (UInt64 x = 0; x < entries; ++x)
	{
		result[x] = zip_get_name(file, x, ZIP_FL_ENC_GUESS);
	}

	return result;
}

void GLOWE::Package::readFile(const String& filename, String& buff, const String& password)
{
	if (!isOpen || chosenOpenMode != OpenMode::Read)
	{
		return;
	}

	PackageEntryRead entry;
	entry.open(*this, filename, password);
	entry.readAll(buff);
}

GLOWE::PackageEntryRead::PackageEntryRead()
	: entry(nullptr), usedPackage(nullptr), isOpen(false), size(0)
{
}

GLOWE::PackageEntryRead::~PackageEntryRead()
{
	close();
}

void GLOWE::PackageEntryRead::open(Package & package, const String& filename, const String& password)
{
	if (!(package.checkIsOpen()) || isOpen)
	{
		return;
	}

	if (!password.isEmpty())
	{
		entry = zip_fopen_encrypted(package.getRawZipFile(), filename.getCharArray(), ZIP_FL_ENC_GUESS, password.getCharArray());
	}
	else
	{
		entry = zip_fopen(package.getRawZipFile(), filename.getCharArray(), ZIP_FL_ENC_GUESS);
	}

	if (entry == nullptr)
	{
		//WarningThrow(false, u8"Can't open entry from package. Entry: " + filename + ". Provided password: " + password);
		return;
	}

	zip_stat_t stats;
	auto res = zip_stat(package.getRawZipFile(), filename.getCharArray(), 0, &stats);

	if (res != -1)
	{
		size = stats.size;
	}

	usedPackage = &package;
	isOpen = true;
	usedFilename = filename;
}

void GLOWE::PackageEntryRead::close()
{
	if (isOpen)
	{
		zip_fclose(entry);
		entry = nullptr;
		isOpen = false;
		size = 0;
		usedFilename = String();
	}
}

void GLOWE::PackageEntryRead::read(void* data, const unsigned long long howManyBytes)
{
	if (!isOpen || howManyBytes == 0)
	{
		return;
	}

	zip_fread(entry, data, howManyBytes);
}

void GLOWE::PackageEntryRead::readAll(String& buff)
{
	if (!isOpen)
	{
		return;
	}

	buff.resize(size);
	zip_fread(entry, &(buff.getString().front()), size);
}

GLOWE::UInt64 GLOWE::PackageEntryRead::getSize() const
{
	return size;
}

void GLOWE::PackageMgr::closeAllPackagesForFilename(const String& filename)
{
	DefaultLockGuard lock(mutex);
	for (auto& pair : packages)
	{
		auto findIt = pair.second.find(filename);
		if (findIt != pair.second.end())
		{
			findIt->second.close();
		}
	}
}

GLOWE::Package & GLOWE::PackageMgr::getPackage(const String & pathHash)
{
	DefaultUniqueLock lock(mutex);
	Map<String, Package>& temp = packages[Thread::getCurrentThreadID()];
	lock.unlock();
	Package& temp2 = temp[pathHash];

	if (!temp2.checkIsOpen())
	{
		temp2.open(pathHash, Package::OpenMode::Read);
	}

	return temp2;
}

void GLOWE::PackageMgr::declareCustomerThread(const Thread::Id & id)
{
	if (id == Thread::Id())
	{
		packages.insert(std::make_pair(Thread::getCurrentThreadID(), Map<String, Package>()));
	}
	else
	{
		packages.insert(std::make_pair(id, Map<String, Package>()));
	}
}

void GLOWE::PackageMgr::undeclareCustomerThread(const Thread::Id & id)
{
	if (id == Thread::Id())
	{
		packages.erase(Thread::getCurrentThreadID());
	}
	else
	{
		packages.erase(id);
	}
}
