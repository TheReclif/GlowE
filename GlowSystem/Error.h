#pragma once
#ifndef ERRORMGR_INCLUDED
#define ERRORMGR_INCLUDED

#include "Log.h"
#include "Utility.h"
#include "String.h"
#include "UtfStringConverter.h"

#define GLOWE_ERRORCODE_TOTALFAILURE 0x0F
#define GLOWE_ERRORCODE_ASSERTIONFAILED 0xFF

#pragma region
#define WarningThrow(IsGraphicsAPI, WarningInfo) \
{\
	String _desc_ = String(u8"Warning.") + String(u8"\nIs this code using graphics API: ") + String(GlowU8Stringify(IsGraphicsAPI)) + String(u8"\nWarning info: ") + WarningInfo + String(u8"\n");\
	\
	String _whereItHappened_ = String(u8"File: ") + String(__FILE__) + u8" \nFunc: " + String(__func__) + u8"\nLine: " + toString(__LINE__) + String(u8"\n");\
	\
	OneInstance<ErrorMgr>::instance().throwError(_desc_, _whereItHappened_, ErrorMgr::ErrorType::Warning);\
}

#define ErrorThrow(IsGraphicsAPI)\
{\
	String _desc_ = String(u8"Error.") + String(u8"\nIs this code using graphics API: ") + String(GlowU8Stringify(IsGraphicsAPI)) + String(u8"\n"); \
	\
	String _whereItHappened_ = String(u8"File: ") + String(__FILE__) + u8" \nFunc: " + String(__func__) + u8"\nLine: " + toString(__LINE__) + String(u8"\n"); \
	\
	OneInstance<ErrorMgr>::instance().throwError(_desc_, _whereItHappened_, ErrorMgr::ErrorType::Warning);\
}

#define GlowAssert(...) { if (!(__VA_ARGS__)) { throw GLOWE::StringException("Assertion failed: "_str #__VA_ARGS__); }}
#pragma endregion

namespace GLOWE
{
	class GLOWSYSTEM_EXPORT ErrorInfo
	{
	private:
		String place;
		String desc;
	public:
		ErrorInfo(String descArg, String placeArg);
		~ErrorInfo() = default;

		String getErrorPlace();
		String getErrorDesc();
	};

	class GLOWSYSTEM_EXPORT ErrorMgr
		: public Subsystem, public Lockable
	{
	public:
		enum class ErrorType
		{
			Nothing = 0,
			Warning,
			TotalFailure
		};
	private:
		ErrorType errorType;
		LogMgr stdErrorLogMgr;
		String failureDesc;
		String failurePlace;
		bool isFailure;

		void writeErrorDesc();
	public:
		ErrorMgr();
		~ErrorMgr() = default;

		void throwError(const String& desc, const String& place, const ErrorType failureType = ErrorType::TotalFailure);

		ErrorType getErrorType();

		bool checkIsFailure();
	};

	/// @brief Basic exception class, does not rely on dynamic allocation.
	class GLOWSYSTEM_EXPORT Exception
		: public std::exception
	{
	private:
		const char* exceptText;
		String backtrace;
	public:
		Exception();
		Exception(const char* const arg);

		virtual const char* what() const noexcept override;

		const char* getBacktrace() const noexcept;
		const String& getBacktraceStr() const noexcept;
	};

	class GLOWSYSTEM_EXPORT NullPointerException
		: public Exception
	{
	public:
		NullPointerException() noexcept
			: Exception("SafePtr tried to dereference/access null pointer, but was successfully disallowed to do so.") {};
	};

	class GLOWSYSTEM_EXPORT NotImplementedException
		: public Exception
	{
	public:
		NotImplementedException() noexcept
			: Exception("Not implemented.") {};
	};

	/// @brief Exception class that can store a custom exception description.
	class GLOWSYSTEM_EXPORT StringException
		: public Exception
	{
	protected:
		String text;
	public:
		inline StringException()
			: text()
		{}
		inline StringException(const String& str)
			: text(str)
		{}
		inline StringException(String&& str)
			: text(std::move(str))
		{}

		inline virtual const char* what() const noexcept override
		{
			return text.getCharArray();
		}
	};
	
	/// @brief Prints current thread's backtrace while omitting some first entries.
	/// @param entriesToOmit How many entries to omit. By default 2 to avoid bloating the backtrace with internal calls. Pass 2 or more
	/// @return Human readable backtrace
	GLOWSYSTEM_EXPORT String getBacktrace(const unsigned int entriesToOmit = 2);
}

#endif
