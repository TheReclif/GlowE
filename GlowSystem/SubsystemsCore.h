#pragma once
#ifndef GLOWE_SYSTEM_SUBSYSTEMSCORE_INCLUDED
#define GLOWE_SYSTEM_SUBSYSTEMSCORE_INCLUDED

#include "glowsystem_export.h"

#include <map>
#include <stack>
#include <typeindex>
#include <memory>

namespace GLOWE
{
	class SubsystemsCore;

	class GLOWSYSTEM_EXPORT Subsystem
	{
	public:
		virtual ~Subsystem() = default;

		virtual int getUnloadOrder() const;

		friend class SubsystemsCore;
	};

	class SubsystemsCore
	{
	private:
		using Type = std::type_index;
	private:
		std::map<Type, Subsystem*> subsystems;
		std::map<int, std::stack<std::unique_ptr<Subsystem>>, std::greater<int>> subsystemsOrder;
	public:
		GLOWSYSTEM_EXPORT void cleanUp();

		GLOWSYSTEM_EXPORT SubsystemsCore() = default;
		GLOWSYSTEM_EXPORT ~SubsystemsCore();

		/// @brief Used to get (and register, if the subsystem is not present in the core) a reference to the subsystem of the given type.
		/// @tparam T Type of the subsystem to get/register
		/// @return Reference to the subsystem with type T
		template <class T>
		T& getSubsystem()
		{
			static_assert(std::is_base_of<Subsystem, T>::value, "Type must be a subsystem.");

			T* result = static_cast<T*>(subsystems[typeid(T)]);

			if (result == nullptr)
			{
				// Don't worry about result = new T.
				subsystems[typeid(T)] = (result = new T);
				subsystemsOrder[result->getUnloadOrder()].emplace(result);
			}

			return *result;
		}

		/// @brief Used to register the subsystem if the default construction from the new operator is not sufficient.
		/// @tparam T Type of the subsystem
		/// @param subsystem Unique pointer to the subsystem to register
		/// @return Reference to the newly registered subsystem
		template<class T>
		T& registerSubsystem(std::unique_ptr<T>&& subsystem)
		{
			static_assert(std::is_base_of<Subsystem, T>::value, "Type must be a subsystem.");

			T& result = *subsystem;
			subsystems[typeid(T)] = subsystem.get();
			subsystemsOrder[result.getUnloadOrder()].emplace(std::move(subsystem));

			return result;
		}

		static GLOWSYSTEM_EXPORT SubsystemsCore& getGlobalCore();
	};
}

#endif
