#pragma once
#ifndef GLOWE_SYSTEM_NUMBERSTORAGECLASSES_INCLUDED
#define GLOWE_SYSTEM_NUMBERSTORAGECLASSES_INCLUDED

#include <array>
#include <type_traits>

namespace GLOWE
{
#define Storage1DDef(Name, Type) \
	template<unsigned int NumOfElems = 1> \
	using Name = Array1D<Type, NumOfElems>;

#define Storage2DDef(Name, Type) \
	template<unsigned int X, unsigned int Y> \
	using Name = Array2D<Type, X, Y>;

	// 1Ds

	template<class Type, unsigned int NumOfElems = 1>
	using Array1D = std::template array<Type, NumOfElems>;

	Storage1DDef(Float1D, float);
	Storage1DDef(Double1D, double);
	Storage1DDef(Int1D, signed int);
	Storage1DDef(UInt1D, unsigned int);
	Storage1DDef(Byte1D, signed char);
	Storage1DDef(UByte1D, unsigned char);
	Storage1DDef(Short1D, signed short);
	Storage1DDef(UShort1D, unsigned short);

	// 2Ds
	template<class Type, unsigned int X, unsigned int Y>
	using Array2D = std::template array<std::template array<Type, Y>, X>;

	Storage2DDef(Float2D, float);
	Storage2DDef(Double2D, double);
	Storage2DDef(Int2D, signed int);
	Storage2DDef(UInt2D, unsigned int);
	Storage2DDef(Byte2D, signed char);
	Storage2DDef(UByte2D, unsigned char);
	Storage2DDef(Short2D, signed short);
	Storage2DDef(UShort2D, unsigned short);

#undef Storage1DDef
#undef Storage2DDef

	// 1Ds
	using Float2 = Float1D<2>;
	using Float3 = Float1D<3>;
	using Float4 = Float1D<4>;

	using Int2 = Int1D<2>;
	using Int3 = Int1D<3>;
	using Int4 = Int1D<4>;

	using UInt2 = UInt1D<2>;
	using UInt3 = UInt1D<3>;
	using UInt4 = UInt1D<4>;

	using Short2 = Short1D<2>;
	using Short3 = Short1D<3>;
	using Short4 = Short1D<4>;

	using UShort2 = UShort1D<2>;
	using UShort3 = UShort1D<3>;
	using UShort4 = UShort1D<4>;

	// 2Ds
	using Float2x2 = Float2D<2, 2>;
	using Float3x3 = Float2D<3, 3>;
	using Float4x4 = Float2D<4, 4>;

	using Float2x3 = Float2D<2, 3>;
	using Float3x2 = Float2D<3, 2>;

	using Float4x3 = Float2D<4, 3>;
	using Float3x4 = Float2D<3, 4>;
}

#endif
