#pragma once
#ifndef GLOWE_STRINGSTORAGE_INCLUDED
#define GLOWE_STRINGSTORAGE_INCLUDED

#include "Utility.h"
#include "String.h"
#include "Hash.h"

namespace GLOWE
{
	class GLOWSYSTEM_EXPORT StringStorage
	{
	private:
		Map<Hash, String> strings;
	public:
		void add(Hash hsh, const String& str);
		void remove(Hash hsh);

		String& get(Hash hsh);
		const String& get(Hash hsh) const;
	};
}

#endif