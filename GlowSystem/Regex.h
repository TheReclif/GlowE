#pragma once
#ifndef GLOWE_SYSTEM_REGEX_INCLUDED
#define GLOWE_SYSTEM_REGEX_INCLUDED

#include "Utility.h"
#include "String.h"

namespace GLOWE
{
	/*
	template<class CharType>
	class RegexTraits
	{};

	template<>
	class RegexTraits<char>
		: public std::regex_traits<char>
	{
	public:
		using string_type = String::Utf8String;
		using collate_type = std::collate<char>;
	};

	template<>
	class RegexTraits<wchar_t>
		: public std::regex_traits<wchar_t>
	{
	public:
		using string_type = String::WideString;
		using collate_type = std::collate<wchar_t>;
	};
	*/

	template<class CharType>
	using BasicRegex = std::basic_regex<CharType, std::regex_traits<CharType>>;

	using Regex = BasicRegex<char>;
	using WRegex = BasicRegex<wchar_t>;

	using RegexMatch = std::match_results<String::Utf8String::const_iterator, StdAllocator<std::sub_match<String::Utf8String::const_iterator>>>;
	using WRegexMatch = std::match_results<String::WideString::const_iterator, StdAllocator<std::sub_match<String::WideString::const_iterator>>>;

	using RegexSubMatch = std::sub_match<String::Utf8String::const_iterator>;
	using WRegexSubMatch = std::sub_match<String::WideString::const_iterator>;
}

#endif
