#pragma once
#ifndef GLOWE_STRING_INCLUDED
#define GLOWE_STRING_INCLUDED

#include <string>
#include <string_view>
#include <sstream>

#include "MemoryMgr.h"
#include "BinarySerializable.h"
#include "Hash.h"

namespace GLOWE
{
	class GLOWSYSTEM_EXPORT StringView
		: public std::string_view
	{
	public:
		using std::string_view::string_view;

		StringView(const std::string_view& arg)
			: std::string_view(arg)
		{}

		int toInt(const int base = 10) const;
		//long toLong(const int base = 10) const;
		//long long toLongLong(const int base = 10) const;
		unsigned int toUInt(const int base = 10) const;
		//unsigned long toULong(const int base = 10) const;
		//unsigned long long toULongLong(const int base = 10) const;
		float toFloat() const;
		//double toDouble() const;
		//long double toLongDouble() const;
		//bool toBool() const;
	};

	class GLOWSYSTEM_EXPORT String
		: public BinarySerializable, public Hashable
	{
	public:
		using Utf8Char = char;
		using Utf16Char = char16_t;
		using Utf32Char = char32_t;
		using WideChar = wchar_t;

		using BasicChar = Utf8Char;

		using Utf8String = typename std::template basic_string<Utf8Char, std::template char_traits<Utf8Char>, GLOWE::template StdAllocator<Utf8Char>>;
		using Utf16String = typename std::template basic_string<Utf16Char, std::template char_traits<Utf16Char>, GLOWE::template StdAllocator<Utf16Char>>;
		using Utf32String = typename std::template basic_string<Utf32Char, std::template char_traits<Utf32Char>, GLOWE::template StdAllocator<Utf32Char>>;
		using WideString = typename std::template basic_string<WideChar, std::template char_traits<WideChar>, GLOWE::template StdAllocator<WideChar>>;

		using BasicString = Utf8String;

		using Iterator = BasicString::iterator;
		using ConstIterator = BasicString::const_iterator;

		using ReverseIterator = BasicString::reverse_iterator;
		using ConstReverseIterator = BasicString::const_reverse_iterator;

		using IStringStream = typename std::template basic_istringstream<BasicChar, std::template char_traits<BasicChar>, GLOWE::template StdAllocator<BasicChar>>;
		using OStringStream = typename std::template basic_ostringstream<BasicChar, std::template char_traits<BasicChar>, GLOWE::template StdAllocator<BasicChar>>;
		using StringStream = typename std::template basic_stringstream<BasicChar, std::template char_traits<BasicChar>, GLOWE::template StdAllocator<BasicChar>>;
	public:
		// To iterate over complete Utf 8 multi byte characters.
		class GLOWSYSTEM_EXPORT Utf8Iterator
			: public std::bidirectional_iterator_tag
		{
		private:
			const char* arr;
			unsigned char bytes;
		public:
			Utf8Iterator() = default;
			Utf8Iterator(const char* a1, const unsigned char a2)
				: arr(a1), bytes(a2) {};

			Utf8Iterator operator++();
			Utf8Iterator operator++(int);

			Utf8Iterator operator--();
			Utf8Iterator operator--(int);

			const char* operator*() const noexcept;

			bool operator==(const Utf8Iterator& arg) const;
			bool operator==(const char* arg) const;
			bool operator==(const String& arg) const;

			bool operator!=(const Utf8Iterator& arg) const;
			bool operator!=(const char* arg) const;
			bool operator!=(const String& arg) const;

			unsigned int getBytesCount() const;
			Utf32Char asUtf32Char() const;

			friend class String;
		};
	private:
		BasicString baseString;
	public:
		static const std::size_t invalidPos;
	public:
		String() = default;
		String(const String& str);
		String(String&& str) noexcept;

		explicit String(const std::size_t strSize);

		template<class ItIn>
		String(ItIn begin, ItIn end);

		String(const char* buffer, const std::size_t buffSize);

		String(const std::size_t howMany, const BasicChar ch);

		String(const Utf8String& str);
		String(const Utf16String& str);
		String(const Utf32String& str);
		String(const WideString& str);

		String(Utf8String&& str);
		String(Utf16String&& str);
		String(Utf32String&& str);
		String(WideString&& str);

		String(const std::string& str);
		String(const std::wstring& str);

		String(std::string&& str);
		String(std::wstring&& str);

		String(const BasicChar ch);
		String(const Utf16Char ch);
		String(const Utf32Char ch);

		String(const BasicChar* str);
		String(const WideChar* str);

		String(StringView str);

		virtual ~String() = default;

		virtual void serialize(LoadSaveHandle& handle) const override;
		virtual void deserialize(LoadSaveHandle& handle) override;

		virtual Hash getHash() const override;

		const char* getCharArray() const;
		char* getCharArray();

		std::size_t getSize() const;
		std::size_t getCapacity() const;

		void clear();

		String& append(const String& arg);
		String& append(const std::size_t howMany, const BasicChar ch);
		String& append(const char* buff, const std::size_t howMany);

		String getSubstring(const std::size_t pos = 0, const std::size_t length = invalidPos) const;

		String& erase(const std::size_t pos, const std::size_t howMany = invalidPos);

		String& insert(const std::size_t pos, const String& str);
		String& insert(const std::size_t pos, const char* str);
		String& insert(const std::size_t pos, const char* str, const std::size_t howMany);
		String& insert(const std::size_t pos, const char ch);
		String& insert(const ConstIterator& it, const String& str);
		String& insert(const ConstIterator& it, const char* str);
		String& insert(const ConstIterator& it, const char* str, const std::size_t howMany);
		String& insert(const ConstIterator& it, const char ch);

		Utf8Iterator utf8Begin() const;
		Utf8Iterator utf8End() const;

		Iterator begin();
		ConstIterator begin() const;

		ReverseIterator rbegin();
		ConstReverseIterator rbegin() const;

		Iterator end();
		ConstIterator end() const;

		ReverseIterator rend();
		ConstReverseIterator rend() const;

		BasicString& getString();
		const BasicString& getString() const;

		BasicChar& getLastChar();
		const BasicChar& getLastChar() const;

		BasicChar& getFirstChar();
		const BasicChar& getFirstChar() const;

		BasicChar operator[](const unsigned int index) const;
		BasicChar& operator[](const unsigned int index);

		String getToken(const char* delims, const unsigned int whichToken = 0) const;
		unsigned int getTokensCount(const char* delims) const;
		Vector<String> tokenize(const char* delims) const;

		std::size_t find(const BasicChar toFind, const std::size_t pos = 0) const;
		std::size_t find(const String& toFind, const std::size_t pos = 0) const;

		std::size_t rfind(const BasicChar toFind, const std::size_t pos = invalidPos) const;
		std::size_t rfind(const String& toFind, const std::size_t pos = invalidPos) const;

		String& replace(const std::size_t pos, const std::size_t howMany, const String& withWhat);
		String& replace(const String& what, const String& withWhat);
		template<class It>
		String& replace(const ConstIterator& first, const ConstIterator& last, const It& firstFrom, const It& lastFrom)
		{
			baseString.replace(first, last, firstFrom, lastFrom);
			return *this;
		}

		void resize(const std::size_t newSize);
		void resize(const std::size_t newSize, const BasicChar ch);

		void reserve(const std::size_t newSize);

		/// @brief Converts the string to its uppercase version using a provided locale.
		/// @param locale Optional name of the locale to use. nullptr (default) is the default one, "" is the root one
		void toUppercase(const char* const locale = nullptr);
		/// @brief Converts the string to its lowercase version using a provided locale.
		/// @param locale Optional name of the locale to use. nullptr (default) is the default one, "" is the root one
		void toLowercase(const char* const locale = nullptr);
		/// @brief Converts the string to a uppercase copy.
		/// @param locale Optional name of the locale to use. nullptr (default) is the default one, "" is the root one
		/// @return Uppercase string
		String asUppercase(const char* const locale = nullptr) const;
		/// @brief Converts the string to a lowercase copy.
		/// @param locale Optional name of the locale to use. nullptr (default) is the default one, "" is the root one
		/// @return Lowercase string
		String asLowercase(const char* const locale = nullptr) const;

		Utf32String toUtf32() const;
		Utf16String toUtf16() const;
		WideString toWide() const;

		operator std::string() const;
		operator std::wstring() const;
		operator const BasicString&() const;
		operator StringView() const;

		String operator+(const String& right) const;
		String operator+(const BasicChar* right) const;
		String operator+(const BasicChar right) const;
		String operator+(const Utf16Char right) const;
		String operator+(const Utf32Char right) const;

		String& operator+=(const String& right);
		String& operator+=(const BasicChar* right);
		String& operator+=(const BasicChar right);
		String& operator+=(const Utf16Char right);
		String& operator+=(const Utf32Char right);

		String& operator=(const String& arg);
		String& operator=(String&& arg) noexcept;
		String& operator=(const BasicChar* right);
		String& operator=(const WideChar* right);

		bool operator==(const String& right) const;
		bool operator==(const BasicChar* right) const;
		bool operator!=(const String& right) const;
		bool operator!=(const BasicChar* right) const;

		bool operator<(const String& right) const;
		bool operator>(const String& right) const;

		bool isEmpty() const;

		int toInt(const int base = 10) const;
		long toLong(const int base = 10) const;
		long long toLongLong(const int base = 10) const;
		unsigned int toUInt(const int base = 10) const;
		unsigned long toULong(const int base = 10) const;
		unsigned long long toULongLong(const int base = 10) const;
		float toFloat() const;
		double toDouble() const;
		long double toLongDouble() const;
		bool toBool() const;
	};

	using Utf8String = String::Utf8String;
	using Utf16String = String::Utf16String;
	using Utf32String = String::Utf32String;
	using WideString = String::WideString;

	using WideChar = String::WideChar;
	using Char8 = String::Utf8Char;
	using Char16 = String::Utf16Char;
	using Char32 = String::Utf32Char;

	using IStringStream = String::IStringStream;
	using OStringStream = String::OStringStream;
	using StringStream = String::StringStream;

	GLOWSYSTEM_EXPORT String toString(const int arg);
	GLOWSYSTEM_EXPORT String toString(const long arg);
	GLOWSYSTEM_EXPORT String toString(const long long arg);
	GLOWSYSTEM_EXPORT String toString(const unsigned int arg);
	GLOWSYSTEM_EXPORT String toString(const unsigned long arg);
	GLOWSYSTEM_EXPORT String toString(const unsigned long long arg);
	GLOWSYSTEM_EXPORT String toString(const float arg);
	GLOWSYSTEM_EXPORT String toString(const double arg);
	GLOWSYSTEM_EXPORT String toString(const long double arg);
	GLOWSYSTEM_EXPORT String toString(const bool arg);
	GLOWSYSTEM_EXPORT String toString(const void* const arg);

	GLOWSYSTEM_EXPORT std::ostream& operator << (std::ostream& str, const String& right);
	GLOWSYSTEM_EXPORT std::istream& operator >> (std::istream& str, String& right);

	GLOWSYSTEM_EXPORT String operator+(const String::BasicChar* left, const String& right);
	GLOWSYSTEM_EXPORT String operator+(const String::WideChar* left, const String& right);

	GLOWSYSTEM_EXPORT String operator ""_str(const char* str, const std::size_t size);
	GLOWSYSTEM_EXPORT String operator ""_str(const wchar_t* str, const std::size_t);

	template<class ItIn>
	inline String::String(ItIn begin, ItIn end)
		: baseString(begin, end)
	{
	}
}

namespace std
{
	template<>
	struct hash<GLOWE::String>
	{
		inline size_t operator()(const GLOWE::String& arg) const noexcept
		{
			// Code adapted from https://en.cppreference.com/w/cpp/utility/hash/operator() (probably FNV hash).
			size_t result = 2166136261;

			for (const auto x : arg)
			{
				result = (result * 16777619) ^ x;
			}

			return result;
		}
	};
}

#endif
