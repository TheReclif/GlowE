#include "SerializableDataCreator.h"

GLOWE::Object* GLOWE::Hidden::GlobalObjectResolveEnvironment::getObject(const Object::InstanceId id) const
{
	return getInstance<ObjectRegistry>().getObject(id);
}

void GLOWE::Hidden::serializeMapSerializableValue(LoadSaveHandle& handle, const String& name, const UniquePtr<SerializableValueBase>& ptr)
{
	handle << name << ptr->getType() << *ptr;
}

void GLOWE::Hidden::deserializeMapSerializableValue(LoadSaveHandle& handle, Serializable::Variables& vars)
{
	String str;
	SerializableValueBase::Type type{};
	handle >> str >> type;
	auto unqPtr = SerializableValueBase::createFromEnum(type);
	handle >> *unqPtr;
	vars.emplaceOrAssign(std::move(str), std::move(unqPtr));
}

bool GLOWE::Hidden::LocalObjectResolveEnvironment::registerObject(const Object::InstanceId id, Object* const obj)
{
	if (objs.count(id) == 0)
	{
		objs.emplace(id, obj);
		return true;
	}
	return false;
}

GLOWE::Object* GLOWE::Hidden::LocalObjectResolveEnvironment::getObject(const Object::InstanceId id) const
{
	const auto it = objs.find(id);
	if (it != objs.end())
	{
		return it->second;
	}
	return nullptr;
}

GLOWE::Hidden::VariablesSerializableValue::VariablesSerializableValue(Serializable::Variables&& arg) noexcept
	: value(std::move(arg))
{
}

GLOWE::Hidden::VariablesSerializableValue& GLOWE::Hidden::VariablesSerializableValue::operator=(Serializable::Variables&& arg) noexcept
{
	value = std::move(arg);
	return *this;
}

GLOWE::UniquePtr<GLOWE::Hidden::SerializableValueBase> GLOWE::Hidden::VariablesSerializableValue::clone() const
{
	UniquePtr<VariablesSerializableValue> result = makeUniquePtr<VariablesSerializableValue>();
	
	for (const auto& x : value)
	{
		result->value.emplaceOrAssign(x.first, x.second->clone());
	}

	return result;
}

GLOWE::Hidden::SerializableValueBase::Type GLOWE::Hidden::VariablesSerializableValue::getType() const
{
	return Type::Variables;
}

void GLOWE::Hidden::VariablesSerializableValue::serialize(LoadSaveHandle& handle) const
{
	handle << static_cast<UInt32>(value.size());
	for (const auto& x : value)
	{
		handle << x.first << x.second->getType() << *x.second;
	}
}

void GLOWE::Hidden::VariablesSerializableValue::deserialize(LoadSaveHandle& handle)
{
	UInt32 size{};
	String str;
	Type type{};
	UniquePtr<SerializableValueBase> uniquePtr;

	handle >> size;
	for (unsigned int x = 0; x < size; ++x)
	{
		handle >> str >> type;
		uniquePtr = createFromEnum(type);
		handle >> *uniquePtr;
		value.emplaceOrAssign(str, std::move(uniquePtr));
	}
}

GLOWE::Hidden::VariablesArraySerializableValue::VariablesArraySerializableValue(Vector<Serializable::Variables>&& arg)
	: value(std::move(arg))
{
}

GLOWE::Hidden::VariablesArraySerializableValue& GLOWE::Hidden::VariablesArraySerializableValue::operator=(Vector<Serializable::Variables>&& arg)
{
	value = std::move(arg);
	return *this;
}

GLOWE::UniquePtr<GLOWE::Hidden::SerializableValueBase> GLOWE::Hidden::VariablesArraySerializableValue::clone() const
{
	Vector<Serializable::Variables> result(value.size());

	for (unsigned int x = 0, size = value.size(); x < size; ++x)
	{
		for (const auto& y : value[x])
		{
			result[x].emplaceOrAssign(y.first, y.second->clone());
		}
	}

	return makeUniquePtr<VariablesArraySerializableValue>(std::move(result));
}

GLOWE::Hidden::SerializableValueBase::Type GLOWE::Hidden::VariablesArraySerializableValue::getType() const
{
	return Type::VariablesArray;
}

void GLOWE::Hidden::VariablesArraySerializableValue::serialize(LoadSaveHandle& handle) const
{
	handle << static_cast<UInt32>(value.size());
	for (const auto& x : value)
	{
		handle << static_cast<UInt32>(x.size());
		for (const auto& y : x)
		{
			handle << y.first << y.second->getType() << *y.second;
		}
	}
}

void GLOWE::Hidden::VariablesArraySerializableValue::deserialize(LoadSaveHandle& handle)
{
	UInt32 size{};
	String str;
	Type type{};
	handle >> size;
	Vector<Serializable::Variables> vars(size);
	for (auto& x : vars)
	{
		handle >> size;
		for (unsigned int y = 0; y < size; ++y)
		{
			handle >> str >> type;
			auto unqPtr = createFromEnum(type);
			unqPtr->deserialize(handle);
			x.emplaceOrAssign(str, std::move(unqPtr));
		}
	}

	value = std::move(vars);
}

GLOWE::UniquePtr<GLOWE::Hidden::SerializableValueBase> GLOWE::Hidden::SerializableValueBase::createFromEnum(const Type& type)
{
	UniquePtr<SerializableValueBase> result;
	switch (type)
	{
	case SerializableValueBase::Type::Bool:
		result = makeUniquePtr<BoolSerializableValue>();
		break;
	case SerializableValueBase::Type::UInt:
		result = makeUniquePtr<UIntSerializableValue>();
		break;
	case SerializableValueBase::Type::Int:
		result = makeUniquePtr<IntSerializableValue>();
		break;
	case SerializableValueBase::Type::UInt64:
		result = makeUniquePtr<UInt64SerializableValue>();
		break;
	case SerializableValueBase::Type::Int64:
		result = makeUniquePtr<Int64SerializableValue>();
		break;
	case SerializableValueBase::Type::Float:
		result = makeUniquePtr<FloatSerializableValue>();
		break;
	case SerializableValueBase::Type::Double:
		result = makeUniquePtr<DoubleSerializableValue>();
		break;
	case SerializableValueBase::Type::String:
		result = makeUniquePtr<StringSerializableValue>();
		break;
	case SerializableValueBase::Type::BoolArray:
		result = makeUniquePtr<BoolArraySerializableValue>();
		break;
	case SerializableValueBase::Type::UIntArray:
		result = makeUniquePtr<UIntArraySerializableValue>();
		break;
	case SerializableValueBase::Type::IntArray:
		result = makeUniquePtr<IntArraySerializableValue>();
		break;
	case SerializableValueBase::Type::FloatArray:
		result = makeUniquePtr<FloatArraySerializableValue>();
		break;
	case SerializableValueBase::Type::DoubleArray:
		result = makeUniquePtr<DoubleArraySerializableValue>();
		break;
	case SerializableValueBase::Type::StringArray:
		result = makeUniquePtr<StringArraySerializableValue>();
		break;
	case SerializableValueBase::Type::Variables:
		result = makeUniquePtr<VariablesSerializableValue>();
		break;
	case SerializableValueBase::Type::VariablesArray:
		result = makeUniquePtr<VariablesArraySerializableValue>();
		break;
	case SerializableValueBase::Type::Map:
		result = makeUniquePtr<MapSerializableValue>();
		break;
	default:
		break;
	}

	return result;
}

GLOWE::UniquePtr<GLOWE::Hidden::SerializableValueBase> GLOWE::Hidden::SerializableDetail::serializeValue(const Variables& value)
{
	Serializable::Variables map;

	for (const auto& x : value)
	{
		map.emplaceOrAssign(x.first, x.second->clone());
	}

	return makeUniquePtr<VariablesSerializableValue>(std::move(map));
}

void GLOWE::Hidden::SerializableDetail::deserializeValue(const SerializableValueBase* serVal, Variables& value, const ObjectResolveEnvironment&)
{
	const VariablesSerializableValue* const vars = dynamic_cast<const VariablesSerializableValue*>(serVal);
	if (vars)
	{
		for (const auto& x : vars->value)
		{
			value.emplaceOrAssign(x.first, x.second->clone());
		}
	}
}

void GLOWE::Hidden::MapSerializableValue::serialize(LoadSaveHandle& handle) const
{
	handle << static_cast<UInt32>(value.size());
	for (const auto& x : value)
	{
		handle << x.first->getType() << *x.first;
		handle << x.second->getType() << *x.second;
	}
}

void GLOWE::Hidden::MapSerializableValue::deserialize(LoadSaveHandle& handle)
{
	UInt32 size{};
	handle >> size;
	decltype(value) vec(size);
	for (auto& x : value)
	{
		Type type{};
		handle >> type;
		auto unqPtr = createFromEnum(type);
		handle >> *unqPtr >> type;
		auto unqPtr2 = createFromEnum(type);
		handle >> *unqPtr2;
		x = std::make_pair(std::move(unqPtr), std::move(unqPtr2));
	}
}

GLOWE::Vector<GLOWE::Serializable::EditorUILayoutElem> GLOWE::Serializable::getVisibleLayout()
{
	Vector<EditorUILayoutElem> visibleItems;
	auto accessors = getAccessors();
	visibleItems.reserve(accessors.size());

	for (auto& x : accessors)
	{
		visibleItems.emplace_back(EditorUILayoutElem::Type::Field);
		auto& currField = visibleItems.back().fieldData;
		currField.name = x.first;
		currField.value = std::move(x.second);
	}

	return visibleItems;
}

GLOWE::Serializable::EditorUILayoutElem::EditorUILayoutElem(EditorUILayoutElem&& arg) noexcept
{
	switch (type)
	{
	case Type::Field:
		new (&fieldData) FieldData(std::move(arg.fieldData));
		break;
	case Type::Header:
		new (&headerName) String(std::move(arg.headerName));
		break;
	case Type::Button:
		new (&buttonData) ButtonData(std::move(arg.buttonData));
		break;
	case Type::Custom:
		new (&customData) CustomData(std::move(arg.customData));
		break;
	}
}

GLOWE::Serializable::EditorUILayoutElem::EditorUILayoutElem(const Type newType)
	: type(newType)
{
	switch (type)
	{
	case Type::Field:
		new (&fieldData) FieldData();
		break;
	case Type::Header:
		new (&headerName) String();
		break;
	case Type::Button:
		new (&buttonData) ButtonData();
		break;
	case Type::Custom:
		new (&customData) CustomData();
		break;
	}
}

GLOWE::Serializable::EditorUILayoutElem::~EditorUILayoutElem()
{
	switch (type)
	{
	case Type::Field:
		fieldData.~FieldData();
		break;
	case Type::Header:
		headerName.~String();
		break;
	case Type::Button:
		buttonData.~ButtonData();
		break;
	case Type::Custom:
		customData.~CustomData();
		break;
	}
}

GLOWE::Serializable::Variables::Container::const_iterator GLOWE::Serializable::Variables::begin() const
{
	return value.begin();
}

GLOWE::Serializable::Variables::Container::iterator GLOWE::Serializable::Variables::begin()
{
	return value.begin();
}

GLOWE::Serializable::Variables::Container::const_iterator GLOWE::Serializable::Variables::end() const
{
	return value.end();
}

GLOWE::Serializable::Variables::Container::iterator GLOWE::Serializable::Variables::end()
{
	return value.end();
}

GLOWE::Serializable::Variables::Container::const_iterator GLOWE::Serializable::Variables::find(const String& arg) const
{
	return value.find(arg);
}

std::size_t GLOWE::Serializable::Variables::size() const
{
	return value.size();
}

GLOWE::Serializable::Variables::TypeDoesNotMatchException::TypeDoesNotMatchException(const String& typeRequested, const String& requestedName)
	: reason("Requested type " + typeRequested + " for " + requestedName + " not does not match.")
{
}

const char* GLOWE::Serializable::Variables::TypeDoesNotMatchException::what() const noexcept
{
	return reason.getCharArray();
}

GLOWE::UniquePtr<GLOWE::Hidden::SerializableValueBase>& GLOWE::Serializable::Variables::operator[](const String& name)
{
	return value[name];
}

void GLOWE::Serializable::Variables::set(const String& name, const bool val)
{
	value[name] = makeUniquePtr<Hidden::BoolSerializableValue>(val);
}

void GLOWE::Serializable::Variables::set(const String& name, const float val)
{
	value[name] = makeUniquePtr<Hidden::FloatSerializableValue>(val);
}

void GLOWE::Serializable::Variables::set(const String& name, const double val)
{
	value[name] = makeUniquePtr<Hidden::DoubleSerializableValue>(val);
}

void GLOWE::Serializable::Variables::set(const String& name, const UInt32 val)
{
	value[name] = makeUniquePtr<Hidden::UIntSerializableValue>(val);
}

void GLOWE::Serializable::Variables::set(const String& name, const Int32 val)
{
	value[name] = makeUniquePtr<Hidden::IntSerializableValue>(val);
}

void GLOWE::Serializable::Variables::set(const String& name, const UInt64 val)
{
	value[name] = makeUniquePtr<Hidden::UInt64SerializableValue>(val);
}

void GLOWE::Serializable::Variables::set(const String& name, const Int64 val)
{
	value[name] = makeUniquePtr<Hidden::Int64SerializableValue>(val);
}

void GLOWE::Serializable::Variables::set(const String& name, const String& val)
{
	value[name] = makeUniquePtr<Hidden::StringSerializableValue>(val);
}

void GLOWE::Serializable::Variables::set(const String& name, const Variables& val)
{
	value[name] = makeUniquePtr<Hidden::VariablesSerializableValue>(val.clone());
}

void GLOWE::Serializable::Variables::set(const String& name, Variables&& val)
{
	value[name] = makeUniquePtr<Hidden::VariablesSerializableValue>(std::move(val));
}

void GLOWE::Serializable::Variables::set(const String& name, const Vector<Variables>& val)
{
	auto ptr = makeUniquePtr<Hidden::VariablesArraySerializableValue>();
	ptr->value.reserve(val.size());
	for (const auto& x : val)
	{
		ptr->value.emplace_back(x.clone());
	}
	value[name] = std::move(ptr);
}

void GLOWE::Serializable::Variables::set(const String& name, Vector<Variables>&& val)
{
	value[name] = makeUniquePtr<Hidden::VariablesArraySerializableValue>(std::move(val));
}

bool GLOWE::Serializable::Variables::getBool(const String& name) const
{
	const auto result = dynamic_cast<const Hidden::BoolSerializableValue*>(value.at(name).get());
	if (!result)
	{
		throw TypeDoesNotMatchException("bool", name);
	}
	return result->value;
}

float GLOWE::Serializable::Variables::getFloat(const String& name) const
{
	const auto result = dynamic_cast<const Hidden::FloatSerializableValue*>(value.at(name).get());
	if (!result)
	{
		throw TypeDoesNotMatchException("float", name);
	}
	return result->value;
}

double GLOWE::Serializable::Variables::getDouble(const String& name) const
{
	const auto result = dynamic_cast<const Hidden::DoubleSerializableValue*>(value.at(name).get());
	if (!result)
	{
		throw TypeDoesNotMatchException("double", name);
	}
	return result->value;
}

GLOWE::UInt32 GLOWE::Serializable::Variables::getUInt(const String& name) const
{
	const auto result = dynamic_cast<const Hidden::UIntSerializableValue*>(value.at(name).get());
	if (!result)
	{
		throw TypeDoesNotMatchException("unsigned int", name);
	}
	return result->value;
}

GLOWE::Int32 GLOWE::Serializable::Variables::getInt(const String& name) const
{
	const auto result = dynamic_cast<const Hidden::IntSerializableValue*>(value.at(name).get());
	if (!result)
	{
		throw TypeDoesNotMatchException("int", name);
	}
	return result->value;
}

GLOWE::UInt64 GLOWE::Serializable::Variables::getUInt64(const String& name) const
{
	const auto result = dynamic_cast<const Hidden::UInt64SerializableValue*>(value.at(name).get());
	if (!result)
	{
		throw TypeDoesNotMatchException("unsigned int64", name);
	}
	return result->value;
}

GLOWE::Int64 GLOWE::Serializable::Variables::getInt64(const String& name) const
{
	const auto result = dynamic_cast<const Hidden::Int64SerializableValue*>(value.at(name).get());
	if (!result)
	{
		throw TypeDoesNotMatchException("int64", name);
	}
	return result->value;
}

const GLOWE::String& GLOWE::Serializable::Variables::getString(const String& name) const
{
	const auto result = dynamic_cast<const Hidden::StringSerializableValue*>(value.at(name).get());
	if (!result)
	{
		throw TypeDoesNotMatchException("string", name);
	}
	return result->value;
}

const GLOWE::Serializable::Variables& GLOWE::Serializable::Variables::getVariables(const String& name) const
{
	const auto result = dynamic_cast<const Hidden::VariablesSerializableValue*>(value.at(name).get());
	if (!result)
	{
		throw TypeDoesNotMatchException("variables", name);
	}
	return result->value;
}

GLOWE::Serializable::Variables& GLOWE::Serializable::Variables::getVariables(const String& name)
{
	auto result = dynamic_cast<Hidden::VariablesSerializableValue*>(value.at(name).get());
	if (!result)
	{
		throw TypeDoesNotMatchException("variables", name);
	}
	return result->value;
}

const GLOWE::Vector<GLOWE::Serializable::Variables>& GLOWE::Serializable::Variables::getVariablesArray(const String& name) const
{
	auto result = dynamic_cast<Hidden::VariablesArraySerializableValue*>(value.at(name).get());
	if (!result)
	{
		throw TypeDoesNotMatchException("variables array", name);
	}
	return result->value;
}


GLOWE::Vector<GLOWE::Serializable::Variables>& GLOWE::Serializable::Variables::getVariablesArray(const String& name)
{
	auto result = dynamic_cast<Hidden::VariablesArraySerializableValue*>(value.at(name).get());
	if (!result)
	{
		throw TypeDoesNotMatchException("variables array", name);
	}
	return result->value;
}

GLOWE::Serializable::Variables GLOWE::Serializable::Variables::clone() const
{
	Variables result;
	for (const auto& x : value)
	{
		result.value.emplace(x.first, x.second->clone());
	}
	return result;
}

void GLOWE::Serializable::Variables::clear()
{
	value.clear();
}

void GLOWE::Serializable::Variables::erase(const String& name)
{
	value.erase(name);
}

bool GLOWE::Serializable::Variables::checkIsPresent(const String& name) const
{
	return value.count(name) > 0;
}
