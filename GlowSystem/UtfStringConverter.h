#pragma once
#ifndef UTFCONVERTER_INCLUDED
#define UTFCONVERTER_INCLUDED

#include <locale>
#include <string>

#include "String.h"

namespace GLOWE
{
	template<class Facet>
	class LocaleIndependentFacet
		: public Facet
	{
	public:
		template<class... Args>
		LocaleIndependentFacet(Args&& ...args) : Facet(std::forward<Args>(args)...) {};
		~LocaleIndependentFacet() {};
	};

	template<class Facet>
	class FacetProvider
		: public Subsystem
	{
	private:
		Facet facet;
	public:
		static const Facet& getFacet()
		{
			return getInstance<FacetProvider<Facet>>().facet;
		}
	};

	GLOWSYSTEM_EXPORT bool isPrintable(const String::Utf32Char character);
	GLOWSYSTEM_EXPORT bool isGraphical(const String::Utf32Char character);
	GLOWSYSTEM_EXPORT bool isWhiteSpace(const String::Utf32Char character);
	GLOWSYSTEM_EXPORT bool isLineBreakPoint(const String::Utf32Char character);
	GLOWSYSTEM_EXPORT bool isUppercase(const String::Utf32Char character);
	GLOWSYSTEM_EXPORT bool isLowercase(const String::Utf32Char character);
	GLOWSYSTEM_EXPORT bool isDigit(const String::Utf32Char character);

#define GLOWE_DISABLE_MSV_CODECVT_WORKAROUND // Maybe it will work?
#if !defined(_MSC_VER) || defined(GLOWE_DISABLE_MSV_CODECVT_WORKAROUND)
	using CodecvtUtf8ToUtf16 = LocaleIndependentFacet<std::codecvt<char16_t, char, std::mbstate_t>>;
	using CodecvtUtf8ToUtf32 = LocaleIndependentFacet<std::codecvt<char32_t, char, std::mbstate_t>>;
#define GlowCodecvtWorkaroundChar16
#define GlowCodecvtWorkaroundChar32
#else
	using CodecvtUtf8ToUtf16 = LocaleIndependentFacet<std::codecvt<unsigned short, char, std::mbstate_t>>;
	using CodecvtUtf8ToUtf32 = LocaleIndependentFacet<std::codecvt<unsigned int, char, std::mbstate_t>>;
#define GlowCodecvtWorkaroundChar16 (unsigned short*)
#define GlowCodecvtWorkaroundChar32 (unsigned int*)
#endif

	using CodecvtUtf8ToWide = LocaleIndependentFacet<std::codecvt<wchar_t, char, std::mbstate_t>>;

	class GLOWSYSTEM_EXPORT Locale
		: public Subsystem
	{
	private:
		std::locale loc;
	public:
		Locale();
		virtual ~Locale();

		std::locale& getLocale();
		const std::locale& getLocale() const;
	};

	template<unsigned int Base>
	class Utf {};

	template<>
	class GLOWSYSTEM_EXPORT Utf<8>
	{
	public:
		static void toUtf16(const Utf8String& in, Utf16String& out);
		static void toUtf32(const Utf8String& in, Utf32String& out);
		static void toWide(const Utf8String& in, WideString& out);

		static Utf16String toUtf16(const Utf8String& in);
		static Utf32String toUtf32(const Utf8String& in);
		static WideString toWide(const Utf8String& in);

		static void fromUtf16(const Utf16String& in, Utf8String& out);
		static void fromUtf32(const Utf32String& in, Utf8String& out);
		static void fromWide(const WideString& in, Utf8String& out);

		static Utf8String fromUtf16(const Utf16String& in);
		static Utf8String fromUtf32(const Utf32String& in);
		static Utf8String fromWide(const WideString& in);
	};

	template<>
	class GLOWSYSTEM_EXPORT Utf<16>
	{
	public:
		static void toUtf8(const Utf16String& in, Utf8String& out);
		static void toUtf32(const Utf16String& in, Utf32String& out);
		static void toWide(const Utf16String& in, WideString& out);

		static Utf8String toUtf8(const Utf16String& in);
		static Utf32String toUtf32(const Utf16String& in);
		static WideString toWide(const Utf16String& in);

		static void fromUtf8(const Utf8String& in, Utf16String& out);
		static void fromUtf32(const Utf32String& in, Utf16String& out);
		static void fromWide(const WideString& in, Utf16String& out);

		static Utf16String fromUtf8(const Utf8String& in);
		static Utf16String fromUtf32(const Utf32String& in);
		static Utf16String fromWide(const WideString& in);
	};

	template<>
	class GLOWSYSTEM_EXPORT Utf<32>
	{
	public:
		static void toUtf8(const Utf32String& in, Utf8String& out);
		static void toUtf16(const Utf32String& in, Utf16String& out);
		static void toWide(const Utf32String& in, WideString& out);

		static Utf8String toUtf8(const Utf32String& in);
		static Utf16String toUtf16(const Utf32String& in);
		static WideString toWide(const Utf32String& in);

		static void fromUtf8(const Utf8String& in, Utf32String& out);
		static void fromUtf16(const Utf16String& in, Utf32String& out);
		static void fromWide(const WideString& in, Utf32String& out);

		static Utf32String fromUtf8(const Utf8String& in);
		static Utf32String fromUtf16(const Utf16String& in);
		static Utf32String fromWide(const WideString& in);
	};
}

#endif
