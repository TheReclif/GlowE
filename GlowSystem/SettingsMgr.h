#pragma once
#ifndef GLOWE_SETTINGSMGR_INCLUDED
#define GLOWE_SETTINGSMGR_INCLUDED

#include "Utility.h"
#include "SerializableDataCreator.h"
#include "LocalizationMgr.h"
#include "String.h"
#include "FileIO.h"
#include "Values.h"
#include "FileParser.h"
#include "Filesystem.h"

#ifndef GLOWE_DEFAULT_SETTINGS_PATH
#define GLOWE_DEFAULT_SETTINGS_PATH u8"Settings.cfg"
#endif

namespace GLOWE
{
	class GLOWSYSTEM_EXPORT SettingsMgr
		: public Subsystem
	{
	private:
		FileParser settings;

		bool isLoaded;

		template<class T>
		static T* internalSettingAlloc();

		template<class T>
		static void internalSettingFree(T* ptr);
	public:
		SettingsMgr();
		~SettingsMgr() = default;

		void exportToFile(const String& filename = GLOWE_DEFAULT_SETTINGS_PATH) const;
		String exportToString() const;
		void importFromFile(const String& filename = GLOWE_DEFAULT_SETTINGS_PATH);
		void importFromString(const String& strToParse);

		void createDefaultSettings(const String& filename) const;

		SharedPtr<Value> getSetting(const String& name);
		SharedPtr<Value> getSetting(const String& name, const String& section);

		float getSetting(const String& name, const String& section, const float defaultValue);
		unsigned int getSetting(const String& name, const String& section, const unsigned int defaultValue);
		int getSetting(const String& name, const String& section, const int defaultValue);
		bool getSetting(const String& name, const String& section, const bool defaultValue);
		String getSetting(const String& name, const String& section, const char* defaultValue);
		String getSetting(const String& name, const String& section, const String& defaultValue);

		bool checkIsLoaded() const;
	};
	template<class T>
	inline T * SettingsMgr::internalSettingAlloc()
	{
		return getInstance<GlobalAllocator>().allocate<T>();
	}
	template<class T>
	inline void SettingsMgr::internalSettingFree(T * ptr)
	{
		getInstance<GlobalAllocator>().deallocate<T>(ptr);
	}
}

#endif