#include "Random.h"

#include <chrono>

GLOWE::Random::Random()
	: randomGen(), uintDist(), intDist(), floatDist(), doubleDist(), seed()
{
	reseed();
}

GLOWE::Random::Random(const long long newSeed)
	: randomGen(), uintDist(), intDist(), floatDist(), doubleDist(), seed()
{
	reseed(newSeed);
}

unsigned int GLOWE::Random::randomUInt(const unsigned int minInt, const unsigned int maxInt)
{
	if (uintDist.a() != minInt || uintDist.b() != maxInt)
	{
		uintDist.param(decltype(uintDist)::param_type(minInt, maxInt));
	}
	return uintDist(randomGen);
}

int GLOWE::Random::randomInt(const int minInt, const int maxInt)
{
	if (intDist.a() != minInt || intDist.b() != maxInt)
	{
		intDist.param(decltype(intDist)::param_type(minInt, maxInt));
	}
	return intDist(randomGen);
}

double GLOWE::Random::randomDouble(const double minDouble, const double maxDouble)
{
	if (doubleDist.a() != minDouble || doubleDist.b() != minDouble)
	{
		doubleDist.param(decltype(doubleDist)::param_type(minDouble, maxDouble));
	}
	return doubleDist(randomGen);
}

float GLOWE::Random::randomFloat(const float minFloat, const float maxFloat)
{
	if (floatDist.a() != minFloat || floatDist.b() != maxFloat)
	{
		floatDist.param(decltype(floatDist)::param_type(minFloat, maxFloat));
	}
	return floatDist(randomGen);
}

std::mt19937& GLOWE::Random::getGenerator()
{
	return randomGen;
}

void GLOWE::Random::reseed()
{
	std::random_device seeder;

	long long temp;
	if (seeder.entropy() == 0.0)
	{
		temp = std::chrono::system_clock::now().time_since_epoch().count();
	}
	else
	{
		temp = seeder();
	}

	randomGen.seed(temp);
	seed = temp;
}

void GLOWE::Random::reseed(const long long newSeed)
{
	randomGen.seed(newSeed);
	seed = newSeed;
}

long long GLOWE::Random::getSeed() const
{
	return seed;
}
