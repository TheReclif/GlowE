#include "Color.h"
#include "LoadSaveHandle.h"

constexpr GLOWE::Color::ColorBase GLOWE::Color::white;
constexpr GLOWE::Color::ColorBase GLOWE::Color::grey;
constexpr GLOWE::Color::ColorBase GLOWE::Color::black;

constexpr GLOWE::Color::ColorBase GLOWE::Color::red;
constexpr GLOWE::Color::ColorBase GLOWE::Color::green;
constexpr GLOWE::Color::ColorBase GLOWE::Color::blue;

constexpr GLOWE::Color::ColorBase GLOWE::Color::cyan;
constexpr GLOWE::Color::ColorBase GLOWE::Color::magenta;
constexpr GLOWE::Color::ColorBase GLOWE::Color::yellow;

GLOWE::Color::operator GLOWE::Float4() const
{
	return Float4{ rgba[0], rgba[1], rgba[2], rgba[3] };
}

void GLOWE::Color::setRGBA(const float rr, const float gg, const float bb, const float aa)
{
	rgba[0] = rr;
	rgba[1] = gg;
	rgba[2] = bb;
	rgba[3] = aa;
}

void GLOWE::Color::setBGRA(const float bb, const float gg, const float rr, const float aa)
{
	rgba[0] = rr;
	rgba[1] = gg;
	rgba[2] = bb;
	rgba[3] = aa;
}

void GLOWE::Color::setRGB(const float rr, const float gg, const float bb)
{
	rgba[0] = rr;
	rgba[1] = gg;
	rgba[2] = bb;
	rgba[3] = 1.0f;
}

void GLOWE::Color::setBGR(const float bb, const float gg, const float rr)
{
	rgb[0] = rr;
	rgb[1] = gg;
	rgb[2] = bb;
	rgb[3] = 1.0f;
}

void GLOWE::Color::serialize(LoadSaveHandle& handle) const
{
	handle << rgba[0] << rgba[1] << rgba[2] << rgba[3];
}

void GLOWE::Color::deserialize(LoadSaveHandle& handle)
{
	handle >> rgba[0] >> rgba[1] >> rgba[2] >> rgba[3];
}

GLOWE::Color::ColorBase::operator const GLOWE::Color&() const
{
	return *reinterpret_cast<const Color*>(this);
}

GLOWE::Color::ColorBase::operator const GLOWE::Float4&() const
{
	return *reinterpret_cast<const Float4*>(this);
}

bool GLOWE::Color::operator==(const Color& other) const
{
	return r == other.r &&
		g == other.g &&
		b == other.b &&
		a == other.a;
}

bool GLOWE::Color::operator!=(const Color& other) const
{
	return !(*this == other);
}
