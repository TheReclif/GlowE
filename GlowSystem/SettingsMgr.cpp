#include "SettingsMgr.h"

GLOWE::SettingsMgr::SettingsMgr()
	: isLoaded(false), settings()
{
	importFromFile();
}

void GLOWE::SettingsMgr::exportToFile(const String & filename) const
{
	if (isLoaded)
	{
		settings.parseToFile(filename);
	}
}

GLOWE::String GLOWE::SettingsMgr::exportToString() const
{
	String temp;
	if (isLoaded)
	{
		settings.parseToString(temp);
	}
	return temp;
}

void GLOWE::SettingsMgr::importFromFile(const String & filename)
{
	if (isLoaded)
	{
		return;
	}

	FilePath path = filename;

	if (!Filesystem::isRegularFile(path))
	{
		if (!path.hasExtension())
		{
			path.replaceExtension(u8"cfg");
		}

		createDefaultSettings(path);
	}

	settings.parseFromFile(path);

	isLoaded = true;
}

void GLOWE::SettingsMgr::importFromString(const String & strToParse)
{
	if (isLoaded)
	{
		return;
	}

	settings.parseFromString(strToParse);

	isLoaded = true;
}

void GLOWE::SettingsMgr::createDefaultSettings(const String & filename) const
{
	//Wanna add new settings or summon an evil user-provided script that will do it for you? Do it here, in this function!

	UIntValue msaaLevel, windowWidth, windowHeight, presentInterval;
	StringValue windowTitle, usedRenderBackend;
	BoolValue useMultithreadRender;

	msaaLevel.fromUInt(0);
	windowWidth.fromUInt(800);
	windowHeight.fromUInt(600);
	presentInterval.fromUInt(1);

	windowTitle.fromString(u8"GlowEngine Zuzi 1.0");
	usedRenderBackend.fromString(u8"Direct3D11");

	useMultithreadRender.fromBool(true);

	FileParser parser;

	parser.setValue(u8"MSAALevel", makeSharedPtr<UIntValue>(std::move(msaaLevel)), u8"Graphics");
	parser.setValue(u8"UseMultithreadedRendering", makeSharedPtr<BoolValue>(std::move(useMultithreadRender)), u8"Graphics");
	parser.setValue(u8"UsedRenderBackend", makeSharedPtr<StringValue>(std::move(usedRenderBackend)), u8"Graphics");

	parser.setValue(u8"Width", makeSharedPtr<UIntValue>(std::move(windowWidth)), u8"Window");
	parser.setValue(u8"Height", makeSharedPtr<UIntValue>(std::move(windowHeight)), u8"Window");
	parser.setValue(u8"Title", makeSharedPtr<StringValue>(std::move(windowTitle)), u8"Window");
	parser.setValue(u8"PresentInterval", makeSharedPtr<UIntValue>(std::move(presentInterval)), u8"Window");

	parser.parseToFile(filename);
}

GLOWE::SharedPtr<GLOWE::Value> GLOWE::SettingsMgr::getSetting(const String & name)
{
	return settings.getValue(name);
}

GLOWE::SharedPtr<GLOWE::Value> GLOWE::SettingsMgr::getSetting(const String & name, const String& section)
{
	return settings.getValue(name, section);
}

float GLOWE::SettingsMgr::getSetting(const String& name, const String& section, const float defaultValue)
{
	const auto val = settings.getValue(name, section);
	if (val)
	{
		return val->asFloat();
	}
	return defaultValue;
}

int GLOWE::SettingsMgr::getSetting(const String& name, const String& section, const int defaultValue)
{
	const auto val = settings.getValue(name, section);
	if (val)
	{
		return val->asInt();
	}
	return defaultValue;
}

unsigned int GLOWE::SettingsMgr::getSetting(const String& name, const String& section, const unsigned int defaultValue)
{
	const auto val = settings.getValue(name, section);
	if (val)
	{
		return val->asUInt();
	}
	return defaultValue;
}

bool GLOWE::SettingsMgr::getSetting(const String& name, const String& section, const bool defaultValue)
{
	const auto val = settings.getValue(name, section);
	if (val)
	{
		return val->asBool();
	}
	return defaultValue;
}

GLOWE::String GLOWE::SettingsMgr::getSetting(const String& name, const String& section, const char* defaultValue)
{
	const auto val = settings.getValue(name, section);
	if (val)
	{
		return val->asString();
	}
	return defaultValue;
}

GLOWE::String GLOWE::SettingsMgr::getSetting(const String& name, const String& section, const String& defaultValue)
{
	const auto val = settings.getValue(name, section);
	if (val)
	{
		return val->asString();
	}
	return defaultValue;
}

bool GLOWE::SettingsMgr::checkIsLoaded() const
{
	return isLoaded;
}
