#include "AsyncIO.h"

GLOWE::Package & GLOWE::AsyncIOMgr::getPackageForRead(const String & filename)
{
	return getInstance<PackageMgr>().getPackage(filename);
}

GLOWE::AsyncIOMgr::AsyncIOMgr()
	: threads(getCPUCoresCount()), packageMgr(getInstance<PackageMgr>())
{
	Vector<Thread::Id> ids = threads.getThreadIDs();

	for (auto&& x : ids)
	{
		packageMgr.declareCustomerThread(x);
	}
}

GLOWE::AsyncIOMgr::IsReadyFlag GLOWE::AsyncIOMgr::writeToFile(const String & filename, const void * data, const unsigned long long dataSize, const std::function<void()>& callbackAtCompletion)
{
	auto&& func = [](const String filename, const void* data, const unsigned long long dataSize, const std::function<void()> callbackAtCompletion)
	{
		File file;
		file.open(filename, std::ios::out | std::ios::binary | std::ios::trunc);

		if (file.checkIsOpen())
		{
			file.write(data, dataSize);
		}

		if (callbackAtCompletion)
		{
			callbackAtCompletion();
		}
	};

	return threads.addTask(func, filename, data, dataSize, callbackAtCompletion);
}

GLOWE::AsyncIOMgr::IsReadyFlag GLOWE::AsyncIOMgr::writeToPackage(const String& packageName, const String& filename, const void* data, const unsigned long long dataSize, const std::function<void(Package&)>& callbackAtCompletion)
{
	auto&& func = [this](const String packageName, const String filename, const void* data, const unsigned long long dataSize, const std::function<void(Package&)> callbackAtCompletion)
	{
		DefaultUniqueLock mapLock(writePackagesMute);
		auto& tempPair = writePackages[packageName];
		mapLock.unlock();
		Package& package = tempPair.first;
		DefaultLockGuard packGuard(tempPair.second);
		
		if (!package.checkIsOpen())
		{
			getInstance<PackageMgr>().closeAllPackagesForFilename(packageName);
			package.open(packageName, Package::OpenMode::Write);
			if (!package.checkIsOpen())
			{
				return;
			}
		}

		package.addFile(filename, (const char*)data, dataSize);

		if (callbackAtCompletion)
		{
			callbackAtCompletion(package);
		}
	};

	return threads.addTask(func, packageName, filename, data, dataSize, callbackAtCompletion);
}

GLOWE::AsyncIOMgr::IsReadyFlag GLOWE::AsyncIOMgr::applyPackageChanges(const String& packageName)
{
	auto&& func = [this](const String packageName)
	{
		DefaultUniqueLock mapLock(writePackagesMute);
		const auto it = writePackages.find(packageName);
		mapLock.unlock();

		if (it != writePackages.end())
		{
			DefaultLockGuard packGuard(it->second.second);
			it->second.first.applyChanges();
		}
	};

	return threads.addTask(func, packageName);
}

GLOWE::AsyncIOMgr::IsReadyFlag GLOWE::AsyncIOMgr::discardPackageChanges(const String& packageName)
{
	auto&& func = [this](const String packageName)
	{
		DefaultUniqueLock mapLock(writePackagesMute);
		const auto it = writePackages.find(packageName);
		mapLock.unlock();

		if (it != writePackages.end())
		{
			DefaultLockGuard packGuard(it->second.second);
			it->second.first.discardChanges();
		}
	};

	return threads.addTask(func, packageName);
}

GLOWE::AsyncIOMgr::IsReadyFlag GLOWE::AsyncIOMgr::readWholeFile(const String& filename, String& buffer)
{
	auto&& func = [](const String filename, String& buffer)
	{
		File file;
		file.open(filename, std::ios::in | std::ios::binary);

		if (file.checkIsOpen())
		{
			buffer.resize(file.getFileSize());
			file.read(buffer.getCharArray(), buffer.getSize());
		}
	};

	return threads.addTask(func, filename, std::ref(buffer));
}

GLOWE::AsyncIOMgr::IsReadyFlag GLOWE::AsyncIOMgr::readWholeFileFromPackage(const String& packageName, const String& filename, String& buffer)
{
	auto&& func = [this](const String filename, const String packageName, String& buffer)
	{
		Package& package = getPackageForRead(packageName);

		if (package.checkIsOpen() && package.checkFileForExistence(filename))
		{
			package.readFile(filename, buffer);
		}
	};

	return threads.addTask(func, filename, packageName, std::ref(buffer));
}

GLOWE::AsyncIOMgr::IsReadyFlag GLOWE::AsyncIOMgr::readFromFile(const String& filename, void* data, const unsigned long long dataSize)
{
	auto&& func = [](const String filename, void* data, const unsigned long long dataSize)
	{
		File file;
		file.open(filename, std::ios::in | std::ios::binary);

		if (file.checkIsOpen())
		{
			unsigned long long temp = dataSize;
			const unsigned long long temp2 = file.getFileSize();

			if (temp > temp2)
			{
				temp = temp2;
			}

			file.read(data, temp);
		}
	};

	return threads.addTask(func, filename, data, dataSize);
}

GLOWE::AsyncIOMgr::IsReadyFlag GLOWE::AsyncIOMgr::readFromPackage(const String& packageName, const String& filename, void* data, const unsigned long long dataSize)
{
	auto&& func = [this](const String packageName, const String filename, void* data, const unsigned long long dataSize)
	{
		Package& package = getPackageForRead(packageName);

		if (package.checkIsOpen())
		{
			PackageEntryRead entry;
			entry.open(package, filename);
			if (entry.checkIsOpen())
			{
				entry.read(data, dataSize);
				entry.close();
			}
			package.close();
		}
	};

	return threads.addTask(func, packageName, filename, data, dataSize);
}

GLOWE::AsyncIOMgr::IsReadyFlag GLOWE::AsyncIOMgr::readFromPackage(Package& package, const String& filename, void* data, const unsigned long long dataSize)
{
	auto&& func = [](Package& package, const String filename, void* data, const unsigned long long dataSize)
	{
		if (package.checkIsOpen())
		{
			PackageEntryRead entry;
			entry.open(package, filename);
			if (entry.checkIsOpen())
			{
				entry.read(data, dataSize);
				entry.close();
			}
		}
	};

	return threads.addTask(func, std::ref(package), filename, data, dataSize);
}

void GLOWE::AsyncIOMgr::waitForRemainingTasks() const
{
	threads.waitForRemainingTasks();
}
