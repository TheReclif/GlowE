#pragma once
#ifndef GLOWE_INL_THREADCLASS_INCLUDED
#define GLOWE_INL_THREADCLASS_INCLUDED

#include "ThreadClass.h"

// TODO IMPORTANT: Random dead locks.
// Possibly fixed.
template<class MutexType>
inline GLOWE::ReadWriteMutex<MutexType>::ReadWriteMutex()
	: writeFlag(false), readersActive(0), writersWaiting(0), mutex(), cv()
{
}

template<class MutexType>
void GLOWE::ReadWriteMutex<MutexType>::lockRead()
{
	UniqueLock<MutexType> lock(mutex);
	cv.wait(lock, [this]() -> bool
		{
			return (!writeFlag) && (writersWaiting == 0);
		});

	++readersActive;
}

template<class MutexType>
void GLOWE::ReadWriteMutex<MutexType>::lockWrite()
{
	UniqueLock<MutexType> lock(mutex);
	++writersWaiting;
	cv.wait(lock, [this]() -> bool
		{
			return (!writeFlag) && (readersActive == 0);
		});
	--writersWaiting;
	writeFlag = true;
}

template<class MutexType>
void GLOWE::ReadWriteMutex<MutexType>::unlockRead()
{
	UniqueLock<MutexType> lock(mutex);
	if ((--readersActive) == 0)
	{
		cv.notify_all();
	}
}

template<class MutexType>
void GLOWE::ReadWriteMutex<MutexType>::unlockWrite()
{
	UniqueLock<MutexType> lock(mutex);
	writeFlag = false;
	cv.notify_all();
}

template<class MutexType>
GLOWE::ReadWriteLock<MutexType>::ReadWriteLock(ReadWriteMutex<MutexType>& arg)
	: mutex(arg), lockType(0)
{
}

template<class MutexType>
GLOWE::ReadWriteLock<MutexType>::~ReadWriteLock()
{
	unlock();
}

template<class MutexType>
void GLOWE::ReadWriteLock<MutexType>::lockRead()
{
	if (lockType == 0)
	{
		mutex.lockRead();
		lockType = 1;
	}
}

template<class MutexType>
void GLOWE::ReadWriteLock<MutexType>::lockWrite()
{
	if (lockType == 0)
	{
		mutex.lockWrite();
		lockType = 2;
	}
}

template<class MutexType>
void GLOWE::ReadWriteLock<MutexType>::unlock()
{
	switch (lockType)
	{
	case 0:
		return;
	case 1:
		mutex.unlockRead();
		break;
	case 2:
		mutex.unlockWrite();
		break;
	}

	lockType = 0;
}

template<class MutexType>
unsigned char GLOWE::ReadWriteLock<MutexType>::getLockType() const
{
	return lockType;
}

template<class MutexType>
GLOWE::RWReadLock<MutexType>::RWReadLock(ReadWriteMutex<MutexType>& arg)
	: lk(arg)
{
	lock();
}

template<class MutexType>
void GLOWE::RWReadLock<MutexType>::lock()
{
	lk.lockRead();
}

template<class MutexType>
void GLOWE::RWReadLock<MutexType>::unlock()
{
	lk.unlock();
}

template<class MutexType>
GLOWE::RWWriteLock<MutexType>::RWWriteLock(ReadWriteMutex<MutexType>& arg)
	: lk(arg)
{
	lock();
}

template<class MutexType>
void GLOWE::RWWriteLock<MutexType>::lock()
{
	lk.lockWrite();
}

template<class MutexType>
void GLOWE::RWWriteLock<MutexType>::unlock()
{
	lk.unlock();
}

#endif
