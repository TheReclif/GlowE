#pragma once
#ifndef GLOWE_SYSTEM_ASYNCIO_INCLUDED
#define GLOWE_SYSTEM_ASYNCIO_INCLUDED

#include "ThreadPool.h"
#include "Package.h"
#include "FileIO.h"

namespace GLOWE
{
	class GLOWSYSTEM_EXPORT AsyncIOMgr
		: public Subsystem
	{
	public:
		using IsReadyFlag = ThreadPool::TaskResult<void>;
	private:
		ThreadPool threads;

		Mutex writePackagesMute;
		Map<Hash, Pair<Package, Mutex>> writePackages;

		PackageMgr& packageMgr;
	private:
		Package& getPackageForRead(const String& filename);
	public:
		AsyncIOMgr();
		~AsyncIOMgr() = default;

		IsReadyFlag writeToFile(const String& filename, const void* data, const unsigned long long dataSize, const std::function<void()>& callbackAtCompletion = nullptr);
		IsReadyFlag writeToPackage(const String& packageName, const String& filename, const void* data, const unsigned long long dataSize, const std::function<void(Package&)>& callbackAtCompletion = nullptr);
		IsReadyFlag applyPackageChanges(const String& packageName);
		IsReadyFlag discardPackageChanges(const String& packageName);

		IsReadyFlag readWholeFile(const String& filename, String& buffer);
		IsReadyFlag readWholeFileFromPackage(const String& packageName, const String& filename, String& buffer);

		IsReadyFlag readFromFile(const String& filename, void* data, const unsigned long long dataSize);
		IsReadyFlag readFromPackage(const String& packageName, const String& filename, void* data, const unsigned long long dataSize);
		IsReadyFlag readFromPackage(Package& package, const String& filename, void* data, const unsigned long long dataSize);

		void waitForRemainingTasks() const;
	};
}

#endif
