#pragma once
#ifndef THREAD_POOL_INCLUDED
#define THREAD_POOL_INCLUDED

#include "Utility.h"
#include "Error.h"
#include "ThreadClass.h"

namespace GLOWE
{
	template<class T>
	using Future = std::template future<T>;
	template<class T>
	using SharedFuture = std::template shared_future<T>;

	class GLOWSYSTEM_EXPORT ThreadPool
	{
	public:
		template<class T>
		using TaskResult = Future<T>;
		template<class T>
		using SharedTaskResult = SharedFuture<T>;
	private:
		using TaskFunction = std::function<void()>;
		using ConditionalVariable = std::condition_variable;
		using Mutex = std::mutex;
		using MutexUniqueLock = std::unique_lock<Mutex>;
		using MutexGuard = std::lock_guard<Mutex>;
		template<class Func, class ...Args>
		using ReturnType = typename std::result_of<Func(Args...)>::type;

		/*struct OrderedTask
		{
			Importance importance;
			TaskFunction func;

			template<class FuncType>
			inline OrderedTask(const Importance order, FuncType&& task)
				: importance(order), func(std::forward<FuncType>(task))
			{}
		};

		struct OrderedTaskComparator
		{
			constexpr inline bool operator()(const OrderedTask& a, const OrderedTask& b) const
			{
				return a.importance < b.importance;
			}
		};*/
	private:
		Atomic<unsigned int> awaitingWorkers;
		Vector<Thread> workers;
		Queue<TaskFunction> tasks;
		ConditionalVariable conditional;
		Mutex mutex;

		Atomic<bool> shouldStop;
	public:
		ThreadPool() = default;
		explicit ThreadPool(const unsigned int workersCount);
		~ThreadPool();

		void create(const unsigned int& workersCount);
		void destroy();

		template<class Task, class ...Args>
		ThreadPool::template TaskResult<ThreadPool::template ReturnType<Task, Args...>> addTask(Task&& function, Args&&... args);

		/// @brief Returns the task container's mutex. Lock it to add multiple tasks more efficiently (without Vector<std::function<RT>> object in addTasks) in conjunction with addTaskNoLock. Call onLockEnd after all of the tasks are added, otherwise the worker threads may not take up the tasks and block indefinitely.
		/// @return Task container's mutex
		Mutex& getTasksMutex();
		template<class Task, class ...Args>
		ThreadPool::template TaskResult<ThreadPool::template ReturnType<Task, Args...>> addTaskNoLock(Task&& function, Args&&... args);
		/// @brief Call just after unlocking the tasks' mutex from getTasksMutex.
		void onLockEnd();

		/// @brief Adds multiple tasks efficiently with a default, Normal importance. This method does not allow passing arguments to the tasks.
		/// @tparam RT Tasks' return type
		/// @param tasksToAdd Taks to add
		/// @param results Task results' vector
		template<class RT>
		void addTasks(Vector<std::function<RT()>>&& tasksToAdd, Vector<TaskResult<RT>>& results);

		Vector<Thread::Id> getThreadIDs() const;
		unsigned int getThreadsCount() const;

		// Just a while loop. Nothing sophisticated.
		void waitForRemainingTasks() const;
	};

	template<class ReturnType>
	class PackagedTask
	{
	private:
		std::function<ReturnType()> func;
		Promise<ReturnType> promise;
	public:
		PackagedTask()
			: func(), promise(std::allocator_arg, StdAllocator<unsigned char>())
		{}
		template<class Func, class... Args>
		PackagedTask(Func&& fun, Args&&... args)
			: func(std::bind(std::forward<Func>(fun), std::forward<Args>(args)...)), promise(std::allocator_arg, StdAllocator<unsigned char>())
		{}
		template<class Func>
		PackagedTask(Func&& fun)
			: func(std::forward<Func>(fun)), promise(std::allocator_arg, StdAllocator<unsigned char>())
		{}
		virtual ~PackagedTask() = default;
		
		Future<ReturnType> getFuture()
		{
			return promise.get_future();
		}

		void operator()()
		{
			try
			{
				promise.set_value(func());
			}
			catch (...)
			{
				try
				{
					promise.set_exception(std::current_exception());
				}
				catch (...)
				{
					WarningThrow(false, "An exception has been thrown when setting promise's exception.");
				}
			}
		}
	};
	template<>
	class PackagedTask<void>
	{
	private:
		std::function<void()> func;
		Promise<void> promise;
	public:
		PackagedTask()
			: func(), promise(std::allocator_arg, StdAllocator<unsigned char>())
		{}
		template<class Func, class... Args>
		PackagedTask(Func&& fun, Args&&... args)
			: func(std::bind(std::forward<Func>(fun), std::forward<Args>(args)...)), promise(std::allocator_arg, StdAllocator<unsigned char>())
		{}
		template<class Func>
		PackagedTask(Func&& fun)
			: func(std::forward<Func>(fun)), promise(std::allocator_arg, StdAllocator<unsigned char>())
		{}
		virtual ~PackagedTask() = default;

		Future<void> getFuture()
		{
			return promise.get_future();
		}

		void operator()()
		{
			try
			{
				func();
				promise.set_value();
			}
			catch (const Exception& e)
			{
				WarningThrow(false, e.what() + "\nat\n" + e.getBacktraceStr());
				try
				{
					promise.set_exception(std::current_exception());
				}
				catch (...)
				{
					WarningThrow(false, "An exception has been thrown when setting promise's exception.");
				}
			}
			catch (...)
			{
				try
				{
					promise.set_exception(std::current_exception());
				}
				catch (...)
				{
					WarningThrow(false, "An exception has been thrown when setting promise's exception.");
				}
			}
		}
	};

	template<class Task, class ...Args>
	ThreadPool::template TaskResult<ThreadPool::template ReturnType<Task, Args...>> ThreadPool::addTask(Task&& function, Args&& ...args)
	{
		//auto task = makeSharedPtr<std::packaged_task<ReturnType<Task, Args...>()>>(std::allocator_arg, StdAllocator<unsigned char>(), std::bind(std::forward<Task>(function), std::forward<Args>(args)...));
		auto packagedTask = makeUniquePtr<PackagedTask<ThreadPool::template ReturnType<Task, Args...>>>(std::forward<Task>(function), std::forward<Args>(args)...);
		auto returnVal = packagedTask->getFuture();

		{
			MutexGuard guard(mutex);

			if (shouldStop || workers.empty())
			{
				ErrorThrow(false);
				return returnVal;
			}

			tasks.emplace(std::bind([](PackagedTask<ThreadPool::template ReturnType<Task, Args...>>* ptr)
			{
				UniquePtr<PackagedTask<ThreadPool::template ReturnType<Task, Args...>>> guard(ptr);
				(*ptr)();
			}, packagedTask.release()));
		}
		conditional.notify_one();

		return returnVal;
	}

	template<class Task, class ...Args>
	ThreadPool::template TaskResult<ThreadPool::template ReturnType<Task, Args...>> ThreadPool::addTaskNoLock(Task&& function, Args && ...args)
	{
		auto packagedTask = makeUniquePtr<PackagedTask<ThreadPool::template ReturnType<Task, Args...>>>(std::forward<Task>(function), std::forward<Args>(args)...);
		auto returnVal = packagedTask->getFuture();

		if (shouldStop || workers.empty())
		{
			ErrorThrow(false);
			return returnVal;
		}

		tasks.emplace(std::bind([](PackagedTask<ThreadPool::template ReturnType<Task, Args...>>* ptr)
		{
			UniquePtr<PackagedTask<ThreadPool::template ReturnType<Task, Args...>>> guard(ptr);
			(*ptr)();
		}, packagedTask.release()));

		return returnVal;
	}

	template<class RT>
	void ThreadPool::addTasks(Vector<std::function<RT()>>&& tasksToAdd, Vector<TaskResult<RT>>& results)
	{
		MutexGuard guard(mutex);

		if (shouldStop || workers.empty())
		{
			ErrorThrow(false);
			return;
		}

		for (auto& x : tasksToAdd)
		{
			auto packTask = makeUniquePtr<PackagedTask<RT>>(std::move(x));
			results.emplace_back(packTask->getFuture());
			tasks.emplace(std::bind([](PackagedTask<RT>* ptr)
			{
				UniquePtr<PackagedTask<RT>> guard(ptr);
				(*ptr)();
			}, packTask.release()));
		}

		conditional.notify_all();

		tasksToAdd.clear();
	}
}

#endif