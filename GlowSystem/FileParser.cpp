#include "FileParser.h"

const GLOWE::String GLOWE::FileParser::globalSectionName = u8"__Global__";

GLOWE::FileParser::FileParser()
{
	sections.insert(std::make_pair(globalSectionName, Section()));
}

GLOWE::FileParser::FileParser(const String & filename)
{
	this->parseFromFile(filename);
}

void GLOWE::FileParser::parseFromFile(const String & filename)
{
	File file;
	file.open(filename, std::ios::in);
	if (!file.checkIsOpen())
	{
		return;
	}

	String temp(file.getFileSize());
	file.read(temp.getCharArray(), temp.getSize());

	file.close();

	parseFromString(temp);
}

void GLOWE::FileParser::parseToFile(const String & filename) const
{
	File file;
	file.open(filename, std::ios::out | std::ios::trunc);

	String temp;
	parseToString(temp);

	file.write(temp.getCharArray(), temp.getSize());

	file.close();
}

void GLOWE::FileParser::parseToString(String & buffOut) const
{
	OStringStream oss;

	try
	{
		for (const auto& x : sections.at(globalSectionName))
		{
			oss << Value::getTypename(x.second->getType()) << u8" " << x.first << u8" = " << x.second->asString() << u8"\n";
		}
	}
	catch (...)
	{

	}

	for (const auto& section : sections)
	{
		if (section.first != globalSectionName)
		{
			oss << u8"[" << section.first << u8"]\n";

			for (const auto& x : section.second)
			{
				oss << Value::getTypename(x.second->getType()) << u8" " << x.first << u8" = " << x.second->asString() << u8"\n";
			}
		}
	}

	buffOut = oss.str();
}

void GLOWE::FileParser::parseFromString(const String & buffIn)
{
	sections.clear();
	IStringStream stream(buffIn);

	String currentSection = globalSectionName;
	String name, value, type;
	String line;

	ValuePtr tempVal;

	bool isNewLine = true, isSectionBeingNamed = false, isStringBeingParsed = false, wasEqualSignAdded = false;

	enum class ParsingStage
	{
		None,
		Type,
		Name,
		Value
	};

	ParsingStage stage = ParsingStage::None;

	while (!stream.eof())
	{
		tempVal = nullptr;
		std::getline(stream, line.getString());
		if (line.getString().empty())
		{
			return;
		}

		isNewLine = true;
		isSectionBeingNamed = false;
		stage = ParsingStage::None;
		isStringBeingParsed = false;
		wasEqualSignAdded = false;

		for (const auto& x : line.getString())
		{
			if (isNewLine)
			{
				if (x == '[')
				{
					isSectionBeingNamed = true;
					currentSection = u8"";
				}
				else if (std::iscntrl(x, getInstance<Locale>().getLocale()))
				{
					continue;
				}
				else if (std::isalpha(x, getInstance<Locale>().getLocale()))
				{
					stage = ParsingStage::Type;
					type.getString().clear();
					type.getString() += x;
				}
				else
				{
					WarningThrow(false, String(u8"Unexpected token. Line: ") + line);
					break;
				}

				isNewLine = false;
			}
			else
			{
				if (isSectionBeingNamed)
				{
					if (x == ']')
					{
						sections.insert(std::make_pair(currentSection, Section()));
						break;
					}
					else if (std::isalnum(x, getInstance<Locale>().getLocale()))
					{
						currentSection.getString() += x;
					}
					else
					{
						WarningThrow(false, String(u8"Unexpected token. Line: ") + line);
						break;
					}
				}
				else
				{
					if (std::isalnum(x, getInstance<Locale>().getLocale()) || (std::ispunct(x, getInstance<Locale>().getLocale()) && x != '='))
					{
						switch (stage)
						{
						case ParsingStage::Type:
							type.getString() += x;
							break;
						case ParsingStage::Name:
							name.getString() += x;
							break;
						case ParsingStage::Value:
							value.getString() += x;
							isStringBeingParsed = true;
							break;
						default:
							stage = ParsingStage::Type;
							type.getString().clear();
							type.getString() += x;
							break;
						}
					}
					else if (std::isblank(x, getInstance<Locale>().getLocale()))
					{
						switch (stage)
						{
						case ParsingStage::Type:
							name.getString().clear();
							stage = ParsingStage::Name;
							break;
						case ParsingStage::Name:
							value.getString().clear();
							stage = ParsingStage::Value;
							break;
						case ParsingStage::Value:
							if (isStringBeingParsed)
							{
								value.getString() += x;
							}
							break;
						default:
							break;
						}
					}
					else if (x == '=')
					{
						if (!wasEqualSignAdded)
						{
							if (stage == ParsingStage::Name)
							{
								stage = ParsingStage::Value;
							}
							else if (stage == ParsingStage::Value && !isStringBeingParsed)
							{
							}
							else
							{
								WarningThrow(false, String(u8"Unexpected token. Line: ") + line);
								break;
							}

							wasEqualSignAdded = true;
						}
						else
						{
							WarningThrow(false, String(u8"Illegal token. Line: ") + line);
							break;
						}
					}
					else
					{
						WarningThrow(false, String(u8"Unexpected token. Line: ") + line);
						break;
					}
				}
			}
		}

#define ValueTypeIf(typeToCheck) \
	if(type == GlowU8Stringify(typeToCheck)) \
	{ \
		tempVal = makeSharedPtr<typeToCheck##Value>(); \
		tempVal->fromString(value); \
	} \
	else

		if (!isSectionBeingNamed)
		{
			ValueTypeIf(Float)
			ValueTypeIf(FloatArray)
			ValueTypeIf(Int)
			ValueTypeIf(IntArray)
			ValueTypeIf(UInt)
			ValueTypeIf(UIntArray)
			ValueTypeIf(Double)
			ValueTypeIf(DoubleArray)
			ValueTypeIf(Bool)
			ValueTypeIf(BoolArray)
			ValueTypeIf(String)
			ValueTypeIf(StringArray)
			{
				WarningThrow(false, u8"Type is incorrect. Type: " + type);
			}

#undef ValueTypeIf

			if (tempVal)
			{
				sections[currentSection].insert(std::make_pair(name, tempVal));
			}
		}
	}
}

void GLOWE::FileParser::setValue(const String & name, const ValuePtr& arg, const String & section)
{
	sections[section][name] = arg;
}

GLOWE::FileParser::ValuePtr GLOWE::FileParser::getValue(const String & name, const String& section) const
{
	const auto secIt = sections.find(section);
	if (secIt == sections.end())
	{
		return nullptr;
	}

	const auto valIt = secIt->second.find(name);
	if (valIt == secIt->second.end())
	{
		return nullptr;
	}

	return valIt->second;
}

bool GLOWE::FileParser::doesValueExist(const String & name, const String & section) const
{
	return getValue(name, section) != nullptr;
}

bool GLOWE::FileParser::doesSectionExist(const String & section) const
{
	return sections.count(section);
}

GLOWE::FileParser::Section & GLOWE::FileParser::getSection(const String& sectionName)
{
	return sections[sectionName];
}

const GLOWE::FileParser::Section & GLOWE::FileParser::getSection(const String & sectionName) const
{
	return sections.at(sectionName);
}
