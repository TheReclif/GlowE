#pragma once
#ifndef GLOWE_SYSTEM_ACCESSORS_INCLUDED
#define GLOWE_SYSTEM_ACCESSORS_INCLUDED

#include "String.h"

namespace GLOWE
{
	/// @brief Base accessor class. Accessor allows to access a value that physically exists in the memory or is accessed using setters and getters, i.e. unifies the way of accessing a variable.
	class GLOWSYSTEM_EXPORT BaseAccessor
	{
	public:
		enum class Type
		{
			Simple,
			MultipleVars,
			MultipleVarsInPairs
		};
	public:
		virtual ~BaseAccessor() = default;

		virtual Hash getHash() const = 0;
		virtual std::type_index getType() const = 0;
		virtual Type getAccessorType() const = 0;
	};

	/// @brief An accessor for simple types (just copying is used).
	/// @see GLOWE::BaseAccessor
	/// @tparam T Simple type.
	template<class T>
	class SimpleAccessor
		: public BaseAccessor
	{
	private:
		std::function<T()> getter;
		std::function<void(const T&)> setter;
	public:
		SimpleAccessor() = delete;
		/// @brief Creates an accessor from a getter and a setter functions.
		/// @tparam GetFunc Getter's type. Must be invokable with no parameters and return T.
		/// @tparam SetFunc Setter's type. Must be invokable with a const reference to T as a parameter.
		/// @param getFun Getter. Must be invokable with no parameters and return T.
		/// @param setFun Setter. Must be invokable with a const reference to T as a parameter.
		template<class GetFunc, class SetFunc>
		SimpleAccessor(GetFunc&& getFun, SetFunc&& setFun)
			: getter(std::forward<GetFunc>(getFun)), setter(std::forward<SetFunc>(setFun))
		{
		}

		/// @brief Creates an accessor from a reference to a variable placed in memory.
		/// @param value Reference to the variable.
		SimpleAccessor(T& value)
			: SimpleAccessor([&]() mutable -> T { return value; }, [&](const T& newVal) mutable { value = newVal; })
		{
		}

		/// @brief Gets the value.
		/// @return Value.
		T get() const
		{
			return getter();
		}
		/// @brief Sets the value.
		/// @param val New value.
		void set(const T& val)
		{
			setter(val);
		}

		virtual Hash getHash() const override
		{
			const T temp = get();
			return Hash(reinterpret_cast<const char* const>(&temp), sizeof(T));
		}
		virtual std::type_index getType() const override
		{
			return typeid(SimpleAccessor<T>);
		}
		virtual Type getAccessorType() const override
		{
			return Type::Simple;
		}
	};

	template<>
	inline Hash SimpleAccessor<String>::getHash() const
	{
		return Hash(get());
	}

	using IntAccessor = SimpleAccessor<int>;
	using UIntAccessor = SimpleAccessor<unsigned int>;

	using Int64Accessor = SimpleAccessor<Int64>;
	using UInt64Accessor = SimpleAccessor<UInt64>;

	using FloatAccessor = SimpleAccessor<float>;
	using DoubleAccessor = SimpleAccessor<double>;

	using StringAccessor = SimpleAccessor<String>;

	/// @brief An accessor for a array of simple types (not other arrays).
	/// @see GLOWE::BaseAccessor
	/// @tparam T Simple type.
	template<class T>
	class ArrayOfSimplesAccessor
		: public BaseAccessor
	{
	private:
		std::function<UInt64()> getSizeFunc;
		std::function<void(const UInt64)> setSizeFunc;

		std::function<Vector<T>()> arrayGetter;
		std::function<T(const UInt64)> elementGetter;

		std::function<void(const T* const, const UInt64)> arraySetter;
		std::function<void(const T&, const UInt64)> elementSetter;

		std::function<Hash()> hashGetter;
	public:
		ArrayOfSimplesAccessor() = delete;
		// TODO: Fill this... s*it.
		/// @brief Creates an array accessor from provided functions.
		/// @param getSizeFun 
		/// @param setSizeFun 
		/// @param arrGetter 
		/// @param elemGetter 
		/// @param arrSetter 
		/// @param elemSetter 
		template<class GetSizeFunc, class SetSizeFunc, class ArrayGetter, class ElemGetter, class ArraySetter, class ElemSetter, class HashGetter>
		ArrayOfSimplesAccessor(
			GetSizeFunc&& getSizeFun,
			SetSizeFunc&& setSizeFun,
			ArrayGetter&& arrGetter,
			ElemGetter&& elemGetter,
			ArraySetter&& arrSetter,
			ElemSetter&& elemSetter,
			HashGetter&& hashSetFunc)
			:
			getSizeFunc(std::forward<GetSizeFunc>(getSizeFun)),
			setSizeFunc(std::forward<SetSizeFunc>(setSizeFun)),
			arrayGetter(std::forward<ArrayGetter>(arrGetter)),
			elementGetter(std::forward<ElemGetter>(elemGetter)),
			arraySetter(std::forward<ArraySetter>(arrSetter)),
			elementSetter(std::forward<ElemSetter>(elemSetter)),
			hashGetter(std::forward<HashGetter>(hashSetFunc))
		{}
		/// @brief Creates an accessor for a fixed size array.
		/// @param arr Accessed array.
		template<UInt64 ArraySize>
		ArrayOfSimplesAccessor(T(&arr)[ArraySize])
			: getSizeFunc([]() mutable -> UInt64 { return ArraySize; }),
			setSizeFunc(nullptr),
			arrayGetter([&]() mutable -> Vector<T> { return Vector<T>(arr, arr + ArraySize); }),
			elementGetter([&](const UInt64 elemId) mutable -> T { return arr[elemId]; }),
			arraySetter([&](const T* const data, const UInt64 arraySize) mutable { std::copy(data, data + (clamp(arraySize, static_cast<UInt64>(0), ArraySize)), arr); }),
			elementSetter([&](const T& elem, const UInt64 whichElem) mutable { if (whichElem < ArraySize) arr[whichElem] = elem; }),
			hashGetter([&]() -> Hash { return Hash(reinterpret_cast<const char* const>(arr), sizeof(T) * ArraySize); })
		{
		}
		/// @brief Creates an accessor for a vector (resizable array).
		/// @param arr Accessed vector.
		ArrayOfSimplesAccessor(Vector<T>& arr)
			: getSizeFunc([&]() mutable -> UInt64 { return arr.size(); }),
			setSizeFunc([&](const UInt64 newSize) mutable { arr.resize(newSize); }),
			arrayGetter([&]() mutable -> Vector<T> { return arr; }),
			elementGetter([&](const UInt64 elemId) mutable -> T { return arr[elemId]; }),
			arraySetter([&](const T* const data, const UInt64 arraySize) mutable { arr.assign(data, data + arraySize); }),
			elementSetter([&](const T& elem, const UInt64 whichElem) mutable { if (whichElem < arr.size()) arr[whichElem] = elem; }),
			hashGetter([&]() -> Hash { return Hash(reinterpret_cast<const char* const>(arr.data()), sizeof(T) * arr.size()).data ^ arr.size(); })
		{
		}
		/// @brief Creates an accessor for a fixed size array (in a std::array object).
		/// @param arr Accessed array.
		template<UInt64 ArraySize>
		ArrayOfSimplesAccessor(Array<T, ArraySize>& arr)
			: getSizeFunc([]() mutable -> UInt64 { return ArraySize; }),
			setSizeFunc(nullptr),
			arrayGetter([&]() mutable -> Vector<T> { return Vector<T>(arr.begin(), arr.begin() + ArraySize); }),
			elementGetter([&](const UInt64 elemId) mutable -> T { return arr[elemId]; }),
			arraySetter([&](const T* const data, const UInt64 arraySize) mutable { std::copy(data, data + (clamp(arraySize, static_cast<UInt64>(0), ArraySize)), arr.begin()); }),
			elementSetter([&](const T& elem, const UInt64 whichElem) mutable { if (whichElem < ArraySize) arr[whichElem] = elem; }),
			hashGetter([&]() -> Hash { return Hash(reinterpret_cast<const char* const>(arr.data()), sizeof(T) * ArraySize); })
		{
		}

		/// @brief Returns size of the array.
		/// @return Array's size.
		UInt64 getSize() const
		{
			return getSizeFunc();
		}
		/// @brief If possible, changes the size of the array.
		/// @param newSize Size to set.
		void setSize(const UInt64 newSize)
		{
			if (setSizeFunc)
			{
				setSizeFunc(newSize);
			}
		}
		/// @brief Returns whether the array can be resized.
		/// @return true if possible, false otherwise.
		bool checkCanChangeSize() const
		{
			return static_cast<bool>(setSizeFunc);
		}

		/// @brief Returns a copy of the array.
		/// @return Vector being a copy of the original array.
		Vector<T> getArray() const
		{
			return arrayGetter();
		}
		/// @brief Returns (without checking bounds) n-th element.
		/// @param id Which element.
		/// @return Requested element.
		T getElement(const UInt64 id) const
		{
			return elementGetter(id);
		}

		/// @brief Replaces contents of the current array and, if possible, resizes the array to match the given array.
		/// @param newArr Given array.
		void setArray(const Vector<T>& newArr)
		{
			arraySetter(newArr.data(), newArr.size());
		}
		/// @brief Replaces contents of the current array and, if possible, resizes the array to match the given array.
		/// @param arr Given array.
		/// @param arrSize Size of the given array.
		void setArray(const T* const arr, const UInt64 arrSize)
		{
			arraySetter(arr, arrSize);
		}
		/// @brief Sets the n-th element.
		/// @param element Value to be set.
		/// @param elemId Which element to set.
		void setElement(const T& element, const UInt64 elemId)
		{
			elementSetter(element, elemId);
		}

		virtual Hash getHash() const override
		{
			return hashGetter();
		}
		virtual std::type_index getType() const override
		{
			return typeid(ArrayOfSimplesAccessor<T>);
		}
		virtual Type getAccessorType() const override
		{
			return Type::MultipleVars;
		}
	};
}

#endif
