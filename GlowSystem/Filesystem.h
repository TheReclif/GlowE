#pragma once
#ifndef GLOWE_SYSTEM_FILESYSTEM_INCLUDED
#define GLOWE_SYSTEM_FILESYSTEM_INCLUDED

#include "Utility.h"
#include "String.h"

#if defined(GLOWE_COMPILE_FOR_WINDOWS)
#include "Windows/WindowsFilesystem.h"
#elif defined(GLOWE_COMPILE_FOR_LINUX)
#include "Linux/LinuxFilesystem.h"
#endif

namespace GLOWE
{
#if defined(GLOWE_COMPILE_FOR_WINDOWS)
	using FilePath = Windows::FilePath;
	using Filesystem = Windows::Filesystem;
	using FileListing = Windows::FileListing;
	using FileWatcher = Windows::FilesystemWatcher;
#elif defined(GLOWE_COMPILE_FOR_LINUX)
	using FilePath = Linux::FilePath;
	using Filesystem = Linux::Filesystem;
	using FileListing = Linux::FileListing;
	using FileWatcher = Linux::FilesystemWatcher;
#endif
}

#endif
