#pragma once
#ifndef GLOWE_DEBUG_INCLUDED
#define GLOWE_DEBUG_INCLUDED

#include "glowsystem_export.h"

#if defined(_MSC_VER) && defined(DEBUG)
#define GLOW_EXPAND_MACRO(x) __pragma(message(__FILE__ _CRT_STRINGIZE((__LINE__): \nmacro\t)#x" expands to:\n" _CRT_STRINGIZE(x)))
#else
#define GLOW_EXPAND_MACRO(x)
#endif

namespace GLOWE
{
	class String;

	GLOWSYSTEM_EXPORT void debugPrint(const String& msg);
	GLOWSYSTEM_EXPORT void debugPrintln(const String& msg);
}

#endif
