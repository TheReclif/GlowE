#pragma once
#ifndef GLOW_SYSTEM_RANDOM_INCLUDED
#define GLOW_SYSTEM_RANDOM_INCLUDED

#include "glowsystem_export.h"

#include <random>

namespace GLOWE
{
	class GLOWSYSTEM_EXPORT Random
		: public Subsystem
	{
	private:
		std::mt19937 randomGen;
		std::uniform_int_distribution<unsigned int> uintDist;
		std::uniform_int_distribution<int> intDist;
		std::uniform_real_distribution<float> floatDist;
		std::uniform_real_distribution<double> doubleDist;
		long long seed;
	public:
		Random();
		Random(const long long newSeed);
		~Random() = default;

		unsigned int randomUInt(const unsigned int minInt = 0, const unsigned int maxInt = std::numeric_limits<unsigned int>::max());
		int randomInt(const int minInt = std::numeric_limits<int>::min(), const int maxInt = std::numeric_limits<int>::max());
		double randomDouble(const double minDouble = 0.0, const double maxDouble = 0.0);
		float randomFloat(const float minFloat = 0.0, const float maxFloat = 0.0);

		/// @brief Returns the internal mt19937 generator. For use with other distributions e.g. normal distribution.
		/// @return Reference to the internal mt19937 generator.
		std::mt19937& getGenerator();

		void reseed();
		void reseed(const long long newSeed);

		long long getSeed() const;
	};
}

#endif
