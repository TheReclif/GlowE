#pragma once
#ifndef GLOWE_ENGINE_RENDERINGENGINE_INCLUDED
#define GLOWE_ENGINE_RENDERINGENGINE_INCLUDED

#include "Material.h"

#include "../GlowSystem/ThreadPool.h"

#include "../GlowMath/Frustum.h"

#include "../GlowGraphics/Uniform/BasicRendererImplementation.h"
#include "../GlowGraphics/Uniform/RenderTargetImplementation.h"
#include "../GlowGraphics/Uniform/Viewport.h"
#include "../GlowGraphics/Uniform/RenderingMgr.h"

namespace GLOWE
{
	class Renderable;

	class GLOWENGINE_EXPORT RenderingEngine
		: public Subsystem
	{
	public:
		struct RenderPass
		{
			struct Settings
			{
				bool needsLightsCulling, needsRenderablesCulling;

				Settings();
			};
			Settings settings;

			RenderTargetImpl* target; // No ownership.
			Set<unsigned int> notRendered;

			// When overriden prepareMatEnvFunc is unused.
			struct Callbacks
			{
				std::function<void(BasicRendererImpl&)> clearTargetFunc;
				std::function<void(MaterialEnvironment&)> prepareMatEnvFunc;
				std::function<const Frustum & ()> getFrustumFunc;
				std::function<const Viewport & ()> getViewportFunc;
				std::function<void(ThreadPool&, Vector<ThreadPool::TaskResult<void>>&, Deque<CommandList>&)> renderOverrideMultiThreaded;
				std::function<void(BasicRendererImpl&)> renderOverrideSingleThread;
				std::function<unsigned int()> renderOverrideGetCommandListsCount;
				std::function<bool()> shouldRender = [] { return true; };

				Callbacks() = default;
			};
			Callbacks callbacks;
			MaterialEnvironment matEnv;

			RenderPass() = default;
		};

		struct OnRenderCallback
		{
			/// @brief Function to call.
			std::function<void(const RenderPass&, BasicRendererImpl&)> callback;
			/// @brief When (in the rendering function) the callback shall be called.
			enum class When : unsigned char
			{
				/// @brief Called before the rendering/scheduling process takes place, but after the pass initialization (i.e. after prepareRenderPass was called and culling was performed).
				Before,
				/// @brief Called just after the rendering/scheduling process takes place.
				After
			} when = When::Before;

			inline OnRenderCallback(std::function<void(const RenderPass&, BasicRendererImpl&)>&& func, const When whenToCall)
				: callback(std::move(func)), when(whenToCall)
			{}
		};
	private:
		Map<Material*, List<Renderable*>> renderables; // No ownership.
		Multimap<int, RenderPass> renderPasses;

		// Schedule data.
		Mutex scheduleMutex;
		Vector<ThreadPool::TaskResult<void>> scheduleCommandLists;
		Deque<CommandList> resultingScheduleCommandLists;
		Vector<Frustum> scheduleFrustums;
		MaterialEnvironment env;
		List<OnRenderCallback> afterRenderCallbacks, beforeRenderCallbacks;
	public:
		using RenderPassHandle = Multimap<int, RenderPass>::iterator;
		using RenderableHandle = List<Renderable*>::const_iterator;
		using OnRenderCallbackHandle = List<OnRenderCallback>::const_iterator;
	private:
		static void prepareRenderPass(BasicRendererImpl& renderer, const RenderPass& rp, MaterialEnvironment& matEnv);
		static void prepareRenderPass(BasicRendererImpl& renderer, const RenderPass& rp);
		void updateEngineConstants();
	public:
		// Note: There can be multiple render passes with the same id. Order of their rendering is not well defined, although they should render in ther placement order, i.e. the first one will be rendered first.
		RenderPassHandle addRenderPass(RenderTargetImpl& renderTarget, const Vector<unsigned int>& notRendered, const int id, const RenderPass::Callbacks& callbacks = RenderPass::Callbacks(), const RenderPass::Settings& settings = RenderPass::Settings());
		void removeRenderPass(const RenderPassHandle& it);

		RenderableHandle addRenderable(Renderable* renderable);
		void removeRenderable(const RenderableHandle& handle);

		OnRenderCallbackHandle addRenderCallback(OnRenderCallback&& callback);
		void removeRenderCallback(const OnRenderCallbackHandle& handle);

		// Note: This one will use RenderingMgr.
		void render();
		void render(BasicRendererImpl& renderer);

		ThreadPool::TaskResult<void> scheduleRender(ThreadPool& threads);
	};
}

#endif
