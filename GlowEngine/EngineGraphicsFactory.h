#pragma once
#ifndef GLOWE_ENGINE_GRAPHICSFACTORY_INCLUDED
#define GLOWE_ENGINE_GRAPHICSFACTORY_INCLUDED

#include "ShaderCacheImplementation.h"

#include "../GlowGraphics/Uniform/GraphicsFactory.h"

namespace GLOWE
{
	class GLOWENGINE_EXPORT EngineGraphicsFactory
		: public GraphicsFactoryImpl
	{
	public:
		virtual ~EngineGraphicsFactory() = default;

		virtual UniquePtr<ShaderCacheImpl> createShaderCache() const = 0;
	};

	template<>
	GLOWENGINE_EXPORT EngineGraphicsFactory& OneInstance<EngineGraphicsFactory>::instance();

	template<>
	GLOWENGINE_EXPORT UniquePtr<ShaderCacheImpl> createGraphicsObject();
}

#endif
