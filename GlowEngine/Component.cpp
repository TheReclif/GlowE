#include "Component.h"
#include "GameObject.h"

GLOWE::Components::Component::Component(GameObject* newParent)
	: isActive(true), gameObject(newParent), wasFirstActivated(false)
{
}

GLOWE::GameObject* GLOWE::Components::Component::getGameObject() const
{
	return gameObject;
}

void GLOWE::Components::Component::setGameObject(GameObject* newOwner)
{
	if (newOwner && !gameObject)
	{
		gameObject = newOwner;
		isActive = true;
	}
}

bool GLOWE::Components::Component::checkIsActive() const
{
	return isActive && gameObject->checkIsActive();
}

bool GLOWE::Components::Component::checkIsActiveLocally() const
{
	return isActive;
}

bool GLOWE::Components::Component::checkWasFirstActivated() const
{
	return wasFirstActivated;
}

void GLOWE::Components::Component::setFirstActivation()
{
	wasFirstActivated = true;
}

void GLOWE::Components::Component::activate()
{
	if (!isActive)
	{
		isActive = true;
		onActivate();
	}
}

void GLOWE::Components::Component::deactivate()
{
	if (isActive)
	{
		isActive = false;
		onDeactivate();
	}
}

GLOWE::UniquePtr<GLOWE::Component> GLOWE::Components::ComponentFactoryStorage::createComponent(GameObject* const newParent, const Hash& hash)
{
	return factories.at(hash)->construct(newParent);
}

GLOWE::UniquePtr<GLOWE::Component> GLOWE::Components::ComponentFactoryStorage::createComponent(GameObject* const newParent, const String& name)
{
	return createComponent(newParent, Hash(name));
}

void GLOWE::Components::ComponentFactoryStorage::addFactory(AbstractComponentFactory& arg)
{
	// In case there's already a factory, overwrite it.
	factories[arg.getHash()] = &arg;
}
