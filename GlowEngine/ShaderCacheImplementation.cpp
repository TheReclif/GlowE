#include "ShaderCacheImplementation.h"
#include "../GlowGraphics/Uniform/GraphicsFactory.h"

template<>
GLOWE::ShaderCacheImpl& GLOWE::OneInstance<GLOWE::ShaderCacheImpl>::instance()
{
	static auto& cache = SubsystemsCore::getGlobalCore().registerSubsystem(createGraphicsObject<ShaderCacheImpl>());
	return cache;
}
