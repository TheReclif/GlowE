#include "GameObject.h"
#include "LayersMgr.h"
#include "Scene.h"
#include "SimpleRaceDetector.h"

#include "../GlowComponents/TransformComponent.h"

GLOWE::GameObject::GameObject(const String& str, Scene& newOwningScene, const CanCreateGameObjectTag&)
	: isActive(false), name(str), owningScene(&newOwningScene), transform(&addComponent<Components::Transform>()), parent(nullptr), layer(0), scheduledActDeact(ScheduledActDeact::None), components(), children(), gameRegistryHandle(getInstance<GameObjectRegistry>().registerGameObject(this))
{
	activate();
}

GLOWE::GameObject::~GameObject()
{
	orphan();
	getInstance<GameObjectRegistry>().deregisterGameObject(gameRegistryHandle);
	layer = std::numeric_limits<unsigned int>::max();
}

void GLOWE::GameObject::orphan()
{
	setParent(nullptr);
}

void GLOWE::GameObject::setParent(GameObject * newParent)
{
	SimpleRaceDetector<GameObject> _;

	if (parent)
	{
		parent->detachChild(this);
	}

	parent = newParent;

	if (newParent)
	{
		parent->attachChild(this);
	}

	const bool wasActive = scheduledActDeact == ScheduledActDeact::None ? checkIsActive() : scheduledActDeact == ScheduledActDeact::Deactivation;

	updateRememberedParentStatus();

	const bool isActiveNow = scheduledActDeact == ScheduledActDeact::None ? checkIsActive() : scheduledActDeact == ScheduledActDeact::Deactivation;

	if (wasActive && !isActiveNow)
	{
		scheduleDeactivate(getInstance<GameObjectRegistry>(), wasActive);
	}
	else
	if (!wasActive && isActiveNow)
	{
		scheduleActivate(getInstance<GameObjectRegistry>());
	}
}

GLOWE::Component * GLOWE::GameObject::getComponent(const String& type) const
{
	auto temp =  components.find(type);

	if (temp != components.end())
	{
		return temp->second.get();
	}

	return nullptr;
}

GLOWE::Vector<GLOWE::Component*> GLOWE::GameObject::getComponents(const String& type) const
{
	Vector<Component*> result;

	const auto temp = components.equal_range(type);

	for (auto it = temp.first; it != temp.second; ++it)
	{
		result.push_back(it->second.get());
	}

	return result;
}

const GLOWE::Multimap<GLOWE::String, GLOWE::UniquePtr<GLOWE::Component>>& GLOWE::GameObject::getAllComponents() const
{
	return components;
}

GLOWE::GameObject * GLOWE::GameObject::getParent() const
{
	return parent;
}

GLOWE::GameObject * GLOWE::GameObject::getChild(const String & childName) const
{
	auto it = children.find(childName);
	if (it != children.end())
	{
		return it->second;
	}

	return nullptr;
}

GLOWE::Components::Transform* GLOWE::GameObject::getTransform()
{
	return transform;
}

const GLOWE::Components::Transform* GLOWE::GameObject::getTransform() const
{
	return transform;
}

GLOWE::Component& GLOWE::GameObject::addComponent(const String& type)
{
	return addComponent(getInstance<Components::ComponentFactoryStorage>().createComponent(this, type));
}

GLOWE::Component& GLOWE::GameObject::addComponent(UniquePtr<Component>&& arg)
{
	SimpleRaceDetector<GameObject> _;

	Component& result = *components.insert(std::make_pair(arg->getTypeHash(), std::move(arg)))->second;
	ComponentLock lock(result);
	result.setGameObject(this);

	if (checkIsActive() && scheduledActDeact == ScheduledActDeact::None)
	{
		result.onFirstActivate();
		result.onActivate();
		result.setFirstActivation();
	}

	return result;
}

void GLOWE::GameObject::deleteComponent(Component * arg)
{
	// TODO: IMPORTANT! Possible race condition when a component tries to delete itself (most probably will fail in the most unexpected way if the programmer does it wrong... which is surprisingly easy).
	SimpleRaceDetector<GameObject> _;
	if (layer == std::numeric_limits<unsigned int>::max() || !checkIfContains(arg))
	{
		return;
	}

	const auto endIt = components.end();

	for (auto it = components.begin(); it != endIt; ++it)
	{
		if (it->second.get() == arg)
		{
			if (it->second->checkIsActive())
			{
				it->second->deactivate();
			}
			components.erase(it);
			return;
		}
	}
}

void GLOWE::GameObject::sendEvent(const GameEvent& event, const Component* sender)
{
	for (auto& component : components)
	{
		if (component.second->checkIsActive() && component.second.get() != sender)
		{
			ComponentLock lock(*component.second);
			component.second->processEvent(event);
		}
	}
}

void GLOWE::GameObject::sendEventToChildren(const GameEvent& event, const Component* sender)
{
	sendEvent(event, sender);

	for (auto& child : children)
	{
		if (child.second->checkIsActive())
		{
			child.second->sendEvent(event, sender);
		}
	}
}

void GLOWE::GameObject::sendEventToChildrenRecursive(const GameEvent& event, const Component* sender)
{
	sendEvent(event, sender);

	for (auto& child : children)
	{
		child.second->sendEventToChildrenRecursive(event, sender);
	}
}

GLOWE::Scene* GLOWE::GameObject::getOwningScene() const
{
	return owningScene;
}

void GLOWE::GameObject::setOwningScene(Scene* scene)
{
	SimpleRaceDetector<GameObject> _;
	if (!scene)
	{
		throw Exception("scene was nullptr");
	}

	if (parent)
	{
		if (parent->owningScene != scene)
		{
			throw StringException("Parent's owning scene is different than scene named " + scene->getName());
		}
	}

	scene->transferOwnership(this);

	for (auto& child : children)
	{
		child.second->setOwningScene(scene);
	}
}

GLOWE::String GLOWE::GameObject::getPath() const
{
	Vector<String> parts;
	parts.reserve(8);
	const GameObject* obj = this;
	unsigned int size = 0;
	while (obj)
	{
		parts.emplace_back(obj->getName());
		size += parts.back().getSize() + 1;
		obj = obj->getParent();
	}

	String result;
	result.reserve(size - 1);

	const auto end = parts.rend();
	for (auto x = parts.rbegin(); x != end; ++x)
	{
		result += (*x);
		result += '/';
	}

	result.erase(result.getSize() - 1, 1);
	return result;
}

bool GLOWE::GameObject::checkIfContains(const Component* component) const
{
	SimpleRaceDetector<GameObject> _;
	for (const auto& x : components)
	{
		if (component == x.second.get())
		{
			return true;
		}
	}

	return false;
}

void GLOWE::GameObject::attachChild(GameObject * newChild)
{
	if (newChild)
	{
		children.emplace(newChild->getName(), newChild);
	}
}

void GLOWE::GameObject::attachChild(const String & childName)
{
	GameObject* temp = getInstance<GameObjectRegistry>().getGameObject(childName);
	if (temp)
	{
		children.emplace(temp->getName(), temp);
	}
}

void GLOWE::GameObject::notifyAttachChild(GameObject * newChild)
{
	newChild->setParent(this);
}

void GLOWE::GameObject::notifyAttachChild(const String & childName)
{
	GameObject* temp = getInstance<GameObjectRegistry>().getGameObject(childName);
	if (temp)
	{
		temp->setParent(this);
	}
}

void GLOWE::GameObject::detachChild(GameObject * childToDetach)
{
	if (childToDetach)
	{
		children.erase(childToDetach->getName());
	}
}

void GLOWE::GameObject::detachChild(const String & childName)
{
	children.erase(childName);
}

void GLOWE::GameObject::notifyDetachChild(GameObject * childToDetach)
{
	if (childToDetach)
	{
		childToDetach->orphan();
	}
}

void GLOWE::GameObject::notifyDetachChild(const String & childName)
{
	GameObject* temp = getInstance<GameObjectRegistry>().getGameObject(childName);
	if (temp)
	{
		temp->orphan();
	}
}

bool GLOWE::GameObject::operator==(const String & arg) const
{
	return name == arg;
}

bool GLOWE::GameObject::operator==(const GameObject & arg) const
{
	return this == &arg;
}

const GLOWE::String & GLOWE::GameObject::getName() const
{
	return name;
}

GLOWE::GameObject::ScheduledActDeact GLOWE::GameObject::getScheduledActDeactInfo() const
{
	return scheduledActDeact;
}

void GLOWE::GameObject::ensureActDeact()
{
	SimpleRaceDetector<GameObject> _;
	switch (scheduledActDeact)
	{
	case ScheduledActDeact::None:
		return;
	case ScheduledActDeact::Activation:
		getInstance<GameObjectRegistry>().abortActivation(activateHandle);
		updateActivate();
		break;
	case ScheduledActDeact::Deactivation:
		getInstance<GameObjectRegistry>().abortDeactivation(activateHandle);
		updateDeactivate();
		break;
	}

	for (auto& child : children)
	{
		child.second->ensureActDeact();
	}

	scheduledActDeact = ScheduledActDeact::None;
}

void GLOWE::GameObject::setName(const String & arg)
{
	SimpleRaceDetector<GameObject> _;
	GameObjectRegistry& registry = getInstance<GameObjectRegistry>();
	registry.deregisterGameObject(gameRegistryHandle);
	name = arg;
	gameRegistryHandle = registry.registerGameObject(this);
}

void GLOWE::GameObject::scheduleActivate(GameObjectRegistry& registry)
{
	updateRememberedParentStatus();
	if (checkIsActive())
	{
		if (scheduledActDeact == ScheduledActDeact::Deactivation)
		{
			registry.abortDeactivation(activateHandle);
		}

		activateHandle = registry.activateGameObject(this);
		scheduledActDeact = ScheduledActDeact::Activation;

		for (const auto& child : children)
		{
			child.second->scheduleActivate(registry);
		}
	}
}

void GLOWE::GameObject::scheduleDeactivate(GameObjectRegistry& registry, const bool wasActive)
{
	updateRememberedParentStatus();
	if (wasActive)
	{
		if (scheduledActDeact == ScheduledActDeact::Activation)
		{
			registry.abortActivation(activateHandle);
		}

		activateHandle = registry.deactivateGameObject(this);
		scheduledActDeact = ScheduledActDeact::Deactivation;

		for (const auto& child : children)
		{
			child.second->scheduleDeactivate(registry, child.second->checkIsActiveLocally());
		}
	}
}

void GLOWE::GameObject::updateActivate()
{
	for (const auto& component : components)
	{
		if (component.second->checkIsActiveLocally())
		{
			ComponentLock lock(*component.second);
			if (!component.second->checkWasFirstActivated())
			{
				component.second->onFirstActivate();
				component.second->onActivate();
				component.second->setFirstActivation();
			}
			else
			{
				component.second->onActivate();
			}
		}
	}

	activateHandle = ActivateHandle();
	scheduledActDeact = ScheduledActDeact::None;
}

void GLOWE::GameObject::updateDeactivate()
{
	for (const auto& component : components)
	{
		if (component.second->checkIsActiveLocally())
		{
			component.second->onDeactivate();
		}
	}

	activateHandle = ActivateHandle();
	scheduledActDeact = ScheduledActDeact::None;
}

void GLOWE::GameObject::updateRememberedParentStatus()
{
	rememberedParentStatus = parent ? parent->checkIsActive() : true;
}

void GLOWE::GameObject::activate(const bool immediately)
{
	SimpleRaceDetector<GameObject> _;
	if (!checkIsActiveLocally())
	{
		isActive = true;

		if (!immediately)
		{
			scheduleActivate(getInstance<GameObjectRegistry>());
		}
		else
		{
			updateActivate();
		}
	}
}

void GLOWE::GameObject::deactivate(const bool immediately)
{
	SimpleRaceDetector<GameObject> _;
	if (checkIsActiveLocally())
	{
		const bool wasActive = checkIsActive();
		isActive = false;

		if (!immediately)
		{
			scheduleDeactivate(getInstance<GameObjectRegistry>(), wasActive);
		}
		else
		{
			updateDeactivate();
		}
	}
}

void GLOWE::GameObject::setActive(const bool arg)
{
	if (arg)
	{ 
		activate();
	}
	else
	{
		deactivate();
	}
}

bool GLOWE::GameObject::checkIsActive() const
{
	return isActive && rememberedParentStatus;
}

bool GLOWE::GameObject::checkIsActiveLocally() const
{
	return isActive;
}

void GLOWE::GameObject::setLayer(const unsigned int newLayer)
{
	SimpleRaceDetector<GameObject> _;
	if (getInstance<LayersMgr>().isLayerPresent(newLayer))
	{
		layer = newLayer;
	}
}

void GLOWE::GameObject::setLayer(const String& newLayer)
{
	SimpleRaceDetector<GameObject> _;
	LayersMgr& layersMgr = getInstance<LayersMgr>();
	if (layersMgr.isLayerPresent(newLayer))
	{
		layer = layersMgr.getLayer(newLayer);
	}
}

unsigned int GLOWE::GameObject::getLayer() const
{
	return layer;
}

GLOWE::GameObject* GLOWE::GameObject::findGameObject(const String& name)
{
	const auto scene = getInstance<SceneMgr>().getActiveScene();
	if (!scene)
	{
		return nullptr;
	}

	throw NotImplementedException();
}

GLOWE::GameObject& GLOWE::GameObject::createGameObject(const String& name, GameObject* parent, const bool activateImmediately)
{
	const auto scene = getInstance<SceneMgr>().getActiveScene();
	if (!scene)
	{
		throw Exception("Unable to create a GameObject: no active scene");
	}

	return createGameObject(name, *scene, parent, activateImmediately);
}

GLOWE::GameObject& GLOWE::GameObject::createGameObject(const String& name, Scene& scene, GameObject* parent, const bool activateImmediately)
{
	GameObject& result = scene.createGameObject(name);
	if (parent)
	{
		result.setParent(parent);
	}
	if (activateImmediately)
	{
		result.ensureActDeact();
	}
	return result;
}

void GLOWE::GameObject::destroy(GameObject* gameObject)
{
	if (gameObject)
	{
		destroy(*gameObject);
	}
}

void GLOWE::GameObject::destroy(GameObject& gameObject)
{
	gameObject.owningScene->destroyGameObject(gameObject);
}

void GLOWE::GameObject::destroyImmediate(GameObject* gameObject)
{
	gameObject->owningScene->destroyGameObjectImmediate(gameObject);
}

void GLOWE::GameObject::destroyImmediate(GameObject& gameObject)
{
	gameObject.owningScene->destroyGameObjectImmediate(gameObject);
}

GLOWE::GameObjectRegistry::ActivateHandle GLOWE::GameObjectRegistry::activateGameObject(GameObject* gameObj)
{
	toActivate.push_back(gameObj);
	return std::prev(toActivate.end());
}

GLOWE::GameObjectRegistry::ActivateHandle GLOWE::GameObjectRegistry::deactivateGameObject(GameObject* gameObj)
{
	toDeactivate.push_back(gameObj);
	return std::prev(toDeactivate.end());
}

void GLOWE::GameObjectRegistry::abortActivation(const ActivateHandle& handle)
{
	toActivate.erase(handle);
}

void GLOWE::GameObjectRegistry::abortDeactivation(const ActivateHandle& handle)
{
	toDeactivate.erase(handle);
}

void GLOWE::GameObjectRegistry::updateActivation()
{
	for (const auto& x : toActivate)
	{
		x->updateActivate();
	}

	for (const auto& x : toDeactivate)
	{
		x->updateDeactivate();
	}

	toActivate.clear();
	toDeactivate.clear();
}

GLOWE::GameObjectRegistry::GameObjectHandle GLOWE::GameObjectRegistry::registerGameObject(GameObject * gameObj)
{
	if (!gameObj)
	{
		throw Exception("Passed null GameObject");
	}

	return gameObjectsByNames.emplace(gameObj->getName(), gameObj);
}

void GLOWE::GameObjectRegistry::deregisterGameObject(const GameObjectHandle& handle)
{
	gameObjectsByNames.erase(handle);
}

GLOWE::GameObject * GLOWE::GameObjectRegistry::getGameObject(const String & gameObjectName) const
{
	auto it = gameObjectsByNames.find(gameObjectName);
	if (it != gameObjectsByNames.end())
	{
		return it->second;
	}

	return nullptr;
}

bool GLOWE::GameObjectRegistry::isExisting(const String & gameObjectName) const
{
	return gameObjectsByNames.count(gameObjectName) > 0;
}

void GLOWE::GameObjectRegistry::invokeOnAll(const std::function<void(const String&, GameObject&)>& func)
{
	for (auto& x : gameObjectsByNames)
	{
		func(x.first, *(x.second));
	}
}

void GLOWE::GameObjectRegistry::invokeOnAll(const std::function<void(const String&, const GameObject&)>& func) const
{
	for (const auto& x : gameObjectsByNames)
	{
		func(x.first, *(x.second));
	}
}
