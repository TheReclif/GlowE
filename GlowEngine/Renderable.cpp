#include "Renderable.h"

void GLOWE::Renderable::setActive(const bool arg)
{
	isActive = arg;
}

bool GLOWE::Renderable::checkIsActive() const
{
	return isActive;
}

void GLOWE::Renderable::init()
{
	handleInRenderingEngine = getInstance<RenderingEngine>().addRenderable(this);
}

void GLOWE::Renderable::cleanUp()
{
	getInstance<RenderingEngine>().removeRenderable(handleInRenderingEngine);
}

GLOWE::Hash GLOWE::Renderable::getVariantHash() const
{
	return variantHash;
}

bool GLOWE::Renderable::isExcludedFromRendering(const RenderingEngine::RenderPass& renderPass) const
{
	return isExcludedFromRenderingCustom(renderPass) || (!isActive);
}

void GLOWE::Renderable::recalculateVariantHash()
{
	variantHash = recalculateVariantHashCustom();
}
