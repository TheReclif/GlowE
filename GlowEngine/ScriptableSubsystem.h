#pragma once
#ifndef GLOWE_ENGINE_SCRIPTABLESUBSYSTEM_INCLUDED
#define GLOWE_ENGINE_SCRIPTABLESUBSYSTEM_INCLUDED

#include "glowengine_export.h"

namespace GLOWE
{
	/// @brief Base class for scriptable subsystems. Scriptable subsystem is updated by the engine in the loop. It can be specified when to update the subsystem.
	class GLOWENGINE_EXPORT ScriptableSubsystem
	{
	public:
		virtual ~ScriptableSubsystem() = default;

		/// @brief Update function. Called synchronously every frame in specified moment e.g. before or after rendering.
		virtual void update() = 0;
	};
}
#endif
