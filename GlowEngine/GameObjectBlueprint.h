#pragma once
#ifndef GLOWE_ENGINE_GAMEOBJECTBLUEPRINT_INCLUDED
#define GLOWE_ENGINE_GAMEOBJECTBLUEPRINT_INCLUDED

#include "GameObject.h"
#include "Component.h"

#include "Resource.h"

namespace GLOWE
{
	/// @brief A class that allows to save and leater recreate any game object.
	class GLOWENGINE_EXPORT GameObjectBlueprint
		: public BinarySerializable, public NoCopy
	{
	private:
		String blueprintName;
		Multimap<Hash, Component::Variables> componentBlueprints;
		Vector<ResourceHandle<void>> resourcesToCache;
	public:
		GameObjectBlueprint() = default;
		/// @brief Constructs a blueprint from serialized components' values.
		/// @param name Blueprint name.
		/// @param components Components' serialized variables.
		/// @param resourcesToKeep Resources needed by the game object.
		GameObjectBlueprint(const String& name, const Multimap<Hash, Component::Variables>& components, const Vector<ResourceHandle<void>>& resourcesToKeep);

		/// @brief Creates a game object out of the saved state.
		/// @param name New game object's name. If empty, the blueprint name will be chosen.
		/// @return Reference to the created game object.
		GameObject& createGameObject(const String& name = String()) const;

		virtual void serialize(LoadSaveHandle& handle) const override;
		virtual void deserialize(LoadSaveHandle& handle) override;
	};

	template<>
	GLOWENGINE_EXPORT ResourceHandle<GameObjectBlueprint> ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToRetrieve);
	template<>
	GLOWENGINE_EXPORT ResourceHandle<GameObjectBlueprint> ResourceMgr::getDefaultResource();
}

#endif
