#include "Material.h"
#include "ShaderCacheImplementation.h"

#include "../GlowGraphics/Uniform/GraphicsFactory.h"

void GLOWE::Material::clear()
{
	cbufferNames.clear();
	textureNames.clear();
	uavNames.clear();
	cbuffers.clear();
	textures.clear();
	uavs.clear();
	passes.clear();
	effect = ResourceHandle<Effect>();
	associatedTechnique = nullptr;
}

bool GLOWE::Material::checkIsValid() const
{
	return associatedTechnique;
}

int GLOWE::Material::getCBufferPosition(const Hash& hash) const
{
	auto it = cbufferNames.find(hash);
	if (it != cbufferNames.end())
	{
		return it->second;
	}

	return -1;
}

int GLOWE::Material::getCBufferFieldPosition(const unsigned int cbuffPos, const Hash& fieldName) const
{
	if (cbuffPos < cbuffers.size())
	{
		return cbuffers[cbuffPos].cbufferInEffect->layout.getElemPos(fieldName);
	}

	return -1;
}

int GLOWE::Material::getTexturePosition(const Hash& hash) const
{
	auto it = textureNames.find(hash);
	if (it != textureNames.end())
	{
		return it->second;
	}

	return -1;
}

int GLOWE::Material::getUAVPosition(const Hash& hash) const
{
	auto it = uavNames.find(hash);
	if (it != uavNames.end())
	{
		return it->second;
	}

	return -1;
}

// These 2 methods must be exactly the same (there is an implicit conversion to a proper union type).
void GLOWE::Material::setTexture(const unsigned int pos, const ResourceHandle<TextureImpl>& newTexture)
{
	GlowAssert(pos < textures.size());
	textures[pos] = newTexture;
}

void GLOWE::Material::setTexture(const unsigned int pos, TextureImpl* const newTexture)
{
	GlowAssert(pos < textures.size());
	textures[pos] = newTexture;
}

bool GLOWE::Material::checkIsTexturePresent(const unsigned int pos) const
{
	if (pos < textures.size())
	{
		if (textures[pos].isOwned)
		{
			return textures[pos].handle;
		}
		return textures[pos].texPurePtr;
	}

	return false;
}

void GLOWE::Material::setUAV(const unsigned int pos, UnorderedAccessViewImpl* const newUav)
{
	GlowAssert(pos < uavs.size());
	uavs[pos] = newUav;
}

bool GLOWE::Material::checkIsUAVPresent(const unsigned int pos) const
{
	if (pos < uavs.size())
	{
		return uavs[pos];
	}

	return false;
}

void GLOWE::Material::applyEnvironment(const MaterialEnvironment& env)
{
	for (unsigned int x = 0; x < cbuffers.size(); ++x)
	{
		for (auto& field : cbuffers[x].engineSuppliedVersions)
		{
			auto findResult = env.variables.find(field);
			if (findResult != env.variables.end())
			{
				modifyCBuffer(
					x,
					cbuffers[x].cbufferInEffect->layout.getElemPos(field),
					findResult->second.first.data(),
					findResult->second.first.size());
			}
			else
			{
				//WarningThrow(false, "Unable to find a variable in MaterialEnvironment.");
			}
		}
	}
}

void GLOWE::Material::apply(const unsigned int passId, const Hash& variantHash, BasicRendererImpl& renderer)
{
	// Probably only one count is needed (assumption - the requested pass exists (or not) always with a brother in Effect).
	if (!associatedTechnique || associatedTechnique->passes.count(passId) == 0 || passes.count(passId) == 0)
	{
		return;
	}

	associatedTechnique->passes.at(passId).apply(variantHash, renderer); // TODO: sortowanie po tagach. (Nie wiem o co mi chodzi�o. Chyba zrobione gdzie�Eindziej).

	const Pass& pass = passes.at(passId);
	const Effect::Pass& effectPass = associatedTechnique->passes.at(passId);

	// Maybe an array with not updated cbuffers instead of all buffers? Better performance of this loop, but slightelly bigger memory usage (additional vector for indices of not updated cbuffers).
	for (auto& x : cbuffers)
	{
		if (x.isDirty)
		{
			x.cbufferBuffer->update(renderer, x.dataToUpdate.getData(), x.dataToUpdate.getBufferSize());
			x.isDirty = false;
		}
	}

	auto processShader = [&](const Pass::ShaderDesc & info, const Effect::Pass::ShaderDesc & effectInfo, const ShaderType shaderType)
	{
		if (info.isPresent)
		{
			const ShaderResourceViewImpl* srvs[16];
			unsigned int usedSRVs = info.usedTextures.size();

			const BufferImpl* buffs[16];
			unsigned int usedBuffs = info.usedCBuffers.size();

			const SamplerImpl* samps[16];
			unsigned int usedSamps = effectInfo.usedSamplers.size();

			const UnorderedAccessViewImpl* uavsArray[16];
			unsigned int usedUAVs = effectInfo.usedUAVs.size();

			if (usedBuffs > 0)
			{
				for (unsigned int x = 0; x < usedBuffs; ++x)
				{
					buffs[x] = cbuffers[info.usedCBuffers[x]].cbufferBuffer.get();
				}

				renderer.setConstantBuffers(buffs, 0, usedBuffs, shaderType);
			}

			if (usedSamps > 0)
			{
				for (unsigned int x = 0; x < usedSamps; ++x)
				{
					samps[x] = effect->samplers[effectInfo.usedSamplers[x]].get();
				}

				renderer.setSamplers(samps, 0, usedSamps, shaderType);
			}

			if (usedSRVs > 0)
			{
				for (unsigned int x = 0; x < usedSRVs; ++x)
				{
					TextureImpl* const temp = textures[info.usedTextures[x]].getTexture();
					const ShaderResourceViewImpl* tempSrv = srvs[x] = temp ? &temp->getSRV() : nullptr;
					if (tempSrv && temp->checkShouldGenerateMipmaps())
					{
						renderer.customFunction([temp](BasicRendererImpl& renderer)
						{
							if (temp->checkShouldGenerateMipmaps())
							{
								temp->abortMipmapGeneration();
								renderer.generateMipmaps(&temp->getSRV());
							}
						});
					}
				}

				renderer.setSRV(srvs, 0, usedSRVs, shaderType);
			}

			// No else statement (if no uavs are used then what's the point of explicitly unbinding them)?
			if (usedUAVs > 0)
			{
				for (unsigned int x = 0; x < usedUAVs; ++x)
				{
					uavsArray[x] = uavs[effectInfo.usedUAVs[x]];
				}
				
				if (shaderType == ShaderType::PixelShader)
				{
					renderer.setUAVs(uavsArray, usedUAVs, BasicRendererImpl::SetUAVsForPixelShader);
				}
				else
				{
					renderer.setUAVs(uavsArray, usedUAVs, 0);
				}
			}
		}
	};

	processShader(pass.computeShader, effectPass.computeShader, ShaderType::ComputeShader);
	processShader(pass.vertexShader, effectPass.vertexShader, ShaderType::VertexShader);
	processShader(pass.pixelShader, effectPass.pixelShader, ShaderType::PixelShader);
	processShader(pass.geometryShader, effectPass.geometryShader, ShaderType::GeometryShader);
}

const GLOWE::ShaderCollectionImpl& GLOWE::Material::getShaderCollection(const unsigned int passId, const Hash& variantHash)
{
	GlowAssert(associatedTechnique != nullptr);
	const Effect::Pass& pass = associatedTechnique->passes.at(passId);
	DefaultLockGuard lock(collMutex);
	auto& result = pass.parent->shaderColls[pass.variants.at(variantHash)];

	if (!result.second)
	{
		result.second = getInstance<ResourceMgr>().load<ShaderCollectionImpl>(getInstance<ShaderCacheImpl>().getCollectionFileHash(result.first), result.first);
	}

	return *result.second;
}

const GLOWE::Vector<GLOWE::Effect::Technique::Tag>& GLOWE::Material::getTags() const
{
	GlowAssert(associatedTechnique != nullptr);
	return associatedTechnique->tags;
}

const GLOWE::Map<unsigned int, GLOWE::Material::Pass>& GLOWE::Material::getPasses() const
{
	return passes;
}

void GLOWE::Material::modifyCBuffer(const unsigned int cbuffPos, const unsigned int elemPos, const void* data, const unsigned int dataSize)
{
	GlowAssert(cbuffPos < cbuffers.size());
	CBuffer& cbuff = cbuffers[cbuffPos];

	cbuff.isDirty = true;
	cbuff.dataToUpdate.modifyElement(0, elemPos, data, dataSize);

	if (!cbuff.isOwner)
	{
		// Create a new buffer.
		BufferDesc buffDesc;
		buffDesc.binds = BufferDesc::Binding::ConstantBuffer;
		buffDesc.cpuAccess = CPUAccess::Write;
		buffDesc.usage = Usage::Default;

		// Prbly needless.
		//buffDesc.layout = cbuff.dataToUpdate.getLayout();

		cbuff.cbufferBuffer = createGraphicsObject<BufferImpl>();
		cbuff.cbufferBuffer->create(buffDesc, cbuff.dataToUpdate.getBufferSize(), std::cref(*getInstance<ResourceMgr>().getDevice()), cbuff.dataToUpdate.getData());
		cbuff.isDirty = false;
		cbuff.isOwner = true;
	}
}

void GLOWE::Material::createFromEffect(Material& result, const GraphicsDeviceImpl& device, const ResourceHandle<Effect>& effect, const Hash& techniqueName, const Hash& techniqueGroupName)
{
	try
	{
		Map<unsigned int, unsigned int> textureMappings, cbufferMappings, uavMappings;

		Effect::Technique& technique = effect->getTechnique(techniqueName, techniqueGroupName);
		result.effect = effect;
		result.associatedTechnique = &technique;

		auto processShaderDesc = [&](const Effect::Pass::ShaderDesc & desc, Pass::ShaderDesc & descOut)
		{
			descOut.isPresent = desc.isPresent;

			if (desc.isPresent)
			{
				for (const auto& x : desc.usedCBuffers)
				{
					if (cbufferMappings.count(x) == 0)
					{
						unsigned int temp = cbufferMappings.size();

						result.cbufferNames.emplace(effect->cbuffers[x].first, temp);

						CBuffer tempCBuff;
						tempCBuff.isOwner = true;
						tempCBuff.isDirty = false;
						tempCBuff.dataToUpdate.setLayout(effect->cbuffers[x].second.layout.toStructureLayout());
						tempCBuff.dataToUpdate.resize(1);
						tempCBuff.cbufferInEffect = &effect->cbuffers[x].second;
						tempCBuff.cbufferBuffer = createGraphicsObject<BufferImpl>();

						for (const auto& y : effect->cbuffers[x].second.variables)
						{
							if (y.second.first == GSILShader::Property::Type::EngineSupplied)
							{
								tempCBuff.engineSuppliedVersions.emplace(y.first);
							}
						}

						BufferDesc buffDesc;
						buffDesc.binds = BufferDesc::Binding::ConstantBuffer;
						buffDesc.cpuAccess = CPUAccess::Write;
						buffDesc.usage = Usage::Dynamic;
						//buffDesc.layout = tempCBuff.dataToUpdate.getLayout();

						tempCBuff.cbufferBuffer->create(buffDesc, tempCBuff.dataToUpdate.getBufferSize(), device);

						result.cbuffers.push_back(std::move(tempCBuff));
						cbufferMappings.emplace(x, temp);
					}

					descOut.usedCBuffers.push_back(cbufferMappings.at(x));
				}
				
				for (const auto& x : desc.usedUAVs)
				{
					if (uavMappings.count(x) == 0)
					{
						unsigned int temp = uavMappings.size();

						result.uavNames.emplace(effect->uavs[x].first, temp);
						result.uavs.emplace_back();
						uavMappings.emplace(x, temp);
					}

					descOut.usedUAVs.push_back(uavMappings.at(x));
				}

				for (const auto& x : desc.usedTextures)
				{
					if (textureMappings.count(x) == 0)
					{
						unsigned int temp = textureMappings.size();

						result.textureNames.emplace(effect->textures[x].first, temp);
						result.textures.emplace_back(); // TODO: Replace with default texture (from ResourceMgr).
						textureMappings.emplace(x, temp);
					}

					descOut.usedTextures.push_back(textureMappings.at(x));
				}
			}
		};

		// Fill the cbuffers' and textures' stuff. And passes too ;).
		for (const auto& x : result.associatedTechnique->passes)
		{
			Pass pass;

			processShaderDesc(x.second.computeShader, pass.computeShader);
			processShaderDesc(x.second.vertexShader, pass.vertexShader);
			processShaderDesc(x.second.pixelShader, pass.pixelShader);
			processShaderDesc(x.second.geometryShader, pass.geometryShader);

			result.passes.emplace(x.first, std::move(pass));
		}
	}
	catch (const std::exception& e)
	{
		WarningThrow(false, "std::exception in createFromEffect: " + String(e.what()));
	}
	catch (const Exception& e)
	{
		WarningThrow(false, "Exception in createFromEffect: " + String(e.what()));
	}
	catch (...)
	{
		WarningThrow(false, "Unknown error in createFromEffect");
	}
}

template<>
GLOWE::ResourceHandle<GLOWE::Material> GLOWE::ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToRetrieve)
{
	ResourceHandle<Material> result;
	MemoryLoadSaveHandle handle(memory, size);

	UInt32 matCount{}, fieldCount{};
	handle >> matCount;

	for (unsigned int x = 0; x < matCount; ++x)
	{
		Hash matHash;
		handle >> matHash;
		if (matHash != subresourceToRetrieve)
		{
			handle.skipBytes(2 * trueSizeof<Hash>());
			UInt32 tempUInt32{};
			handle >> fieldCount;

			for (unsigned int y = 0; y < fieldCount; ++y)
			{
				handle.skipBytes(2 * trueSizeof<Hash>());
				handle >> tempUInt32;
				handle.skipBytes(tempUInt32);
			}
		}
		else
		{
			UInt32 tempUInt32{};
			Hash groupHash, techHash, cbuffHash, fieldHash;
			handle >> groupHash >> techHash;
			result = makeSharedPtr<Material>(); // TODO: Check
			Material::createFromEffect(*result, *graphicsDevice, loadedDependiencies.at(u8"Effect").treatAs<Effect>(), techHash, groupHash);
			handle >> fieldCount;
			for (const auto& dep : loadedDependiencies)
			{
				if (dep.first != Hash(u8"Effect"))
				{
					const int texturePos = result->getTexturePosition(dep.first);
					if (texturePos >= 0)
					{
						// Valid texture.
						result->setTexture(texturePos, dep.second.treatAs<TextureImpl>());
					}
				}
			}

			String tempBuffer;
			for (unsigned int y = 0; y < fieldCount; ++y)
			{
				handle >> cbuffHash >> fieldHash >> tempUInt32;
				tempBuffer.resize(tempUInt32);
				handle.read(tempBuffer.getCharArray(), tempUInt32);

				const int cbuffPos = result->getCBufferPosition(cbuffHash);
				if (cbuffPos >= 0)
				{
					const int fieldPos = result->getCBufferFieldPosition(cbuffPos, fieldHash);
					
					if (fieldPos >= 0)
					{
						result->modifyCBuffer(cbuffPos, fieldPos, tempBuffer.getCharArray(), tempUInt32);
					}
				}
			}

			break;
		}
	}

	return result;
}

template<>
GLOWE::ResourceHandle<GLOWE::Material> GLOWE::ResourceMgr::getDefaultResource()
{
	ResourceHandle<Material> result = makeSharedPtr<Material>();
	result->effect = getDefaultResource<Effect>();
	return result;
}

GLOWE::Material::OptionalNotOwnedTexture::OptionalNotOwnedTexture()
	: OptionalNotOwnedTexture(nullptr)
{
}

GLOWE::Material::OptionalNotOwnedTexture::OptionalNotOwnedTexture(OptionalNotOwnedTexture&& arg)
	: isOwned(arg.isOwned)
{
	if (isOwned)
	{
		new (&handle) ResourceHandle<TextureImpl>(std::move(arg.handle));
		arg.isOwned = false;
	}
	else
	{
		texPurePtr = exchange(arg.texPurePtr, nullptr);
	}
}

GLOWE::Material::OptionalNotOwnedTexture& GLOWE::Material::OptionalNotOwnedTexture::operator=(OptionalNotOwnedTexture&& arg)
{
	if (isOwned)
	{
		handle.~ResourceHandle();
	}

	isOwned = arg.isOwned;
	if (arg.isOwned)
	{
		new (&handle) ResourceHandle<TextureImpl>(std::move(arg.handle));
		arg.isOwned = false;
	}
	else
	{
		texPurePtr = exchange(arg.texPurePtr, nullptr);
	}

	return *this;
}

GLOWE::Material::OptionalNotOwnedTexture::OptionalNotOwnedTexture(const ResourceHandle<TextureImpl>& arg)
	: isOwned(true), handle(arg)
{
}

GLOWE::Material::OptionalNotOwnedTexture::OptionalNotOwnedTexture(TextureImpl* const arg)
	: isOwned(false), texPurePtr(arg)
{
}

GLOWE::Material::OptionalNotOwnedTexture::~OptionalNotOwnedTexture()
{
	if (isOwned)
	{
		handle.~ResourceHandle();
	}
}

GLOWE::TextureImpl* GLOWE::Material::OptionalNotOwnedTexture::getTexture() const
{
	if (isOwned)
	{
		return handle.getResource().get();
	}

	return texPurePtr;
}
