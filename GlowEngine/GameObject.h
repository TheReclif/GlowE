#pragma once
#ifndef GLOWE_GAMEOBJECT_INCLUDED
#define GLOWE_GAMEOBJECT_INCLUDED

#include "Component.h"
#include "Scene.h"

namespace GLOWE
{
	class GameObjectRegistry;

	namespace Components
	{
		class Component;
		class Transform;
	}

	// Like with the Component class, we have to make some assumptions.
	// 1. A parent will always know about all of its children.
	// 2. When a parent is being destroyed, it must notify its own parent.
	// 3. A GameObject will always have a parent.
	// 4. If a GameObject is on the top of the hierarchy, i.e. it doesn't have a parent, then its parent is a special object called "something".
	class GLOWENGINE_EXPORT GameObject
		: public Lockable, public Object, public Serializable
	{
	public:
		enum class ScheduledActDeact : unsigned char
		{
			None,
			Activation,
			Deactivation
		};
	private:
		using ActivateHandle = List<GameObject*>::const_iterator;
		using GameRegistryHandle = Multimap<String, GameObject*>::const_iterator;
	protected:
		struct CanCreateGameObjectTag
		{};
	protected:
		ScheduledActDeact scheduledActDeact;
		bool isActive, rememberedParentStatus;

		String name;

		ActivateHandle activateHandle;
		Scene* owningScene;
		Scene::GameObjectHandle inSceneHandle;
		Multimap<String, UniquePtr<Component>> components;
		Map<String, GameObject*> children;
		GameObject* parent;
		Components::Transform* transform;
		// Layer also indicates if the object is currently being destroyed (when it's equal to max of unsigned int).
		unsigned int layer;
	private:
		// It must be here to force the "name" variable to be initialized before.
		GameRegistryHandle gameRegistryHandle;
	private:
		void attachChild(GameObject* newChild);
		void attachChild(const String& childName);

		void notifyAttachChild(GameObject* newChild);
		void notifyAttachChild(const String& childName);

		void detachChild(GameObject* childToDetach);
		void detachChild(const String& childName);

		void notifyDetachChild(GameObject* childToDetach);
		void notifyDetachChild(const String& childName);

		void scheduleActivate(GameObjectRegistry& registry);
		void scheduleDeactivate(GameObjectRegistry& registry, const bool wasActive);

		void updateActivate();
		void updateDeactivate();

		void updateRememberedParentStatus();
	public:
		GameObject() = delete;
		GameObject(const String& str, Scene& newOwningScene, const CanCreateGameObjectTag&);
		
		virtual ~GameObject();

		GlowSerialize(GlowBases(), GlowField(isActive), GlowField(layer), GlowField(parent), GlowField(name));

		virtual void orphan();
		virtual void setParent(GameObject* newParent);

		Component* getComponent(const String& type) const;
		Vector<Component*> getComponents(const String& type) const;

		const Multimap<String, UniquePtr<Component>>& getAllComponents() const;

		template<class T>
		T* getComponent() const
		{
			return static_cast<T*>(getComponent(Components::ComponentReflection<T>::name()));
		}
		template<class T>
		Vector<Component*> getComponents() const
		{
			return getComponents(Components::ComponentReflection<T>::name());
		}

		template<class T>
		T* getVirtualComponent() const
		{
			for (const auto& comp : components)
			{
				T* result = dynamic_cast<T*>(comp.second.get());
				if (result)
				{
					return result;
				}
			}

			return nullptr;
		}
		template<class T>
		Vector<T*> getVirtualComponents() const
		{
			Vector<T*> result;
			result.reserve(components.size() / 3); // Aprox.
			for (const auto& comp : components)
			{
				T* temp = dynamic_cast<T*>(comp.second.get());
				if (temp)
				{
					result.push_back(temp);
				}
			}
			return result;
		}

		GameObject* getParent() const;

		GameObject* getChild(const String& childName) const;

		Components::Transform* getTransform();
		const Components::Transform* getTransform() const;

		Component& addComponent(const String& type);
		Component& addComponent(UniquePtr<Component>&& arg);

		template<class ComponentType, class ...Args>
		inline ComponentType& addComponent(Args&& ...args)
		{
			static_assert(std::is_base_of<Component, ComponentType>::value, "Type must derive from Component");
			return static_cast<ComponentType&>(addComponent(makeUniquePtr<ComponentType>(this, std::forward<Args>(args)...)));
		}

		void deleteComponent(Component* arg);

		// These calls are made immediately and synchronously.
		void sendEvent(const GameEvent& event, const Component* sender); // Send just to me.
		void sendEventToChildren(const GameEvent& event, const Component* sender); // Send to me AND to my children.
		void sendEventToChildrenRecursive(const GameEvent& event, const Component* sender); // Send to basically everyone.

		/// @brief Returns the scene that owns this object.
		/// @return Object owning scene
		Scene* getOwningScene() const;
		/// @brief Sets the object's owning scene.
		/// @note Throws an exception if the parent belongs to an another scene.
		/// @param scene New owning scene to set
		void setOwningScene(Scene* scene);

		/// @brief Returns object's in the hierarchy (e.g. for a object called "B" that has an orphaned parent called "A" the method will return "A/B").
		/// @return GameObject's path
		String getPath() const;

		/// @brief Checks whether the given component is present in this GameObject, i.e. 
		/// @param component 
		/// @return 
		bool checkIfContains(const Component* component) const;

		bool operator==(const String& arg) const;
		bool operator==(const GameObject& arg) const;

		const String& getName() const;
		ScheduledActDeact getScheduledActDeactInfo() const;

		// Ensure that the object (and its children) is up and running (calls activation/deactivation methods from its components).
		// May be performance unfriendly.
		void ensureActDeact();

		void setName(const String& arg);

		void activate(const bool immediately = false);
		void deactivate(const bool immediately = false);

		void setActive(const bool arg);

		bool checkIsActive() const;
		bool checkIsActiveLocally() const;

		void setLayer(const unsigned int newLayer);
		void setLayer(const String& newLayer);
		unsigned int getLayer() const;

		static GameObject* findGameObject(const String& name);

		static GameObject& createGameObject(const String& name, GameObject* parent = nullptr, const bool activateImmediately = true);
		static GameObject& createGameObject(const String& name, Scene& scene, GameObject* parent = nullptr, const bool activateImmediately = true);

		static void destroy(GameObject* gameObject);
		static void destroy(GameObject& gameObject);
		static void destroyImmediate(GameObject* gameObject);
		static void destroyImmediate(GameObject& gameObject);

		friend class Components::Transform;
		friend class Scene;
		friend class GameObjectRegistry;
	};

	// TODO: Modify (GameObject names).
	class GLOWENGINE_EXPORT GameObjectRegistry
		: public Subsystem
	{
	private:
		Multimap<String, GameObject*> gameObjectsByNames;
		
		List<GameObject*> toActivate, toDeactivate;
		using ActivateHandle = decltype(toActivate)::const_iterator;
	public:
		using GameObjectHandle = decltype(gameObjectsByNames)::const_iterator;
	public:
		ActivateHandle activateGameObject(GameObject* gameObj);
		ActivateHandle deactivateGameObject(GameObject* gameObj);
		void abortActivation(const ActivateHandle& handle);
		void abortDeactivation(const ActivateHandle& handle);

		void updateActivation();

		GameObjectHandle registerGameObject(GameObject* gameObj);
		void deregisterGameObject(const GameObjectHandle& handle);

		GameObject* getGameObject(const String& gameObjectName) const;

		bool isExisting(const String& gameObjectName) const;

		void invokeOnAll(const std::function<void(const String&, GameObject&)>& func);
		void invokeOnAll(const std::function<void(const String&, const GameObject&)>& func) const;
	};
}

#endif
