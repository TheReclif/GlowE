#include "LayersMgr.h"
#include "GameObject.h"

GLOWE::LayersMgr::LayersMgr()
	: layers({ std::make_pair(u8"Default", 0) })
{
}

unsigned int GLOWE::LayersMgr::addLayer(const String& layer)
{
	const unsigned int newLayer = layers.size();
	const auto it = layers.find(layer);

	if (it == layers.end())
	{
		layers.emplace(layer, newLayer);
		return newLayer;
	}
	else
	{
		return it->second;
	}
}

bool GLOWE::LayersMgr::isLayerPresent(const String& layer)
{
	return layers.count(layer) == 1;
}

bool GLOWE::LayersMgr::isLayerPresent(const unsigned int layer)
{
	return layer < layers.size();
}

unsigned int GLOWE::LayersMgr::getLayer(const String& layer)
{
	return layers.at(layer);
}
