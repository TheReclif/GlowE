#pragma once
#ifndef GLOWE_GAMEEVENT_INCLUDED
#define GLOWE_GAMEEVENT_INCLUDED

#include "glowengine_export.h"

#include "../GlowWindow/Uniform/Event.h"

namespace GLOWE
{
	class GUIElement;

	namespace Components
	{
		class Transform;
	}

	struct GLOWENGINE_EXPORT GameEvent
	{
		enum class Type
		{
			Unknown = 0, // Invalid event type
			System, // System event - only valid option is "Event systemEvent" from "info" union.
			TransformUpdated, // Sent when Transform isDirty is true when getMatrix is called.
			GUI // Same as system event, but resent for all GUIElements by GUITop.
		};

		struct GUIEvent
		{
			enum class Type
			{
				Unknown = 0,
				MouseEntered,
				MouseLeft,
				MouseButtonDownInside,
				MouseButtonUpInside,
				MouseMovedInside
			};

			union
			{
				Event::MouseButtonInfo mouseButtonInfo;
				Event::MouseMoveInfo mouseMoveInfo;
			};
			Type type;
			GUIElement* whichElement;
		};

		union Info
		{
			Event systemEvent;
			Components::Transform* transform;
			GUIEvent guiEvent;

			constexpr Info() noexcept : transform(nullptr) {};
			constexpr explicit Info(const Event& event) noexcept : systemEvent(event) {};
			constexpr explicit Info(Components::Transform* const ptr) noexcept : transform(ptr) {};
			constexpr explicit Info(const GUIEvent& event) noexcept : guiEvent(event) {};
		};

		Type type = Type::Unknown;
		Info info;
	public:
		GameEvent() noexcept = default;
		GameEvent(const Type newType, const Info& newInfo) noexcept;
		GameEvent(const Event& event) noexcept;
		GameEvent(Components::Transform* const ptr) noexcept;
		
		bool operator==(const Type other) const noexcept
		{
			return other == type;
		}

		bool operator!=(const Type other) const noexcept
		{
			return other != type;
		}

		operator Type() const noexcept
		{
			return type;
		}
	};
}

#endif
