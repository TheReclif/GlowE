#pragma once
#ifndef GLOWE_ENGINE_RENDERABLE_INCLUDED
#define GLOWE_ENGINE_RENDERABLE_INCLUDED

#include "RenderingEngine.h"

#include "../GlowMath/Frustum.h"

namespace GLOWE
{
	class GLOWENGINE_EXPORT Renderable
	{
	private:
		bool isActive = false;
		Hash variantHash;
	private:
		virtual bool isExcludedFromRenderingCustom(const RenderingEngine::RenderPass&) const { return false; };
		virtual Hash recalculateVariantHashCustom() = 0;
	protected:
		RenderingEngine::RenderableHandle handleInRenderingEngine;
	protected:
		void init(); // Must be called in ctor.
		void cleanUp(); // Must be called in dtor.
	public:
		Renderable() = default;
		virtual ~Renderable() = default;

		void setActive(const bool arg);
		bool checkIsActive() const;

		Hash getVariantHash() const;

		bool isExcludedFromRendering(const RenderingEngine::RenderPass& renderPass) const;
		virtual void render(BasicRendererImpl& renderer, MaterialEnvironment& matEnv) = 0;

		void recalculateVariantHash();

		virtual Material* getMaterialPtr() const = 0; // Used to sort renderables.
	};
}

#endif
