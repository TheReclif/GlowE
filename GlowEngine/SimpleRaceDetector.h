#pragma once
#ifndef GLOWE_ENGINE_SIMPLERACEDETECTOR_INCLUDED
#define GLOWE_ENGINE_SIMPLERACEDETECTOR_INCLUDED

#include "../GlowSystem/Error.h"
#include "../GlowSystem/ThreadClass.h"

namespace GLOWE
{
	template<class T>
	class SimpleRaceDetector
	{
	private:
		static std::atomic<std::size_t> owner;
	public:
		SimpleRaceDetector()
		{
			std::size_t expected = owner;
			const auto hash = std::hash<std::thread::id>{}(GLOWE::Thread::getCurrentThreadID());
			if ((expected != 0 && expected != hash) || !owner.compare_exchange_strong(expected, hash))
			{
#ifdef _MSC_VER
				DebugBreak();
#else
#error No debug break available
#endif
			}
		}
		~SimpleRaceDetector()
		{
			owner = 0;
		}
	};

	template<class T>
	std::atomic<std::size_t> SimpleRaceDetector<T>::owner = 0;
}

#endif
