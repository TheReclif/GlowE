#include "ResourceLoaders.h"
#include "EngineGraphicsFactory.h"

#include "../GlowGraphics/Uniform/TextureLoader.h"
#include "../GlowGraphics/Uniform/GraphicsFactory.h"

#include <FreeImage.h>

template<>
GLOWE::ResourceHandle<GLOWE::ShaderCollectionImpl> GLOWE::ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToRetrieve)
{
	ResourceHandle<String> result = makeSharedPtr<String>((const char*)memory, size);

	return result.treatAs<ShaderCollectionImpl>();
}

template<>
GLOWE::ResourceHandle<GLOWE::ShaderCollectionImpl> GLOWE::ResourceMgr::getDefaultResource()
{
	ResourceHandle<ShaderCollectionImpl> result = uniqueToShared(createGraphicsObject<ShaderCollectionImpl>());
	return result;
}

template<>
GLOWE::ResourceHandle<GLOWE::ShaderCollectionImpl> GLOWE::ResourceMgr::preLoad(const Hash&, const Hash& subresourceHash, const Map<Hash, ResourceHandle<void>>&)
{
	ResourceHandle<ShaderCollectionImpl> result;
	ShaderCacheImpl& cache = getInstance<ShaderCacheImpl>();
	if (cache.checkIsLoaded())
	{
		result = uniqueToShared(createGraphicsObject<ShaderCollectionImpl>());
		cache.loadCollection(*graphicsDevice, subresourceHash, *result);
	}

	return result;
}

template<>
GLOWE::ResourceHandle<GLOWE::TextureImpl> GLOWE::ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash&)
{
	TextureLoader& texLoader = getInstance<TextureLoader>();
	ResourceHandle<TextureImpl> result;
	MemoryLoadSaveHandle handle(memory, size);

	TextureDesc desc;
	UInt64 tempSize{};
	handle >> desc >> tempSize;
	desc.type = TextureDesc::Type::Texture2D;

	result = uniqueToShared(texLoader.loadTextureFromMemory(handle.getReadPtr(), tempSize, desc, *graphicsDevice));

	return result;
}

template<>
GLOWE::ResourceHandle<GLOWE::TextureImpl> GLOWE::ResourceMgr::getDefaultResource()
{
	SharedPtr<TextureImpl> result(createGraphicsObject<TextureImpl>());
	TextureDesc desc;
	desc.width = desc.height = 1;
	desc.arraySize = 1;
	desc.cpuAccess = CPUAccess::None;
	desc.binding = TextureDesc::Binding::Default;
	desc.generateMipmaps = false;
	desc.mipmapsCount = 1;
	desc.type = TextureDesc::Type::Texture2D;
	desc.usage = Usage::Immutable;
	desc.format = Format(Format::Type::Float, 4);
	Color color = Color::from255RGBA(31, 250, 244);

	result->create(desc, *graphicsDevice, { &color });

	return result;
}

template<>
GLOWE::ResourceHandle<GLOWE::Geometry> GLOWE::ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToRetrieve)
{
	ResourceHandle<Geometry> result = makeSharedPtr<Geometry>();
	Geometry& geo = *result;
	MemoryLoadSaveHandle handle(memory, size);

	UInt32 objectsCount{};
	handle >> objectsCount;

	for (unsigned int x = 0; x < objectsCount; ++x)
	{
		Hash hash;
		handle >> hash;
		if (hash != subresourceToRetrieve)
		{
			handle.skipBytes(2 * sizeof(UInt) + 2 * sizeof(Float3));
		}
		else
		{
			handle >> geo.offset >> geo.count >> geo.localAABB.center >> geo.localAABB.halfSize;
			break;
		}
	}

	geo.buffers = loadedDependiencies.at(u8"Buffers").treatAs<GeometryBuffersImpl>().getResource();

	return result;
}

template<>
GLOWE::ResourceHandle<GLOWE::Geometry> GLOWE::ResourceMgr::getDefaultResource()
{
	auto geoBuffers = getDefaultResource<GeometryBuffers>();
	ResourceHandle<Geometry> result = makeSharedPtr<Geometry>();

	result->buffers = geoBuffers.treatAs<GeometryBuffersImpl>().getResource();
	result->count = 0;
	result->offset = 0;

	return result;
}

template<>
GLOWE::ResourceHandle<GLOWE::GeometryBuffers> GLOWE::ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToRetrieve)
{
	ResourceHandle<GeometryBuffers> result = makeSharedPtr<GeometryBuffers>();
	GeometryBuffers& geo = *result;
	MemoryLoadSaveHandle handle(memory, size);

	UInt32 objectsCount{}, verticesCount{};
	handle >> objectsCount;
	handle.skipBytes(objectsCount * (trueSizeof<Hash>() + 2 * sizeof(UInt) + 2 * sizeof(Float3)));

	handle >> geo.vertexLayout >> verticesCount;
	bool isNormalPresent = geo.vertexLayout.getVertexLayout().checkIsElemPresent("Normal"), isTexCoordPresent = geo.vertexLayout.getVertexLayout().checkIsElemPresent("TexCoord");
	const unsigned int vertexStructSize = sizeof(Float3) + (isNormalPresent ? 2 * sizeof(Float3) : 0) + (isTexCoordPresent ? sizeof(Float2) : 0);
	const UInt64 vertBufferSize = static_cast<const UInt64>(vertexStructSize) * static_cast<const UInt64>(verticesCount);

	BufferDesc buffDesc;
	buffDesc.binds = BufferDesc::Binding::VertexBuffer;
	buffDesc.cpuAccess = CPUAccess::None;
	buffDesc.layout = geo.vertexLayout.getVertexLayout().toStructureLayout();
	buffDesc.usage = Usage::Immutable;

	geo.vertexBuffer->create(buffDesc, vertBufferSize, *graphicsDevice, handle.getReadPtr());
	handle.skipBytes(vertBufferSize);
	UInt32 indicesCount{};
	handle >> indicesCount;

	buffDesc.binds = BufferDesc::Binding::IndexBuffer;
	buffDesc.layout = StructureLayout();
	buffDesc.layout.addElement(Format(verticesCount <= std::numeric_limits<unsigned short>::max() ? Format::Type::UnsignedShort : Format::Type::UnsignedInt, 1));
	geo.indicesBuffer->create(buffDesc, static_cast<const UInt64>(indicesCount) * static_cast<const UInt64>(buffDesc.layout.getElem(0).getSize()), *graphicsDevice, handle.getReadPtr());

	return result;
}

template<>
GLOWE::ResourceHandle<GLOWE::GeometryBuffers> GLOWE::ResourceMgr::getDefaultResource()
{
	ResourceHandle<GeometryBuffers> result = makeSharedPtr<GeometryBuffers>();

	return result;
}
