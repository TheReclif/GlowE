#pragma once
#ifndef GLOWE_ENGINE_LAYERSMGR_INCLUDED
#define GLOWE_ENGINE_LAYERSMGR_INCLUDED

#include "glowengine_export.h"

#include "../GlowSystem/OneInstance.h"
#include "../GlowSystem/String.h"

namespace GLOWE
{
	class GLOWENGINE_EXPORT LayersMgr
		: public Subsystem
	{
	private:
		UnorderedMap<String, unsigned int> layers;
	public:
		LayersMgr();

		unsigned int addLayer(const String& layer);

		bool isLayerPresent(const String& layer);
		bool isLayerPresent(const unsigned int layer);

		unsigned int getLayer(const String& layer);
	};
}

#endif
