#include "ScriptMgr.h"

#include "../GlowComponents/ScriptBehaviour.h"

static constexpr unsigned int tasksSingleThreadThreshold = 10;

struct InvokeInfoBlock
{
	GLOWE::Atomic<unsigned int> completedWorkItems = 0, currentItem = 0;
	GLOWE::Promise<void> result;
};

GLOWE::ScriptMgr::ScriptHandle GLOWE::ScriptMgr::registerScript(Components::ScriptBehaviour* arg)
{
	// TODO: Do so that it works.
	registeredScripts.emplace(arg);
	if (arg->checkIsAsyncUpdated())
	{
		asyncUpdatedScripts.emplace(arg);
	}
	if (arg->checkIsProcessingEvents())
	{
		eventProcessingScripts.emplace(arg);
	}
	if (arg->checkIsUpdated())
	{
		postSyncUpdatedScripts.emplace(arg);
	}
	return arg;
}

void GLOWE::ScriptMgr::unregisterScript(const ScriptHandle& handle)
{
	asyncUpdatedScripts.erase(handle);
	eventProcessingScripts.erase(handle);
	postSyncUpdatedScripts.erase(handle);
	registeredScripts.erase(handle);
}

void GLOWE::ScriptMgr::invokeOnAll(const std::function<void(Components::ScriptBehaviour&)>& func)
{
	for (const auto& x : registeredScripts)
	{
		if (x->checkIsActive())
		{
			func(*x);
		}
	}
}

void GLOWE::ScriptMgr::invokePostSyncUpdate()
{
	for (const auto& x : postSyncUpdatedScripts)
	{
		x->postUpdateSync();
	}
}

GLOWE::ThreadPool::TaskResult<void> GLOWE::ScriptMgr::invokeOnAllAsync(ThreadPool& threadPool, const std::function<void(Components::ScriptBehaviour&)>& func)
{
	if (registeredScripts.size() <= tasksSingleThreadThreshold)
	{
		Promise<void> promise;
		invokeOnAll(func);
		promise.set_value();
		return promise.get_future();
	}
	SharedPtr<InvokeInfoBlock> invokeInfo = makeSharedPtr<InvokeInfoBlock>();
	const auto taskCount = std::min(static_cast<std::size_t>(threadPool.getThreadsCount()), registeredScripts.size());
	{
		DefaultLockGuard guard(threadPool.getTasksMutex());
		for (std::size_t x = 0; x < taskCount; ++x)
		{
			threadPool.addTaskNoLock([this, invokeInfo, func]()
			{
				const unsigned int scripts = registeredScripts.size();
				unsigned int itemToWorkOn = invokeInfo->currentItem.fetch_add(1, std::memory_order::memory_order_relaxed), offset = itemToWorkOn, temp = 0;
				auto it = registeredScripts.begin();
				while (itemToWorkOn < scripts)
				{
					it = std::next(it, offset);
					auto& script = **it;
					if (script.checkIsActive())
					{
						func(script);
					}
					temp = invokeInfo->currentItem.fetch_add(1, std::memory_order::memory_order_relaxed);
					if ((invokeInfo->completedWorkItems.fetch_add(1, std::memory_order::memory_order_relaxed) + 1) == scripts)
					{
						invokeInfo->result.set_value();
					}
					else
						if (temp < scripts)
						{
							offset = (temp - itemToWorkOn);
						}
					itemToWorkOn = temp;
				}
			});
		}
	}
	threadPool.onLockEnd();

	return invokeInfo->result.get_future();
}

GLOWE::ThreadPool::TaskResult<void> GLOWE::ScriptMgr::invokeAsyncUpdate(ThreadPool& threadPool)
{
	if (asyncUpdatedScripts.size() <= tasksSingleThreadThreshold)
	{
		Promise<void> promise;
		for (const auto& x : asyncUpdatedScripts)
		{
			if (x->checkIsActive())
			{
				ComponentLock lock(*x);
				x->update();
			}
		}
		promise.set_value();
		return promise.get_future();
	}
	SharedPtr<InvokeInfoBlock> invokeInfo = makeSharedPtr<InvokeInfoBlock>();
	const auto taskCount = std::min(static_cast<std::size_t>(threadPool.getThreadsCount()), asyncUpdatedScripts.size());
	{
		DefaultLockGuard guard(threadPool.getTasksMutex());
		for (std::size_t x = 0; x < taskCount; ++x)
		{
			threadPool.addTaskNoLock([this, invokeInfo]()
			{
				const unsigned int scripts = asyncUpdatedScripts.size();
				unsigned int itemToWorkOn = invokeInfo->currentItem.fetch_add(1, std::memory_order::memory_order_relaxed), offset = itemToWorkOn, temp = 0;
				auto it = asyncUpdatedScripts.begin();
				while (itemToWorkOn < scripts)
				{
					it = std::next(it, offset);
					auto& script = **it;
					if (script.checkIsActive())
					{
						ComponentLock lock(script);
						script.update();
					}
					temp = invokeInfo->currentItem.fetch_add(1, std::memory_order::memory_order_relaxed);
					if ((invokeInfo->completedWorkItems.fetch_add(1, std::memory_order::memory_order_relaxed) + 1) == scripts)
					{
						invokeInfo->result.set_value();
					}
					else
					if (temp < scripts)
					{
						offset = (temp - itemToWorkOn);
					}
					itemToWorkOn = temp;
				}
			});
		}
	}
	threadPool.onLockEnd();

	return invokeInfo->result.get_future();
}

// Potentially needs to unlock the mutex after every processed event (it allows the user to avoid deadlocks when two or more components want to lock each other, but it creates a significant overhead of many mutex locks).
GLOWE::ThreadPool::TaskResult<void> GLOWE::ScriptMgr::invokeAsyncEventProcessing(ThreadPool& threadPool, const Deque<Event>& events)
{
	if (eventProcessingScripts.size() <= tasksSingleThreadThreshold)
	{
		Promise<void> promise;
		for (const auto& x : eventProcessingScripts)
		{
			if (x->checkIsActive())
			{
				ComponentLock lock(*x);
				for (const auto& event : events)
				{
					x->processEvent(event);
				}
			}
		}
		promise.set_value();
		return promise.get_future();
	}
	SharedPtr<InvokeInfoBlock> invokeInfo = makeSharedPtr<InvokeInfoBlock>();
	const auto taskCount = std::min(static_cast<std::size_t>(threadPool.getThreadsCount()), eventProcessingScripts.size());
	{
		DefaultLockGuard guard(threadPool.getTasksMutex());
		for (std::size_t x = 0; x < taskCount; ++x)
		{
			threadPool.addTaskNoLock([this, invokeInfo, &events]()
			{
				const unsigned int scripts = eventProcessingScripts.size();
				unsigned int itemToWorkOn = invokeInfo->currentItem.fetch_add(1, std::memory_order::memory_order_relaxed), offset = itemToWorkOn, temp = 0;
				auto it = eventProcessingScripts.begin();
				while (itemToWorkOn < scripts)
				{
					it = std::next(it, offset);
					auto& script = **it;
					if (script.checkIsActive())
					{
						ComponentLock lock(script);
						for (const auto& event : events)
						{
							script.processEvent(event);
						}
					}
					temp = invokeInfo->currentItem.fetch_add(1, std::memory_order::memory_order_relaxed);
					if ((invokeInfo->completedWorkItems.fetch_add(1, std::memory_order::memory_order_relaxed) + 1) == scripts)
					{
						invokeInfo->result.set_value();
					}
					else
					if (temp < scripts)
					{
						offset = (temp - itemToWorkOn);
					}
					itemToWorkOn = temp;
				}
			});
		}
	}
	threadPool.onLockEnd();

	return invokeInfo->result.get_future();
}
