#pragma once
#ifndef GLOWE_INL_RESOURCE_INCLUDED
#define GLOWE_INL_RESOURCE_INCLUDED

#include "Resource.h"

template<class T>
inline GLOWE::ResourceHandle<T>::operator bool() const
{
	return resourcePtr.get();
}

template<class T>
inline const GLOWE::SharedPtr<T>& GLOWE::ResourceHandle<T>::getResource() const
{
	return resourcePtr;
}

template<class T>
inline GLOWE::Hash GLOWE::ResourceHandle<T>::getResourceHash() const
{
	return resourceHash;
}

template<class T>
inline GLOWE::Hash GLOWE::ResourceHandle<T>::getSubresourceHash() const
{
	return subresourceHash;
}

template<class T>
inline void GLOWE::ResourceHandle<T>::setResourceHash(const Hash& arg)
{
	resourceHash = arg;
}

template<class T>
inline void GLOWE::ResourceHandle<T>::setSubresourceHash(const Hash& arg)
{
	subresourceHash = arg;
}

template<class T>
inline void GLOWE::ResourceHandle<T>::swap(ResourceHandle<T>& handle)
{
	std::swap(resourcePtr, handle.resourcePtr);
	std::swap(resourceHash, handle.resourceHash);
	std::swap(subresourceHash, handle.subresourceHash);
}

template<class T>
inline GLOWE::ResourceHandle<T> GLOWE::ResourceMgr::load(const char* name, const char* resourceName)
{
	return load<T>(Hash(name), Hash(resourceName));
}

template<class T>
inline GLOWE::ResourceHandle<T> GLOWE::ResourceMgr::load(const String& name, const String& resourceName)
{
	return load<T>(Hash(name), Hash(resourceName));
}

template<class T>
GLOWE::ResourceHandle<T> GLOWE::ResourceMgr::load(const Hash& nameHash, const Hash& resourceHash)
{
	try
	{
		const ResourcePlacement placement = checkResourcePlacement(nameHash);
		if (placement != ResourcePlacement::NonExisting)
		{
			ResourceFile& file = getResourceFileInfo(nameHash);
			SubresourceInfo& subres = file.subresources.at(resourceHash);
			DefaultLockGuard lockGuard(subres.mutex);
			SharedPtr<T> tempPtrLocked = std::static_pointer_cast<T>(subres.resourcePtr.lock());

			if (!tempPtrLocked)
			{
				ResourceHandle<T> result;
				// Load the subresource (and its subresources).
				Map<Hash, ResourceHandle<void>> deps;
				loadDependiencies(nameHash, file, subres, deps);
				result = preLoad<T>(nameHash, resourceHash, deps);
				if (!result)
				{
					SharedPtr<String> fileContents = privLoad(nameHash);
					result = loadDynamic<T>(fileContents->getCharArray(), fileContents->getSize(), deps, resourceHash);
				}

				result.setResourceHash(nameHash);
				result.setSubresourceHash(resourceHash);
				subres.resourcePtr = std::static_pointer_cast<void>(result.getResource());

				return result;
			}
			else
			{
				return ResourceHandle<T>(tempPtrLocked, nameHash, resourceHash);
			}
		}
		else
		{
			throw Exception("The resource cannot be located");
		}
	}
	catch (const Exception& e)
	{
		WarningThrow(false, "Unable to load resource");
		std::rethrow_exception(std::current_exception());
	}
	catch (const std::exception& e)
	{
		throw StringException("Unable to load a resource (std::exception): "_str + e.what());
	}
	catch (...)
	{
		throw Exception("Unable to load a resource. Unknown reason.");
	}

	return getDefaultResource<T>();
}

// In order to enable a class GLOWSYSTEM_EXPORT to be loaded like a resource, specialize one or more of the template methods below:
template<class T>
inline GLOWE::ResourceHandle<T> GLOWE::ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToRetrieve)
{
	// gcc hack.
	static_assert(std::is_same<T, void>::value && !std::is_same<T, void>::value, "Unable to load unspecialized type.");
	return ResourceHandle<T>();
}

template<class T>
inline GLOWE::ResourceHandle<T> GLOWE::ResourceMgr::getDefaultResource()
{
	// gcc hack.
	static_assert(std::is_same<T, void>::value && !std::is_same<T, void>::value, "Unable to get default resource of unspecialized type.");
	return ResourceHandle<T>();
}

// Loads the whole file, there is no internal division to subresources.
template<>
inline GLOWE::ResourceHandle<GLOWE::String> GLOWE::ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToRetrieve)
{
	return makeSharedPtr<String>(static_cast<const char*>(memory), size);
}

template<>
inline GLOWE::ResourceHandle<GLOWE::String> GLOWE::ResourceMgr::getDefaultResource()
{
	static ResourceHandle<String> result = makeSharedPtr<String>("");
	return result;
}

template<class T>
inline GLOWE::ResourceHandle<T> GLOWE::loadResource(const char* name, const char* resourceName)
{
	return getInstance<ResourceMgr>().load<T>(name, resourceName);
}

template<class T>
inline GLOWE::ResourceHandle<T> GLOWE::loadResource(const String& name, const String& resourceName)
{
	return getInstance<ResourceMgr>().load<T>(name, resourceName);
}

template<class T>
inline GLOWE::ResourceHandle<T> GLOWE::loadResource(const Hash& nameHash, const Hash& resourceName)
{
	return getInstance<ResourceMgr>().load<T>(nameHash, resourceName);
}

#endif
