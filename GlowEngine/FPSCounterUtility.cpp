#include "FPSCounterUtility.h"

void GLOWE::FPSCounterUtility::update()
{
	++frames;
	time += frameClock.getAndReset();
	if (time >= samplingTime)
	{
		const float secs = time.asSeconds();
		currentFPS = static_cast<float>(frames) / secs;
		currentTimePerFrame = secs / static_cast<float>(frames);
		time = Time::ZeroTime;
		frames = 0;
	}
}

GLOWE::FPSCounterUtility::FPSCounterUtility()
	: frames(0), time(), currentTimePerFrame(), frameClock(), currentFPS(0), samplingTime(Time::fromSeconds(1.0f))
{
	getInstance<Engine>().addSubsystem(this, Engine::SubsystemUpdateRoutine::BeforeScriptUpdate);
}

void GLOWE::FPSCounterUtility::setSamplingTime(const Time& newSaTime)
{
	samplingTime = newSaTime;
}

float GLOWE::FPSCounterUtility::getCurrentFPS() const
{
	return currentFPS;
}

GLOWE::Time GLOWE::FPSCounterUtility::getCurrentTimePerFrame() const
{
	return currentTimePerFrame;
}
