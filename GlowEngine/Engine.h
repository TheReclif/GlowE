#pragma once
#ifndef GLOWE_ENGINE_INCLUDED
#define GLOWE_ENGINE_INCLUDED

#include "glowengine_export.h"

#include "ScriptableSubsystem.h"
#include "EngineGraphicsFactory.h"

#include "../GlowSystem/Time.h"
#include "../GlowSystem/ThreadPool.h"

#include "../GlowGraphics/Uniform/GraphicsWindow.h"

namespace GLOWE
{
	/// @brief Main engine class. Manages the initialization, main loop and clean up operations.
	class GLOWENGINE_EXPORT Engine
		: public Subsystem
	{
	public:
		/// @brief Enum specifying when to update the scriptable subsystem.
		enum class SubsystemUpdateRoutine
		{
			BeforeRender,
			AfterRender,
			BeforeScriptUpdate,
			AfterScriptUpdate
		};

		/// @brief Enum specifying consecutive loop phases. Values are ordered by the order of execution.
		enum class LoopPhase
		{
			BeforeScriptProcessing,
			EventProcessing,
			AsyncUpdate,
			PostSyncUpdate,
			ActivationUpdate,
			AfterScriptProcessing,
			BeforeRender,
			Render,
			AfterRender
		};
	private:
		String windowTitle;
		bool shouldChangeWindowTitle;

		UniquePtr<GraphicsDeviceImpl> gc;
		GraphicsWindow mainWindow;
		ThreadPool engineThreads;

		Clock timeFromStart;
		Time whenToExit, accumulatedExitTime, deltaTime, physicsUpdateInterval, targetFrameTime;
		bool shouldExit, exitScheduled, isStarted, isClosedProcessingEnabled;
		std::function<void(Engine&)> callbackAfterInitialize;
		std::function<bool()> shouldPresentThisFrame;
		List<std::function<void()>> cleanUpCallbacks;
		unsigned int frameId;

		Multimap<int, ScriptableSubsystem*> beforeRenderSubsystems, afterRenderSubsystems, beforeScriptUpdateSubsystems, afterScriptUpdateSubsystems;
		Vector<UniquePtr<ScriptableSubsystem>> ownedSubsystems;
		volatile LoopPhase currentLoopPhase;
	private:
		void loop(); // Called by ctor.
		void initialize(); // Loads basic resources and resources' info.
		void cleanUp();
	public:
		Engine();
		~Engine();

		/// @brief Adds a scriptable subsystem to the subsystems' registry.
		/// @param subsystem Subsystem to add. Will be owned by the Engine and destroyed at the Engine's clean up.
		/// @param updateRoutine When to update the subsystem.
		/// @param order Order in the registry.
		void addSubsystem(UniquePtr<ScriptableSubsystem>&& subsystem, const SubsystemUpdateRoutine updateRoutine, const int order = 0);
		/// @brief Adds a scriptable subsystem to the subsystems' registry.
		/// @param subsystem Subsystem to add.
		/// @param updateRoutine When to update the subsystem.
		/// @param order Order in the registry.
		void addSubsystem(ScriptableSubsystem* const subsystem, const SubsystemUpdateRoutine updateRoutine, const int order = 0);

		/// @brief Registers a callback that is called just before the clean up.
		/// @param callback Callback to register.
		/// @return Newly added callback's iterator. Used in removeCleanUpCallback.
		List<std::function<void()>>::const_iterator addCleanUpCallback(const std::function<void()>& callback);
		/// @brief Removes a clean up callback.
		/// @param it Callback iterator.
		void removeCleanUpCallback(const List<std::function<void()>>::const_iterator it);

		/// @brief Sets a function to be called after the engine's initialization.
		/// @param func Function to call. It must return nothing and has one parameter - a reference to the engine.
		void setCallbackAfterInitialize(const std::function<void(Engine&)>& func);
		/// @brief Sets a function that's called before every display call to check whether it's needed. Always true by default.
		/// @param predicate Predicate to set.
		void setShouldPresentThisFramePredicate(const std::function<bool()>& predicate);

		/// @brief Schedules an immediate application exit, i.e. at the end of the loop update. Can be aborted in the same frame.
		void exit();
		/// @brief Schedules a timed application exit. Can be aborted.
		/// @param when How much time must pass before exiting
		void scheduleExit(const Time& when);
		/// @brief Aborts already scheduled exit. Doesn't work when it's too late, i.e. during the destruction of script objects.
		void abortExit();

		/// @brief Starts the engine loop. Should be called exactly once.
		void startLoop();

		/// @brief Sets the window's title.
		/// @param newTitle Window's title.
		void setWindowTitle(const String& newTitle);
		/// @brief Returns the window's title.
		/// @return Window title.
		String getWindowTitle() const;

		/// @brief Returns an amount of time that has passed from the previous frame. If it's too big, it will get clamped to targetFrameTime value.
		/// @return Frame time delta.
		const Time& getDeltaTime() const;
		/// @brief Returns an amount of time that the engine is running.
		/// @return Time from engine construction.
		Time getTimeFromStart() const;

		/// @brief Returns the graphics device.
		/// @return Graphics device.
		GraphicsDeviceImpl& getGraphicsContext();
		/// @brief Returns the graphics device.
		/// @return Graphics device.
		const GraphicsDeviceImpl& getGraphicsContext() const;

		/// @brief Returns current frame's id. The first frame has an id 0.
		/// @return Frame's id.
		unsigned int getCurrentFrameId() const;
		/// @brief Returns current loop phase.
		/// @return Current loop phase
		LoopPhase getCurrentLoopPhase() const;

		/// @brief Enables or disables key repeating. Key repeating 
		/// @param repeatKey true to enable key repeating, false otherwise.
		void setKeyRepeat(const bool repeatKey);
		/// @brief Enables/disables engine Event::Closed processing. When enabled, the engine closes the application if the close signal is sent.
		/// @param processClosed Enable or disable processing.
		void setClosedEventProcessing(const bool processClosed);
		
		/// @brief Returns the engine's window. Caution: use at own risk. Preferably use Engine's methods whenever possible. On Windows: calls to WinAPI tend to crash the program when being done from a thread that does not own the window. Therefore, DO NOT use it in async methods (try to use sync update in scripts).
		/// @return Reference to engine's window.
		GraphicsWindow& getWindow();
		/// @brief Returns the engine allocated thread pool. It's used internally by the engine, so use it with caution.
		/// @return Engine's internal thread pool.
		ThreadPool& getEngineThreads();
	};
}

#endif
