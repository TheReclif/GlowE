#pragma once
#ifndef GLOWE_ENGINE_MATERIAL_INCLUDED
#define GLOWE_ENGINE_MATERIAL_INCLUDED

#include "Effect.h"
#include "Resource.h"

#include "../GlowGraphics/Uniform/UnorderedAccessViewImpl.h"
#include "../GlowGraphics/Uniform/MaterialEnvironment.h"

namespace GLOWE
{
	class GLOWENGINE_EXPORT Material
		: public NoCopy
	{
	public:
		struct GLOWENGINE_EXPORT CBuffer
		{
			const Effect::CBuffer* cbufferInEffect = nullptr;
			bool isDirty = false, isOwner = true;
			SharedPtr<BufferImpl> cbufferBuffer;
			BasicBuffer dataToUpdate;
			Set<Hash> engineSuppliedVersions;

			CBuffer() = default;
			~CBuffer() = default;
		};

		struct GLOWENGINE_EXPORT Pass
		{
			struct ShaderDesc
			{
				Vector<unsigned int> usedCBuffers, usedTextures, usedUAVs;
				bool isPresent = false;
			};

			ShaderDesc pixelShader, vertexShader, geometryShader, computeShader;
		};
	private:
		struct OptionalNotOwnedTexture
		{
			union
			{
				ResourceHandle<TextureImpl> handle;
				TextureImpl* texPurePtr;
			};
			bool isOwned;

			OptionalNotOwnedTexture();
			OptionalNotOwnedTexture(const OptionalNotOwnedTexture&) = delete;
			OptionalNotOwnedTexture(OptionalNotOwnedTexture&& arg);

			OptionalNotOwnedTexture& operator=(const OptionalNotOwnedTexture&) = delete;
			OptionalNotOwnedTexture& operator=(OptionalNotOwnedTexture&& arg);

			OptionalNotOwnedTexture(const ResourceHandle<TextureImpl>& arg);
			OptionalNotOwnedTexture(TextureImpl* const arg);

			~OptionalNotOwnedTexture();

			TextureImpl* getTexture() const;
		};
	private:
		Mutex collMutex;

		Map<Hash, unsigned int> cbufferNames, textureNames, uavNames;
		Map<unsigned int, Pass> passes; // We don't have to think about variants.

		Vector<CBuffer> cbuffers; // All constant buffers.

		Vector<OptionalNotOwnedTexture> textures;

		Vector<UnorderedAccessViewImpl*> uavs;

		ResourceHandle<Effect> effect;

		Effect::Technique* associatedTechnique = nullptr; // Passes here.
	public:
		Material() = default;
		~Material() = default;

		void clear();

		/// @brief Checks whether the material was properly initialized i.e. no errors occured during its construction.
		/// @return true if valid, false otherwise.
		bool checkIsValid() const;

		int getCBufferPosition(const Hash& hash) const;
		int getCBufferFieldPosition(const unsigned int cbuffPos, const Hash& fieldName) const;
		int getTexturePosition(const Hash& hash) const;
		int getUAVPosition(const Hash& hash) const;

		void setTexture(const unsigned int pos, const ResourceHandle<TextureImpl>& newTexture);
		void setTexture(const unsigned int pos, TextureImpl* const newTexture);
		bool checkIsTexturePresent(const unsigned int pos) const;

		void setUAV(const unsigned int pos, UnorderedAccessViewImpl* const newUav);
		bool checkIsUAVPresent(const unsigned int pos) const;

		void applyEnvironment(const MaterialEnvironment& env); // Call before calling apply.
		void apply(const unsigned int passId, const Hash& variantHash, BasicRendererImpl& renderer);

		const ShaderCollectionImpl& getShaderCollection(const unsigned int passId, const Hash& variantHash);

		const Vector<Effect::Technique::Tag>& getTags() const;
		const Map<unsigned int, Pass>& getPasses() const;
		
		template<class T>
		void modifyCBuffer(const unsigned int cbuffPos, const unsigned int elemPos, const T& arg)
		{
			if (cbuffPos >= cbuffers.size())
			{
				return;
			}
			CBuffer& cbuff = cbuffers[cbuffPos];

			cbuff.isDirty = true;
			cbuff.dataToUpdate.modifyElement<T>(0, elemPos, arg);

			if (!cbuff.isOwner)
			{
				// Create a new buffer.
				BufferDesc buffDesc;
				buffDesc.binds = BufferDesc::Binding::ConstantBuffer;
				buffDesc.cpuAccess = CPUAccess::Write;
				buffDesc.usage = Usage::Default;

				// Possibly needless.
				buffDesc.layout = cbuff.dataToUpdate.getLayout();

				auto tempShared = createGraphicsObject<BufferImpl>();
				tempShared->create(buffDesc, cbuff.dataToUpdate.getBufferSize(), std::cref(*getInstance<ResourceMgr>().getDevice()), cbuff.dataToUpdate.getData());
				cbuff.cbufferBuffer = uniqueToShared(std::move(tempShared));
				cbuff.isDirty = false;
				cbuff.isOwner = true;
			}
		}
		void modifyCBuffer(const unsigned int cbuffPos, const unsigned int elemPos, const void* data, const unsigned int dataSize);

		static void createFromEffect(Material& result, const GraphicsDeviceImpl& device, const ResourceHandle<Effect>& effect, const Hash& techniqueName, const Hash& techniqueGroupName = Hash());

		friend class ResourceMgr;
	};

	template<>
	GLOWENGINE_EXPORT ResourceHandle<Material> ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToRetrieve);

	template<>
	GLOWENGINE_EXPORT ResourceHandle<Material> ResourceMgr::getDefaultResource();
}

#endif
