#pragma once
#ifndef GLOWE_ENGINE_RESOURCE_INCLUDED
#define GLOWE_ENGINE_RESOURCE_INCLUDED

#include "glowengine_export.h"

#include "../GlowSystem/Utility.h"
#include "../GlowSystem/String.h"
#include "../GlowSystem/Package.h"
#include "../GlowSystem/ThreadPool.h"
#include "../GlowSystem/LoadSaveHandle.h"
#include "../GlowSystem/FileParser.h"
#include "../GlowSystem/Filesystem.h"

// Important! First comes the internal usage hash, then the resource identyfing one.
// Tables from: https://ozh.github.io/ascii-tables/
// Resource Mgr design:
// Package layout:
// Package Definition File (PD);
// Resource Files (RFs).
// PD:
//             Element                   Size                  Type          
//  ----------------------------- ------------------- ---------------------- 
//   Version                       8 bytes             UInt64                
//   Number of resources           8 bytes             UInt32                
//   Number of virtual resources   8 bytes             UInt32                
//   List of resources             At least 16 bytes   PackagedResourceFile  
//   List of virtual resources     At least 24 bytes   ResourceFile          

// Packaged resource layout:
//          Element                 Size                    Type            
//  ------------------------ ------------------- -------------------------- 
//   Hash                     8 bytes             Hash                    
//   Path                     N bytes             FilePath                  
//   Number of subresources   4 bytes             UInt32                    
//   List of subresources     At least 88 bytes   Hash + SubresourceInfo  
// Compiled resource layout:
//        Element                  Size              Type    
//  ------------------- -------------------------- --------- 
//   Magic string        sizeof(u8"ZuziaK") bytes   char[]   
//   Importer settings   Depends                    Depends  
//   Resource data       N bytes                    String   
// Subresource info layout:
//               Element                Size                    Type                 
//  --------------------------------- --------- ------------------------------------ 
//   Type                              1 byte    SubresourceType                     
//   Number of internal dependencies   4 bytes   UInt32                              
//   Internal dependencies             N bytes   Pair<Hash, Hash>                
//   Number of external dependencies   4 bytes   UInt32                              
//   External dependencies             N bytes   Pair<Hash, Pair<Hash, Hash>>  

// Important!
// When specializing loadDynamic template, use makeSharedPtr<T>(args) instead of ResourceHandle<T>(arg).

namespace GLOWE
{
	class ResourceMgr;
	class GraphicsDeviceImpl;

	template<class T>
	class ResourceHandle
	{
	private:
		SharedPtr<T> resourcePtr;
		Hash resourceHash, subresourceHash;
	public:
		ResourceHandle() = default;
		inline ResourceHandle(const SharedPtr<T>& ptr)
			: resourcePtr(ptr), resourceHash(), subresourceHash()
		{}
		inline ResourceHandle(const SharedPtr<T>& ptr, const Hash& resHash, const Hash& subresHash)
			: resourcePtr(ptr), resourceHash(resHash), subresourceHash(subresHash)
		{}

		GlowStaticSerialize(GlowBases(), GlowField(resourceHash), GlowField(subresourceHash));

		operator bool() const;

		const SharedPtr<T>& getResource() const;

		template<class Type>
		inline ResourceHandle<Type>& treatAs()
		{
			return reinterpret_cast<ResourceHandle<Type>&>(*this);
		}

		template<class Type>
		inline const ResourceHandle<Type>& treatAs() const
		{
			return reinterpret_cast<const ResourceHandle<Type>&>(*this);
		}

		typename std::add_lvalue_reference<T>::type operator*() const
		{
			return *resourcePtr;
		}

		T* operator->() const
		{
			return resourcePtr.get();
		}

		Hash getResourceHash() const;
		Hash getSubresourceHash() const;

		void setResourceHash(const Hash& arg);
		void setSubresourceHash(const Hash& arg);

		void swap(ResourceHandle<T>& handle);

		friend class ResourceMgr;
	};

	class ResourceMgr
		: public Subsystem
	{
	public:
		// Expand this enum to support more subresources.
		enum class SubresourceType : UInt32
		{
			Texture,
			Material,
			Effect,
			HLSLShaderCode,
			GLSLShaderCode,
			ShaderCollection,
			Geometry,
			GeometryBuffers,
			GameObjectBlueprint,
			Scene,
			Font,

			ElemsCount
		};

		enum class ResourcePlacement : unsigned char
		{
			NonExisting,
			InPackage,
			OnDisk
		};
	public:
		static constexpr char magicString[] = u8"ZuziaK";
		static constexpr char packageDefinitionFileName[] = u8"PackageDefinition.pd";

		class GLOWENGINE_EXPORT SubresourceInfo
		{
		public:
			WeakPtr<void> resourcePtr;
			// Map <internalName -> <nameInFile, isLazy>>
			Vector<Pair<Hash, Pair<Hash, bool>>> internalDependiencies;
			// Map <internalName -> <fileName, nameInFile, isLazy>>
			Vector<Pair<Hash, std::tuple<Hash, Hash, bool>>> externalDependiencies;
			SubresourceType type;
			Mutex mutex;
		public:
			SubresourceInfo() = default;

			SubresourceInfo(const SubresourceType& newType)
				: resourcePtr(), internalDependiencies(), externalDependiencies(), type(newType), mutex() {};

			// May be worth removing.
			SubresourceInfo(const SubresourceInfo& arg)
				: resourcePtr(arg.resourcePtr), internalDependiencies(arg.internalDependiencies), externalDependiencies(arg.externalDependiencies), type(arg.type), mutex() {};
			SubresourceInfo(SubresourceInfo&& arg) noexcept
				: resourcePtr(std::move(arg.resourcePtr)), internalDependiencies(std::move(arg.internalDependiencies)), externalDependiencies(std::move(arg.externalDependiencies)), type(arg.type), mutex() {};
		};

		class GLOWENGINE_EXPORT ResourceFile
			: private NoCopy
		{
		public:
			Map<Hash, SubresourceInfo> subresources;
			FilePath path;
		public:
			inline ResourceFile(ResourceFile&& arg) noexcept
				: subresources(std::move(arg.subresources)), path(std::move(arg.path)) {};
			inline ResourceFile(Map<Hash, SubresourceInfo>&& a1, const FilePath& a2)
				: subresources(std::move(a1)), path(a2) {};
		};

		class GLOWENGINE_EXPORT PackagedResourceFile
			: public ResourceFile
		{
		public:
			const FilePath& packagePath;
		public:
			inline PackagedResourceFile(Map<Hash, SubresourceInfo>&& a1, const FilePath& a2, const FilePath& a3)
				: ResourceFile(std::move(a1), a2), packagePath(a3) {};
		};
	private:
		Map<Hash, PackagedResourceFile> files;
		Map<Hash, ResourceFile> filesOnDisk;

		Map<Hash, FilePath> packagePaths;
		Map<Hash, Pair<WeakPtr<String>, Mutex>> loadedFiles;

		Map<SubresourceType, Vector<Pair<Hash, Hash>>> subresourcesByTypes;

		PackageMgr& packageMgr;

		const GraphicsDeviceImpl* graphicsDevice;

		Mutex diskFilesMutex, filesMutex, subresourcesByTypesMutex;
	private:
		GLOWENGINE_EXPORT static bool checkIfMagicStringIsCompatible(const char* arg);

		GLOWENGINE_EXPORT SharedPtr<String> privLoad(const Hash& nameHash);
		GLOWENGINE_EXPORT SharedPtr<String> privLoad(const ResourcePlacement placement, const Hash& hash, const ResourceFile& fileInfo);

		GLOWENGINE_EXPORT ResourceFile& getResourceFileInfo(const Hash& resHash);

		template<class T>
		ResourceHandle<T> preLoad(const Hash& resourceHash, const Hash& subresourceHash, const Map<Hash, ResourceHandle<void>>& deps) { return ResourceHandle<T>(); };

		GLOWENGINE_EXPORT void loadDependiencies(const Hash& hash, const ResourceFile& file, const SubresourceInfo& subres, Map<Hash, ResourceHandle<void>>& deps);
		GLOWENGINE_EXPORT void loadDependiencies(const Hash& hash, const ResourceFile& file, Vector<SubresourceInfo*> subresources, Vector<Map<Hash, ResourceHandle<void>>>& deps);

		GLOWENGINE_EXPORT static bool verifyVersion(const UInt64 ver);

		GLOWENGINE_EXPORT void cleanUp();
	public:
		GLOWENGINE_EXPORT ResourceMgr();
		GLOWENGINE_EXPORT ~ResourceMgr() = default;

		GLOWENGINE_EXPORT void initialize(const GraphicsDeviceImpl& device);
		GLOWENGINE_EXPORT ThreadPool::TaskResult<void> initializeAsync(const GraphicsDeviceImpl& device, ThreadPool& threads);

		GLOWENGINE_EXPORT const GraphicsDeviceImpl* getDevice() const;

		// For packaged resources prepend the package's name to the "name" parameter.
		// Eg. "SampleGame/Textures/Zuzi.png".
		template<class T>
		ResourceHandle<T> load(const char* name, const char* resourceName);
		template<class T>
		ResourceHandle<T> load(const String& name, const String& resourceName);
		template<class T>
		ResourceHandle<T> load(const Hash& nameHash, const Hash& resourceHash);

		GLOWENGINE_EXPORT Vector<ResourceHandle<void>> load(const Hash& nameHash, const Vector<Hash>& subresources);

		GLOWENGINE_EXPORT ResourceHandle<void> load(const char* name, const char* resourceName);
		GLOWENGINE_EXPORT ResourceHandle<void> load(const String& name, const String& resourceName);
		GLOWENGINE_EXPORT ResourceHandle<void> load(const Hash& nameHash, const Hash& resourceName);

		GLOWENGINE_EXPORT bool isLoadable(const Hash& nameHash) const;
		GLOWENGINE_EXPORT bool isLoadable(const Hash& nameHash, const Hash& resourceHash) const;

		GLOWENGINE_EXPORT const Vector<Pair<Hash, Hash>>& getSubresourcesOfType(const SubresourceType& type) const;

		GLOWENGINE_EXPORT ResourcePlacement checkResourcePlacement(const Hash& resHash) const;

		GLOWENGINE_EXPORT const ResourceFile& getResourceFileInfo(const Hash& resHash) const;

		// Does not register the newly created resource.
		template<class T>
		ResourceHandle<T> loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToRetrieve = Hash());
		GLOWENGINE_EXPORT ResourceHandle<void> loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const SubresourceType subresourceType, const Hash& subresourceToRetrieve = Hash());

		template<class T>
		ResourceHandle<T> getDefaultResource();

		GLOWENGINE_EXPORT void scanDirectory(const FilePath& path, ThreadPool& threads); // Will call scanFile for every found package in specified directory.
		GLOWENGINE_EXPORT Vector<ThreadPool::TaskResult<void>> scanDirectoryAsync(const FilePath& path, ThreadPool& threads);
		GLOWENGINE_EXPORT void scanFile(const FilePath& path);

		friend class Engine;
	};

	// Just a handful of useful helpers.
	template<class T>
	ResourceHandle<T> loadResource(const char* name, const char* resourceName);
	template<class T>
	ResourceHandle<T> loadResource(const String& name, const String& resourceName);
	template<class T>
	ResourceHandle<T> loadResource(const Hash& nameHash, const Hash& resourceName);
}

#include "Resource.inl"
#include "ResourceLoaders.h"

#endif
