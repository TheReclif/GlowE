#pragma once
#ifndef GLOWE_ENGINE_SCENE_INCLUDED
#define GLOWE_ENGINE_SCENE_INCLUDED

#include "../GlowSystem/OneInstance.h"
#include "../GlowSystem/MemoryMgr.h"
#include "../GlowSystem/String.h"

#include "glowengine_export.h"

namespace GLOWE
{
	class GameObject;
	class Scene;
	class Engine;

	/// @brief Class for managing all of the currently existing scenes.
	class GLOWENGINE_EXPORT SceneMgr
		: public Subsystem, public NoCopyNoMove
	{
	public:
		using SceneHandle = Map<Hash, UniquePtr<Scene>>::iterator;
		using ConstSceneHandle = Map<Hash, UniquePtr<Scene>>::const_iterator;
	private:
		struct SceneHandleComparator
		{
			bool operator()(const SceneHandle& l, const SceneHandle& r) const
			{
				return &(*l) < &(*r);
			}
		};
	private:
		Map<Hash, UniquePtr<Scene>> scenes;
		Scene* activeScene;
		Set<SceneHandle, SceneHandleComparator> scenesToUnload;
	private:
		void updateUnload();
	public:
		SceneMgr();

		/// @brief Creates and registers an empty scene.
		/// @param myName Scene's name.
		/// @return Reference to the created scene.
		Scene& createEmptyScene(const String& myName);

		/// @brief Sets the currently active scene. Active scene is a scene that every freshly created game object will belong to.
		/// @param scene Scene to set.
		void setActiveScene(Scene& scene);
		/// @brief Sets the currently active scene. Active scene is a scene that every freshly created game object will belong to.
		/// @param scene Pointer to scene to set.
		void setActiveScene(Scene* scene);
		/// @brief Returns the currently active scene.
		/// @return Active scene. Can be nullptr, but only if there are no scenes present.
		Scene* getActiveScene() const;
		/// @brief Returns a scene with the given name.
		/// @param myName Name of the scene to look up.
		/// @return Pointer to the found scene if it exists or nullptr otherwise.
		Scene* getScene(const Hash& myName) const;

		/// @brief Schedules given scene for unloading.
		/// @param scene Pointer to scene to unload.
		void unload(Scene* scene);
		/// @brief Schedules given scene for unloading.
		/// @param scene Scene to unload.
		void unload(Scene& scene);

		/// @brief Returns the begin iterator of the scenes' container. Used primarily in range for loops.
		/// @return Scenes' container begin iterator.
		ConstSceneHandle begin() const;
		/// @brief Returns the end iterator of the scenes' container. Used primarily in range for loops.
		/// @return Scenes' container end iterator.
		ConstSceneHandle end() const;

		/// @brief Unloads all scenes. Use of this method is highly unrecommended.
		void cleanUp();

		friend class Engine;
	};

	/// @brief Scene class. Scenes are a way of grouping game objects and managing their life time.
	class GLOWENGINE_EXPORT Scene
		: public NoCopy
	{
	private:
		struct CanConstructSceneTag
		{};
	public:
		using GameObjectHandle = List<UniquePtr<GameObject>>::iterator;
		using ConstGameObjectHandle = List<UniquePtr<GameObject>>::const_iterator;
	private:
		List<UniquePtr<GameObject>> gameObjects;
		SceneMgr::SceneHandle sceneMgrHandle;
		List<GameObjectHandle> objectsToDestroy;
		Set<GameObject*> addedObjects;
		List<GameObject*> finalObjectsKillList;
		String name;
	private:
		void updateChildrenToKill(GLOWE::GameObject* gameObject);
		void updateDestroy();

		Scene() = delete;
		Scene(const String& myName);
	public:
		/// @brief Default scene's constructor. Available only to befriended classes. To properly create a scene, use Scene::createEmptyScene or loadResource/ResourceMgr::load.
		/// @param myName Scene's name.
		/// @param  A default object of CanConstructSceneTag. This struct is available only to befriended classes.
		Scene(const String& myName, const CanConstructSceneTag&);
		Scene(Scene&&) noexcept = default;
		virtual ~Scene(); // Unloads everything.

		/// @brief Creates a game object belonging to this scene.
		/// @param name Game object's name.
		/// @return Reference to the created game object.
		GameObject& createGameObject(const String& name);
		/// @brief Tries to find a game object in the global scope (i.e. without a parent) with a given name.
		/// @param name Name of the game object to find
		/// @return A pointer to the found game object or nullptr if no object with this name was found
		GameObject* findGameObject(const String& name) const;
		/// @brief Destroys the given game object. It must belong to this scene, otherwise the destruction will not occur.
		/// @param gameObject Game object to destroy.
		void destroyGameObject(GameObject& gameObject);
		/// @brief Destroys the given game object. It must belong to this scene, otherwise the destruction will not occur.
		/// @param gameObject Pointer to the game object to destroy.
		void destroyGameObject(GameObject* gameObject);
		/// @brief Destroyes the given game object immediately. Prefer using destroyGameObject. It must belong to this scene, otherwise the destruction will not occur.
		/// @param gameObject Game object to destroy.
		void destroyGameObjectImmediate(GameObject& gameObject);
		/// @brief Destroyes the given game object immediately. Prefer using destroyGameObject. It must belong to this scene, otherwise the destruction will not occur.
		/// @param gameObject Pointer to the game object to destroy.
		void destroyGameObjectImmediate(GameObject* gameObject);

		// TODO: Modify (GameObject names) (GameObjectRegistry).

		/// @brief Changes the game object's name. Throws an exception if the given object is not owned by this scene.
		/// @param gameObject Game object to modify
		/// @param newName New game object's name
		void changeGameObjectName(GameObject* gameObject, const String& newName);

		/// @brief Returns the begin iterator of the game objects' container.
		/// @return Begin iterator of the game objects' container.
		GameObjectHandle begin();
		/// @brief Returns the begin iterator of the game objects' container.
		/// @return Begin iterator of the game objects' container.
		ConstGameObjectHandle begin() const;

		/// @brief Returns the end iterator of the game objects' container.
		/// @return End iterator of the game objects' container.
		GameObjectHandle end();
		/// @brief Returns the end iterator of the game objects' container.
		/// @return End iterator of the game objects' container.
		ConstGameObjectHandle end() const;

		/// @brief Transfers the given game object to this scene.
		/// @param obj Game object to transfer.
		void transferOwnership(GameObject* obj);

		/// @brief Returns the name of the scene.
		/// @return Scene's name.
		const String& getName() const;

		/// @brief Unloads the scene.
		void unload();

		/// @brief Creates an empty scene.
		/// @param myName Scene's name.
		/// @return Reference to the created scene.
		static Scene& createEmptyScene(const String& myName);

		friend class SceneMgr;
	};
}

#endif
