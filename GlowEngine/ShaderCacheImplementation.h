#pragma once
#ifndef GLOWE_UNIFORM_SHADERCACHEIMPL_INCLUDED
#define GLOWE_UNIFORM_SHADERCACHEIMPL_INCLUDED

#include "glowengine_export.h"

#include "../GlowSystem/Utility.h"
#include "../GlowSystem/Hash.h"
#include "../GlowSystem/ThreadPool.h"

namespace GLOWE
{
	class GraphicsDeviceImpl;
	class ShaderCollectionImpl;

	class GLOWENGINE_EXPORT ShaderCacheImpl
		: public Subsystem, public NoCopy
	{
	public:
		virtual ~ShaderCacheImpl() = default;

		virtual void loadCollection(const GraphicsDeviceImpl& device, const Hash& hash, ShaderCollectionImpl& coll) = 0;
		
		virtual Hash getCollectionFileHash(const Hash& collHash) const = 0;

		virtual bool loadCache() = 0;
		virtual ThreadPool::TaskResult<bool> loadCacheAsync(ThreadPool& threadPool) = 0;

		virtual void createCache() const = 0; // Must create the cache on disk.
		virtual ThreadPool::TaskResult<void> createCacheAsync(ThreadPool& threadPool) = 0; // Same as above.

		virtual ThreadPool::TaskResult<bool> createAndLoadAsync(ThreadPool& threadPool) = 0;

		virtual bool checkIsLoaded() const = 0;
	};

	template<>
	GLOWENGINE_EXPORT ShaderCacheImpl& OneInstance<ShaderCacheImpl>::instance();
}

#endif
