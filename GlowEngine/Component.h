#pragma once
#ifndef GLOWE_COMPONENT_INCLUDED
#define GLOWE_COMPONENT_INCLUDED

#include "GameEvent.h"

#include "../GlowSystem/Object.h"
#include "../GlowSystem/MemoryMgr.h"

// Macro definition
#define GlowInitComponentFactoryReflection(ClassName) \
namespace Hidden \
{ \
	template class AutoRegisterChild<ClassName>; \
}
// End of the macro

#define GlowSerializeComponent(ClassName, bases, ...) \
GlowSerialize(bases, __VA_ARGS__); \
GlowComponentTypeGetters(ClassName);
#define GlowSerializeComponentCustomAccessors(ClassName, bases, ...) \
GlowSerializeCustomAccessors(bases, __VA_ARGS__); \
GlowComponentTypeGetters(ClassName);

#define GlowInitCFR(Type) GlowInitComponentFactoryReflection(Type)
#define GlowInitCFROutsideNamespace(Type) namespace GLOWE { namespace Components{ GlowInitCFR(Type); } }
#define GlowInitScriptReflection(Type) GlowInitCFROutsideNamespace(Type) // Cannot be inside any namespace.

#define GlowComponentTypeGetters(ClassName) \
static inline ::GLOWE::UniquePtr<::GLOWE::Component> _construct(::GLOWE::GameObject* const newParent) { return ::GLOWE::makeUniquePtr<ClassName>(newParent); } \
static inline const char* getTypeNameGlobal() { return GlowU8Stringify(ClassName); } \
static inline ::GLOWE::Hash getTypeHashGlobal() { return ::GLOWE::Hash(GlowU8Stringify(ClassName)); } \
virtual inline ::GLOWE::String getTypeName() const override { return getTypeNameGlobal(); }; \
virtual inline ::GLOWE::Hash getTypeHash() const override { return getTypeHashGlobal(); };

namespace GLOWE
{
	class GameObject;

	using ComponentLock = Lockable::UniqueLock;

	namespace Components
	{
		template<class ComponentToExamine>
		class ComponentReflection
		{
		public:
			static inline const char* name() { return ComponentToExamine::getTypeNameGlobal(); }
			static inline const Hash hash() { return ComponentToExamine::getTypeHashGlobal(); }
		};

		// Some assumptions must be made in order to make this Component layout less-or-more flawless.
		// 1. Components are destroyed only by their gameObject. It means that they don't have to notify the gameObject about their imminent destruction.
		// 2. Component's gameObject exists ALWAYS. Because heroes never die.
		// 3. Only gameObject (GameObject) can kill their children (Components).

		/// @brief Base component class. Every component in GlowE must derive from Component or a class already deriving from Component.
		class GLOWENGINE_EXPORT Component
			: public Lockable, public Object, public Serializable
		{
		protected:
			GameObject* gameObject;
		private:
			bool isActive, wasFirstActivated;
		public:
			using Variables = Serializable::Variables;
		public:
			Component() = delete;
			Component(Component&&) noexcept = default;

			Component(GameObject* newParent);
			virtual ~Component() = default;

			GameObject* getGameObject() const;
			void setGameObject(GameObject* newOwner); // Will work only if the current parent is nullptr. Use clone to copy the existing and valid Component.

			bool checkIsActive() const;
			bool checkIsActiveLocally() const;
			bool checkWasFirstActivated() const;

			void setFirstActivation();

			void activate();
			void deactivate();

			virtual void processEvent(const GameEvent& event) = 0;

			virtual String getTypeName() const = 0;
			virtual Hash getTypeHash() const = 0;

			/// @brief Called once, at the object creation or when the editor restarts the game to the default state. In the editor mode it's called after deserialization.
			//virtual void initialize() = 0;

			// Useful callbacks when working with component serializers.
			/// @brief Called just before serialization.
			virtual void onBeforeSerialize() {};
			/// @brief Called just after serialization.
			virtual void onAfterSerialize() {};
			/// @brief Called just before deserialization.
			virtual void onBeforeDeserialize() {};
			/// @brief Called just after deserialization.
			virtual void onAfterDeserialize() {};

			/// @brief Called when the first activation is performed.
			virtual void onFirstActivate() = 0;

			/// @brief Called on every activation except the first one.
			virtual void onActivate() = 0;
			/// @brief Called on every deactivation.
			virtual void onDeactivate() = 0;

			/// @brief Called when the editor changes any of the values.
			/// @param modifiedField Name of the modified field.
			virtual void onEditorChange(const String& modifiedField) {};

			template<class Type>
			static inline Hash getTypeHash()
			{
				static_assert(std::is_base_of<Component, Type>::value, "Component::getTypeHash works only for classes that derives from Component.");
				return ComponentReflection<Type>::getHash();
			}

			template<class Type>
			static inline String getTypeName()
			{
				static_assert(std::is_base_of<Component, Type>::value, "Component::getTypeName works only for classes that derives from Component.");
				return ComponentReflection<Type>::getName();
			}
		};

		class GLOWENGINE_EXPORT AbstractComponentFactory
		{
		public:
			virtual ~AbstractComponentFactory() = default;

			inline bool isHashEqual(const Hash& hash) const
			{
				return hash == getHash();
			}
			inline bool isNameEqual(const String& name) const
			{
				return name == getName();
			}

			virtual String getName() const = 0;
			virtual Hash getHash() const = 0;

			virtual UniquePtr<Component> construct(GameObject* const newParent) const = 0;
		};

		template<class T>
		class ComponentFactoryRegistrator
		{
		public:
			ComponentFactoryRegistrator();
		};

		namespace Hidden
		{
			template<class ComponentToConstruct>
			class DetailComponentFactory
				: public GLOWE::Components::AbstractComponentFactory
			{
			public:
				virtual ~DetailComponentFactory() = default;

				virtual inline String getName() const override { return ComponentToConstruct::getTypeNameGlobal(); }
				virtual inline Hash getHash() const override { return ComponentToConstruct::getTypeHashGlobal(); }

				virtual inline UniquePtr<Component> construct(GameObject* const newParent) const override { return ComponentToConstruct::_construct(newParent); }
			};
			template<class Child>
			class AutoRegisterChild
			{
			private:
				static const ComponentFactoryRegistrator<DetailComponentFactory<Child>> myLovelyRegistrator;
			};
			template<class Child>
			const ComponentFactoryRegistrator<DetailComponentFactory<Child>> AutoRegisterChild<Child>::myLovelyRegistrator = ComponentFactoryRegistrator<DetailComponentFactory<Child>>();
		}

		class GLOWENGINE_EXPORT ComponentFactoryStorage
			: public Subsystem, public NoCopy
		{
		private:
			std::map<Hash, AbstractComponentFactory*> factories; // Not owning container.
		public:
			void addFactory(AbstractComponentFactory& arg);

			UniquePtr<Component> createComponent(GameObject* const newParent, const Hash& hash);
			UniquePtr<Component> createComponent(GameObject* const newParent, const String& name);
		};

		template<class T>
		inline ComponentFactoryRegistrator<T>::ComponentFactoryRegistrator()
		{
			static_assert(std::is_base_of<AbstractComponentFactory, T>::value, "Must be used with a class that derives from AbstractComponentFactory.");
			static T factory; // For dll it's destroyed on dll unload. For everything else it's destroyed on static objects uninitialization.
			getInstance<ComponentFactoryStorage>().addFactory(factory);
		}
	}

	using Component = Components::Component;
}

#endif
