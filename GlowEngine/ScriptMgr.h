#pragma once
#ifndef GLOWE_SCRIPTING_SCRIPTMGR_INCLUDED
#define GLOWE_SCRIPTING_SCRIPTMGR_INCLUDED

#include "glowengine_export.h"

#include "GameEvent.h"

#include "../GlowSystem/MemoryMgr.h"
#include "../GlowSystem/ThreadPool.h"

namespace GLOWE
{
	namespace Components
	{
		class ScriptBehaviour;
	}

	class GLOWENGINE_EXPORT ScriptMgr
		: public Subsystem
	{
	public:
		using ScriptContainer = UnorderedSet<Components::ScriptBehaviour*>;
		//using ScriptHandle = ScriptContainer::iterator;
		using ScriptHandle = Components::ScriptBehaviour*;
	private:
		ScriptContainer registeredScripts, asyncUpdatedScripts, eventProcessingScripts, postSyncUpdatedScripts;
	public:
		ScriptHandle registerScript(Components::ScriptBehaviour* arg);
		void unregisterScript(const ScriptHandle& handle);

		void invokeOnAll(const std::function<void(Components::ScriptBehaviour&)>& func);
		void invokePostSyncUpdate();

		ThreadPool::TaskResult<void> invokeOnAllAsync(ThreadPool& threadPool, const std::function<void(Components::ScriptBehaviour&)>& func);

		ThreadPool::TaskResult<void> invokeAsyncUpdate(ThreadPool& threadPool);
		ThreadPool::TaskResult<void> invokeAsyncEventProcessing(ThreadPool& threadPool, const Deque<Event>& events);
	};
}

#endif
