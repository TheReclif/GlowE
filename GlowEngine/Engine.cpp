#include "Engine.h"
#include "GameObject.h"
#include "ScriptMgr.h"
#include "Resource.h"

#include "../GlowSystem/Log.h"
#include "../GlowSystem/AsyncIO.h"

#include "../GlowComponents/ScriptBehaviour.h"

#include "RenderingEngine.h"
#include "Effect.h"
#include "Material.h"
#include "DebugDrawMgr.h"

#include "../GlowGraphics/Uniform/GSILShaderCompiler.h"
#include "../GlowGraphics/Uniform/Geometry.h"

#include "GUI/Text.h"

class FrameAllocatorMgrWrapper
	: public GLOWE::ScriptableSubsystem
{
public:
	GLOWE::FrameAllocatorMgr& frameAllocMgr = GLOWE::getInstance<GLOWE::FrameAllocatorMgr>();

	void update() override
	{
		frameAllocMgr.clearAllAllocators();
	}
};

class SwapChainSizeUpdater
	: public GLOWE::ScriptableSubsystem
{
public:
	GLOWE::Engine& engine = GLOWE::getInstance<GLOWE::Engine>();

	void update() override
	{
		engine.getWindow().getSwapChain().updateSize();
	}
};

void GLOWE::Engine::loop()
{
	getInstance<LogMgr>() << u8"Engine loop start. Scheduling initialization.";
	Clock gameClock;
	bool initSuccessful = false;
	try
	{
		initialize();
		initSuccessful = true;
	}
	catch (const Exception& e)
	{
		WarningThrow(false, "Unable to initialize the engine: GLOWE::Exception caught. Info: "_str + e.what() + ". Backtrace: " + e.getBacktraceStr());
	}
	catch (const std::exception& e)
	{
		WarningThrow(false, "Unable to initialize the engine: std::exception caught. Info: "_str + e.what());
	}
	catch (...)
	{
		WarningThrow(false, "Unable to initialize the engine: unknown exception caught.");
	}

	if (!initSuccessful)
	{
		getInstance<LogMgr>() << u8"Engine initialization failed. Check the error log for more info.";
		// Limited clean up.
		mainWindow.destroy();
		engineThreads.destroy();
		return;
	}

	try
	{
		{
			Time temp = gameClock.getElapsedTime();
			getInstance<LogMgr>() << u8"Initialization took " + toString(temp.asSeconds()) + u8" seconds.";
			gameClock.reset();
		}

		if (callbackAfterInitialize)
		{
			callbackAfterInitialize(*this);
		}

		auto& scriptMgr = getInstance<ScriptMgr>();
		auto& eventMgr = getInstance<EventMgr>();
		auto& renderingEngine = getInstance<RenderingEngine>();
		auto& gameObjectRegistry = getInstance<GameObjectRegistry>();

		GameEvent::Info eventInfo{};
		Queue<ThreadPool::TaskResult<void>> threadPoolResults;
		ThreadPool::TaskResult<void> renderingResult;

		// Main loop.
		while (!shouldExit)
		{
			currentLoopPhase = LoopPhase::BeforeScriptProcessing;
			const auto anyEventsToProcess = mainWindow.peekEvents(); // Empties the window event queue.

			deltaTime = gameClock.getAndReset();
			if (deltaTime > Time::fromSeconds(0.125f))
			{
				deltaTime = targetFrameTime;
			}

			for (auto& x : beforeScriptUpdateSubsystems)
			{
				x.second->update();
			}

			currentLoopPhase = LoopPhase::EventProcessing;
			if (anyEventsToProcess)
			{
				const auto& eventsQueue = eventMgr.getEventsQueue(mainWindow);
				threadPoolResults.push(scriptMgr.invokeAsyncEventProcessing(engineThreads, eventsQueue));

				if (isClosedProcessingEnabled)
				{
					for (const auto& x : eventsQueue)
					{
						if (x == Event::Type::Closed)
						{
							this->exit();
							break;
						}
					}
				}
			}

			currentLoopPhase = LoopPhase::AsyncUpdate;
			// May be called alongside event processing. Use caution.
			threadPoolResults.push(scriptMgr.invokeAsyncUpdate(engineThreads));

			while (!threadPoolResults.empty())
			{
				threadPoolResults.front().wait();
				threadPoolResults.pop();
			}
			eventMgr.clear(mainWindow);

			currentLoopPhase = LoopPhase::PostSyncUpdate;
			scriptMgr.invokePostSyncUpdate();
			currentLoopPhase = LoopPhase::ActivationUpdate;
			gameObjectRegistry.updateActivation();

			currentLoopPhase = LoopPhase::AfterScriptProcessing;
			for (auto& x : afterScriptUpdateSubsystems)
			{
				x.second->update();
			}

#ifndef GLOWE_GAME_PLAYER_BUILD
			// TODO: Probably a bad way to enable editor to work more efficiently.
			if (!shouldPresentThisFrame())
			{
				Clock::waitForGivenTime(Time::fromMilliseconds(25));
				continue;
			}
#endif

			currentLoopPhase = LoopPhase::BeforeRender;
			// Update subsystems.
			for (auto& x : beforeRenderSubsystems)
			{
				x.second->update();
			}

			currentLoopPhase = LoopPhase::Render;
			renderingResult = renderingEngine.scheduleRender(engineThreads);

			if (shouldChangeWindowTitle)
			{
				mainWindow.setTitle(windowTitle);
				shouldChangeWindowTitle = false;
			}

			renderingResult.wait();

			currentLoopPhase = LoopPhase::AfterRender;
			for (auto& x : afterRenderSubsystems)
			{
				x.second->update();
			}

			++frameId;
			mainWindow.display();
		}

		getInstance<LogMgr>() << u8"Engine loop end. Scheduling clean up.";
		gameClock.reset();
	}
	catch (const Exception & e)
	{
		WarningThrow(false, "Info: " + e.what() + ". Backtrace:\n" + e.getBacktraceStr());
		cleanUp();
		getInstance<LogMgr>() << u8"An uncatched error has occured. Aborting GlowE...";
		getInstance<LogMgr>().flush();
		return;
	}
	catch (const std::exception& e)
	{
		WarningThrow(false, e.what());
		cleanUp();
		getInstance<LogMgr>() << u8"An uncatched error has occured. Aborting GlowE...";
		getInstance<LogMgr>().flush();
		return;
	}
	catch (...)
	{
		WarningThrow(false, "An unidentifyable error has occured.");
		cleanUp();
		getInstance<LogMgr>() << u8"An uncatched (and unidentifyable) error occured. Aborting GlowE...";
		getInstance<LogMgr>().flush();
		return;
	}
	cleanUp();
	Time tempTime = gameClock.getElapsedTime();
	getInstance<LogMgr>() << u8"Clean up took " + toString(tempTime.asSeconds()) + " seconds.";
	getInstance<LogMgr>() << u8"GlowE signing off. Sayonara ^^!";
	getInstance<LogMgr>().flush();
}

void GLOWE::Engine::initialize()
{
	if (!Filesystem::isExisting("GlowCore.gpckg"))
	{
		WarningThrow(false, u8"Unable to find GlowCore.gpckg in the working directory. If you're the end user, please reinstall the application or use the \"find missing files\" option of your digital distribution service (Steam, Origin, etc.).");
		throw GLOWE::Exception(u8"Unable to find GlowCore.gpckg in the working directory");
	}

	engineThreads.create(getCPUCoresCount());
	ThreadPool::TaskResult<void> fontCacheLoadResult = engineThreads.addTask([]()
	{
		getInstance<FontCache>().init();
	});
	getInstance<LogMgr>() << u8"Engine has created a thread pool consisting of " + toString(getCPUCoresCount()) + u8" threads.";
	Hidden::enableEngineRenderBackend();

	SettingsMgr& settings = getInstance<SettingsMgr>();
	if (!settings.checkIsLoaded())
	{
		settings.importFromFile();
	}

	VideoMode mode;
	mode.x = settings.getSetting(u8"Width", u8"Window", 8U);
	mode.y = settings.getSetting(u8"Height", u8"Window", 8U);
	windowTitle = settings.getSetting(u8"Title", u8"Window", "GlowEngine Application");

	gc = createGraphicsObject<GraphicsDeviceImpl>();
	gc->create(GraphicsDeviceSettings());
	getInstance<LogMgr>() << u8"Graphics device created.";

	SwapChainDesc swapChainDesc;
	swapChainDesc.bufferCount = settings.getSetting(u8"BufferCount", u8"Graphics", 1U);
	swapChainDesc.initialPresentInterval = settings.getSetting(u8"PresentInterval", u8"Window", 1U);
	swapChainDesc.msaa = settings.getSetting(u8"MSAALevel", u8"Graphics", 0U);
	swapChainDesc.size = mode;

	mainWindow.create(*gc, mode, windowTitle, swapChainDesc);
	shouldChangeWindowTitle = false;
	getInstance<InputMgr>()._initForEngine(mainWindow);

	ResourceMgr& resMgr = getInstance<ResourceMgr>();
	const auto isResourceMgrReady = resMgr.initializeAsync(getGraphicsContext(), engineThreads);

	ShaderCacheImpl& shaderCache = getInstance<ShaderCacheImpl>();
	if (!shaderCache.checkIsLoaded())
	{
		if (!shaderCache.loadCache())
		{
			WarningThrow(false, u8"Unable to load shader cache. Generating a new one...");

			mainWindow.setTitle(u8"Generating shader cache...");
			mainWindow.resize(VideoMode(512, 256, 32));
			mainWindow.getSwapChain().updateSize();
			isResourceMgrReady.wait();
			ThreadPool::TaskResult<bool> shaderCacheResult = shaderCache.createAndLoadAsync(engineThreads);

			EventMgr& eventMgr = getInstance<EventMgr>();
			BasicRendererImpl& renderer = getGraphicsContext().getDefaultRenderer();
			RenderTargetImpl& rt = mainWindow.getSwapChain().getRenderTarget();

			ResourceHandle<TextureImpl> shaderGenWarningScreen = resMgr.load<TextureImpl>(u8"GlowCore/Textures/ShaderCacheGenWarning.gtex", Hash());
			const Float4 vertices[4] =
			{
				Float4{ 1.0f, 1.0f, 1.0f, 0.0f },
				Float4{ 1.0f, -1.0f, 1.0f, 1.0f },
				Float4{ -1.0f, -1.0f, 0.0f, 1.0f },
				Float4{ -1.0f, 1.0f, 0.0f, 0.0f }
			};
			const unsigned short indices[6] =
			{
				0, 1, 2,
				0, 2, 3
			};

			ResourceHandle<GeometryBuffers> buffers = makeSharedPtr<GeometryBuffers>();

			BufferDesc buffDesc;

			buffDesc.binds = BufferDesc::Binding::VertexBuffer;
			buffDesc.cpuAccess = CPUAccess::None;
			buffDesc.usage = Usage::Immutable;
			buffDesc.layout.addElement(Format(Format::Type::Float, 2));
			buffDesc.layout.addElement(Format(Format::Type::Float, 2));
			buffers->vertexBuffer->create(buffDesc, sizeof(Float4) * 4, getGraphicsContext(), vertices);

			buffDesc.binds = BufferDesc::Binding::IndexBuffer;
			buffDesc.layout = StructureLayout();
			buffDesc.layout.addElement(Format(Format::Type::UnsignedShort, 1));
			buffers->indicesBuffer->create(buffDesc, sizeof(unsigned short) * 6, getGraphicsContext(), indices);

			buffers->vertexLayout.addVertexElement("position", Format(Format::Type::Float, 2));
			buffers->vertexLayout.addVertexElement("texCoord", Format(Format::Type::Float, 2));

			GSILShader shader;
			shader.fromGSILCode(R"***(
			Sampler DefaultSampler
			{
				MinFilter = Nearest;
				MagFilter = Nearest;
				MipFilter = Nearest;
			};

			Texture2D TexToShow(DefaultSampler) : Property("TexToShow");

			Struct InOut VSIn
			{
				Float2 position;
				Float2 texCoord;
			};

			Struct InOut PSIn
			{
				SV Float4 position;
				Float2 texCoord;
			};

			Struct InOut PSOut
			{
				SV Float4 target;
			};

			PSIn VSMain(VSIn input)
			{
				PSIn output;
	
				output.position = Float4(input.position.xy, 0.0f, 1.0f);
				output.texCoord = input.texCoord;
	
				return output;
			}

			PSOut PSMain(PSIn input)
			{
				PSOut output;
	
				output.target = TexToShow.sample(input.texCoord);
	
				return output;
			}

			Technique Default
			{
				Pass 0
				{
					VertexShader = CompileShader(5.0, VSMain);
					PixelShader = CompileShader(5.0, PSMain);
				};
			};
		)***");

			Geometry geometry;
			geometry.buffers = buffers.treatAs<GeometryBuffersImpl>().getResource();
			geometry.count = 6;
			geometry.offset = 0;

			ResourceHandle<Effect> tempEffect(makeSharedPtr<Effect>(std::move(Effect::createFromGSIL(shader, getGraphicsContext()))));
			Material mat;
			Material::createFromEffect(mat, getGraphicsContext(), tempEffect, u8"Default");
			const int texPos = mat.getTexturePosition("TexToShow");
			if (texPos == -1)
			{
				WarningThrow(false, "Unable to find \"TexToShow\" in a default shader (placed inside the code itself). The DLL or the application may be corrupted.")
				return;
			}
			mat.setTexture(texPos, shaderGenWarningScreen);

			Viewport vp;
			vp.height = 256;
			vp.maxDepth = 1.0f;
			vp.minDepth = 0.0f;
			vp.width = 512;
			vp.topX = vp.topY = 0.0f;

			renderer.setViewport(vp);

			do
			{
				mainWindow.peekEvents();
				while (!eventMgr.checkIsEmpty(mainWindow))
				{
					Event tempEvent;
					eventMgr.popEvent(mainWindow, tempEvent);
				}

				renderer.clearColor(&rt);
				renderer.clearDepthStencil(&rt);

				renderer.setRenderTarget(&rt);
				renderer.setPrimitiveTopology(BasicRendererImpl::PrimitiveTopology::TriangleList);
				geometry.apply(renderer, getGraphicsContext(), mat.getShaderCollection(0, Hash()));

				mat.apply(0, Hash(), renderer);

				geometry.draw(renderer);

				mainWindow.display();
			} while (shaderCacheResult.wait_for(std::chrono::microseconds(50)) != std::future_status::ready);

			if (!shaderCacheResult.get())
			{
				WarningThrow(false, u8"Unable to load/create shader cache.");
			}

			mainWindow.setTitle(windowTitle);
			mainWindow.resize(mode);
		}
	}

	addSubsystem(makeUniquePtr<FrameAllocatorMgrWrapper>(), SubsystemUpdateRoutine::AfterRender);
	addSubsystem(makeUniquePtr<SwapChainSizeUpdater>(), SubsystemUpdateRoutine::BeforeScriptUpdate);
	fontCacheLoadResult.wait();
	isResourceMgrReady.wait();

	getInstance<DebugDrawMgr>().attachToGC(&getGraphicsContext());
}

void GLOWE::Engine::cleanUp()
{
	for (const auto& callback : cleanUpCallbacks)
	{
		callback();
	}

	const auto fontCleanUpResult = getInstance<FontCache>().cleanUpAsync(engineThreads, getGraphicsContext().getDefaultRenderer());
	afterRenderSubsystems.clear();
	beforeRenderSubsystems.clear();
	afterScriptUpdateSubsystems.clear();
	beforeScriptUpdateSubsystems.clear();
	ownedSubsystems.clear();
	fontCleanUpResult.wait();
	getInstance<AsyncIOMgr>().waitForRemainingTasks(); // We do it so that caches can save their work.
	getInstance<SceneMgr>().cleanUp();
	getInstance<ResourceMgr>().cleanUp();
	getInstance<InputMgr>()._cleanUpForEngine();
	mainWindow.destroy();
	gc->destroy();
	gc = nullptr;
	engineThreads.destroy();
}

GLOWE::Engine::Engine()
	: shouldExit(false), timeFromStart(), exitScheduled(false), isStarted(false), isClosedProcessingEnabled(true), engineThreads(), targetFrameTime(Time::fromSeconds(1.0f / 60.0f)), frameId(0), ownedSubsystems(), shouldPresentThisFrame([]() -> bool { return true; })
{
	getInstance<LogMgr>() << u8"GLOWE::Engine ctor.";
}

GLOWE::Engine::~Engine()
{
	getInstance<LogMgr>() << u8"GLOWE::Engine dtor.";
}

void GLOWE::Engine::addSubsystem(UniquePtr<ScriptableSubsystem>&& subsystem, const SubsystemUpdateRoutine updateRoutine, const int order)
{
	switch (updateRoutine)
	{
	case SubsystemUpdateRoutine::BeforeRender:
		beforeRenderSubsystems.emplace(order, subsystem.get());
		break;
	case SubsystemUpdateRoutine::AfterRender:
		afterRenderSubsystems.emplace(order, subsystem.get());
		break;
	case SubsystemUpdateRoutine::BeforeScriptUpdate:
		beforeScriptUpdateSubsystems.emplace(order, subsystem.get());
		break;
	case SubsystemUpdateRoutine::AfterScriptUpdate:
		afterScriptUpdateSubsystems.emplace(order, subsystem.get());
		break;
	}
	ownedSubsystems.emplace_back(std::move(subsystem));
}

void GLOWE::Engine::addSubsystem(ScriptableSubsystem* const subsystem, const SubsystemUpdateRoutine updateRoutine, const int order)
{
	switch (updateRoutine)
	{
	case SubsystemUpdateRoutine::BeforeRender:
		beforeRenderSubsystems.emplace(order, subsystem);
		break;
	case SubsystemUpdateRoutine::AfterRender:
		afterRenderSubsystems.emplace(order, subsystem);
		break;
	case SubsystemUpdateRoutine::BeforeScriptUpdate:
		beforeScriptUpdateSubsystems.emplace(order, subsystem);
		break;
	case SubsystemUpdateRoutine::AfterScriptUpdate:
		afterScriptUpdateSubsystems.emplace(order, subsystem);
		break;
	}
}

GLOWE::List<std::function<void()>>::const_iterator GLOWE::Engine::addCleanUpCallback(const std::function<void()>& callback)
{
	cleanUpCallbacks.emplace_back(callback);
	return std::prev(cleanUpCallbacks.cend());
}

void GLOWE::Engine::removeCleanUpCallback(const List<std::function<void()>>::const_iterator it)
{
	cleanUpCallbacks.erase(it);
}

void GLOWE::Engine::setCallbackAfterInitialize(const std::function<void(Engine&)>& func)
{
	callbackAfterInitialize = func;
}

void GLOWE::Engine::setShouldPresentThisFramePredicate(const std::function<bool()>& predicate)
{
	if (predicate)
	{
		shouldPresentThisFrame = predicate;
	}
	else
	{
		throw Exception("Cannot set \"shouldPresentThisFrame\" to null");
	}
}

void GLOWE::Engine::exit()
{
	shouldExit = true;
}

void GLOWE::Engine::scheduleExit(const Time& when)
{
	exitScheduled = true;
	whenToExit = when;
}

void GLOWE::Engine::abortExit()
{
	exitScheduled = false;
	shouldExit = false;
	whenToExit = accumulatedExitTime = Time();
}

void GLOWE::Engine::startLoop()
{
	if (!isStarted)
	{
		isStarted = true;
		loop();
	}
}

void GLOWE::Engine::setWindowTitle(const String& newTitle)
{
	if (mainWindow.checkIsCreated())
	{
		windowTitle = newTitle;
		shouldChangeWindowTitle = true;
	}
}

GLOWE::String GLOWE::Engine::getWindowTitle() const
{
	return windowTitle;
}

const GLOWE::Time& GLOWE::Engine::getDeltaTime() const
{
	return deltaTime;
}

GLOWE::Time GLOWE::Engine::getTimeFromStart() const
{
	return timeFromStart.getElapsedTime();
}

GLOWE::GraphicsDeviceImpl& GLOWE::Engine::getGraphicsContext()
{
	return *gc;
}

const GLOWE::GraphicsDeviceImpl& GLOWE::Engine::getGraphicsContext() const
{
	return *gc;
}

unsigned int GLOWE::Engine::getCurrentFrameId() const
{
	return frameId;
}

GLOWE::Engine::LoopPhase GLOWE::Engine::getCurrentLoopPhase() const
{
	return currentLoopPhase;
}

void GLOWE::Engine::setKeyRepeat(const bool repeatKey)
{
	mainWindow.setKeyRepeat(repeatKey);
}

void GLOWE::Engine::setClosedEventProcessing(const bool processClosed)
{
	isClosedProcessingEnabled = processClosed;
}

GLOWE::GraphicsWindow& GLOWE::Engine::getWindow()
{
	return mainWindow;
}

GLOWE::ThreadPool& GLOWE::Engine::getEngineThreads()
{
	return engineThreads;
}
