#include "GameObjectBlueprint.h"
#include "../GlowSystem/LoadSaveHandle.h"

GLOWE::GameObjectBlueprint::GameObjectBlueprint(const String& name, const Multimap<Hash, Component::Variables>& components, const Vector<ResourceHandle<void>>& resourcesToKeep)
	: blueprintName(name), componentBlueprints(), resourcesToCache(resourcesToKeep)
{
	for (const auto& x : components)
	{
		auto it = componentBlueprints.emplace(std::piecewise_construct, std::forward_as_tuple(x.first), std::tuple<>());
		for (const auto& y : x.second)
		{
			it->second.emplaceOrAssign(y.first, y.second->clone());
		}
	}
}

GLOWE::GameObject& GLOWE::GameObjectBlueprint::createGameObject(const String& name) const
{
	String nameToChoose(name);

	if (nameToChoose.isEmpty())
	{
		nameToChoose = blueprintName;
	}

	GameObject& result = GameObject::createGameObject(nameToChoose);

	for (const auto& componentData : componentBlueprints)
	{
		// First: type hash, second: component variables.
		Component& comp = result.addComponent(componentData.first);
		comp.deserialize(componentData.second, GLOWE::Hidden::GlobalObjectResolveEnvironment());
	}

	return result;
}

void GLOWE::GameObjectBlueprint::serialize(LoadSaveHandle& handle) const
{
	handle << blueprintName << static_cast<UInt32>(componentBlueprints.size());
	for (const auto& x : componentBlueprints)
	{
		handle << x.first << x.second.size();
		for (const auto& y : x.second)
		{
			Hidden::serializeMapSerializableValue(handle, y.first, y.second);
		}
	}
	handle << static_cast<UInt32>(resourcesToCache.size());
	for (const auto& x : resourcesToCache)
	{
		handle << x.getResourceHash() << x.getSubresourceHash();
	}
}

void GLOWE::GameObjectBlueprint::deserialize(LoadSaveHandle& handle)
{
	UniquePtr<Hidden::SerializableValueBase> unqPtr;
	UInt32 u32{};
	String str;
	Hash hash, subresHash;
	handle >> blueprintName >> u32;
	for (unsigned int x = 0; x < u32; ++x)
	{
		UInt32 varsSize{};
		handle >> hash >> varsSize;
		auto it = componentBlueprints.emplace(std::piecewise_construct, std::forward_as_tuple(hash), std::tuple<>());
		for (unsigned int y = 0; y < varsSize; ++y)
		{
			Hidden::deserializeMapSerializableValue(handle, it->second);
		}
	}
	handle >> u32;
	resourcesToCache.resize(u32);
	for (unsigned int x = 0; x < u32; ++x)
	{
		handle >> hash >> subresHash;
		resourcesToCache[x] = getInstance<ResourceMgr>().load(hash, subresHash);
	}
}

template<>
GLOWE::ResourceHandle<GLOWE::GameObjectBlueprint> GLOWE::ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToRetrieve)
{
	ResourceHandle<GameObjectBlueprint> result = makeSharedPtr<GameObjectBlueprint>();
	MemoryLoadSaveHandle handle(memory, size);

	handle >> *result;

	return result;
}

template<>
GLOWE::ResourceHandle<GLOWE::GameObjectBlueprint> GLOWE::ResourceMgr::getDefaultResource()
{
	ResourceHandle<GameObjectBlueprint> result = makeSharedPtr<GameObjectBlueprint>();
	return result;
}
