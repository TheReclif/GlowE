#include "Effect.h"
#include "Material.h"
#include "Engine.h"

GLOWE::Effect::Effect(const Effect& arg)
	: shaderColls(arg.shaderColls), depthStencilStates(arg.depthStencilStates), blendStates(arg.blendStates), rastStates(arg.rastStates), samplers(arg.samplers), inputLayouts(arg.inputLayouts), textures(arg.textures), cbuffers(arg.cbuffers), techniqueGroups(arg.techniqueGroups)
{
	for (auto& x : techniqueGroups)
	{
		for (auto& y : x.second)
		{
			for (auto& pass : y.second.passes)
			{
				pass.second.parent = this;
			}
		}
	}
}

GLOWE::Effect::Effect(Effect&& arg)
	: shaderColls(std::move(arg.shaderColls)), depthStencilStates(std::move(arg.depthStencilStates)), blendStates(std::move(arg.blendStates)), rastStates(std::move(arg.rastStates)), samplers(std::move(arg.samplers)), inputLayouts(std::move(arg.inputLayouts)), textures(std::move(arg.textures)), cbuffers(std::move(arg.cbuffers)), techniqueGroups(std::move(arg.techniqueGroups))
{
	for (auto& x : techniqueGroups)
	{
		for (auto& y : x.second)
		{
			for (auto& pass : y.second.passes)
			{
				pass.second.parent = this;
			}
		}
	}
}

const GLOWE::Effect::Pass & GLOWE::Effect::getPass(const Hash& techniqueName, const unsigned int passId, const Hash& techniqueGroupName) const
{
	return getTechnique(techniqueName, techniqueGroupName).passes.at(passId);
}

const GLOWE::Effect::Technique& GLOWE::Effect::getTechnique(const Hash & techniqueName, const Hash & techniqueGroupName) const
{
	return getTechniqueGroup(techniqueGroupName).at(techniqueName);
}

const GLOWE::Map<GLOWE::Hash, GLOWE::Effect::Technique>& GLOWE::Effect::getTechniqueGroup(const Hash & techniqueGroupName) const
{
	return techniqueGroups.at(techniqueGroupName);
}

GLOWE::Effect::Pass& GLOWE::Effect::getPass(const Hash& techniqueName, const unsigned int passId, const Hash& techniqueGroupName)
{
	return getTechnique(techniqueName, techniqueGroupName).passes.at(passId);
}

GLOWE::Effect::Technique& GLOWE::Effect::getTechnique(const Hash& techniqueName, const Hash& techniqueGroupName)
{
	return getTechniqueGroup(techniqueGroupName).at(techniqueName);
}

GLOWE::Map<GLOWE::Hash, GLOWE::Effect::Technique>& GLOWE::Effect::getTechniqueGroup(const Hash& techniqueGroupName)
{
	return techniqueGroups.at(techniqueGroupName);
}

GLOWE::Effect GLOWE::Effect::createFromGSIL(const GSILShader& gsilShader, const GraphicsDeviceImpl& device)
{
	Effect result;

	String header;
	const auto usedAPI = device.getAPIInfo().usedAPI;
	switch (usedAPI)
	{
	case GraphicsAPIInfo::GraphicsAPI::DirectX:
		header = gsilShader.toHLSL();
		break;
	}

	for (const auto& x : gsilShader.depthStencilStates)
	{
		result.depthStencilStates.push_back(getInstance<GPUStuffFactory<DepthStencilStateImpl>>().create(x.second, device));
	}

	for (const auto& x : gsilShader.blendStates)
	{
		result.blendStates.push_back(getInstance<GPUStuffFactory<BlendStateImpl>>().create(x.second, device));
	}

	for (const auto& x : gsilShader.rasterizerStates)
	{
		result.rastStates.push_back(getInstance<GPUStuffFactory<RasterizerStateImpl>>().create(x.second, device));
	}

	for (const auto& x : gsilShader.samplers)
	{
		result.samplers.push_back(getInstance<GPUStuffFactory<SamplerImpl>>().create(x.second, device));
	}

	for (const auto& x : gsilShader.textures)
	{
		result.textures.emplace_back(x.name, x.type);
	}

	for (const auto& x : gsilShader.constantBuffers)
	{
		result.cbuffers.emplace_back(x.name, Effect::CBuffer());
		Effect::CBuffer& cbuff = result.cbuffers[result.cbuffers.size() - 1].second;
		cbuff.layout = x.generateCbufferPropertyLayout().toHashedStructureLayout();
		for (const auto& field : x.fields)
		{
			const Hash name(field.property.name);
			auto& constraint = cbuff.variables[name];

			constraint.first = field.property.type;
			if (field.property.constraints.size() > 0)
			{
				constraint.second.reserve(field.property.constraints.size());

				for (const auto& y : field.property.constraints)
				{
					constraint.second.push_back(y);
				}
			}
		}
	}

	{
		String tempHLSLOutput = header, buffer;
		Map<Hash, SharedPtr<ComputeShaderImpl>> computeShaders;
		Map<Hash, SharedPtr<VertexShaderImpl>> vertexShaders;
		Map<Hash, SharedPtr<GeometryShaderImpl>> geometryShaders;
		//Map<Hash, SharedPtr<HullShaderImpl>> hullShaders;
		//Map<Hash, SharedPtr<DomainShaderImpl>> domainShaders;
		Map<Hash, SharedPtr<PixelShaderImpl>> pixelShaders;
		Map<Hash, unsigned int> colls;

		Map<Hash, unsigned int> registeredLayouts;
		Vector<InputLayoutDesc> inputLayouts;
		ThreadPool compilatorThreads(getCPUCoresCount());
		Vector<ThreadPool::TaskResult<void>> compileResults;
		ReadWriteMutex<Mutex> shaderMapsMutexes[5]{};

		for (const auto& x : gsilShader.techniqueGroups)
		{
			Map<Hash, Effect::Technique>& group = result.techniqueGroups[x.first];
			//tempStr = "G" + x.first;
			for (const auto& y : x.second)
			{
				Effect::Technique& technique = group[y.first];
				//tempStr += "T" + y.first;
				for (const auto& z : y.second.passes)
				{
					auto createShaderDefHash = [&y](const GSILShader::Pass::ShaderDefinition& def, const Hash& variant) -> Hash
					{
						String buffer;
						buffer += def.func;
						buffer += toString(def.version);
						if (!variant.isHashed())
						{
							for (const auto& x : def.replacements)
							{
								buffer += x;
							}
						}
						else
						{
							for (const auto& x : y.second.variants.at(variant))
							{
								buffer += toString(x);
							}
						}
						Hash result(buffer);
						return result;
					};

					Effect::Pass temp(&result);

					//tempStr += "P" + toString(z.first);
					const GSILShader::Pass& pass = z.second;

					if (!pass.vertexShader.func.isEmpty())
					{
						temp.vertexShader.isPresent = true;
						temp.vertexShader.usedCBuffers = pass.vertexShader.usedCBuffers;
						temp.vertexShader.usedUAVs = pass.vertexShader.usedUAVs;
						temp.vertexShader.usedTextures = pass.vertexShader.usedTextures;
						for (const auto& tempTex : temp.vertexShader.usedTextures)
						{
							temp.vertexShader.usedSamplers.push_back(gsilShader.textures[tempTex].samplerID);
						}
					}
					else
					{
						temp.vertexShader.isPresent = false;
					}

					if (!pass.pixelShader.func.isEmpty())
					{
						temp.pixelShader.isPresent = true;
						temp.pixelShader.usedCBuffers = pass.pixelShader.usedCBuffers;
						temp.pixelShader.usedUAVs = pass.pixelShader.usedUAVs;
						temp.pixelShader.usedTextures = pass.pixelShader.usedTextures;
						for (const auto& tempTex : temp.pixelShader.usedTextures)
						{
							temp.pixelShader.usedSamplers.push_back(gsilShader.textures[tempTex].samplerID);
						}
					}
					else
					{
						temp.pixelShader.isPresent = false;
					}

					if (!pass.computeShader.func.isEmpty())
					{
						temp.computeShader.isPresent = true;
						temp.computeShader.usedCBuffers = pass.computeShader.usedCBuffers;
						temp.computeShader.usedUAVs = pass.computeShader.usedUAVs;
						temp.computeShader.usedTextures = pass.computeShader.usedTextures;
						for (const auto& tempTex : temp.computeShader.usedTextures)
						{
							temp.computeShader.usedSamplers.push_back(gsilShader.textures[tempTex].samplerID);
						}
					}
					else
					{
						temp.computeShader.isPresent = false;
					}

					if (!pass.geometryShader.func.isEmpty())
					{
						temp.geometryShader.isPresent = true;
						temp.geometryShader.usedCBuffers = pass.geometryShader.usedCBuffers;
						temp.geometryShader.usedUAVs = pass.geometryShader.usedUAVs;
						temp.geometryShader.usedTextures = pass.geometryShader.usedTextures;
						for (const auto& tempTex : temp.geometryShader.usedTextures)
						{
							temp.geometryShader.usedSamplers.push_back(gsilShader.textures[tempTex].samplerID);
						}
					}
					else
					{
						temp.geometryShader.isPresent = false;
					}

					// Reserve space in advance.
					for (const auto& variant : pass.owner->variants)
					{
						temp.variants[variant.first];
					}

					compileResults.resize(pass.owner->variants.size());
					unsigned int variantId = 0;
					for (const auto& variant : pass.owner->variants)
					{
						compileResults[variantId] = compilatorThreads.addTask([&gsilShader, usedAPI, &temp, &colls, &result, &device, &pass, &header, &x, &y, &z, &tempHLSLOutput, &shaderMapsMutexes, &vertexShaders, &pixelShaders, &computeShaders, &geometryShaders](const Pair<const Hash, Vector<int>>& variant)
						{
							Hash hash;
							ShaderCollectionDesc collDesc;
							const String tempStr = "G" + x.first + "T" + y.first + "P" + toString(z.first) + "V" + variant.first.asNumberString();
							hash = tempStr;
							ShaderImpl::CompilationData compData;
							String threadHLSLOutput = tempHLSLOutput;

							{
								ReadWriteLock<Mutex> lock(shaderMapsMutexes[0]);
								lock.lockRead();
								if (temp.vertexShader.isPresent && vertexShaders.count(hash) == 0)
								{
									lock.unlock();
									compData.entrypoint = pass.vertexShader.func;
									compData.version = MathHelper::getShaderVersionFromFloat(pass.vertexShader.version);

									switch (usedAPI)
									{
									case GraphicsAPIInfo::GraphicsAPI::DirectX:
										threadHLSLOutput += gsilShader.toHLSL(pass, variant.first, ShaderType::VertexShader);
										break;
									}

									SharedPtr<VertexShaderImpl> shader(createGraphicsObject<VertexShaderImpl>());
									shader->compileFromCode(threadHLSLOutput, device, compData);

									const GSILShader::Function* mainFunc = nullptr;
									const GSILShader::Structure* inputStructure = nullptr;
									/*
									for (const auto func : pass.vertexShader.usedFunctions)
									{
										if (functions[func].name == pass.vertexShader.func)
										{
											mainFunc = &functions[func];
											break;
										}
									}
									*/

									for (const auto& func : gsilShader.functions)
									{
										if (func.name == pass.vertexShader.func)
										{
											mainFunc = &func;
											break;
										}
									}

									if (!mainFunc)
									{
										return;
									}

									// Find the input vertex/instance structure.
									for (const auto& arg : mainFunc->arguments)
									{
										for (const auto& tempStruct : gsilShader.structures)
										{
											if (tempStruct.isInOut && arg.find(tempStruct.name) != String::invalidPos)
											{
												// Found it.
												inputStructure = &tempStruct;
												break;
											}
										}

										if (inputStructure)
										{
											break;
										}
									}

									if (!inputStructure)
									{
										WarningThrow(false, u8"Unable to find in out structure for function " + mainFunc->name + '.');
										return;
									}

									for (const auto& field : inputStructure->fields)
									{
										if (field.isInstanceData)
										{
											shader->instanceLayoutDesc.addInstanceElement(field.name, field.asFormat());
										}
									}

									collDesc.vertexShader = shader;
									lock.lockWrite();
									vertexShaders.emplace(hash, std::move(shader));
									lock.unlock();
									threadHLSLOutput.resize(header.getSize());
								}
							}

							{
								ReadWriteLock<Mutex> lock(shaderMapsMutexes[1]);
								lock.lockRead();
								if (temp.pixelShader.isPresent && pixelShaders.count(hash) == 0)
								{
									lock.unlock();
									compData.entrypoint = pass.pixelShader.func;
									compData.version = MathHelper::getShaderVersionFromFloat(pass.pixelShader.version);

									switch (usedAPI)
									{
									case GraphicsAPIInfo::GraphicsAPI::DirectX:
										threadHLSLOutput += gsilShader.toHLSL(pass, variant.first, ShaderType::PixelShader);
										break;
									}

									SharedPtr<PixelShaderImpl> shader(createGraphicsObject<PixelShaderImpl>());
									shader->compileFromCode(threadHLSLOutput, device, compData);
									collDesc.pixelShader = shader;
									lock.lockWrite();
									pixelShaders.emplace(hash, std::move(shader));
									lock.unlock();
									threadHLSLOutput.resize(header.getSize());
								}
							}

							{
								ReadWriteLock<Mutex> lock(shaderMapsMutexes[2]);
								lock.lockRead();
								if (temp.computeShader.isPresent && computeShaders.count(hash) == 0)
								{
									lock.unlock();
									compData.entrypoint = pass.computeShader.func;
									compData.version = MathHelper::getShaderVersionFromFloat(pass.computeShader.version);

									switch (usedAPI)
									{
									case GraphicsAPIInfo::GraphicsAPI::DirectX:
										threadHLSLOutput += gsilShader.toHLSL(pass, variant.first, ShaderType::ComputeShader);
										break;
									}

									SharedPtr<ComputeShaderImpl> shader(createGraphicsObject<ComputeShaderImpl>());
									shader->compileFromCode(threadHLSLOutput, device, compData);
									collDesc.computeShader = shader;
									lock.lockWrite();
									computeShaders.emplace(hash, std::move(shader));
									lock.unlock();
									threadHLSLOutput.resize(header.getSize());
								}
							}

							{
								ReadWriteLock<Mutex> lock(shaderMapsMutexes[3]);
								lock.lockRead();
								if (temp.geometryShader.isPresent && geometryShaders.count(hash) == 0)
								{
									lock.unlock();
									compData.entrypoint = pass.geometryShader.func;
									compData.version = MathHelper::getShaderVersionFromFloat(pass.geometryShader.version);

									switch (usedAPI)
									{
									case GraphicsAPIInfo::GraphicsAPI::DirectX:
										threadHLSLOutput += gsilShader.toHLSL(pass, variant.first, ShaderType::GeometryShader);
										break;
									}

									SharedPtr<GeometryShaderImpl> shader(createGraphicsObject<GeometryShaderImpl>());
									shader->compileFromCode(threadHLSLOutput, device, compData);
									collDesc.geometryShader = shader;
									lock.lockWrite();
									geometryShaders.emplace(hash, std::move(shader));
									lock.unlock();
									threadHLSLOutput.resize(header.getSize());
								}
							}

							Hash tempHash = collDesc.getHash();
							{
								ReadWriteLock<Mutex> lock(shaderMapsMutexes[4]);
								lock.lockRead();
								if (colls.count(hash) == 0)
								{
									lock.unlock();
									lock.lockWrite();
									result.shaderColls.emplace_back(tempHash, getInstance<GPUStuffFactory<ShaderCollectionImpl>>().create(collDesc));
									colls.emplace(tempHash, static_cast<unsigned int>(result.shaderColls.size() - 1));
								}

								temp.variants.at(variant.first) = colls.at(tempHash);
							}
						}, std::cref(variant));
						++variantId;
					}

					int inputLayoutToChoose = -1;

					compileResults[0].wait();

					if (!pass.vertexShader.func.isEmpty())
					{
						ReadWriteLock<Mutex> lock(shaderMapsMutexes[4]);
						lock.lockRead();
						GlowAssert(!colls.empty());
						GlowAssert(colls.begin()->second < result.shaderColls.size());
						const VertexShaderImpl& usedVertexShader = *(result.shaderColls[colls.begin()->second].second->getDesc().vertexShader);
						lock.unlock();
						const GSILShader::Function* tempFunc = nullptr;

						for (const auto& fun : gsilShader.functions)
						{
							if (fun.name == pass.vertexShader.func)
							{
								tempFunc = &fun;
								break;
							}
						}
					}

					temp.blendStateId = z.second.blendStateId;
					temp.dssId = z.second.dssId;
					temp.rastId = z.second.rastId;
					temp.inputLayoutId = z.second.inputLayoutId;

					for (const auto& waitResult : compileResults)
					{
						waitResult.wait();
					}

					technique.passes.emplace(z.first, std::move(temp));
				}
			}
		}
	}

	return result;
}

void GLOWE::Effect::Pass::apply(const Hash& variant, BasicRendererImpl & renderer)
{
	if (blendStateId >= 0)
	{
		renderer.setBlendState(parent->blendStates[blendStateId].get());
	}
	else
	{
		renderer.setBlendState(nullptr);
	}

	if (dssId >= 0)
	{
		renderer.setDepthStencilState(parent->depthStencilStates[dssId].get());
	}
	else
	{
		renderer.setDepthStencilState(nullptr);
	}

	if (rastId >= 0)
	{
		renderer.setRasterizerState(parent->rastStates[rastId].get());
	}
	else
	{
		renderer.setRasterizerState(nullptr);
	}

	{
		int shaderCollId = -1;
		const auto it = variants.find(variant);
		if (it != variants.end())
		{
			shaderCollId = it->second;
		}

		if (shaderCollId >= 0)
		{
			auto& tempPair = parent->shaderColls[shaderCollId];
			if (!tempPair.second)
			{
				// May cause stalls.
				tempPair.second = getInstance<ResourceMgr>().load<ShaderCollectionImpl>(getInstance<ShaderCacheImpl>().getCollectionFileHash(tempPair.first), tempPair.first);
			}
			renderer.setShaderCollection(tempPair.second.getResource().get());
		}
		else
		{
			renderer.setShaderCollection(nullptr);
		}
	}
}

const GLOWE::InputLayoutDesc& GLOWE::Effect::Pass::getInputLayout() const
{
	if (inputLayoutId < 0)
	{
		if (vertexShader.isPresent)
		{
			throw InputLayoutUnavailableException("Vertex shader does not have an input layout.");
		}
		else
		{
			throw InputLayoutUnavailableException("The pass can't have an input layout as its vertex shader is not present.");
		}
	}

	return parent->inputLayouts[inputLayoutId];
}

template<>
GLOWE::ResourceHandle<GLOWE::Effect> GLOWE::ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToRetrieve)
{
	MemoryLoadSaveHandle handle(memory, size);
	ResourceHandle<Effect> result = makeSharedPtr<Effect>();

	UInt32 tempUInt32{};
	
	// DSS.
	handle >> tempUInt32;
	result->depthStencilStates.resize(tempUInt32);
	for (auto& x : result->depthStencilStates)
	{
		DepthStencilDesc desc;
		handle >> desc;
		x = getInstance<GPUStuffFactory<DepthStencilStateImpl>>().create(desc, *graphicsDevice);
	}

	// Blend states.
	handle >> tempUInt32;
	result->blendStates.resize(tempUInt32);
	for (auto& x : result->blendStates)
	{
		BlendDesc desc;
		handle >> desc;
		x = getInstance<GPUStuffFactory<BlendStateImpl>>().create(desc, *graphicsDevice);
	}

	// Rasterizer states.
	handle >> tempUInt32;
	result->rastStates.resize(tempUInt32);
	for (auto& x : result->rastStates)
	{
		RasterizerDesc desc;
		handle >> desc;
		x = getInstance<GPUStuffFactory<RasterizerStateImpl>>().create(desc, *graphicsDevice);
	}

	// Samplers.
	handle >> tempUInt32;
	result->samplers.resize(tempUInt32);
	for (auto& x : result->samplers)
	{
		SamplerDesc desc;
		handle >> desc;
		x = getInstance<GPUStuffFactory<SamplerImpl>>().create(desc, *graphicsDevice);
	}

	// Textures.
	handle >> tempUInt32;
	result->textures.resize(tempUInt32);
	for (auto& x : result->textures)
	{
		String tempStr;
		handle >> tempStr >> x.second;
		x.first = tempStr;
	}

	// UAVs.
	handle >> tempUInt32;
	result->uavs.resize(tempUInt32); // Changed from reserve.
	for (auto& x : result->uavs)
	{
		String tempStr;
		handle >> tempStr >> x.second;
		x.first = tempStr;
	}

	// Input layouts.
	handle >> tempUInt32;
	result->inputLayouts.resize(tempUInt32);
	for (auto& x : result->inputLayouts)
	{
		handle >> x;
	}

	// Cbuffers.
	handle >> tempUInt32;
	result->cbuffers.resize(tempUInt32);
	for (auto& x : result->cbuffers)
	{
		handle >> x.first >> x.second;
	}

	// Technique groups.

	UInt32 groups{}, techs{}, passes{};
	Hash groupHash, techHash;
	handle >> groups;
	for (unsigned int x = 0; x < groups; ++x)
	{
		handle >> groupHash >> techs;
		for (unsigned int y = 0; y < techs; ++y)
		{
			UInt32 tagsCount{};

			handle >> techHash >> tagsCount;
			Effect::Technique& currentTechnique = result->techniqueGroups[groupHash][techHash];
			currentTechnique.tags.resize(tagsCount);
			for (auto& tag : currentTechnique.tags)
			{
				handle >> tag;
			}

			handle >> passes;
			for (unsigned int z = 0; z < passes; ++z)
			{
				Hash hash;
				UInt32 blendId{}, dssId{}, rastId{}, variantsSize{};
				handle >> tempUInt32 >> blendId >> dssId >> rastId;

				//Effect veryTempEffect;
				Effect::Pass& tempPass = currentTechnique.passes.emplace(std::piecewise_construct, std::forward_as_tuple(tempUInt32), std::forward_as_tuple(result.getResource().get())).first->second;
				tempPass.blendStateId = blendId;
				tempPass.dssId = dssId;
				tempPass.rastId = rastId;
				//tempPass.shaderCollId = result->shaderColls.size();

				auto processShader = [](Effect::Pass::ShaderDesc& shaderDesc, LoadSaveHandle& handle)
				{
					UInt32 tempUInt32{};
					UInt64 tempUInt64{};

					handle >> tempUInt64;
					handle.skipBytes(sizeof(float) + tempUInt64);
					shaderDesc.isPresent = (tempUInt64 > 0);

					handle >> tempUInt32;
					for (unsigned int x = 0; x < tempUInt32; ++x)
					{
						handle >> tempUInt64;
						handle.skipBytes(tempUInt64);
					}

					// CBuffers.
					handle >> tempUInt32;
					shaderDesc.usedCBuffers.resize(tempUInt32);
					for (auto& x : shaderDesc.usedCBuffers)
					{
						handle >> tempUInt32;
						x = tempUInt32;
					}
					// UAVs.
					handle >> tempUInt32;
					shaderDesc.usedUAVs.resize(tempUInt32);
					for (auto& x : shaderDesc.usedUAVs)
					{
						handle >> tempUInt32;
						x = tempUInt32;
					}
					// Textures.
					handle >> tempUInt32;
					shaderDesc.usedTextures.resize(tempUInt32);
					shaderDesc.usedSamplers.resize(tempUInt32);
					for (auto& x : shaderDesc.usedTextures)
					{
						handle >> tempUInt32;
						x = tempUInt32;
					}
					for (auto& x : shaderDesc.usedSamplers)
					{
						handle >> tempUInt32;
						x = tempUInt32;
					}
				};

				// Ignore (but also partially load) the rest of data.
					// Compute.
				processShader(tempPass.computeShader, handle);
				// Vertex.
				processShader(tempPass.vertexShader, handle);
				// Geometry.
				processShader(tempPass.geometryShader, handle);
				// Pixel.
				processShader(tempPass.pixelShader, handle);

				handle >> variantsSize;
				for (unsigned int w = 0; w < variantsSize; ++w)
				{
					Hash realVariantHash;
					handle >> hash >> realVariantHash;
					handle.skipBytes(trueSizeof<Hash>() * 4);
					tempPass.variants.emplace(realVariantHash, static_cast<unsigned int>(result->shaderColls.size()));
					result->shaderColls.emplace_back(hash, nullptr);
				}
			}
		}
	}

	return result;
}

void GLOWE::Effect::CBuffer::serialize(LoadSaveHandle & handle) const
{
	UInt32 tempUInt32 = variables.size();
	handle << layout << tempUInt32;
	for (const auto& x : variables)
	{
		handle << x.first << x.second.first;
		tempUInt32 = x.second.second.size();
		handle << tempUInt32;
		for (const auto& y : x.second.second)
		{
			handle << y.first << y.second;
		}
	}
}

void GLOWE::Effect::CBuffer::deserialize(LoadSaveHandle & handle)
{
	UInt32 tempInt, tempInt2;
	Hash tempHash;
	GSILShader::Property::Type propertyType;

	handle >> layout >> tempInt;
	for (unsigned int x = 0; x < tempInt; ++x)
	{
		handle >> tempHash >> propertyType >> tempInt2;
		
		auto& constraint = variables[tempHash];
		constraint.first = propertyType;

		if (tempInt2 > 0)
		{
			Vector<Pair<Hash, String>>& vec = constraint.second;
			vec.resize(tempInt2);
			for (auto& y : vec)
			{
				handle >> y.first >> y.second;
			}
		}
	}
}

template<>
GLOWE::ResourceHandle<GLOWE::Effect> GLOWE::ResourceMgr::getDefaultResource()
{
	ResourceHandle<Effect> result = makeSharedPtr<Effect>();

	return result;
}
