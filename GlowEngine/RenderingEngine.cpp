#include "RenderingEngine.h"
#include "Renderable.h"
#include "../GlowGraphics/Uniform/Cullable.h"
#include "../GlowGraphics/Uniform/LightingMgr.h"
#include "Engine.h"

void GLOWE::RenderingEngine::prepareRenderPass(BasicRendererImpl& renderer, const RenderPass& rp, MaterialEnvironment& matEnv)
{
	if (rp.callbacks.prepareMatEnvFunc)
	{
		rp.callbacks.prepareMatEnvFunc(matEnv);
	}
	
	prepareRenderPass(renderer, rp);
}

void GLOWE::RenderingEngine::prepareRenderPass(BasicRendererImpl& renderer, const RenderPass& rp)
{
	renderer.setRenderTarget(rp.target);
	rp.callbacks.clearTargetFunc(renderer);
	renderer.setViewport(rp.callbacks.getViewportFunc());
}

void GLOWE::RenderingEngine::updateEngineConstants()
{
	Engine& engine = getInstance<Engine>();
	const Vector4F currTime = engine.getTimeFromStart().asSeconds(), deltaTime = engine.getDeltaTime().asSeconds(), mults(1.0f, 2.0f, 4.0f, 0.25f);

	env.setVariable("Time", (currTime * mults).toFloat4());
	env.setVariable("Delta Time", (deltaTime * mults).toFloat4());
}

GLOWE::RenderingEngine::RenderPassHandle GLOWE::RenderingEngine::addRenderPass(RenderTargetImpl& renderTarget, const Vector<unsigned int>& notRendered, const int id, const RenderPass::Callbacks& callbacks, const RenderPass::Settings& settings)
{
	RenderPass pass;
	
	pass.target = &renderTarget;
	pass.notRendered.insert(notRendered.begin(), notRendered.end());
	pass.callbacks = callbacks;
	pass.settings = settings;

	return renderPasses.emplace(id, std::move(pass));
}

void GLOWE::RenderingEngine::removeRenderPass(const RenderPassHandle& it)
{
	renderPasses.erase(it);
}

GLOWE::RenderingEngine::RenderableHandle GLOWE::RenderingEngine::addRenderable(Renderable* renderable)
{
	List<Renderable*>& list = renderables[renderable->getMaterialPtr()];
	list.push_back(renderable);
	return std::prev(list.end());
}

void GLOWE::RenderingEngine::removeRenderable(const RenderableHandle& handle)
{
	renderables.at((*handle)->getMaterialPtr()).erase(handle);
}

GLOWE::RenderingEngine::OnRenderCallbackHandle GLOWE::RenderingEngine::addRenderCallback(OnRenderCallback&& callback)
{
	switch (callback.when)
	{
	case OnRenderCallback::When::Before:
		beforeRenderCallbacks.emplace_back(std::move(callback));
		return std::prev(beforeRenderCallbacks.end());
	case OnRenderCallback::When::After:
		afterRenderCallbacks.emplace_back(std::move(callback));
		return std::prev(afterRenderCallbacks.end());
	default:
		throw StringException("Invalid callback.when: " + toString(static_cast<unsigned int>(callback.when)));
	}
}

void GLOWE::RenderingEngine::removeRenderCallback(const OnRenderCallbackHandle& handle)
{
	switch (handle->when)
	{
	case OnRenderCallback::When::Before:
		beforeRenderCallbacks.erase(handle);
		break;
	case OnRenderCallback::When::After:
		afterRenderCallbacks.erase(handle);
		break;
	default:
		throw StringException("Invalid handle->when: " + toString(static_cast<unsigned int>(handle->when)));
	}
}

void GLOWE::RenderingEngine::render()
{
	RenderingMgr& renderMgr = getInstance<RenderingMgr>();

	CommandList commands;
	updateEngineConstants();

	for (const auto& pass : renderPasses)
	{
		if (!pass.second.callbacks.shouldRender())
		{
			continue;
		}

		prepareRenderPass(commands, pass.second, env);
		if (pass.second.settings.needsLightsCulling || pass.second.settings.needsRenderablesCulling)
		{
			Frustum frustum = pass.second.callbacks.getFrustumFunc();
			if (pass.second.settings.needsLightsCulling)
			{
				getInstance<LightingMgr>().performLightsCulling(frustum);
			}
			if (pass.second.settings.needsRenderablesCulling)
			{
				getInstance<CullingMgr>().performCulling(frustum);
			}
		}

		for (const auto& x : beforeRenderCallbacks)
		{
			x.callback(pass.second, commands);
		}

		if (pass.second.callbacks.renderOverrideSingleThread == nullptr)
		{
			for (auto& renderablesList : renderables)
			{
				Map<const Renderable*, Hash> hashes;
				renderablesList.second.sort([&hashes](const Renderable* a, const Renderable* b) -> bool
					{
						if (!a->checkIsActive())
						{
							return true;
						}

						if (!b->checkIsActive())
						{
							return false;
						}

						Map<const Renderable*, Hash>::const_iterator itA = hashes.find(a), itB = hashes.find(b);

						if (itA == hashes.end())
						{
							itA = hashes.emplace(a, a->getVariantHash()).first;
						}

						if (itB == hashes.end())
						{
							itB = hashes.emplace(a, a->getVariantHash()).first;
						}

						return itA->second < itB->second;
					});

				for (auto& renderable : renderablesList.second)
				{
					if (!renderable->isExcludedFromRendering(pass.second))
					{
						renderable->render(commands, env);
					}
				}

				renderMgr.addCommandList(std::move(commands));
				commands.clear();
			}
		}
		else
		{
			pass.second.callbacks.renderOverrideSingleThread(commands);
			renderMgr.addCommandList(std::move(commands));
			commands.clear();
		}

		for (const auto& x : afterRenderCallbacks)
		{
			x.callback(pass.second, commands);
		}
	}
}

void GLOWE::RenderingEngine::render(BasicRendererImpl& renderer)
{
	updateEngineConstants();

	for (const auto& pass : renderPasses)
	{
		prepareRenderPass(renderer, pass.second, env);
		if (pass.second.settings.needsLightsCulling || pass.second.settings.needsRenderablesCulling)
		{
			Frustum frustum = pass.second.callbacks.getFrustumFunc();
			if (pass.second.settings.needsLightsCulling)
			{
				getInstance<LightingMgr>().performLightsCulling(frustum);
			}
			if (pass.second.settings.needsRenderablesCulling)
			{
				getInstance<CullingMgr>().performCulling(frustum);
			}
		}

		for (const auto& x : beforeRenderCallbacks)
		{
			x.callback(pass.second, renderer);
		}
		if (pass.second.callbacks.renderOverrideSingleThread == nullptr)
		{
			for (auto& renderablesList : renderables)
			{
				Map<const Renderable*, Hash> hashes;
				renderablesList.second.sort([&hashes](const Renderable* a, const Renderable* b) -> bool
					{
						if (!a->checkIsActive())
						{
							return true;
						}

						if (!b->checkIsActive())
						{
							return false;
						}

						Map<const Renderable*, Hash>::const_iterator itA = hashes.find(a), itB = hashes.find(b);

						if (itA == hashes.end())
						{
							itA = hashes.emplace(a, a->getVariantHash()).first;
						}

						if (itB == hashes.end())
						{
							itB = hashes.emplace(a, a->getVariantHash()).first;
						}

						return itA->second < itB->second;
					});

				for (auto& renderable : renderablesList.second)
				{
					if (!renderable->isExcludedFromRendering(pass.second))
					{
						renderable->render(renderer, env);
					}
				}
			}
		}
		else
		{
			pass.second.callbacks.renderOverrideSingleThread(renderer);
		}

		for (const auto& x : afterRenderCallbacks)
		{
			x.callback(pass.second, renderer);
		}
	}
}

GLOWE::ThreadPool::TaskResult<void> GLOWE::RenderingEngine::scheduleRender(ThreadPool& threads)
{
	unsigned int passesCount = renderPasses.size() * (2 + renderables.size() + afterRenderCallbacks.size() + beforeRenderCallbacks.size());
	for (const auto& x : renderPasses)
	{
		if (x.second.callbacks.renderOverrideMultiThreaded)
		{
			passesCount -= renderables.size();
			passesCount += x.second.callbacks.renderOverrideGetCommandListsCount();
		}
	}

	UniqueLock<Mutex> lock(scheduleMutex);

	scheduleFrustums.clear();
	scheduleCommandLists.reserve(passesCount);
	//resultingScheduleCommandLists.reserve(passesCount);
	scheduleFrustums.reserve(renderPasses.size());

	updateEngineConstants();

	// Workers...
	for (auto& x : renderPasses)
	{
		resultingScheduleCommandLists.emplace_back();
		scheduleCommandLists.emplace_back(threads.addTask([](const RenderPass& pass, CommandList& commands)
		{
			prepareRenderPass(commands, pass);
		}, std::cref(x.second), std::ref(resultingScheduleCommandLists.back())));

		ThreadPool::SharedTaskResult<void> cullResult;

		if (x.second.settings.needsLightsCulling)
		{
			scheduleFrustums.emplace_back(x.second.callbacks.getFrustumFunc());
			if (x.second.settings.needsRenderablesCulling)
			{
				cullResult = getInstance<CullingMgr>().scheduleCullingAndLightCulling(threads, scheduleFrustums.back());
			}
			else
			{
				cullResult = getInstance<LightingMgr>().scheduleLightsCulling(threads, scheduleFrustums.back());
			}
		}
		else
		if (x.second.settings.needsRenderablesCulling)
		{
			scheduleFrustums.emplace_back(x.second.callbacks.getFrustumFunc());
			cullResult = getInstance<CullingMgr>().scheduleCullingAndLightCulling(threads, scheduleFrustums.back());
		}

		for (const auto& y : beforeRenderCallbacks)
		{
			resultingScheduleCommandLists.emplace_back();
			scheduleCommandLists.emplace_back(threads.addTask([](const RenderPass& pass, const OnRenderCallback& callback, CommandList& commands)
			{
				callback.callback(pass, commands);
			}, std::cref(x.second), std::cref(y), std::ref(resultingScheduleCommandLists.back())));
		}

		if (x.second.callbacks.renderOverrideMultiThreaded == nullptr)
		{
			x.second.matEnv = env;
			x.second.callbacks.prepareMatEnvFunc(x.second.matEnv);

			for (auto& renderablesList : renderables)
			{
				resultingScheduleCommandLists.emplace_back();
				scheduleCommandLists.emplace_back(threads.addTask([cullResult](const RenderPass& pass, Pair<Material* const, List<Renderable*>>& renderablesList, const MaterialEnvironment& matEnv, CommandList& commands)
					{
						thread_local MaterialEnvironment threadMatEnv;

						threadMatEnv = matEnv;
						Map<const Renderable*, Hash> hashes;
						renderablesList.second.sort([&hashes](const Renderable* a, const Renderable* b) -> bool
							{
								if (!a->checkIsActive())
								{
									return true;
								}

								if (!b->checkIsActive())
								{
									return false;
								}

								Map<const Renderable*, Hash>::const_iterator itA = hashes.find(a), itB = hashes.find(b);

								if (itA == hashes.end())
								{
									itA = hashes.emplace(a, a->getVariantHash()).first;
								}

								if (itB == hashes.end())
								{
									itB = hashes.emplace(a, a->getVariantHash()).first;
								}

								return itA->second < itB->second;
							});

						cullResult.wait();

						for (auto& renderable : renderablesList.second)
						{
							if (!renderable->isExcludedFromRendering(pass))
							{
								renderable->render(commands, threadMatEnv);
							}
						}
					}, std::cref(x.second), std::ref(renderablesList), std::cref(x.second.matEnv), std::ref(resultingScheduleCommandLists.back())));
			}
		}
		else
		{
			x.second.callbacks.renderOverrideMultiThreaded(threads, scheduleCommandLists, resultingScheduleCommandLists);
		}

		for (const auto& y : afterRenderCallbacks)
		{
			resultingScheduleCommandLists.emplace_back();
			scheduleCommandLists.emplace_back(threads.addTask([](const RenderPass& pass, const OnRenderCallback& callback, CommandList& commands)
			{
				callback.callback(pass, commands);
			}, std::cref(x.second), std::cref(y), std::ref(resultingScheduleCommandLists.back())));
		}
	}

	// ...and the collector, which we return.
	return threads.addTask([this]()
	{
		RenderingMgr& renderMgr = getInstance<RenderingMgr>();

		// We do everything we can before waiting for rendering's completion.
		{
			UniqueLock<Mutex> lock(scheduleMutex);

			for (unsigned int x = 0; x < resultingScheduleCommandLists.size(); ++x)
			{
				if (scheduleCommandLists[x].valid())
				{
					scheduleCommandLists[x].wait();
				}
				renderMgr.addCommandList(std::move(resultingScheduleCommandLists[x]));
			}

			scheduleCommandLists.clear();
			resultingScheduleCommandLists.clear();
		}

		renderMgr.waitForCompletion();
	});
}

GLOWE::RenderingEngine::RenderPass::Settings::Settings()
	: needsLightsCulling(true), needsRenderablesCulling(true)
{
}
