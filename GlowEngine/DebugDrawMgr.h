#pragma once
#ifndef GLOW_ENGINE_DEBUGDRAWMGR_INCLUDED
#define GLOW_ENGINE_DEBUGDRAWMGR_INCLUDED

#include "glowengine_export.h"

#include "Material.h"

#include "GUI/GUIRect.h"

#include "../GlowSystem/Utility.h"
#include "../GlowSystem/Color.h"
#include "../GlowSystem/Time.h"

#include "../GlowMath/VectorClasses.h"
#include "../GlowMath/Transform.h"

#include "../GlowGraphics/Uniform/GraphicsDeviceImplementation.h"
#include "../GlowGraphics/Uniform/BufferImplementation.h"

namespace GLOWE
{
	/// @brief Debug drawing facility. Allows the user to draw various debugging primitives on every render target in the rendering engine.
	class GLOWENGINE_EXPORT DebugDrawMgr
		: public Subsystem, public Lockable
	{
	private:
		enum class Type : unsigned char
		{
			Line,
			Cross,
			//Sphere,
			//Circle,
			//Axes,
			Triangle,
			Rect,

			Count
		};
		struct PrimitiveDef
		{
			unsigned char mask;
			Time endTime;
			Color color;
			union
			{
				struct
				{
					Float3 begin, end;
				} line;
				struct
				{
					Float3 position;
					float size;
				} cross;
				struct
				{
					Float3 position;
					float radius;
				} sphere;
				struct
				{
					Float3 center, planeNormal;
					float radius;
				} circle;
				struct
				{
					Transform transform;
				} axes;
				struct
				{
					Float3x3 vertices;
				} triangle;
				GUIRect rect;
			};

			inline PrimitiveDef()
				: mask(0), endTime(), color(), axes()
			{}

			inline bool operator<(const PrimitiveDef& right) const
			{
				return mask < right.mask;
			}
		};

		static constexpr unsigned int typesCount = static_cast<unsigned int>(Type::Count);
		/// @brief Amount of vertices the default not resized buffer can accomodate.
		static constexpr unsigned int defaultBufferSize = 32;
	private:
		Engine& engine;
		Vector<Pair<Multiset<PrimitiveDef>::const_iterator, Type>> toDeleteBuffer;
		GraphicsDeviceImpl* attachedGC;
		std::array<Multiset<PrimitiveDef>, typesCount> primitives;
		bool needsBufferUpdate;
		unsigned int lastPrimitiveUpdateFrameId;
		UniquePtr<BufferImpl> buffer;
		UniquePtr<InputLayoutImpl> inputLayout;
		Material normalMat, wireframeMat, noDepthMat, noDepthWireframeMat;
		unsigned int lineNormalSectionSize, lineNoDepthSectionSize;
		unsigned int triangleNormalSectionSize, triangleNoDepthSectionSize, triangleWireframeSectionSize, triangleBothSectionSize;
	private:
		void updateBuffer(BasicRendererImpl& renderer);
		void updatePrimitives();
	public:
		DebugDrawMgr();
		virtual ~DebugDrawMgr() = default;

		void attachToGC(GraphicsDeviceImpl* gc);

		void addLine(const Float3& begin, const Float3& end, const Color& color, const Time& duration = Time::ZeroTime, const bool depthTest = false);
		void addCross(const Float3& position, const Color& color, const float size, const Time& duration = Time::ZeroTime, const bool depthTest = false);
		// TODO: Add in the future (if needed).
		//void addSphere(const Float3& center, const float radius, const Color& color, const Time& duration = Time::ZeroTime, const bool depthTest = false, const bool wireframe = true);
		//void addCircle(const Float3& center, const Float3& planeNormal, const float radius, const Color& color, const Time& duration = Time::ZeroTime, const bool depthTest = false, const bool wireframe = true);
		//void addAxes(const Transform& transform, const Color& color, const Time& duration = Time::ZeroTime, const bool depthTest = false, const bool wireframe = true);
		void addTriangle(const Float3x3& vertices, const Color& color, const Time& duration = Time::ZeroTime, const bool depthTest = false, const bool wireframe = true);
		void addRect(const GUIRect& rect, const Color& color, const Time& duration = Time::ZeroTime, const bool depthTest = false);
	};
}

#endif