#include "DebugDrawMgr.h"
#include "Engine.h"
#include "RenderingEngine.h"
#include "Resource.h"

// Debug primitives drawing principle:
// First, the system draws lines that are drawn normally, i.e. with depth. Next, depth-disabled lines are drawn.
// Next are triangles - normal, without depth, wireframe with depth and, at last, wireframe without depth.

enum Mask : unsigned char
{
	None = 0,
	Depth = 1,
	Wireframe = 1 << 1,
	Both = Depth | Wireframe
};

constexpr inline unsigned char boolsToMask(const bool depth, const bool wireframe)
{
	return (depth ? Depth : None) | (wireframe ? Wireframe : None);
}

struct Vertex
{
	GLOWE::Float3 pos;
	GLOWE::Float4 color;
};

static_assert(sizeof(Vertex) == (7 * sizeof(float)), "Vertex must be 7 floats in size");

void GLOWE::DebugDrawMgr::updateBuffer(BasicRendererImpl& renderer)
{
#if defined(_DEBUG) || defined(GLOWE_ENABLE_DEBUG_DRAW)
	lineNormalSectionSize = lineNoDepthSectionSize = triangleBothSectionSize = triangleNoDepthSectionSize = triangleNormalSectionSize = triangleWireframeSectionSize = 0;
	Vector<Vertex> tempBuffer, finalBuffer;
	finalBuffer.reserve(buffer->getSize() / sizeof(Vertex));

	// TODO: Optimize.
	for (unsigned int x = 0; x < typesCount; ++x)
	{
		const Type type = static_cast<Type>(x);
		const auto& container = primitives[x];
		for (const auto& primitive : container)
		{
			bool isLine = false;
			switch (type)
			{
			case Type::Line:
			{
				isLine = true;
				tempBuffer.resize(2);
				tempBuffer[0].pos = Float3{ primitive.line.begin[0], primitive.line.begin[1], primitive.line.begin[2] };
				tempBuffer[1].pos = Float3{ primitive.line.end[0], primitive.line.end[1], primitive.line.end[2] };
			}
				break;
			case Type::Cross:
			{
				isLine = true;
				tempBuffer.resize(6);
				Vector3F center(primitive.cross.position[0], primitive.cross.position[1], primitive.cross.position[2]);
				tempBuffer[0].pos = (center + (primitive.cross.size * Vector3F(1.0f, 0, 0))).toFloat3();
				tempBuffer[1].pos = (center - (primitive.cross.size * Vector3F(1.0f, 0, 0))).toFloat3();

				tempBuffer[2].pos = (center + (primitive.cross.size * Vector3F(0, 1.0f, 0))).toFloat3();
				tempBuffer[3].pos = (center - (primitive.cross.size * Vector3F(0, 1.0f, 0))).toFloat3();

				tempBuffer[4].pos = (center + (primitive.cross.size * Vector3F(0, 0, 1.0f))).toFloat3();
				tempBuffer[5].pos = (center - (primitive.cross.size * Vector3F(0, 0, 1.0f))).toFloat3();
			}
				break;
			case Type::Triangle:
			{
				tempBuffer.resize(3);
				for (unsigned int x = 0; x < 3; ++x)
				{
					tempBuffer[x].pos = primitive.triangle.vertices[x];
				}
			}
				break;
			case Type::Rect:
			{
				isLine = true;
				tempBuffer.resize(8);
				const Float2 size = primitive.rect.getScaledSize();
				tempBuffer[0].pos = Float3{ primitive.rect.pos[0], primitive.rect.pos[1], 0.0f };
				tempBuffer[1].pos = Float3{ primitive.rect.pos[0] + size[0], primitive.rect.pos[1], 0.0f };

				tempBuffer[2].pos = Float3{ primitive.rect.pos[0] + size[0], primitive.rect.pos[1], 0.0f };
				tempBuffer[3].pos = Float3{ primitive.rect.pos[0] + size[0], primitive.rect.pos[1] + size[1], 0.0f };

				tempBuffer[4].pos = Float3{ primitive.rect.pos[0] + size[0], primitive.rect.pos[1] + size[1], 0.0f };
				tempBuffer[5].pos = Float3{ primitive.rect.pos[0], primitive.rect.pos[1] + size[1], 0.0f };

				tempBuffer[6].pos = Float3{ primitive.rect.pos[0], primitive.rect.pos[1] + size[1], 0.0f };
				tempBuffer[7].pos = Float3{ primitive.rect.pos[0], primitive.rect.pos[1], 0.0f };
			}
				break;
			default:
				throw Exception("Invalid (not supported) debug primitive");
			}

			for (auto& x : tempBuffer)
			{
				x.color = primitive.color;
			}

			unsigned int offset;
			if (isLine)
			{
				if (primitive.mask & Depth)
				{
					offset = lineNormalSectionSize;
					lineNormalSectionSize += tempBuffer.size();
				}
				else
				{
					offset = lineNormalSectionSize + lineNoDepthSectionSize;
					lineNoDepthSectionSize += tempBuffer.size();
				}
			}
			else
			{
				offset = lineNormalSectionSize + lineNoDepthSectionSize;
				switch (primitive.mask)
				{
				case None:
					offset += triangleNormalSectionSize;
					triangleNormalSectionSize += tempBuffer.size();
					break;
				case Depth:
					offset += triangleNormalSectionSize + triangleNoDepthSectionSize;
					triangleNoDepthSectionSize += tempBuffer.size();
					break;
				case Wireframe:
					offset += triangleNormalSectionSize + triangleNoDepthSectionSize + triangleWireframeSectionSize;
					triangleWireframeSectionSize += tempBuffer.size();
					break;
				case Both:
					offset += triangleNormalSectionSize + triangleNoDepthSectionSize + triangleWireframeSectionSize + triangleBothSectionSize;
					triangleBothSectionSize += tempBuffer.size();
					break;
				}
			}

			finalBuffer.insert(finalBuffer.begin() + offset, tempBuffer.begin(), tempBuffer.end());
		}
	}

	if ((sizeof(Vertex) * finalBuffer.size()) > buffer->getSize())
	{
		const auto desc = buffer->getDesc();
		buffer->destroy();
		buffer->create(desc, sizeof(Vertex) * finalBuffer.size(), *attachedGC, finalBuffer.data());
	}
	else
	{
		renderer.updateBuffer(buffer.get(), finalBuffer.data(), sizeof(Vertex)* finalBuffer.size());
	}

	needsBufferUpdate = false;
#endif
}

void GLOWE::DebugDrawMgr::updatePrimitives()
{
#if defined(_DEBUG) || defined(GLOWE_ENABLE_DEBUG_DRAW)
	for (const auto& x : toDeleteBuffer)
	{
		primitives[static_cast<unsigned char>(x.second)].erase(x.first);
	}
	if (!toDeleteBuffer.empty())
	{
		needsBufferUpdate = true;
	}
	toDeleteBuffer.clear();
	const Time timeNow = engine.getTimeFromStart();
	for (unsigned int x = 0; x < typesCount; ++x)
	{
		const auto& container = primitives[x];
		for (auto y = container.begin(); y != container.end(); ++y)
		{
			if (timeNow >= y->endTime)
			{
				toDeleteBuffer.emplace_back(std::make_pair(y, static_cast<Type>(x)));
			}
		}
	}
	lastPrimitiveUpdateFrameId = engine.getCurrentFrameId();
#endif
}

GLOWE::DebugDrawMgr::DebugDrawMgr()
	: engine(getInstance<Engine>()), toDeleteBuffer(), attachedGC(nullptr), needsBufferUpdate(false), lastPrimitiveUpdateFrameId(std::numeric_limits<unsigned int>::max()), buffer(), normalMat(), wireframeMat(), noDepthMat(), noDepthWireframeMat(), primitives(), lineNoDepthSectionSize(0), lineNormalSectionSize(0), triangleBothSectionSize(0), triangleNoDepthSectionSize(0), triangleNormalSectionSize(0), triangleWireframeSectionSize(0)
{
}

void GLOWE::DebugDrawMgr::attachToGC(GraphicsDeviceImpl* gc)
{
#if !defined(_DEBUG) && !defined(GLOWE_ENABLE_DEBUG_DRAW)
	(void)gc;
#else
	if (attachedGC)
	{
		throw Exception("Making multiple calls to attachToGC is not allowed");
	}
	if (gc == nullptr)
	{
		throw Exception("GC cannot be a nullptr when calling attachToGC");
	}
	attachedGC = gc;
	getInstance<RenderingEngine>().addRenderCallback(RenderingEngine::OnRenderCallback([this](const RenderingEngine::RenderPass& pass, BasicRendererImpl& renderer)
	{
		// TODO: Maybe restrict this guard a little?
		Lockable::Guard guard(*this);
		if (engine.getCurrentFrameId() != lastPrimitiveUpdateFrameId)
		{
			updatePrimitives();
		}

		if (needsBufferUpdate)
		{
			updateBuffer(renderer);
		}

		// No primitives to draw.
		if ((lineNormalSectionSize + lineNoDepthSectionSize + triangleNormalSectionSize + triangleNoDepthSectionSize + triangleWireframeSectionSize + triangleBothSectionSize) == 0)
		{
			return;
		}

		renderer.setVertexBuffer(buffer.get());
		renderer.setInputLayout(inputLayout.get());
		if ((lineNormalSectionSize + triangleNormalSectionSize) > 0)
		{
			normalMat.applyEnvironment(pass.matEnv);
			normalMat.apply(0, Hash(), renderer);

			if (lineNormalSectionSize > 0)
			{
				renderer.setPrimitiveTopology(BasicRendererImpl::PrimitiveTopology::LineList);
				renderer.draw(lineNormalSectionSize, 0);
			}
			if (triangleNormalSectionSize > 0)
			{
				renderer.setPrimitiveTopology(BasicRendererImpl::PrimitiveTopology::TriangleList);
				renderer.draw(triangleNormalSectionSize, lineNormalSectionSize + lineNoDepthSectionSize);
			}
		}

		if ((lineNoDepthSectionSize + triangleNoDepthSectionSize) > 0)
		{
			noDepthMat.applyEnvironment(pass.matEnv);
			noDepthMat.apply(0, Hash(), renderer);

			if (lineNoDepthSectionSize > 0)
			{
				renderer.setPrimitiveTopology(BasicRendererImpl::PrimitiveTopology::LineList);
				renderer.draw(lineNoDepthSectionSize, lineNormalSectionSize);
			}
			if (triangleNoDepthSectionSize > 0)
			{
				renderer.setPrimitiveTopology(BasicRendererImpl::PrimitiveTopology::TriangleList);
				renderer.draw(triangleNoDepthSectionSize, lineNormalSectionSize + lineNoDepthSectionSize + triangleNormalSectionSize);
			}
		}

		if (triangleWireframeSectionSize > 0)
		{
			wireframeMat.applyEnvironment(pass.matEnv);
			wireframeMat.apply(0, Hash(), renderer);

			renderer.setPrimitiveTopology(BasicRendererImpl::PrimitiveTopology::TriangleList);
			renderer.draw(triangleWireframeSectionSize, lineNormalSectionSize + lineNoDepthSectionSize + triangleNormalSectionSize + triangleNoDepthSectionSize);
		}

		if (triangleBothSectionSize > 0)
		{
			noDepthWireframeMat.applyEnvironment(pass.matEnv);
			noDepthWireframeMat.apply(0, Hash(), renderer);

			renderer.setPrimitiveTopology(BasicRendererImpl::PrimitiveTopology::TriangleList);
			renderer.draw(triangleBothSectionSize, lineNormalSectionSize + lineNoDepthSectionSize + triangleNormalSectionSize + triangleNoDepthSectionSize + triangleWireframeSectionSize);
		}
	}, RenderingEngine::OnRenderCallback::When::After));
	buffer = createGraphicsObject<BufferImpl>();
	BufferDesc desc;
	desc.binds = BufferDesc::Binding::VertexBuffer;
	desc.cpuAccess = CPUAccess::Write;
	desc.usage = Usage::Dynamic;
	desc.layout.addElement(Format(Format::Type::Float, 3));
	desc.layout.addElement(Format(Format::Type::Float, 4));

	buffer->create(desc, sizeof(Vertex) * defaultBufferSize, *gc);

	const auto effect = loadResource<Effect>("GlowCore/Shaders/StandardDebug.geff", "Effect");
	Material::createFromEffect(normalMat, *gc, effect, "Default", "DebugDraw");
	Material::createFromEffect(noDepthMat, *gc, effect, "NoDepth", "DebugDraw");
	Material::createFromEffect(wireframeMat, *gc, effect, "Wireframe", "DebugDraw");
	Material::createFromEffect(noDepthWireframeMat, *gc, effect, "NoDepthWireframe", "DebugDraw");

	InputLayoutDesc layoutDesc;
	layoutDesc.addVertexElement("Position", desc.layout.getElem(0));
	layoutDesc.addVertexElement("Color", desc.layout.getElem(1));

	inputLayout = createGraphicsObject<InputLayoutImpl>();
	inputLayout->create(layoutDesc, *gc, *normalMat.getShaderCollection(0, Hash()).getDesc().vertexShader);
#endif
}

void GLOWE::DebugDrawMgr::addLine(const Float3& begin, const Float3& end, const Color& color, const Time& duration, const bool depthTest)
{
#if defined(_DEBUG) || defined(GLOWE_ENABLE_DEBUG_DRAW)
	PrimitiveDef primitive;
	primitive.line.begin = begin;
	primitive.line.end = end;
	primitive.endTime = engine.getTimeFromStart() + duration;
	primitive.mask = boolsToMask(depthTest, false);
	primitive.color = color;
	primitives[static_cast<unsigned char>(Type::Line)].emplace(primitive);
	needsBufferUpdate = true;
#endif
}

void GLOWE::DebugDrawMgr::addCross(const Float3& position, const Color& color, const float size, const Time& duration, const bool depthTest)
{
#if defined(_DEBUG) || defined(GLOWE_ENABLE_DEBUG_DRAW)
	PrimitiveDef primitive;
	primitive.cross.position = position;
	primitive.cross.size = size;
	primitive.endTime = engine.getTimeFromStart() + duration;
	primitive.mask = boolsToMask(depthTest, false);
	primitive.color = color;
	primitives[static_cast<unsigned char>(Type::Cross)].emplace(primitive);
	needsBufferUpdate = true;
#endif
}

void GLOWE::DebugDrawMgr::addTriangle(const Float3x3& vertices, const Color& color, const Time& duration, const bool depthTest, const bool wireframe)
{
#if defined(_DEBUG) || defined(GLOWE_ENABLE_DEBUG_DRAW)
	PrimitiveDef primitive;
	primitive.triangle.vertices = vertices;
	primitive.endTime = engine.getTimeFromStart() + duration;
	primitive.mask = boolsToMask(depthTest, wireframe);
	primitive.color = color;
	primitives[static_cast<unsigned char>(Type::Triangle)].emplace(primitive);
	needsBufferUpdate = true;
#endif
}

void GLOWE::DebugDrawMgr::addRect(const GUIRect& rect, const Color& color, const Time& duration, const bool depthTest)
{
#if defined(_DEBUG) || defined(GLOWE_ENABLE_DEBUG_DRAW)
	PrimitiveDef primitive;
	primitive.rect = rect;
	primitive.endTime = engine.getTimeFromStart() + duration;
	primitive.mask = boolsToMask(depthTest, false);
	primitive.color = color;
	primitives[static_cast<unsigned char>(Type::Rect)].emplace(primitive);
	needsBufferUpdate = true;
#endif
}
