#include "EngineGraphicsFactory.h"

template<>
GLOWE::EngineGraphicsFactory& GLOWE::OneInstance<GLOWE::EngineGraphicsFactory>::instance()
{
	static EngineGraphicsFactory& result = dynamic_cast<EngineGraphicsFactory&>(getInstance<GraphicsFactoryImpl>());
	return result;
}

template<>
GLOWE::UniquePtr<GLOWE::ShaderCacheImpl> GLOWE::createGraphicsObject()
{
	return getInstance<EngineGraphicsFactory>().createShaderCache();
}
