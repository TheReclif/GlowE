#pragma once
#ifndef GLOWE_ENGINE_RESOURCELOADERS_INCLUDED
#define GLOWE_ENGINE_RESOURCELOADERS_INCLUDED

#include "Resource.h"

#include "../GlowGraphics/Uniform/Geometry.h"
#include "../GlowGraphics/Uniform/ShaderCollectionImplementation.h"

namespace GLOWE
{
	// Shader collection.
	template<>
	GLOWENGINE_EXPORT ResourceHandle<ShaderCollectionImpl> ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToRetrieve);
	template<>
	GLOWENGINE_EXPORT ResourceHandle<ShaderCollectionImpl> ResourceMgr::getDefaultResource();
	template<>
	GLOWENGINE_EXPORT ResourceHandle<ShaderCollectionImpl> ResourceMgr::preLoad(const Hash&, const Hash& hash, const Map<Hash, ResourceHandle<void>>&);

	// Texture.
	template<>
	GLOWENGINE_EXPORT ResourceHandle<TextureImpl> ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash&);
	template<>
	GLOWENGINE_EXPORT ResourceHandle<TextureImpl> GLOWE::ResourceMgr::getDefaultResource();

	// Geometry.
	template<>
	GLOWENGINE_EXPORT ResourceHandle<Geometry> ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToRetrieve);
	template<>
	GLOWENGINE_EXPORT ResourceHandle<Geometry> ResourceMgr::getDefaultResource();
	template<>
	GLOWENGINE_EXPORT ResourceHandle<GeometryBuffers> ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToRetrieve);
	template<>
	GLOWENGINE_EXPORT ResourceHandle<GeometryBuffers> ResourceMgr::getDefaultResource();
}

#endif
