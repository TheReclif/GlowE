#pragma once
#ifndef GLOWE_ENGINE_FPRCOUNTER_INCLUDED
#define GLOWE_ENGINE_FPRCOUNTER_INCLUDED

#include "Engine.h"
#include "ScriptableSubsystem.h"

namespace GLOWE
{
	/// @brief A utility that can be used to count FPS and time per frame for diagnostic purposes. To use it, just call getInstance on FPSCounterUtility. This call will also initialize the FPS counter as it's not initialized until its first use. The default sampling time is 1 second.
	class GLOWENGINE_EXPORT FPSCounterUtility
		: public Subsystem, public ScriptableSubsystem
	{
	private:
		unsigned int frames;
		Time time, currentTimePerFrame, samplingTime;
		Clock frameClock;
		float currentFPS;
	protected:
		virtual void update() override;
	public:
		/// @brief Registers the scriptable subsystem in the Engine.
		FPSCounterUtility();

		/// @brief Sets the frame sampling time, e.g. if set to 1 second, the subsystem counts frames for at least 1 second and averages them to obtain FPS.
		/// @param newSaTime New sampling time.
		void setSamplingTime(const Time& newSaTime);

		/// @brief Returns the average FPS.
		/// @return Currently averaged FPS.
		float getCurrentFPS() const;
		/// @brief Returns the average time per frame.
		/// @return Currently averaged time per frame.
		Time getCurrentTimePerFrame() const;
	};
}

#endif
