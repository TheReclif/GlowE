#pragma once
#ifndef GLOWE_ENGINE_EFFECT_INCLUDED
#define GLOWE_ENGINE_EFFECT_INCLUDED

#include "../GlowGraphics/Uniform/GSILShaderCompiler.h"
#include "../GlowGraphics/Uniform/ShaderCollectionImplementation.h"
#include "../GlowGraphics/Uniform/DepthStencilStateImplementation.h"
#include "../GlowGraphics/Uniform/BlendStateImplementation.h"
#include "../GlowGraphics/Uniform/RasterizerStateImplementation.h"
#include "../GlowGraphics/Uniform/SamplerImplementation.h"
#include "../GlowGraphics/Uniform/TextureImplementation.h"
#include "../GlowGraphics/Uniform/GPUStaffFactory.h"
#include "../GlowGraphics/Uniform/InputLayoutImplementation.h"
#include "../GlowGraphics/Uniform/GraphicsDeviceImplementation.h"

#include "Resource.h"

namespace GLOWE
{
	class GLOWENGINE_EXPORT Effect
	{
	public:
		class GLOWENGINE_EXPORT InputLayoutUnavailableException
			: public Exception
		{
		public:
			using Exception::Exception;
		};

		class GLOWENGINE_EXPORT Pass
		{
		public:
			struct ShaderDesc
			{
				Vector<unsigned int> usedCBuffers, usedTextures, usedSamplers, usedUAVs;
				bool isPresent = false;
			};
		public:
			Effect* parent;
			int blendStateId, dssId, rastId, inputLayoutId;
			ShaderDesc pixelShader, vertexShader, geometryShader, computeShader;
			Map<Hash, unsigned int> variants;
		public:
			Pass() : parent(nullptr), blendStateId(-1), dssId(-1), rastId(-1), inputLayoutId(-1), variants() {};
			Pass(Effect* const arg) : parent(arg), blendStateId(-1), dssId(-1), rastId(-1), inputLayoutId(-1), variants() {};

			void apply(const Hash& variant, BasicRendererImpl& renderer);

			/// @brief Returns the pass' input layout. Throws InputLayoutUnavailableException if it's unavailable for some reason, eg. when the vertex shader is not bound (possible for compute shaders).
			/// @return Const reference to the aformentioned input layout. 
			const InputLayoutDesc& getInputLayout() const;
		};

		struct GLOWENGINE_EXPORT CBuffer
			: public BinarySerializable
		{
			HashedStructureLayout layout;
			Map<Hash, Pair<GSILShader::Property::Type, Vector<Pair<Hash, String>>>> variables;

			virtual void serialize(LoadSaveHandle& handle) const override;
			virtual void deserialize(LoadSaveHandle& handle) override;
		};

		class GLOWENGINE_EXPORT Technique
		{
		public:
			using Tag = GSILShader::Technique::Tag;
		public:
			Vector<Tag> tags;
			Map<unsigned int, Pass> passes;
		};
	private:
		Vector<Pair<Hash, ResourceHandle<ShaderCollectionImpl>>> shaderColls;
		Vector<SharedPtr<DepthStencilStateImpl>> depthStencilStates;
		Vector<SharedPtr<BlendStateImpl>> blendStates;
		Vector<SharedPtr<RasterizerStateImpl>> rastStates;
		Vector<SharedPtr<SamplerImpl>> samplers;

		Vector<InputLayoutDesc> inputLayouts;
		Vector<Pair<Hash, TextureDesc::Type>> textures;
		Vector<Pair<Hash, GSILShader::UAV::Type>> uavs;
		Vector<Pair<Hash, CBuffer>> cbuffers;

		Map<Hash, Map<Hash, Technique>> techniqueGroups;
	public:
		Effect() = default;
		~Effect() = default;

		Effect(const Effect& arg);
		Effect(Effect&& arg);

		const Pass& getPass(const Hash& techniqueName, const unsigned int passId, const Hash& techniqueGroupName = Hash()) const;
		const Technique& getTechnique(const Hash& techniqueName, const Hash& techniqueGroupName = Hash()) const;
		const Map<Hash, Technique>& getTechniqueGroup(const Hash& techniqueGroupName = Hash()) const;

		Pass& getPass(const Hash& techniqueName, const unsigned int passId, const Hash& techniqueGroupName = Hash());
		Technique& getTechnique(const Hash& techniqueName, const Hash& techniqueGroupName = Hash());
		Map<Hash, Technique>& getTechniqueGroup(const Hash& techniqueGroupName = Hash());

		static Effect createFromGSIL(const GSILShader& gsilShader, const GraphicsDeviceImpl& device);

		friend class ResourceMgr;
		friend class Material;
	};

	template<>
	GLOWENGINE_EXPORT ResourceHandle<Effect> ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToRetrieve);
	template<>
	GLOWENGINE_EXPORT ResourceHandle<Effect> ResourceMgr::getDefaultResource();
}

#endif
