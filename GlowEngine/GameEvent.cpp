#include "GameEvent.h"

#include "GUI/GUIElement.h"

GLOWE::GameEvent::GameEvent(const Type newType, const Info & newInfo) noexcept
	: type(newType), info(newInfo)
{
}

GLOWE::GameEvent::GameEvent(const Event& event) noexcept
	: type(Type::System), info(event)
{
}

GLOWE::GameEvent::GameEvent(Components::Transform* const ptr) noexcept
	: type(Type::TransformUpdated), info(ptr)
{
}
