#include "Resource.h"

#include "../GlowSystem/AsyncIO.h"

#include "../GlowGraphics/Uniform/GraphicsDeviceImplementation.h"
#include "../GlowGraphics/Uniform/TextureImplementation.h"
#include "../GlowGraphics/Uniform/GSILShaderCompiler.h"
#include "../GlowGraphics/Uniform/Geometry.h"

#include "GUI/Text.h"

#include "GameObjectBlueprint.h"
#include "Material.h"

constexpr char GLOWE::ResourceMgr::magicString[];
constexpr char GLOWE::ResourceMgr::packageDefinitionFileName[];

bool GLOWE::ResourceMgr::checkIfMagicStringIsCompatible(const char* arg)
{
	return std::strncmp(magicString, arg, sizeof(magicString) - 1);
}

GLOWE::SharedPtr<GLOWE::String> GLOWE::ResourceMgr::privLoad(const Hash& nameHash)
{
	auto& filePair = loadedFiles[nameHash];
	WeakPtr<String>& weakPtr = filePair.first;
	DefaultLockGuard lockGuard(filePair.second);

	if (weakPtr.expired())
	{
		SharedPtr<String> result = makeSharedPtr<String>();
		const ResourceFile& resFile = getResourceFileInfo(nameHash);

		switch (checkResourcePlacement(nameHash))
		{
		case ResourcePlacement::InPackage:
		{
			const PackagedResourceFile& packResFile = static_cast<const PackagedResourceFile&>(resFile);
			Package& package = packageMgr.getPackage(packResFile.packagePath);
			package.readFile(resFile.path, *result);
		}
		break;
		case ResourcePlacement::OnDisk:
		{
			File file;
			file.open(resFile.path, std::ios::in | std::ios::binary);
			result->resize(file.getFileSize());
			file.read(result->getCharArray(), result->getSize());
		}
		break;
		default:
			WarningThrow(false, u8"Resource cannot be located.");
			throw Exception(u8"Resource cannot be located.");
		}

		weakPtr = result;
		return std::move(result);
	}
	else
	{
		return weakPtr.lock();
	}
}

GLOWE::SharedPtr<GLOWE::String> GLOWE::ResourceMgr::privLoad(const ResourcePlacement placement, const Hash& hash, const ResourceFile& fileInfo)
{
	auto& filePair = loadedFiles[hash];
	WeakPtr<String>& weakPtr = filePair.first;
	DefaultLockGuard lockGuard(filePair.second);

	if (weakPtr.expired())
	{
		SharedPtr<String> result = makeSharedPtr<String>();

		switch (placement)
		{
		case ResourcePlacement::InPackage:
		{
			const PackagedResourceFile& packResFile = static_cast<const PackagedResourceFile&>(fileInfo);
			Package& package = packageMgr.getPackage(packResFile.packagePath);
			package.readFile(fileInfo.path, *result);
		}
		break;
		case ResourcePlacement::OnDisk:
		{
			File file;
			file.open(fileInfo.path, std::ios::in | std::ios::binary);
			result->resize(file.getFileSize());
			file.read(result->getCharArray(), result->getSize());
		}
		break;
		default:
			WarningThrow(false, u8"Resource cannot be located.");
			throw Exception(u8"Resource cannot be located.");
		}

		weakPtr = result;
		return std::move(result);
	}
	else
	{
		return weakPtr.lock();
	}
}

GLOWE::ResourceMgr::ResourceFile& GLOWE::ResourceMgr::getResourceFileInfo(const Hash& resHash)
{
	switch (checkResourcePlacement(resHash))
	{
	case ResourcePlacement::InPackage:
		return files.at(resHash);
	case ResourcePlacement::OnDisk:
		return filesOnDisk.at(resHash);
	default:
		WarningThrow(false, u8"Resource cannot be located.");
		throw Exception(u8"Resource cannot be located.");
	}
}

void GLOWE::ResourceMgr::loadDependiencies(const Hash& hash, const ResourceFile& file, const SubresourceInfo& subres, Map<Hash, ResourceHandle<void>>& deps)
{
	Map<Hash, SharedPtr<String>> tempFiles;

	for (const auto& dep : subres.externalDependiencies)
	{
		if ((!std::get<2>(dep.second)) && tempFiles.count(std::get<0>(dep.second)) == 0 && getResourceFileInfo(std::get<0>(dep.second)).subresources.at(std::get<1>(dep.second)).resourcePtr.expired())
		{
			// Cache this file.
			tempFiles.emplace(std::get<0>(dep.second), privLoad(std::get<0>(dep.second)));
		}

		deps.emplace(dep.first, load(std::get<0>(dep.second), std::get<1>(dep.second)));
	}

	// Cache the file for the internal dependencies.
	SharedPtr<String> thisFile = privLoad(checkResourcePlacement(hash), hash, file);

	for (const auto& dep : subres.internalDependiencies)
	{
		if (!dep.second.second)
		{
			deps.emplace(dep.first, load(hash, dep.second.first));
		}
	}
}

void GLOWE::ResourceMgr::loadDependiencies(const Hash& hash, const ResourceFile& file, Vector<SubresourceInfo*> subresources, Vector<Map<Hash, ResourceHandle<void>>>& deps)
{
	deps.resize(subresources.size());
	Map<Hash, SharedPtr<String>> tempFiles;
	SharedPtr<String> thisFile = privLoad(checkResourcePlacement(hash), hash, file);

	for (unsigned int x = 0; x < subresources.size(); ++x)
	{
		SubresourceInfo& subres = *subresources[x];
		Map<Hash, ResourceHandle<void>>& currDeps = deps[x];
		for (const auto& dep : subres.externalDependiencies)
		{
			if ((!std::get<2>(dep.second)) && tempFiles.count(std::get<0>(dep.second)) == 0 && getResourceFileInfo(std::get<0>(dep.second)).subresources.at(std::get<1>(dep.second)).resourcePtr.expired())
			{
				// Cache this file.
				tempFiles.emplace(std::get<0>(dep.second), privLoad(std::get<0>(dep.second)));
			}

			currDeps.emplace(dep.first, load(std::get<0>(dep.second), std::get<1>(dep.second)));
		}

		for (const auto& dep : subres.internalDependiencies)
		{
			if (!dep.second.second)
			{
				currDeps.emplace(dep.first, load(hash, dep.second.first));
			}
		}
	}
}

bool GLOWE::ResourceMgr::verifyVersion(const UInt64 ver)
{
	return ver == versionNumberUInt64;
}

void GLOWE::ResourceMgr::cleanUp()
{
	DefaultLockGuard filesLock(filesMutex), diskFilesLock(diskFilesMutex), subresLock(subresourcesByTypesMutex);
	subresourcesByTypes.clear();
	loadedFiles.clear();
	packagePaths.clear();
	filesOnDisk.clear();
	files.clear();
}

GLOWE::ResourceMgr::ResourceMgr()
	: files(), filesOnDisk(), packageMgr(getInstance<PackageMgr>()), graphicsDevice(nullptr)
{
	// Initialize resource type mappings.
	for (unsigned int x = 0; x < static_cast<unsigned int>(SubresourceType::ElemsCount); ++x)
	{
		subresourcesByTypes.emplace((SubresourceType)x, Vector<Pair<Hash, Hash>>());
	}
}

void GLOWE::ResourceMgr::initialize(const GraphicsDeviceImpl& device)
{
	if (!graphicsDevice)
	{
		graphicsDevice = &device;
		FilePath path = ".\\";
		path.replacePreferredSeparators();
		ThreadPool threads;
		threads.create(getCPUCoresCount());
		scanDirectory(path, threads);

		for (const auto& file : files)
		{
			loadedFiles.emplace(std::piecewise_construct, std::forward_as_tuple(file.first), std::tuple<>());
		}

		for (const auto& file : filesOnDisk)
		{
			loadedFiles.emplace(std::piecewise_construct, std::forward_as_tuple(file.first), std::tuple<>());
		}
	}
}

GLOWE::ThreadPool::TaskResult<void> GLOWE::ResourceMgr::initializeAsync(const GraphicsDeviceImpl& device, ThreadPool& threads)
{
	ThreadPool::TaskResult<void> result;
	if (!graphicsDevice)
	{
		graphicsDevice = &device;
		FilePath path = ".\\";
		path.replacePreferredSeparators();
		TrivialUniquePtr<Vector<ThreadPool::TaskResult<void>>> resultsUnique = makeTrivialUniquePtr<Vector<ThreadPool::TaskResult<void>>>();
		(*resultsUnique) = scanDirectoryAsync(path, threads);
		result = threads.addTask([this](Vector<ThreadPool::TaskResult<void>>* const uniquePtr)
		{
			TrivialUniquePtr<Vector<ThreadPool::TaskResult<void>>> results(uniquePtr);

			for (const auto& x : *results)
			{
				x.wait();
			}

			for (const auto& file : files)
			{
				loadedFiles.emplace(std::piecewise_construct, std::forward_as_tuple(file.first), std::tuple<>());
			}

			for (const auto& file : filesOnDisk)
			{
				loadedFiles.emplace(std::piecewise_construct, std::forward_as_tuple(file.first), std::tuple<>());
			}
		}, resultsUnique.release());
	}

	return result;
}

const GLOWE::GraphicsDeviceImpl* GLOWE::ResourceMgr::getDevice() const
{
	return graphicsDevice;
}

GLOWE::Vector<GLOWE::ResourceHandle<void>> GLOWE::ResourceMgr::load(const Hash& nameHash, const Vector<Hash>& subresources)
{
	Vector<ResourceHandle<void>> result(subresources.size());
	SharedPtr<String> thisFile = privLoad(nameHash);

	for (unsigned int x = 0; x < subresources.size(); ++x)
	{
		result[x] = load(nameHash, subresources[x]);
	}

	return result;
}

GLOWE::ResourceHandle<void> GLOWE::ResourceMgr::load(const char* name, const char* resourceName)
{
	return load(Hash(name), Hash(resourceName));
}

GLOWE::ResourceHandle<void> GLOWE::ResourceMgr::load(const String& name, const String& resourceName)
{
	return load(Hash(name), Hash(resourceName));
}

GLOWE::ResourceHandle<void> GLOWE::ResourceMgr::load(const Hash& nameHash, const Hash& resourceName)
{
	try
	{
		const SubresourceType type = getResourceFileInfo(nameHash).subresources.at(resourceName).type;

		switch (type)
		{
		case SubresourceType::Geometry:
			return load<Geometry>(nameHash, resourceName).treatAs<void>();
		case SubresourceType::GeometryBuffers:
			return load<GeometryBuffers>(nameHash, resourceName).treatAs<void>();
		case SubresourceType::GameObjectBlueprint:
			return load<GameObjectBlueprint>(nameHash, resourceName).treatAs<void>();
		case SubresourceType::Texture:
			return load<TextureImpl>(nameHash, resourceName).treatAs<void>();
		case SubresourceType::Effect:
			return load<Effect>(nameHash, resourceName).treatAs<void>();
		case SubresourceType::Material:
			return load<Material>(nameHash, resourceName).treatAs<void>();
		case SubresourceType::ShaderCollection:
			return load<ShaderCollectionImpl>(nameHash, resourceName).treatAs<void>();
		case SubresourceType::HLSLShaderCode:
		case SubresourceType::GLSLShaderCode:
		{
			return load<String>(nameHash, resourceName).treatAs<void>();
		}
		case SubresourceType::Font:
		{
			return load<Font>(nameHash, resourceName).treatAs<void>();
		}
		default:
			return ResourceHandle<void>();
		}
	}
	catch (...)
	{
		return ResourceHandle<void>();
	}
}

bool GLOWE::ResourceMgr::isLoadable(const Hash & nameHash) const
{
	if (filesOnDisk.count(nameHash) > 0)
	{
		const ResourceFile& rf = filesOnDisk.at(nameHash);

		return Filesystem::isExisting(rf.path) && !(Filesystem::isDirectory(rf.path));
	}
	if (files.count(nameHash) > 0)
	{
		const PackagedResourceFile& rf = files.at(nameHash);

		return packageMgr.getPackage(rf.packagePath).checkFileForExistence(rf.path);
	}

	return false;
}

bool GLOWE::ResourceMgr::isLoadable(const Hash & nameHash, const Hash & resourceHash) const
{
	if (filesOnDisk.count(nameHash) > 0)
	{
		const ResourceFile& rf = filesOnDisk.at(nameHash);

		return Filesystem::isExisting(rf.path) && !(Filesystem::isDirectory(rf.path)) && (rf.subresources.count(resourceHash) > 0);
	}
	if (files.count(nameHash) > 0)
	{
		const PackagedResourceFile& rf = files.at(nameHash);

		return packageMgr.getPackage(rf.packagePath).checkFileForExistence(rf.path) && (rf.subresources.count(resourceHash) > 0);
	}

	return false;
}

const GLOWE::Vector<GLOWE::Pair<GLOWE::Hash, GLOWE::Hash>>& GLOWE::ResourceMgr::getSubresourcesOfType(const SubresourceType & type) const
{
	return subresourcesByTypes.at(type);
}

GLOWE::ResourceMgr::ResourcePlacement GLOWE::ResourceMgr::checkResourcePlacement(const Hash & resHash) const
{
	if (files.count(resHash) > 0)
	{
		return ResourcePlacement::InPackage;
	}
	if (filesOnDisk.count(resHash) > 0)
	{
		return ResourcePlacement::OnDisk;
	}

	return ResourcePlacement::NonExisting;
}

const GLOWE::ResourceMgr::ResourceFile & GLOWE::ResourceMgr::getResourceFileInfo(const Hash & resHash) const
{
	switch (checkResourcePlacement(resHash))
	{
	case ResourcePlacement::InPackage:
		return files.at(resHash);
	case ResourcePlacement::OnDisk:
		return filesOnDisk.at(resHash);
	default:
		WarningThrow(false, u8"Resource cannot be located.");
		throw Exception(u8"Resource cannot be located.");
	}
}

GLOWE::ResourceHandle<void> GLOWE::ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const SubresourceType subresourceType, const Hash& subresourceToRetrieve)
{
	switch (subresourceType)
	{
	case SubresourceType::Geometry:
		return loadDynamic<Geometry>(memory, size, loadedDependiencies, subresourceToRetrieve).treatAs<void>();
	case SubresourceType::GeometryBuffers:
		return loadDynamic<GeometryBuffers>(memory, size, loadedDependiencies, subresourceToRetrieve).treatAs<void>();
	case SubresourceType::GameObjectBlueprint:
		return loadDynamic<GameObjectBlueprint>(memory, size, loadedDependiencies, subresourceToRetrieve).treatAs<void>();
	case SubresourceType::Texture:
		return loadDynamic<TextureImpl>(memory, size, loadedDependiencies, subresourceToRetrieve).treatAs<void>();
	case SubresourceType::Effect:
		return loadDynamic<Effect>(memory, size, loadedDependiencies, subresourceToRetrieve).treatAs<void>();
	case SubresourceType::Material:
		return loadDynamic<Material>(memory, size, loadedDependiencies, subresourceToRetrieve).treatAs<void>();
	case SubresourceType::ShaderCollection:
		return loadDynamic<ShaderCollectionImpl>(memory, size, loadedDependiencies, subresourceToRetrieve).treatAs<void>();
	case SubresourceType::HLSLShaderCode:
	case SubresourceType::GLSLShaderCode:
	{
		return loadDynamic<String>(memory, size, loadedDependiencies, subresourceToRetrieve).treatAs<void>();
	}
	case SubresourceType::Font:
	{
		return loadDynamic<Font>(memory, size, loadedDependiencies, subresourceToRetrieve).treatAs<void>();
	}
	default:
		return ResourceHandle<void>();
	}
}

void GLOWE::ResourceMgr::scanDirectory(const FilePath & path, ThreadPool& threads)
{
	const auto results = scanDirectoryAsync(path, threads);

	for (const auto& x : results)
	{
		x.wait();
	}
}

GLOWE::Vector<GLOWE::ThreadPool::TaskResult<void>> GLOWE::ResourceMgr::scanDirectoryAsync(const FilePath& path, ThreadPool& threads)
{
	Vector<ThreadPool::TaskResult<void>> result;
	FilePath tempPath = path;
	if (tempPath.hasExtension())
	{
		tempPath.removeExtension();
	}
	tempPath.append(u8"\\*.gpckg"_str);
	tempPath.replacePreferredSeparators();
	FileListing listing;
	listing.create(tempPath);

	unsigned int threadPoolSize = listing.getSize();

	if (threadPoolSize > getCPUCoresCount())
	{
		threadPoolSize = getCPUCoresCount();
	}

	Vector<ThreadPool::TaskResult<void>> results;
	results.reserve(listing.getSize());

	for (const auto& x : listing)
	{
		results.emplace_back(threads.addTask([x, this]() { scanFile(x); }));
	}

	return result;
}

void GLOWE::ResourceMgr::scanFile(const FilePath & path)
{
	auto loadSubresourceInfo = [](MemoryLoadSaveHandle& handle, SubresourceInfo& result)
	{
		UInt32 temp{};
		Hash tempHash, tempHash2, tempHash3;
		bool tempBool{};

		handle >> result.type >> temp;

		// Internal deps.
		for (UInt32 x = 0; x < temp; ++x)
		{
			handle >> tempHash >> tempHash2 >> tempBool;
			result.internalDependiencies.emplace_back(std::make_pair(tempHash, std::make_pair(tempHash2, tempBool)));
		}

		handle >> temp;
		// External deps.
		for (UInt32 x = 0; x < temp; ++x)
		{
			handle >> tempHash >> tempHash2 >> tempHash3 >> tempBool;
			result.externalDependiencies.emplace_back(std::make_pair(tempHash, std::tuple<Hash, Hash, bool>(tempHash2, tempHash3, tempBool)));
		}
	};

	if (!Filesystem::isExisting(path) || !Filesystem::isRegularFile(path))
	{
		return;
	}

	Package& package = packageMgr.getPackage(path);
	if (!package.checkFileForExistence(packageDefinitionFileName))
	{
		WarningThrow(false, "No package definition file found in " + path);
		return;
	}

	String contents, packageName;

	package.readFile(packageDefinitionFileName, contents);

	MemoryLoadSaveHandle handle(contents.getCharArray(), contents.getSize());

	FilePath& packagePath = packagePaths[Hash(path)];
	packagePath = path;

	FilePath tempPath;
	Hash tempHash, tempHash2;
	UInt64 version;
	UInt32 resCount, virtualResCount, tempUInt32;
	handle >> version;

	if (!verifyVersion(version))
	{
		WarningThrow(false, "Incompatible package " + path.getString() + ". Package version " + toString(version) + " incompatible with " + toString(GLOWE::versionNumberUInt64) + ".");
		package.close();
		return;
	}

	handle >> packageName >> resCount >> virtualResCount;

	Map<Hash, SubresourceInfo> subresources;

	// Packaged resources.
	for (UInt32 x = 0; x < resCount; ++x)
	{
		handle >> tempHash >> tempPath >> tempUInt32;
		if (tempUInt32 > 0)
		{
			DefaultLockGuard guard(subresourcesByTypesMutex);
			for (UInt32 y = 0; y < tempUInt32; ++y)
			{
				handle >> tempHash2;
				SubresourceInfo& info = subresources.emplace(std::piecewise_construct, std::forward_as_tuple(tempHash2), std::tuple<>()).first->second;
				loadSubresourceInfo(handle, info);
				subresourcesByTypes.at(info.type).emplace_back(tempHash, tempHash2);
			}
		}

		{
			DefaultLockGuard guard(filesMutex);
			files.emplace(std::piecewise_construct, std::forward_as_tuple(tempHash), std::forward_as_tuple(std::move(subresources), tempPath, packagePath));
		}
		subresources = Map<Hash, SubresourceInfo>();
	}

	// Resources on disk.
	for (UInt32 x = 0; x < virtualResCount; ++x)
	{
		handle >> tempHash >> tempPath >> tempUInt32;
		if (tempUInt32 > 0)
		{
			DefaultLockGuard guard(subresourcesByTypesMutex);
			for (UInt32 y = 0; y < tempUInt32; ++y)
			{
				handle >> tempHash2;
				SubresourceInfo& info = subresources.emplace(std::piecewise_construct, std::forward_as_tuple(tempHash2), std::tuple<>()).first->second;
				loadSubresourceInfo(handle, info);
				subresourcesByTypes.at(info.type).emplace_back(tempHash, tempHash2);
			}
		}

		{
			DefaultLockGuard guard(diskFilesMutex);
			filesOnDisk.emplace(std::piecewise_construct, std::forward_as_tuple(tempHash), std::forward_as_tuple(std::move(subresources), tempPath));
		}
		subresources = Map<Hash, SubresourceInfo>();
	}
}
