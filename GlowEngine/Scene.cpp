#include "Scene.h"
#include "GameObject.h"
#include "SimpleRaceDetector.h"

void GLOWE::Scene::updateChildrenToKill(GLOWE::GameObject* gameObject)
{
	if (addedObjects.count(gameObject) == 0)
	{
		addedObjects.emplace(gameObject);
		for (const auto& x : gameObject->children)
		{
			updateChildrenToKill(x.second);
		}
		finalObjectsKillList.emplace_back(gameObject);
	}
}

void GLOWE::Scene::updateDestroy()
{
	if (objectsToDestroy.size() > 0)
	{
		for (const auto& x : objectsToDestroy)
		{
			updateChildrenToKill(x->get());
		}

		for (const auto& x : finalObjectsKillList)
		{
			gameObjects.erase(x->inSceneHandle);
		}

		addedObjects.clear();
		finalObjectsKillList.clear();
		objectsToDestroy.clear();
	}
}

GLOWE::Scene::Scene(const String& myName)
	: gameObjects(), sceneMgrHandle(), objectsToDestroy(), addedObjects(), finalObjectsKillList(), name(myName)
{
}

GLOWE::Scene::Scene(const String& myName, const CanConstructSceneTag&)
	: Scene(myName)
{
}

GLOWE::Scene::~Scene()
{
	updateDestroy(); // Just in case.

	for (auto& gObj : gameObjects)
	{
		if (gObj->getParent() == nullptr)
		{
			objectsToDestroy.emplace_back(gObj->inSceneHandle);
		}
	}

	updateDestroy();
}

GLOWE::GameObject& GLOWE::Scene::createGameObject(const String& name)
{
	//gameObjects.emplace_back(getInstance<GlobalAllocator>().allocate<GameObject>(name, *this, GameObject::CanCreateGameObjectTag()));
	SimpleRaceDetector<Scene> _;
	gameObjects.emplace_back(makeUniquePtr<GameObject>(name, *this, GameObject::CanCreateGameObjectTag()));
	GameObject& result = *gameObjects.back();
	result.inSceneHandle = std::prev(gameObjects.end());
	return result;
}

GLOWE::GameObject* GLOWE::Scene::findGameObject(const String& name) const
{
	return nullptr;
}

void GLOWE::Scene::destroyGameObject(GameObject& gameObject)
{
	SimpleRaceDetector<Scene> _;
	if (gameObject.owningScene == this)
	{
		objectsToDestroy.emplace_back(gameObject.inSceneHandle);
	}
	else
	{
		WarningThrow(false, "Cannot destroy a GameObject that is not owned by scene referenced by \"this\". GameObject: " + gameObject.name + '.');
	}
}

void GLOWE::Scene::destroyGameObject(GameObject* gameObject)
{
	if (gameObject)
	{
		destroyGameObject(*gameObject);
	}
}

void GLOWE::Scene::destroyGameObjectImmediate(GameObject& gameObject)
{
	SimpleRaceDetector<Scene> _;
	if (gameObject.owningScene == this)
	{
		for (const auto& x : gameObject.children)
		{
			destroyGameObjectImmediate(x.second);
		}

		gameObjects.erase(gameObject.inSceneHandle);
	}
	else
	{
		WarningThrow(false, "Cannot destroy a GameObject that is not owned by scene referenced by \"this\". GameObject: " + gameObject.name + '.');
	}
}

void GLOWE::Scene::destroyGameObjectImmediate(GameObject* gameObject)
{
	if (gameObject)
	{
		destroyGameObjectImmediate(*gameObject);
	}
}

GLOWE::Scene::GameObjectHandle GLOWE::Scene::begin()
{
	return gameObjects.begin();
}

GLOWE::Scene::ConstGameObjectHandle GLOWE::Scene::begin() const
{
	return gameObjects.begin();
}

GLOWE::Scene::GameObjectHandle GLOWE::Scene::end()
{
	return gameObjects.end();
}

GLOWE::Scene::ConstGameObjectHandle GLOWE::Scene::end() const
{
	return gameObjects.end();
}

void GLOWE::Scene::transferOwnership(GameObject* obj)
{
	SimpleRaceDetector<Scene> _;
	gameObjects.emplace_back(std::move(*obj->inSceneHandle));
	obj->owningScene->gameObjects.erase(obj->inSceneHandle);
	obj->inSceneHandle = std::prev(gameObjects.end());
	obj->owningScene = this;
}

const GLOWE::String& GLOWE::Scene::getName() const
{
	return name;
}

void GLOWE::Scene::unload()
{
	getInstance<SceneMgr>().unload(this);
}

GLOWE::Scene& GLOWE::Scene::createEmptyScene(const String& myName)
{
	return getInstance<SceneMgr>().createEmptyScene(myName);
}

void GLOWE::SceneMgr::updateUnload()
{
	for (auto& scene : scenesToUnload)
	{
		Scene* oldScene = scene->second.get();

		scenes.erase(scene);

		if (activeScene == oldScene)
		{
			if (scenes.size())
			{
				activeScene = scenes.begin()->second.get();
			}
			else
			{
				activeScene = nullptr;
			}
		}
	}

	scenesToUnload.clear();
}

GLOWE::SceneMgr::SceneMgr()
	: scenes(), activeScene(nullptr), scenesToUnload()
{
}

GLOWE::Scene& GLOWE::SceneMgr::createEmptyScene(const String& myName)
{
	//scenes.emplace_back(getInstance<GlobalAllocator>().allocate<Scene>(Scene::CanConstructSceneTag()));
	Scene& result = *(scenes.emplace(Hash(myName), makeUniquePtr<Scene>(myName, Scene::CanConstructSceneTag())).first->second);
	result.sceneMgrHandle = std::prev(scenes.end());
	if (!activeScene)
	{
		setActiveScene(result);
	}
	return result;
}

void GLOWE::SceneMgr::setActiveScene(Scene& scene)
{
	activeScene = &scene;
}

void GLOWE::SceneMgr::setActiveScene(Scene* scene)
{
	if (scene)
	{
		activeScene = scene;
	}
}

GLOWE::Scene* GLOWE::SceneMgr::getActiveScene() const
{
	return activeScene;
}

GLOWE::Scene* GLOWE::SceneMgr::getScene(const Hash& myName) const
{
	const auto findIt = scenes.find(myName);
	if (findIt != scenes.end())
	{
		return findIt->second.get();
	}

	return nullptr;
}

void GLOWE::SceneMgr::unload(Scene* scene)
{
	scenesToUnload.insert(scene->sceneMgrHandle);
}

void GLOWE::SceneMgr::unload(Scene& scene)
{
	scenesToUnload.insert(scene.sceneMgrHandle);
}

GLOWE::SceneMgr::ConstSceneHandle GLOWE::SceneMgr::begin() const
{
	return scenes.begin();
}

GLOWE::SceneMgr::ConstSceneHandle GLOWE::SceneMgr::end() const
{
	return scenes.end();
}

void GLOWE::SceneMgr::cleanUp()
{
	scenes.clear();
	activeScene = nullptr;
}
