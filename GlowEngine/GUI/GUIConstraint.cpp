#include "GUIConstraint.h"
#include "GUIElement.h"

#include "../GameObject.h"
#include "../Component.h"

GLOWE::GUIConstraint::GUIConstraint()
	: type(Type::Invalid)
{
}

GLOWE::GUIConstraint::GUIConstraint(const Type constraintType)
	: type(constraintType)
{
}

GLOWE::GUIConstraint::GUIConstraint(const Type constraintType, const float distance, GUIElement* const elem, const Edge edge)
	: fixedData{ distance, elem, edge }, type(constraintType)
{
}

GLOWE::GUIConstraint::GUIConstraint(GUIElement* const elem)
	: fixedData{0, elem, Edge::LeftUp}, type(Type::PureDependency)
{
}

GLOWE::GUIConstraint::GUIConstraint(const Type constraintType, const float leftUpPart, const float rightBottomPart, GUIElement* const leftUp, GUIElement* const rightDown, const Edge leftUpEdge, const Edge rightDownEdge)
	: ratioData{ leftUpPart, rightBottomPart, leftUp, rightDown, leftUpEdge, rightDownEdge }, type(constraintType)
{
}

GLOWE::GUIConstraint::GUIConstraint(const Type constraintType, const float aspectRatio, const bool prioritizeWidthResize)
	: keepAspectRatioData{ prioritizeWidthResize, aspectRatio }, type(constraintType)
{
}

GLOWE::GUICycleDetected::GUICycleDetected(Vector<GUIElement*>&& arg)
	: GUIException("GUI cycle detected. Check the constraints. Elements:\n"), cycledElements(std::move(arg))
{
	for (const auto x : cycledElements)
	{
		Component* const comp = dynamic_cast<Component*>(x);
		if (comp)
		{
			text += comp->getTypeName() + " from GameObject \"" + comp->getGameObject()->getName() + "\"\n";
		}
		else
		{
			text += "element uncastable to Component\n";
		}
	}
}
