#ifndef GLOWE_GUI_SPRITE_INCLUDED
#define GLOWE_GUI_SPRITE_INCLUDED

#include "../Resource.h"

#include "../../GlowGraphics/Uniform/MaterialEnvironment.h"
#include "../../GlowGraphics/Uniform/TextureImplementation.h"

#include "GUIRect.h"

namespace GLOWE
{
	class GUIElement;

	class GLOWENGINE_EXPORT Sprite
	{
	private:
		Float4 border = Float4{ 0, 0, 0, 0 };
		ResourceHandle<TextureImpl> texture;
		GUIRect textureRect;
	public:
		void setTexture(const ResourceHandle<TextureImpl>& newTexture, const GUIRect& newTextureRect = GUIRect());
		void setTextureRect(const GUIRect& newTextureRect);
		void setBorder(const Float4& newBorder);

		const ResourceHandle<TextureImpl>& getTexture() const;
		GUIRect getTextureRect() const;
		Float4 getBorder() const;

		void apply(MaterialEnvironment& matEnv, GUIElement* const element);
	};
}

#endif
