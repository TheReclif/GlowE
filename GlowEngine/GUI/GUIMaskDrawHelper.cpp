#include "GUIMaskDrawHelper.h"

#include "../Engine.h"

GLOWE::Hidden::GUIMaskDrawHelper::GUIMaskDrawHelper()
	: usedMaterial(), guiPlane(loadResource<Geometry>("GlowCore/BasicGeometry/GUIPlane.gmsh", "GUIPlane")), engine(getInstance<Engine>())
{
	auto effect = loadResource<Effect>("GlowCore/Shaders/StandardGUIMask.geff", "Effect");
	Material::createFromEffect(usedMaterial, engine.getGraphicsContext(), effect, "DefaultGUIMask");
	const auto cbuffPos = usedMaterial.getCBufferPosition("Material");
	usedMaterial.modifyCBuffer<Color>(cbuffPos, usedMaterial.getCBufferFieldPosition(cbuffPos, "Color"), Color(0, 0, 0, 0));
}

void GLOWE::Hidden::GUIMaskDrawHelper::drawMask(const GUIRect& rectToDraw, BasicRendererImpl& renderer, MaterialEnvironment& matEnv)
{
	const auto scaledSize = rectToDraw.getScaledSize();
	Transform tempTrans;
	tempTrans.setScale(Float3{ scaledSize[0], scaledSize[1], 0.0f });
	tempTrans.setTranslation(Float3{ rectToDraw.x, rectToDraw.y, 0.0f });

	matEnv.setVariable<Float4x4>("World View Proj", Matrix4x4(matEnv.getVariable<Float4x4>("View Proj")) * tempTrans.calculateMatrix());

	usedMaterial.applyEnvironment(matEnv);
	usedMaterial.apply(0, Hash(), renderer);
	guiPlane->apply(renderer, engine.getGraphicsContext(), usedMaterial.getShaderCollection(0, Hash()));
	renderer.setPrimitiveTopology(BasicRendererImpl::PrimitiveTopology::TriangleList);
	guiPlane->draw(renderer);
}
