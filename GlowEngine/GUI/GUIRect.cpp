#include "GUIRect.h"
#include "../../GlowMath/VectorClasses.h"

GLOWE::Float2 GLOWE::GUIRect::getScaledSize() const
{
	return (Vector2F(size) * Vector2F(scale)).toFloat2();
}

bool GLOWE::GUIRect::isPointInside(const Float2& point) const
{
	const auto newSize = getScaledSize();
	return point[0] >= pos[0] && point[1] >= pos[1] && point[0] <= (pos[0] + newSize[0]) && point[1] <= (pos[1] + newSize[1]);
}

bool GLOWE::GUIRect::isRectIntersecting(const GUIRect& other) const
{
	return
		other.x <= (x + (width * xScale)) && (other.x + (other.width * other.xScale)) >= x
		&&
		other.y <= (y + (height * yScale)) && (other.y + (other.height * other.yScale)) >= y;
}

GLOWE::GUIRect GLOWE::GUIRect::calculateIntersection(const GUIRect& other) const
{
	// https://math.stackexchange.com/questions/7356/how-to-find-rectangle-intersection-on-a-coordinate-plane
	const float leftX = std::max(x, other.x);
	const float rightX = std::min(x + (width * xScale), other.x + (other.width * other.xScale));
	const float topY = std::max(y, other.y);
	const float bottomY = std::min(y + (height * yScale), other.y + (other.height * other.yScale));

	if (leftX < rightX && topY < bottomY)
	{
		return GUIRect({ leftX, topY }, { rightX - leftX, bottomY - topY });
	}
	
	return GUIRect();
}

GLOWE::Rect GLOWE::GUIRect::toRect() const
{
	const auto scaledSize = getScaledSize();
	return Rect
	{
		{ static_cast<int>(x), static_cast<int>(y) } ,
		{ static_cast<int>(x + scaledSize[0]), static_cast<int>(y + scaledSize[1]) }
	};
}
