#pragma once
#ifndef GLOWE_GUI_RECT_INCLUDED
#define GLOWE_GUI_RECT_INCLUDED

#include "../Engine.h"

#include "../../GlowMath/Rect.h"

namespace GLOWE
{
	class GLOWENGINE_EXPORT GUIRect
	{
	public:
		union
		{
			Float2 pos;
			struct
			{
				float x, y;
			};
		};
		union
		{
			Float2 size;
			struct
			{
				float width, height;
			};
		};
		union
		{
			Float2 scale;
			struct
			{
				float xScale, yScale;
			};
		};
	public:
		inline GUIRect() noexcept
			: pos{ 0.0f, 0.0f }, size{ 0.0f, 0.0f }, scale{ 1.0f, 1.0f }
		{}
		inline GUIRect(const Float2& newPos, const Float2& newSize, const Float2& newScale = Float2{ 1.0f, 1.0f }) noexcept
			: pos(newPos), size(newSize), scale(newScale)
		{}

		Float2 getScaledSize() const;

		bool isPointInside(const Float2& point) const;
		bool isRectIntersecting(const GUIRect& other) const;

		/// @brief Calculates the rect intersection. Width and height will never be negative.
		/// @param other Second rect.
		/// @return Intersection of the two rects.
		GUIRect calculateIntersection(const GUIRect& other) const;

		Rect toRect() const;
	};
}

#endif
