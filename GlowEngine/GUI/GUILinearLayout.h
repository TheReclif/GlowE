#pragma once
#ifndef GLOWE_GRAPHICS_GUILINEARLAYOUT_INCLUDED
#define GLOWE_GRAPHICS_GUILINEARLAYOUT_INCLUDED

#include "GUIContainer.h"

namespace GLOWE
{
	class GLOWENGINE_EXPORT GUILinearLayout
		: public GLOWE::GUIContainer
	{
	private:
		float offset, contentSize;
		bool isVertical;
		std::function<void(const float)> onOffsetChanged;
	private:
		virtual bool onAfterElementsUpdate() override;

		virtual void invalidateElement(GUIElement* const element, const InvalidationType inval) override;
	public:
		/// @brief Creates the linear layout.
		/// @param vertical true if vertical linear layout, false otherwise.
		GUILinearLayout(const bool vertical = true);

		void setOffset(const float newOffset);
		float getOffset() const;

		void setIsVertical(const bool vertical);
		bool checkIsVertical() const;

		float getContentSize() const;

		void setOffsetChangedCallback(const std::function<void(const float)>& newCallback);
	};
}

#endif
