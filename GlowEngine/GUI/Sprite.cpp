#include "Sprite.h"
#include "GUIElement.h"

void GLOWE::Sprite::setTexture(const ResourceHandle<TextureImpl>& newTexture, const GUIRect& newTextureRect)
{
	texture = newTexture;
	textureRect = newTextureRect;
	if (textureRect.width == 0 && textureRect.height == 0)
	{
		const auto& desc = texture->getDesc();
		textureRect.width = desc.width;
		textureRect.height = desc.height;
	}
}

void GLOWE::Sprite::setTextureRect(const GUIRect& newTextureRect)
{
	textureRect = newTextureRect;
}

void GLOWE::Sprite::setBorder(const Float4& newBorder)
{
	border = newBorder;
}

const GLOWE::ResourceHandle<GLOWE::TextureImpl>& GLOWE::Sprite::getTexture() const
{
	return texture;
}

GLOWE::GUIRect GLOWE::Sprite::getTextureRect() const
{
	return textureRect;
}

GLOWE::Float4 GLOWE::Sprite::getBorder() const
{
	return border;
}

void GLOWE::Sprite::apply(MaterialEnvironment& matEnv, GUIElement* const element)
{
	const Vector4F spriteSize(textureRect.width, textureRect.width, textureRect.height, textureRect.height);
	const Vector4F localBorder(border);

	matEnv.setVariable("Border", (localBorder / spriteSize).toFloat4());

	const auto size = element->getGlobalGUIRect().getScaledSize();
	const Vector4F rectSize(size[0], size[0], size[1], size[1]);
	matEnv.setVariable("Window Border", (localBorder / rectSize).toFloat4());
}
