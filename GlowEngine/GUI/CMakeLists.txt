target_sources(GlowEngine
	PRIVATE
		Selectable.cpp
		Sprite.cpp
		GUIConstraint.cpp
		GUIContainer.cpp
		GUIElement.cpp
		GUILinearLayout.cpp
		GUIMaskDrawHelper.cpp
		GUIRect.cpp
		Text.cpp
#	PUBLIC
#		GUIConstraint.h
#		GUIContainer.h
#		GUIElement.h
#		GUILinearLayout.h
#		GUIRect.h
#		GUITop.h
#		Text.h
)
install(DIRECTORY "." DESTINATION include/GlowEngine/GUI FILES_MATCHING PATTERN "*.h")