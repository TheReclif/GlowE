#pragma once
#ifndef GLOWE_GUI_TEXT_INCLUDED
#define GLOWE_GUI_TEXT_INCLUDED

#include "GUIRect.h"

#include "../Resource.h"

#include "../../GlowGraphics/Uniform/TextureAtlas.h"

#include <ft2build.h>
#include FT_FREETYPE_H

namespace GLOWE
{
	class Font;

	struct GLOWENGINE_EXPORT Glyph
	{
		UInt glyphIndex;
		Int2 advance, size, translation;
		unsigned int fontSize = 0;
		Hash fontHash;
		ResourceHandle<TextureAtlas> atlas;
		TextureAtlas::SubtextureDesc subtexInAtlas;
		
		Glyph() = default;
		Glyph(const Glyph&) = delete;
		Glyph(Glyph&& arg) noexcept
			: glyphIndex(arg.glyphIndex), advance(arg.advance), size(arg.size), translation(arg.translation), fontSize(arg.fontSize), fontHash(arg.fontHash), atlas(std::move(arg.atlas)), subtexInAtlas(arg.subtexInAtlas)
		{
		}
		~Glyph() = default;

		Glyph& operator=(const Glyph&) = delete;
		Glyph& operator=(Glyph&& arg) noexcept
		{
			glyphIndex = arg.glyphIndex;
			advance = arg.advance;
			size = arg.size;
			translation = arg.translation;
			fontSize = arg.fontSize;
			fontHash = arg.fontHash;
			atlas = std::move(arg.atlas);
			subtexInAtlas = arg.subtexInAtlas;

			return *this;
		}
	};

	struct GLOWENGINE_EXPORT LaidOutGlyph
	{
		Int2 advance, offset;
		unsigned int cluster, glyphIndex;
	};

	// Represents one face (only one style).
	class GLOWENGINE_EXPORT Font
		: public Lockable
	{
	public:
		struct Metrics
		{
			float height, ascender, descender;
		};
	private:
		struct Raqm;

		Metrics invalidMetrics{ 0, 0, 0 };
		bool isLoaded, hasKerning, hasColor;
		unsigned int currentFontSize;
		FT_Face face;
		Hash fontStyleHash;
		String fontStyleName;
		SharedPtr<String> fontFileData;
		UnorderedMap<unsigned int, Metrics> metrics;
		TrivialUniquePtr<Raqm> raqmInstance;
	private:
		void postLoad();
		void generateGlyphForCache(const unsigned int fontSize, const String::Utf32Char character, String& textureOut, TextureDesc& resultTexDescOut, Glyph& glyph);
		void generateGlyphForCache(const unsigned int fontSize, const unsigned int glyphId, String& textureOut, TextureDesc& resultTexDescOut, Glyph& glyph);
		bool setFontSize(const unsigned int fontSize);
	public:
		Font();
		explicit Font(const String& filename, const bool loadColor = false, const unsigned int faceID = 0);
		Font(const void* data, const UInt64 dataSize, const bool loadColor = false, const unsigned int faceID = 0);
		~Font();

		void loadFromFile(const String& filename, const bool loadColor = false, const unsigned int faceID = 0);
		void loadFromMemory(const void* data, const UInt64 dataSize, const bool loadColor = false, const unsigned int faceID = 0); // Warning! The user is obligated to free the memory and it must be valid (not freed) until the call to unload.
		void loadFromMemory(const SharedPtr<String>& data, const bool loadColor = false, const unsigned int faceID = 0);
		void unload();

		const Metrics& getFontMetrics(const unsigned int fontSize);

		bool checkIsLoaded() const;
		bool checkHasKerning() const;
		bool checkIfHasColor() const;

		const Glyph& getGlyph(const unsigned int fontSize, const LaidOutGlyph& laidOutGlyph, BasicRendererImpl& renderer);
		const Glyph& getGlyph(const unsigned int fontSize, const String::Utf32Char character, BasicRendererImpl& renderer); // Will use the font cache.
		const Glyph* getGlyphLoaded(const String::Utf32Char character, const unsigned int fontSize); // Will use the font cache.
		const FT_Face& getFace() const;

		Vector<LaidOutGlyph> layoutText(const String& utf8Text, const String& bcp47Language, const unsigned int fontSize);
		Vector<LaidOutGlyph> layoutText(const Utf32String& utf32Text, const String& bcp47Language, const unsigned int fontSize);

		Map<String::Utf32Char, unsigned int> getCharmap() const;

		Int2 getKerning(const Glyph& leftGlyph, const Glyph& rightGlyph);
		Int2 getKerning(const Glyph& leftGlyph, const Glyph& rightGlyph) const;

		Hash getHash() const;
		String getName() const;

		friend class FontCache;
	};

	class GLOWENGINE_EXPORT FontCache
		: public Subsystem, public Lockable
	{
	public:
		static constexpr char cacheName[] = "FontCache.cache";
		static constexpr unsigned int defaultSize[2] = { 256, 256 };
	private:
		struct FontInfo
		{
			ResourceHandle<TextureAtlas> atlas;
			Map<unsigned int, Glyph> glyphs;
			Map<String::Utf32Char, unsigned int> codePointsToGlyphs;
			bool wasAtlasModified; // Indicates if needs to write font info to the appropriate file.
		};
	private:
		Mutex mutex;
		Map<Hash, Map<unsigned int, FontInfo>> fonts;
		Package loadedCache;
	private:
		void useFont(Font& font, const unsigned int fontSize);
		const Glyph& createGlyph(const String::Utf32Char character, Font& font, const unsigned int fontSize, BasicRendererImpl& renderer);
		const Glyph& createGlyph(const unsigned int glyphId, Font& font, const unsigned int fontSize, BasicRendererImpl& renderer);
	public:
		void init();
		ThreadPool::TaskResult<void> cleanUpAsync(ThreadPool& threads, BasicRendererImpl& renderer); // Created for immediate renderer.

		const Glyph& getGlyph(const LaidOutGlyph& laidOutGlyph, Font& font, const unsigned int fontSize, BasicRendererImpl& renderer);
		const Glyph& getGlyph(const String::Utf32Char character, Font& font, const unsigned int fontSize, BasicRendererImpl& renderer);
		const Glyph* getGlyphLoaded(const String::Utf32Char character, Font& font, const unsigned int fontSize);
	};
	
	class GLOWENGINE_EXPORT FontUtility
		: public Subsystem
	{
	private:
		FT_Library library;
		FT_Error initResultCode;
	public:
		FontUtility();
		~FontUtility();

		bool checkIsFontSystemWorking() const;

		FT_Library& getLibrary();

		friend class Font;
	};

	template<>
	GLOWENGINE_EXPORT ResourceHandle<Font> ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToLoad);
	template<>
	GLOWENGINE_EXPORT ResourceHandle<Font> GLOWE::ResourceMgr::getDefaultResource();
}

#endif
