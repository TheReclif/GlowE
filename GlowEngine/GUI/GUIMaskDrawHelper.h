#pragma once
#ifndef GLOWE_GUI_GUIMASKDRAW_INCLUDED
#define GLOWE_GUI_GUIMASKDRAW_INCLUDED

#include "GUIRect.h"

#include "../ScriptableSubsystem.h"
#include "../Material.h"
#include "../Resource.h"

namespace GLOWE
{
	namespace Hidden
	{
		class GLOWENGINE_EXPORT GUIMaskDrawHelper
			: public Subsystem
		{
			// TODO: Clean up on Engine::cleanUp.
		private:
			Material usedMaterial;
			ResourceHandle<Geometry> guiPlane;
			Engine& engine;
		public:
			GUIMaskDrawHelper();

			void drawMask(const GUIRect& rectToDraw, BasicRendererImpl& renderer, MaterialEnvironment& matEnv);
		};
	}
}

#endif
