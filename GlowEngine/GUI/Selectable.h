#pragma once
#ifndef GLOWE_SELECTABLE_GUI_INCLUDED
#define GLOWE_SELECTABLE_GUI_INCLUDED

#include "../GameEvent.h"

namespace GLOWE
{
	/// @brief Base selectable class, allows the element to be pointed at by mouse, navigated by keyboard, etc.
	class Selectable
	{
	public:

	};

	/// @brief Parent for multiple Selectables. Processes their events and optimizes it.
	class SelectableHost
	{
	public:
		/// @brief Resolves the game event for the hosted selectables.
		/// @param event Event to resolve
		void resolveEvent(const GameEvent& event);
	};
}

#endif
