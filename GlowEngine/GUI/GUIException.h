#pragma once
#ifndef GLOWE_GUI_GUIEXCEPTION_INCLUDED
#define GLOWE_GUI_GUIEXCEPTION_INCLUDED

#include "../../GlowSystem/Error.h"

namespace GLOWE
{
	/// @brief Base exception class for GUI exceptions. Same interface as StringException.
	class GUIException
		: public StringException
	{
	public:
		using StringException::StringException;
	};
}

#endif
