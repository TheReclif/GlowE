#pragma once
#ifndef GLOWE_GUI_ELEMENT_INCLUDED
#define GLOWE_GUI_ELEMENT_INCLUDED

#include "GUIRect.h"
#include "GUIConstraint.h"

#include "../Renderable.h"
#include "../GameEvent.h"

namespace GLOWE
{
	class GUIContainer;
	class GameObject;

	class AutoFindParentOnStart
	{};

	/// @brief Base class for all GUI elements. The user can override the provided callbacks to update the element's internal state.
	///
	/// The provided callbacks are (placed in an order of calling):
	/// - onRecalcSchedule
	/// - onRectUpdated
	/// - onRecalcDone (may be called twice if the parent container requests a second update pass)
	/// - updateImpl (called after all of the elements are recalculated, just before the call to renderImpl)
	/// - renderImpl - called to render the element. May not be called every frame if the GUI view is used
	class GLOWENGINE_EXPORT GUIElement
	{
	private:
		GUIContainer* parent;
		GUIRect rect, globalRect;
		TrivialUniquePtr<GUIRect> rectToUpdate;
		enum class RectToUpdate : unsigned char
		{
			None = 0,
			Local,
			Global,
			FromParent
		};
		union
		{
			struct
			{
				RectToUpdate positionUpdate : 2, scaleUpdate : 2, sizeUpdate : 2;
			};
			unsigned char _debugUpdateView;
		};
		/// @brief List of elements in the same container that depend on this element.
		UnorderedMap<GUIElement*, unsigned int> elementsDependingOnMe;
		unsigned int lastUpdateFrame;
	protected:
		Set<GUIConstraint>* myConstraints;
	protected:
		void updateRect();
	
		/// @brief Called just to render the element.
		/// @param renderer Renderer to use (may be a command list).
		/// @param matEnv Material environment to use.
		virtual void renderImpl(BasicRendererImpl& renderer, MaterialEnvironment& matEnv) = 0;
		/// @brief Called when the object was recalculated in the same frame.
		virtual void updateImpl() {};
		/// @brief Called on every scheduleRecalc call.
		virtual void onRecalcSchedule() {};
		/// @brief Called on every updateRect call (most probably once every frame at max, depends on any calls to set*GUIRect in the frame).
		virtual void onRectUpdated() {};
		/// @brief Called after element's update. May be called more than once per frame, but twice at most during one frame.
		virtual void onRecalcDone() {};
		/// @brief Called by GUI Top when all of the updates are completed.
		virtual void onUpdateEnd() {};
		/// @brief Called to inform the element that it's being marked for redrawing.
		/// @note Should not be called directly.
		/// @param Redraw options
		virtual void markForRedraw(const unsigned char) {};
		virtual void drawMask(BasicRendererImpl& renderer, MaterialEnvironment& matEnv);
		virtual void processElementsToDraw() {};
	public:
		using ConstraintHandle = Set<GUIConstraint>::iterator;
	public:
		/// @brief Global mutex for operations on GUI.
		static RecursiveMutex guiMutex;
		using GuiMutexLock = LockGuard<decltype(guiMutex)>;
	public:
		GUIElement();
		virtual ~GUIElement() = default;

		bool autoFindContainer(GameObject* const gameObject);

		void render(BasicRendererImpl& renderer, MaterialEnvironment& matEnv);

		virtual void processGUIEvent(const GameEvent::GUIEvent& event) = 0;

		/// @brief Used to mark an element for a redraw operation.
		void markForRedraw();

		// Positioning in the rendering queue, not on the screen.
		virtual void placeAtBack();
		virtual void placeAtFront();
		virtual void placeBefore(GUIElement* const beforeWhat);

		void setLocalGUIRect(const GUIRect& newRect);
		void setGlobalGUIRect(const GUIRect& newRect);

		void setLocalPosition(const Float2& pos);
		void setLocalSize(const Float2& size);
		void setLocalScale(const Float2& scale);

		void setGlobalPosition(const Float2& pos);
		void setGlobalSize(const Float2& size);
		void setGlobalScale(const Float2& scale);

		void ensureRectWillBeUpdated();

		void scheduleRecalc();

		const GUIRect& getLocalGUIRect() const;
		const GUIRect& getGlobalGUIRect() const;

		/// @brief Returns and id of the last frame the element was updated.
		/// @return Id of last update frame.
		unsigned int getLastUpdateFrame() const;

		/// @brief Returns a pointer to the element's parent.
		/// @return Pointer to parent. Will always be not null, except GUITop, which does not have a parent.
		GUIContainer* getParent() const;

		/// @brief Checks whether the object should be rendered.
		/// @return True if the element is active (for rendering), false otherwise.
		virtual bool checkIsActiveInGUI() const;

		/// @brief Stretches the element to its parent.
		/// @param border Distance to parent's edges. Default is 0 (tightly clings to edges).
		void stretchToParent(const float border = 0.0f);
		/// @brief Adds constraints so that the element is stretched to this element ie. is contained inside of it. The only way to remove the constraints is to call clearConstraints.
		/// @param elem Which element to stretch to.
		/// @param border Distance to containing element's edges. Default is 0 (tightly clings to edges).
		void stretchToElement(GUIElement* const elem, const float border = 0.0f);
		ConstraintHandle addConstraintToThis(const GUIConstraint& constraint);
		void removeConstraintFromThis(const ConstraintHandle& handle);
		void clearConstraints();

		void orphan();
		virtual void setParent(GLOWE::GUIContainer* const newParent);

		friend class GUIContainer;
	};

	class GLOWENGINE_EXPORT GUIWindowGuide
		: public GUIElement
	{
	public:
		virtual void processGUIEvent(const GameEvent::GUIEvent& event) override {};
		virtual void renderImpl(BasicRendererImpl& renderer, MaterialEnvironment& matEnv) override {};
	};
}

#endif
