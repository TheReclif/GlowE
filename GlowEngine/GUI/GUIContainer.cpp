#include "GUIContainer.h"

#include "../GameObject.h"
#include "../Component.h"
#include "../Engine.h"
#include "../DebugDrawMgr.h"

bool GLOWE::GUIContainer::isRecursiveCycleDetectionEnabled = false;

bool GLOWE::GUIContainer::detectCycle(GUIElement* const where, const GUIConstraint& constraint) const
{
	using Type = GUIConstraint::Type;
	switch (constraint.type)
	{
	case Type::FixedUp:
	case Type::FixedDown:
	case Type::FixedLeft:
	case Type::FixedRight:
	case Type::PureDependency:
		return detectCycleFor(constraint.fixedData.whichElement, where);
	case Type::HorizontalRatio:
	case Type::VerticalRatio:
		return detectCycleFor(constraint.ratioData.leftUp, where) || detectCycleFor(constraint.ratioData.rightBottom, where);
	}
	return false;
}

bool GLOWE::GUIContainer::detectCycleFor(GUIElement* const where, GUIElement* const forWhat) const
{
	using Type = GUIConstraint::Type;
	if (where->myConstraints)
	{
		for (const auto& x : (*where->myConstraints))
		{
			switch (x.type)
			{
			case Type::FixedUp:
			case Type::FixedDown:
			case Type::FixedLeft:
			case Type::FixedRight:
			case Type::PureDependency:
				if (x.fixedData.whichElement == forWhat)
				{
					return true;
				}
				if (detectCycleFor(x.fixedData.whichElement, forWhat))
				{
					return true;
				}
				break;
			case Type::HorizontalRatio:
			case Type::VerticalRatio:
				if (x.ratioData.leftUp == forWhat || x.ratioData.rightBottom == forWhat)
				{
					return true;
				}
				if (detectCycleFor(x.ratioData.leftUp, forWhat) || detectCycleFor(x.ratioData.rightBottom, forWhat))
				{
					return true;
				}
				break;
			}
		}
	}

	return false;
}

GLOWE::GUIRect GLOWE::GUIContainer::getScissorRect() const
{
	auto result = getGlobalGUIRect();
	const auto parent = getParent();
	if (parent)
	{
		result = result.calculateIntersection(parent->getGlobalGUIRect());
	}
	return result;
}

void GLOWE::GUIContainer::setRecursiveCycleDetectionEnabled(const bool enable)
{
	isRecursiveCycleDetectionEnabled = enable;
}

bool GLOWE::GUIContainer::checkIfRecursiveCycleDetectionIsEnabled()
{
	return isRecursiveCycleDetectionEnabled;
}

void GLOWE::GUIContainer::markElementForRedraw(GUIElement* const element)
{
	if (element->parent != this)
	{
		throw GUIException("An element tried to mark itself for a redraw in the wrong parent");
	}

	elementsToRedraw[element] = (Element | Mask);
	element->markForRedraw(Element | Mask);
	if (parent)
	{
		parent->markElementForRedrawJustElement(this);
	}
}

GLOWE::GUIContainer::OrderedIterator GLOWE::GUIContainer::begin()
{
	return elementsOrder.begin();
}

GLOWE::GUIContainer::OrderedConstIterator GLOWE::GUIContainer::begin() const
{
	return elementsOrder.begin();
}

GLOWE::GUIContainer::OrderedReversedIterator GLOWE::GUIContainer::rbegin()
{
	return elementsOrder.rbegin();
}

GLOWE::GUIContainer::OrderedReversedConstIterator GLOWE::GUIContainer::rbegin() const
{
	return elementsOrder.rbegin();
}

GLOWE::GUIContainer::OrderedIterator GLOWE::GUIContainer::end()
{
	return elementsOrder.end();
}

GLOWE::GUIContainer::OrderedConstIterator GLOWE::GUIContainer::end() const
{
	return elementsOrder.end();
}

GLOWE::GUIContainer::OrderedReversedIterator GLOWE::GUIContainer::rend()
{
	return elementsOrder.rend();
}

GLOWE::GUIContainer::OrderedReversedConstIterator GLOWE::GUIContainer::rend() const
{
	return elementsOrder.rend();
}

bool GLOWE::GUIContainer::autoFindContainer(GameObject* const gameObject)
{
	if (!parent)
	{
		GameObject* tempParent = gameObject;
		GUIContainer* tempGUIParent = nullptr;

		while (!tempGUIParent && tempParent)
		{
			tempGUIParent = tempParent->getVirtualComponent<GLOWE::GUIContainer>();

			if (tempGUIParent == this)
			{
				tempGUIParent = nullptr;
				const auto components = tempParent->getVirtualComponents<GLOWE::GUIContainer>();
				for (const auto x : components)
				{
					if (x != this)
					{
						tempGUIParent = x;
						break;
					}
				}
			}

			tempParent = tempParent->getParent();
		}

		if (tempGUIParent)
		{
			tempGUIParent->addElementBack(this);
			return true;
		}
	}

	return false;
}

void GLOWE::GUIContainer::addElementFront(GUIElement* const elem)
{
	if (elem->parent)
	{
		if (elem->parent == this)
		{
			throw UnableToAddGUIElementException("Element is not an orphan (but is already a child of the container)");
		}
		throw UnableToAddGUIElementException("Element is not an orphan");
	}

	elementsOrder.emplace_front(elem);
	const auto elemIt = elements.emplace(elem, elementsOrder.begin()).first;
	elem->myConstraints = &elemIt->second.constraints;
	elem->parent = this;
	elem->scheduleRecalc();
}

void GLOWE::GUIContainer::addElementBack(GUIElement* const elem)
{
	if (elem->parent)
	{
		if (elem->parent == this)
		{
			throw UnableToAddGUIElementException("Element is not an orphan (but is already a child of the container)");
		}
		throw UnableToAddGUIElementException("Element is not an orphan");
	}

	elementsOrder.emplace_back(elem);
	const auto elemIt = elements.emplace(elem, std::prev(elementsOrder.end())).first;
	elem->myConstraints = &elemIt->second.constraints;
	elem->parent = this;
	elem->scheduleRecalc();
}

void GLOWE::GUIContainer::addElementBefore(GUIElement* const elem, GUIElement* const beforeWhat)
{
	const auto whereIt = elements.find(beforeWhat);
	if (whereIt == elements.end())
	{
		throw UnableToFindTargetElementException("beforeWhat element is not contained inside the same GUIContainer.");
	}

	if (elem->parent)
	{
		if (elem->parent == this)
		{
			throw UnableToAddGUIElementException("Element is not an orphan (but is already a child of the container)");
		}
		throw UnableToAddGUIElementException("Element is not an orphan");
	}

	elementsOrder.emplace(whereIt->second.elementHandle, elem);
	const auto elemIt = elements.emplace(elem, elementsOrder.begin()).first;
	elem->myConstraints = &elemIt->second.constraints;
	elem->parent = this;
	elem->scheduleRecalc();
}

GLOWE::String GLOWE::GUIContainer::dbgPrintRects(const unsigned int tabs, const bool printGlobal) const
{
	String result;

	for (const auto& x : elementsOrder)
	{
		const GUIRect& rect = printGlobal ? x->getGlobalGUIRect() : x->getLocalGUIRect();
		if (tabs > 0)
		{
			result += String(tabs, '\t');
		}
		result += "x: " + toString(rect.x) + '\t';
		result += "y: " + toString(rect.y) + '\t';
		result += "width: " + toString(rect.width) + '\t';
		result += "height: " + toString(rect.height) + '\n';
		const auto asContainer = dynamic_cast<GUIContainer*>(x);
		if (asContainer)
		{
			result += asContainer->dbgPrintRects(tabs + 1, printGlobal);
		}
	}

	return result;
}

void GLOWE::GUIContainer::dbgDrawRects(const Time& howLong, const Color& color) const
{
	// TODO: Test
	auto& debugDraw = getInstance<DebugDrawMgr>();
	debugDraw.addRect(getGlobalGUIRect(), color, howLong);
	for (const auto x : elementsOrder)
	{
		const GUIContainer* container = dynamic_cast<const GUIContainer*>(x);
		if (container)
		{
			container->dbgDrawRects(howLong, color);
		}
		else
		{
			debugDraw.addRect(x->getGlobalGUIRect(), color, howLong);
		}
	}
}

void GLOWE::GUIContainer::placeAtBack(GUIElement* const elem)
{
	const auto it = elements.find(elem);
	if (it == elements.end())
	{
		WarningThrow(false, "Unable to find element in the elements map.");
		return;
	}

	elementsOrder.erase(it->second.elementHandle);
	elementsOrder.emplace_back(elem);
	it->second.elementHandle = std::prev(elementsOrder.end());
}

void GLOWE::GUIContainer::placeAtFront(GUIElement* const elem)
{
	const auto it = elements.find(elem);
	if (it == elements.end())
	{
		WarningThrow(false, "Unable to find element in the elements map.");
		return;
	}

	elementsOrder.erase(it->second.elementHandle);
	elementsOrder.emplace_front(elem);
	it->second.elementHandle = elementsOrder.begin();
}

void GLOWE::GUIContainer::placeBefore(GUIElement* const elem, GUIElement* const beforeWhat)
{
	const auto itElem = elements.find(elem), beforeIt = elements.find(beforeWhat);
	if (itElem == elements.end() || beforeIt == elements.end())
	{
		WarningThrow(false, "Unable to find elem or beforeWhat in the elements map.");
		return;
	}

	elementsOrder.erase(itElem->second.elementHandle);
	itElem->second.elementHandle = elementsOrder.emplace(beforeIt->second.elementHandle, elem);
}

GLOWE::GUIContainer::ConstraintHandle GLOWE::GUIContainer::addConstraint(GUIElement* const handle, const GUIConstraint& constraint)
{
	const auto elemIt = elements.find(handle);
	if (elemIt == elements.end())
	{
		throw UnableToFindTargetElementException("Unable to find element in the elements' container");
	}

	if (isRecursiveCycleDetectionEnabled)
	{
		if (detectCycle(handle, constraint))
		{
			throw GUICycleDetected({ handle });
		}
	}

	using Type = GUIConstraint::Type;

	switch (constraint.type)
	{
	case Type::FixedUp:
	case Type::FixedDown:
	case Type::FixedLeft:
	case Type::FixedRight:
	case Type::PureDependency:
		if (elements.count(constraint.fixedData.whichElement) == 0 && constraint.fixedData.whichElement != this)
		{
			throw UnableToFindTargetElementException("Unable to find Fixed* constraint's target element in the elements' container");
		}
		++constraint.fixedData.whichElement->elementsDependingOnMe[handle];
		break;
	case Type::HorizontalRatio:
	case Type::VerticalRatio:
		if (elements.count(constraint.ratioData.leftUp) == 0 && constraint.ratioData.leftUp != this)
		{
			throw UnableToFindTargetElementException("Unable to find *Ratio constraint's left/up target element in the elements' container");
		}
		if (elements.count(constraint.ratioData.rightBottom) == 0 && constraint.ratioData.rightBottom != this)
		{
			throw UnableToFindTargetElementException("Unable to find *Ratio constraint's right/bottom target element in the elements' container");
		}
		++constraint.ratioData.leftUp->elementsDependingOnMe[handle];
		++constraint.ratioData.rightBottom->elementsDependingOnMe[handle];
		break;
	case Type::KeepAspectRatio:
		if (constraint.keepAspectRatioData.aspectRatio <= 0.0f)
		{
			throw InvalidGUIConstraintData("Invalid aspect ratio (ar <= 0)");
		}
		break;
	case Type::MatchContentHeight:
	case Type::MatchContentWidth:
		if (dynamic_cast<GUIContainer*>(handle) == nullptr)
		{
			throw InvalidGUIConstraintData("Unable to add MatchContent* constraint to a non-container GUI element");
		}
		break;
	}

	return elemIt->second.constraints.emplace(constraint).first;
}

GLOWE::GUIContainer::ConstraintHandle GLOWE::GUIContainer::addConstraint(GUIElement& handle, const GUIConstraint& constraint)
{
	return addConstraint(&handle, constraint);
}

void GLOWE::GUIContainer::removeConstraint(GUIElement* const element, const ConstraintHandle& constraint)
{
	const auto elemIt = elements.find(element);
	if (elemIt == elements.end())
	{
		throw UnableToFindTargetElementException("Unable to find element in the elements' container");
	}
	
	unbindConstraint(element, *constraint);

	elemIt->second.constraints.erase(constraint);
	element->scheduleRecalc();
}

void GLOWE::GUIContainer::removeConstraint(GUIElement& element, const ConstraintHandle& constraint)
{
	removeConstraint(&element, constraint);
}

void GLOWE::GUIContainer::removeElement(GUIElement* const handle)
{
	const auto elemIt = elements.find(handle);
	if (elemIt == elements.end())
	{
		throw UnableToFindTargetElementException("Unable to find element in the elements' container");
	}

	uninvalidateElement(handle);

	for (const auto& x : elemIt->second.constraints)
	{
		unbindConstraint(handle, x);
	}

	if (myConstraints && ((myConstraints->count(GUIConstraint(GUIConstraint::Type::MatchContentHeight)) > 0) || (myConstraints->count(GUIConstraint(GUIConstraint::Type::MatchContentWidth)) > 0)))
	{
		scheduleRecalc();
	}

	using Type = GUIConstraint::Type;
	for (auto x = handle->elementsDependingOnMe.begin(); x != handle->elementsDependingOnMe.end();)
	{
		// Ignore all children - they (and their constraints) stay.
		if (x->first->getParent() == handle)
		{
			++x;
			continue;
		}

		auto& constraintsContainer = *x->first->myConstraints;
		for (auto it = constraintsContainer.begin(); it != constraintsContainer.end();)
		{
			switch (it->type)
			{
			case Type::FixedUp:
			case Type::FixedDown:
			case Type::FixedLeft:
			case Type::FixedRight:
			case Type::PureDependency:
				if (it->fixedData.whichElement == handle)
				{
					it = constraintsContainer.erase(it);
				}
				else
				{
					++it;
				}
				break;
			case Type::HorizontalRatio:
			case Type::VerticalRatio:
				if (it->ratioData.leftUp == handle || it->ratioData.rightBottom == handle)
				{
					it = constraintsContainer.erase(it);
				}
				else
				{
					++it;
				}
				break;
			default:
				++it;
				break;
			}
		}
		x->first->scheduleRecalc();
		x = handle->elementsDependingOnMe.erase(x);
	}

	handle->myConstraints = nullptr;
	handle->parent = nullptr;
	elementsOrder.erase(elemIt->second.elementHandle);
	elements.erase(elemIt);
}

void GLOWE::GUIContainer::removeElement(GUIElement& handle)
{
	removeElement(&handle);
}

void GLOWE::GUIContainer::clearConstraints(GUIElement* const handle)
{
	const auto elemIt = elements.find(handle);
	if (elemIt == elements.end())
	{
		throw UnableToFindTargetElementException("Unable to find element in the elements' container");
	}

	for (const auto& x : elemIt->second.constraints)
	{
		unbindConstraint(handle, x);
	}
	elemIt->second.constraints.clear();

	handle->scheduleRecalc();
}

void GLOWE::GUIContainer::clearConstraints(GUIElement& handle)
{
	clearConstraints(&handle);
}

bool GLOWE::GUIContainer::checkIsScheduledForRecalc(GUIElement* const handle) const
{
	return invalidatedElements.count(handle) > 0;
}

bool GLOWE::GUIContainer::checkIsAnythingToRedraw() const
{
	return !elementsToRedraw.empty();
}

void GLOWE::GUIContainer::processGUIEvent(const GameEvent::GUIEvent& event)
{
	for (auto& elem : elementsOrder)
	{
		if (elem != event.whichElement && elem->checkIsActiveInGUI())
		{
			bool testResult = false;
			GameEvent::GUIEvent tempEvent = event;
			switch (event.type)
			{
			case GameEvent::GUIEvent::Type::MouseButtonUpInside:
			case GameEvent::GUIEvent::Type::MouseButtonDownInside:
				testResult = elem->getGlobalGUIRect().isPointInside({ static_cast<float>(event.mouseButtonInfo.x), static_cast<float>(event.mouseButtonInfo.y) });
				break;
			case GameEvent::GUIEvent::Type::MouseMovedInside:
			case GameEvent::GUIEvent::Type::MouseEntered:
			case GameEvent::GUIEvent::Type::MouseLeft:
				testResult = elem->getGlobalGUIRect().isPointInside({ static_cast<float>(event.mouseMoveInfo.x), static_cast<float>(event.mouseMoveInfo.y) });
				if (testResult)
				{
					if (elementsWithMouseInside.count(elem) == 0)
					{
						elementsWithMouseInside.emplace(elem);

						tempEvent.type = GameEvent::GUIEvent::Type::MouseEntered;
						tempEvent.mouseMoveInfo = event.mouseMoveInfo;
					}
				}
				else
				{
					const auto elemIt = elementsWithMouseInside.find(elem);
					if (elemIt != elementsWithMouseInside.end())
					{
						elementsWithMouseInside.erase(elemIt);
						testResult = true;

						tempEvent.type = GameEvent::GUIEvent::Type::MouseLeft;
						tempEvent.mouseMoveInfo = event.mouseMoveInfo;
					}
				}
				break;
			}

			if (testResult)
			{
				tempEvent.whichElement = elem;
				elem->processGUIEvent(tempEvent);
			}
		}
	}
}

void GLOWE::GUIContainer::update()
{
	updateRect();
	onBeforeElementsUpdate();

	const auto sortedListValidFor = invalidatedElements.size();
	if (!sortedListValidFor)
	{
		return;
	}

	auto sortedList = topoSort();
	const unsigned int frameId = getInstance<Engine>().getCurrentFrameId();
	for (const auto x : sortedList)
	{
		recalcElement(x);
		x->onRecalcDone();
		x->lastUpdateFrame = frameId;
	}

	if (onAfterElementsUpdate())
	{
		if (sortedListValidFor != invalidatedElements.size())
		{
			sortedList.clear();
			topoSort(sortedList);
		}

		for (const auto x : sortedList)
		{
			recalcElement(x);
			x->onRecalcDone();
		}
	}

	if (myConstraints && (!elements.empty()))
	{
		GUIConstraint tempFind(GUIConstraint::Type::MatchContentHeight);
		const bool isMatchHeightPresent = (myConstraints->count(tempFind) > 0);
		tempFind.type = GUIConstraint::Type::MatchContentWidth;
		const bool isMatchWidthPresent = (myConstraints->count(tempFind) > 0);
		if (isMatchHeightPresent || isMatchWidthPresent)
		{
			//float minEdgeX = std::numeric_limits<float>::infinity(), maxEdgeX = -std::numeric_limits<float>::infinity(), minEdgeY = std::numeric_limits<float>::infinity(), maxEdgeY = -std::numeric_limits<float>::infinity();
			float minEdgeX = 0, maxEdgeX = 0, minEdgeY = 0, maxEdgeY = 0;
			for (const auto& x : elements)
			{
				if (!x.first->checkIsActiveInGUI())
				{
					continue;
				}

				const auto& elemRect = x.first->getLocalGUIRect();
				const auto scaledSize = elemRect.getScaledSize();
				if (elemRect.x < minEdgeX)
				{
					minEdgeX = elemRect.x;
				}
				if ((elemRect.x + scaledSize[0]) > maxEdgeX)
				{
					maxEdgeX = elemRect.x + scaledSize[0];
				}

				if (elemRect.y < minEdgeY)
				{
					minEdgeY = elemRect.y;
				}
				if ((elemRect.y + scaledSize[1]) > maxEdgeY)
				{
					maxEdgeY = elemRect.y + scaledSize[1];
				}
			}

			if (isMatchHeightPresent)
			{
				rect.height = maxEdgeY - minEdgeY;
				globalRect.height = rect.height * globalRect.yScale;
			}

			if (isMatchWidthPresent)
			{
				rect.width = maxEdgeX - minEdgeX;
				globalRect.width = rect.width * globalRect.xScale;
			}
		}
	}
}

void GLOWE::GUIContainer::recalcElement(GUIElement* const element)
{
	element->updateRect();
	float leftEdge, rightEdge, upEdge, bottomEdge;
	{
		const auto& elementGlobalRect = element->getGlobalGUIRect();
		const auto scaledSize = elementGlobalRect.getScaledSize();
		leftEdge = std::round(elementGlobalRect.x);
		rightEdge = std::round(leftEdge + scaledSize[0]);
		upEdge = std::round(elementGlobalRect.y);
		bottomEdge = std::round(upEdge + scaledSize[1]);
	}

	using Type = GUIConstraint::Type;
	using Edge = GUIConstraint::Edge;
	const auto keepARConstraint = element->myConstraints->find(GUIConstraint(Type::KeepAspectRatio));
	if (keepARConstraint != element->myConstraints->end())
	{
		if (keepARConstraint->keepAspectRatioData.prioritizeWidthResize)
		{
			const float height = bottomEdge - upEdge, width = height * keepARConstraint->keepAspectRatioData.aspectRatio;
			rightEdge = leftEdge + width;
		}
		else
		{
			const float width = rightEdge - leftEdge, height = width / keepARConstraint->keepAspectRatioData.aspectRatio;
			bottomEdge = upEdge + height;
		}
	}

	const auto templateSize = element->getLocalGUIRect().getScaledSize();
	bool isXLocked = false, isYLocked = false;

	for (const auto& x : (*element->myConstraints))
	{
		switch (x.type)
		{
		case Type::FixedDown:
		{
			const auto& guiRect = x.fixedData.whichElement->getGlobalGUIRect();
			float destEdge = guiRect.y;
			switch(x.fixedData.whichEdge)
			{
			case Edge::RightDown:
				destEdge += guiRect.getScaledSize()[1];
				break;
			case Edge::LeftUp:
				break;
			}
			bottomEdge = destEdge - x.fixedData.distance;
			if (!isYLocked)
			{
				isYLocked = true;
				upEdge = bottomEdge - templateSize[1];
			}
		}
			break;
		case Type::FixedUp:
		{
			const auto& guiRect = x.fixedData.whichElement->getGlobalGUIRect();
			float destEdge = guiRect.y;
			switch (x.fixedData.whichEdge)
			{
			case Edge::RightDown:
				destEdge += guiRect.getScaledSize()[1];
				break;
			case Edge::LeftUp:
				break;
			}
			upEdge = destEdge + x.fixedData.distance;
			if (!isYLocked)
			{
				isYLocked = true;
				bottomEdge = upEdge + templateSize[1];
			}
		}
			break;
		case Type::FixedLeft:
		{
			const auto& guiRect = x.fixedData.whichElement->getGlobalGUIRect();
			float destEdge = guiRect.x;
			switch (x.fixedData.whichEdge)
			{
			case Edge::RightDown:
				destEdge += guiRect.getScaledSize()[0];
				break;
			case Edge::LeftUp:
				break;
			}
			leftEdge = destEdge + x.fixedData.distance;
			if (!isXLocked)
			{
				isXLocked = true;
				rightEdge = leftEdge + templateSize[0];
			}
		}
			break;
		case Type::FixedRight:
		{
			const auto& guiRect = x.fixedData.whichElement->getGlobalGUIRect();
			float destEdge = guiRect.x;
			switch (x.fixedData.whichEdge)
			{
			case Edge::RightDown:
				destEdge += guiRect.getScaledSize()[0];
				break;
			case Edge::LeftUp:
				break;
			}
			rightEdge = destEdge - x.fixedData.distance;
			if (!isXLocked)
			{
				isXLocked = true;
				leftEdge = rightEdge - templateSize[0];
			}
		}
			break;
		case Type::HorizontalRatio:
		{
			if (isXLocked)
			{
				continue;
			}

			const GUIRect& leftRect = x.ratioData.leftUp->getGlobalGUIRect(), & rightRect = x.ratioData.rightBottom->getGlobalGUIRect();
			const auto leftSize = leftRect.getScaledSize(), rightSize = rightRect.getScaledSize();
			const float leftTargetEdge = x.ratioData.whichEdgeLeftUp == GUIConstraint::Edge::LeftUp ? leftRect.x : leftRect.x + leftSize[0], rightTargetEdge = x.ratioData.whichEdgeRightBottom == GUIConstraint::Edge::LeftUp ? rightRect.x : rightRect.x + rightSize[0];
			const float segmentToDivide = rightTargetEdge - leftTargetEdge - templateSize[0], segmentPart = segmentToDivide / (x.ratioData.leftUpPart + x.ratioData.rightBottomPart);
			float leftSegment = segmentPart * x.ratioData.leftUpPart, rightSegment = segmentToDivide - leftSegment;

			if ((x.ratioData.leftUpPart + x.ratioData.rightBottomPart) < MathHelper::Epsilon)
			{
				leftSegment = rightSegment = (segmentToDivide / 2.0f);
			}

			leftEdge = leftTargetEdge + leftSegment;
			rightEdge = rightTargetEdge - rightSegment;

			isXLocked = true;
		}
			break;
		case Type::VerticalRatio:
		{
			if (isYLocked)
			{
				continue;
			}

			const GUIRect& upRect = x.ratioData.leftUp->getGlobalGUIRect(), & downRect = x.ratioData.rightBottom->getGlobalGUIRect();
			const auto upSize = upRect.getScaledSize(), downSize = downRect.getScaledSize();
			const float upTargetEdge = x.ratioData.whichEdgeLeftUp == GUIConstraint::Edge::LeftUp ? upRect.y : upRect.y + upSize[1], downTargetEdge = x.ratioData.whichEdgeRightBottom == GUIConstraint::Edge::LeftUp ? downRect.y : downRect.y + downSize[1];
			const float segmentToDivide = downTargetEdge - upTargetEdge - templateSize[1], segmentPart = segmentToDivide / (x.ratioData.leftUpPart + x.ratioData.rightBottomPart);

			float upSegment = segmentPart * x.ratioData.leftUpPart, downSegment = segmentToDivide - upSegment;
			if ((x.ratioData.leftUpPart + x.ratioData.rightBottomPart) < MathHelper::Epsilon)
			{
				upSegment = downSegment = (segmentToDivide / 2.0f);
			}

			upEdge = upTargetEdge + upSegment;
			bottomEdge = downTargetEdge - downSegment;

			isYLocked = true;
		}
			break;
		}
	}

	element->globalRect.x = leftEdge;
	element->globalRect.width = rightEdge - leftEdge;
	element->globalRect.y = upEdge;
	element->globalRect.height = bottomEdge - upEdge;

	element->rect.pos = (Vector2F(element->globalRect.pos) - Vector2F(globalRect.pos)).toFloat2();
	element->rect.size = (Vector2F(element->globalRect.size) / Vector2F(element->globalRect.scale)).toFloat2();
}

void GLOWE::GUIContainer::markElementForRedrawJustElement(GUIElement* const element)
{
	if (parent && elementsToRedraw.empty())
	{
		parent->markElementForRedrawJustElement(this);
	}
	elementsToRedraw[element] = (Element | Mask);
}

void GLOWE::GUIContainer::renderImpl(BasicRendererImpl& renderer, MaterialEnvironment& matEnv)
{
	if (elementsToRedraw.empty())
	{
#ifdef DEBUG
		//DebugBreak();
#endif
		return;
	}

	const auto scissorRect = getScissorRect();
	const auto scaledSize = scissorRect.getScaledSize();
	if (scaledSize[0] <= 0 || scaledSize[1] <= 0)
	{
		return;
	}
	for (const auto x : elementsOrder)
	{
		// Maybe (x.second | Element) is needed here?
		const auto it = elementsToRedraw.find(x);
		if (it != elementsToRedraw.end() && (it->second & Element))
		{
			const auto temp = x->getGlobalGUIRect().calculateIntersection(scissorRect);
			if (temp.width > 0 && temp.height > 0)
			{
				renderer.setScissorRect(temp.toRect());
				x->render(renderer, matEnv);
			}
		}
	}
	elementsToRedraw.clear();
}

void GLOWE::GUIContainer::onRectUpdated()
{
	for (auto& x : elementsOrder)
	{
		x->ensureRectWillBeUpdated();
	}
}

void GLOWE::GUIContainer::onRecalcDone()
{
	update();
}

void GLOWE::GUIContainer::onRecalcSchedule()
{
	for (auto& x : elementsOrder)
	{
		x->scheduleRecalc();
	}
}

void GLOWE::GUIContainer::onUpdateEnd()
{
	for (const auto& x : invalidatedElements)
	{
		x.first->onUpdateEnd();
	}
	invalidatedElements.clear();
}

void GLOWE::GUIContainer::markForRedraw(const unsigned char options)
{
	for (auto& x : elements)
	{
		x.first->markForRedraw(options);
		elementsToRedraw[x.first] |= Element;
	}
}

void GLOWE::GUIContainer::drawMask(BasicRendererImpl& renderer, MaterialEnvironment& matEnv)
{
	if (elementsToRedraw.empty())
	{
#ifdef DEBUG
		//DebugBreak();
#endif
		return;
	}

	const auto scissorRect = getScissorRect();
	const auto scaledSize = scissorRect.getScaledSize();
	if (scaledSize[0] <= 0 || scaledSize[1] <= 0)
	{
		return;
	}
	for (const auto& x : elementsToRedraw)
	{
		if (x.second & Mask)
		{
			const auto temp = x.first->getGlobalGUIRect().calculateIntersection(scissorRect);
			if (temp.width > 0 && temp.height > 0)
			{
				renderer.setScissorRect(temp.toRect());
				x.first->drawMask(renderer, matEnv);
			}
		}
	}
}

void GLOWE::GUIContainer::processElementsToDraw()
{
	if (elementsToRedraw.empty())
	{
		return;
	}

	Vector<const GUIRect*> rectsToTest;
	rectsToTest.reserve(elementsToRedraw.size());
	for (const auto& x : elementsToRedraw)
	{
		if (x.second & Mask)
		{
			rectsToTest.emplace_back(&x.first->getGlobalGUIRect());
		}
	}

	const auto elementsToRedrawEnd = elementsToRedraw.end();
	for (const auto& x : elements)
	{
		// TODO: HACK: This is a shity sollution. What I think is happening here is: when there are two elements overlapping and the element A gets marked for a redraw, the element B gets marked as well. It propagates this information deeper into its hierarchy tree, but if the element B was marked earlier by e.g. its child, the element B gets skipped by this logic and is not marked recursively for redrawal. It causes some weird graphics glitches in the overlapping area.
		// Note: this comment was made for the commented out version of the code. The current fix is the second check in the if clause.
		const auto it = elementsToRedraw.find(x.first);
		if (it != elementsToRedrawEnd && !(it->second & Element))
		{
			x.first->processElementsToDraw();
			continue;
		}

		for (const auto& y : rectsToTest)
		{
			if (x.first->getGlobalGUIRect().isRectIntersecting(*y))
			{
				elementsToRedraw.emplace(x.first, Element);
				x.first->markForRedraw(Element);
				x.first->processElementsToDraw();
				break;
			}
		}
	}
}

GLOWE::Vector<GLOWE::GUIElement*> GLOWE::GUIContainer::topoSort()
{
	Vector<GUIElement*> sortedList;
	topoSort(sortedList);
	return sortedList;
}

void GLOWE::GUIContainer::topoSort(Vector<GUIElement*>& sortedElements)
{
	sortedElements.reserve(invalidatedElements.size());
	persistentCounters.reserve(invalidatedElements.size());
	for (const auto& x : invalidatedElements)
	{
		populateTopoCounters(x.first);
	}

	while (!persistentElementQueue.empty())
	{
		GUIElement* const currElem = persistentElementQueue.front();
		sortedElements.emplace_back(currElem);

		for (const auto& x : currElem->elementsDependingOnMe)
		{
			checkElementForTopoQueue(x.first, x.second);
		}

		persistentElementQueue.pop();
	}

	if (sortedElements.size() != invalidatedElements.size())
	{
		Vector<GUIElement*> cycled;
		for (const auto& x : persistentCounters)
		{
			if (x.second > 0)
			{
				cycled.emplace_back(x.first);
			}
		}

		persistentCounters.clear();

		throw GUICycleDetected(std::move(cycled));
	}

	persistentCounters.clear();
}

void GLOWE::GUIContainer::populateTopoCounters(GUIElement* const element)
{
	if (persistentCounters.count(element) > 0)
	{
		return;
	}

	unsigned int edges = 0;
	for (const auto& constraint : (*element->myConstraints))
	{
		switch (constraint.type)
		{
		case GUIConstraint::Type::FixedDown:
		case GUIConstraint::Type::FixedUp:
		case GUIConstraint::Type::FixedLeft:
		case GUIConstraint::Type::FixedRight:
		case GUIConstraint::Type::PureDependency:
			if ((constraint.fixedData.whichElement != this) && (invalidatedElements.count(constraint.fixedData.whichElement) > 0))
			{
				++edges;
			}
			break;
		case GUIConstraint::Type::VerticalRatio:
		case GUIConstraint::Type::HorizontalRatio:
			if ((constraint.ratioData.leftUp != this) && (invalidatedElements.count(constraint.ratioData.leftUp) > 0))
			{
				++edges;
			}
			if ((constraint.ratioData.rightBottom != this) && (invalidatedElements.count(constraint.ratioData.rightBottom) > 0))
			{
				++edges;
			}
			break;
		}
	}

	if (edges == 0)
	{
		persistentElementQueue.push(element);
	}

	persistentCounters.emplace(element, edges);
}

void GLOWE::GUIContainer::checkElementForTopoQueue(GUIElement* const element, const unsigned int edgesToRemove)
{
	if (element != this)
	{
		const auto it = persistentCounters.find(element);
		if (it != persistentCounters.end() && it->second > 0)
		{
			if ((it->second -= edgesToRemove) == 0)
			{
				persistentElementQueue.push(element);
			}
		}
	}
}

void GLOWE::GUIContainer::invalidateElement(GUIElement* const element, const InvalidationType inval)
{
	if (inval == InvalidationType::None)
	{
		throw GUIException("inval cannot be InvalidationType::None");
	}
	if (element->parent != this)
	{
		throw GUIException("Element " + toString(element) + " tried to invalidate itself in the wrong object (parent not matching \"this\")");
	}
	if (parent && invalidatedElements.empty())
	{
		const auto parentInval = ((myConstraints->count(GUIConstraint(GUIConstraint::Type::MatchContentHeight)) > 0) || (myConstraints->count(GUIConstraint(GUIConstraint::Type::MatchContentHeight)) > 0)) ? InvalidationType::IsInvalidItself : InvalidationType::ContainsInvalid;
		
		parent->invalidateElement(this, parentInval);
	}
	const auto it = invalidatedElements.find(element);
	if (it == invalidatedElements.end())
	{
		invalidatedElements.emplace(element, inval);
	}
	else
	{
		if (inval == InvalidationType::IsInvalidItself || it->second == InvalidationType::ContainsInvalid)
		{
			it->second = inval;
		}
	}

	markElementForRedraw(element);
}

void GLOWE::GUIContainer::uninvalidateElement(GUIElement* const element)
{
	if (element->parent != this)
	{
		throw GUIException("Element " + toString(element) + " tried to uninvalidate itself in the wrong object (parent not matching \"this\")");
	}

	const auto it = invalidatedElements.find(element);
	if (it == invalidatedElements.end())
	{
		return;
	}

	elementsToRedraw.erase(element);
	invalidatedElements.erase(it);
	if (invalidatedElements.empty() && parent)
	{
		if (parent->getInvalidationInfo(this) != InvalidationType::IsInvalidItself)
		{
			parent->uninvalidateElement(this);
		}
	}
}

GLOWE::GUIContainer::InvalidationType GLOWE::GUIContainer::getInvalidationInfo(GUIElement* const element) const
{
	const auto it = invalidatedElements.find(element);
	if (it != invalidatedElements.end())
	{
		return it->second;
	}

	return InvalidationType::None;
}

void GLOWE::GUIContainer::unbindConstraint(GUIElement* const element, const GUIConstraint& constraint)
{
	using Type = GUIConstraint::Type;
	switch (constraint.type)
	{
	case Type::FixedUp:
	case Type::FixedDown:
	case Type::FixedLeft:
	case Type::FixedRight:
	case Type::PureDependency:
	{
		const auto it = constraint.fixedData.whichElement->elementsDependingOnMe.find(element);
		--it->second;
		if (it->second == 0)
		{
			constraint.fixedData.whichElement->elementsDependingOnMe.erase(it);
		}
	}
	break;
	case Type::HorizontalRatio:
	case Type::VerticalRatio:
	{
		const auto leftUpIt = constraint.ratioData.leftUp->elementsDependingOnMe.find(element),
			rightBottomIt = constraint.ratioData.rightBottom->elementsDependingOnMe.find(element);
		--leftUpIt->second;
		--rightBottomIt->second;
		if (leftUpIt->second == 0)
		{
			constraint.ratioData.leftUp->elementsDependingOnMe.erase(leftUpIt);
		}
		if (rightBottomIt->second == 0)
		{
			constraint.ratioData.rightBottom->elementsDependingOnMe.erase(rightBottomIt);
		}
	}
	break;
	}
}
