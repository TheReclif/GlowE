#include "Text.h"

#include "../Resource.h"
#include "../Engine.h"

#include "../../GlowSystem/AsyncIO.h"
#include "../../GlowSystem/Error.h"

#include "../../GlowGraphics/Uniform/GraphicsDeviceImplementation.h"
#include "../../GlowGraphics/Uniform/GraphicsFactory.h"

#include <raqm.h>

constexpr char GLOWE::FontCache::cacheName[];
constexpr unsigned int GLOWE::FontCache::defaultSize[2];

struct GLOWE::Font::Raqm
{
	raqm_t* rq;
	
	Raqm()
		: rq(raqm_create())
	{
		if (!rq)
		{
			throw Exception("Unable to create a new raqm_t instance");
		}
	}

	~Raqm()
	{
		raqm_destroy(rq);
	}
	
	Vector<LaidOutGlyph> layoutText(const String& utf8String, const Font& font, const String& bcp47Language)
	{
		const auto dir = RAQM_DIRECTION_DEFAULT;
		const auto textLen = utf8String.getSize();
		raqm_clear_contents(rq);
		if (!raqm_set_text_utf8(rq, utf8String.getCharArray(), textLen))
		{
			throw Exception("\"raqm_set_text_utf8\" failed");
		}

		if (!raqm_set_freetype_face(rq, font.getFace()))
		{
			throw Exception("\"raqm_set_freetype_face\" failed");
		}

		if (!raqm_set_par_direction(rq, dir))
		{
			throw Exception("\"raqm_set_par_direction\" failed");
		}

		if (!raqm_set_language(rq, bcp47Language.getCharArray(), 0, textLen))
		{
			throw Exception("\"raqm_set_language\" failed");
		}

		if (!raqm_layout(rq))
		{
			throw Exception("\"raqm_layout\" failed");
		}

		size_t glyphCount = 0;
		const auto glyphs = raqm_get_glyphs(rq, &glyphCount);

		const auto finalGlyphCount = glyphCount;
		
		Vector<LaidOutGlyph> result(finalGlyphCount);
		for (size_t x = 0; x < finalGlyphCount; ++x)
		{
			const auto& glyph = glyphs[x];
			result[x].cluster = glyph.cluster;
			result[x].advance = { glyph.x_advance >> 6, glyph.y_advance >> 6 };
			result[x].offset = { glyph.x_offset >> 6, glyph.y_offset >> 6 };
			result[x].glyphIndex = glyph.index;
		}

		return result;
	}

	Vector<LaidOutGlyph> layoutText(const Utf32String& utf32String, const Font& font, const String& bcp47Language)
	{
		const auto dir = RAQM_DIRECTION_DEFAULT;
		const auto textLen = utf32String.size();
		raqm_clear_contents(rq);
		if (!raqm_set_text(rq, reinterpret_cast<const uint32_t*>(utf32String.c_str()), textLen))
		{
			throw Exception("\"raqm_set_text_utf8\" failed");
		}

		if (!raqm_set_freetype_face(rq, font.getFace()))
		{
			throw Exception("\"raqm_set_freetype_face\" failed");
		}

		if (!raqm_set_par_direction(rq, dir))
		{
			throw Exception("\"raqm_set_par_direction\" failed");
		}

		if (!raqm_set_language(rq, bcp47Language.getCharArray(), 0, textLen))
		{
			throw Exception("\"raqm_set_language\" failed");
		}

		if (!raqm_layout(rq))
		{
			throw Exception("\"raqm_layout\" failed");
		}

		size_t glyphCount = 0;
		const auto glyphs = raqm_get_glyphs(rq, &glyphCount);

		const auto finalGlyphCount = glyphCount;

		Vector<LaidOutGlyph> result(finalGlyphCount);
		for (size_t x = 0; x < finalGlyphCount; ++x)
		{
			const auto& glyph = glyphs[x];
			result[x].cluster = glyph.cluster;
			result[x].advance = { glyph.x_advance >> 6, glyph.y_advance >> 6 };
			result[x].offset = { glyph.x_offset >> 6, glyph.y_offset >> 6 };
			result[x].glyphIndex = glyph.index;
		}

		return result;
	}
};

GLOWE::FontUtility::FontUtility()
	: library(), initResultCode(FT_Err_Ignore)
{
	initResultCode = FT_Init_FreeType(&library);
	if (initResultCode)
	{
		WarningThrow(false, "Unable to init FreeType. Error code: " + toString(initResultCode));
	}
}

GLOWE::FontUtility::~FontUtility()
{
	if (checkIsFontSystemWorking())
	{
		FT_Done_FreeType(library);
	}
}

bool GLOWE::FontUtility::checkIsFontSystemWorking() const
{
	return initResultCode == FT_Err_Ok;
}

FT_Library& GLOWE::FontUtility::getLibrary()
{
	return library;
}

void GLOWE::Font::postLoad()
{
	hasKerning = FT_HAS_KERNING(face);
	FT_Error charmapError = FT_Select_Charmap(face, FT_ENCODING_UNICODE);

	if (charmapError != FT_Err_Ok)
	{
		WarningThrow(false, "Font does not support Unicode.");
	}

	String& fontName = fontStyleName;
	fontName.clear();
	if (face->family_name)
	{
		fontName += face->family_name;
	}
	if (face->style_name)
	{
		fontName += ' ';
		fontName += face->style_name;
	}

	fontStyleHash = fontName;
}

void GLOWE::Font::generateGlyphForCache(const unsigned int fontSize, const String::Utf32Char character, String& textureOut, TextureDesc& resultTexDescOut, Glyph& glyph)
{
	const auto glyphIndex = FT_Get_Char_Index(face, character);
	if (glyphIndex == 0)
	{
		WarningThrow(false, "Undefined character code: " + String(character) + ".");
		return;
	}
	generateGlyphForCache(fontSize, glyphIndex, textureOut, resultTexDescOut, glyph);
}

void GLOWE::Font::generateGlyphForCache(const unsigned fontSize, const unsigned glyphId, String& textureOut,
	TextureDesc& resultTexDescOut, Glyph& glyph)
{
	if (!setFontSize(fontSize))
	{
		return;
	}
	glyph.glyphIndex = glyphId;
	FT_GlyphSlot slot = face->glyph;
	const FT_Int32 loadFlags = hasColor ? FT_LOAD_COLOR : FT_LOAD_RENDER;
	FT_Error error = FT_Load_Glyph(face, glyph.glyphIndex, loadFlags);
	if (error != FT_Err_Ok)
	{
		WarningThrow(false, "Unable to load/render a glyph for id: " + toString(glyphId) + ". Error code: " + toString(error));
		return;
	}
	glyph.advance = { slot->advance.x >> 6, slot->advance.y >> 6 };
	glyph.size = { slot->metrics.width >> 6, slot->metrics.height >> 6 };
	glyph.translation = { slot->bitmap_left, -slot->bitmap_top };
	glyph.fontHash = fontStyleHash;
	glyph.fontSize = fontSize;

	if (slot->bitmap.width > 0 && slot->bitmap.rows > 0)
	{
		resultTexDescOut.format = Format(Format::Type::UnsignedByte, hasColor ? 4 : 1);

		resultTexDescOut.arraySize = 1;
		resultTexDescOut.binding = TextureDesc::Binding::Default;
		resultTexDescOut.width = slot->bitmap.width;
		resultTexDescOut.height = slot->bitmap.rows;
		resultTexDescOut.depth = 1;
		resultTexDescOut.mipmapsCount = 1;
		resultTexDescOut.type = TextureDesc::Type::Texture2D;

		textureOut.resize(resultTexDescOut.format.getSize() * resultTexDescOut.width * resultTexDescOut.height);
		std::memcpy(textureOut.getCharArray(), slot->bitmap.buffer, textureOut.getSize());
	}
}

bool GLOWE::Font::setFontSize(const unsigned fontSize)
{
	if (currentFontSize != fontSize)
	{
		const FT_Error error = FT_Set_Pixel_Sizes(face, 0, fontSize);
		if (error != FT_Err_Ok)
		{
			WarningThrow(false, "Unable to set font size: " + toString(fontSize) + ". Error code: " + toString(error));
			return false;
		}
		currentFontSize = fontSize;
	}

	return true;
}

GLOWE::Font::Font()
	: isLoaded(false), hasKerning(false), hasColor(false), currentFontSize(0), face(), fontStyleHash(), fontStyleName(), fontFileData(), raqmInstance(makeTrivialUniquePtr<Raqm>())
{
}

GLOWE::Font::Font(const String& filename, const bool loadColor, const unsigned int faceID)
	: isLoaded(false), hasKerning(false), hasColor(false), currentFontSize(0), face(), fontStyleHash(), fontStyleName(), fontFileData(), raqmInstance(makeTrivialUniquePtr<Raqm>())
{
	loadFromFile(filename, loadColor, faceID);
}

GLOWE::Font::Font(const void* data, const UInt64 dataSize, const bool loadColor, const unsigned int faceID)
	: isLoaded(false), hasKerning(false), hasColor(false), currentFontSize(0), face(), fontStyleHash(), fontStyleName(), fontFileData(), raqmInstance(makeTrivialUniquePtr<Raqm>())
{
	loadFromMemory(data, dataSize, loadColor, faceID);
}

GLOWE::Font::~Font()
{
	unload();
}

void GLOWE::Font::loadFromFile(const String& filename, const bool loadColor, const unsigned int faceID)
{
	unload();
	File fontFile;
	fontFile.open(filename, File::OpenFlag::Binary | File::OpenFlag::In);
	if (!fontFile.checkIsOpen())
	{
		isLoaded = FT_Err_Cannot_Open_Resource;
		return;
	}
	fontFileData = makeSharedPtr<String>(fontFile.getFileSize());
	fontFile.read(fontFileData->getCharArray(), fontFileData->getSize());
	fontFile.close();

	// Same as loadFromMemory where data is SharedPtr<String>.
	FT_Error error = FT_New_Memory_Face(getInstance<FontUtility>().getLibrary(), reinterpret_cast<const FT_Byte*>(fontFileData->getCharArray()), fontFileData->getSize(), faceID, &face);
	isLoaded = error == FT_Err_Ok;
	hasColor = loadColor;
	if (isLoaded)
	{
		postLoad();
	}
	else
	{
		fontFileData.reset();
	}
}

void GLOWE::Font::loadFromMemory(const void* data, const UInt64 dataSize, const bool loadColor, const unsigned int faceID)
{
	unload();
	FT_Error error = FT_New_Memory_Face(getInstance<FontUtility>().getLibrary(), static_cast<const FT_Byte*>(data), dataSize, faceID, &face);
	isLoaded = error == FT_Err_Ok;
	hasColor = loadColor;
	if (isLoaded)
	{
		postLoad();
	}
}

void GLOWE::Font::loadFromMemory(const SharedPtr<String>& data, const bool loadColor, const unsigned int faceID)
{
	unload();
	fontFileData = data;
	FT_Error error = FT_New_Memory_Face(getInstance<FontUtility>().getLibrary(), reinterpret_cast<const FT_Byte*>(fontFileData->getCharArray()), fontFileData->getSize(), faceID, &face);
	isLoaded = error == FT_Err_Ok;
	hasColor = loadColor;
	if (isLoaded)
	{
		postLoad();
	}
	else
	{
		fontFileData.reset();
	}
}

void GLOWE::Font::unload()
{
	if (isLoaded)
	{
		FT_Done_Face(face);
		fontFileData.reset();
		isLoaded = false;
	}
}

const GLOWE::Font::Metrics& GLOWE::Font::getFontMetrics(const unsigned int fontSize)
{
	const auto it = metrics.find(fontSize);
	if (it != metrics.end())
	{
		return it->second;
	}

	if (!setFontSize(fontSize))
	{
		return invalidMetrics;
	}

	return metrics.emplace(fontSize, Metrics{ static_cast<float>(face->size->metrics.height >> 6), static_cast<float>(face->size->metrics.ascender >> 6), static_cast<float>(face->size->metrics.descender >> 6) }).first->second;
}

bool GLOWE::Font::checkIsLoaded() const
{
	return isLoaded;
}

bool GLOWE::Font::checkHasKerning() const
{
	return hasKerning;
}

bool GLOWE::Font::checkIfHasColor() const
{
	return hasColor;
}

const GLOWE::Glyph& GLOWE::Font::getGlyph(const unsigned fontSize, const LaidOutGlyph& laidOutGlyph,
	BasicRendererImpl& renderer)
{
	return getInstance<FontCache>().getGlyph(laidOutGlyph, *this, fontSize, renderer);
}

const GLOWE::Glyph& GLOWE::Font::getGlyph(const unsigned int fontSize, const String::Utf32Char character, BasicRendererImpl& renderer)
{
	return getInstance<FontCache>().getGlyph(character, *this, fontSize, renderer);
}

const GLOWE::Glyph* GLOWE::Font::getGlyphLoaded(const String::Utf32Char character, const unsigned int fontSize)
{
	return getInstance<FontCache>().getGlyphLoaded(character, *this, fontSize);
}

const FT_Face& GLOWE::Font::getFace() const
{
	return face;
}

GLOWE::Vector<GLOWE::LaidOutGlyph> GLOWE::Font::layoutText(const String& utf8Text, const String& bcp47Language, const unsigned int fontSize)
{
	if (!setFontSize(fontSize))
	{
		return {};
	}
	return raqmInstance->layoutText(utf8Text, *this, bcp47Language);
}

GLOWE::Vector<GLOWE::LaidOutGlyph> GLOWE::Font::layoutText(const Utf32String& utf32Text, const String& bcp47Language, const unsigned int fontSize)
{
	if (!setFontSize(fontSize))
	{
		return {};
	}
	return raqmInstance->layoutText(utf32Text, *this, bcp47Language);
}

GLOWE::Map<GLOWE::String::Utf32Char, unsigned int> GLOWE::Font::getCharmap() const
{
	Map<String::Utf32Char, unsigned int> result;
	FT_UInt glyphId = 0;
	FT_ULong characterCode = FT_Get_First_Char(face, &glyphId);
	while (glyphId != 0)
	{
		result.emplace(characterCode, glyphId);
		characterCode = FT_Get_Next_Char(face, characterCode, &glyphId);
	}

	return result;
}

GLOWE::Int2 GLOWE::Font::getKerning(const Glyph& leftGlyph, const Glyph& rightGlyph)
{
	if (leftGlyph.fontSize != rightGlyph.fontSize)
	{
		throw StringException("Mismatched glyph font sizes "_str + toString(leftGlyph.fontSize) + " and " + toString(rightGlyph.fontSize) + " for glyphs " + toString(leftGlyph.glyphIndex) + " and " + toString(rightGlyph.glyphIndex));
	}

	if (!setFontSize(leftGlyph.fontSize))
	{
		return Int2{ 0, 0 };
	}

	FT_Vector kern;
	FT_Get_Kerning(face, leftGlyph.glyphIndex, rightGlyph.glyphIndex, FT_KERNING_DEFAULT, &kern);
	return Int2{ kern.x >> 6, kern.y >> 6 };
}

GLOWE::Int2 GLOWE::Font::getKerning(const Glyph& leftGlyph, const Glyph& rightGlyph) const
{
	if (leftGlyph.fontSize != rightGlyph.fontSize)
	{
		throw StringException("Mismatched glyph font sizes "_str + toString(leftGlyph.fontSize) + " and " + toString(rightGlyph.fontSize) + " for glyphs " + toString(leftGlyph.glyphIndex) + " and " + toString(rightGlyph.glyphIndex));
	}

	if (leftGlyph.fontSize != currentFontSize)
	{
		throw Exception("Glyph size and font size mismatch");
	}

	FT_Vector kern;
	FT_Get_Kerning(face, leftGlyph.glyphIndex, rightGlyph.glyphIndex, FT_KERNING_DEFAULT, &kern);
	return Int2{ kern.x >> 6, kern.y >> 6 };
}

GLOWE::Hash GLOWE::Font::getHash() const
{
	return fontStyleHash;
}

GLOWE::String GLOWE::Font::getName() const
{
	return fontStyleName;
}

template<>
GLOWE::ResourceHandle<GLOWE::Font> GLOWE::ResourceMgr::loadDynamic(const void* memory, const UInt64 size, const Map<Hash, ResourceHandle<void>>& loadedDependiencies, const Hash& subresourceToLoad)
{
	ResourceHandle<Font> result = makeSharedPtr<Font>();
	MemoryLoadSaveHandle handle(memory, size);

	UInt32 fontsCount{};
	UInt64 memSize{};
	Hash fontHash;
	int chosenFaceId = -1, tempFontId = -1;
	handle >> fontsCount;
	for (unsigned int x = 0; x < fontsCount; ++x)
	{
		handle >> fontHash >> tempFontId;
		if (subresourceToLoad == fontHash)
		{
			chosenFaceId = tempFontId;
		}
	}

	if (chosenFaceId >= 0)
	{
		handle >> memSize;
		SharedPtr<String> fileData = makeSharedPtr<String>(static_cast<const char*>(handle.getReadPtr()), memSize);
		result->loadFromMemory(fileData, chosenFaceId);
	}
	else
	{
		WarningThrow(false, "Unable to find specified face inside a font file.");
	}

	return result;
}

template<>
GLOWE::ResourceHandle<GLOWE::Font> GLOWE::ResourceMgr::getDefaultResource()
{
	SharedPtr<Font> result = makeSharedPtr<Font>();

	return result;
}

void GLOWE::FontCache::useFont(Font& font, const unsigned int fontSize)
{
	const GraphicsDeviceImpl& gc = getInstance<Engine>().getGraphicsContext();
	bool needToLoad = false;
	auto fontIt = fonts.find(font.getHash());
	decltype(fontIt->second.end()) sizeIt;
	if (fontIt == fonts.end())
	{
		fontIt = fonts.emplace(std::piecewise_construct, std::forward_as_tuple(font.getHash()), std::tuple<>()).first;
		sizeIt = fontIt->second.emplace(std::piecewise_construct, std::forward_as_tuple(fontSize), std::tuple<>()).first;
		needToLoad = true;
	}
	else
	{
		sizeIt = fontIt->second.find(fontSize);
		if (sizeIt == fontIt->second.end())
		{
			sizeIt = fontIt->second.emplace(std::piecewise_construct, std::forward_as_tuple(fontSize), std::tuple<>()).first;
			needToLoad = true;
		}
	}

	if (!needToLoad)
	{
		return;
	}

	FontInfo& fontInfo = sizeIt->second;
	fontInfo.atlas = makeSharedPtr<TextureAtlas>();
	const String filePath = font.getHash().asNumberString() + '/' + toString(fontSize);
	// Need to load/create a new atlas.
	if (loadedCache.checkIsOpen() && loadedCache.checkFileForExistence(filePath))
	{
		// Load.
		String fileBuff;
		loadedCache.readFile(filePath, fileBuff);

		Vector<Pair<unsigned int, TextureAtlas::SubtextureDesc>> freeBlocks;
		Vector<Pair<Hash, TextureAtlas::SubtextureDesc>> subtextures;

		MemoryLoadSaveHandle handle(fileBuff.getCharArray(), fileBuff.getSize());
		UInt32 tempU32{};
		handle >> tempU32;
		for (unsigned int x = 0; x < tempU32; ++x)
		{
			String::Utf32Char character;
			handle >> character;

			Glyph& glyph = fontInfo.glyphs.emplace(std::piecewise_construct, std::forward_as_tuple(character), std::tuple<>()).first->second;
			handle >> glyph.advance >> glyph.glyphIndex >> glyph.size >> glyph.translation >> glyph.subtexInAtlas;
			glyph.atlas = fontInfo.atlas;
		}
		handle >> tempU32;
		for (unsigned int x = 0; x < tempU32; ++x)
		{
			unsigned int glyphId;
			String::Utf32Char character;
			handle >> character >> glyphId;
			fontInfo.codePointsToGlyphs.emplace(character, glyphId);
		}

		handle >> tempU32;
		freeBlocks.resize(tempU32);
		for (auto& x : freeBlocks)
		{
			handle >> x.first >> x.second;
		}
		handle >> tempU32;
		subtextures.resize(tempU32);
		for (auto& x : subtextures)
		{
			handle >> x.first >> x.second;
		}

		UInt64 tempStrSize{};
		TextureDesc texDesc;
		handle >> texDesc >> tempStrSize;
		const Hash dataHash(static_cast<const char*>(handle.getWritePtr()), handle.getDataSize());

		fontInfo.atlas->create(&gc, texDesc, handle.getReadPtr());
		fontInfo.atlas->overrideBlocks(freeBlocks, subtextures);
		fontInfo.wasAtlasModified = false;
	}
	else
	{
		// Create.
		fontInfo.wasAtlasModified = true;
		TextureDesc texDesc;
		texDesc.arraySize = 1;
		texDesc.binding = TextureDesc::Binding::Default;
		texDesc.cpuAccess = CPUAccess::None;
		texDesc.depth = 0;
		texDesc.format = Format(Format::Type::UnsignedByte, font.checkIfHasColor() ? 4 : 1);
		texDesc.generateMipmaps = false;
		texDesc.width = defaultSize[0];
		texDesc.height = defaultSize[1];
		texDesc.mipmapsCount = 1;
		texDesc.multisamplingLevel = 0;
		texDesc.type = TextureDesc::Type::Texture2D;
		texDesc.usage = Usage::Default;
		fontInfo.atlas->create(&gc, texDesc);
	}
}

const GLOWE::Glyph& GLOWE::FontCache::createGlyph(const String::Utf32Char character, Font& font, const unsigned int fontSize, BasicRendererImpl& renderer)
{
	useFont(font, fontSize);
	FontInfo& fontInfo = fonts[font.getHash()][fontSize];
	fontInfo.wasAtlasModified = true;
	Glyph glyph;
	String texOut;
	TextureDesc texDescOut;
	font.generateGlyphForCache(fontSize, character, texOut, texDescOut, glyph);

	const auto glyphId = glyph.glyphIndex;
	if (texDescOut.width > 0 && texDescOut.height > 0)
	{
		glyph.atlas = fontInfo.atlas;
		const auto result = fontInfo.atlas->addTexture(Hash(toString(glyphId)), { texDescOut.width, texDescOut.height }, texOut.getCharArray(), renderer);
		if (result)
		{
			glyph.subtexInAtlas = *result;
		}
	}

	fontInfo.codePointsToGlyphs.emplace(character, glyphId);
	return fontInfo.glyphs.emplace(glyphId, std::move(glyph)).first->second;
}

const GLOWE::Glyph& GLOWE::FontCache::createGlyph(const unsigned glyphId, Font& font, const unsigned fontSize,
	BasicRendererImpl& renderer)
{
	useFont(font, fontSize);
	FontInfo& fontInfo = fonts[font.getHash()][fontSize];
	fontInfo.wasAtlasModified = true;
	Glyph glyph;
	String texOut;
	TextureDesc texDescOut;
	font.generateGlyphForCache(fontSize, glyphId, texOut, texDescOut, glyph);
	if (texDescOut.width > 0 && texDescOut.height > 0)
	{
		glyph.atlas = fontInfo.atlas;
		const auto result = fontInfo.atlas->addTexture(Hash(toString(glyphId)), { texDescOut.width, texDescOut.height }, texOut.getCharArray(), renderer);
		if (result)
		{
			glyph.subtexInAtlas = *result;
		}
	}
	
	return fontInfo.glyphs.emplace(glyphId, std::move(glyph)).first->second;
}

void GLOWE::FontCache::init()
{
	// Slower, but requires less memory.
	loadedCache.open(cacheName, Package::OpenMode::Read);
	/*File cacheFile;
	cacheFile.open(cacheName, std::ios::in | std::ios::binary);
	if (cacheFile.checkIsOpen())
	{
		String memory(cacheFile.getFileSize());
		cacheFile.read(memory.getCharArray(), memory.getSize());
		loadedCache.openFromMemory(memory);
	}*/
}

GLOWE::ThreadPool::TaskResult<void> GLOWE::FontCache::cleanUpAsync(ThreadPool& threads, BasicRendererImpl& renderer)
{
	SharedPtr<Map<String, Vector<ThreadPool::TaskResult<void>>>> futures = makeSharedPtr<Map<String, Vector<ThreadPool::TaskResult<void>>>>();
	const GraphicsDeviceImpl& gc = getInstance<Engine>().getGraphicsContext();
	loadedCache.close();

	for (const auto& font : fonts)
	{
		const String dir = font.first.asNumberString() + '/';

		for (const auto& sizeVariant : font.second)
		{
			if (sizeVariant.second.wasAtlasModified)
			{
				StringLoadSaveHandle handle;
				handle << static_cast<UInt32>(sizeVariant.second.glyphs.size());
				for (const auto& glyph : sizeVariant.second.glyphs)
				{
					handle << glyph.first << glyph.second.advance << glyph.second.glyphIndex << glyph.second.size << glyph.second.translation << glyph.second.subtexInAtlas;
				}
				handle << static_cast<UInt32>(sizeVariant.second.codePointsToGlyphs.size());
				for (const auto& codePointMapping : sizeVariant.second.codePointsToGlyphs)
				{
					handle << codePointMapping.first << codePointMapping.second;
				}
				const TextureImpl& tex = *sizeVariant.second.atlas->getTexture();
				UniquePtr<TextureImpl> tempTex(createGraphicsObject<TextureImpl>());
				TextureDesc tempDesc = tex.getDesc();
				tempDesc.usage = Usage::Staging;
				tempDesc.cpuAccess = CPUAccess::Read;
				tempTex->create(tempDesc, gc);
				renderer.copyTextures(&tex, tempTex.get());
				const unsigned int width = tex.getDesc().width, height = tex.getDesc().height, size = width * height * tex.getDesc().format.getSize();
				String tempBuffer(size, '\0');
				renderer.readTexture(tempTex.get(), tempBuffer.getCharArray(), { 0, 0, 0 }, { width, height, 0 });

				Vector<Pair<unsigned int, TextureAtlas::SubtextureDesc>> freeBlocks;
				Vector<Pair<Hash, TextureAtlas::SubtextureDesc>> subtextures;
				sizeVariant.second.atlas->getBlocksData(freeBlocks, subtextures);

				handle << static_cast<UInt32>(freeBlocks.size());
				for (const auto& x : freeBlocks)
				{
					handle << x.first << x.second;
				}
				handle << static_cast<UInt32>(subtextures.size());
				for (const auto& x : subtextures)
				{
					handle << x.first << x.second;
				}

				handle << tex.getDesc() << tempBuffer;
				const Hash dataHash(handle.getString());
				SharedPtr<String> buffer = makeSharedPtr<String>(handle.consumeString());

				(*futures)[dir + toString(sizeVariant.first)].emplace_back(getInstance<AsyncIOMgr>().writeToPackage(cacheName, dir + toString(sizeVariant.first), buffer->getCharArray(), buffer->getSize(), [buffer](Package& package) { package.applyChanges(); }));
			}
		}
	}

	return threads.addTask([futures]()
	{
		for (const auto& x : *futures)
		{
			for (const auto& y : x.second)
			{
				y.wait();
			}

			getInstance<AsyncIOMgr>().applyPackageChanges(x.first);
		}
	});
}

const GLOWE::Glyph& GLOWE::FontCache::getGlyph(const LaidOutGlyph& laidOutGlyph, Font& font, const unsigned fontSize,
	BasicRendererImpl& renderer)
{
	Lockable::Guard guard(*this);
	const auto fontIt = fonts.find(font.getHash());
	if (fontIt != fonts.end())
	{
		const auto sizeIt = fontIt->second.find(fontSize);
		if (sizeIt != fontIt->second.end())
		{
			const auto glyphIt = sizeIt->second.glyphs.find(laidOutGlyph.glyphIndex);
			if (glyphIt != sizeIt->second.glyphs.end())
			{
				return glyphIt->second;
			}
		}
	}

	return createGlyph(laidOutGlyph.glyphIndex, font, fontSize, renderer);
}

const GLOWE::Glyph& GLOWE::FontCache::getGlyph(const String::Utf32Char character, Font& font, const unsigned int fontSize, BasicRendererImpl& renderer)
{
	Lockable::Guard guard(*this);
	const auto fontIt = fonts.find(font.getHash());
	if (fontIt != fonts.end())
	{
		const auto sizeIt = fontIt->second.find(fontSize);
		if (sizeIt != fontIt->second.end())
		{
			const auto mappingIt = sizeIt->second.codePointsToGlyphs.find(character);
			if (mappingIt != sizeIt->second.codePointsToGlyphs.end())
			{
				const auto glyphIt = sizeIt->second.glyphs.find(mappingIt->second);
				if (glyphIt != sizeIt->second.glyphs.end())
				{
					return glyphIt->second;
				}
			}
		}
	}

	return createGlyph(character, font, fontSize, renderer);
}

const GLOWE::Glyph* GLOWE::FontCache::getGlyphLoaded(const String::Utf32Char character, Font& font, const unsigned int fontSize)
{
	Lockable::Guard guard(*this);
	useFont(font, fontSize);
	const auto fontIt = fonts.find(font.getHash());
	if (fontIt != fonts.end())
	{
		const auto sizeIt = fontIt->second.find(fontSize);
		if (sizeIt != fontIt->second.end())
		{
			const auto mappingIt = sizeIt->second.codePointsToGlyphs.find(character);
			if (mappingIt != sizeIt->second.codePointsToGlyphs.end())
			{
				const auto glyphIt = sizeIt->second.glyphs.find(mappingIt->second);
				if (glyphIt != sizeIt->second.glyphs.end())
				{
					return &glyphIt->second;
				}
			}
		}
	}

	return nullptr;
}
