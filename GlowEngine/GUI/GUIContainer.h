#pragma once
#ifndef GLOWE_GUI_CONTAINER_INCLUDED
#define GLOWE_GUI_CONTAINER_INCLUDED

#include "../../GlowSystem/MemoryMgr.h"

#include "../../GlowGraphics/Uniform/MaterialEnvironment.h"

#include "GUIElement.h"
#include "GUIConstraint.h"

namespace GLOWE
{
	class GLOWENGINE_EXPORT UnableToFindTargetElementException
		: public GUIException
	{
	public:
		using GUIException::GUIException;
	};

	class GLOWENGINE_EXPORT InvalidGUIConstraintData
		: public GUIException
	{
	public:
		using GUIException::GUIException;
	};

	class GLOWENGINE_EXPORT UnableToAddGUIElementException
		: public GUIException
	{
	public:
		using GUIException::GUIException;
	};

	class GLOWENGINE_EXPORT GUIContainer
		: public GUIElement
	{
	private:
		using ElementHandle = List<GUIElement*>::iterator;

		struct ElementDef
		{
			using ConstraintContainer = Set<GUIConstraint>;

			ConstraintContainer constraints;
			ElementHandle elementHandle;

			inline ElementDef() = default;
			inline ElementDef(const ElementHandle& newHandle)
				: constraints(), elementHandle(newHandle)
			{
			}
		};
	public:
		enum WhatToRedraw : unsigned char
		{
			Element = maskOption(0),
			Mask = maskOption(1)
		};

		enum class InvalidationType : unsigned char
		{
			None,
			ContainsInvalid,
			IsInvalidItself
		};
	public:
		using ConstraintHandle = ElementDef::ConstraintContainer::iterator;
		using OrderedIterator = List<GUIElement*>::iterator;
		using OrderedConstIterator = List<GUIElement*>::const_iterator;
		using OrderedReversedIterator = List<GUIElement*>::reverse_iterator;
		using OrderedReversedConstIterator = List<GUIElement*>::const_reverse_iterator;
	protected:
		using InternalHandle = Map<GUIElement*, ElementDef>::iterator;
	protected:
		UnorderedMap<GUIElement*, ElementDef> elements;
		List<GUIElement*> elementsOrder;
		UnorderedSet<GUIElement*> elementsWithMouseInside;
		UnorderedMap<GUIElement*, InvalidationType> invalidatedElements;
		UnorderedMap<GUIElement*, unsigned char> elementsToRedraw;

		UnorderedMap<GUIElement*, unsigned char> persistentCounters;
		Queue<GUIElement*> persistentElementQueue;
	private:
		static bool isRecursiveCycleDetectionEnabled;
	protected:
		void update();

		virtual void renderImpl(BasicRendererImpl& renderer, MaterialEnvironment& matEnv) override;
		virtual void onRectUpdated() override;
		virtual void onRecalcDone() override;
		virtual void onRecalcSchedule() override;
		/// @brief Called just before the update of invalidated elements. The call may happen even if there are no invalidated elements to recalculate.
		virtual void onBeforeElementsUpdate() {};
		/// @brief Called just after the update of invalidated elements.
		/// @return true if you want a second rect update of the elements, false otherwise.
		virtual bool onAfterElementsUpdate() { return false; };
		virtual void onUpdateEnd() override;

		virtual void markForRedraw(const unsigned char options) override;
		virtual void drawMask(BasicRendererImpl& renderer, MaterialEnvironment& matEnv) override;
		virtual void processElementsToDraw() override;

		virtual void invalidateElement(GUIElement* const element, const InvalidationType inval);
		virtual void uninvalidateElement(GUIElement* const element);
	private:
		Vector<GUIElement*> topoSort();
		void topoSort(Vector<GUIElement*>& sortedElements);
		void populateTopoCounters(GUIElement* const element);
		void checkElementForTopoQueue(GUIElement* const element, const unsigned int edgesToRemove);

		void recalcElement(GUIElement* const element);

		void markElementForRedrawJustElement(GUIElement* const element);

		InvalidationType getInvalidationInfo(GUIElement* const element) const;

		static void unbindConstraint(GUIElement* const element, const GUIConstraint& constraint);

		bool detectCycle(GUIElement* const where, const GUIConstraint& constraint) const;
		bool detectCycleFor(GUIElement* const where, GUIElement* const forWhat) const;
	public:
		/// @brief Calculates the container's scissor rect, with all parent transformations applied.
		/// @return Scissor rect.
		GUIRect getScissorRect() const;
	
		virtual ~GUIContainer() = default;

		/// @brief Enables or disables greedy cycle detection. Disabled by default in release builds, enabled by default in debug builds. When disabled, the only cycle detection is done during topo sort. When enabled, adding constraints will recursively check for cycles in elements' graph.
		/// @param enable Enable or disable the detection system.
		static void setRecursiveCycleDetectionEnabled(const bool enable);
		static bool checkIfRecursiveCycleDetectionIsEnabled();

		void markElementForRedraw(GUIElement* const element);
		using GUIElement::markForRedraw;

		// In order to use for range loop. Allows for iteration over all elements in their respective order. Used e.g. in GUILinearLayout.
		OrderedIterator begin();
		OrderedConstIterator begin() const;
		OrderedReversedIterator rbegin();
		OrderedReversedConstIterator rbegin() const;

		OrderedIterator end();
		OrderedConstIterator end() const;
		OrderedReversedIterator rend();
		OrderedReversedConstIterator rend() const;

		bool autoFindContainer(GameObject* const gameObject);

		void addElementFront(GUIElement* const elem);
		void addElementBack(GUIElement* const elem);
		void addElementBefore(GUIElement* const elem, GUIElement* const beforeWhat);

		String dbgPrintRects(const unsigned int tabs = 0, const bool printGlobal = true) const;
		void dbgDrawRects(const Time& howLong = Time::ZeroTime, const Color& color = Color::green) const;

		using GUIElement::placeAtBack;
		void placeAtBack(GUIElement* const elem);
		using GUIElement::placeAtFront;
		void placeAtFront(GUIElement* const elem);
		using GUIElement::placeBefore;
		void placeBefore(GUIElement* const elem, GUIElement* const beforeWhat);

		ConstraintHandle addConstraint(GUIElement* const handle, const GUIConstraint& constraint);
		ConstraintHandle addConstraint(GUIElement& handle, const GUIConstraint& constraint);

		void removeConstraint(GUIElement* const element, const ConstraintHandle& constraint);
		void removeConstraint(GUIElement& element, const ConstraintHandle& constraint);

		void removeElement(GUIElement* const handle);
		void removeElement(GUIElement& handle);

		void clearConstraints(GUIElement* const handle);
		void clearConstraints(GUIElement& handle);

		bool checkIsScheduledForRecalc(GUIElement* const handle) const;
		bool checkIsAnythingToRedraw() const;

		virtual void processGUIEvent(const GameEvent::GUIEvent& event) override;

		friend class GUIElement;
	};
}

#endif
