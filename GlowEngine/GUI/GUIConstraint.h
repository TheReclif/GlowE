#pragma once
#ifndef GLOWE_GUI_GUICONSTRAINT_INCLUDED
#define GLOWE_GUI_GUICONSTRAINT_INCLUDED

#include "GUIRect.h"
#include "GUIException.h"

namespace GLOWE
{
	class GUIElement;

	/// @brief Structure used to describe GUI constraints. Constructors don't check if the given constraint type matches the constructor's argument layout.
	struct GLOWENGINE_EXPORT GUIConstraint
	{
		/// @brief Type of constraint.
		enum class Type : unsigned char
		{
			/// @brief Fixed distance from the left edge of the target element.
			FixedLeft,
			/// @brief Fixed distance from the right edge of the target element.
			FixedRight,
			/// @brief Fixed distance from the top edge of the target element.
			FixedUp,
			/// @brief Fixed distance from the bottom edge of the target element.
			FixedDown,
			/// @brief Creates a pure dependency to a element. Does not influence rect calculations. Uses fixedData.whichElement to specify the element.
			PureDependency,

			/// @brief Constant ratio of distance from the left edge to some other element to distance from the right edge to some other element.
			HorizontalRatio,
			/// @brief Constant ratio of distance from the top edge to some other element to distance from the bottom edge to some other element.
			VerticalRatio,

			/// @brief Makes the element as wide as its content. Can be applied only to objects deriving from GUIContainer.
			MatchContentHeight,
			/// @brief Makes the element as high as its content. Can be applied only to objects deriving from GUIContainer.
			MatchContentWidth,

			/// @brief Makes the element try to match given aspect ration. Incompatible with double Fixed constraints.
			KeepAspectRatio,

			/// @brief Constraints count. Not a valid constraint type.
			ConstraintsCount,
			/// @brief Invalid constraint type.
			Invalid = ConstraintsCount
		};

		/// @brief Which edge of the element to use.
		enum class Edge : unsigned char
		{
			/// @brief Left for horizontal; top for vertical constraints.
			LeftUp,
			/// @brief Right for horizontal; bottom for vertical constraints.
			RightDown
		};

		union
		{
			/// @brief Data describing Fixed* family of constraints.
			struct
			{
				/// @brief Distance from affected element's edge to target element's.
				float distance;
				/// @brief Target element.
				GUIElement* whichElement;
				/// @brief Which edge of the target element.
				Edge whichEdge;
			} fixedData;
			/// @brief Data describing *Ratio family of constraints.
			struct
			{
				/// @brief Left/top (nominator) part of the ratio.
				float leftUpPart;
				/// @brief Right/down (denominator) part of the ratio.
				float rightBottomPart;
				/// @brief Target element for the left edge (horizontal ratio) or the top edge (vertical ratio).
				GUIElement* leftUp;
				/// @brief Target element for the right edge (horizontal ratio) or the bottom edge (vertical ratio).
				GUIElement* rightBottom;
				/// @brief Which edges of the target elements to use.
				Edge whichEdgeLeftUp, whichEdgeRightBottom;
			} ratioData;
			/// @brief Data describing KeepAspectRatio constraint.
			struct
			{
				/// @brief Tells the constraint which dimension to resize when both can be resized. True for width prioritisation, false for height.
				bool prioritizeWidthResize;
				/// @brief Aspect ratio to keep.
				float aspectRatio;
			} keepAspectRatioData;
		};

		/// @brief Type of the constraint.
		Type type;

		GUIConstraint();
		/// @brief Match size constraint constructor.
		/// @param constraintType Match content constraint type.
		explicit GUIConstraint(const Type constraintType);
		/// @brief Fixed constraint constructor.
		/// @param constraintType Fixed constraint type.
		/// @param distance Fixed distance.
		/// @param elem Target element.
		/// @param edge Which edge of the target.
		GUIConstraint(const Type constraintType, const float distance, GUIElement* const elem, const Edge edge);
		/// @brief Pure dependency constructor.
		/// @param elem Target element.
		GUIConstraint(GUIElement* const elem);
		/// @brief Ratio constraint constructor.
		/// @param constraintType Ratio constraint type.
		/// @param leftUpPart Left/top (nominator) part of the ratio.
		/// @param rightBottomPart Right/down (denominator) part of the ratio.
		/// @param leftUp Left/top target element.
		/// @param rightDown Right/bottom target element.
		/// @param leftUpEdge Which edge to use for the left/top target element.
		/// @param rightDownEdge Which edge to use for the right/bottom target element.
		GUIConstraint(const Type constraintType, const float leftUpPart, const float rightBottomPart, GUIElement* const leftUp, GUIElement* const rightDown, const Edge leftUpEdge, const Edge rightDownEdge);
		/// @brief KeepAspectRatio constraint constructor.
		/// @param constraintType Must be KeepAspectRatio
		/// @param aspectRatio Aspect ratio to keep.
		/// @param prioritizeWidthResize True for width resize prioritisation, false for height.
		GUIConstraint(const Type constraintType, const float aspectRatio, const bool prioritizeWidthResize = true);

		// Those operators are used only for std::set ordering.
		constexpr inline bool operator<(const GUIConstraint& b) const
		{
			return type < b.type;
		}
		constexpr inline bool operator==(const GUIConstraint& b) const
		{
			return type == b.type;
		}
	};

	class GLOWENGINE_EXPORT GUICycleDetected
		: public GUIException
	{
	public:
		Vector<GUIElement*> cycledElements;
	public:
		GUICycleDetected(Vector<GUIElement*>&& arg);
	};
}
#endif
