#include "GUILinearLayout.h"

bool GLOWE::GUILinearLayout::onAfterElementsUpdate()
{
	float off = offset;
	contentSize = 0;
	bool result = false;
	// (Not applicable anymore) When breaking here, after a managed element had been disabled, the button's position is set and it's being scheduled for an update. However, because of some bug (let's call it GB-INSPECTOR-NOT-WORKING), the button is not updated anywhere in the future. It's in a limbo state where its position is scheduled for an update, but the element itself is not. This bug is caused (I'm about 70% certain about it) by the deactivated element not notifying all of the elements in the linear layout. To fix this, every element should have a constraint (which does nothing) with every other element in the linear layout. This would cause a cycle though. Maybe the methods in the linear layout can force invalidate all of the elements inside?
	// TODO: This bug has been fixed by checking if the invalidated elements' list was updated during the call to onAfterElementsUpdate and if it did, the update routine recalculates the topo sort and does a second update on the newly invalidated elements. The aftermatch is just a minor graphics glitch, where the old element's pixels are not cleared when its position changes (but it should!).
	for (auto& x : elementsOrder)
	{
		if (!x->checkIsActiveInGUI())
		{
			continue;
		}
		result = true;
		const auto& rect = x->getLocalGUIRect();
		const auto size = rect.getScaledSize();
		Float2 pos{ rect.x, rect.y };
		if (!isVertical)
		{
			pos[0] = off;
			off += size[0];
			contentSize += size[0];
		}
		else
		{
			pos[1] = off;
			off += size[1];
			contentSize += size[1];
		}
		x->setLocalPosition(pos);
	}

	return result;
}

void GLOWE::GUILinearLayout::invalidateElement(GUIElement* const element, const InvalidationType inval)
{
	GLOWE::GUIContainer::invalidateElement(element, inval);
	for (const auto x : elementsOrder)
	{
		if (!x->checkIsActiveInGUI())
		{
			continue;
		}
		const auto it = invalidatedElements.find(x);
		if (it == invalidatedElements.end() || it->second == InvalidationType::ContainsInvalid)
		{
			// It can be implemented more efficiently.
			x->scheduleRecalc();
		}
	}
}

GLOWE::GUILinearLayout::GUILinearLayout(const bool vertical)
	: isVertical(vertical), contentSize(0), offset(0), onOffsetChanged(nullptr)
{
}

void GLOWE::GUILinearLayout::setOffset(const float newOffset)
{
	offset = newOffset;
	if (onOffsetChanged)
	{
		onOffsetChanged(offset);
	}
	scheduleRecalc();
}

float GLOWE::GUILinearLayout::getOffset() const
{
	return offset;
}

void GLOWE::GUILinearLayout::setIsVertical(const bool vertical)
{
	isVertical = vertical;
	scheduleRecalc();
}

bool GLOWE::GUILinearLayout::checkIsVertical() const
{
	return isVertical;
}

float GLOWE::GUILinearLayout::getContentSize() const
{
	return contentSize;
}

void GLOWE::GUILinearLayout::setOffsetChangedCallback(const std::function<void(const float)>& newCallback)
{
	onOffsetChanged = newCallback;
}
