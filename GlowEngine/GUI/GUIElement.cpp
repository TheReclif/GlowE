#include "GUIElement.h"
#include "GUIContainer.h"
#include "GUIMaskDrawHelper.h"

#include "../SimpleRaceDetector.h"
#include "../Component.h"
#include "../GameObject.h"

GLOWE::RecursiveMutex GLOWE::GUIElement::guiMutex;

void GLOWE::GUIElement::updateRect()
{
	if ((rectToUpdate == nullptr) && (positionUpdate == RectToUpdate::None) && (scaleUpdate == RectToUpdate::None) && (sizeUpdate == RectToUpdate::None))
	{
		return;
	}

	GlowAssert(parent != nullptr);
	const auto& parentRect = parent->getGlobalGUIRect();

	switch (positionUpdate)
	{
	case RectToUpdate::Local:
		rect.pos = rectToUpdate->pos;
	case RectToUpdate::FromParent:
		globalRect.pos = (Vector2F(rect.pos) + Vector2F(parentRect.pos)).toFloat2();
		break;
	case RectToUpdate::Global:
		globalRect.pos = rectToUpdate->pos;
		rect.pos = (Vector2F(globalRect.pos) - Vector2F(parentRect.pos)).toFloat2();
		break;
	}

	switch (scaleUpdate)
	{
	case RectToUpdate::Local:
		rect.scale = rectToUpdate->scale;
	case RectToUpdate::FromParent:
		globalRect.scale = (Vector2F(rect.scale) * Vector2F(parentRect.scale)).toFloat2();
		break;
	case RectToUpdate::Global:
		globalRect.scale = rectToUpdate->scale;
		rect.scale = (Vector2F(globalRect.scale) / Vector2F(parentRect.scale)).toFloat2();
		break;
	}

	switch (sizeUpdate)
	{
	case RectToUpdate::Local:
		rect.size = rectToUpdate->size;
	case RectToUpdate::FromParent:
		globalRect.size = (Vector2F(globalRect.scale) * Vector2F(rect.size)).toFloat2();
		break;
	case RectToUpdate::Global:
		globalRect.size = rectToUpdate->size;
		rect.size = (Vector2F(globalRect.size) / Vector2F(globalRect.scale)).toFloat2();
		break;
	}

	positionUpdate = scaleUpdate = sizeUpdate = RectToUpdate::None;
	rectToUpdate.reset();
	onRectUpdated();
}

void GLOWE::GUIElement::drawMask(BasicRendererImpl& renderer, MaterialEnvironment& matEnv)
{
	getInstance<Hidden::GUIMaskDrawHelper>().drawMask(globalRect, renderer, matEnv);
}

GLOWE::GUIElement::GUIElement()
	: parent(nullptr), myConstraints(nullptr), rect(), globalRect(), rectToUpdate(), positionUpdate(RectToUpdate::None), scaleUpdate(RectToUpdate::None), sizeUpdate(RectToUpdate::None)
{
}

bool GLOWE::GUIElement::autoFindContainer(GameObject* const gameObject)
{
	if (!parent)
	{
		GameObject* tempParent = gameObject;
		GUIContainer* tempGUIParent = nullptr;

		while (!tempGUIParent && tempParent)
		{
			tempGUIParent = tempParent->getVirtualComponent<GUIContainer>();
			tempParent = tempParent->getParent();
		}

		if (tempGUIParent)
		{
			tempGUIParent->addElementBack(this);
			return true;
		}
	}

	return false;
}

void GLOWE::GUIElement::render(BasicRendererImpl& renderer, MaterialEnvironment& matEnv)
{
	if (checkIsActiveInGUI())
	{
		if (lastUpdateFrame == getInstance<Engine>().getCurrentFrameId())
		{
			updateImpl();
		}

		renderImpl(renderer, matEnv);
	}
}

void GLOWE::GUIElement::markForRedraw()
{
	// HACK: Delete after hunting down bugs
	SimpleRaceDetector<GUIElement> _;
	if (parent)
	{
		parent->markElementForRedraw(this);
	}
	else
	{
		markForRedraw(GUIContainer::Mask | GUIContainer::Element);
	}
}

void GLOWE::GUIElement::placeAtBack()
{
	if (parent)
	{
		parent->placeAtBack(this);
	}
}

void GLOWE::GUIElement::placeAtFront()
{
	if (parent)
	{
		parent->placeAtFront(this);
	}
}

void GLOWE::GUIElement::placeBefore(GUIElement* const beforeWhat)
{
	if (parent)
	{
		parent->placeBefore(this, beforeWhat);
	}
}

void GLOWE::GUIElement::setLocalGUIRect(const GUIRect& newRect)
{
	if (!parent)
	{
		rect = globalRect = newRect;
		onRectUpdated();
		lastUpdateFrame = getInstance<Engine>().getCurrentFrameId();
	}
	else
	{
		if (rectToUpdate)
		{
			*rectToUpdate = newRect;
		}
		else
		{
			rectToUpdate = makeTrivialUniquePtr<GUIRect>(newRect);
		}
		positionUpdate = scaleUpdate = sizeUpdate = RectToUpdate::Local;
		scheduleRecalc();
	}
}

void GLOWE::GUIElement::setGlobalGUIRect(const GUIRect& newRect)
{
	if (!parent)
	{
		rect = globalRect = newRect;
		onRectUpdated();
		lastUpdateFrame = getInstance<Engine>().getCurrentFrameId();
	}
	else
	{
		if (rectToUpdate)
		{
			*rectToUpdate = newRect;
		}
		else
		{
			rectToUpdate = makeTrivialUniquePtr<GUIRect>(newRect);
		}
		positionUpdate = scaleUpdate = sizeUpdate = RectToUpdate::Global;
		scheduleRecalc();
	}
}

void GLOWE::GUIElement::setLocalPosition(const Float2& pos)
{
	if (!parent)
	{
		rect.pos = globalRect.pos = pos;
		onRectUpdated();
		lastUpdateFrame = getInstance<Engine>().getCurrentFrameId();
	}
	else
	{
		if (!rectToUpdate)
		{
			rectToUpdate = makeTrivialUniquePtr<GUIRect>();
		}
		rectToUpdate->pos = pos;
		positionUpdate = RectToUpdate::Local;
		scheduleRecalc();
	}
}

void GLOWE::GUIElement::setLocalSize(const Float2& size)
{
	if (!parent)
	{
		rect.size = globalRect.size = size;
		onRectUpdated();
		lastUpdateFrame = getInstance<Engine>().getCurrentFrameId();
	}
	else
	{
		if (!rectToUpdate)
		{
			rectToUpdate = makeTrivialUniquePtr<GUIRect>();
		}
		rectToUpdate->size = size;
		sizeUpdate = RectToUpdate::Local;
		scheduleRecalc();
	}
}

void GLOWE::GUIElement::setLocalScale(const Float2& scale)
{
	if (!parent)
	{
		rect.scale = globalRect.scale = scale;
		onRectUpdated();
		lastUpdateFrame = getInstance<Engine>().getCurrentFrameId();
	}
	else
	{
		if (!rectToUpdate)
		{
			rectToUpdate = makeTrivialUniquePtr<GUIRect>();
		}
		rectToUpdate->scale = scale;
		scaleUpdate = RectToUpdate::Local;
		scheduleRecalc();
	}
}

void GLOWE::GUIElement::setGlobalPosition(const Float2& pos)
{
	if (!parent)
	{
		rect.pos = globalRect.pos = pos;
		onRectUpdated();
		lastUpdateFrame = getInstance<Engine>().getCurrentFrameId();
	}
	else
	{
		if (!rectToUpdate)
		{
			rectToUpdate = makeTrivialUniquePtr<GUIRect>();
		}
		rectToUpdate->pos = pos;
		positionUpdate = RectToUpdate::Global;
		scheduleRecalc();
	}
}

void GLOWE::GUIElement::setGlobalSize(const Float2& size)
{
	if (!parent)
	{
		rect.size = globalRect.size = size;
		onRectUpdated();
		lastUpdateFrame = getInstance<Engine>().getCurrentFrameId();
	}
	else
	{
		if (!rectToUpdate)
		{
			rectToUpdate = makeTrivialUniquePtr<GUIRect>();
		}
		rectToUpdate->size = size;
		sizeUpdate = RectToUpdate::Global;
		scheduleRecalc();
	}
}

void GLOWE::GUIElement::setGlobalScale(const Float2& scale)
{
	if (!parent)
	{
		rect.scale = globalRect.scale = scale;
		onRectUpdated();
		lastUpdateFrame = getInstance<Engine>().getCurrentFrameId();
	}
	else
	{
		if (!rectToUpdate)
		{
			rectToUpdate = makeTrivialUniquePtr<GUIRect>();
		}
		rectToUpdate->scale = scale;
		scaleUpdate = RectToUpdate::Global;
		scheduleRecalc();
	}
}

void GLOWE::GUIElement::ensureRectWillBeUpdated()
{
	if (positionUpdate == RectToUpdate::None)
	{
		positionUpdate = RectToUpdate::FromParent;
	}
	if (scaleUpdate == RectToUpdate::None)
	{
		scaleUpdate = RectToUpdate::FromParent;
	}
	if (sizeUpdate == RectToUpdate::None)
	{
		sizeUpdate = RectToUpdate::FromParent;
	}
}

void GLOWE::GUIElement::scheduleRecalc()
{
	GlowAssert(parent != nullptr);
	if (parent->checkIsScheduledForRecalc(this))
	{
		return;
	}
	// HACK: Delete after hunting down bugs
	SimpleRaceDetector<GUIElement> _;
	ensureRectWillBeUpdated();
	parent->invalidateElement(this, GUIContainer::InvalidationType::IsInvalidItself);
	for (const auto& x : elementsDependingOnMe)
	{
		x.first->scheduleRecalc();
	}
	onRecalcSchedule();
}

const GLOWE::GUIRect& GLOWE::GUIElement::getLocalGUIRect() const
{
	return rect;
}

const GLOWE::GUIRect& GLOWE::GUIElement::getGlobalGUIRect() const
{
	return globalRect;
}

unsigned int GLOWE::GUIElement::getLastUpdateFrame() const
{
	return lastUpdateFrame;
}

GLOWE::GUIContainer* GLOWE::GUIElement::getParent() const
{
	return parent;
}

bool GLOWE::GUIElement::checkIsActiveInGUI() const
{
	return true;
}

void GLOWE::GUIElement::stretchToParent(const float border)
{
	stretchToElement(parent, border);
}

void GLOWE::GUIElement::stretchToElement(GUIElement* const elem, const float border)
{
	GlowAssert(elem != nullptr);
	addConstraintToThis(GUIConstraint(GUIConstraint::Type::FixedUp, border, elem, GUIConstraint::Edge::LeftUp));
	addConstraintToThis(GUIConstraint(GUIConstraint::Type::FixedDown, border, elem, GUIConstraint::Edge::RightDown));
	addConstraintToThis(GUIConstraint(GUIConstraint::Type::FixedLeft, border, elem, GUIConstraint::Edge::LeftUp));
	addConstraintToThis(GUIConstraint(GUIConstraint::Type::FixedRight, border, elem, GUIConstraint::Edge::RightDown));
}

GLOWE::GUIElement::ConstraintHandle GLOWE::GUIElement::addConstraintToThis(const GUIConstraint& constraint)
{
	GlowAssert(parent != nullptr);
	return parent->addConstraint(this, constraint);
}

void GLOWE::GUIElement::removeConstraintFromThis(const ConstraintHandle& handle)
{
	GlowAssert(parent != nullptr);
	parent->removeConstraint(this, handle);
}

void GLOWE::GUIElement::clearConstraints()
{
	GlowAssert(parent != nullptr);
	parent->clearConstraints(this);
}

void GLOWE::GUIElement::orphan()
{
	setParent(nullptr);
}

void GLOWE::GUIElement::setParent(GUIContainer* const newParent)
{
	if (parent)
	{
		parent->removeElement(this);
	}
	if (newParent)
	{
		newParent->addElementBack(this);
	}
}
