#include "Sphere.h"

bool GLOWE::Sphere::isInsidePlane(const Plane& plane) const
{
	return plane.getDistanceFromPoint(center) + range >= -MathHelper::Epsilon;
}

bool GLOWE::Sphere::isInsideFrustum(const Frustum& frustum) const
{
	for (unsigned int x = 0; x < 6; ++x)
	{
		if (!isInsidePlane(frustum.planes[x]))
		{
			return false;
		}
	}

	return true;
}

bool GLOWE::Sphere::isIntersectingSphere(const Sphere& sphere) const
{
	Vector3F centerA = center, centerB = sphere.center;
	float rangesSum = range + sphere.range;

	return (centerA - centerB).getMagnitudeSquared() <= (rangesSum * rangesSum); // No sqrt whatsoever.
}

bool GLOWE::Sphere::isPointInside(const Vector3F& point) const
{
	return (point - center).getMagnitudeSquared() <= (range * range);
}
