#pragma once
#ifndef GLOWE_MATH_SSE1_QUATERNIONCLASSES_INL_INCLLUDED
#define GLOWE_MATH_SSE1_QUATERNIONCLASSES_INL_INCLLUDED 1

#include "SSE1QuaternionClasses.h"

#if GLOWE_USED_SSE > GLOWE_NO_SSE
inline GLOWE::SSE1::Quaternion::Quaternion()
	: m(_mm_setzero_ps())
{
}

inline GLOWE::SSE1::Quaternion::Quaternion(const __m128 arg)
	: m(arg)
{
}

inline GLOWE::SSE1::Quaternion::Quaternion(const Quaternion& arg)
	: m(arg.m)
{
}

inline GLOWE::SSE1::Quaternion::Quaternion(Quaternion&& arg) noexcept
	: m(std::move(arg.m))
{
}

inline GLOWE::SSE1::Quaternion::Quaternion(const float arg)
	: m(_mm_set_ps1(arg))
{
}

inline GLOWE::SSE1::Quaternion::Quaternion(const Float4 & arg)
	: m(_mm_set_ps(arg[3], arg[2], arg[1], arg[0]))
{
}

inline GLOWE::SSE1::Quaternion::Quaternion(const float arg[4])
	: m(_mm_set_ps(arg[3], arg[2], arg[1], arg[0]))
{
}

inline GLOWE::SSE1::Quaternion::Quaternion(const float a, const float b, const float c, const float d)
	: m(_mm_set_ps(d, c, b, a))
{
}

inline float & GLOWE::SSE1::Quaternion::operator[](const unsigned short id)
{
	return m[id];
}

inline float GLOWE::SSE1::Quaternion::operator[](const unsigned short id) const
{
	return m[id];
}

inline GLOWE::Float4 GLOWE::SSE1::Quaternion::toFloat4() const
{
	alignas(16) Float4 result;
	_mm_store_ps(result.data(), m);
	return result;
}

inline GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::operator+(const Quaternion& right) const
{
	return Quaternion(_mm_add_ps(m, right.m));
}

inline GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::operator-(const Quaternion& right) const
{
	return Quaternion(_mm_sub_ps(m, right.m));
}

inline GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::operator*(const Quaternion& right) const
{
	__m128 temp = _mm_shuffle_ps(right.m, right.m, _MM_SHUFFLE(0, 0, 0, 0));
	__m128 result = _mm_mul_ps(temp, m);

	temp = _mm_shuffle_ps(right.m, right.m, _MM_SHUFFLE(1, 1, 1, 1));
	
	__m128 zuza = _mm_xor_ps(temp, _mm_set_ps(-0.0f, 0.0f, 0.0f, -0.0f));
	temp = _mm_shuffle_ps(m, m, _MM_SHUFFLE(2, 3, 0, 1));

	temp = _mm_mul_ps(temp, zuza);
	result = _mm_add_ps(result, temp);

	temp = _mm_shuffle_ps(right.m, right.m, _MM_SHUFFLE(2, 2, 2, 2));
	zuza = _mm_xor_ps(temp, _mm_set_ps(0.0f, 0.0f, -0.0f, -0.0f));

	temp = _mm_shuffle_ps(m, m, _MM_SHUFFLE(1, 0, 3, 2));
	temp = _mm_mul_ps(temp, zuza);
	result = _mm_add_ps(result, temp);

	temp = _mm_shuffle_ps(right.m, right.m, _MM_SHUFFLE(3, 3, 3, 3));
	zuza = _mm_xor_ps(temp, _mm_set_ps(0.0f, -0.0f, 0.0f, -0.0f));

	temp = _mm_shuffle_ps(m, m, _MM_SHUFFLE(0, 1, 2, 3));
	temp = _mm_mul_ps(temp, zuza);
	result = _mm_add_ps(result, temp);

	return result;
}

inline GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::operator/(const Quaternion& right) const
{
	return (*this) * right.inverse();
}

inline GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::operator+(const float right) const
{
	return Quaternion(_mm_add_ps(m, _mm_set_ps1(right)));
}

inline GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::operator-(const float right) const
{
	return Quaternion(_mm_sub_ps(m, _mm_set_ps1(right)));
}

inline GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::operator*(const float right) const
{
	return Quaternion(_mm_mul_ps(m, _mm_set_ps1(right)));
}

inline GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::operator/(const float right) const
{
	return Quaternion(_mm_div_ps(m, _mm_set_ps1(right)));
}

inline float GLOWE::SSE1::Quaternion::norm() const
{
	__m128 temp = _mm_mul_ps(m, m);
	temp = _mm_add_ps(temp, _mm_shuffle_ps(temp, temp, _MM_SHUFFLE(0, 0, 3, 2)));
	__m128 zuza = _mm_shuffle_ps(temp, temp, _MM_SHUFFLE(0, 0, 0, 1));
	temp = _mm_add_ss(temp, zuza);

	return _mm_cvtss_f32(temp);
}

inline float GLOWE::SSE1::Quaternion::modulus() const
{
	__m128 temp = _mm_mul_ps(m, m);
	temp = _mm_add_ps(temp, _mm_shuffle_ps(temp, temp, _MM_SHUFFLE(0, 0, 3, 2)));
	__m128 zuza = _mm_shuffle_ps(temp, temp, _MM_SHUFFLE(0, 0, 0, 1));
	temp = _mm_add_ss(temp, zuza);
	zuza = _mm_sqrt_ss(temp);

	return _mm_cvtss_f32(zuza);
}

inline GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::inverse() const
{
	return (conjugate() / norm());
}

inline GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::conjugate() const
{
	return _mm_xor_ps(m, _mm_set_ps(-0.0f, -0.0f, -0.0f, 0.0f));
}

inline GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::normalize() const
{
	return (*this) / modulus();
}

GLOWE::Vector3F GLOWE::SSE1::Quaternion::asEulerAngles() const
{
	alignas(16) float f[4];

	float roll, pitch, yaw;

	__m128 temp0a = _mm_shuffle_ps(m, m, _MM_SHUFFLE(2, 0, 1, 0)); // w, x, w, y
	__m128 temp0b = _mm_shuffle_ps(m, m, _MM_SHUFFLE(2, 3, 1, 1)); // x, x, z, y
	__m128 temp0 = _mm_mul_ps(temp0a, temp0b); // wx, xx, wz, yy

	__m128 temp1a = _mm_shuffle_ps(m, m, _MM_SHUFFLE(3, 1, 2, 2)); // y, y, x, z
	__m128 temp1b = _mm_shuffle_ps(m, m, _MM_SHUFFLE(3, 2, 2, 3)); // z, y, y, z
	__m128 temp1 = _mm_mul_ps(temp1a, temp1b); // yz, yy, xy, zz

	__m128 zuza = _mm_add_ps(temp0, temp1);
	zuza = _mm_add_ps(zuza, zuza);
	
	_mm_store_ps(f, zuza);

	// f[0] - 2(wx + yz)
	// f[1] - 2(xx + yy)
	// f[2] - 2(wz + xy)
	// f[3] - 2(yy + zz)

	float sinRoll = f[0];
	float cosRoll = 1.0f - f[1];

	float sinYaw = f[2];
	float cosYaw = 1.0f - f[3];

	_mm_store_ps(f, m);

	float sinPitch = 2.0f * (f[0] * f[2] - f[3] * f[1]);
	if (std::fabs(sinPitch) >= 1.0f)
	{
		pitch = std::copysign(MathHelper::Pi / 2.0f, sinPitch);
	}
	else
	{
		pitch = std::asin(sinPitch);
	}

	roll = std::atan2(sinRoll, cosRoll);
	yaw = std::atan2(sinYaw, cosYaw);

	return GLOWE::Vector3F(roll, pitch, yaw);
}

GLOWE::Vector4F GLOWE::SSE1::Quaternion::asAxisAngle() const
{
	alignas(16) float f[4];
	_mm_store_ps(f, m);

	const float zuza = 1.0f / std::sqrt(1.0f - (f[0] * f[0]));

	f[0] = std::acos(f[0]);

	__m128 temp = _mm_load_ps(f);
	__m128 temp1 = _mm_set_ps(zuza, zuza, zuza, 2.0f);
	temp = _mm_mul_ps(temp, temp1);

	return GLOWE::Vector4F(temp);
}

GLOWE::Matrix3x3 GLOWE::SSE1::Quaternion::asRotationMatrix() const
{
	// wPow2, xPow2, yPow2, zPow2
	__m128 pow2 = _mm_mul_ps(m, m);
	pow2 = _mm_add_ps(pow2, pow2);

	// xw2, yw2, zw2, xy2
	__m128 firstPart = _mm_shuffle_ps(m, m, _MM_SHUFFLE(1, 3, 2, 1));
	{
		__m128 temp = _mm_shuffle_ps(m, m, _MM_SHUFFLE(2, 0, 0, 0));

		temp = _mm_mul_ps(firstPart, temp);
		firstPart = _mm_add_ps(temp, temp);
	}

	// xz2, yz2, co�, co�
	__m128 secondPart = _mm_shuffle_ps(m, m, _MM_SHUFFLE(0, 0, 2, 1));
	{
		__m128 temp = _mm_shuffle_ps(m, m, _MM_SHUFFLE(3, 3, 3, 3));

		temp = _mm_mul_ps(secondPart, temp);
		secondPart = _mm_add_ps(temp, temp);
	}

	alignas(16) float pow2F[4], firstPartF[4], secondPartF[4];

	_mm_store_ps(pow2F, pow2);
	_mm_store_ps(firstPartF, firstPart);
	_mm_store_ps(secondPartF, secondPart);

	__m128 row0, row1, row2;

	row0 = _mm_set_ps(0.0f, secondPartF[0], firstPartF[3], 1.0f);
	row0 = _mm_sub_ps(row0, _mm_set_ps(0.0f, -firstPartF[1], firstPartF[2], pow2F[2] + pow2F[3]));

	row1 = _mm_set_ps(0.0f, secondPartF[1], 1.0f, firstPartF[3]);
	row1 = _mm_sub_ps(row1, _mm_set_ps(0.0f, firstPartF[0], pow2F[1] + pow2F[3], -firstPartF[2]));

	row2 = _mm_set_ps(0.0f, 1.0f, secondPartF[1], secondPartF[0]);
	row2 = _mm_sub_ps(row2, _mm_set_ps(0.0f, pow2F[1] + pow2F[2], -firstPartF[0], firstPartF[1]));

	return Matrix3x3(row0, row1, row2);
}

inline GLOWE::Vector3F GLOWE::SSE1::Quaternion::transformPoint(const GLOWE::Vector3F& vec) const
{
	const GLOWE::Vector3F u(m.getY(), m.getZ(), m.getW());
	const float w = m.getX();

	return 2.0f * u.scalarProduct(vec) * u + (w * w - u.getScalarProduct()) * vec + 2.0f * w * u.vectorProduct(vec);
}

GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::lookAt(const GLOWE::Vector3F& dir, const GLOWE::Vector3F& up, const GLOWE::Vector3F& forward)
{
	// https://gamedev.stackexchange.com/questions/53129/quaternion-look-at-with-up-vector
	Vector3F axis = forward.vectorProduct(dir).normalize();
	float angle = std::acos(forward.scalarProduct(dir));
	if (axis.getMagnitudeSquared() < 0.9f)
	{
		axis = up;
	}

	const Quaternion rot1 = Quaternion::fromAxisAngle(angle, axis);

	const Vector3F newUp = rot1.transformPoint(up), localRight = dir.vectorProduct(up).normalize(), localUp = localRight.vectorProduct(dir);
	axis = newUp.vectorProduct(localUp).normalize();
	if (axis.getMagnitudeSquared() < 0.9f)
	{
		return rot1;
	}
	angle = std::acos(newUp.scalarProduct(localUp));

	return Quaternion::fromAxisAngle(angle, axis) * rot1;
}

inline const GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::getIdentity()
{
	return Quaternion(1.0f, 0.0f, 0.0f, 0.0f);
}

GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::fromEulerAngles(const GLOWE::Vector3F& angles)
{
	__m128 temp = _mm_mul_ps(angles.m, _mm_set_ps1(1.0f/2.0f));

	__m128 cosArr = cos(temp);
	__m128 sinArr = sin(temp);

	__m128 zuza = _mm_mul_ps(_mm_shuffle_ps(sinArr, sinArr, _MM_SHUFFLE(3, 1, 0, 2)), cosArr);

	alignas(16) float cosF[4], sinF[4], abcF[4];

	_mm_store_ps(cosF, cosArr);
	_mm_store_ps(sinF, sinArr);
	_mm_store_ps(abcF, zuza);

	temp = _mm_set_ps(abcF[0], abcF[2], abcF[1], cosF[0] * cosF[2]);
	temp = _mm_mul_ps(temp, _mm_shuffle_ps(cosArr, cosArr, _MM_SHUFFLE(1, 0, 2, 1)));

	__m128 temp2 = _mm_set_ps(abcF[2], abcF[1], abcF[0], sinF[2] * sinF[1]);
	temp2 = _mm_mul_ps(temp2, _mm_shuffle_ps(sinArr, sinArr, _MM_SHUFFLE(0, 2, 1, 0)));
	temp2 = _mm_xor_ps(temp2, _mm_set_ps(-0.0f, 0.0f, -0.0f, 0.0f));

	return Quaternion(_mm_add_ps(temp, temp2));
}

GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::fromAxisAngle(const GLOWE::Vector4F& angles)
{
	__m128 temp = _mm_mul_ps(angles.m, _mm_set_ps(1.0f, 1.0f, 1.0f, 1.0f / 2.0f));
	const float halfAngle = _mm_cvtss_f32(temp);
	const float halfAngleSin = std::sin(halfAngle);
	const float halfAngleCos = std::cos(halfAngle);
	
	temp = _mm_move_ss(temp, _mm_set_ss(halfAngleCos));

	return Quaternion(_mm_mul_ps(temp, _mm_set_ps(halfAngleSin, halfAngleSin, halfAngleSin, 1.0f)));
}

inline GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::fromAxisAngle(const float angle, const GLOWE::Vector3F & axis)
{
	return Quaternion::fromAxisAngle(GLOWE::Vector4F(angle, axis[0], axis[1], axis[2]));
}

GLOWE::SSE1::Quaternion GLOWE::SSE1::Quaternion::fromRotationMatrix(const Matrix3x3& mat)
{
	const float w = std::sqrt(1.0f + mat(0, 0) + mat(1, 1) + mat(2, 2)) * (1.0f / 2.0f);
	const float w4 = (1.0f / 4.0f) * w;

	__m128 temp = _mm_set_ps(mat(1, 0), mat(0, 2), mat(2, 1), w);
	__m128 zuza = _mm_sub_ps(temp, _mm_set_ps(mat(0, 1), mat(2, 0), mat(1, 2), 0.0f));

	return Quaternion(_mm_mul_ps(zuza, _mm_set_ps(w4, w4, w4, 1.0f)));
}
#endif

#endif