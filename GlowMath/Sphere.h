#pragma once
#ifndef GLOWE_MATH_SPHERE_INCLUDED
#define GLOWE_MATH_SPHERE_INCLUDED

#include "../GlowSystem/NumberStorage.h"

#include "Frustum.h"
#include "VectorClasses.h"

namespace GLOWE
{
	class GLOWMATH_EXPORT alignas(16) Sphere
	{
	public:
		Float3 center;
		float range;
	public:
		Sphere() = default;
		inline Sphere(const Float3& newCenter, const float newRange)
			: center(newCenter), range(newRange)
		{}

		bool isInsidePlane(const Plane& plane) const;
		bool isInsideFrustum(const Frustum& frustum) const;
		bool isIntersectingSphere(const Sphere& sphere) const;
		bool isPointInside(const Vector3F& point) const;
	};
}

#endif
