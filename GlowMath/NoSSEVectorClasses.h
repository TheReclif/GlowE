#pragma once
#ifndef GLOWE_NOSSEVECTORCLASSES_INCLUDED
#define GLOWE_NOSSEVECTORCLASSES_INCLUDED

#include "Math.h"

namespace GLOWE
{
	namespace NoSSE
	{
		template<class Type, unsigned int ElemsCount>
		class Vector
		{
		private:
			Type elems[ElemsCount];
		public:
			inline Vector() = default;
			inline Vector(const Type allElems);
			inline Vector(const Type arg[ElemsCount]);
			inline Vector(const Array1D<Type, ElemsCount>& arg);

			inline operator GLOWE::Array1D<Type, ElemsCount>() const;

			inline Vector operator-() const;

			inline Vector operator+(const Vector& right) const;
			inline Vector operator-(const Vector& right) const;
			inline Vector operator*(const Vector& right) const;
			inline Vector operator/(const Vector& right) const;

			inline Vector operator+(const Type& right) const;
			inline Vector operator-(const Type& right) const;
			inline Vector operator*(const Type& right) const;
			inline Vector operator/(const Type& right) const;

			inline Vector& operator+=(const Vector& right);
			inline Vector& operator-=(const Vector& right);
			inline Vector& operator*=(const Vector& right);
			inline Vector& operator/=(const Vector& right);

			inline Vector& operator+=(const Type& right);
			inline Vector& operator-=(const Type& right);
			inline Vector& operator*=(const Type& right);
			inline Vector& operator/=(const Type& right);

			inline Type getMagnitude() const;
			inline Type getMagnitudeSquared() const;
			inline Type getScalarProduct() const;
			inline Type scalarProduct(const Vector& vec) const;

			inline Vector convertToRadians() const;
			inline Vector convertToDegrees() const;
			inline Vector normalize() const;
			inline Vector normalizeFast() const;

			inline Vector abs() const;
			inline Vector clamp(const Vector& min, const Vector& max) const;

			inline bool checkIsNormalized() const;

			inline bool operator==(const Vector& right) const;
			inline bool operator<(const Vector& right) const;
			inline bool operator>(const Vector& right) const;

			inline Type& operator[](const unsigned int pos);
			inline Type operator[](const unsigned int pos) const;

			template<class Func>
			inline void applyFunction(Func&& func);
		};

		template<>
		class GLOWMATH_EXPORT Vector<float, 2>
		{
		private:
			float f[2];
		public:
			inline Vector();
			inline Vector(const float a, const float b);
			inline Vector(const float ab);
			inline Vector(const float ab[2]);
			inline Vector(const Float2& ab);

			inline operator GLOWE::Float2() const;

			inline Vector operator-() const;

			inline Vector operator+(const Vector& right) const;
			inline Vector operator-(const Vector& right) const;
			inline Vector operator*(const Vector& right) const;
			inline Vector operator/(const Vector& right) const;

			inline Vector operator+(const float right) const;
			inline Vector operator-(const float right) const;
			inline Vector operator*(const float right) const;
			inline Vector operator/(const float right) const;

			inline Vector& operator+=(const Vector& right);
			inline Vector& operator-=(const Vector& right);
			inline Vector& operator*=(const Vector& right);
			inline Vector& operator/=(const Vector& right);

			inline Vector& operator+=(const float right);
			inline Vector& operator-=(const float right);
			inline Vector& operator*=(const float right);
			inline Vector& operator/=(const float right);

			inline float getMagnitude() const;
			inline float getMagnitudeSquared() const;
			inline float getScalarProduct() const;
			inline float scalarProduct(const Vector& vec) const;

			inline Vector convertToRadians() const;
			inline Vector convertToDegrees() const;
			inline Vector normalize() const;
			inline Vector normalizeFast() const;

			inline Vector abs() const;
			inline Vector clamp(const Vector& min, const Vector& max) const;

			inline bool checkIsNormalized() const;

			inline bool operator==(const Vector& right) const;
			inline bool operator<(const Vector& right) const;
			inline bool operator>(const Vector& right) const;

			inline float operator[](const unsigned int pos) const;

			inline float getX() const;
			inline float getY() const;

			inline void setX(const float arg);
			inline void setY(const float arg);

			template<class Func>
			inline void applyFunction(Func&& func);
		};

		template<>
		class GLOWMATH_EXPORT Vector<float, 3>
		{
		private:
			float f[3];
		public:
			inline Vector();
			inline Vector(const float a, const float b, const float c);
			inline Vector(const float abc);
			inline Vector(const float abc[3]);
			inline Vector(const Float3& abc);

			inline operator GLOWE::Float3() const;

			inline Vector operator-() const;

			inline Vector operator+(const Vector& right) const;
			inline Vector operator-(const Vector& right) const;
			inline Vector operator*(const Vector& right) const;
			inline Vector operator/(const Vector& right) const;

			inline Vector operator+(const float right) const;
			inline Vector operator-(const float right) const;
			inline Vector operator*(const float right) const;
			inline Vector operator/(const float right) const;

			inline Vector& operator+=(const Vector& right);
			inline Vector& operator-=(const Vector& right);
			inline Vector& operator*=(const Vector& right);
			inline Vector& operator/=(const Vector& right);

			inline Vector& operator+=(const float right);
			inline Vector& operator-=(const float right);
			inline Vector& operator*=(const float right);
			inline Vector& operator/=(const float right);

			inline float getMagnitude() const;
			inline float getMagnitudeSquared() const;
			inline float getScalarProduct() const;
			inline float scalarProduct(const Vector& vec) const;

			//Vector3F getVectorProduct() const; Is always 0.0f
			inline Vector vectorProduct(const Vector& vec) const;

			inline Vector convertToRadians() const;
			inline Vector convertToDegrees() const;
			inline Vector normalize() const;
			inline Vector normalizeFast() const;

			inline Vector abs() const;
			inline Vector clamp(const Vector& min, const Vector& max) const;

			inline bool checkIsNormalized() const;

			inline bool operator==(const Vector& right) const;
			inline bool operator<(const Vector& right) const;
			inline bool operator>(const Vector& right) const;

			inline float operator[](const unsigned int pos) const;

			inline float getX() const;
			inline float getY() const;
			inline float getZ() const;

			inline void setX(const float arg);
			inline void setY(const float arg);
			inline void setZ(const float arg);

			template<class Func>
			inline void applyFunction(Func&& func);
		};

		template<>
		class GLOWMATH_EXPORT Vector<float, 4>
		{
		private:
			float f[4];
		public:
			inline Vector();
			inline Vector(const float a, const float b, const float c, const float d);
			inline Vector(const float abcd);
			inline Vector(const float abcd[4]);
			inline Vector(const Float4& abcd);

			inline operator GLOWE::Float4() const;

			inline Vector operator-() const;

			inline Vector operator+(const Vector& right) const;
			inline Vector operator-(const Vector& right) const;
			inline Vector operator*(const Vector& right) const;
			inline Vector operator/(const Vector& right) const;

			inline Vector operator+(const float right) const;
			inline Vector operator-(const float right) const;
			inline Vector operator*(const float right) const;
			inline Vector operator/(const float right) const;

			inline Vector& operator+=(const Vector& right);
			inline Vector& operator-=(const Vector& right);
			inline Vector& operator*=(const Vector& right);
			inline Vector& operator/=(const Vector& right);

			inline Vector& operator+=(const float right);
			inline Vector& operator-=(const float right);
			inline Vector& operator*=(const float right);
			inline Vector& operator/=(const float right);

			inline float getMagnitude() const;
			inline float getMagnitudeSquared() const;
			inline float getScalarProduct() const;
			inline float scalarProduct(const Vector& vec) const;

			inline Vector convertToRadians() const;
			inline Vector convertToDegrees() const;
			inline Vector normalize() const;
			inline Vector normalizeFast() const;

			inline Vector abs() const;
			inline Vector clamp(const Vector& min, const Vector& max) const;

			inline bool checkIsNormalized() const;

			inline bool operator==(const Vector& right) const;
			inline bool operator<(const Vector& right) const;
			inline bool operator>(const Vector& right) const;

			inline float operator[](const unsigned int pos) const;

			inline float getX() const;
			inline float getY() const;
			inline float getZ() const;
			inline float getW() const;

			inline void setX(const float arg);
			inline void setY(const float arg);
			inline void setZ(const float arg);
			inline void setW(const float arg);

			template<class Func>
			inline void applyFunction(Func&& func);
		};

		template<class T, unsigned int Count>
		GLOWE::NoSSE::Vector<T, Count> operator+(const T& s, const GLOWE::NoSSE::Vector<T, Count>& vec)
		{
			return vec + s;
		}

		template<class T, unsigned int Count>
		GLOWE::NoSSE::Vector<T, Count> operator*(const T& s, const GLOWE::NoSSE::Vector<T, Count>& vec)
		{
			return vec * s;
		}

#include "NoSSEVectorClasses.inl"
	}
}

#endif
