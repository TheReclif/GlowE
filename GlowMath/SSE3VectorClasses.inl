#pragma once
#ifndef GLOWE_MATH_SSE3VECTORCLASSES_INL_INCLUDED
#define GLOWE_MATH_SSE3VECTORCLASSES_INL_INCLUDED

#include "SSE3VectorClasses.h"

#if GLOWE_USED_SSE >= GLOWE_SSE_3
inline GLOWE::SSE3::Vector2F::Vector2F(const SSE1::Vector2F& arg)
	: SSE1::Vector2F(arg.m)
{
}

inline GLOWE::SSE3::Vector2F::Vector2F(SSE1::Vector2F&& arg)
	: SSE1::Vector2F(arg.m)
{
	arg.m = _mm_setzero_ps();
}

inline GLOWE::SSE3::Vector2F& GLOWE::SSE3::Vector2F::operator=(const SSE1::Vector2F& arg)
{
	m = arg.m;
	return *this;
}

inline GLOWE::SSE3::Vector2F& GLOWE::SSE3::Vector2F::operator=(SSE1::Vector2F&& arg) noexcept
{
	m = std::move(arg.m);
	return *this;
}

inline float GLOWE::SSE3::Vector2F::getScalarProduct() const
{
	__m128 r1, r2;
	float result;

	r1 = _mm_mul_ps(m, m);
	r2 = _mm_hadd_ps(r1, r1);
	_mm_store_ss(&result, _mm_hadd_ps(r2, r2));

	return result;
}

inline float GLOWE::SSE3::Vector2F::scalarProduct(const SSE1::Vector2F & vec) const
{
	__m128 r1, r2;
	float result;

	r1 = _mm_mul_ps(m, vec.m);
	r2 = _mm_hadd_ps(r1, r1);
	_mm_store_ss(&result, _mm_hadd_ps(r2, r2));

	return result;
}

inline GLOWE::SSE3::Vector3F::Vector3F(const SSE1::Vector3F& arg)
	: SSE1::Vector3F(arg.m)
{
}

inline GLOWE::SSE3::Vector3F::Vector3F(SSE1::Vector3F&& arg)
	: SSE1::Vector3F(arg.m)
{
	arg.m = _mm_setzero_ps();
}

inline GLOWE::SSE3::Vector3F& GLOWE::SSE3::Vector3F::operator=(const SSE1::Vector3F& arg)
{
	m = arg.m;
	return *this;
}

inline GLOWE::SSE3::Vector3F& GLOWE::SSE3::Vector3F::operator=(SSE1::Vector3F&& arg) noexcept
{
	m = std::move(arg.m);
	return *this;
}

inline float GLOWE::SSE3::Vector3F::getScalarProduct() const
{
	__m128 r1, r2;
	float result;

	r1 = _mm_mul_ps(m, m);
	r2 = _mm_hadd_ps(r1, r1);
	_mm_store_ss(&result, _mm_hadd_ps(r2, r2));

	return result;
}

inline float GLOWE::SSE3::Vector3F::scalarProduct(const SSE1::Vector3F & vec) const
{
	__m128 r1, r2;
	float result;

	r1 = _mm_mul_ps(m, vec.m);
	r2 = _mm_hadd_ps(r1, r1);
	_mm_store_ss(&result, _mm_hadd_ps(r2, r2));

	return result;
}

inline GLOWE::SSE3::Vector4F::Vector4F(const SSE1::Vector4F& arg)
	: SSE1::Vector4F(arg.m)
{
}

inline GLOWE::SSE3::Vector4F::Vector4F(SSE1::Vector4F&& arg)
	: SSE1::Vector4F(arg.m)
{
	arg.m = _mm_setzero_ps();
}

inline GLOWE::SSE3::Vector4F& GLOWE::SSE3::Vector4F::operator=(const SSE1::Vector4F& arg)
{
	m = arg.m;
	return *this;
}

inline GLOWE::SSE3::Vector4F& GLOWE::SSE3::Vector4F::operator=(SSE1::Vector4F&& arg) noexcept
{
	m = std::move(arg.m);
	return *this;
}

inline float GLOWE::SSE3::Vector4F::getScalarProduct() const
{
	__m128 r1, r2;
	float result;

	r1 = _mm_mul_ps(m, m);
	r2 = _mm_hadd_ps(r1, r1);
	_mm_store_ss(&result, _mm_hadd_ps(r2, r2));

	return result;
}

inline float GLOWE::SSE3::Vector4F::scalarProduct(const SSE1::Vector4F & vec) const
{
	__m128 r1, r2;
	float result;

	r1 = _mm_mul_ps(m, vec.m);
	r2 = _mm_hadd_ps(r1, r1);
	_mm_store_ss(&result, _mm_hadd_ps(r2, r2));

	return result;
}
#endif

#endif