#pragma once
#ifndef GLOWE_MATH_QUATERNIONCLASSES_INCLUDED
#define GLOWE_MATH_QUATERNIONCLASSES_INCLUDED

#include "Math.h"

#if GLOWE_USED_SSE > GLOWE_NO_SSE
#include "SSE1QuaternionClasses.h"
#else
#include "NoSSEQuaternionClasses.h"
#endif

namespace GLOWE
{
#if GLOWE_USED_SSE > GLOWE_NO_SSE
	using Quaternion = SSE1::Quaternion;
#else
	using Quaternion = NoSSE::Quaternion;
#endif
}

#endif
