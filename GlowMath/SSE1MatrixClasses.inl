#pragma once
#ifndef GLOW_SSE1_MATRIXCLASSES_INL_INCLUDED
#define GLOW_SSE1_MATRIXCLASSES_INL_INCLUDED

#include "SSE1MatrixClasses.h"

#if GLOWE_USED_SSE > GLOWE_NO_SSE
static inline float getNthPowerOfMinusOne(const unsigned int power)
{
	return ((power % 2) == 0) ? 1.0f : -1.0f;
};

// Matrix<2, 2>

GLOWE::SSE1::Matrix<2, 2>::Matrix()
	: row0{ _mm_setzero_ps() }, row1{ _mm_setzero_ps() }
{
}

GLOWE::SSE1::Matrix<2, 2>::Matrix(const __m128 a, const __m128 b)
	: row0{ a }, row1{ b }
{
}

GLOWE::SSE1::Matrix<2, 2>::Matrix(const Float2x2& arg)
	: row0{ _mm_set_ps(0.0f, 0.0f, arg[0][1], arg[0][0]) }, row1{ _mm_set_ps(0.0f, 0.0f, arg[1][1], arg[1][0]) }
{
}

GLOWE::SSE1::Matrix<2, 2>::Matrix(const float arg[2][2])
	: row0 { _mm_set_ps(0.0f, 0.0f, arg[0][1], arg[0][0]) }, row1{ _mm_set_ps(0.0f, 0.0f, arg[1][1], arg[1][0]) }
{
}

GLOWE::SSE1::Matrix<2, 2>::Matrix(
	const float m00, const float m01,
	const float m10, const float m11
)
	: row0{ _mm_set_ps(0.0f, 0.0f, m01, m00) }, row1{ _mm_set_ps(0.0f, 0.0f, m11, m10) }
{
}

GLOWE::SSE1::Matrix<2, 2>::Matrix(const float arg)
	: row0{ _mm_set_ps(0.0f, 0.0f, arg, arg) }, row1{ _mm_set_ps(0.0f, 0.0f, arg, arg) }
{
}

float& GLOWE::SSE1::Matrix<2, 2>::operator()(const unsigned short x, const unsigned short y)
{
	return (x == 0) ? row0.f[y] : row1.f[y];
}

float GLOWE::SSE1::Matrix<2, 2>::operator()(const unsigned short x, const unsigned short y) const
{
	return (x == 0) ? row0.f[y] : row1.f[y];
}

GLOWE::SSE1::Matrix<2, 2>::operator GLOWE::Float2x2() const
{
	return Float2x2{
		Float2{ row0.f[0], row0.f[1] },
		Float2{ row1.f[0], row1.f[1] }
	};
}

GLOWE::SSE1::Matrix<2, 2> GLOWE::SSE1::Matrix<2, 2>::operator-() const
{
	return Matrix<2, 2>(_mm_sub_ps(_mm_setzero_ps(), row0.m), _mm_sub_ps(_mm_setzero_ps(), row1.m));
}

GLOWE::SSE1::Matrix<2, 2> GLOWE::SSE1::Matrix<2, 2>::operator+(GlowAbove3XMMRegister(const Matrix) right) const
{
	return Matrix<2, 2>(_mm_add_ps(row0.m, right.row0.m), _mm_add_ps(row1.m, right.row1.m));
}

GLOWE::SSE1::Matrix<2, 2> GLOWE::SSE1::Matrix<2, 2>::operator-(GlowAbove3XMMRegister(const Matrix) right) const
{
	return Matrix<2, 2>(_mm_sub_ps(row0.m, right.row0.m), _mm_sub_ps(row1.m, right.row1.m));
}

GLOWE::SSE1::Matrix<2, 2> GLOWE::SSE1::Matrix<2, 2>::operator*(GlowAbove3XMMRegister(const Matrix) right) const
{
	Matrix<2, 2> result;

	auto calcNRow2x2 = [right](const __m128 row) -> __m128
	{
		__m128 zuzia = _mm_mul_ps(_mm_shuffle_ps(row, row, _MM_SHUFFLE(0, 0, 0, 0)), right.row0.m);
		return _mm_add_ps(zuzia, _mm_mul_ps(_mm_shuffle_ps(row, row, _MM_SHUFFLE(1, 1, 1, 1)), right.row1.m));
	};

	result.row0.m = calcNRow2x2(row0.m);
	result.row1.m = calcNRow2x2(row1.m);

	return result;
}

GLOWE::SSE1::Matrix<2, 2> GLOWE::SSE1::Matrix<2, 2>::operator/(GlowAbove3XMMRegister(const Matrix) right) const
{
	return (*this) * right.inverse();
}

template<unsigned int x>
GLOWE::SSE1::Matrix<2, x> GLOWE::SSE1::Matrix<2, 2>::operator*(const Matrix<2, x>& right) const
{
	Matrix<2, x> result;

	for (unsigned int i = 0; i < 2; ++i)
	{
		for (unsigned int j = 0; j < x; ++j)
		{
			result(i, j) = (*this)(i, 0) * right(0, j) + (*this)(i, 1) * right(1, j);
		}
	}

	return result;
}

template<unsigned int x>
GLOWE::SSE1::Matrix<2, x> GLOWE::SSE1::Matrix<2, 2>::operator/(const Matrix<2, x>& right) const
{
	return (*this) * right.inverse();
}

inline GLOWE::Vector2F GLOWE::SSE1::Matrix<2, 2>::operator*(GlowAbove3XMMRegister(const GLOWE::Vector2F) right) const
{
	// row0[0], row0[1], row1[0], row1[1]
	__m128 temp = _mm_shuffle_ps(row0.m, row1.m, _MM_SHUFFLE(1, 0, 1, 0));
	
	// right[0], right[1], right[0], right[1]
	__m128 first = _mm_shuffle_ps(right.m, right.m, _MM_SHUFFLE(1, 0, 1, 0));

	first = _mm_mul_ps(first, temp);

	temp = _mm_shuffle_ps(first, first, _MM_SHUFFLE(3, 3, 0, 0));

	temp = _mm_add_ps(temp, first);

	return GLOWE::Vector2F(_mm_shuffle_ps(temp, _mm_setzero_ps(), _MM_SHUFFLE(3, 3, 2, 0)));
}

GLOWE::SSE1::Matrix<2, 2> GLOWE::SSE1::Matrix<2, 2>::operator+(const float right) const
{
	return (*this) + Matrix<2, 2>(right);
}

GLOWE::SSE1::Matrix<2, 2> GLOWE::SSE1::Matrix<2, 2>::operator-(const float right) const
{
	return (*this) - Matrix<2, 2>(right);
}

GLOWE::SSE1::Matrix<2, 2> GLOWE::SSE1::Matrix<2, 2>::operator*(const float right) const
{
	__m128 row0Temp, row1Temp;

	row0Temp = _mm_mul_ps(row0.m, _mm_set_ps1(right));
	row1Temp = _mm_mul_ps(row1.m, _mm_set_ps1(right));

	return Matrix<2, 2>(row0Temp, row1Temp);
}

GLOWE::SSE1::Matrix<2, 2> GLOWE::SSE1::Matrix<2, 2>::operator/(const float right) const
{
	__m128 row0Temp, row1Temp;

	row0Temp = _mm_div_ps(row0.m, _mm_set_ps1(right));
	row1Temp = _mm_div_ps(row1.m, _mm_set_ps1(right));

	return Matrix<2, 2>(row0Temp, row1Temp);
}

GLOWE::SSE1::Matrix<2, 2>& GLOWE::SSE1::Matrix<2, 2>::operator+=(GlowAbove3XMMRegister(const Matrix) right)
{
	return (*this) = (*this) + right;
}

GLOWE::SSE1::Matrix<2, 2>& GLOWE::SSE1::Matrix<2, 2>::operator-=(GlowAbove3XMMRegister(const Matrix) right)
{
	return (*this) = (*this) - right;
}

GLOWE::SSE1::Matrix<2, 2>& GLOWE::SSE1::Matrix<2, 2>::operator*=(GlowAbove3XMMRegister(const Matrix) right)
{
	return (*this) = (*this) * right;
}

GLOWE::SSE1::Matrix<2, 2>& GLOWE::SSE1::Matrix<2, 2>::operator/=(GlowAbove3XMMRegister(const Matrix) right)
{
	return (*this) = (*this) / right;
}

GLOWE::SSE1::Matrix<2, 2>& GLOWE::SSE1::Matrix<2, 2>::operator+=(const float right)
{
	return (*this) = (*this) + right;
}

GLOWE::SSE1::Matrix<2, 2>& GLOWE::SSE1::Matrix<2, 2>::operator-=(const float right)
{
	return (*this) = (*this) - right;
}

GLOWE::SSE1::Matrix<2, 2>& GLOWE::SSE1::Matrix<2, 2>::operator*=(const float right)
{
	return (*this) = (*this) * right;
}

GLOWE::SSE1::Matrix<2, 2>& GLOWE::SSE1::Matrix<2, 2>::operator/=(const float right)
{
	return (*this) = (*this) / right;
}

bool GLOWE::SSE1::Matrix<2, 2>::operator==(GlowAbove3XMMRegister(const Matrix) right) const
{
	__m128 zuza;
	zuza = _mm_cmpeq_ps(row0.m, right.row0.m);
	int a = _mm_movemask_ps(zuza) << 4;
	zuza = _mm_cmpeq_ps(row1.m, right.row1.m);
	a |= _mm_movemask_ps(zuza);
	return a == 0x00FF;
}

bool GLOWE::SSE1::Matrix<2, 2>::operator!=(GlowAbove3XMMRegister(const Matrix) right) const
{
	return !((*this) == right);
}

GLOWE::SSE1::Matrix<2, 2> GLOWE::SSE1::Matrix<2, 2>::transpose() const
{
	__m128 zuza, zuza1;
	zuza = _mm_unpacklo_ps(row0.m, row1.m);			// a c b d
	zuza1 = _mm_movehl_ps(_mm_setzero_ps(), zuza);  // c d 0 0
	zuza = _mm_movelh_ps(zuza, _mm_setzero_ps());	// a b 0 0
	return Matrix<2, 2>(zuza, zuza1);
}

GLOWE::SSE1::Matrix<2, 2> GLOWE::SSE1::Matrix<2, 2>::inverse() const
{
	return adjugateMatrix()* (1.0f / determinant());
}

float GLOWE::SSE1::Matrix<2, 2>::determinant() const
{
	__m128 zuza = _mm_shuffle_ps(row1.m, row1.m, 0xF1);
	zuza = _mm_mul_ps(row0.m, zuza);
	__m128 zuza1 = _mm_shuffle_ps(zuza, zuza, 0xFD);
	zuza = _mm_sub_ps(zuza, zuza1);

	return _mm_cvtss_f32(zuza);
}

float GLOWE::SSE1::Matrix<2, 2>::cofactor(const unsigned int row, const unsigned int column) const
{
	return getNthPowerOfMinusOne(row + column + 2) * minor(row, column);
}

GLOWE::SSE1::Matrix<2, 2> GLOWE::SSE1::Matrix<2, 2>::cofactorMatrix() const
{
	return Matrix<2, 2>(Float2x2
	{
		Float2{ cofactor(0, 0), cofactor(0, 1) },
		Float2{ cofactor(1, 0), cofactor(1, 1) }
	}
	);
}

GLOWE::SSE1::Matrix<2, 2> GLOWE::SSE1::Matrix<2, 2>::adjugateMatrix() const
{
	return cofactorMatrix().transpose();
}

float GLOWE::SSE1::Matrix<2, 2>::minor(const unsigned int rowToDelete, const unsigned int columnToDelete) const
{
	if (rowToDelete == 0)
	{
		return row1.f[columnToDelete == 1 ? 0 : 1];
	}

	return row0.f[columnToDelete == 1 ? 0 : 1];
}

bool GLOWE::SSE1::Matrix<2, 2>::isInvertable() const
{
	return !(MathHelper::compareFloats(0.0f, determinant()));
}

inline GLOWE::Vector2F GLOWE::SSE1::Matrix<2, 2>::getRow(const unsigned int row) const
{
	switch (row)
	{
	case 0:
		return row0.m;
	case 1:
		return row1.m;
	}

	return GLOWE::Vector2F();
}

const GLOWE::SSE1::Matrix<2, 2> GLOWE::SSE1::Matrix<2, 2>::getIdentity()
{
	static const Float2x2 zuzia{
		Float2{ 1.0f, 0.0f },
		Float2{ 0.0f, 1.0f }
	};
	return Matrix<2, 2>(zuzia);
}

// Matrix<3, 3>
GLOWE::SSE1::Matrix<3, 3>::Matrix()
	: row0{ _mm_setzero_ps() }, row1{ _mm_setzero_ps() }, row2{ _mm_setzero_ps() }
{
}

GLOWE::SSE1::Matrix<3, 3>::Matrix(const __m128 a, const __m128 b, const __m128 c)
	: row0{ a }, row1{ b }, row2{ c }
{
}

GLOWE::SSE1::Matrix<3, 3>::Matrix(const Float3x3& arg)
	: row0{ _mm_set_ps(0.0f, arg[0][2], arg[0][1], arg[0][0]) },
	row1{ _mm_set_ps(0.0f, arg[1][2], arg[1][1], arg[1][0]) },
	row2{ _mm_set_ps(0.0f, arg[2][2], arg[2][1], arg[2][0]) }
{
}

GLOWE::SSE1::Matrix<3, 3>::Matrix(const float arg[3][3])
	: row0{ _mm_set_ps(0.0f, arg[0][2], arg[0][1], arg[0][0]) },
	row1{ _mm_set_ps(0.0f, arg[1][2], arg[1][1], arg[1][0]) },
	row2{ _mm_set_ps(0.0f, arg[2][2], arg[2][1], arg[2][0]) }
{
}

GLOWE::SSE1::Matrix<3, 3>::Matrix(
	const float m00, const float m01, const float m02,
	const float m10, const float m11, const float m12,
	const float m20, const float m21, const float m22
)
	: row0{ _mm_set_ps(0.0f, m02, m01, m00) },
	row1{ _mm_set_ps(0.0f, m12, m11, m10) },
	row2{ _mm_set_ps(0.0f, m22, m21, m20) }
{
}

GLOWE::SSE1::Matrix<3, 3>::Matrix(const float arg)
	: row0{ _mm_set_ps(0.0f, arg, arg, arg) },
	row1{ _mm_set_ps(0.0f, arg, arg, arg) },
	row2{ _mm_set_ps(0.0f, arg, arg, arg) }
{
}

float& GLOWE::SSE1::Matrix<3, 3>::operator()(const unsigned short x, const unsigned short y)
{
	switch (x)
	{
	case 0:
		return row0.f[y];
		break;
	case 1:
		return row1.f[y];
		break;
	case 2:
		return row2.f[y];
		break;
	}

	throw Exception("x out of bounds.");
}

float GLOWE::SSE1::Matrix<3, 3>::operator()(const unsigned short x, const unsigned short y) const
{
	switch (x)
	{
	case 0:
		return row0.f[y];
		break;
	case 1:
		return row1.f[y];
		break;
	case 2:
		return row2.f[y];
		break;
	}

	return NAN;
}

GLOWE::SSE1::Matrix<3, 3>::operator Float3x3() const
{
	return Float3x3{
		Float3{ row0.f[0], row0.f[1], row0.f[2] },
		Float3{ row1.f[0], row1.f[1], row1.f[2] },
		Float3{ row2.f[0], row2.f[1], row2.f[2] }
	};
}

GLOWE::SSE1::Matrix<3, 3> GLOWE::SSE1::Matrix<3, 3>::operator+(GlowAbove3XMMRegister(const Matrix) right) const
{
	return Matrix<3, 3>(_mm_add_ps(row0.m, right.row0.m), _mm_add_ps(row1.m, right.row1.m), _mm_add_ps(row2.m, right.row2.m));
}

GLOWE::SSE1::Matrix<3, 3> GLOWE::SSE1::Matrix<3, 3>::operator-(GlowAbove3XMMRegister(const Matrix) right) const
{
	return Matrix<3, 3>(_mm_sub_ps(row0.m, right.row0.m), _mm_sub_ps(row1.m, right.row1.m), _mm_sub_ps(row2.m, right.row2.m));
}

GLOWE::SSE1::Matrix<3, 3> GLOWE::SSE1::Matrix<3, 3>::operator*(GlowAbove3XMMRegister(const Matrix) right) const
{
	Matrix<3, 3> result;

	auto calcNthRow = [right](const __m128 row) -> __m128
	{
		__m128 result = _mm_mul_ps(_mm_shuffle_ps(row, row, _MM_SHUFFLE(0, 0, 0, 0)), right.row0.m);
		result = _mm_add_ps(result, _mm_mul_ps(_mm_shuffle_ps(row, row, _MM_SHUFFLE(1, 1, 1, 1)), right.row1.m));
		return _mm_add_ps(result, _mm_mul_ps(_mm_shuffle_ps(row, row, _MM_SHUFFLE(2, 2, 2, 2)), right.row2.m));
	};

	result.row0.m = calcNthRow(row0.m);
	result.row1.m = calcNthRow(row1.m);
	result.row2.m = calcNthRow(row2.m);

	return result;
}

GLOWE::SSE1::Matrix<3, 3> GLOWE::SSE1::Matrix<3, 3>::operator/(GlowAbove3XMMRegister(const Matrix) right) const
{
	return (*this) + right.inverse();
}

template<unsigned int x>
GLOWE::SSE1::Matrix<3, x> GLOWE::SSE1::Matrix<3, 3>::operator*(const Matrix<3, x>& right) const
{
	Matrix<3, x> result;

	for (unsigned int i = 0; i < 3; ++i)
	{
		for (unsigned int j = 0; j < x; ++j)
		{
			result(i, j) = (*this)(i, 0) * right(0, j) + (*this)(i, 1) * right(1, j) + (*this)(i, 2) * right(2, j);
		}
	}

	return result;
}

template<unsigned int x>
GLOWE::SSE1::Matrix<3, x> GLOWE::SSE1::Matrix<3, 3>::operator/(const Matrix<3, x>& right) const
{
	return (*this) * right.inverse();
}

inline GLOWE::Vector3F GLOWE::SSE1::Matrix<3, 3>::operator*(GlowAbove3XMMRegister(const GLOWE::Vector3F) right) const
{
	// a b c 0
	__m128 first = _mm_mul_ps(row0.m, right.m);
	// d e f 0
	__m128 second = _mm_mul_ps(row1.m, right.m);
	// g h i 0
	__m128 third = _mm_mul_ps(row2.m, right.m);

	Matrix<3, 3> temp(first, second, third);
	temp = temp.transpose();

	first = temp.row0.m;
	second = temp.row1.m;
	third = temp.row2.m;

	first = _mm_add_ps(first, second);
	third = _mm_add_ps(third, first);

	return GLOWE::Vector3F(third);
}

GLOWE::SSE1::Matrix<3, 3> GLOWE::SSE1::Matrix<3, 3>::operator+(const float right) const
{
	__m128 zuza = _mm_add_ps(row0.m, _mm_set_ps1(right));
	__m128 zuza1 = _mm_add_ps(row1.m, _mm_set_ps1(right));
	__m128 zuza2 = _mm_add_ps(row2.m, _mm_set_ps1(right));

	return Matrix<3, 3>(zuza, zuza1, zuza2);
}

GLOWE::SSE1::Matrix<3, 3> GLOWE::SSE1::Matrix<3, 3>::operator-(const float right) const
{
	__m128 zuza = _mm_sub_ps(row0.m, _mm_set_ps1(right));
	__m128 zuza1 = _mm_sub_ps(row1.m, _mm_set_ps1(right));
	__m128 zuza2 = _mm_sub_ps(row2.m, _mm_set_ps1(right));

	return Matrix<3, 3>(zuza, zuza1, zuza2);
}

GLOWE::SSE1::Matrix<3, 3> GLOWE::SSE1::Matrix<3, 3>::operator*(const float right) const
{
	__m128 zuza = _mm_mul_ps(row0.m, _mm_set_ps1(right));
	__m128 zuza1 = _mm_mul_ps(row1.m, _mm_set_ps1(right));
	__m128 zuza2 = _mm_mul_ps(row2.m, _mm_set_ps1(right));

	return Matrix<3, 3>(zuza, zuza1, zuza2);
}

GLOWE::SSE1::Matrix<3, 3> GLOWE::SSE1::Matrix<3, 3>::operator/(const float right) const
{
	__m128 zuza = _mm_div_ps(row0.m, _mm_set_ps1(right));
	__m128 zuza1 = _mm_div_ps(row1.m, _mm_set_ps1(right));
	__m128 zuza2 = _mm_div_ps(row2.m, _mm_set_ps1(right));

	return Matrix<3, 3>(zuza, zuza1, zuza2);
}

GLOWE::SSE1::Matrix<3, 3>& GLOWE::SSE1::Matrix<3, 3>::operator+=(GlowAbove3XMMRegister(const Matrix) right)
{
	return (*this) = (*this) + right;
}

GLOWE::SSE1::Matrix<3, 3>& GLOWE::SSE1::Matrix<3, 3>::operator-=(GlowAbove3XMMRegister(const Matrix) right)
{
	return (*this) = (*this) - right;
}

GLOWE::SSE1::Matrix<3, 3>& GLOWE::SSE1::Matrix<3, 3>::operator*=(GlowAbove3XMMRegister(const Matrix) right)
{
	return (*this) = (*this) * right;
}

GLOWE::SSE1::Matrix<3, 3>& GLOWE::SSE1::Matrix<3, 3>::operator/=(GlowAbove3XMMRegister(const Matrix) right)
{
	return (*this) = (*this) / right;
}

GLOWE::SSE1::Matrix<3, 3>& GLOWE::SSE1::Matrix<3, 3>::operator+=(const float right)
{
	return (*this) = (*this) + right;
}

GLOWE::SSE1::Matrix<3, 3>& GLOWE::SSE1::Matrix<3, 3>::operator-=(const float right)
{
	return (*this) = (*this) - right;
}

GLOWE::SSE1::Matrix<3, 3>& GLOWE::SSE1::Matrix<3, 3>::operator*=(const float right)
{
	return (*this) = (*this) * right;
}

GLOWE::SSE1::Matrix<3, 3>& GLOWE::SSE1::Matrix<3, 3>::operator/=(const float right)
{
	return (*this) = (*this) / right;
}

bool GLOWE::SSE1::Matrix<3, 3>::operator==(GlowAbove3XMMRegister(const Matrix) right) const
{
	__m128 zuza;
	zuza = _mm_cmpeq_ps(row0.m, right.row0.m);
	int a = _mm_movemask_ps(zuza) << 8;
	zuza = _mm_cmpeq_ps(row1.m, right.row1.m);
	a |= _mm_movemask_ps(zuza) << 4;
	zuza = _mm_cmpeq_ps(row2.m, right.row2.m);
	a |= _mm_movemask_ps(zuza);
	return a == 0x0FFF;
}

bool GLOWE::SSE1::Matrix<3, 3>::operator!=(GlowAbove3XMMRegister(const Matrix) right) const
{
	return !((*this) == right);
}

GLOWE::SSE1::Matrix<3, 3> GLOWE::SSE1::Matrix<3, 3>::transpose() const
{
	__m128 zuza, zuza1, zuza2;

	zuza = _mm_unpacklo_ps(row0.m, row1.m);			// a d b e
	zuza2 = _mm_unpackhi_ps(row0.m, row1.m);		// c f 0 0

	zuza1 = _mm_shuffle_ps(zuza, row2.m, _MM_SHUFFLE(3, 0, 1, 0));  // a d g 0
	zuza = _mm_shuffle_ps(zuza, row2.m, _MM_SHUFFLE(3, 1, 3, 2));	// b e h 0
	zuza2 = _mm_shuffle_ps(zuza2, row2.m, _MM_SHUFFLE(3, 2, 1, 0)); // c f i 0

	return Matrix<3, 3>(zuza1, zuza, zuza2);
}

GLOWE::SSE1::Matrix<3, 3> GLOWE::SSE1::Matrix<3, 3>::inverse() const
{
	return adjugateMatrix()* (1.0f / determinant());
}

float GLOWE::SSE1::Matrix<3, 3>::determinant() const
{
	Vector3F vecA(row0.m), vecB(row1.m), vecC(row2.m);

	return vecA.scalarProduct(vecB.vectorProduct(vecC));
}

float GLOWE::SSE1::Matrix<3, 3>::cofactor(const unsigned int row, const unsigned int column) const
{
	return getNthPowerOfMinusOne(row + column + 2) * minor(row, column);
}

GLOWE::SSE1::Matrix<3, 3> GLOWE::SSE1::Matrix<3, 3>::cofactorMatrix() const
{
	Matrix<3, 3> result;

	for (unsigned int x = 0; x < 3; ++x)
	{
		for (unsigned int y = 0; y < 3; ++y)
		{
			result(x, y) = cofactor(x, y);
		}
	}

	return result;
}

GLOWE::SSE1::Matrix<3, 3> GLOWE::SSE1::Matrix<3, 3>::adjugateMatrix() const
{
	return cofactorMatrix().transpose();
}

float GLOWE::SSE1::Matrix<3, 3>::minor(const unsigned int rowToDelete, const unsigned int columnToDelete) const
{
	Matrix<2, 2> zuza;
	unsigned int tempRowID = 0;

	for (unsigned int x = 0; x < 3; ++x)
	{
		if (x != rowToDelete)
		{
			for (unsigned int y = 0, tempColumnID = 0; y < 3; ++y)
			{
				if (y != columnToDelete)
				{
					zuza(tempRowID, tempColumnID) = (*this)(x, y);
					++tempColumnID;
				}
			}

			++tempRowID;
		}
	}

	return zuza.determinant();
}

bool GLOWE::SSE1::Matrix<3, 3>::isInvertable() const
{
	return determinant() != 0.0f;
}

inline GLOWE::Vector3F GLOWE::SSE1::Matrix<3, 3>::getRow(const unsigned int row) const
{
	switch (row)
	{
	case 0:
		return row0.m;
	case 1:
		return row1.m;
	case 2:
		return row2.m;
	}

	return GLOWE::Vector3F();
}

const GLOWE::SSE1::Matrix<3, 3> GLOWE::SSE1::Matrix<3, 3>::getIdentity()
{
	static const Float3x3 zuzia{
		Float3{ 1.0f, 0.0f, 0.0f },
		Float3{ 0.0f, 1.0f, 0.0f },
		Float3{ 0.0f, 0.0f, 1.0f }
	};
	return Matrix<3, 3>(zuzia);
}

// Matrix<4, 4>

GLOWE::SSE1::Matrix<4, 4>::Matrix()
	: row0{ _mm_setzero_ps() },
	row1{ _mm_setzero_ps() },
	row2{ _mm_setzero_ps() },
	row3{ _mm_setzero_ps() }
{
}

GLOWE::SSE1::Matrix<4, 4>::Matrix(const __m128 a, const __m128 b, const __m128 c, GlowAbove3XMMRegister(const __m128) d)
	: row0{ a },
	row1{ b },
	row2{ c },
	row3{ d }
{
}

GLOWE::SSE1::Matrix<4, 4>::Matrix(const Float4x4& arg)
	: row0{ _mm_set_ps(arg[0][3], arg[0][2], arg[0][1], arg[0][0]) },
	row1{ _mm_set_ps(arg[1][3], arg[1][2], arg[1][1], arg[1][0]) },
	row2{ _mm_set_ps(arg[2][3], arg[2][2], arg[2][1], arg[2][0]) },
	row3{ _mm_set_ps(arg[3][3], arg[3][2], arg[3][1], arg[3][0]) }
{
}

GLOWE::SSE1::Matrix<4, 4>::Matrix(const float arg[4][4])
	: row0{ _mm_set_ps(arg[0][3], arg[0][2], arg[0][1], arg[0][0]) },
	row1{ _mm_set_ps(arg[1][3], arg[1][2], arg[1][1], arg[1][0]) },
	row2{ _mm_set_ps(arg[2][3], arg[2][2], arg[2][1], arg[2][0]) },
	row3{ _mm_set_ps(arg[3][3], arg[3][2], arg[3][1], arg[3][0]) }
{
}

GLOWE::SSE1::Matrix<4, 4>::Matrix(
	const float m00, const float m01, const float m02, const float m03,
	const float m10, const float m11, const float m12, const float m13,
	const float m20, const float m21, const float m22, const float m23,
	const float m30, const float m31, const float m32, const float m33
)
	: row0{ _mm_set_ps(m03, m02, m01, m00) },
	row1{ _mm_set_ps(m13, m12, m11, m10) },
	row2{ _mm_set_ps(m23, m22, m21, m20) },
	row3{ _mm_set_ps(m33, m32, m31, m30) }
{
}

GLOWE::SSE1::Matrix<4, 4>::Matrix(const float arg)
	: row0{ _mm_set_ps1(arg) },
	row1{ _mm_set_ps1(arg) },
	row2{ _mm_set_ps1(arg) },
	row3{ _mm_set_ps1(arg) }
{
}

float& GLOWE::SSE1::Matrix<4, 4>::operator()(const unsigned short x, const unsigned short y)
{
	switch (x)
	{
	case 0:
		return row0.f[y];
		break;
	case 1:
		return row1.f[y];
		break;
	case 2:
		return row2.f[y];
		break;
	default:
		return row3.f[y];
		break;
	}
}

float GLOWE::SSE1::Matrix<4, 4>::operator()(const unsigned short x, const unsigned short y) const
{
	switch (x)
	{
	case 0:
		return row0.f[y];
		break;
	case 1:
		return row1.f[y];
		break;
	case 2:
		return row2.f[y];
		break;
	default:
		return row3.f[y];
		break;
	}
}

GLOWE::SSE1::Matrix<4, 4>::operator GLOWE::Float4x4() const
{
	return Float4x4{
		Float4{ row0.f[0], row0.f[1], row0.f[2], row0.f[3] },
		Float4{ row1.f[0], row1.f[1], row1.f[2], row1.f[3] },
		Float4{ row2.f[0], row2.f[1], row2.f[2], row2.f[3] },
		Float4{ row3.f[0], row3.f[1], row3.f[2], row3.f[3] }
	};
}

GLOWE::SSE1::Matrix<4, 4> GLOWE::SSE1::Matrix<4, 4>::operator-() const
{
	__m128 a = _mm_sub_ps(_mm_setzero_ps(), row0.m);
	__m128 b = _mm_sub_ps(_mm_setzero_ps(), row1.m);
	__m128 c = _mm_sub_ps(_mm_setzero_ps(), row2.m);
	__m128 d = _mm_sub_ps(_mm_setzero_ps(), row3.m);

	return Matrix<4, 4>(a, b, c, d);
}

GLOWE::SSE1::Matrix<4, 4> GLOWE::SSE1::Matrix<4, 4>::operator+(GlowAbove3XMMRegister(const Matrix) right) const
{
	return Matrix<4, 4>(
		_mm_add_ps(row0.m, right.row0.m),
		_mm_add_ps(row1.m, right.row1.m),
		_mm_add_ps(row2.m, right.row2.m),
		_mm_add_ps(row3.m, right.row3.m));
}

GLOWE::SSE1::Matrix<4, 4> GLOWE::SSE1::Matrix<4, 4>::operator-(GlowAbove3XMMRegister(const Matrix) right) const
{
	return Matrix<4, 4>(
		_mm_sub_ps(row0.m, right.row0.m),
		_mm_sub_ps(row1.m, right.row1.m),
		_mm_sub_ps(row2.m, right.row2.m),
		_mm_sub_ps(row3.m, right.row3.m));
}

GLOWE::SSE1::Matrix<4, 4> GLOWE::SSE1::Matrix<4, 4>::operator*(GlowAbove3XMMRegister(const Matrix) right) const
{
	auto calcNthRow = [right](const __m128 row) -> __m128
	{
		__m128 result = _mm_mul_ps(_mm_shuffle_ps(row, row, _MM_SHUFFLE(0, 0, 0, 0)), right.row0.m);
		result = _mm_add_ps(result, _mm_mul_ps(_mm_shuffle_ps(row, row, _MM_SHUFFLE(1, 1, 1, 1)), right.row1.m));
		result =  _mm_add_ps(result, _mm_mul_ps(_mm_shuffle_ps(row, row, _MM_SHUFFLE(2, 2, 2, 2)), right.row2.m));
		return _mm_add_ps(result, _mm_mul_ps(_mm_shuffle_ps(row, row, _MM_SHUFFLE(3, 3, 3, 3)), right.row3.m));
	};

	return Matrix<4, 4>(calcNthRow(row0.m), calcNthRow(row1.m), calcNthRow(row2.m), calcNthRow(row3.m));
}

GLOWE::SSE1::Matrix<4, 4> GLOWE::SSE1::Matrix<4, 4>::operator/(GlowAbove3XMMRegister(const Matrix) right) const
{
	return (*this) * right.inverse();
}

template<unsigned int x>
GLOWE::SSE1::Matrix<4, x> GLOWE::SSE1::Matrix<4, 4>::operator*(const Matrix<4, x>& right) const
{
	Matrix<4, x> result;

	for (unsigned int i = 0; i < 4; ++i)
	{
		for (unsigned int j = 0; j < x; ++j)
		{
			result(i, j) = (*this)(i, 0) * right(0, j) + (*this)(i, 1) * right(1, j) + (*this)(i, 2) * right(2, j) + (*this)(i, 3) * right(3, j);
		}
	}

	return result;
}

template<unsigned int x>
GLOWE::SSE1::Matrix<4, x> GLOWE::SSE1::Matrix<4, 4>::operator/(const Matrix<4, x>& right) const
{
	return (*this) * right.inverse();
}

inline GLOWE::Vector4F GLOWE::SSE1::Matrix<4, 4>::operator*(GlowAbove3XMMRegister(const GLOWE::Vector4F) right) const
{
	__m128 first = _mm_mul_ps(row0.m, right.m);
	__m128 second = _mm_mul_ps(row1.m, right.m);
	__m128 third = _mm_mul_ps(row2.m, right.m);
	__m128 fourth = _mm_mul_ps(row3.m, right.m);

	Matrix<4, 4> temp(first, second, third, fourth);
	temp = temp.transpose();

	first = temp.row0.m;
	second = temp.row1.m;
	third = temp.row2.m;
	fourth = temp.row3.m;

	first = _mm_add_ps(first, third);
	second = _mm_add_ps(second, fourth);
	first = _mm_add_ps(first, second);

	return GLOWE::Vector4F(first);
}

GLOWE::SSE1::Matrix<4, 4> GLOWE::SSE1::Matrix<4, 4>::operator+(const float right) const
{
	__m128 a = _mm_add_ps(row0.m, _mm_set_ps1(right));
	__m128 b = _mm_add_ps(row1.m, _mm_set_ps1(right));
	__m128 c = _mm_add_ps(row2.m, _mm_set_ps1(right));
	__m128 d = _mm_add_ps(row3.m, _mm_set_ps1(right));

	return Matrix<4, 4>(a, b, c, d);
}

GLOWE::SSE1::Matrix<4, 4> GLOWE::SSE1::Matrix<4, 4>::operator-(const float right) const
{
	__m128 a = _mm_sub_ps(row0.m, _mm_set_ps1(right));
	__m128 b = _mm_sub_ps(row1.m, _mm_set_ps1(right));
	__m128 c = _mm_sub_ps(row2.m, _mm_set_ps1(right));
	__m128 d = _mm_sub_ps(row3.m, _mm_set_ps1(right));

	return Matrix<4, 4>(a, b, c, d);
}

GLOWE::SSE1::Matrix<4, 4> GLOWE::SSE1::Matrix<4, 4>::operator*(const float right) const
{
	__m128 a = _mm_mul_ps(row0.m, _mm_set_ps1(right));
	__m128 b = _mm_mul_ps(row1.m, _mm_set_ps1(right));
	__m128 c = _mm_mul_ps(row2.m, _mm_set_ps1(right));
	__m128 d = _mm_mul_ps(row3.m, _mm_set_ps1(right));

	return Matrix<4, 4>(a, b, c, d);
}

GLOWE::SSE1::Matrix<4, 4> GLOWE::SSE1::Matrix<4, 4>::operator/(const float right) const
{
	__m128 a = _mm_div_ps(row0.m, _mm_set_ps1(right));
	__m128 b = _mm_div_ps(row1.m, _mm_set_ps1(right));
	__m128 c = _mm_div_ps(row2.m, _mm_set_ps1(right));
	__m128 d = _mm_div_ps(row3.m, _mm_set_ps1(right));

	return Matrix<4, 4>(a, b, c, d);
}

GLOWE::SSE1::Matrix<4, 4>& GLOWE::SSE1::Matrix<4, 4>::operator+=(GlowAbove3XMMRegister(const Matrix) right)
{
	return (*this) = (*this) + right;
}

GLOWE::SSE1::Matrix<4, 4>& GLOWE::SSE1::Matrix<4, 4>::operator-=(GlowAbove3XMMRegister(const Matrix) right)
{
	return (*this) = (*this) - right;
}

GLOWE::SSE1::Matrix<4, 4>& GLOWE::SSE1::Matrix<4, 4>::operator*=(GlowAbove3XMMRegister(const Matrix) right)
{
	return (*this) = (*this) * right;
}

GLOWE::SSE1::Matrix<4, 4>& GLOWE::SSE1::Matrix<4, 4>::operator/=(GlowAbove3XMMRegister(const Matrix) right)
{
	return (*this) = (*this) / right;
}

GLOWE::SSE1::Matrix<4, 4>& GLOWE::SSE1::Matrix<4, 4>::operator+=(const float right)
{
	return (*this) = (*this) + right;
}

GLOWE::SSE1::Matrix<4, 4>& GLOWE::SSE1::Matrix<4, 4>::operator-=(const float right)
{
	return (*this) = (*this) - right;
}

GLOWE::SSE1::Matrix<4, 4>& GLOWE::SSE1::Matrix<4, 4>::operator*=(const float right)
{
	return (*this) = (*this) * right;
}

GLOWE::SSE1::Matrix<4, 4>& GLOWE::SSE1::Matrix<4, 4>::operator/=(const float right)
{
	return (*this) = (*this) / right;
}

bool GLOWE::SSE1::Matrix<4, 4>::operator==(GlowAbove3XMMRegister(const Matrix) right) const
{
	__m128 zuza;
	zuza = _mm_cmpeq_ps(row0.m, right.row0.m);
	int a = _mm_movemask_ps(zuza);
	zuza = _mm_cmpeq_ps(row1.m, right.row1.m);
	a &= _mm_movemask_ps(zuza);
	zuza = _mm_cmpeq_ps(row2.m, right.row2.m);
	a &= _mm_movemask_ps(zuza);
	zuza = _mm_cmpeq_ps(row3.m, right.row3.m);
	a &= _mm_movemask_ps(zuza);
	return a == 0xF;
}

bool GLOWE::SSE1::Matrix<4, 4>::operator!=(GlowAbove3XMMRegister(const Matrix) right) const
{
	return !((*this) == right);
}

GLOWE::SSE1::Matrix<4, 4> GLOWE::SSE1::Matrix<4, 4>::transpose() const
{
	Matrix<4, 4> result(row0.m, row1.m, row2.m, row3.m);

	_MM_TRANSPOSE4_PS(result.row0.m, result.row1.m, result.row2.m, result.row3.m);

	return result;
}

GLOWE::SSE1::Matrix<4, 4> GLOWE::SSE1::Matrix<4, 4>::inverse() const
{
	return adjugateMatrix()* (1.0f / determinant());
}

float GLOWE::SSE1::Matrix<4, 4>::determinant() const
{
	Matrix<2, 2> zuza(
		row0.f[0], row0.f[1],
		row1.f[0], row1.f[1]
	);
	Matrix<2, 2> zuza1(
		row0.f[2], row0.f[3],
		row1.f[2], row1.f[3]
	);
	Matrix<2, 2> zuza2(
		row2.f[0], row2.f[1],
		row3.f[0], row3.f[1]
	);
	Matrix<2, 2> zuza3(
		row2.f[2], row2.f[3],
		row3.f[2], row3.f[3]
	);

	return zuza3.determinant()* (zuza - zuza1 * zuza3.inverse()* zuza2).determinant();
}

float GLOWE::SSE1::Matrix<4, 4>::cofactor(const unsigned int row, const unsigned int column) const
{
	return getNthPowerOfMinusOne(row + column + 2) * minor(row, column);
}

GLOWE::SSE1::Matrix<4, 4> GLOWE::SSE1::Matrix<4, 4>::cofactorMatrix() const
{
	Matrix<4, 4> result;

	for (unsigned int x = 0; x < 4; ++x)
	{
		for (unsigned int y = 0; y < 4; ++y)
		{
			result(x, y) = cofactor(x, y);
		}
	}

	return result;
}

GLOWE::SSE1::Matrix<4, 4> GLOWE::SSE1::Matrix<4, 4>::adjugateMatrix() const
{
	return cofactorMatrix().transpose();
}

float GLOWE::SSE1::Matrix<4, 4>::minor(const unsigned int rowToDelete, const unsigned int columnToDelete) const
{
	Matrix<3, 3> zuza;
	unsigned int tempRowID = 0;

	for (unsigned int x = 0; x < 4; ++x)
	{
		if (x != rowToDelete)
		{
			for (unsigned int y = 0, tempColumnID = 0; y < 4; ++y)
			{
				if (y != columnToDelete)
				{
					zuza(tempRowID, tempColumnID) = (*this)(x, y);
					++tempColumnID;
				}
			}

			++tempRowID;
		}
	}

	return zuza.determinant();
}

bool GLOWE::SSE1::Matrix<4, 4>::isInvertable() const
{
	return determinant() != 0.0f;
}

inline GLOWE::Vector4F GLOWE::SSE1::Matrix<4, 4>::getRow(const unsigned int row) const
{
	switch (row)
	{
	case 0:
		return row0.m;
	case 1:
		return row1.m;
	case 2:
		return row2.m;
	case 3:
		return row3.m;
	}

	return GLOWE::Vector4F();
}

const GLOWE::SSE1::Matrix<4, 4> GLOWE::SSE1::Matrix<4, 4>::getIdentity()
{
	static const Float4x4 zuza{
		Float4{ 1.0f, 0.0f, 0.0f, 0.0f },
		Float4{ 0.0f, 1.0f, 0.0f, 0.0f },
		Float4{ 0.0f, 0.0f, 1.0f, 0.0f },
		Float4{ 0.0f, 0.0f, 0.0f, 1.0f }
	};
	return Matrix<4, 4>(zuza);
}
#undef GlowAbove3XMMRegister
#endif

#endif
