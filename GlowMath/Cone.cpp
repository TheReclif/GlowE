#include "Cone.h"

// https://books.google.com/books?id=WGpL6Sk9qNAC&pg=PA165&lpg=PA165&dq=cone+intersection+with+plane+test&source=bl&ots=Pn5ShG2feR&sig=e5PmVOesQDZxp9KobbHJr15OHeU&hl=en&sa=X&ved=0ahUKEwjvwfCE0PPRAhXLQSYKHdI2Ac44ChDoAQhEMAk#v=onepage&q=cone%20intersection%20with%20plane%20test&f=false
bool GLOWE::Cone::isInsidePlane(const Plane& plane) const
{
	if (plane.isPointInside(center))
	{
		return true;
	}

	const float r = range * std::tan(spot);

	const Vector3F dirVec = direction;
	const Vector3F m = Vector3F(plane.getNormal()).vectorProduct(dirVec).vectorProduct(dirVec);
	const Vector3F mostExtendedPoint = Vector3F(center) + (Vector3F(direction) * range) + (m * r);

	return plane.isPointInside(mostExtendedPoint);
}

bool GLOWE::Cone::isInsideFrustum(const Frustum& frustum) const
{
	for (unsigned int x = 0; x < 6; ++x)
	{
		if (!isInsidePlane(frustum.planes[x]))
		{
			return false;
		}
	}

	return true;
}
