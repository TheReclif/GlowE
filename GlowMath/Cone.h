#pragma once
#ifndef GLOWE_MATH_CONE_INCLUDED
#define GLOWE_MATH_CONE_INCLUDED

#include "../GlowSystem/NumberStorage.h"

#include "Frustum.h"

namespace GLOWE
{
	class GLOWMATH_EXPORT alignas(16) Cone
	{
	public:
		Float3 center, direction;
		float range, spot;
	public:
		Cone() = default;
		inline Cone(const Float3& newCenter, const Float3& newDir, const float newRange, const float newSpot)
			: center(newCenter), direction(Vector3F(newDir).normalize().toFloat3()), range(newRange), spot(newSpot)
		{} // No need to normalize newDir.

		bool isInsidePlane(const Plane& plane) const;
		bool isInsideFrustum(const Frustum& frustum) const;
	};
}

#endif
