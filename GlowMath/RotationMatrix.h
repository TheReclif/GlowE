#pragma once
#ifndef GLOWE_MATH_ROTATIONMATRIX_INCLUDED
#define GLOWE_MATH_ROTATIONMATRIX_INCLUDED

#include "glowmath_export.h"

#include "MatrixClasses.h"
#include "QuaternionClasses.h"
#include "VectorClasses.h"

namespace GLOWE
{
	class GLOWMATH_EXPORT alignas(alignof(Matrix3x3)) RotationMatrix
		: public Matrix3x3
	{
	private:
		using Mat3x3 = const Matrix3x3&;
		using Mat4x4 = const Matrix4x4&;
		using Vec4 = const Vector4F&;
		using Vec3 = const Vector3F&;
		using Quat = const Quaternion&;
	public:
		RotationMatrix();
		RotationMatrix(const RotationMatrix&) = default;
		RotationMatrix(RotationMatrix&&) = default;
		RotationMatrix(Mat3x3 arg);
		RotationMatrix(Mat4x4 arg);

		~RotationMatrix() = default;

		RotationMatrix& operator=(const RotationMatrix&) = default;
		RotationMatrix& operator=(RotationMatrix&&) = default;

		inline operator GLOWE::Matrix4x4() const;

		inline Quaternion toQuaternion() const;
		inline Vector4F toAxisAngle() const; // Angle, axis.x, axis.y, axis.z
		inline Vector3F toEulerAngles() const;

		static inline RotationMatrix fromQuaternion(const Quat qt);
		static inline RotationMatrix fromAxisAngle(const Vec4 vec);
		static inline RotationMatrix fromEulerAngles(const Vec3 vec); // x, y and z

		static inline Matrix3x3 mat4x4ToMat3x3(const Mat4x4 arg);
	};
}

#if GLOWE_USED_SSE > GLOWE_NO_SSE
#include "SSE1RotationMatrix.inl"
#else
#include "NoSSERotationMatrix.inl"
#endif

#endif
