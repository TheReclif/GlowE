#pragma once
#ifndef GLOWE_MATHHELPER_INCLUDED
#define GLOWE_MATHHELPER_INCLUDED

#include "glowmath_export.h"

#include "../GlowSystem/Utility.h"
#include "../GlowSystem/NumberStorage.h"

// SIMD.
#if GLOWE_USED_SSE > GLOWE_NO_SSE
#include <smmintrin.h>
#include <emmintrin.h>
#include <xmmintrin.h>
#ifdef _MSC_VER
#define GLOW_VECTORCALL __vectorcall
#else
#define GLOW_VECTORCALL
#endif
#endif

// https://docs.microsoft.com/en-us/windows/desktop/direct3d9/dx9-graphics-reference-d3dx-functions-math

#define checkIsDirectXMathSupported checkIsXNAMathSupported

#ifdef _MSC_VER
#define cpuid(info, x)    __cpuidex(info, x, 0)
#else
#include <cpuid.h>
inline void cpuid(int info[4], int InfoType) 
{
	__cpuid_count(InfoType, 0, info[0], info[1], info[2], info[3]);
}
#endif

namespace GLOWE
{
	namespace MathHelper
	{
		namespace Priv
		{
			static const union
			{
				UInt32 _uint32MaskAll = 0xFFFFFFFF;
				float _floatAllBitsSet;
			};
		}

		constexpr float Pi = 3.141592653589793238462643f;
		constexpr float Pi2 = Pi * 2.0f;
		constexpr float PiDiv2 = Pi / 2.0f;
		constexpr float PiDiv4 = Pi / 4.0f;
		constexpr float OneDivPi = 1.0f / Pi;
		constexpr float OneDivPi2 = 1.0f / Pi2;

		constexpr float Epsilon = 0.00001f;

		const float FloatAllBitsSet = Priv::_floatAllBitsSet;

		GLOWMATH_EXPORT bool checkIsChosenSSESupported();

		//float calcAspectRatio(const float width, const float height);

		inline constexpr float convertToRadians(const float degrees)
		{
			return degrees * Pi / 180.0f;
		}

		inline constexpr float convertToDegrees(const float radians)
		{
			return radians * 180.0f / Pi;
		}

		inline constexpr float fabs(const float arg)
		{
			return (arg > 0.0f) ? arg : -arg;
		}

		inline constexpr bool compareFloats(const float a, const float b)
		{
			return fabs(a - b) <= MathHelper::Epsilon;
		}

		inline constexpr float calcAspectRatio(const Float2& resolution)
		{
			return resolution[0] / resolution[1];
		}

		inline constexpr float calcAspectRatio(const float width, const float height)
		{
			return width / height;
		}

		inline UInt2 getShaderVersionFromFloat(const float arg)
		{
			return UInt2{ static_cast<unsigned int>(arg - std::fmod(arg, 1.0f)), static_cast<unsigned int>(10.0f * std::fmod(arg, 1.0f)) };
		}

		template<class T>
		inline constexpr T lerp(const T& a, const T& b, const float t)
		{
			return (a * (1.0f - t)) + (b * t);
		}

		template<class T>
		inline constexpr T inverseLerp(const T& a, const T& b, const T& value)
		{
			return (a - value) / (a - b);
		}
	}
}

#endif