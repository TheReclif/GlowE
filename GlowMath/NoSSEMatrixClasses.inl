#pragma once
#ifndef GLOWE_MATH_NOSSE_INL_MATRIXCLASSES_INCLUDED
#define GLOWE_MATH_NOSSE_INL_MATRIXCLASSES_INCLUDED

#include "NoSSEMatrixClasses.h"

// Default template Matrix
#pragma region
static inline float getNthPowerOfMinusOne(const unsigned int power)
{
	return ((power % 2) == 0) ? 1.0f : -1.0f;
};

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols>::Matrix()
	: elems()
{
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols>::Matrix(const float arg[rows][cols])
{
	for (unsigned int x = 0; x < rows; ++x)
	{
		std::memcpy(elems[x], arg[x], sizeof(float) * cols);
	}
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols>::Matrix(const Float2D<rows, cols>& arg)
{
	for (unsigned int x = 0; x < rows; ++x)
	{
		std::memcpy(elems[x], arg[x].data(), sizeof(float[cols]));
	}
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols>::Matrix(const float arg)
{
	for (unsigned int x = 0; x < rows; ++x)
	{
		std::fill_n(elems[x], cols, arg);
	}
}

template<unsigned int rows, unsigned int cols>
inline float & GLOWE::NoSSE::Matrix<rows, cols>::operator()(const unsigned short x, const unsigned short y)
{
	return elems[x][y];
}

template<unsigned int rows, unsigned int cols>
inline const float & GLOWE::NoSSE::Matrix<rows, cols>::operator()(const unsigned short x, const unsigned short y) const
{
	return elems[x][y];
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> & GLOWE::NoSSE::Matrix<rows, cols>::operator=(const Matrix & right)
{
	for (unsigned int x = 0; x < rows; ++x)
	{
		std::memcpy(elems[x], right.elems[x], sizeof(float) * cols);
	}

	return *this;
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> GLOWE::NoSSE::Matrix<rows, cols>::operator-() const
{
	Matrix<rows, cols> result(*this);

	for (unsigned int x = 0; x < rows; ++x)
	{
		for (unsigned int y = 0; y < cols; ++y)
		{
			result.elems[x][y] *= -1.0f;
		}
	}

	return std::move(result);
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> GLOWE::NoSSE::Matrix<rows, cols>::operator+(const float arg) const
{
	Matrix<rows, cols> result(*this);

	for (unsigned int x = 0; x < rows; ++x)
	{
		for (unsigned int y = 0; y < cols; ++y)
		{
			result.elems[x][y] += arg;
		}
	}

	return std::move(result);
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> GLOWE::NoSSE::Matrix<rows, cols>::operator-(const float arg) const
{
	Matrix<rows, cols> result(*this);

	for (unsigned int x = 0; x < rows; ++x)
	{
		for (unsigned int y = 0; y < cols; ++y)
		{
			result.elems[x][y] -= arg;
		}
	}

	return std::move(result);
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> GLOWE::NoSSE::Matrix<rows, cols>::operator*(const float arg) const
{
	Matrix<rows, cols> result(*this);

	for (unsigned int x = 0; x < rows; ++x)
	{
		for (unsigned int y = 0; y < cols; ++y)
		{
			result.elems[x][y] *= arg;
		}
	}

	return std::move(result);
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> GLOWE::NoSSE::Matrix<rows, cols>::operator/(const float arg) const
{
	Matrix<rows, cols> result(*this);

	for (unsigned int x = 0; x < rows; ++x)
	{
		for (unsigned int y = 0; y < cols; ++y)
		{
			result.elems[x][y] /= arg;
		}
	}

	return std::move(result);
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> GLOWE::NoSSE::Matrix<rows, cols>::operator+(const Matrix & right) const
{
	static_assert(rows == cols, "Cannot add matrices of different sizes.");

	Matrix<rows, cols> result(*this);

	for (unsigned int x = 0; x < rows; ++x)
	{
		for (unsigned int y = 0; y < cols; ++y)
		{
			result.elems[x][y] += right.elems[x][y];
		}
	}

	return std::move(result);
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> GLOWE::NoSSE::Matrix<rows, cols>::operator-(const Matrix & right) const
{
	static_assert(rows == cols, "Cannot subtract matrices of different sizes.");

	Matrix<rows, cols> result(*this);

	for (unsigned int x = 0; x < rows; ++x)
	{
		for (unsigned int y = 0; y < cols; ++y)
		{
			result.elems[x][y] -= right.elems[x][y];
		}
	}

	return std::move(result);
}

template<unsigned int rows, unsigned int cols>
template<unsigned int x>
inline GLOWE::NoSSE::Matrix<rows, x> GLOWE::NoSSE::Matrix<rows, cols>::operator*(const Matrix<cols, x> & right) const
{
	Matrix<rows, x> result;

	for (unsigned int i = 0; i < rows; ++i)
	{
		for (unsigned int j = 0; j < x; ++j)
		{
			float& currElem = result(i, j);
			for (unsigned int temp = 0; temp < cols; ++temp)
			{
				currElem += (*this)(i, temp) * right(temp, j);
			}
		}
	}

	return std::move(result);
}

template<unsigned int rows, unsigned int cols>
template<unsigned int x>
inline GLOWE::NoSSE::Matrix<rows, x> GLOWE::NoSSE::Matrix<rows, cols>::operator/(const Matrix<cols, x> & right) const
{
	return (*this) * right.inverse();
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::VectorF<cols> GLOWE::NoSSE::Matrix<rows, cols>::operator*(const VectorF<cols>& right) const
{
	Matrix<1, cols> temp;

	for (unsigned int x = 0; x < cols; ++x)
	{
		temp[x] = right[x];
	}

	temp = (*this) * right;

	VectorF<cols> result;

	for (unsigned int x = 0; x < cols; ++x)
	{
		result[x] = temp[x];
	}

	return result;
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> & GLOWE::NoSSE::Matrix<rows, cols>::operator+=(const float arg) const
{
	return *this = *this + arg;
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> & GLOWE::NoSSE::Matrix<rows, cols>::operator-=(const float arg) const
{
	return *this = *this - arg;
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> & GLOWE::NoSSE::Matrix<rows, cols>::operator*=(const float arg) const
{
	return *this = *this * arg;
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> & GLOWE::NoSSE::Matrix<rows, cols>::operator/=(const float arg) const
{
	return *this = *this / arg;
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> & GLOWE::NoSSE::Matrix<rows, cols>::operator+=(const Matrix & right) const
{
	return *this = *this + right;
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> & GLOWE::NoSSE::Matrix<rows, cols>::operator-=(const Matrix & right) const
{
	return *this = *this - right;
}

template<unsigned int rows, unsigned int cols>
template<unsigned int x, unsigned int y>
inline GLOWE::NoSSE::Matrix<rows, cols> & GLOWE::NoSSE::Matrix<rows, cols>::operator*=(const Matrix<x, y> & right) const
{
	return *this = ((*this) * right);
}

template<unsigned int rows, unsigned int cols>
template<unsigned int x, unsigned int y>
inline GLOWE::NoSSE::Matrix<rows, cols> & GLOWE::NoSSE::Matrix<rows, cols>::operator/=(const Matrix<x, y> & right) const
{
	return *this = ((*this) / right);
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> GLOWE::NoSSE::Matrix<rows, cols>::inverse() const
{
	const float invDet = 1.0f / determinant();
	return adjugateMatrix() * invDet;
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<cols, rows> GLOWE::NoSSE::Matrix<rows, cols>::transpose() const
{
	//TODO: Optimization
	Matrix<cols, rows> result;

	for (unsigned int x = 0; x < rows; ++x)
	{
		for (unsigned int y = 0; y < cols; ++y)
		{
			result(y, x) = elems[x][y];
		}
	}

	return result;
}

template<unsigned int rows, unsigned int cols>
inline float GLOWE::NoSSE::Matrix<rows, cols>::determinant() const
{
	static_assert(rows == cols, "Cannot compute determinant of non-square matrices.");

	float result = 0.0f;

	for (unsigned int x = 0; x < cols; ++x)
	{
		result += elems[0][x] * cofactor(0, x);
	}

	return result;
}

template<unsigned int rows, unsigned int cols>
inline float GLOWE::NoSSE::Matrix<rows, cols>::cofactor(const unsigned int row, const unsigned int column) const
{
	return getNthPowerOfMinusOne(row + column + 2) * minor(row, column);
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> GLOWE::NoSSE::Matrix<rows, cols>::cofactorMatrix() const
{
	Matrix<rows, cols> result;

	for (unsigned int x = 0; x < rows; ++x)
	{
		for (unsigned int y = 0; y < cols; ++y)
		{
			result(x, y) = cofactor(x, y);
		}
	}

	return std::move(result);
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::NoSSE::Matrix<rows, cols> GLOWE::NoSSE::Matrix<rows, cols>::adjugateMatrix() const
{
	return cofactorMatrix().transpose();
}

template<unsigned int rows, unsigned int cols>
inline float GLOWE::NoSSE::Matrix<rows, cols>::minor(const unsigned int rowToDelete, const unsigned int columnToDelete) const
{
	static_assert(rows == cols, "Cannot compute minor of non-square matrices.");

	Matrix<rows - 1, cols - 1> temp;

	unsigned int currRowPos = 0, currColPos = 0;

	for (unsigned int x = 0; x < rows; ++x)
	{
		if (x != rowToDelete)
		{
			for (unsigned int y = 0, currColPos = 0; y < cols; ++y)
			{
				if (y != columnToDelete)
				{
					temp(currRowPos, currColPos) = elems[x][y];
					++currColPos;
				}
			}
			++currRowPos;
		}
	}

	return temp.determinant();
}

template<unsigned int rows, unsigned int cols>
inline bool GLOWE::NoSSE::Matrix<rows, cols>::isInvertable() const
{
	return determinant() != 0.0f;
}

template<unsigned int rows, unsigned int cols>
template<class Func>
inline void GLOWE::NoSSE::Matrix<rows, cols>::applyFunction(Func&& func)
{
	for (unsigned int x = 0; x < rows; ++x)
	{
		for (unsigned int y = 0; y < cols; ++y)
		{
			elems[x][y] = func(elems[x][y]);
		}
	}
}

template<unsigned int rows, unsigned int cols>
inline const GLOWE::NoSSE::Matrix<rows, cols> & GLOWE::NoSSE::Matrix<rows, cols>::getIdentity()
{
	static_assert(rows == cols, "Cannot create identity matrix for non-square matrix.");
	static Matrix<rows, cols> identity;
	static bool wasCreated = false;

	if (!wasCreated)
	{
		for (unsigned int x = 0; x < rows; ++x)
		{
			identity(x, x) = 1.0f;
		}
	}

	return identity;
}
#pragma endregion

// Matrix<2, 2>
#pragma region
inline GLOWE::NoSSE::Matrix<2, 2>::Matrix()
	: elems()
{
}

inline GLOWE::NoSSE::Matrix<2, 2>::Matrix(const Float2x2& arg)
	: elems{ { arg[0][0], arg[0][1] } , { arg[1][0], arg[1][1] } }
{
}

inline GLOWE::NoSSE::Matrix<2, 2>::Matrix(const float arg[2][2])
	: elems{ { arg[0][0], arg[0][1] } ,{ arg[1][0], arg[1][1] } }
{
}

inline GLOWE::NoSSE::Matrix<2, 2>::Matrix(
	const float m00, const float m01,
	const float m10, const float m11
)
	: elems{
		{ m00, m01 },
		{ m10, m11 }
	}
{
}

inline GLOWE::NoSSE::Matrix<2, 2>::Matrix(const float arg)
	: elems{ arg, arg }
{
}

inline float& GLOWE::NoSSE::Matrix<2, 2>::operator()(const unsigned short x, const unsigned short y)
{
	return elems[x][y];
}

inline const float& GLOWE::NoSSE::Matrix<2, 2>::operator()(const unsigned short x, const unsigned short y) const
{
	return elems[x][y];
}

inline GLOWE::NoSSE::Matrix<2, 2>::operator GLOWE::Float2x2() const
{
	return Float2x2{ Float2{elems[0][0], elems[0][1]}, Float2{elems[1][0], elems[1][1]} };
}

inline GLOWE::NoSSE::Matrix<2, 2> GLOWE::NoSSE::Matrix<2, 2>::operator-() const
{
	return Matrix<2, 2>(-elems[0][0], -elems[0][1], -elems[1][0], -elems[1][1]);
}

inline GLOWE::NoSSE::Matrix<2, 2> GLOWE::NoSSE::Matrix<2, 2>::operator+(const Matrix<2, 2>& right) const
{
	return Matrix<2, 2>(elems[0][0] + right.elems[0][0],
							elems[0][1] + right.elems[0][1],
							elems[1][0] + right.elems[1][0],
							elems[1][1] + right.elems[1][1]);
}

inline GLOWE::NoSSE::Matrix<2, 2> GLOWE::NoSSE::Matrix<2, 2>::operator-(const Matrix<2, 2>& right) const
{
	return Matrix<2, 2>(elems[0][0] - right.elems[0][0],
		elems[0][1] - right.elems[0][1],
		elems[1][0] - right.elems[1][0],
		elems[1][1] - right.elems[1][1]);
}

inline GLOWE::NoSSE::Matrix<2, 2> GLOWE::NoSSE::Matrix<2, 2>::operator*(const Matrix<2, 2>& right) const
{
	return Matrix<2, 2>(Float2x2
	{
		Float2{ (elems[0][0] * right.elems[0][0]) + (elems[0][1] * right.elems[1][0]),
				(elems[0][0] * right.elems[0][1]) + (elems[0][1] * right.elems[1][1]) },
		Float2{ (elems[1][0] * right.elems[0][0]) + (elems[1][1] * right.elems[1][0]),
				(elems[1][0] * right.elems[0][1]) + (elems[1][1] * right.elems[1][1]) }
	});
}

inline GLOWE::NoSSE::Matrix<2, 2> GLOWE::NoSSE::Matrix<2, 2>::operator/(const Matrix<2, 2>& right) const
{
	return (*this * right.inverse());
}

template<unsigned int x>
inline GLOWE::NoSSE::Matrix<2, x> GLOWE::NoSSE::Matrix<2, 2>::operator*(const Matrix<2, x>& right) const
{
	Matrix<2, x> result;

	for (unsigned int i = 0; i < 2; ++i)
	{
		for (unsigned int j = 0; j < x; ++j)
		{
			float& currElem = result(i, j);
			for (unsigned int temp = 0; temp < 2; ++temp)
			{
				currElem += (*this)(i, temp) * right(temp, j);
			}
		}
	}

	return std::move(result);
}

template<unsigned int x>
inline GLOWE::NoSSE::Matrix<2, x> GLOWE::NoSSE::Matrix<2, 2>::operator/(const Matrix<2, x>& right) const
{
	return (*this * right.inverse());
}

inline GLOWE::VectorF<2> GLOWE::NoSSE::Matrix<2, 2>::operator*(const GLOWE::VectorF<2> & right) const
{
	return GLOWE::VectorF<2>(
		elems[0][0] * right[0] + elems[0][1] * right[1],
		elems[1][0] * right[0] + elems[1][1] * right[1]
	);
}

inline GLOWE::NoSSE::Matrix<2, 2> GLOWE::NoSSE::Matrix<2, 2>::operator+(const float right) const
{
	return Matrix<2, 2>(elems[0][0] + right,
		elems[0][1] + right,
		elems[1][0] + right,
		elems[1][1] + right);
}

inline GLOWE::NoSSE::Matrix<2, 2> GLOWE::NoSSE::Matrix<2, 2>::operator-(const float right) const
{
	return Matrix<2, 2>(elems[0][0] - right,
		elems[0][1] - right,
		elems[1][0] - right,
		elems[1][1] - right);
}

inline GLOWE::NoSSE::Matrix<2, 2> GLOWE::NoSSE::Matrix<2, 2>::operator*(const float right) const
{
	return Matrix<2, 2>(elems[0][0] * right,
		elems[0][1] * right,
		elems[1][0] * right,
		elems[1][1] * right);
}

inline GLOWE::NoSSE::Matrix<2, 2> GLOWE::NoSSE::Matrix<2, 2>::operator/(const float right) const
{
	return Matrix<2, 2>(elems[0][0] / right,
		elems[0][1] / right,
		elems[1][0] / right,
		elems[1][1] / right);
}

inline GLOWE::NoSSE::Matrix<2, 2>& GLOWE::NoSSE::Matrix<2, 2>::operator+=(const Matrix<2, 2>& right)
{
	return *this = *this + right;
}

inline GLOWE::NoSSE::Matrix<2, 2>& GLOWE::NoSSE::Matrix<2, 2>::operator-=(const Matrix<2, 2>& right)
{
	return *this = *this - right;
}

inline GLOWE::NoSSE::Matrix<2, 2>& GLOWE::NoSSE::Matrix<2, 2>::operator*=(const Matrix<2, 2>& right)
{
	return *this = *this * right;
}

inline GLOWE::NoSSE::Matrix<2, 2>& GLOWE::NoSSE::Matrix<2, 2>::operator/=(const Matrix<2, 2>& right)
{
	return *this = *this / right;
}

inline GLOWE::NoSSE::Matrix<2, 2>& GLOWE::NoSSE::Matrix<2, 2>::operator+=(const float right)
{
	return *this = *this + right;
}

inline GLOWE::NoSSE::Matrix<2, 2>& GLOWE::NoSSE::Matrix<2, 2>::operator-=(const float right)
{
	return *this = *this - right;
}

inline GLOWE::NoSSE::Matrix<2, 2>& GLOWE::NoSSE::Matrix<2, 2>::operator*=(const float right)
{
	return *this = *this * right;
}

inline GLOWE::NoSSE::Matrix<2, 2>& GLOWE::NoSSE::Matrix<2, 2>::operator/=(const float right)
{
	return *this = *this / right;
}

inline bool GLOWE::NoSSE::Matrix<2, 2>::operator==(const Matrix<2, 2>& right) const
{
	return elems[0][0] == right.elems[0][0]
		&& elems[0][1] == right.elems[0][1]
		&& elems[1][0] == right.elems[1][0]
		&& elems[1][1] == right.elems[1][1];
}

inline bool GLOWE::NoSSE::Matrix<2, 2>::operator!=(const Matrix<2, 2>& right) const
{
	return !(*this == right);
}

inline GLOWE::NoSSE::Matrix<2, 2> GLOWE::NoSSE::Matrix<2, 2>::transpose() const
{
	return Matrix<2, 2>(Float2x2
	{
		Float2{ elems[0][0], elems[1][0] },
		Float2{ elems[0][1], elems[1][1] }
	});
}

inline GLOWE::NoSSE::Matrix<2, 2> GLOWE::NoSSE::Matrix<2, 2>::inverse() const
{
	Matrix<2, 2> result(Float2x2
	{
		Float2{ elems[1][1], -elems[0][1] },
		Float2{ -elems[1][0], elems[0][0] }
	}
	);

	return result * (1.0f / determinant());
}

inline float GLOWE::NoSSE::Matrix<2, 2>::determinant() const
{
	return ((elems[0][0] * elems[1][1]) - (elems[0][1] * elems[1][0]));
}

inline float GLOWE::NoSSE::Matrix<2, 2>::cofactor(const unsigned int row, const unsigned int column) const
{
	return getNthPowerOfMinusOne(row + column + 2) * minor(row, column);
}

inline GLOWE::NoSSE::Matrix<2, 2> GLOWE::NoSSE::Matrix<2, 2>::cofactorMatrix() const
{
	return Matrix<2, 2>(Float2x2
	{
		Float2{ cofactor(0, 0), cofactor(0, 1) },
		Float2{ cofactor(1, 0), cofactor(1, 1) }
	}
	);
}

inline GLOWE::NoSSE::Matrix<2, 2> GLOWE::NoSSE::Matrix<2, 2>::adjugateMatrix() const
{
	return cofactorMatrix().transpose();
}

inline float GLOWE::NoSSE::Matrix<2, 2>::minor(const unsigned int rowToDelete, const unsigned int columnToDelete) const
{
	return elems[rowToDelete == 1 ? 0 : 1][columnToDelete == 1 ? 0 : 1];
}

inline bool GLOWE::NoSSE::Matrix<2, 2>::isInvertable() const
{
	return determinant() != 0.0f;
}

template<class Func>
inline void GLOWE::NoSSE::Matrix<2, 2>::applyFunction(Func&& func)
{
	for (unsigned int x = 0; x < 2; ++x)
	{
		for (unsigned int y = 0; y < 2; ++y)
		{
			elems[x][y] = func(elems[x][y]);
		}
	}
}

inline const GLOWE::NoSSE::Matrix<2, 2>& GLOWE::NoSSE::Matrix<2, 2>::getIdentity()
{
	static Matrix<2, 2> result(Float2x2
	{
		Float2{ 1.0f, 0.0f },
		Float2{ 0.0f, 1.0f }
	}
	);
	return result;
}
#pragma endregion

// Matrix<3, 3>
#pragma region
inline GLOWE::NoSSE::Matrix<3, 3>::Matrix()
	: elems()
{
}

inline GLOWE::NoSSE::Matrix<3, 3>::Matrix(const Float3x3& arg)
	: elems{
		{ arg[0][0], arg[0][1], arg[0][2] },
		{ arg[1][0], arg[1][1], arg[1][2] },
		{ arg[2][0], arg[2][1], arg[2][2] }
	}
{
}

inline GLOWE::NoSSE::Matrix<3, 3>::Matrix(const float arg[3][3])
	: elems{
		{ arg[0][0], arg[0][1], arg[0][2] },
		{ arg[1][0], arg[1][1], arg[1][2] },
		{ arg[2][0], arg[2][1], arg[2][2] }
	}
{
}

inline GLOWE::NoSSE::Matrix<3, 3>::Matrix(
	const float m00, const float m01, const float m02,
	const float m10, const float m11, const float m12,
	const float m20, const float m21, const float m22
)
	: elems{
		{ m00, m01, m02 },
		{ m10, m11, m12 },
		{ m20, m21, m22 }
	}
{
}

inline GLOWE::NoSSE::Matrix<3, 3>::Matrix(const float arg)
	: elems{ arg, arg, arg }
{
}

inline float& GLOWE::NoSSE::Matrix<3, 3>::operator()(const unsigned short x, const unsigned short y)
{
	return elems[x][y];
}

inline const float& GLOWE::NoSSE::Matrix<3, 3>::operator()(const unsigned short x, const unsigned short y) const
{
	return elems[x][y];
}

inline GLOWE::NoSSE::Matrix<3, 3>::operator GLOWE::Float3x3() const
{
	return Float3x3{
		Float3{ elems[0][0], elems[0][1], elems[0][2] },
		Float3{ elems[1][0], elems[1][1], elems[1][2] },
		Float3{ elems[2][0], elems[2][1], elems[2][2] }
	};
}

inline GLOWE::NoSSE::Matrix<3, 3> GLOWE::NoSSE::Matrix<3, 3>::operator-() const
{
	Matrix<3, 3> result(*this);
	for(unsigned int x = 0; x < 3; ++x)
	{
		for(unsigned int y = 0; y < 3; ++y)
		{
			result(x, y) = -elems[x][y];
		}
	}

	return std::move(result);
}

inline GLOWE::NoSSE::Matrix<3, 3> GLOWE::NoSSE::Matrix<3, 3>::operator+(const Matrix<3, 3>& right) const
{
	Matrix<3, 3> result(*this);
	for(unsigned int x = 0; x < 3; ++x)
	{
		for(unsigned int y = 0; y < 3; ++y)
		{
			result(x, y) += right(x, y);
		}
	}

	return std::move(result);
}

inline GLOWE::NoSSE::Matrix<3, 3> GLOWE::NoSSE::Matrix<3, 3>::operator-(const Matrix<3, 3>& right) const
{
	Matrix<3, 3> result(*this);
	for(unsigned int x = 0; x < 3; ++x)
	{
		for(unsigned int y = 0; y < 3; ++y)
		{
			result(x, y) -= right(x, y);
		}
	}

	return std::move(result);
}

inline GLOWE::NoSSE::Matrix<3, 3> GLOWE::NoSSE::Matrix<3, 3>::operator*(const Matrix<3, 3>& right) const
{
	Matrix<3, 3> result;
	for (unsigned int i = 0; i < 3; ++i)
	{
		for (unsigned int j = 0; j < 3; ++j)
		{
			result(i, j) = (*this)(i, 0) * right(0, j) + (*this)(i, 1) * right(1, j) + (*this)(i, 2) * right(2, j);
		}
	}

	return std::move(result);
}

inline GLOWE::NoSSE::Matrix<3, 3> GLOWE::NoSSE::Matrix<3, 3>::operator/(const Matrix<3, 3>& right) const
{
	return (*this * right.inverse());
}

template<unsigned int y>
inline GLOWE::NoSSE::Matrix<3, y> GLOWE::NoSSE::Matrix<3, 3>::operator*(const Matrix<3, y>& right) const
{
	Matrix<3, y> result;

	for (unsigned int i = 0; i < 3; ++i)
	{
		for (unsigned int j = 0; j < y; ++j)
		{
			result(i, j) = (*this)(i, 0) * right(0, j) + (*this)(i, 1) * right(1, j) + (*this)(i, 2) * right(2, j);
		}
	}

	return std::move(result);
}

template<unsigned int y>
inline GLOWE::NoSSE::Matrix<3, y> GLOWE::NoSSE::Matrix<3, 3>::operator/(const Matrix<3, y>& right) const
{
	return (*this * right.inverse());
}

inline GLOWE::VectorF<3> GLOWE::NoSSE::Matrix<3, 3>::operator*(const GLOWE::VectorF<3> & right) const
{
	return VectorF<3>(Float3{
		elems[0][0] * right[0] + elems[0][1] * right[1] + elems[0][2] * right[2],
		elems[1][0] * right[0] + elems[1][1] * right[1] + elems[1][2] * right[2],
		elems[2][0] * right[0] + elems[2][1] * right[1] + elems[2][2] * right[2]
	});
}

inline GLOWE::NoSSE::Matrix<3, 3> GLOWE::NoSSE::Matrix<3, 3>::operator+(const float right) const
{
	Matrix<3, 3> result(*this);
	for(unsigned int x = 0; x < 3; ++x)
	{
		for(unsigned int y = 0; y < 3; ++y)
		{
			result(x, y) += right;
		}
	}

	return std::move(result);
}

inline GLOWE::NoSSE::Matrix<3, 3> GLOWE::NoSSE::Matrix<3, 3>::operator-(const float right) const
{
	Matrix<3, 3> result(*this);
	for(unsigned int x = 0; x < 3; ++x)
	{
		for(unsigned int y = 0; y < 3; ++y)
		{
			result(x, y) -= right;
		}
	}

	return std::move(result);
}

inline GLOWE::NoSSE::Matrix<3, 3> GLOWE::NoSSE::Matrix<3, 3>::operator*(const float right) const
{
	Matrix<3, 3> result(*this);
	for(unsigned int x = 0; x < 3; ++x)
	{
		for(unsigned int y = 0; y < 3; ++y)
		{
			result(x, y) *= right;
		}
	}

	return std::move(result);
}

inline GLOWE::NoSSE::Matrix<3, 3> GLOWE::NoSSE::Matrix<3, 3>::operator/(const float right) const
{
	Matrix<3, 3> result(*this);
	for(unsigned int x = 0; x < 3; ++x)
	{
		for(unsigned int y = 0; y < 3; ++y)
		{
			result(x, y) /= right;
		}
	}

	return std::move(result);
}

inline GLOWE::NoSSE::Matrix<3, 3>& GLOWE::NoSSE::Matrix<3, 3>::operator+=(const Matrix<3, 3>& right)
{
	return (*this = *this + right);
}

inline GLOWE::NoSSE::Matrix<3, 3>& GLOWE::NoSSE::Matrix<3, 3>::operator-=(const Matrix<3, 3>& right)
{
	return (*this = *this - right);
}

inline GLOWE::NoSSE::Matrix<3, 3>& GLOWE::NoSSE::Matrix<3, 3>::operator*=(const Matrix<3, 3>& right)
{
	return (*this = *this * right);
}

inline GLOWE::NoSSE::Matrix<3, 3>& GLOWE::NoSSE::Matrix<3, 3>::operator/=(const Matrix<3, 3>& right)
{
	return (*this = *this / right);
}

inline GLOWE::NoSSE::Matrix<3, 3>& GLOWE::NoSSE::Matrix<3, 3>::operator+=(const float right)
{
	return (*this = *this + right);
}

inline GLOWE::NoSSE::Matrix<3, 3>& GLOWE::NoSSE::Matrix<3, 3>::operator-=(const float right)
{
	return (*this = *this - right);
}

inline GLOWE::NoSSE::Matrix<3, 3>& GLOWE::NoSSE::Matrix<3, 3>::operator*=(const float right)
{
	return (*this = *this * right);
}

inline GLOWE::NoSSE::Matrix<3, 3>& GLOWE::NoSSE::Matrix<3, 3>::operator/=(const float right)
{
	return (*this = *this / right);
}

inline GLOWE::NoSSE::Matrix<3, 3> GLOWE::NoSSE::Matrix<3, 3>::transpose() const
{
	return Float3x3{
		Float3{ elems[0][0], elems[1][0], elems[2][0] },
		Float3{ elems[0][1], elems[1][1], elems[2][1] },
		Float3{ elems[0][2], elems[1][2], elems[2][2] }
	};
}

inline GLOWE::NoSSE::Matrix<3, 3> GLOWE::NoSSE::Matrix<3, 3>::inverse() const
{
	return adjugateMatrix() * (1.0f/determinant());
}

inline float GLOWE::NoSSE::Matrix<3, 3>::cofactor(const unsigned int row, const unsigned int column) const
{
	return getNthPowerOfMinusOne(row + column + 2) * minor(row, column);
}

inline GLOWE::NoSSE::Matrix<3, 3> GLOWE::NoSSE::Matrix<3, 3>::cofactorMatrix() const
{
	Matrix<3, 3> result;

	for (unsigned int x = 0; x < 3; ++x)
	{
		for (unsigned int y = 0; y < 3; ++y)
		{
			result(x, y) = cofactor(x, y);
		}
	}

	return std::move(result);
}

inline GLOWE::NoSSE::Matrix<3, 3> GLOWE::NoSSE::Matrix<3, 3>::adjugateMatrix() const
{
	return cofactorMatrix().transpose();
}

inline float GLOWE::NoSSE::Matrix<3, 3>::determinant() const
{
	// Sarrus
	float sum1 = elems[0][0] * elems[1][1] * elems[2][2]
	+ elems[0][1] * elems[1][2] * elems[2][0]
	+ elems[0][2] * elems[1][0] * elems[2][1];
	float sum2 = elems[0][2] * elems[1][1] * elems[2][0]
	+ elems[0][0] * elems[1][2] * elems[2][1]
	+ elems[0][1] * elems[1][0] * elems[2][2];

	return sum1 - sum2;
}

inline float GLOWE::NoSSE::Matrix<3, 3>::minor(const unsigned int row, const unsigned int column) const
{
	Matrix<2, 2> temp;

	unsigned int currRowPos = 0, currColPos = 0;

	for (unsigned int x = 0; x < 3; ++x)
	{
		if (x != row)
		{
			for (unsigned int y = 0; y < 3; ++y)
			{
				if (y != column)
				{
					temp(currRowPos, currColPos) = elems[x][y];
					++currColPos;
				}
			}
			++currRowPos;
		}
	}

	return temp.determinant();
}

inline bool GLOWE::NoSSE::Matrix<3, 3>::isInvertable() const
{
	return determinant() != 0.0f;
}

template<class Func>
inline void GLOWE::NoSSE::Matrix<3, 3>::applyFunction(Func&& func)
{
	for (unsigned int x = 0; x < 3; ++x)
	{
		for (unsigned int y = 0; y < 3; ++y)
		{
			elems[x][y] = func(elems[x][y]);
		}
	}
}

inline const GLOWE::NoSSE::Matrix<3, 3>& GLOWE::NoSSE::Matrix<3, 3>::getIdentity()
{
	static Matrix<3, 3> result(Float3x3
	{
		Float3{ 1.0f, 0.0f, 0.0f },
		Float3{ 0.0f, 1.0f, 0.0f },
		Float3{ 0.0f, 0.0f, 1.0f }
	}
	);
	return result;
}

template<unsigned int rows, unsigned int cols>
inline GLOWE::VectorF<rows> GLOWE::NoSSE::operator*(const VectorF<rows>& left, const Matrix<rows, cols>& right)
{
	Matrix<1, rows> temp;

	for (unsigned int x = 0; x < rows; ++x)
	{
		temp(0, x) = left[x];
	}

	VectorF<rows> result;
	auto a = temp * right;

	for (unsigned int x = 0; x < rows; ++x)
	{
		result[x] = a(0, x);
	}

	return result;
}

#pragma endregion
#endif
