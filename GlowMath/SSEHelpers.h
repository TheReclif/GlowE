#pragma once
#ifndef GLOWE_MATH_SSEHELPERS_INCLUDED
#define GLOWE_MATH_SSEHELPERS_INCLUDED

#include "Math.h"

#if GLOWE_USED_SSE > GLOWE_NO_SSE
namespace GLOWE
{
	namespace SSE1
	{
		inline unsigned int signbit(const __m128 arg);

		inline __m128 setSignbit(const __m128 arg);
		inline __m128 unsetSignbit(const __m128 arg);
		inline __m128 flipSignbit(const __m128 arg);

		inline __m128 sqrt(const __m128 arg);

		inline __m128 sin(const __m128 arg);
		inline __m128 cos(const __m128 arg);
		inline __m128 tan(const __m128 arg);
		inline __m128 ctan(const __m128 arg);

		inline __m128 asin(const __m128 arg);
		inline __m128 acos(const __m128 arg);
		inline __m128 atan(const __m128 arg);
		inline __m128 actan(const __m128 arg);

		inline __m128 atan2(const __m128 y, const __m128 x);

		inline __m128 fabs(const __m128 arg);

		inline __m128 convertToRadians(const __m128 arg);
		inline __m128 convertToDegrees(const __m128 arg);
	}
}
#include "SSEHelpers.inl"

#endif
#endif
