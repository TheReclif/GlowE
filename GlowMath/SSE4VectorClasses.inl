#pragma once
#ifndef GLOWE_MATH_SSE4VECTORCLASSES_INL_INCLUDED
#define GLOWE_MATH_SSE4VECTORCLASSES_INL_INCLUDED

#include "SSE4VectorClasses.h"

#if GLOWE_USED_SSE >= GLOWE_SSE_4
inline GLOWE::SSE4::Vector2F::Vector2F(const SSE1::Vector2F& arg)
	: SSE1::Vector2F(arg.m)
{
}

inline GLOWE::SSE4::Vector2F::Vector2F(SSE1::Vector2F&& arg)
	: SSE1::Vector2F(arg.m)
{
	arg.m = _mm_setzero_ps();
}

inline float GLOWE::SSE4::Vector2F::getScalarProduct() const
{
	return _mm_cvtss_f32(_mm_dp_ps(m, m, 0x31));
}

inline float GLOWE::SSE4::Vector2F::scalarProduct(const SSE1::Vector2F & vec) const
{
	return _mm_cvtss_f32(_mm_dp_ps(m, vec.m, 0x31));
}

inline GLOWE::SSE4::Vector2F& GLOWE::SSE4::Vector2F::operator=(const SSE1::Vector2F& arg)
{
	m = arg.m;
	return *this;
}

inline GLOWE::SSE4::Vector2F& GLOWE::SSE4::Vector2F::operator=(SSE1::Vector2F&& arg) noexcept
{
	m = std::move(arg.m);
	return *this;
}

inline GLOWE::SSE4::Vector3F::Vector3F(const SSE1::Vector3F& arg)
	: SSE1::Vector3F(arg.m)
{
}

inline GLOWE::SSE4::Vector3F::Vector3F(SSE1::Vector3F&& arg)
	: SSE1::Vector3F(arg.m)
{
	arg.m = _mm_setzero_ps();
}

inline float GLOWE::SSE4::Vector3F::getScalarProduct() const
{
	return _mm_cvtss_f32(_mm_dp_ps(m, m, 0x71));
}

inline float GLOWE::SSE4::Vector3F::scalarProduct(const SSE1::Vector3F & vec) const
{
	return _mm_cvtss_f32(_mm_dp_ps(m, vec.m, 0x71));
}

inline GLOWE::SSE4::Vector3F& GLOWE::SSE4::Vector3F::operator=(const SSE1::Vector3F& arg)
{
	m = arg.m;
	return *this;
}

inline GLOWE::SSE4::Vector3F& GLOWE::SSE4::Vector3F::operator=(SSE1::Vector3F&& arg) noexcept
{
	m = std::move(arg.m);
	return *this;
}

inline GLOWE::SSE4::Vector4F::Vector4F(const SSE1::Vector4F& arg)
	: SSE1::Vector4F(arg.m)
{
}

inline GLOWE::SSE4::Vector4F::Vector4F(SSE1::Vector4F&& arg)
	: SSE1::Vector4F(arg.m)
{
	arg.m = _mm_setzero_ps();
}

inline float GLOWE::SSE4::Vector4F::getScalarProduct() const
{
	return _mm_cvtss_f32(_mm_dp_ps(m, m, 0xF1));
}

inline float GLOWE::SSE4::Vector4F::scalarProduct(const SSE1::Vector4F & vec) const
{
	return _mm_cvtss_f32(_mm_dp_ps(m, vec.m, 0xF1));
}

inline GLOWE::SSE4::Vector4F& GLOWE::SSE4::Vector4F::operator=(const SSE1::Vector4F& arg)
{
	m = arg.m;
	return *this;
}

inline GLOWE::SSE4::Vector4F& GLOWE::SSE4::Vector4F::operator=(SSE1::Vector4F&& arg) noexcept
{
	m = std::move(arg.m);
	return *this;
}
#endif

#endif
