#pragma once
#ifndef GLOWE_MATH_TRANSFORM_INCLUDED
#define GLOWE_MATH_TRANSFORM_INCLUDED

#include "../GlowSystem/SerializableDataCreator.h"

#include "MatrixClasses.h"
#include "VectorClasses.h"
#include "QuaternionClasses.h"
#include "RotationMatrix.h"

namespace GLOWE
{
	class GLOWMATH_EXPORT alignas(16) Transform
	{
	private:
		using Mat3x3 = const Matrix3x3&;
		using Mat4x4 = const Matrix4x4&;
		using Vec3 = const Vector3F&;
		using Vec4 = const Vector4F&;
		using Quat = const Quaternion&;
		using RotMat = const RotationMatrix&;
	private:
		Float3 translationVec, scaleVec;
		Float4 rotationQuaternion;
	public:
		Transform();
		Transform(Vec3 translation, Vec3 scaleArg, Quat rotation);
		Transform(const Float3& translation, const Float3& scaleArg, const Float4& rotation);
		~Transform() = default;

		void setScale(Vec3 newScale);
		void setScale(const Float3& newScale);

		void setTranslation(Vec3 newTranslation);
		void setTranslation(const Float3& newTranslation);

		void setRotation(Quat newRotation);
		void setRotation(RotMat newRotation);
		void setRotation(const Float3& angles); // Interprets floats as euler angles and converts them to apropiate Quaternion object before passing to rotate(Quat).
		void setRotation(const Float4& newRotation);

		// Setters
		void scale(Vec3 arg);
		void scale(const Float3& arg);

		void translate(Vec3 arg);
		void translate(const Float3& arg);

		void rotate(Quat arg);
		void rotate(RotMat arg); // Warning: Converts arg to Quaternion type, then passes it to rotate(Quat).
		void rotate(const Float3& arg); // Interprets floats as euler angles and converts them to apropiate Quaternion object before passing to rotate(Quat).
		void rotate(const Float4& arg);

		// Matrix multiplying convention: T * R * S
		Matrix4x4 calculateMatrix() const;

		void compose(const Float3& t, const Float3& s, const Float4& r);
		void compose(const Float3& t, const Float3& s, RotMat r);
		void compose(Vec3 t, Vec3 s, Quat r);
		void compose(Vec3 t, Vec3 s, RotMat r);

		void lookAt(const Float3 & at, const Float3 & up = VecHelpers::up, const Float3 & forward = VecHelpers::forward);

		// Getters
		Float3 getScale() const;
		Float3 getTranslation() const;
		Float4 getRotation() const;

		Float3 getForward() const; // Returns forward vector (normalized).
		Float3 getRight() const; // Returns right vector (normalized).
		Float3 getUp() const; // Returns up vector (normalized).

		static Matrix4x4 createScaleMatrix(const Float3& arg);
		static Matrix4x4 createScaleMatrix(Vec3 arg);

		static Matrix4x4 createTranslationMatrix(const Float3& arg);
		static Matrix4x4 createTranslationMatrix(Vec3 arg);
	};
}

#endif
