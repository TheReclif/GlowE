#pragma once
#ifndef GLOWE_MATH_CAMERAMATH_INCLUDED
#define GLOWE_MATH_CAMERAMATH_INCLUDED

#include "Math.h"

#if GLOWE_USED_SSE == GLOWE_NO_SSE
#include "NoSSECamera.h"
#elif GLOWE_USED_SSE >= GLOWE_SSE_1
#include "SSE1Camera.h"
#endif
namespace GLOWE
{
#if GLOWE_USED_SSE == GLOWE_NO_SSE
	using CameraMath = NoSSE::Camera;
#elif GLOWE_USED_SSE >= GLOWE_SSE_1 && GLOWE_USED_SSE <= GLOWE_SSE_4
	using CameraMath = SSE1::Camera;
#else
#error No SSE specified.
#endif
}

#endif
