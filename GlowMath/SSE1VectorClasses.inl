#pragma once
#ifndef GLOWE_MATH_SSE1VECTORCLASSES_INL_INCLUDED
#define GLOWE_MATH_SSE1VECTORCLASSES_INL_INCLUDED

#include "SSE1VectorClasses.h"

#if GLOWE_USED_SSE >= GLOWE_SSE_1
inline GLOWE::SSE1::VectorBase::VectorBase()
	: m(_mm_setzero_ps())
{
}

inline GLOWE::SSE1::VectorBase::VectorBase(const VectorBase & arg)
	: m(arg.m)
{
}

inline GLOWE::SSE1::VectorBase::VectorBase(VectorBase && arg) noexcept
	: m(std::move(arg.m))
{
}

inline GLOWE::SSE1::VectorBase::VectorBase(const __m128 arg)
	: m(arg)
{
}

inline GLOWE::SSE1::VectorBase & GLOWE::SSE1::VectorBase::operator=(const VectorBase & arg)
{
	m = arg.m;
	return (*this);
}

inline GLOWE::SSE1::VectorBase & GLOWE::SSE1::VectorBase::operator=(VectorBase && arg) noexcept
{
	m = std::move(arg.m);
	return (*this);
}

inline float GLOWE::SSE1::VectorBase::operator[](const unsigned int id) const
{
	union
	{
		__m128 m128;
		float f[4];
	};

	m128 = m;

	return f[id];
}

inline float& GLOWE::SSE1::VectorBase::operator[](const unsigned int id)
{
	return ((float*)(&m))[id];
}

inline GLOWE::SSE1::Vector2F::Vector2F() noexcept
	: VectorBase()
{
}

inline GLOWE::SSE1::Vector2F::Vector2F(const Vector2F& arg) noexcept
	: VectorBase(arg.m)
{
}

inline GLOWE::SSE1::Vector2F::Vector2F(Vector2F&& arg) noexcept
	: VectorBase(std::move(arg.m))
{
}

inline GLOWE::SSE1::Vector2F::Vector2F(const __m128 arg)
	: VectorBase(arg)
{
}

inline GLOWE::SSE1::Vector2F::Vector2F(const float a, const float b)
	: VectorBase(_mm_set_ps(0.0f, 0.0f, b, a))
{
}

inline GLOWE::SSE1::Vector2F::Vector2F(const float ab)
	: VectorBase(_mm_set_ps(0.0f, 0.0f, ab, ab))
{
}

inline GLOWE::SSE1::Vector2F::Vector2F(const float ab[2])
	: VectorBase(_mm_set_ps(0.0f, 0.0f, ab[1], ab[0]))
{
}

inline GLOWE::SSE1::Vector2F::Vector2F(const Float2 & ab)
	: VectorBase(_mm_set_ps(0.0f, 0.0f, ab[1], ab[0]))
{
}

inline GLOWE::SSE1::Vector2F::operator __m128() const
{
	return m;
}

inline GLOWE::Float2 GLOWE::SSE1::Vector2F::toFloat2() const
{
	return{ getX(), getY() };
}

inline GLOWE::SSE1::Vector2F GLOWE::SSE1::Vector2F::operator-() const
{
	return Vector2F(_mm_sub_ps(_mm_setzero_ps(), m));
}

inline GLOWE::SSE1::Vector2F GLOWE::SSE1::Vector2F::operator+(const Vector2F & right) const
{
	return Vector2F(_mm_add_ps(m, right.m));
}

inline GLOWE::SSE1::Vector2F GLOWE::SSE1::Vector2F::operator-(const Vector2F & right) const
{
	return Vector2F(_mm_sub_ps(m, right.m));
}

inline GLOWE::SSE1::Vector2F GLOWE::SSE1::Vector2F::operator*(const Vector2F & right) const
{
	return Vector2F(_mm_mul_ps(m, right.m));
}

inline GLOWE::SSE1::Vector2F GLOWE::SSE1::Vector2F::operator/(const Vector2F & right) const
{
	return Vector2F(_mm_div_ps(m, right.m));
}

inline GLOWE::SSE1::Vector2F GLOWE::SSE1::Vector2F::operator+(const float right) const
{
	return Vector2F(_mm_add_ps(m, _mm_set_ps1(right)));
}

inline GLOWE::SSE1::Vector2F GLOWE::SSE1::Vector2F::operator-(const float right) const
{
	return Vector2F(_mm_sub_ps(m, _mm_set_ps1(right)));
}

inline GLOWE::SSE1::Vector2F GLOWE::SSE1::Vector2F::operator*(const float right) const
{
	return Vector2F(_mm_mul_ps(m, _mm_set_ps1(right)));
}

inline GLOWE::SSE1::Vector2F GLOWE::SSE1::Vector2F::operator/(const float right) const
{
	return Vector2F(_mm_div_ps(m, _mm_set_ps1(right)));
}

inline GLOWE::SSE1::Vector2F & GLOWE::SSE1::Vector2F::operator+=(const Vector2F & right)
{
	return *this = *this + right;
}

inline GLOWE::SSE1::Vector2F & GLOWE::SSE1::Vector2F::operator-=(const Vector2F & right)
{
	return *this = *this - right;
}

inline GLOWE::SSE1::Vector2F & GLOWE::SSE1::Vector2F::operator*=(const Vector2F & right)
{
	return *this = *this * right;
}

inline GLOWE::SSE1::Vector2F & GLOWE::SSE1::Vector2F::operator/=(const Vector2F & right)
{
	return *this = *this / right;
}

inline GLOWE::SSE1::Vector2F & GLOWE::SSE1::Vector2F::operator+=(const float right)
{
	return *this = *this + right;
}

inline GLOWE::SSE1::Vector2F & GLOWE::SSE1::Vector2F::operator-=(const float right)
{
	return *this = *this - right;
}

inline GLOWE::SSE1::Vector2F & GLOWE::SSE1::Vector2F::operator*=(const float right)
{
	return *this = *this * right;
}

inline GLOWE::SSE1::Vector2F & GLOWE::SSE1::Vector2F::operator/=(const float right)
{
	return *this = *this / right;
}

inline float GLOWE::SSE1::Vector2F::getMagnitude() const
{
	return std::sqrt(getScalarProduct());
}

inline float GLOWE::SSE1::Vector2F::getMagnitudeSquared() const
{
	return getScalarProduct();
}

inline float GLOWE::SSE1::Vector2F::getScalarProduct() const
{
	__m128 r1, r2 = m;
	float result;

	r1 = _mm_mul_ps(m, r2);

	r2 = _mm_shuffle_ps(r2, r1, _MM_SHUFFLE(1, 0, 0, 0));
	r2 = _mm_add_ps(r2, r1);
	r1 = _mm_shuffle_ps(r1, r2, _MM_SHUFFLE(0, 3, 0, 0));
	r1 = _mm_add_ps(r1, r2);
	r2 = _mm_shuffle_ps(r1, r1, _MM_SHUFFLE(2, 2, 2, 2));

	_mm_store_ss(&result, r2);

	return result;
}

inline float GLOWE::SSE1::Vector2F::scalarProduct(const Vector2F & vec) const
{
	__m128 r1, r2 = vec.m;
	float result;

	r1 = _mm_mul_ps(m, r2);

	r2 = _mm_shuffle_ps(r2, r1, _MM_SHUFFLE(1, 0, 0, 0));
	r2 = _mm_add_ps(r2, r1);
	r1 = _mm_shuffle_ps(r1, r2, _MM_SHUFFLE(0, 3, 0, 0));
	r1 = _mm_add_ps(r1, r2);
	r2 = _mm_shuffle_ps(r1, r1, _MM_SHUFFLE(2, 2, 2, 2));

	_mm_store_ss(&result, r2);

	return result;
}

inline GLOWE::SSE1::Vector2F GLOWE::SSE1::Vector2F::convertToRadians() const
{
	return Vector2F(SSE1::convertToRadians(m));
}

inline GLOWE::SSE1::Vector2F GLOWE::SSE1::Vector2F::convertToDegrees() const
{
	return Vector2F(SSE1::convertToDegrees(m));
}

inline GLOWE::SSE1::Vector2F GLOWE::SSE1::Vector2F::normalize() const
{
	const float mag = getMagnitude();
	if (MathHelper::compareFloats(mag, 0.0f))
	{
		return GLOWE::SSE1::Vector2F(0.0f, 0.0f);
	}
	return *this / mag;
}

inline GLOWE::SSE1::Vector2F GLOWE::SSE1::Vector2F::normalizeFast() const
{
	return *this / getMagnitude();
}

inline GLOWE::SSE1::Vector2F GLOWE::SSE1::Vector2F::abs() const
{
	return Vector2F(SSE1::fabs(m));
}

inline GLOWE::SSE1::Vector2F GLOWE::SSE1::Vector2F::clamp(const Vector2F& min, const Vector2F& max) const
{
	return _mm_min_ps(max.m, _mm_max_ps(min.m, m)); // Beautiful. No branches whatsoever.
}

inline bool GLOWE::SSE1::Vector2F::checkIsNormalized() const
{
	return std::abs(getMagnitude() - 1.0f) <= MathHelper::Epsilon;
}

inline bool GLOWE::SSE1::Vector2F::operator==(const Vector2F & right) const
{
	return _mm_movemask_ps(_mm_cmpeq_ps(m, right.m)) == 0xF;
}

inline bool GLOWE::SSE1::Vector2F::operator<(const Vector2F & right) const
{
	return (getMagnitude() < right.getMagnitude());
}

inline bool GLOWE::SSE1::Vector2F::operator>(const Vector2F & right) const
{
	return getMagnitude() > right.getMagnitude();
}

inline float GLOWE::SSE1::Vector2F::getX() const
{
	return _mm_cvtss_f32(m);
}

inline float GLOWE::SSE1::Vector2F::getY() const
{
	__m128 temp = _mm_shuffle_ps(m, m, _MM_SHUFFLE(1, 1, 1, 1));
	return _mm_cvtss_f32(temp);
}

inline void GLOWE::SSE1::Vector2F::setX(const float arg)
{
	__m128 temp = _mm_set_ss(arg);
	m = _mm_move_ss(m, temp);
}

inline void GLOWE::SSE1::Vector2F::setY(const float arg)
{
	__m128 temp = _mm_set_ss(arg);
	temp = _mm_unpacklo_ps(m, temp);
	m = _mm_shuffle_ps(temp, m, _MM_SHUFFLE(3, 3, 1, 0));
}

inline GLOWE::SSE1::Vector3F::Vector3F() noexcept
	: VectorBase()
{
}

inline GLOWE::SSE1::Vector3F::Vector3F(const Vector3F& arg) noexcept
	: VectorBase(arg.m)
{
}

inline GLOWE::SSE1::Vector3F::Vector3F(Vector3F&& arg) noexcept
	: VectorBase(std::move(arg.m))
{
}

inline GLOWE::SSE1::Vector3F::Vector3F(const __m128 arg)
	: VectorBase(arg)
{
}

inline GLOWE::SSE1::Vector3F::Vector3F(const float a, const float b, const float c)
	: VectorBase(_mm_set_ps(0.0f, c, b, a))
{
}

inline GLOWE::SSE1::Vector3F::Vector3F(const float abc)
	: VectorBase(_mm_set_ps(0.0f, abc, abc, abc))
{
}

inline GLOWE::SSE1::Vector3F::Vector3F(const float abc[3])
	: VectorBase(_mm_set_ps(0.0f, abc[2], abc[1], abc[0]))
{
}

inline GLOWE::SSE1::Vector3F::Vector3F(const Float3 & abc)
	: VectorBase(_mm_set_ps(0.0f, abc[2], abc[1], abc[0]))
{
}

inline GLOWE::SSE1::Vector3F::operator __m128() const
{
	return m;
}

inline GLOWE::Float3 GLOWE::SSE1::Vector3F::toFloat3() const
{
	return{ getX(), getY(), getZ() };
}

inline GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::operator-() const
{
	return Vector3F(_mm_sub_ps(_mm_setzero_ps(), m));
}

inline GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::operator+(const Vector3F & right) const
{
	return Vector3F(_mm_add_ps(m, right.m));
}

inline GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::operator-(const Vector3F & right) const
{
	return Vector3F(_mm_sub_ps(m, right.m));
}

inline GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::operator*(const Vector3F & right) const
{
	return Vector3F(_mm_mul_ps(m, right.m));
}

inline GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::operator/(const Vector3F & right) const
{
	return Vector3F(_mm_div_ps(m, right.m));
}

inline GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::operator+(const float right) const
{
	return Vector3F(_mm_add_ps(m, _mm_set_ps1(right)));
}

inline GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::operator-(const float right) const
{
	return Vector3F(_mm_sub_ps(m, _mm_set_ps1(right)));
}

inline GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::operator*(const float right) const
{
	return Vector3F(_mm_mul_ps(m, _mm_set_ps1(right)));
}

inline GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::operator/(const float right) const
{
	return Vector3F(_mm_div_ps(m, _mm_set_ps1(right)));
}

inline GLOWE::SSE1::Vector3F & GLOWE::SSE1::Vector3F::operator+=(const Vector3F & right)
{
	return *this = *this + right;
}

inline GLOWE::SSE1::Vector3F & GLOWE::SSE1::Vector3F::operator-=(const Vector3F & right)
{
	return *this = *this - right;
}

inline GLOWE::SSE1::Vector3F & GLOWE::SSE1::Vector3F::operator*=(const Vector3F & right)
{
	return *this = *this * right;
}

inline GLOWE::SSE1::Vector3F & GLOWE::SSE1::Vector3F::operator/=(const Vector3F & right)
{
	return *this = *this / right;
}

inline GLOWE::SSE1::Vector3F & GLOWE::SSE1::Vector3F::operator+=(const float right)
{
	return *this = *this + right;
}

inline GLOWE::SSE1::Vector3F & GLOWE::SSE1::Vector3F::operator-=(const float right)
{
	return *this = *this - right;
}

inline GLOWE::SSE1::Vector3F & GLOWE::SSE1::Vector3F::operator*=(const float right)
{
	return *this = *this * right;
}

inline GLOWE::SSE1::Vector3F & GLOWE::SSE1::Vector3F::operator/=(const float right)
{
	return *this = *this / right;
}

inline float GLOWE::SSE1::Vector3F::getMagnitude() const
{
	return std::sqrt(getScalarProduct());
}

inline float GLOWE::SSE1::Vector3F::getMagnitudeSquared() const
{
	return getScalarProduct();
}

inline float GLOWE::SSE1::Vector3F::getScalarProduct() const
{
	__m128 r1, r2 = m;
	float result;

	r1 = _mm_mul_ps(m, r2);

	r2 = _mm_shuffle_ps(r2, r1, _MM_SHUFFLE(1, 0, 0, 0));
	r2 = _mm_add_ps(r2, r1);
	r1 = _mm_shuffle_ps(r1, r2, _MM_SHUFFLE(0, 3, 0, 0));
	r1 = _mm_add_ps(r1, r2);
	r2 = _mm_shuffle_ps(r1, r1, _MM_SHUFFLE(2, 2, 2, 2));

	_mm_store_ss(&result, r2);

	return result;
}

inline float GLOWE::SSE1::Vector3F::scalarProduct(const Vector3F & vec) const
{
	__m128 r1, r2 = vec.m;
	float result;

	r1 = _mm_mul_ps(m, r2);

	r2 = _mm_shuffle_ps(r2, r1, _MM_SHUFFLE(1, 0, 0, 0));
	r2 = _mm_add_ps(r2, r1);
	r1 = _mm_shuffle_ps(r1, r2, _MM_SHUFFLE(0, 3, 0, 0));
	r1 = _mm_add_ps(r1, r2);
	r2 = _mm_shuffle_ps(r1, r1, _MM_SHUFFLE(2, 2, 2, 2));

	_mm_store_ss(&result, r2);

	return result;
}

GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::vectorProduct(const Vector3F & vec) const
{
	__m128 a_yzx = _mm_shuffle_ps(m, m, _MM_SHUFFLE(3, 0, 2, 1));
	__m128 b_yzx = _mm_shuffle_ps(vec.m, vec.m, _MM_SHUFFLE(3, 0, 2, 1));
	__m128 c = _mm_sub_ps(_mm_mul_ps(m, b_yzx), _mm_mul_ps(a_yzx, vec.m));
	return Vector3F(_mm_shuffle_ps(c, c, _MM_SHUFFLE(3, 0, 2, 1)));
}

GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::convertToRadians() const
{
	return Vector3F(SSE1::convertToRadians(m));
}

GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::convertToDegrees() const
{
	return Vector3F(SSE1::convertToDegrees(m));
}

GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::normalize() const
{
	const float mag = getMagnitude();
	if (MathHelper::compareFloats(mag, 0.0f))
	{
		return GLOWE::SSE1::Vector3F(0.0f, 0.0f, 0.0f);
	}
	return *this / mag;
}

GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::normalizeFast() const
{
	return *this / getMagnitude();
}

inline GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::abs() const
{
	return Vector3F(SSE1::fabs(m));
}

inline GLOWE::SSE1::Vector3F GLOWE::SSE1::Vector3F::clamp(const Vector3F& min, const Vector3F& max) const
{
	return _mm_min_ps(max.m, _mm_max_ps(min.m, m));
}

inline bool GLOWE::SSE1::Vector3F::checkIsNormalized() const
{
	return std::abs(getMagnitude() - 1.0f) <= MathHelper::Epsilon;
}

inline bool GLOWE::SSE1::Vector3F::operator==(const Vector3F & right) const
{
	return _mm_movemask_ps(_mm_cmpeq_ps(m, right.m)) == 0xF;
}

inline bool GLOWE::SSE1::Vector3F::operator<(const Vector3F & right) const
{
	return getMagnitude() < right.getMagnitude();
}

inline bool GLOWE::SSE1::Vector3F::operator>(const Vector3F & right) const
{
	return getMagnitude() > right.getMagnitude();
}

inline float GLOWE::SSE1::Vector3F::getX() const
{
	return _mm_cvtss_f32(m);
}

inline float GLOWE::SSE1::Vector3F::getY() const
{
	__m128 temp = _mm_shuffle_ps(m, m, _MM_SHUFFLE(1, 1, 1, 1));
	return _mm_cvtss_f32(temp);
}

inline float GLOWE::SSE1::Vector3F::getZ() const
{
	__m128 temp = _mm_shuffle_ps(m, m, _MM_SHUFFLE(2, 2, 2, 2));
	return _mm_cvtss_f32(temp);
}

inline void GLOWE::SSE1::Vector3F::setX(const float arg)
{
	__m128 temp = _mm_set_ss(arg);
	m = _mm_move_ss(m, temp);
}

inline void GLOWE::SSE1::Vector3F::setY(const float arg)
{
	// arg 0 0 0
	__m128 temp = _mm_set_ss(arg);
	
	// x arg y 0
	__m128 zuza = _mm_unpacklo_ps(m, temp);

	m = _mm_shuffle_ps(zuza, m, _MM_SHUFFLE(3, 2, 1, 0));
}

inline void GLOWE::SSE1::Vector3F::setZ(const float arg)
{
	// arg 0 0 0
	__m128 temp = _mm_set_ss(arg);

	m = _mm_movelh_ps(m, temp);
}

inline GLOWE::SSE1::Vector4F::Vector4F() noexcept
	: VectorBase()
{
}

inline GLOWE::SSE1::Vector4F::Vector4F(const Vector4F& arg) noexcept
	: VectorBase(arg.m)
{
}

inline GLOWE::SSE1::Vector4F::Vector4F(Vector4F&& arg) noexcept
	: VectorBase(std::move(arg.m))
{
}

inline GLOWE::SSE1::Vector4F::Vector4F(const __m128 arg)
	: VectorBase(arg)
{
}

inline GLOWE::SSE1::Vector4F::Vector4F(const float a, const float b, const float c, const float d)
	: VectorBase(_mm_set_ps(d, c, b, a))
{
}

inline GLOWE::SSE1::Vector4F::Vector4F(const float abcd)
	: VectorBase(_mm_set_ps1(abcd))
{
}

inline GLOWE::SSE1::Vector4F::Vector4F(const float abcd[4])
	: VectorBase(_mm_set_ps(abcd[3], abcd[2], abcd[1], abcd[0]))
{
}

inline GLOWE::SSE1::Vector4F::Vector4F(const Float4 & abcd)
	: VectorBase(_mm_set_ps(abcd[3], abcd[2], abcd[1], abcd[0]))
{
}

inline GLOWE::SSE1::Vector4F::operator __m128() const
{
	return m;
}

inline GLOWE::Float4 GLOWE::SSE1::Vector4F::toFloat4() const
{
	return{ getX(), getY(), getZ(), getW() };
}

inline GLOWE::SSE1::Vector4F GLOWE::SSE1::Vector4F::operator-() const
{
	return Vector4F(_mm_sub_ps(_mm_setzero_ps(), m));
}

inline GLOWE::SSE1::Vector4F GLOWE::SSE1::Vector4F::operator+(const Vector4F & right) const
{
	return Vector4F(_mm_add_ps(m, right.m));
}

inline GLOWE::SSE1::Vector4F GLOWE::SSE1::Vector4F::operator-(const Vector4F & right) const
{
	return Vector4F(_mm_sub_ps(m, right.m));
}

inline GLOWE::SSE1::Vector4F GLOWE::SSE1::Vector4F::operator*(const Vector4F & right) const
{
	return Vector4F(_mm_mul_ps(m, right.m));
}

inline GLOWE::SSE1::Vector4F GLOWE::SSE1::Vector4F::operator/(const Vector4F & right) const
{
	return Vector4F(_mm_div_ps(m, right.m));
}

inline GLOWE::SSE1::Vector4F GLOWE::SSE1::Vector4F::operator+(const float right) const
{
	return Vector4F(_mm_add_ps(m, _mm_set_ps1(right)));
}

inline GLOWE::SSE1::Vector4F GLOWE::SSE1::Vector4F::operator-(const float right) const
{
	return Vector4F(_mm_sub_ps(m, _mm_set_ps1(right)));
}

inline GLOWE::SSE1::Vector4F GLOWE::SSE1::Vector4F::operator*(const float right) const
{
	return Vector4F(_mm_mul_ps(m, _mm_set_ps1(right)));
}

inline GLOWE::SSE1::Vector4F GLOWE::SSE1::Vector4F::operator/(const float right) const
{
	return Vector4F(_mm_div_ps(m, _mm_set_ps1(right)));
}

inline GLOWE::SSE1::Vector4F & GLOWE::SSE1::Vector4F::operator+=(const Vector4F & right)
{
	return *this = *this + right;
}

inline GLOWE::SSE1::Vector4F & GLOWE::SSE1::Vector4F::operator-=(const Vector4F & right)
{
	return *this = *this - right;
}

inline GLOWE::SSE1::Vector4F & GLOWE::SSE1::Vector4F::operator*=(const Vector4F & right)
{
	return *this = *this * right;
}

inline GLOWE::SSE1::Vector4F & GLOWE::SSE1::Vector4F::operator/=(const Vector4F & right)
{
	return *this = *this / right;
}

inline GLOWE::SSE1::Vector4F & GLOWE::SSE1::Vector4F::operator+=(const float right)
{
	return *this = *this + right;
}

inline GLOWE::SSE1::Vector4F & GLOWE::SSE1::Vector4F::operator-=(const float right)
{
	return *this = *this - right;
}

inline GLOWE::SSE1::Vector4F & GLOWE::SSE1::Vector4F::operator*=(const float right)
{
	return *this = *this * right;
}

inline GLOWE::SSE1::Vector4F & GLOWE::SSE1::Vector4F::operator/=(const float right)
{
	return *this = *this / right;
}

inline float GLOWE::SSE1::Vector4F::getMagnitude() const
{
	return std::sqrt(getScalarProduct());
}

inline float GLOWE::SSE1::Vector4F::getMagnitudeSquared() const
{
	return getScalarProduct();
}

inline float GLOWE::SSE1::Vector4F::getScalarProduct() const
{
	__m128 r1, r2 = m;
	float result;

	r1 = _mm_mul_ps(m, r2);

	r2 = _mm_shuffle_ps(r2, r1, _MM_SHUFFLE(1, 0, 0, 0));
	r2 = _mm_add_ps(r2, r1);
	r1 = _mm_shuffle_ps(r1, r2, _MM_SHUFFLE(0, 3, 0, 0));
	r1 = _mm_add_ps(r1, r2);
	r2 = _mm_shuffle_ps(r1, r1, _MM_SHUFFLE(2, 2, 2, 2));

	_mm_store_ss(&result, r2);

	return result;
}

inline float GLOWE::SSE1::Vector4F::scalarProduct(const Vector4F & vec) const
{
	__m128 r1, r2 = vec.m;
	float result;

	r1 = _mm_mul_ps(m, r2);

	r2 = _mm_shuffle_ps(r2, r1, _MM_SHUFFLE(1, 0, 0, 0));
	r2 = _mm_add_ps(r2, r1);
	r1 = _mm_shuffle_ps(r1, r2, _MM_SHUFFLE(0, 3, 0, 0));
	r1 = _mm_add_ps(r1, r2);
	r2 = _mm_shuffle_ps(r1, r1, _MM_SHUFFLE(2, 2, 2, 2));

	_mm_store_ss(&result, r2);

	return result;
}

inline GLOWE::SSE1::Vector4F GLOWE::SSE1::Vector4F::convertToRadians() const
{
	return Vector4F(SSE1::convertToRadians(m));
}

inline GLOWE::SSE1::Vector4F GLOWE::SSE1::Vector4F::convertToDegrees() const
{
	return Vector4F(SSE1::convertToDegrees(m));
}

inline GLOWE::SSE1::Vector4F GLOWE::SSE1::Vector4F::normalize() const
{
	const float mag = getMagnitude();
	if (MathHelper::compareFloats(mag, 0.0f))
	{
		return GLOWE::SSE1::Vector4F(0.0f, 0.0f, 0.0f, 0.0f);
	}
	return *this / mag;
}

inline GLOWE::SSE1::Vector4F GLOWE::SSE1::Vector4F::normalizeFast() const
{
	return *this / getMagnitude();
}

inline GLOWE::SSE1::Vector4F GLOWE::SSE1::Vector4F::abs() const
{
	return Vector4F(SSE1::fabs(m));
}

inline GLOWE::SSE1::Vector4F GLOWE::SSE1::Vector4F::clamp(const Vector4F& min, const Vector4F& max) const
{
	return _mm_min_ps(max.m, _mm_max_ps(min.m, m));
}

inline bool GLOWE::SSE1::Vector4F::checkIsNormalized() const
{
	return std::abs(getMagnitude() - 1.0f) <= MathHelper::Epsilon;
}

inline bool GLOWE::SSE1::Vector4F::operator==(const Vector4F & right) const
{
	return _mm_movemask_ps(_mm_cmpeq_ps(m, right.m)) == 0xF;
}

inline bool GLOWE::SSE1::Vector4F::operator<(const Vector4F & right) const
{
	return getMagnitude()<right.getMagnitude();
}

inline bool GLOWE::SSE1::Vector4F::operator>(const Vector4F & right) const
{
	return getMagnitude()>right.getMagnitude();
}

inline float GLOWE::SSE1::Vector4F::getX() const
{
	return _mm_cvtss_f32(m);
}

inline float GLOWE::SSE1::Vector4F::getY() const
{
	__m128 temp = _mm_shuffle_ps(m, m, _MM_SHUFFLE(1, 1, 1, 1));
	return _mm_cvtss_f32(temp);
}

inline float GLOWE::SSE1::Vector4F::getZ() const
{
	__m128 temp = _mm_shuffle_ps(m, m, _MM_SHUFFLE(2, 2, 2, 2));
	return _mm_cvtss_f32(temp);
}

inline float GLOWE::SSE1::Vector4F::getW() const
{
	__m128 temp = _mm_shuffle_ps(m, m, _MM_SHUFFLE(3, 3, 3, 3));
	return _mm_cvtss_f32(temp);
}

inline void GLOWE::SSE1::Vector4F::setX(const float arg)
{
	__m128 temp = _mm_set_ss(arg);
	m = _mm_move_ss(m, temp);
}

inline void GLOWE::SSE1::Vector4F::setY(const float arg)
{
	// arg 0 0 0
	__m128 temp = _mm_set_ss(arg);

	// x arg y 0
	__m128 zuza = _mm_unpacklo_ps(m, temp);

	m = _mm_shuffle_ps(zuza, m, _MM_SHUFFLE(3, 2, 1, 0));
}

inline void GLOWE::SSE1::Vector4F::setZ(const float arg)
{
	// arg 0 0 0
	__m128 temp = _mm_set_ss(arg);

	// arg arg w w
	__m128 zuza = _mm_shuffle_ps(temp, m, _MM_SHUFFLE(3, 3, 0, 0));

	m = _mm_shuffle_ps(m, zuza, _MM_SHUFFLE(3, 1, 1, 0));
}

inline void GLOWE::SSE1::Vector4F::setW(const float arg)
{
	// arg 0 0 0
	__m128 temp = _mm_set_ss(arg);

	// arg arg z z
	__m128 zuza = _mm_shuffle_ps(temp, m, _MM_SHUFFLE(2, 2, 0, 0));

	m = _mm_shuffle_ps(m, zuza, _MM_SHUFFLE(1, 3, 1, 0));
}

#endif

#endif
