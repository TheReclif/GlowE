#include "Transform.h"

GLOWE::Transform::Transform()
	: translationVec(), scaleVec{ 1.0f, 1.0f, 1.0f }, rotationQuaternion{ 1.0f, 0.0f, 0.0f, 0.0f }
{
}

GLOWE::Transform::Transform(Vec3 translation, Vec3 scaleArg, Quat rotation)
	: translationVec(translation.toFloat3()), scaleVec(scaleArg.toFloat3()), rotationQuaternion(rotation.normalize().toFloat4())
{
}

GLOWE::Transform::Transform(const Float3& translation, const Float3& scaleArg, const Float4& rotation)
	: translationVec(translation), scaleVec(scaleArg), rotationQuaternion(Quaternion(rotation).normalize().toFloat4())
{
}

void GLOWE::Transform::setScale(Vec3 newScale)
{
	scaleVec = newScale.toFloat3();
}

void GLOWE::Transform::setScale(const Float3 & newScale)
{
	scaleVec = newScale;
}

void GLOWE::Transform::setTranslation(Vec3 newTranslation)
{
	translationVec = newTranslation.toFloat3();
}

void GLOWE::Transform::setTranslation(const Float3 & newTranslation)
{
	translationVec = newTranslation;
}

void GLOWE::Transform::setRotation(Quat newRotation)
{
	rotationQuaternion = newRotation.normalize().toFloat4();
}

void GLOWE::Transform::setRotation(RotMat newRotation)
{
	rotationQuaternion = newRotation.toQuaternion().normalize().toFloat4();
}

void GLOWE::Transform::setRotation(const Float3 & angles)
{
	rotationQuaternion = Quaternion::fromEulerAngles(angles).normalize().toFloat4();
}

void GLOWE::Transform::setRotation(const Float4 & newRotation)
{
	rotationQuaternion = Quaternion(newRotation).normalize().toFloat4();
}

void GLOWE::Transform::scale(Vec3 arg)
{
	scaleVec = (Vector3F(scaleVec) * arg).toFloat3();
}

void GLOWE::Transform::scale(const Float3 & arg)
{
	scaleVec = (Vector3F(scaleVec) * Vector3F(arg)).toFloat3();
}

void GLOWE::Transform::translate(Vec3 arg)
{
	translationVec = (Vector3F(translationVec) + arg).toFloat3();
}

void GLOWE::Transform::translate(const Float3 & arg)
{
	translationVec = (Vector3F(translationVec) + Vector3F(arg)).toFloat3();
}

void GLOWE::Transform::rotate(Quat arg)
{
	rotationQuaternion = (arg * Quaternion(rotationQuaternion).normalize()).toFloat4();
}

void GLOWE::Transform::rotate(RotMat arg)
{
	Quaternion temp = arg.toQuaternion().normalize();
	rotate(temp);
}

void GLOWE::Transform::rotate(const Float3 & arg)
{
	Quaternion temp = Quaternion::fromEulerAngles(arg).normalize();
	rotate(temp);
}

void GLOWE::Transform::rotate(const Float4 & arg)
{
	rotate(Quaternion(arg).normalize());
}

GLOWE::Matrix4x4 GLOWE::Transform::calculateMatrix() const
{
	return createTranslationMatrix(translationVec) * (Matrix4x4)RotationMatrix(Quaternion(rotationQuaternion).asRotationMatrix()) * createScaleMatrix(scaleVec);
}

void GLOWE::Transform::compose(const Float3& t, const Float3& s, const Float4& r)
{
	translationVec = t;
	scaleVec = s;
	rotationQuaternion = r;
}

void GLOWE::Transform::compose(const Float3& t, const Float3& s, RotMat r)
{
	translationVec = t;
	scaleVec = s;
	rotationQuaternion = r.toQuaternion().normalize().toFloat4();
}

void GLOWE::Transform::compose(Vec3 t, Vec3 s, Quat r)
{
	translationVec = t.toFloat3();
	scaleVec = s.toFloat3();
	rotationQuaternion = r.normalize().toFloat4();
}

void GLOWE::Transform::compose(Vec3 t, Vec3 s, RotMat r)
{
	translationVec = t.toFloat3();
	scaleVec = s.toFloat3();
	rotationQuaternion = r.toQuaternion().normalize().toFloat4();
}

void GLOWE::Transform::lookAt(const Float3& at, const Float3& up, const Float3& forward)
{
	rotationQuaternion = Quaternion::lookAt((Vector3F(at) - Vector3F(translationVec)), up, forward).normalize().toFloat4();
}

GLOWE::Float3 GLOWE::Transform::getScale() const
{
	return scaleVec;
}

GLOWE::Float3 GLOWE::Transform::getTranslation() const
{
	return translationVec;
}

GLOWE::Float3 GLOWE::Transform::getForward() const
{
	Vector3F vec(VecHelpers::forward);
	Quaternion qt(rotationQuaternion);

	return (qt.asRotationMatrix() * vec).toFloat3();
}

GLOWE::Float3 GLOWE::Transform::getRight() const
{
	Vector3F vec(VecHelpers::right);
	Quaternion qt(rotationQuaternion);

	return (qt.asRotationMatrix() * vec).toFloat3();
}

GLOWE::Float3 GLOWE::Transform::getUp() const
{
	Vector3F vec(VecHelpers::up);
	Quaternion qt(rotationQuaternion);

	return (qt.asRotationMatrix() * vec).toFloat3();
}

GLOWE::Float4 GLOWE::Transform::getRotation() const
{
	return rotationQuaternion;
}

GLOWE::Matrix4x4 GLOWE::Transform::createScaleMatrix(const Float3 & arg)
{
	return Matrix4x4(Float4x4
	{
		Float4{ arg[0], 0.0f, 0.0f, 0.0f },
		Float4{ 0.0f, arg[1], 0.0f, 0.0f },
		Float4{ 0.0f, 0.0f, arg[2], 0.0f },
		Float4{ 0.0f, 0.0f, 0.0f, 1.0f }
	} );
}

GLOWE::Matrix4x4 GLOWE::Transform::createScaleMatrix(Vec3 arg)
{
	return Matrix4x4(Float4x4
	{
		Float4{ arg.getX(), 0.0f, 0.0f, 0.0f },
		Float4{ 0.0f, arg.getY(), 0.0f, 0.0f },
		Float4{ 0.0f, 0.0f, arg.getZ(), 0.0f },
		Float4{ 0.0f, 0.0f, 0.0f, 1.0f }
	});
}

GLOWE::Matrix4x4 GLOWE::Transform::createTranslationMatrix(const Float3 & arg)
{
	return Matrix4x4(Float4x4
	{
		Float4{ 1.0f, 0.0f, 0.0f, arg[0] },
		Float4{ 0.0f, 1.0f, 0.0f, arg[1] },
		Float4{ 0.0f, 0.0f, 1.0f, arg[2] },
		Float4{ 0.0f, 0.0f, 0.0f, 1.0f }
	} );
}

GLOWE::Matrix4x4 GLOWE::Transform::createTranslationMatrix(Vec3 arg)
{
	return Matrix4x4(Float4x4
	{
		Float4{ 1.0f, 0.0f, 0.0f, arg.getX() },
		Float4{ 0.0f, 1.0f, 0.0f, arg.getY() },
		Float4{ 0.0f, 0.0f, 1.0f, arg.getZ() },
		Float4{ 0.0f, 0.0f, 0.0f, 1.0f }
	});
}

