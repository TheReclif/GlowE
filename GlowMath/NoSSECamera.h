#pragma once
#ifndef GLOWE_MATH_NOSSE_CAMERA_INCLUDED
#define GLOWE_MATH_NOSSE_CAMERA_INCLUDED

#include "Math.h"
#include "NoSSEMatrixClasses.h"
#include "Transform.h"

namespace GLOWE
{
	namespace NoSSE
	{
		class GLOWMATH_EXPORT Camera
		{
		private:
			Float4x4 view, projection;
		public:
			Camera();

			void perspectiveProjection(const float width, const float height, const float farF = 1000.0f, const float nearF = 0.01f);
			void perspectiveProjectionFov(const float fov, const float aspectRatio, const float farF = 1000.0f, const float nearF = 0.01f);

			void orthographicProjectionOffScreen(const float left, const float right, const float top, const float bottom, const float farF = 1000.0f, const float nearF = 0.01f);
			void orthographicProjection(const float width, const float height, const float farF = 1000.0f, const float nearF = 0.01f);

			void lookAt(const Float3& eye, const Float3& center, const Float3& up = Float3{ 0.0f, 1.0f, 0.0f });
			void fromTransform(const Transform& transform);

			Matrix<4, 4> getView() const;
			Matrix<4, 4> getProj() const;

			void setProj(const Float4x4& mat);
			void setView(const Float4x4& mat);
		};
	}
}

#endif
