#include "Plane.h"
#include "AxisAlignedBoundingBox.h"

void GLOWE::Plane::fromNormalAndPoint(const Vector3F& normal, const Vector3F& point)
{
	Vector4F tempVec = (Vector4F)normal;
	tempVec.setW(-normal.scalarProduct(point));
	tempVec /= normal.getMagnitude(); // Normalise the plane equation.
	abcd = tempVec.toFloat4();
}

GLOWE::Float3 GLOWE::Plane::getNormal() const
{
	return Float3{ abcd[0], abcd[1], abcd[2] };
}

GLOWE::Plane GLOWE::Plane::normalize() const
{
	Vector3F vec(abcd[0], abcd[1], abcd[2]);
	Vector4F result(abcd);
	
	return Plane((result / vec.getMagnitude()).toFloat4());
}

float GLOWE::Plane::getDistanceFromPoint(const Vector3F& point) const
{
	const Vector4F tempVec(point.getX(), point.getY(), point.getZ(), 1.0f);
	return tempVec.scalarProduct(abcd);
}

GLOWE::Plane::Where GLOWE::Plane::getPointPlacement(const Vector3F& point) const
{
	float distance = getDistanceFromPoint(point);

	if (std::abs(distance) < MathHelper::Epsilon)
	{
		return Where::OnPlane;
	}

	return std::signbit(distance) ? Where::Negative : Where::Positive;
}

bool GLOWE::Plane::isPointInside(const Vector3F& point) const
{
	return getDistanceFromPoint(point) > -MathHelper::Epsilon;
}

bool GLOWE::Plane::isAABBInside(const AABB& aabb) const
{
#if GLOWE_USED_SSE > GLOWE_NO_SSE
	constexpr UInt32 signMask = 0x80000000;
	Vector3F signFlip = _mm_and_ps(Vector4F(abcd).m, _mm_set_ps1(*reinterpret_cast<const float*>(&signMask)));
	Vector4F plane(abcd);
	plane.setW(0.0f);
	Vector3F aabbVec = Vector3F(aabb.center) + _mm_xor_ps(Vector3F(aabb.halfSize).m, signFlip.m);
	const float scalProd = aabbVec.scalarProduct(Vector3F(plane.m));

	return scalProd > -abcd[3];
#else
	Vector3F plane(abcd[0], abcd[1], abcd[2]);
	Vector3F signFlip(
		std::signbit(abcd[0]) ? -1.0f : 1.0f,
		std::signbit(abcd[1]) ? -1.0f : 1.0f,
		std::signbit(abcd[2]) ? -1.0f : 1.0f);
	Vector3F center = aabb.center, halfSize = aabb.halfSize;
	center = center + (halfSize * signFlip);

	return center.scalarProduct(plane) > -abcd[3];
#endif
}

GLOWE::Vector3F GLOWE::Plane::getLineIntersection(const Vector3F& direction, const Vector3F& point) const
{
	Vector3F normal(abcd[0], abcd[1], abcd[2]);

	if (normal.scalarProduct(direction) <= MathHelper::Epsilon)
	{
		if (getDistanceFromPoint(point) < MathHelper::Epsilon)
		{
			return point;
		}
		else
		{
			return { NAN, NAN, NAN };
		}
		throw NotImplementedException();
	}

	float t = -(normal.scalarProduct(point) + abcd[3]) / (normal.scalarProduct(direction));
	return (direction * t) + point;
}
