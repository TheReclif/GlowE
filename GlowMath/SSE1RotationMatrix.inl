#pragma once
#ifndef GLOWE_SSE1_MATH_ROTATIONMATRIX_INL_INCLUDED
#define GLOWE_SSE1_MATH_ROTATIONMATRIX_INL_INCLUDED

#include "RotationMatrix.h"

#if GLOWE_USED_SSE > GLOWE_NO_SSE
GLOWE::RotationMatrix::operator GLOWE::Matrix4x4() const
{
	return Matrix4x4(row0.m, row1.m, row2.m, _mm_set_ps(1.0f, 0.0f, 0.0f, 0.0f));
}

GLOWE::Quaternion GLOWE::RotationMatrix::toQuaternion() const
{
	return Quaternion::fromRotationMatrix(*this);
}

GLOWE::Vector4F GLOWE::RotationMatrix::toAxisAngle() const
{
	// In honour of the brave brain cells that gave up trying to solve everything using only (and only!) SIMD.
	// AAAAAAAAAAAAAAAAAAAAAAAAA
	constexpr float singularityEpsilon = 0.1f;

	// m01, m02, m12, m12
	__m128 firstPart = _mm_shuffle_ps(row0.m, row1.m, _MM_SHUFFLE(2, 2, 2, 1));

	// m10, m20, m11, m21
	__m128 temp = _mm_unpacklo_ps(row1.m, row2.m);

	// m10, m20, m21, m21
	__m128 secondPart = _mm_shuffle_ps(temp, temp, _MM_SHUFFLE(3, 3, 1, 0));

	__m128 diff = _mm_sub_ps(secondPart, firstPart);

	temp = _mm_sub_ps(firstPart, secondPart);

	int result = _mm_movemask_ps(_mm_cmplt_ps(temp, _mm_set_ps1(MathHelper::Epsilon)));

	if (result == 0xF)
	{
		// Singularity - angle equals 0� or 180�.
		// Check for identity matrix (angle of 0�).
		__m128 tempIsIdent = _mm_shuffle_ps(row0.m, row1.m, _MM_SHUFFLE(1, 0, 1, 0));
		tempIsIdent = _mm_shuffle_ps(tempIsIdent, row1.m, _MM_SHUFFLE(0, 0, 2, 0));
		tempIsIdent = GLOWE::SSE1::fabs(_mm_add_ps(row0.m, tempIsIdent));
		if (((_mm_movemask_ps(_mm_cmplt_ps(temp, _mm_set_ps1(singularityEpsilon))) & 0xE) == 0xE) && (row0.f[0] + row1.f[1] + row2.f[2] - 3.0f) < singularityEpsilon)
		{
			// 0�.
			return Vector4F(0.0f, 1.0f, 0.0f, 0.0f);
		}
		else
		{
			// 180�.
			float x, y, z;
			const float xx = (row0.f[0] + 1) / 2;
			const float yy = (row1.f[1] + 1) / 2;
			const float zz = (row2.f[2] + 1) / 2;
			const float xy = (row0.f[1] + row1.f[0]) / 4;
			const float xz = (row0.f[2] + row2.f[0]) / 4;
			const float yz = (row1.f[2] + row2.f[1]) / 4;

			if ((xx > yy) && (xx > zz)) 
			{
				// m[0][0] is the largest diagonal term
				if (xx < MathHelper::Epsilon) 
				{
					x = 0;
					y = 0.7071f;
					z = 0.7071f;
				}
				else
				{
					x = std::sqrt(xx);
					y = xy / x;
					z = xz / x;
				}
			}
			else if (yy > zz)
			{
				// m[1][1] is the largest diagonal term
				if (yy < MathHelper::Epsilon)
				{
					x = 0.7071f;
					y = 0;
					z = 0.7071f;
				}
				else
				{
					y = std::sqrt(yy);
					x = xy / y;
					z = yz / y;
				}
			}
			else
			{
				// m[2][2] is the largest diagonal term so base result on this
				if (zz < MathHelper::Epsilon)
				{
					x = 0.7071f;
					y = 0.7071f;
					z = 0;
				}
				else
				{
					z = std::sqrt(zz);
					x = xz / z;
					y = yz / z;
				}
			}

			return Vector4F(MathHelper::Pi, x, y, z);
		}
	}

	__m128 temp2 = temp;

	// Powers - we need to sum them
	temp = _mm_mul_ps(temp, temp);

	// Pow(2), Pow(2), x, x
	__m128 zuza = _mm_unpackhi_ps(temp, temp);

	{
		// Pow(0) + Pow(2), x, x, x
		zuza = _mm_add_ps(temp, zuza);

		__m128 aaaaaaa = _mm_shuffle_ps(temp, temp, _MM_SHUFFLE(1, 1, 1, 1));
		zuza = _mm_add_ps(zuza, aaaaaaa);

		// Perhaps replace with normal, casual sqrt_ss
		zuza = _mm_rsqrt_ss(zuza); //WARNING: Approximation
		zuza = _mm_shuffle_ps(zuza, zuza, _MM_SHUFFLE(0, 0, 0, 0)); // Duplicate lowest element in every place
	}

	// m[0][0], x, x, m[1][1]
	firstPart = _mm_movelh_ps(row0.m, row1.m);
	// m[2][2], x, -1, -1
	secondPart = _mm_movehl_ps(_mm_set_ps1(-1.0f), row2.m);

	// m00 + m22, x, x, m11 - 1
	firstPart = _mm_add_ps(firstPart, secondPart);

	// m11 - 1, m11 - 1, m11 - 1, m11 - 1
	secondPart = _mm_shuffle_ps(firstPart, firstPart, _MM_SHUFFLE(3, 3, 3, 3));

	firstPart = _mm_add_ss(firstPart, secondPart);
	secondPart = _mm_mul_ss(firstPart, _mm_set_ps1(1.0f / 2.0f));

	const float angle = std::acos(_mm_cvtss_f32(secondPart));

	diff = _mm_mul_ps(diff, zuza);

	alignas(16) float f[4];
	_mm_store_ps(f, diff);

	return Vector4F(angle, f[0], f[1], f[2]);
}

GLOWE::Vector3F GLOWE::RotationMatrix::toEulerAngles() const
{
	// TODO: Test with SSE equivalents

	// Don't worry, Z equals 0 when not modified
	Vector3F result;

	const float m10 = (*this)(1, 0);

	result.setY(std::asin(m10));

	if (m10 > 0.998f)
	{
		result.setX(std::atan2((*this)(0, 2), (*this)(2, 2)));
		result.setY(MathHelper::PiDiv2);
	}
	else
	if (m10 < -0.998f)
	{
		result.setX(std::atan2((*this)(0, 2), (*this)(2, 2)));
		result.setY(-MathHelper::PiDiv2);
	}
	else
	{
		result.setX(std::atan2(-(*this)(2, 0), (*this)(0, 0)));
		result.setZ(std::atan2(-(*this)(1, 2), (*this)(1, 1)));
	}

	// vec(x, y, z)
	return result;
}

GLOWE::RotationMatrix GLOWE::RotationMatrix::fromQuaternion(const Quat qt)
{
	return qt.asRotationMatrix();
}

GLOWE::RotationMatrix GLOWE::RotationMatrix::fromAxisAngle(const Vec4 vec)
{
	const float allMask = MathHelper::FloatAllBitsSet;

	const float angle = _mm_cvtss_f32(vec);

	const float angleCos = std::cos(angle);
	const float angleSin = std::sin(angle);
	const float t = 1.0f - angleCos;

	Matrix3x3 firstMat = angleCos * Matrix3x3::getIdentity();
	Matrix3x3 secondMat;
	
	{
		__m128 firstRow, secondRow, thirdRow;

		firstRow = _mm_shuffle_ps(vec, vec, _MM_SHUFFLE(1, 1, 1, 1));
		firstRow = _mm_mul_ps(firstRow, _mm_shuffle_ps(vec, vec, _MM_SHUFFLE(3, 3, 2, 1)));

		secondRow = _mm_shuffle_ps(vec, vec, _MM_SHUFFLE(2, 2, 2, 1));
		secondRow = _mm_mul_ps(secondRow, _mm_shuffle_ps(vec, vec, _MM_SHUFFLE(3, 3, 2, 2)));

		thirdRow = _mm_shuffle_ps(vec, vec, _MM_SHUFFLE(3, 3, 2, 1));
		thirdRow = _mm_mul_ps(thirdRow, _mm_shuffle_ps(vec, vec, _MM_SHUFFLE(3, 3, 3, 3)));

		auto zeroLastRowElem = [allMask](const __m128 arg) -> __m128
		{
			return _mm_and_ps(arg, _mm_set_ps(0.0f, allMask, allMask, allMask));
		};

		secondMat.row0.m = zeroLastRowElem(firstRow);
		secondMat.row1.m = zeroLastRowElem(secondRow);
		secondMat.row2.m = zeroLastRowElem(thirdRow);
	}

	secondMat *= t;

	Matrix3x3 thirdMat;

	{
		__m128 temp;
		__m128 firstRow, secondRow, thirdRow;

		firstRow = _mm_shuffle_ps(vec, vec, _MM_SHUFFLE(2, 2, 3, 2));
		temp = _mm_and_ps(firstRow, _mm_set_ps(0.0f, allMask, allMask, 0.0f));
		firstRow = _mm_xor_ps(temp, _mm_set_ps(0.0f, 0.0f, -0.0f, 0.0f));

		secondRow = _mm_shuffle_ps(vec, vec, _MM_SHUFFLE(1, 1, 1, 3));
		temp = _mm_and_ps(secondRow, _mm_set_ps(0.0f, allMask, 0.0f, allMask));
		secondRow = _mm_xor_ps(temp, _mm_set_ps(0.0f, -0.0f, 0.0f, 0.0f));

		thirdRow = _mm_shuffle_ps(vec, vec, _MM_SHUFFLE(1, 1, 1, 2));
		temp = _mm_and_ps(thirdRow, _mm_set_ps(0.0f, 0.0f, allMask, allMask));
		thirdRow = _mm_xor_ps(temp, _mm_set_ps(0.0f, 0.0f, 0.0f, -0.0f));

		thirdMat.row0.m = firstRow;
		thirdMat.row1.m = secondRow;
		thirdMat.row2.m = thirdRow;
	}

	thirdMat *= angleSin;

	return firstMat + secondMat + thirdMat;
}

GLOWE::RotationMatrix GLOWE::RotationMatrix::fromEulerAngles(const Vec3 vec)
{
	const float allMask = MathHelper::FloatAllBitsSet;

	// sx, sy, sz, something
	__m128 anglesSin = sin(vec);

	// cx, cy, cz, something
	__m128 anglesCos = cos(vec);

	Matrix3x3 result, tempMat;

	{
		__m128 mask = _mm_set_ps(0.0f, allMask, allMask, 0.0f);

		__m128 temp = _mm_shuffle_ps(anglesSin, anglesCos, 0);
		__m128 firstRow = _mm_set_ps(0.0f, 0.0f, 0.0f, 1.0f);

		__m128 secondRow = _mm_shuffle_ps(temp, temp, _MM_SHUFFLE(0, 1, 2, 3));
		secondRow = _mm_and_ps(secondRow, mask);
		secondRow = _mm_xor_ps(secondRow, _mm_set_ps(0.0f, -0.0f, 0.0f, 0.0f));

		__m128 thirdRow = _mm_shuffle_ps(temp, temp, _MM_SHUFFLE(3, 2, 1, 0));
		thirdRow = _mm_and_ps(thirdRow, mask);

		result.row0.m = firstRow;
		result.row1.m = secondRow;
		result.row2.m = thirdRow;
	}

	{
		__m128 mask = _mm_set_ps(0.0f, allMask, 0.0f, allMask);

		// sy, sy, cy, cy
		__m128 temp = _mm_shuffle_ps(anglesSin, anglesCos, _MM_SHUFFLE(1, 1, 1, 1));

		__m128 firstRow = _mm_shuffle_ps(temp, temp, _MM_SHUFFLE(0, 1, 2, 3));
		firstRow = _mm_and_ps(firstRow, mask);

		__m128 secondRow = _mm_set_ps(0.0f, 0.0f, 1.0f, 0.0f);

		__m128 thirdRow = _mm_shuffle_ps(temp, temp, _MM_SHUFFLE(3, 2, 1, 0));
		thirdRow = _mm_and_ps(thirdRow, mask);
		thirdRow = _mm_xor_ps(thirdRow, _mm_set_ps(0.0f, 0.0f, 0.0f, -0.0f));

		tempMat.row0.m = firstRow;
		tempMat.row1.m = secondRow;
		tempMat.row2.m = thirdRow;
	}

	result *= tempMat;

	{
		__m128 mask = _mm_set_ps(0.0f, 0.0f, allMask, allMask);

		__m128 temp = _mm_shuffle_ps(anglesSin, anglesCos, _MM_SHUFFLE(2, 2, 2, 2));

		__m128 firstRow = _mm_shuffle_ps(temp, temp, _MM_SHUFFLE(3, 3, 0, 2));
		firstRow = _mm_and_ps(firstRow, mask);
		firstRow = _mm_xor_ps(firstRow, _mm_set_ps(0.0f, 0.0f, -0.0f, 0.0f));

		__m128 secondRow = _mm_shuffle_ps(temp, temp, _MM_SHUFFLE(3, 3, 2, 0));
		secondRow = _mm_and_ps(secondRow, mask);

		__m128 thirdRow = _mm_set_ps(0.0f, 1.0f, 0.0f, 0.0f);

		tempMat.row0.m = firstRow;
		tempMat.row1.m = secondRow;
		tempMat.row2.m = thirdRow;
	}

	result *= tempMat;

	return RotationMatrix(result);
}

GLOWE::Matrix3x3 GLOWE::RotationMatrix::mat4x4ToMat3x3(const Mat4x4 arg)
{
	return Matrix3x3(arg.row0.m, arg.row1.m, arg.row2.m);
}

#endif

#endif