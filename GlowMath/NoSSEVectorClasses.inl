#pragma once
#ifndef GLOWE_MATH_NOSSEVECTORCLASSES_INL_INCLUDED
#define GLOWE_MATH_NOSSEVECTORCLASSES_INL_INCLUDED

#include "NoSSEVectorClasses.h"

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount>::Vector(const Type allElems)
{
	std::fill_n(elems, ElemsCount, allElems);
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount>::Vector(const Type arg[ElemsCount])
{
	std::memcpy(elems, arg, sizeof(Type) * ElemsCount);
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount>::Vector(const Array1D<Type, ElemsCount>& arg)
	: elems(arg)
{
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount>::operator Array1D<Type, ElemsCount>() const
{
	Array1D<Type, ElemsCount> result;
	std::memcpy(result.data(), elems, sizeof(Type) * ElemsCount);
	return std::move(result);
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> GLOWE::NoSSE::Vector<Type, ElemsCount>::operator-() const
{
	return *this * (Type)(-1);
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> GLOWE::NoSSE::Vector<Type, ElemsCount>::operator+(const Vector & right) const
{
	Vector<Type, ElemsCount> result(*this);

	for (unsigned int x = 0; x < ElemsCount; ++x)
	{
		result[x] += right[x];
	}

	return result;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> GLOWE::NoSSE::Vector<Type, ElemsCount>::operator-(const Vector & right) const
{
	Vector<Type, ElemsCount> result(*this);

	for (unsigned int x = 0; x < ElemsCount; ++x)
	{
		result[x] -= right[x];
	}

	return result;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> GLOWE::NoSSE::Vector<Type, ElemsCount>::operator*(const Vector & right) const
{
	Vector<Type, ElemsCount> result(*this);

	for (unsigned int x = 0; x < ElemsCount; ++x)
	{
		result[x] *= right[x];
	}

	return result;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> GLOWE::NoSSE::Vector<Type, ElemsCount>::operator/(const Vector & right) const
{
	Vector<Type, ElemsCount> result(*this);

	for (unsigned int x = 0; x < ElemsCount; ++x)
	{
		result[x] /= right[x];
	}

	return result;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> GLOWE::NoSSE::Vector<Type, ElemsCount>::operator+(const Type& right) const
{
	Vector<Type, ElemsCount> result(*this);

	for (unsigned int x = 0; x < ElemsCount; ++x)
	{
		result[x] += right;
	}

	return result;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> GLOWE::NoSSE::Vector<Type, ElemsCount>::operator-(const Type& right) const
{
	Vector<Type, ElemsCount> result(*this);

	for (unsigned int x = 0; x < ElemsCount; ++x)
	{
		result[x] -= right;
	}

	return result;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> GLOWE::NoSSE::Vector<Type, ElemsCount>::operator*(const Type& right) const
{
	Vector<Type, ElemsCount> result(*this);

	for (unsigned int x = 0; x < ElemsCount; ++x)
	{
		result[x] *= right;
	}

	return result;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> GLOWE::NoSSE::Vector<Type, ElemsCount>::operator/(const Type& right) const
{
	Vector<Type, ElemsCount> result(*this);

	for (unsigned int x = 0; x < ElemsCount; ++x)
	{
		result[x] /= right;
	}

	return result;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> & GLOWE::NoSSE::Vector<Type, ElemsCount>::operator+=(const Vector & right)
{
	return *this = *this + right;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> & GLOWE::NoSSE::Vector<Type, ElemsCount>::operator-=(const Vector & right)
{
	return *this = *this - right;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> & GLOWE::NoSSE::Vector<Type, ElemsCount>::operator*=(const Vector & right)
{
	return *this = *this * right;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> & GLOWE::NoSSE::Vector<Type, ElemsCount>::operator/=(const Vector & right)
{
	return *this = *this / right;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> & GLOWE::NoSSE::Vector<Type, ElemsCount>::operator+=(const Type & right)
{
	return *this = *this + right;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> & GLOWE::NoSSE::Vector<Type, ElemsCount>::operator-=(const Type & right)
{
	return *this = *this - right;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> & GLOWE::NoSSE::Vector<Type, ElemsCount>::operator*=(const Type & right)
{
	return *this = *this * right;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> & GLOWE::NoSSE::Vector<Type, ElemsCount>::operator/=(const Type & right)
{
	return *this = *this / right;
}

template<class Type, unsigned int ElemsCount>
inline Type GLOWE::NoSSE::Vector<Type, ElemsCount>::getMagnitude() const
{
	return std::sqrt(getScalarProduct());
}

template<class Type, unsigned int ElemsCount>
inline Type GLOWE::NoSSE::Vector<Type, ElemsCount>::getMagnitudeSquared() const
{
	return getScalarProduct();
}

template<class Type, unsigned int ElemsCount>
inline Type GLOWE::NoSSE::Vector<Type, ElemsCount>::getScalarProduct() const
{
	return scalarProduct(*this);
}

template<class Type, unsigned int ElemsCount>
inline Type GLOWE::NoSSE::Vector<Type, ElemsCount>::scalarProduct(const Vector & vec) const
{
	Type result = elems[0] * vec.elems[0];
	for (unsigned int x = 1; x < ElemsCount; ++x)
	{
		result += elems[x] * vec.elems[x];
	}
	return result;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> GLOWE::NoSSE::Vector<Type, ElemsCount>::convertToRadians() const
{
	Vector<Type, ElemsCount> result;
	for (unsigned int x = 0; x < ElemsCount; ++x)
	{
		result.elems[x] = MathHelper::convertToRadians(elems[x]);
	}
	return result;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> GLOWE::NoSSE::Vector<Type, ElemsCount>::convertToDegrees() const
{
	Vector<Type, ElemsCount> result;
	for (unsigned int x = 0; x < ElemsCount; ++x)
	{
		result.elems[x] = MathHelper::convertToDegrees(elems[x]);
	}
	return result;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> GLOWE::NoSSE::Vector<Type, ElemsCount>::normalize() const
{
	const float mag = getMagnitude();
	if (MathHelper::compareFloats(mag, 0.0f))
	{
		return GLOWE::NoSSE::Vector<Type, ElemsCount>();
	}
	return *this / mag;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> GLOWE::NoSSE::Vector<Type, ElemsCount>::normalizeFast() const
{
	return *this / getMagnitude();
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> GLOWE::NoSSE::Vector<Type, ElemsCount>::abs() const
{
	Vector result;

	for (unsigned int x = 0; x < ElemsCount; ++x)
	{
		result.elems[x] = std::fabs(elems[x]);
	}

	return result;
}

template<class Type, unsigned int ElemsCount>
inline GLOWE::NoSSE::Vector<Type, ElemsCount> GLOWE::NoSSE::Vector<Type, ElemsCount>::clamp(const Vector& min, const Vector& max) const
{
	Vector result;

	for (unsigned int x = 0; x < ElemsCount; ++x)
	{
		result.elems[x] = MathHelper::clamp(elems[x], min.elems[x], max.elems[x]);
	}

	return result;
}

template<class Type, unsigned int ElemsCount>
inline bool GLOWE::NoSSE::Vector<Type, ElemsCount>::checkIsNormalized() const
{
	return fabs(getMagnitude() - 1.0f) <= MathHelper::Epsilon;
}

template<class Type, unsigned int ElemsCount>
inline bool GLOWE::NoSSE::Vector<Type, ElemsCount>::operator==(const Vector & right) const
{
	for (unsigned int x = 0; x < ElemsCount; ++x)
	{
		if (elems[x] != right.elems[x])
		{
			return false;
		}
	}

	return true;
}

template<class Type, unsigned int ElemsCount>
inline bool GLOWE::NoSSE::Vector<Type, ElemsCount>::operator<(const Vector & right) const
{
	return getMagnitude() < right.getMagnitude();
}

template<class Type, unsigned int ElemsCount>
inline bool GLOWE::NoSSE::Vector<Type, ElemsCount>::operator>(const Vector & right) const
{
	return getMagnitude() > right.getMagnitude();
}

template<class Type, unsigned int ElemsCount>
inline Type& GLOWE::NoSSE::Vector<Type, ElemsCount>::operator[](const unsigned int pos)
{
	return elems[pos];
}

template<class Type, unsigned int ElemsCount>
inline Type GLOWE::NoSSE::Vector<Type, ElemsCount>::operator[](const unsigned int pos) const
{
	return elems[pos];
}

template<class Type, unsigned int ElemsCount>
template<class Func>
inline void GLOWE::NoSSE::Vector<Type, ElemsCount>::applyFunction(Func&& func)
{
	for (unsigned int x = 0; x < ElemsCount; ++x)
	{
		elems[x] = func(elems[x]);
	}
}

inline GLOWE::NoSSE::Vector<float, 2>::Vector()
	: f{ 0.0f, 0.0f }
{
}

inline GLOWE::NoSSE::Vector<float, 2>::Vector(const float a, const float b)
	: f{ a, b }
{
}

inline GLOWE::NoSSE::Vector<float, 2>::Vector(const float ab)
	: f{ ab, ab }
{
}

inline GLOWE::NoSSE::Vector<float, 2>::Vector(const float ab[2])
	: f{ ab[0], ab[1] }
{
}

inline GLOWE::NoSSE::Vector<float, 2>::Vector(const Float2 & ab)
	: f{ ab[0], ab[1] }
{
}

inline GLOWE::NoSSE::Vector<float, 2>::operator Float2() const
{
	return Float2{ f[0], f[1] };
}

inline GLOWE::NoSSE::Vector<float, 2> GLOWE::NoSSE::Vector<float, 2>::operator-() const
{
	return Vector(f[0] * (-1.0f), f[1] * (-1.0f));
}

inline GLOWE::NoSSE::Vector<float, 2> GLOWE::NoSSE::Vector<float, 2>::operator+(const Vector & right) const
{
	return Vector(f[0] + right.f[0], f[1] + right.f[1]);
}

inline GLOWE::NoSSE::Vector<float, 2> GLOWE::NoSSE::Vector<float, 2>::operator-(const Vector & right) const
{
	return Vector(f[0] - right.f[0], f[1] - right.f[1]);
}

inline GLOWE::NoSSE::Vector<float, 2> GLOWE::NoSSE::Vector<float, 2>::operator*(const Vector & right) const
{
	return Vector(f[0] * right.f[0], f[1] * right.f[1]);
}

inline GLOWE::NoSSE::Vector<float, 2> GLOWE::NoSSE::Vector<float, 2>::operator/(const Vector & right) const
{
	return Vector(f[0] / right.f[0], f[1] / right.f[1]);
}

inline GLOWE::NoSSE::Vector<float, 2> GLOWE::NoSSE::Vector<float, 2>::operator+(const float right) const
{
	return Vector(f[0] + right, f[1] + right);
}

inline GLOWE::NoSSE::Vector<float, 2> GLOWE::NoSSE::Vector<float, 2>::operator-(const float right) const
{
	return Vector(f[0] - right, f[1] - right);
}

inline GLOWE::NoSSE::Vector<float, 2> GLOWE::NoSSE::Vector<float, 2>::operator*(const float right) const
{
	return Vector(f[0] * right, f[1] * right);
}

inline GLOWE::NoSSE::Vector<float, 2> GLOWE::NoSSE::Vector<float, 2>::operator/(const float right) const
{
	return Vector(f[0] / right, f[1] / right);
}

inline GLOWE::NoSSE::Vector<float, 2> & GLOWE::NoSSE::Vector<float, 2>::operator+=(const Vector & right)
{
	f[0] += right.f[0];
	f[1] += right.f[1];
	return *this;
}

inline GLOWE::NoSSE::Vector<float, 2> & GLOWE::NoSSE::Vector<float, 2>::operator-=(const Vector & right)
{
	f[0] -= right.f[0];
	f[1] -= right.f[1];
	return *this;
}

inline GLOWE::NoSSE::Vector<float, 2> & GLOWE::NoSSE::Vector<float, 2>::operator*=(const Vector & right)
{
	f[0] *= right.f[0];
	f[1] *= right.f[1];
	return *this;
}

inline GLOWE::NoSSE::Vector<float, 2> & GLOWE::NoSSE::Vector<float, 2>::operator/=(const Vector & right)
{
	f[0] /= right.f[0];
	f[1] /= right.f[1];
	return *this;
}

inline GLOWE::NoSSE::Vector<float, 2> & GLOWE::NoSSE::Vector<float, 2>::operator+=(const float right)
{
	f[0] += right;
	f[1] += right;
	return *this;
}

inline GLOWE::NoSSE::Vector<float, 2> & GLOWE::NoSSE::Vector<float, 2>::operator-=(const float right)
{
	f[0] -= right;
	f[1] -= right;
	return *this;
}

inline GLOWE::NoSSE::Vector<float, 2> & GLOWE::NoSSE::Vector<float, 2>::operator*=(const float right)
{
	f[0] *= right;
	f[1] *= right;
	return *this;
}

inline GLOWE::NoSSE::Vector<float, 2> & GLOWE::NoSSE::Vector<float, 2>::operator/=(const float right)
{
	f[0] /= right;
	f[1] /= right;
	return *this;
}

inline float GLOWE::NoSSE::Vector<float, 2>::getMagnitude() const
{
	return sqrtf(getScalarProduct());
}

inline float GLOWE::NoSSE::Vector<float, 2>::getMagnitudeSquared() const
{
	return getScalarProduct();
}

inline float GLOWE::NoSSE::Vector<float, 2>::getScalarProduct() const
{
	return (f[0] * f[0]) + (f[1] * f[1]);
}

inline float GLOWE::NoSSE::Vector<float, 2>::scalarProduct(const Vector & vec) const
{
	return (f[0] * vec.f[0]) + (f[1] * vec.f[1]);
}

inline GLOWE::NoSSE::Vector<float, 2> GLOWE::NoSSE::Vector<float, 2>::convertToRadians() const
{
	return Vector(MathHelper::convertToRadians(f[0]), MathHelper::convertToRadians(f[1]));
}

inline GLOWE::NoSSE::Vector<float, 2> GLOWE::NoSSE::Vector<float, 2>::convertToDegrees() const
{
	return Vector(MathHelper::convertToDegrees(f[0]), MathHelper::convertToDegrees(f[1]));
}

inline GLOWE::NoSSE::Vector<float, 2> GLOWE::NoSSE::Vector<float, 2>::normalize() const
{
	const float mag = getMagnitude();
	if (MathHelper::compareFloats(mag, 0.0f))
	{
		return GLOWE::NoSSE::Vector<float, 2>(0.0f, 0.0f);
	}
	return GLOWE::NoSSE::Vector<float, 2>(f[0] / mag, f[1] / mag);
}

inline GLOWE::NoSSE::Vector<float, 2> GLOWE::NoSSE::Vector<float, 2>::normalizeFast() const
{
	return *this / getMagnitude();
}

inline GLOWE::NoSSE::Vector<float, 2> GLOWE::NoSSE::Vector<float, 2>::abs() const
{
	return NoSSE::Vector<float, 2>(std::fabs(f[0]), std::fabs(f[1]));
}

inline GLOWE::NoSSE::Vector<float, 2> GLOWE::NoSSE::Vector<float, 2>::clamp(const Vector& min, const Vector& max) const
{
	return NoSSE::Vector<float, 2>(GLOWE::clamp(f[0], min.f[0], max.f[0]), GLOWE::clamp(f[1], min.f[1], max.f[1]));
}

inline bool GLOWE::NoSSE::Vector<float, 2>::checkIsNormalized() const
{
	return fabs(getMagnitude() - 1.0f) <= MathHelper::Epsilon;
}

inline bool GLOWE::NoSSE::Vector<float, 2>::operator==(const Vector & right) const
{
	using namespace GLOWE::MathHelper;
	return compareFloats(f[0], right.f[0]) && compareFloats(f[1], right.f[1]);
}

inline bool GLOWE::NoSSE::Vector<float, 2>::operator<(const Vector & right) const
{
	return getMagnitude() < right.getMagnitude();
}

inline bool GLOWE::NoSSE::Vector<float, 2>::operator>(const Vector & right) const
{
	return getMagnitude() > right.getMagnitude();
}

inline float GLOWE::NoSSE::Vector<float, 2>::operator[](const unsigned int pos) const
{
	return f[pos];
}

inline float GLOWE::NoSSE::Vector<float, 2>::getX() const
{
	return f[0];
}

inline float GLOWE::NoSSE::Vector<float, 2>::getY() const
{
	return f[1];
}

inline void GLOWE::NoSSE::Vector<float, 2>::setX(const float arg)
{
	f[0] = arg;
}

inline void GLOWE::NoSSE::Vector<float, 2>::setY(const float arg)
{
	f[1] = arg;
}

template<class Func>
inline void GLOWE::NoSSE::Vector<float, 2>::applyFunction(Func&& func)
{
	for (unsigned int x = 0; x < 2; ++x)
	{
		f[x] = func(f[x]);
	}
}

// NoSSE::Vector3F

GLOWE::NoSSE::Vector<float, 3>::Vector()
	: f{ 0.0f, 0.0f, 0.0f }
{
}

GLOWE::NoSSE::Vector<float, 3>::Vector(const float a, const float b, const float c)
	: f{ a, b, c }
{
}

GLOWE::NoSSE::Vector<float, 3>::Vector(const float abc)
	: f{ abc, abc, abc }
{
}

GLOWE::NoSSE::Vector<float, 3>::Vector(const float abc[3])
	: f{ abc[0], abc[1], abc[2] }
{
}

inline GLOWE::NoSSE::Vector<float, 3>::Vector(const Float3 & abc)
	: f{ abc[0], abc[1], abc[2] }
{
}

inline GLOWE::NoSSE::Vector<float, 3>::operator Float3() const
{
	return Float3{ f[0], f[1], f[2] };
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::operator-() const
{
	return Vector(f[0] * (-1.0f), f[1] * (-1.0f), f[2] * (-1.0f));
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::operator+(const Vector & right) const
{
	return Vector(f[0] + right.f[0], f[1] + right.f[1], f[2] + right.f[2]);
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::operator-(const Vector & right) const
{
	return Vector(f[0] - right.f[0], f[1] - right.f[1], f[2] - right.f[2]);
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::operator*(const Vector & right) const
{
	return Vector(f[0] * right.f[0], f[1] * right.f[1], f[2] * right.f[2]);
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::operator/(const Vector & right) const
{
	return Vector(f[0] / right.f[0], f[1] / right.f[1], f[2] / right.f[2]);
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::operator+(const float right) const
{
	return Vector(f[0] + right, f[1] + right, f[2] + right);
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::operator-(const float right) const
{
	return Vector(f[0] - right, f[1] - right, f[2] - right);
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::operator*(const float right) const
{
	return Vector(f[0] * right, f[1] * right, f[2] * right);
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::operator/(const float right) const
{
	return Vector(f[0] / right, f[1] / right, f[2] / right);
}

inline GLOWE::NoSSE::Vector<float, 3> & GLOWE::NoSSE::Vector<float, 3>::operator+=(const Vector & right)
{
	f[0] += right.f[0];
	f[1] += right.f[1];
	f[2] += right.f[2];

	return *this;
}

inline GLOWE::NoSSE::Vector<float, 3> & GLOWE::NoSSE::Vector<float, 3>::operator-=(const Vector & right)
{
	f[0] -= right.f[0];
	f[1] -= right.f[1];
	f[2] -= right.f[2];

	return *this;
}

inline GLOWE::NoSSE::Vector<float, 3> & GLOWE::NoSSE::Vector<float, 3>::operator*=(const Vector & right)
{
	f[0] *= right.f[0];
	f[1] *= right.f[1];
	f[2] *= right.f[2];

	return *this;
}

inline GLOWE::NoSSE::Vector<float, 3> & GLOWE::NoSSE::Vector<float, 3>::operator/=(const Vector & right)
{
	f[0] /= right.f[0];
	f[1] /= right.f[1];
	f[2] /= right.f[2];

	return *this;
}

inline GLOWE::NoSSE::Vector<float, 3> & GLOWE::NoSSE::Vector<float, 3>::operator+=(const float right)
{
	f[0] += right;
	f[1] += right;
	f[2] += right;

	return *this;
}

inline GLOWE::NoSSE::Vector<float, 3> & GLOWE::NoSSE::Vector<float, 3>::operator-=(const float right)
{
	f[0] -= right;
	f[1] -= right;
	f[2] -= right;

	return *this;
}

inline GLOWE::NoSSE::Vector<float, 3> & GLOWE::NoSSE::Vector<float, 3>::operator*=(const float right)
{
	f[0] *= right;
	f[1] *= right;
	f[2] *= right;

	return *this;
}

inline GLOWE::NoSSE::Vector<float, 3> & GLOWE::NoSSE::Vector<float, 3>::operator/=(const float right)
{
	f[0] /= right;
	f[1] /= right;
	f[2] /= right;

	return *this;
}

inline float GLOWE::NoSSE::Vector<float, 3>::getMagnitude() const
{
	return sqrtf(getScalarProduct());
}

inline float GLOWE::NoSSE::Vector<float, 3>::getMagnitudeSquared() const
{
	return getScalarProduct();
}

inline float GLOWE::NoSSE::Vector<float, 3>::getScalarProduct() const
{
	return (f[0] * f[0]) + (f[1] * f[1]) + (f[2] * f[2]);
}

inline float GLOWE::NoSSE::Vector<float, 3>::scalarProduct(const Vector & vec) const
{
	return (f[0] * vec.f[0]) + (f[1] * vec.f[1]) + (f[2] * vec.f[2]);
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::vectorProduct(const Vector & vec) const
{
	return Vector
	(
		(f[1] * vec.f[2]) - (f[2] * vec.f[1]),
		(f[2] * vec.f[0]) - (f[0] * vec.f[2]),
		(f[0] * vec.f[1]) - (f[1] * vec.f[0])
	);
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::convertToRadians() const
{
	return Vector
	(
		MathHelper::convertToRadians(f[0]),
		MathHelper::convertToRadians(f[1]),
		MathHelper::convertToRadians(f[2])
	);
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::convertToDegrees() const
{
	return Vector
	(
		MathHelper::convertToDegrees(f[0]),
		MathHelper::convertToDegrees(f[1]),
		MathHelper::convertToDegrees(f[2])
	);
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::normalize() const
{
	const float mag = getMagnitude();
	if (MathHelper::compareFloats(mag, 0.0f))
	{
		return GLOWE::NoSSE::Vector<float, 3>(0.0f, 0.0f, 0.0f);
	}
	return GLOWE::NoSSE::Vector<float, 3>(f[0] / mag, f[1] / mag, f[2] / mag);
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::normalizeFast() const
{
	return *this / getMagnitude();
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::abs() const
{
	return NoSSE::Vector<float, 3>(std::fabs(f[0]), std::fabs(f[1]), std::fabs(f[2]));
}

inline GLOWE::NoSSE::Vector<float, 3> GLOWE::NoSSE::Vector<float, 3>::clamp(const Vector& min, const Vector& max) const
{
	return NoSSE::Vector<float, 3>(GLOWE::clamp(f[0], min.f[0], max.f[0]), GLOWE::clamp(f[1], min.f[1], max.f[1]), GLOWE::clamp(f[2], min.f[2], max.f[2]));
}

inline bool GLOWE::NoSSE::Vector<float, 3>::checkIsNormalized() const
{
	return fabs(getMagnitude() - 1.0f) <= MathHelper::Epsilon;
}

inline bool GLOWE::NoSSE::Vector<float, 3>::operator==(const Vector & right) const
{
	using namespace GLOWE::MathHelper;
	return compareFloats(f[0], right.f[0]) && compareFloats(f[1], right.f[1]) && compareFloats(f[2], right.f[2]);
}

inline bool GLOWE::NoSSE::Vector<float, 3>::operator<(const Vector & right) const
{
	return getMagnitude()<right.getMagnitude();
}

inline bool GLOWE::NoSSE::Vector<float, 3>::operator>(const Vector & right) const
{
	return getMagnitude()>right.getMagnitude();
}

inline float GLOWE::NoSSE::Vector<float, 3>::operator[](const unsigned int pos) const
{
	return f[pos];
}

inline float GLOWE::NoSSE::Vector<float, 3>::getX() const
{
	return f[0];
}

inline float GLOWE::NoSSE::Vector<float, 3>::getY() const
{
	return f[1];
}

inline float GLOWE::NoSSE::Vector<float, 3>::getZ() const
{
	return f[2];
}

inline void GLOWE::NoSSE::Vector<float, 3>::setX(const float arg)
{
	f[0] = arg;
}

inline void GLOWE::NoSSE::Vector<float, 3>::setY(const float arg)
{
	f[1] = arg;
}

inline void GLOWE::NoSSE::Vector<float, 3>::setZ(const float arg)
{
	f[2] = arg;
}

template<class Func>
inline void GLOWE::NoSSE::Vector<float, 3>::applyFunction(Func&& func)
{
	for (unsigned int x = 0; x < 3; ++x)
	{
		f[x] = func(f[x]);
	}
}

//NoSSE::Vector4F

GLOWE::NoSSE::Vector<float, 4>::Vector()
	: f{ 0.0f, 0.0f, 0.0f, 0.0f }
{
}

GLOWE::NoSSE::Vector<float, 4>::Vector(const float a, const float b, const float c, const float d)
	: f{ a, b, c, d }
{
}

GLOWE::NoSSE::Vector<float, 4>::Vector(const float abcd)
	: f{ abcd, abcd, abcd, abcd }
{
}

GLOWE::NoSSE::Vector<float, 4>::Vector(const float abcd[4])
	: f{ abcd[0], abcd[1], abcd[2], abcd[3] }
{
}

inline GLOWE::NoSSE::Vector<float, 4>::Vector(const Float4 & abcd)
	: f{ abcd[0], abcd[1], abcd[2], abcd[3] }
{
}

inline GLOWE::NoSSE::Vector<float, 4>::operator GLOWE::Float4() const
{
	return Float4{ f[0], f[1], f[2], f[3] };
}

inline GLOWE::NoSSE::Vector<float, 4> GLOWE::NoSSE::Vector<float, 4>::operator-() const
{
	return Vector(f[0] * (-1.0f), f[1] * (-1.0f), f[2] * (-1.0f), f[3] * (-1.0f));
}

inline GLOWE::NoSSE::Vector<float, 4> GLOWE::NoSSE::Vector<float, 4>::operator+(const Vector & right) const
{
	return Vector
	(
		f[0] + right.f[0],
		f[1] + right.f[1],
		f[2] + right.f[2],
		f[3] + right.f[3]
	);
}

inline GLOWE::NoSSE::Vector<float, 4> GLOWE::NoSSE::Vector<float, 4>::operator-(const Vector & right) const
{
	return Vector
	(
		f[0] - right.f[0],
		f[1] - right.f[1],
		f[2] - right.f[2],
		f[3] - right.f[3]
	);
}

inline GLOWE::NoSSE::Vector<float, 4> GLOWE::NoSSE::Vector<float, 4>::operator*(const Vector & right) const
{
	return Vector
	(
		f[0] * right.f[0],
		f[1] * right.f[1],
		f[2] * right.f[2],
		f[3] * right.f[3]
	);
}

inline GLOWE::NoSSE::Vector<float, 4> GLOWE::NoSSE::Vector<float, 4>::operator/(const Vector & right) const
{
	return Vector
	(
		f[0] / right.f[0],
		f[1] / right.f[1],
		f[2] / right.f[2],
		f[3] / right.f[3]
	);
}

inline GLOWE::NoSSE::Vector<float, 4> GLOWE::NoSSE::Vector<float, 4>::operator+(const float right) const
{
	return Vector
	(
		f[0] + right,
		f[1] + right,
		f[2] + right,
		f[3] + right
	);
}

inline GLOWE::NoSSE::Vector<float, 4> GLOWE::NoSSE::Vector<float, 4>::operator-(const float right) const
{
	return Vector
	(
		f[0] - right,
		f[1] - right,
		f[2] - right,
		f[3] - right
	);
}

inline GLOWE::NoSSE::Vector<float, 4> GLOWE::NoSSE::Vector<float, 4>::operator*(const float right) const
{
	return Vector
	(
		f[0] * right,
		f[1] * right,
		f[2] * right,
		f[3] * right
	);
}

inline GLOWE::NoSSE::Vector<float, 4> GLOWE::NoSSE::Vector<float, 4>::operator/(const float right) const
{
	return Vector
	(
		f[0] / right,
		f[1] / right,
		f[2] / right,
		f[3] / right
	);
}

inline GLOWE::NoSSE::Vector<float, 4> & GLOWE::NoSSE::Vector<float, 4>::operator+=(const Vector & right)
{
	f[0] += right.f[0];
	f[1] += right.f[1];
	f[2] += right.f[2];
	f[3] += right.f[3];

	return *this;
}

inline GLOWE::NoSSE::Vector<float, 4> & GLOWE::NoSSE::Vector<float, 4>::operator-=(const Vector & right)
{
	f[0] -= right.f[0];
	f[1] -= right.f[1];
	f[2] -= right.f[2];
	f[3] -= right.f[3];

	return *this;
}

inline GLOWE::NoSSE::Vector<float, 4> & GLOWE::NoSSE::Vector<float, 4>::operator*=(const Vector & right)
{
	f[0] *= right.f[0];
	f[1] *= right.f[1];
	f[2] *= right.f[2];
	f[3] *= right.f[3];

	return *this;
}

inline GLOWE::NoSSE::Vector<float, 4> & GLOWE::NoSSE::Vector<float, 4>::operator/=(const Vector & right)
{
	f[0] /= right.f[0];
	f[1] /= right.f[1];
	f[2] /= right.f[2];
	f[3] /= right.f[3];

	return *this;
}

inline GLOWE::NoSSE::Vector<float, 4> & GLOWE::NoSSE::Vector<float, 4>::operator+=(const float right)
{
	f[0] += right;
	f[1] += right;
	f[2] += right;
	f[3] += right;

	return *this;
}

inline GLOWE::NoSSE::Vector<float, 4> & GLOWE::NoSSE::Vector<float, 4>::operator-=(const float right)
{
	f[0] -= right;
	f[1] -= right;
	f[2] -= right;
	f[3] -= right;

	return *this;
}

inline GLOWE::NoSSE::Vector<float, 4> & GLOWE::NoSSE::Vector<float, 4>::operator*=(const float right)
{
	f[0] *= right;
	f[1] *= right;
	f[2] *= right;
	f[3] *= right;

	return *this;
}

inline GLOWE::NoSSE::Vector<float, 4> & GLOWE::NoSSE::Vector<float, 4>::operator/=(const float right)
{
	f[0] /= right;
	f[1] /= right;
	f[2] /= right;
	f[3] /= right;

	return *this;
}

inline float GLOWE::NoSSE::Vector<float, 4>::getMagnitude() const
{
	return sqrtf(getScalarProduct());
}

inline float GLOWE::NoSSE::Vector<float, 4>::getMagnitudeSquared() const
{
	return getScalarProduct();
}

inline float GLOWE::NoSSE::Vector<float, 4>::getScalarProduct() const
{
	return (f[0] * f[0]) + (f[1] * f[1]) + (f[2] * f[2]) + (f[3] * f[3]);
}

inline float GLOWE::NoSSE::Vector<float, 4>::scalarProduct(const Vector & vec) const
{
	return (f[0] * vec.f[0]) + (f[1] * vec.f[1]) + (f[2] * vec.f[2]) + (f[3] * vec.f[3]);
}

inline GLOWE::NoSSE::Vector<float, 4> GLOWE::NoSSE::Vector<float, 4>::convertToRadians() const
{
	return Vector
	(
		MathHelper::convertToRadians(f[0]),
		MathHelper::convertToRadians(f[1]),
		MathHelper::convertToRadians(f[2]),
		MathHelper::convertToRadians(f[3])
	);
}

inline GLOWE::NoSSE::Vector<float, 4> GLOWE::NoSSE::Vector<float, 4>::convertToDegrees() const
{
	return Vector
	(
		MathHelper::convertToDegrees(f[0]),
		MathHelper::convertToDegrees(f[1]),
		MathHelper::convertToDegrees(f[2]),
		MathHelper::convertToDegrees(f[3])
	);
}

inline GLOWE::NoSSE::Vector<float, 4> GLOWE::NoSSE::Vector<float, 4>::normalize() const
{
	const float mag = getMagnitude();
	if (MathHelper::compareFloats(mag, 0.0f))
	{
		return GLOWE::NoSSE::Vector<float, 4>(0.0f, 0.0f, 0.0f, 0.0f);
	}
	return GLOWE::NoSSE::Vector<float, 4>(f[0] / mag, f[1] / mag, f[2] / mag, f[3] / mag);
}

inline GLOWE::NoSSE::Vector<float, 4> GLOWE::NoSSE::Vector<float, 4>::normalizeFast() const
{
	return *this / getMagnitude();
}

inline GLOWE::NoSSE::Vector<float, 4> GLOWE::NoSSE::Vector<float, 4>::abs() const
{
	return NoSSE::Vector<float, 4>(std::fabs(f[0]), std::fabs(f[1]), std::fabs(f[2]), std::fabs(f[3]));
}

inline GLOWE::NoSSE::Vector<float, 4> GLOWE::NoSSE::Vector<float, 4>::clamp(const Vector& min, const Vector& max) const
{
	return NoSSE::Vector<float, 4>(GLOWE::clamp(f[0], min.f[0], max.f[0]), GLOWE::clamp(f[1], min.f[1], max.f[1]), GLOWE::clamp(f[2], min.f[2], max.f[2]), GLOWE::clamp(f[3], min.f[3], max.f[3]));
}

inline bool GLOWE::NoSSE::Vector<float, 4>::checkIsNormalized() const
{
	return fabs(getMagnitude() - 1.0f) <= MathHelper::Epsilon;
}

inline bool GLOWE::NoSSE::Vector<float, 4>::operator==(const Vector & right) const
{
	using namespace GLOWE::MathHelper;
	return compareFloats(f[0], right.f[0]) && compareFloats(f[1], right.f[1]) && compareFloats(f[2], right.f[2]) && compareFloats(f[3], right.f[3]);
}

inline bool GLOWE::NoSSE::Vector<float, 4>::operator<(const Vector & right) const
{
	return getMagnitude()<right.getMagnitude();
}

inline bool GLOWE::NoSSE::Vector<float, 4>::operator>(const Vector & right) const
{
	return getMagnitude()>right.getMagnitude();
}

inline float GLOWE::NoSSE::Vector<float, 4>::operator[](const unsigned int pos) const
{
	return f[pos];
}

inline float GLOWE::NoSSE::Vector<float, 4>::getX() const
{
	return f[0];
}

inline float GLOWE::NoSSE::Vector<float, 4>::getY() const
{
	return f[1];
}

inline float GLOWE::NoSSE::Vector<float, 4>::getZ() const
{
	return f[2];
}

inline float GLOWE::NoSSE::Vector<float, 4>::getW() const
{
	return f[3];
}

inline void GLOWE::NoSSE::Vector<float, 4>::setX(const float arg)
{
	f[0] = arg;
}

inline void GLOWE::NoSSE::Vector<float, 4>::setY(const float arg)
{
	f[1] = arg;
}

inline void GLOWE::NoSSE::Vector<float, 4>::setZ(const float arg)
{
	f[2] = arg;
}

inline void GLOWE::NoSSE::Vector<float, 4>::setW(const float arg)
{
	f[3] = arg;
}

template<class Func>
inline void GLOWE::NoSSE::Vector<float, 4>::applyFunction(Func&& func)
{
	for (unsigned int x = 0; x < 4; ++x)
	{
		f[x] = func(f[x]);
	}
}

#endif
