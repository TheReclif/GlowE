#include "Frustum.h"
#include "Sphere.h"
#include "Cone.h"

void GLOWE::Frustum::fromMatrix(const Matrix4x4& matrix)
{
#if GLOWE_USED_SSE > GLOWE_NO_SSE
	//Matrix4x4 columnMatrix = matrix.transpose();
	Vector4F r1 = matrix.getRow(0),
		r2 = matrix.getRow(1),
		r3 = matrix.getRow(2),
		r4 = matrix.getRow(3);
#else
	Vector4F r1(matrix(0, 0), matrix(1, 0), matrix(2, 0), matrix(3, 0)),
		r2(matrix(0, 1), matrix(1, 1), matrix(2, 1), matrix(3, 1)),
		r3(matrix(0, 2), matrix(1, 2), matrix(2, 2), matrix(3, 2)),
		r4(matrix(0, 3), matrix(1, 3), matrix(2, 3), matrix(3, 3));
#endif
	
	planes[0] = Plane((r4 + r1).toFloat4()).normalize();
	planes[1] = Plane((r4 - r1).toFloat4()).normalize();
	planes[2] = Plane((r4 + r2).toFloat4()).normalize();
	planes[3] = Plane((r4 - r2).toFloat4()).normalize();
	planes[4] = Plane((r4 + r3).toFloat4()).normalize();
	planes[5] = Plane((r4 - r3).toFloat4()).normalize();
}

void GLOWE::Frustum::fromPlanes(const Plane newPlanes[6])
{
	std::memcpy(planes, newPlanes, sizeof(Plane) * 6);
}

bool GLOWE::Frustum::isPointInside(const Vector3F& point) const
{
	for (unsigned int x = 0; x < 6; ++x)
	{
		if (!planes[x].isPointInside(point))
		{
			return false;
		}
	}

	return true;
}

bool GLOWE::Frustum::isAABBInside(const AABB& aabb) const
{
	for (unsigned int x = 0; x < 6; ++x)
	{
		if (!planes[x].isAABBInside(aabb))
		{
			return false;
		}
	}

	return true;
}

bool GLOWE::Frustum::isSphereInside(const Sphere& sphere) const
{
	return sphere.isInsideFrustum(*this);
}

bool GLOWE::Frustum::isConeInside(const Cone& cone) const
{
	return cone.isInsideFrustum(*this);
}
