add_library(GlowMath SHARED "")
target_link_libraries(GlowMath PUBLIC GlowSystem)
generate_export_header(GlowMath)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/glowmath_export.h DESTINATION include/GlowMath)
target_include_directories(GlowMath
		PUBLIC
			$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
		)
target_sources(GlowMath
	PRIVATE
		AxisAlignedBoundingBox.cpp
		Cone.cpp
		Frustum.cpp
		Math.cpp
		NoSSECamera.cpp
		NoSSEMatrixClasses.cpp
		NoSSEQuaternionClasses.cpp
		NoSSEVectorClasses.cpp
		Plane.cpp
		Rect.cpp
		RotationMatrix.cpp
		Sphere.cpp
		SSE1Camera.cpp
		Transform.cpp
		NoSSEMatrixClasses.inl
		NoSSEQuaternionClasses.inl
		NoSSERotationMatrix.inl
		NoSSEVectorClasses.inl
		SSE1MatrixClasses.inl
		SSE1QuaternionClasses.inl
		SSE1RotationMatrix.inl
		SSE1VectorClasses.inl
		SSE3VectorClasses.inl
		SSE4VectorClasses.inl
		SSEHelpers.inl
#	PUBLIC
#		AxisAlignedBoundingBox.h
#		CameraMath.h
#		Cone.h
#		Frustum.h
#		Math.h
#		MatrixClasses.h
#		NoSSECamera.h
#		NoSSEMatrixClasses.h
#		NoSSEQuaternionClasses.h
#		NoSSEVectorClasses.h
#		Plane.h
#		QuaternionClasses.h
#		Rect.h
#		RotationMatrix.h
#		Sphere.h
#		SSE1Camera.h
#		SSE1MatrixClasses.h
#		SSE1QuaternionClasses.h
#		SSE1VectorClasses.h
#		SSE3VectorClasses.h
#		SSE4VectorClasses.h
#		SSEHelpers.h
#		Transform.h
#		VectorClasses.h
)
install(DIRECTORY "." DESTINATION include/GlowMath FILES_MATCHING PATTERN "*.h" PATTERN "*.inl")
