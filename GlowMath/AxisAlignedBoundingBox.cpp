#include "AxisAlignedBoundingBox.h"
#include "Sphere.h"
#include "Cone.h"

void GLOWE::AABB::fromVertices(const void* vertices, const unsigned int vertexStructureSize, const unsigned int verticesCount)
{
	Float3 min = { 0.0f, 0.0f, 0.0f }, max = { 0.0f, 0.0f, 0.0f };

	for (unsigned int x = 0; x < verticesCount; ++x)
	{
		const Float3& vertex = *reinterpret_cast<const Float3*>(static_cast<const char*>(vertices) + (vertexStructureSize * x));

		for (unsigned int y = 0; y < 3; ++y)
		{
			if (vertex[y] > max[y])
			{
				max[y] = vertex[y];
			}
			else
			if (vertex[y] < min[y])
			{
				min[y] = vertex[y];
			}
		}
	}

	center = ((Vector3F(max) + Vector3F(min)) * 0.5f).toFloat3();
	halfSize = ((Vector3F(max) - Vector3F(min)) * 0.5f).toFloat3();
}

void GLOWE::AABB::fromIndices(const unsigned int* indicesArray, const unsigned int indicesCount, const void* verticesArray, const unsigned int vertexStructureSize, const unsigned int verticesCount)
{
	Float3 min = { 0.0f, 0.0f, 0.0f }, max = { 0.0f, 0.0f, 0.0f };

	for (unsigned int x = 0; x < indicesCount; ++x)
	{
		const Float3& vertex = *reinterpret_cast<const Float3*>(static_cast<const char*>(verticesArray) + (vertexStructureSize * indicesArray[x]));

		for (unsigned int y = 0; y < 3; ++y)
		{
			if (vertex[y] > max[y])
			{
				max[y] = vertex[y];
			}
			else
			if (vertex[y] < min[y])
			{
				min[y] = vertex[y];
			}
		}
	}

	center = ((Vector3F(max) + Vector3F(min)) * 0.5f).toFloat3();
	halfSize = ((Vector3F(max) - Vector3F(min)) * 0.5f).toFloat3();
}

void GLOWE::AABB::fromIndices(const unsigned short* indicesArray, const unsigned int indicesCount, const void* verticesArray, const unsigned int vertexStructureSize, const unsigned int verticesCount)
{
	Float3 min = { 0.0f, 0.0f, 0.0f }, max = { 0.0f, 0.0f, 0.0f };

	for (unsigned int x = 0; x < indicesCount; ++x)
	{
		const Float3& vertex = *reinterpret_cast<const Float3*>(static_cast<const char*>(verticesArray) + (vertexStructureSize * indicesArray[x]));

		for (unsigned int y = 0; y < 3; ++y)
		{
			if (vertex[y] > max[y])
			{
				max[y] = vertex[y];
			}
			else
			if (vertex[y] < min[y])
			{
				min[y] = vertex[y];
			}
		}
	}

	center = ((Vector3F(max) + Vector3F(min)) * 0.5f).toFloat3();
	halfSize = ((Vector3F(max) - Vector3F(min)) * 0.5f).toFloat3();
}

bool GLOWE::AABB::intersects(const Sphere& sphere) const
{
	return sphere.isPointInside(getClosestPoint(sphere.center));
}

bool GLOWE::AABB::intersects(const AABB& aabb) const
{
#if GLOWE_USED_SSE > GLOWE_NO_SSE
	Vector3F aSubH = Vector3F(center) - halfSize, aAddH = Vector3F(center) + halfSize;
	Vector3F bSubH = Vector3F(aabb.center) - aabb.halfSize, bAddH = Vector3F(aabb.center) + aabb.halfSize;

	return _mm_movemask_ps(_mm_and_ps(_mm_cmple_ps(aSubH.m, bAddH.m), _mm_cmpge_ps(aAddH.m, bSubH.m))) == 0x7;
#else
	Vector3F min = Vector3F(center) - halfSize, max = Vector3F(center) + halfSize;
	Vector3F bMin = Vector3F(aabb.center) - aabb.halfSize, bMax = Vector3F(aabb.center) + aabb.halfSize;

	return (min.getX() <= bMax.getX() && max.getX() >= min.getX()) &&
		(min.getY() <= bMax.getY() && max.getY() >= min.getY()) &&
		(min.getZ() <= bMax.getZ() && max.getZ() >= min.getZ());
#endif
}

// TODO: Test. Probably not working properly.
bool GLOWE::AABB::intersectsLine(const Vector3F& lineStart, const Vector3F& lineDirection) const
{
	const Vector3F vecProd = lineDirection.vectorProduct(lineStart - center).abs(), D = lineDirection.abs();

	if (vecProd.getX() > halfSize[1] * D.getZ() + halfSize[2] * D.getY())
	{
		return false;
	}
	if (vecProd.getY() > halfSize[0] * D.getZ() + halfSize[2] * D.getX())
	{
		return false;
	}
	if (vecProd.getZ() > halfSize[1] * D.getX() + halfSize[0] * D.getY())
	{
		return false;
	}
	return true;
}

// TODO: Test. Probably not working properly.
bool GLOWE::AABB::intersectsSegment(const Vector3F& segmentStart, const Vector3F& segmentEnd) const
{
	const Vector3F segCenter = ((segmentEnd + segmentStart) / 2.0f) - center;
	Vector3F dir = segmentEnd - segmentStart;
	float distance = dir.getMagnitude();
	dir /= distance;
	distance /= 2.0f;

	const Vector3F absP = segCenter.abs(), transformedDir = dir.abs() * distance, tempVec = transformedDir + halfSize;
#if GLOWE_USED_SSE > GLOWE_NO_SSE
	int res = _mm_movemask_ps(_mm_cmpgt_ps(absP.m, tempVec.m));
	if (res)
	{
		return false;
	}
#else
	if (absP.getX() > tempVec.getX() || absP.getY() > tempVec.getY() || absP.getZ() > tempVec.getZ())
	{
		return false;
	}
#endif

	return intersectsLine(segCenter, dir);
}

GLOWE::Vector3F GLOWE::AABB::getClosestPoint(const Vector3F& point) const
{
	return point.clamp(Vector3F(center) - halfSize, Vector3F(center) + halfSize);
}

GLOWE::Vector3F GLOWE::AABB::getVectorToSphere(const Sphere& sphere) const
{
	return Vector3F(sphere.center) - getClosestPoint(sphere.center);
}

GLOWE::Array<GLOWE::Float3, 8> GLOWE::AABB::getVertices() const
{
	Array<Float3, 8> result;

	Vector3F vecCenter = center, vecHalfize = halfSize;
	Vector3F firstOpt = { -halfSize[0], halfSize[1], halfSize[2] };
	Vector3F secondOpt = { halfSize[0], halfSize[1], -halfSize[2] };
	Vector3F thirdOpt = { -halfSize[0], halfSize[1], -halfSize[2] };

	// Upper part.
	result[0] = (vecCenter + vecHalfize).toFloat3();
	result[1] = (vecCenter + firstOpt).toFloat3();
	result[2] = (vecCenter + secondOpt).toFloat3();
	result[3] = (vecCenter + thirdOpt).toFloat3();

	// Bottom part.
	result[4] = (vecCenter - vecHalfize).toFloat3();
	result[5] = (vecCenter - firstOpt).toFloat3();
	result[6] = (vecCenter - secondOpt).toFloat3();
	result[7] = (vecCenter - thirdOpt).toFloat3();

	return result;
}

GLOWE::AABB GLOWE::AABB::transform(const Matrix4x4& mat4x4) const
{
	const Array<Float3, 8> corners = getVertices();
	Float3 min = { 0.0f, 0.0f, 0.0f }, max = { 0.0f, 0.0f, 0.0f };

	for (const auto& corner : corners)
	{
		const Vector4F transCorner = mat4x4 * Vector4F(corner[0], corner[1], corner[2], 1.0f);
		for (unsigned int x = 0; x < 3; ++x)
		{
			if (transCorner[x] > max[x])
			{
				max[x] = transCorner[x];
			}
			else
			if (transCorner[x] < min[x])
			{
				min[x] = transCorner[x];
			}
		}
	}

	Vector3F minVec = min, maxVec = max;

	return AABB(((maxVec + minVec) * 0.5f).toFloat3(), ((maxVec - minVec) * 0.5f).toFloat3());
}
