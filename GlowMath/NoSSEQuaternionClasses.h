#pragma once
#ifndef GLOWE_NOSSE_QUATERNIONCLASSES_INCLUDED
#define GLOWE_NOSSE_QUATERNIONCLASSES_INCLUDED

#include "../GlowSystem/NumberStorage.h"
#include "VectorClasses.h"
#include "MatrixClasses.h"

namespace GLOWE
{
	namespace NoSSE
	{
		class GLOWMATH_EXPORT Quaternion
		{
		private:
			//w is first (!)
			union
			{
				float f[4];
				struct
				{
					float w, x, y, z;
				};
			};
		public:
			inline Quaternion();
			inline Quaternion(const Quaternion& arg);
			inline Quaternion(Quaternion&& arg) noexcept;
			inline Quaternion(const float arg);
			inline Quaternion(const Float4& arg);
			inline Quaternion(const float arg[4]);
			inline Quaternion(const float a, const float b, const float c, const float d);

			Quaternion& operator=(const Quaternion&) = default;

			inline Float4 toFloat4() const;

			inline float& operator[](const unsigned short id);
			inline float operator[](const unsigned short id) const;

			inline Quaternion operator+(const Quaternion& right) const;
			inline Quaternion operator-(const Quaternion& right) const;
			inline Quaternion operator*(const Quaternion& right) const;
			inline Quaternion operator/(const Quaternion& right) const;

			inline Quaternion operator+(const float right) const;
			inline Quaternion operator-(const float right) const;
			inline Quaternion operator*(const float right) const;
			inline Quaternion operator/(const float right) const;

			inline float norm() const;
			inline float modulus() const;

			inline Quaternion inverse() const;
			inline Quaternion conjugate() const;
			inline Quaternion normalize() const;

			// Returns: Vector3F(roll, pitch, yaw)
			inline GLOWE::Vector3F asEulerAngles() const;
			inline GLOWE::Vector4F asAxisAngle() const;
			inline GLOWE::Matrix3x3 asRotationMatrix() const;

			inline GLOWE::Vector3F transformPoint(const GLOWE::Vector3F& vec) const;

			static inline Quaternion lookAt(const GLOWE::Vector3F& dir, const GLOWE::Vector3F& up = VecHelpers::up, const GLOWE::Vector3F& forward = VecHelpers::forward);
			
			static inline const Quaternion getIdentity();

			//Argument: Vector3F(roll, pitch, yaw)
			static inline Quaternion fromEulerAngles(const GLOWE::Vector3F& angles);
			static inline Quaternion fromAxisAngle(const GLOWE::Vector4F& angles); // Angle, axis.x, axis.y, axis.z
			static inline Quaternion fromAxisAngle(const float angle, const GLOWE::Vector3F& axis);
			static inline Quaternion fromRotationMatrix(const GLOWE::Matrix3x3& mat);
		};

#include "NoSSEQuaternionClasses.inl"
	}
}

#endif