#pragma once
#ifndef GLOWE_MATH_FRUSTUM_INCLUDED
#define GLOWE_MATH_FRUSTUM_INCLUDED

#include "Plane.h"
#include "MatrixClasses.h"

namespace GLOWE
{
	class AABB;
	class Cone;
	class Sphere;

	class GLOWMATH_EXPORT alignas(16) Frustum
	{
	public:
		// Check here: https://www.gamedevs.org/uploads/fast-extraction-viewing-frustum-planes-from-world-view-projection-matrix.pdf.
		// Order of planes (assuming the fromMatrix method was called, either manually or by ctor):
		// 0: Left.
		// 1: Right.
		// 2: Bottom.
		// 3: Top.
		// 4: Near.
		// 5: Far.
		Plane planes[6];
	public:
		inline Frustum()
			: planes{ Plane(), Plane(), Plane(), Plane(), Plane(), Plane() }
		{}
		inline Frustum(const Plane newPlanes[6])
		{
			fromPlanes(newPlanes);
		}
		inline Frustum(const Matrix4x4& matrix)
		{
			fromMatrix(matrix);
		}

		// For a projection matrix produces a clip space frustum.
		// For a projection-view matrix produces a world space frustum.
		void fromMatrix(const Matrix4x4& matrix);
		void fromPlanes(const Plane newPlanes[6]);

		bool isPointInside(const Vector3F& point) const;
		bool isAABBInside(const AABB& aabb) const;
		bool isSphereInside(const Sphere& sphere) const;
		bool isConeInside(const Cone& cone) const;
	};
}

#endif
