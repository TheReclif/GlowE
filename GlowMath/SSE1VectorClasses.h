#pragma once
#ifndef GLOWE_SSE1VECTORCLASSES_INCLUDED
#define GLOWE_SSE1VECTORCLASSES_INCLUDED

#include "Math.h"
#include "SSEHelpers.h"

#if GLOWE_USED_SSE >= GLOWE_SSE_1
namespace GLOWE
{
	namespace SSE1
	{
		class GLOWMATH_EXPORT alignas(16) VectorBase
		{
		public:
			__m128 m;

			inline VectorBase();
			inline VectorBase(const VectorBase& arg);
			inline VectorBase(VectorBase&& arg) noexcept;
			inline VectorBase(const __m128 arg);

			inline VectorBase& operator=(const VectorBase& arg);
			inline VectorBase& operator=(VectorBase&& arg) noexcept;

			inline float operator[](const unsigned int arg) const;
			inline float& operator[](const unsigned int arg);
		};

		static_assert(alignof(VectorBase) == 16, "Compiler does not support 16 byte alignment which is required to make use of GLOWE SSE math");

		class GLOWMATH_EXPORT alignas(16) Vector2F
			: public VectorBase
		{
		public:
			inline Vector2F() noexcept;
			inline Vector2F(const Vector2F& arg) noexcept;
			inline Vector2F(Vector2F&& arg) noexcept;
			inline Vector2F(const __m128 arg);
			inline Vector2F(const float a, const float b);
			inline Vector2F(const float ab);
			inline Vector2F(const float ab[2]);
			inline Vector2F(const Float2& ab);
#ifdef GLOWE_COMPILE_FOR_DIRECTX
			inline Vector2F(const ::DirectX::XMFLOAT2& ab);

			inline operator ::DirectX::XMFLOAT2() const;
#endif

			inline Vector2F& operator=(const Vector2F&) = default;
			inline Vector2F& operator=(Vector2F&&) noexcept = default;

			inline operator __m128() const;
			inline Float2 toFloat2() const;

			inline Vector2F operator-() const;

			inline Vector2F operator+(const Vector2F& right) const;
			inline Vector2F operator-(const Vector2F& right) const;
			inline Vector2F operator*(const Vector2F& right) const;
			inline Vector2F operator/(const Vector2F& right) const;

			inline Vector2F operator+(const float right) const;
			inline Vector2F operator-(const float right) const;
			inline Vector2F operator*(const float right) const;
			inline Vector2F operator/(const float right) const;

			inline Vector2F& operator+=(const Vector2F& right);
			inline Vector2F& operator-=(const Vector2F& right);
			inline Vector2F& operator*=(const Vector2F& right);
			inline Vector2F& operator/=(const Vector2F& right);

			inline Vector2F& operator+=(const float right);
			inline Vector2F& operator-=(const float right);
			inline Vector2F& operator*=(const float right);
			inline Vector2F& operator/=(const float right);

			inline float getMagnitude() const;
			inline float getMagnitudeSquared() const;
			inline float getScalarProduct() const;
			inline float scalarProduct(const Vector2F& vec) const;

			inline Vector2F convertToRadians() const;
			inline Vector2F convertToDegrees() const;
			inline Vector2F normalize() const;
			inline Vector2F normalizeFast() const;

			inline Vector2F abs() const;
			inline Vector2F clamp(const Vector2F & min, const Vector2F & max) const;

			inline bool checkIsNormalized() const;

			inline bool operator==(const Vector2F& right) const;
			inline bool operator<(const Vector2F& right) const;
			inline bool operator>(const Vector2F& right) const;

			inline float getX() const;
			inline float getY() const;

			inline void setX(const float arg);
			inline void setY(const float arg);
		};

		class GLOWMATH_EXPORT alignas(16) Vector3F
			: public VectorBase
		{
		public:
			inline Vector3F() noexcept;
			inline Vector3F(const Vector3F& arg) noexcept;
			inline Vector3F(Vector3F&& arg) noexcept;
			inline Vector3F(const __m128 arg);
			inline Vector3F(const float a, const float b, const float c);
			inline Vector3F(const float abc);
			inline Vector3F(const float abc[3]);
			inline Vector3F(const Float3& abc);
#ifdef GLOWE_COMPILE_FOR_DIRECTX
			inline Vector3F(const ::DirectX::XMFLOAT3& abc);

			inline operator ::DirectX::XMFLOAT3() const;
#endif

			inline Vector3F& operator=(const Vector3F&) = default;
			inline Vector3F& operator=(Vector3F&&) noexcept = default;

			inline operator __m128() const;
			inline Float3 toFloat3() const;

			inline Vector3F operator-() const;

			inline Vector3F operator+(const Vector3F& right) const;
			inline Vector3F operator-(const Vector3F& right) const;
			inline Vector3F operator*(const Vector3F& right) const;
			inline Vector3F operator/(const Vector3F& right) const;

			inline Vector3F operator+(const float right) const;
			inline Vector3F operator-(const float right) const;
			inline Vector3F operator*(const float right) const;
			inline Vector3F operator/(const float right) const;

			inline Vector3F& operator+=(const Vector3F& right);
			inline Vector3F& operator-=(const Vector3F& right);
			inline Vector3F& operator*=(const Vector3F& right);
			inline Vector3F& operator/=(const Vector3F& right);

			inline Vector3F& operator+=(const float right);
			inline Vector3F& operator-=(const float right);
			inline Vector3F& operator*=(const float right);
			inline Vector3F& operator/=(const float right);

			inline float getMagnitude() const;
			inline float getMagnitudeSquared() const;
			inline float getScalarProduct() const;
			inline float scalarProduct(const Vector3F& vec) const;

			inline Vector3F vectorProduct(const Vector3F& vec) const;

			inline Vector3F convertToRadians() const;
			inline Vector3F convertToDegrees() const;
			inline Vector3F normalize() const;
			inline Vector3F normalizeFast() const;

			inline Vector3F abs() const;
			inline Vector3F clamp(const Vector3F & min, const Vector3F & max) const;

			inline bool checkIsNormalized() const;

			inline bool operator==(const Vector3F& right) const;
			inline bool operator<(const Vector3F& right) const;
			inline bool operator>(const Vector3F& right) const;

			inline float getX() const;
			inline float getY() const;
			inline float getZ() const;

			inline void setX(const float arg);
			inline void setY(const float arg);
			inline void setZ(const float arg);
		};

		class GLOWMATH_EXPORT alignas(16) Vector4F
			: public VectorBase
		{
		public:
			inline Vector4F() noexcept;
			inline Vector4F(const Vector4F& arg) noexcept;
			inline Vector4F(Vector4F&& arg) noexcept;
			inline Vector4F(const __m128 arg);
			inline Vector4F(const float a, const float b, const float c, const float d);
			inline Vector4F(const float abcd);
			inline Vector4F(const float abcd[4]);
			inline Vector4F(const Float4& abcd);
#ifdef GLOWE_COMPILE_FOR_DIRECTX
			inline Vector4F(const ::DirectX::XMFLOAT4& abcd);

			inline operator ::DirectX::XMFLOAT4() const;
#endif

			inline Vector4F& operator=(const Vector4F&) = default;
			inline Vector4F& operator=(Vector4F&&) noexcept = default;

			inline operator __m128() const;
			inline Float4 toFloat4() const;

			inline Vector4F operator-() const;

			inline Vector4F operator+(const Vector4F& right) const;
			inline Vector4F operator-(const Vector4F& right) const;
			inline Vector4F operator*(const Vector4F& right) const;
			inline Vector4F operator/(const Vector4F& right) const;

			inline Vector4F operator+(const float right) const;
			inline Vector4F operator-(const float right) const;
			inline Vector4F operator*(const float right) const;
			inline Vector4F operator/(const float right) const;

			inline Vector4F& operator+=(const Vector4F& right);
			inline Vector4F& operator-=(const Vector4F& right);
			inline Vector4F& operator*=(const Vector4F& right);
			inline Vector4F& operator/=(const Vector4F& right);

			inline Vector4F& operator+=(const float right);
			inline Vector4F& operator-=(const float right);
			inline Vector4F& operator*=(const float right);
			inline Vector4F& operator/=(const float right);

			inline float getMagnitude() const;
			inline float getMagnitudeSquared() const;
			inline float getScalarProduct() const;
			inline float scalarProduct(const Vector4F& vec) const;

			inline Vector4F convertToRadians() const;
			inline Vector4F convertToDegrees() const;
			inline Vector4F normalize() const;
			inline Vector4F normalizeFast() const;

			inline Vector4F abs() const;
			inline Vector4F clamp(const Vector4F & min, const Vector4F & max) const;

			inline bool checkIsNormalized() const;

			inline bool operator==(const Vector4F& right) const;
			inline bool operator<(const Vector4F& right) const;
			inline bool operator>(const Vector4F& right) const;

			inline float getX() const;
			inline float getY() const;
			inline float getZ() const;
			inline float getW() const;

			inline void setX(const float arg);
			inline void setY(const float arg);
			inline void setZ(const float arg);
			inline void setW(const float arg);
		};

		inline GLOWE::SSE1::Vector2F operator+(const float s, const GLOWE::SSE1::Vector2F& vec)
		{
			return vec + s;
		}

		inline GLOWE::SSE1::Vector2F operator*(const float s, const GLOWE::SSE1::Vector2F& vec)
		{
			return vec * s;
		}

		inline GLOWE::SSE1::Vector3F operator+(const float s, const GLOWE::SSE1::Vector3F& vec)
		{
			return vec + s;
		}

		inline GLOWE::SSE1::Vector3F operator*(const float s, const GLOWE::SSE1::Vector3F& vec)
		{
			return vec * s;
		}

		inline GLOWE::SSE1::Vector4F operator+(const float s, const GLOWE::SSE1::Vector4F& vec)
		{
			return vec + s;
		}

		inline GLOWE::SSE1::Vector4F operator*(const float s, const GLOWE::SSE1::Vector4F& vec)
		{
			return vec * s;
		}

#include "SSE1VectorClasses.inl"
	}
}

#endif

#endif
