#pragma once
#ifndef GLOWE_SSE3VECTORCLASSES_INCLUDED
#define GLOWE_SSE3VECTORCLASSES_INCLUDED

#include "SSE1VectorClasses.h"

#if GLOWE_USED_SSE >= GLOWE_SSE_3
namespace GLOWE
{
	namespace SSE3
	{
		class GLOWMATH_EXPORT alignas(16) Vector2F :
			public SSE1::Vector2F
		{
		public:
			Vector2F() = default;
			inline Vector2F(const SSE1::Vector2F& arg);
			inline Vector2F(SSE1::Vector2F&& arg);

			inline float getScalarProduct() const;
			inline float scalarProduct(const GLOWE::SSE1::Vector2F& vec) const;

			using GLOWE::SSE1::Vector2F::Vector2F;

			inline Vector2F& operator=(const SSE1::Vector2F& arg);
			inline Vector2F& operator=(SSE1::Vector2F&& arg) noexcept;
		};

		class GLOWMATH_EXPORT alignas(16) Vector3F :
			public SSE1::Vector3F
		{
		public:
			Vector3F() = default;
			inline Vector3F(const SSE1::Vector3F& arg);
			inline Vector3F(SSE1::Vector3F&& arg);

			inline float getScalarProduct() const;
			inline float scalarProduct(const GLOWE::SSE1::Vector3F& vec) const;

			using GLOWE::SSE1::Vector3F::Vector3F;

			inline Vector3F& operator=(const SSE1::Vector3F& arg);
			inline Vector3F& operator=(SSE1::Vector3F&& arg) noexcept;
		};

		class GLOWMATH_EXPORT alignas(16) Vector4F :
			public SSE1::Vector4F
		{
		public:
			Vector4F() = default;
			inline Vector4F(const SSE1::Vector4F & arg);
			inline Vector4F(SSE1::Vector4F && arg);

			inline float getScalarProduct() const;
			inline float scalarProduct(const GLOWE::SSE1::Vector4F & vec) const;

			using GLOWE::SSE1::Vector4F::Vector4F;

			inline Vector4F& operator=(const SSE1::Vector4F & arg);
			inline Vector4F& operator=(SSE1::Vector4F && arg) noexcept;
		};

		inline GLOWE::SSE3::Vector2F operator+(const float s, const GLOWE::SSE3::Vector2F& vec)
		{
			return vec + s;
		}

		inline GLOWE::SSE3::Vector2F operator*(const float s, const GLOWE::SSE3::Vector2F& vec)
		{
			return vec * s;
		}

		inline GLOWE::SSE3::Vector3F operator+(const float s, const GLOWE::SSE3::Vector3F& vec)
		{
			return vec + s;
		}

		inline GLOWE::SSE3::Vector3F operator*(const float s, const GLOWE::SSE3::Vector3F& vec)
		{
			return vec * s;
		}

		inline GLOWE::SSE3::Vector4F operator+(const float s, const GLOWE::SSE3::Vector4F& vec)
		{
			return vec + s;
		}

		inline GLOWE::SSE3::Vector4F operator*(const float s, const GLOWE::SSE3::Vector4F& vec)
		{
			return vec * s;
		}

#include "SSE3VectorClasses.inl"
	}
}

#endif

#endif
