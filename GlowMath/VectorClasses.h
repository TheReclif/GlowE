#pragma once
#ifndef GLOWE_VECTORCLASSES_INCLUDED
#define GLOWE_VECTORCLASSES_INCLUDED

#include "../GlowSystem/Color.h"

#include "Math.h"

#include "NoSSEVectorClasses.h"
#include "SSE1VectorClasses.h"
#include "SSE3VectorClasses.h"
#include "SSE4VectorClasses.h"

namespace GLOWE
{
	template<class Type, unsigned int x>
	using VectorType = NoSSE::Vector<Type, x>;
	template<unsigned int x>
	using VectorF = VectorType<float, x>;

#if GLOWE_USED_SSE == GLOWE_NO_SSE
	using Vector2F = VectorF<2>;
	using Vector3F = VectorF<3>;
	using Vector4F = VectorF<4>;
#elif (GLOWE_USED_SSE == GLOWE_SSE_1) || (GLOWE_USED_SSE == GLOWE_SSE_2)
	using Vector2F = SSE1::Vector2F;
	using Vector3F = SSE1::Vector3F;
	using Vector4F = SSE1::Vector4F;
#elif GLOWE_USED_SSE == GLOWE_SSE_3
	using Vector2F = SSE3::Vector2F;
	using Vector3F = SSE3::Vector3F;
	using Vector4F = SSE3::Vector4F;
#elif GLOWE_USED_SSE >= GLOWE_SSE_4
	using Vector2F = SSE4::Vector2F;
	using Vector3F = SSE4::Vector3F;
	using Vector4F = SSE4::Vector4F;
#else
#error GlowEngine SSE version not defined.
#endif

	namespace VecHelpers
	{
		// LH coordinate system. (TODO: Change to RH).
		static const Float3 up = Float3{ 0.0f, 1.0f, 0.0f };
		static const Float3 down = Float3{ 0.0f, -1.0f, 0.0f };
		static const Float3 left = Float3{ -1.0f, 0.0f, 0.0f };
		static const Float3 right = Float3{ 1.0f, 0.0f, 0.0f };
		static const Float3 forward = Float3{ 0.0f, 0.0f, 1.0f };
		static const Float3 backward = Float3{ 0.0f, 0.0f, -1.0f };
	}

	namespace MathHelper
	{
		template<>
		inline Color lerp(const Color& a, const Color& b, const float t)
		{
			return lerp(Vector4F(a), Vector4F(b), t).toFloat4();
		}
	}
}

#endif