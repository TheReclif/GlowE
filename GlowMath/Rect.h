#pragma once
#ifndef GLOWE_MATH_RECT_INCLUDED
#define GLOWE_MATH_RECT_INCLUDED

#include "Math.h"

#include "../GlowSystem/NumberStorage.h"

namespace GLOWE
{
	class GLOWMATH_EXPORT Rect
	{
	public:
		Int2 topLeft, bottomRight;
	public:
		bool isInside(const Int2& point) const;
		bool isInside(const Float2& point) const; // Will do float-int conversion and call isInside(UInt2).

		bool intersects(const Rect& otherRect) const;
	};
}

#endif
