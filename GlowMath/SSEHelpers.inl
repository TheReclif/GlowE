#pragma once
#ifndef GLOWE_MATH_SSEHELPERS_INL_INCLUDED
#define GLOWE_MATH_SSEHELPERS_INL_INCLUDED

#include "SSEHelpers.h"

#if GLOWE_USED_SSE > GLOWE_NO_SSE
inline unsigned int GLOWE::SSE1::signbit(const __m128 arg)
{
	return _mm_movemask_ps(_mm_and_ps(arg, _mm_set_ps1(-0.0f)));
}

inline __m128 GLOWE::SSE1::setSignbit(const __m128 arg)
{
	return _mm_or_ps(arg, _mm_set_ps1(-0.0f));
}

inline __m128 GLOWE::SSE1::unsetSignbit(const __m128 arg)
{
	constexpr UInt32 u32 = 0x7FFFFFFF;
	const float mask = *reinterpret_cast<const float*>(&u32);
	return _mm_and_ps(arg, _mm_set1_ps(mask));
}

inline __m128 GLOWE::SSE1::flipSignbit(const __m128 arg)
{
	return _mm_xor_ps(arg, _mm_set_ps1(-0.0f));
}

inline __m128 GLOWE::SSE1::sqrt(const __m128 arg)
{
	return _mm_sqrt_ps(arg);
}

inline __m128 GLOWE::SSE1::sin(const __m128 arg)
{
	__m128 tempAngle2 = _mm_mul_ps(arg, arg);
	__m128 tempAngle3 = _mm_mul_ps(arg, tempAngle2);
	
	__m128 temp = _mm_div_ps(tempAngle3, _mm_set_ps1(6.0f));
	__m128 result = _mm_sub_ps(arg, temp);

	temp = _mm_mul_ps(tempAngle2, tempAngle3);
	result = _mm_add_ps(result, _mm_div_ps(temp, _mm_set_ps1(120.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_sub_ps(result, _mm_div_ps(temp, _mm_set_ps1(5040.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_add_ps(result, _mm_div_ps(temp, _mm_set_ps1(362880.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_sub_ps(result, _mm_div_ps(temp, _mm_set_ps1(39916800.0f)));

	return result;
}

inline __m128 GLOWE::SSE1::cos(const __m128 arg)
{
	__m128 tempAngle2 = _mm_mul_ps(arg, arg);
	__m128 tempAngle4 = _mm_mul_ps(tempAngle2, tempAngle2);

	__m128 temp = _mm_div_ps(tempAngle2, _mm_set_ps1(2.0f));
	__m128 result = _mm_set_ps1(1.0f);
	result = _mm_sub_ps(result, temp);

	temp = _mm_div_ps(tempAngle4, _mm_set_ps1(24.0f));
	result = _mm_add_ps(result, temp);

	temp = _mm_mul_ps(tempAngle2, tempAngle4);
	temp = _mm_div_ps(temp, _mm_set_ps1(720.0f));
	result = _mm_sub_ps(result, temp);

	temp = _mm_mul_ps(tempAngle4, tempAngle4);
	result = _mm_add_ps(result, _mm_div_ps(temp, _mm_set_ps1(40320.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_sub_ps(result, _mm_div_ps(temp, _mm_set_ps1(3628800.0f)));

	return result;
}

inline __m128 GLOWE::SSE1::tan(const __m128 arg)
{
	return _mm_div_ps(sin(arg), cos(arg));

	/*
	__m128 tempAngle2 = _mm_mul_ps(arg, arg);
	__m128 tempAngle3 = _mm_mul_ps(tempAngle2, arg);

	__m128 result = _mm_add_ps(arg, _mm_div_ps(tempAngle3, _mm_set_ps1(3.0f)));

	__m128 temp = _mm_mul_ps(tempAngle2, tempAngle3);
	result = _mm_add_ps(result, _mm_mul_ps(temp, _mm_set_ps1(2.0f / 15.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_add_ps(result, _mm_mul_ps(temp, _mm_set_ps1(17.0f / 315.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_add_ps(result, _mm_mul_ps(temp, _mm_set_ps1(62.0f / 2835.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_add_ps(result, _mm_mul_ps(temp, _mm_set_ps1(1382.0f / 155925.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_add_ps(result, _mm_mul_ps(temp, _mm_set_ps1(21844.0f / 6081075.0f)));

	//temp = _mm_mul_ps(temp, tempAngle2);
	//result = _mm_add_ps(result, _mm_mul_ps(temp, _mm_set_ps1(929569.0f / 638512875.0f)));

	return result;
	*/
}

inline __m128 GLOWE::SSE1::ctan(const __m128 arg)
{
	return _mm_div_ps(cos(arg), sin(arg));
}

__m128 GLOWE::SSE1::asin(const __m128 arg)
{
	__m128 tempAngle2 = _mm_mul_ps(arg, arg);
	__m128 tempAngle3 = _mm_mul_ps(tempAngle2, arg);
	__m128 tempAngle5 = _mm_mul_ps(tempAngle2, tempAngle3);

	__m128 temp = _mm_div_ps(tempAngle3, _mm_set_ps1(6.0f));
	__m128 result = _mm_add_ps(arg, temp);

	temp = _mm_mul_ps(tempAngle5, _mm_set_ps1(3.0f / 40.0f));
	result = _mm_add_ps(result, temp);

	temp = _mm_mul_ps(tempAngle5, tempAngle2);
	result = _mm_add_ps(result, _mm_mul_ps(temp, _mm_set_ps1(5.0f / 112.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_add_ps(result, _mm_mul_ps(temp, _mm_set_ps1(35.0f / 1152.0f)));

	// 63 / 2816
	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_add_ps(result, _mm_mul_ps(temp, _mm_set_ps1(63.0f / 2816.0f)));

	// 231 / 13312
	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_add_ps(result, _mm_mul_ps(temp, _mm_set_ps1(231.0f / 13312.0f)));

	/*
	// 143 / 10240
	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_add_ps(result, _mm_mul_ps(temp, _mm_set_ps1(143.0f / 10240.0f)));

	// 6435 / 557056
	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_add_ps(result, _mm_mul_ps(temp, _mm_set_ps1(6435.0f / 557056.0f)));

	// 12155 / 1245184
	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_add_ps(result, _mm_mul_ps(temp, _mm_set_ps1(12155.0f / 1245184.0f)));
	*/

	return result;
}

__m128 GLOWE::SSE1::acos(const __m128 arg)
{
	__m128 tempAngle2 = _mm_mul_ps(arg, arg);
	__m128 tempAngle3 = _mm_mul_ps(tempAngle2, arg);
	__m128 tempAngle5 = _mm_mul_ps(tempAngle2, tempAngle3);

	__m128 result = _mm_sub_ps(_mm_set_ps1(MathHelper::PiDiv2), arg);

	__m128 temp = _mm_div_ps(tempAngle3, _mm_set_ps1(6.0f));
	result = _mm_sub_ps(result, temp);

	temp = _mm_mul_ps(tempAngle5, _mm_set_ps1(3.0f / 40.0f));
	result = _mm_sub_ps(result, temp);

	temp = _mm_mul_ps(tempAngle2, tempAngle5);
	result = _mm_sub_ps(result, _mm_mul_ps(temp, _mm_set_ps1(5.0f / 112.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_sub_ps(result, _mm_mul_ps(temp, _mm_set_ps1(35.0f / 1152.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_sub_ps(result, _mm_mul_ps(temp, _mm_set_ps1(63.0f / 2816.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_sub_ps(result, _mm_mul_ps(temp, _mm_set_ps1(231.0f / 13312.0f)));

	return result;
}

__m128 GLOWE::SSE1::atan(const __m128 arg)
{
	__m128 tempAngle2 = _mm_mul_ps(arg, arg);
	__m128 tempAngle3 = _mm_mul_ps(tempAngle2, arg);
	__m128 tempAngle5 = _mm_mul_ps(tempAngle2, tempAngle3);

	__m128 temp = _mm_div_ps(tempAngle3, _mm_set_ps1(3.0f));
	__m128 result = _mm_sub_ps(arg, temp);

	temp = _mm_div_ps(tempAngle5, _mm_set_ps1(5.0f));
	result = _mm_add_ps(result, temp);

	temp = _mm_mul_ps(tempAngle5, tempAngle2);
	result = _mm_sub_ps(result, _mm_div_ps(temp, _mm_set_ps1(7.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_add_ps(result, _mm_div_ps(temp, _mm_set_ps1(9.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_sub_ps(result, _mm_div_ps(temp, _mm_set_ps1(11.0f)));

	return result;
}

__m128 GLOWE::SSE1::actan(const __m128 arg)
{
	__m128 tempAngle2 = _mm_mul_ps(arg, arg);
	__m128 tempAngle3 = _mm_mul_ps(tempAngle2, arg);
	__m128 tempAngle5 = _mm_mul_ps(tempAngle2, tempAngle3);

	__m128 temp = _mm_div_ps(tempAngle3, _mm_set_ps1(3.0f));
	__m128 result = _mm_sub_ps(_mm_set_ps1(MathHelper::PiDiv2), arg);

	result = _mm_add_ps(result, temp);

	temp = _mm_div_ps(tempAngle5, _mm_set_ps1(5.0f));
	result = _mm_sub_ps(result, temp);

	temp = _mm_mul_ps(tempAngle5, tempAngle2);
	result = _mm_add_ps(result, _mm_div_ps(temp, _mm_set_ps1(7.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_sub_ps(result, _mm_div_ps(temp, _mm_set_ps1(9.0f)));

	temp = _mm_mul_ps(temp, tempAngle2);
	result = _mm_add_ps(result, _mm_div_ps(temp, _mm_set_ps1(11.0f)));

	return result;
	//TODO: Test everything
}

__m128 GLOWE::SSE1::atan2(const __m128 y, const __m128 x)
{
	__m128 temp = atan(_mm_div_ps(y, x));

	unsigned int xLessThanZero = _mm_movemask_ps(_mm_cmplt_ps(x, _mm_setzero_ps()));
	unsigned int xEqualsZero = _mm_movemask_ps(_mm_cmpeq_ps(x, _mm_setzero_ps()));
	unsigned int yGreaterThanZero = _mm_movemask_ps(_mm_cmpgt_ps(y, _mm_setzero_ps()));
	unsigned int yLessThanZero = _mm_movemask_ps(_mm_cmplt_ps(y, _mm_setzero_ps()));

	alignas(16) float f[4];
	_mm_store_ps(f, temp);

	for (unsigned int i = 0; i < 4; ++i)
	{
		if (xLessThanZero & (1 << i))
		{
			if (yLessThanZero & (1 << i))
			{
				f[i] -= MathHelper::Pi;
			}
			else
			{
				f[i] += MathHelper::Pi;
			}
		}
		else
		if (xEqualsZero & (1 << i))
		{
			if (yLessThanZero & (1 << i))
			{
				f[i] = -MathHelper::PiDiv2;
			}
			else
			if (yGreaterThanZero & (1 << i))
			{
				f[i] = MathHelper::PiDiv2;
			}
			else
			{
				f[i] = 0.0f;
			}
		}
	}

	return _mm_load_ps(f);
}

inline __m128 GLOWE::SSE1::fabs(const __m128 arg)
{
	return unsetSignbit(arg);
}

inline __m128 GLOWE::SSE1::convertToRadians(const __m128 arg)
{
	return _mm_mul_ps(arg, _mm_set_ps1(MathHelper::Pi / 180.0f));
}

inline __m128 GLOWE::SSE1::convertToDegrees(const __m128 arg)
{
	return _mm_mul_ps(arg, _mm_set_ps1(180.0f / MathHelper::Pi));
}

/*
__m128 GLOWE::SSE1::atan2(const __m128 y, const __m128 x)
{
	__m128 xySqrt = sqrt(_mm_add_ps(_mm_mul_ps(x, x), _mm_mul_ps(y, y)));
	__m128 resultGT = _mm_div_ps(y, _mm_add_ps(xySqrt, x));
	__m128 resultLT = _mm_div_ps(_mm_sub_ps(xySqrt, x), y);

	unsigned int xEqualsZero = _mm_movemask_ps(_mm_cmpeq_ps(x, _mm_setzero_ps()));
	unsigned int xLessThanZero = _mm_movemask_ps(_mm_cmplt_ps(x, _mm_setzero_ps()));
	unsigned int yEqualsZero = _mm_movemask_ps(_mm_cmpeq_ps(y, _mm_setzero_ps()));

	alignas(16) float tempArr[4];
	alignas(16) float tempGT[4];
	alignas(16) float tempLT[4];

	_mm_store_ps(tempGT, resultGT);
	_mm_store_ps(tempLT, resultLT);

	for (unsigned int i = 0; i < 4; ++i)
	{
		// We don't want to run atan twice, so we need to emplace computed values for x > 0 and x <= 0.
		// Next (in the next "for" loop), we will replace all exceptional (y = 0) values with zeros and PIs.

		if (xLessThanZero & (1 << i))
		{
			tempArr[i] = tempLT[i];
		}
		else
		{
			tempArr[i] = tempGT[i];
		}
	}

	__m128 temp = _mm_load_ps(tempArr);

	temp = atan(temp);
	temp = _mm_add_ps(temp, temp);

	_mm_store_ps(tempArr, temp);

	for (unsigned int i = 0; i < 4; ++i)
	{
		// Here, we're going to replace all exceptional values with zeros and PIs.
		// Also, we nedd to check the signbits in order to correctly replace values.
		// It might not be the best solution ever possible, but it's highly accurate and still presumably fast.
		if (yEqualsZero & (1 << i))
		{
			if (xEqualsZero & (1 << i))
			{
				tempArr[i] = 0.0f;
			}
			else
			if (xLessThanZero & (1 << i))
			{
				tempArr[i] = MathHelper::Pi;
			}
		}
	}

	return _mm_load_ps(tempArr);
}
*/

#endif

#endif
