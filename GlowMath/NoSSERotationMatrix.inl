#pragma once
#ifndef GLOWE_NOSSE_MATH_ROTATIONMATRIX_INL_INCLUDED
#define GLOWE_NOSSE_MATH_ROTATIONMATRIX_INL_INCLUDED

#include "RotationMatrix.h"

#if GLOWE_USED_SSE == GLOWE_NO_SSE
GLOWE::RotationMatrix::operator GLOWE::Matrix4x4() const
{
	return Matrix4x4(Float4x4{
		Float4{ elems[0][0], elems[0][1], elems[0][2], 0.0f },
		Float4{ elems[1][0], elems[1][1], elems[1][2], 0.0f },
		Float4{ elems[2][0], elems[2][1], elems[2][2], 0.0f },
		Float4{ 0.0f, 0.0f, 0.0f, 1.0f }
	});
}

GLOWE::Quaternion GLOWE::RotationMatrix::toQuaternion() const
{
	return Quaternion::fromRotationMatrix(*this);
}

GLOWE::Vector4F GLOWE::RotationMatrix::toAxisAngle() const
{
	// TODO
	constexpr float singularityEpsilon = 0.1f;

	if (MathHelper::compareFloats(elems[0][1], elems[1][0])
		&& MathHelper::compareFloats(elems[0][2], elems[2][0])
		&& MathHelper::compareFloats(elems[1][2], elems[2][1]))
	{
		// Singularity - angle equals 0� or 180�
	}

	const float m21m12 = elems[2][1] - elems[1][2];
	const float m02m20 = elems[0][2] - elems[2][0];
	const float m10m01 = elems[1][0] - elems[0][1];

	const float divider = 1.0f / std::sqrt((m21m12 * m21m12) + (m02m20 * m02m20) + (m10m01 * m10m01));

	return Vector4F(std::acos(elems[0][0] + elems[1][1] + elems[2][2] - 1.0f) / 2.0f,
		m21m12 * divider,
		m02m20 * divider,
		m10m01 * divider);
}

GLOWE::Vector3F GLOWE::RotationMatrix::toEulerAngles() const
{
	// Don't worry, Z equals 0 when not modified
	Vector3F result;

	const float m10 = (*this)(1, 0);

	result.setY(std::asin(m10));

	if (m10 > 0.998f)
	{
		result.setX(std::atan2((*this)(0, 2), (*this)(2, 2)));
		result.setY(MathHelper::PiDiv2);
	}
	else
	if (m10 < -0.998f)
	{
		result.setX(std::atan2((*this)(0, 2), (*this)(2, 2)));
		result.setY(-MathHelper::PiDiv2);
	}
	else
	{
		result.setX(std::atan2(-(*this)(2, 0), (*this)(0, 0)));
		result.setZ(std::atan2(-(*this)(1, 2), (*this)(1, 1)));
	}

	// vec(roll, pitch, yaw) or vec(heading, elevation, bank)
	return result;
}

GLOWE::RotationMatrix GLOWE::RotationMatrix::fromQuaternion(const Quat qt)
{
	return qt.asRotationMatrix();
}

GLOWE::RotationMatrix GLOWE::RotationMatrix::fromAxisAngle(const Vec4 vec)
{
	const float c = std::cos(vec.getX());
	const float s = std::sin(vec.getX());
	const float t = 1.0f - c;

	const float tx = t * vec.getY();
	const float ty = t * vec.getZ();

	const float zs = s * vec.getW();
	const float xs = s * vec.getY();
	const float ys = s * vec.getZ();

	return Matrix3x3(Float3x3{
		Float3{ tx * vec.getY() + c, tx * vec.getZ() - zs, tx * vec.getW() + ys },
		Float3{tx * vec.getZ() + zs, ty * vec.getZ() + c, ty * vec.getW() - xs },
		Float3{tx * vec.getW() - ys, ty * vec.getW() + xs, t * vec.getW()* vec.getW() + c}
	});
}

GLOWE::RotationMatrix GLOWE::RotationMatrix::fromEulerAngles(const Vec3 vec)
{
	
	const float sa = std::sin(vec.getZ());
	const float ca = std::cos(vec.getZ());
	const float sb = std::sin(vec.getY());
	const float cb = std::cos(vec.getY());
	const float sh = std::sin(vec.getX());
	const float ch = std::cos(vec.getX());

	return Matrix3x3(Float3x3{
		Float3{ ch * ca, -ch * sa * cb - sh * sb, ch * sa * sb + sh * cb },
		Float3{ sa, ca * cb, -ca * sb },
		Float3{-sh * ca, sh * sa * cb + ch * sb, -sh * sa * sb + ch * cb}
	});

	/*
	Matrix3x3 firstMat(Float3x3{
		Float3{},
		Float3{},
		Float3{}
	});
	*/
}

GLOWE::Matrix3x3 GLOWE::RotationMatrix::mat4x4ToMat3x3(const Mat4x4 arg)
{
	return Matrix3x3(Float3x3{
		Float3{ arg(0, 0), arg(0, 1), arg(0, 2) },
		Float3{ arg(1, 0), arg(1, 1), arg(1, 2) },
		Float3{ arg(2, 0), arg(2, 1), arg(2, 2) }
	});
}

#endif

#endif
