#include "RotationMatrix.h"

GLOWE::RotationMatrix::RotationMatrix()
	: Matrix3x3(Matrix3x3::getIdentity())
{
}

GLOWE::RotationMatrix::RotationMatrix(const Mat3x3 arg)
	: Matrix3x3(arg)
{
}

GLOWE::RotationMatrix::RotationMatrix(const Mat4x4 arg)
	: Matrix3x3(mat4x4ToMat3x3(arg))
{
}
