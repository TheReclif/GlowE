#pragma once
#ifndef GLOWE_MATH_MATRIXCLASSES_INCLUDED
#define GLOWE_MATH_MATRIXCLASSES_INCLUDED

#include "Math.h"

#if GLOWE_USED_SSE == GLOWE_NO_SSE
#include "NoSSEMatrixClasses.h"
#elif GLOWE_USED_SSE >= GLOWE_SSE_1
#include "SSE1MatrixClasses.h"
#endif

namespace GLOWE
{
#if GLOWE_USED_SSE == GLOWE_NO_SSE
	template<unsigned int rows, unsigned int cols>
	using Matrix = NoSSE::Matrix<rows, cols>;
#elif GLOWE_USED_SSE == GLOWE_SSE_1 || GLOWE_USED_SSE == GLOWE_SSE_2
	template<unsigned int rows, unsigned int cols>
	using Matrix = SSE1::Matrix<rows, cols>;
#elif GLOWE_USED_SSE == GLOWE_SSE_3
	template<unsigned int rows, unsigned int cols>
	using Matrix = SSE1::Matrix<rows, cols>;
#elif GLOWE_USED_SSE == GLOWE_SSE_4
	template<unsigned int rows, unsigned int cols>
	using Matrix = SSE1::Matrix<rows, cols>;
#else
#error No SSE specified.
#endif

	using Matrix2x2 = Matrix<2, 2>;
	using Matrix3x3 = Matrix<3, 3>;
	using Matrix4x4 = Matrix<4, 4>;

	template<unsigned int x, unsigned int y>
	inline Matrix<x, y> operator*(const float left, const Matrix<x, y>& right)
	{
		return right * left;
	}
}

#endif
