#pragma once
#ifndef GLOWE_MATH_AABB_INCLUDED
#define GLOWE_MATH_AABB_INCLUDED

#include "../GlowSystem/NumberStorage.h"

#include "Frustum.h"
#include "Transform.h"

namespace GLOWE
{
	class Sphere;
	class Cone;

	class GLOWMATH_EXPORT alignas(16) AABB
	{
	public:
		Float3 center, halfSize;
	public:
		AABB() = default;
		inline AABB(const Float3& newCenter, const Float3& newHalfSize)
			: center(newCenter), halfSize(newHalfSize)
		{}
		inline AABB(const void* vertices, const unsigned int vertexStructureSize, const unsigned int verticesCount)
		{
			fromVertices(vertices, vertexStructureSize, verticesCount);
		}
		inline AABB(const unsigned int* indicesArray, const unsigned int indicesCount, const void* verticesArray, const unsigned int vertexStructureSize, const unsigned int verticesCount)
		{
			fromIndices(indicesArray, indicesCount, verticesArray, vertexStructureSize, verticesCount);
		}
		inline AABB(const unsigned short* indicesArray, const unsigned int indicesCount, const void* verticesArray, const unsigned int vertexStructureSize, const unsigned int verticesCount)
		{
			fromIndices(indicesArray, indicesCount, verticesArray, vertexStructureSize, verticesCount);
		}

		// First element must be the size of Float3 and must represent position.
		void fromVertices(const void* vertices, const unsigned int vertexStructureSize, const unsigned int verticesCount);
		void fromIndices(const unsigned int* indicesArray, const unsigned int indicesCount, const void* verticesArray, const unsigned int vertexStructureSize, const unsigned int verticesCount);
		void fromIndices(const unsigned short* indicesArray, const unsigned int indicesCount, const void* verticesArray, const unsigned int vertexStructureSize, const unsigned int verticesCount);

		bool isInsideFrustum(const Frustum& frustum) const;

		bool contains(const Vector3F& point) const;

		bool intersects(const Sphere& sphere) const;
		bool intersects(const AABB& aabb) const;
		//bool intersects(const Cone& cone) const;
		bool intersectsLine(const Vector3F& lineStart, const Vector3F& lineDirection) const; // TODO: Test. Probably not working properly.
		bool intersectsSegment(const Vector3F& segmentStart, const Vector3F& segmentEnd) const; // TODO: Test. Probably not working properly.

		Vector3F getClosestPoint(const Vector3F& point) const;
		Vector3F getVectorToSphere(const Sphere& sphere) const;
		
		Array<Float3, 8> getVertices() const;

		AABB transform(const Transform& trans) const;
		AABB transform(const Matrix4x4& mat4x4) const;
	};

	inline bool AABB::isInsideFrustum(const Frustum& frustum) const
	{
		return frustum.isAABBInside(*this);
	}

	inline bool AABB::contains(const Vector3F& point) const
	{
		Vector3F distance(Vector3F(center) - point);
#if GLOWE_USED_SSE > GLOWE_NO_SSE
		__m128 distanceM128 = SSE1::fabs(distance.m);
		__m128 halfSizeM128 = Vector3F(halfSize).m;
		int result = _mm_movemask_ps(_mm_cmple_ps(distance.m, halfSizeM128));
		return result == 0x7;
#else
		return (std::abs(distance.getX()) <= halfSize[0])
			&& (std::abs(distance.getY()) <= halfSize[1])
			&& (std::abs(distance.getZ()) <= halfSize[2]);
#endif
	}

	inline AABB GLOWE::AABB::transform(const Transform& trans) const
	{
		return transform(trans.calculateMatrix());
	}
}

#endif
