#pragma once
#ifndef GLOWE_NOSSE_QUATERNIONCLASSES_INL_INCLUDED
#define GLOWE_NOSSE_QUATERNIONCLASSES_INL_INCLUDED

#include "NoSSEQuaternionClasses.h"

inline GLOWE::NoSSE::Quaternion::Quaternion()
	: w(0.0f), x(0.0f), y(0.0f), z(0.0f)
{
}

inline GLOWE::NoSSE::Quaternion::Quaternion(const Quaternion & arg)
	: w(arg.w), x(arg.x), y(arg.y), z(arg.z)
{
}

inline GLOWE::NoSSE::Quaternion::Quaternion(Quaternion && arg) noexcept
	: w(std::move(arg.w)), x(std::move(arg.x)), y(std::move(arg.y)), z(std::move(arg.z))
{
}

inline GLOWE::NoSSE::Quaternion::Quaternion(const float arg)
	: w(arg), x(arg), y(arg), z(arg)
{
}

inline GLOWE::NoSSE::Quaternion::Quaternion(const Float4 & arg)
	: w(arg[0]), x(arg[1]), y(arg[2]), z(arg[3])
{
}

inline GLOWE::NoSSE::Quaternion::Quaternion(const float arg[4])
	: w(arg[0]), x(arg[1]), y(arg[2]), z(arg[3])
{
}

inline GLOWE::NoSSE::Quaternion::Quaternion(const float a, const float b, const float c, const float d)
	: w(a), x(b), y(c), z(d)
{
}

inline GLOWE::Float4 GLOWE::NoSSE::Quaternion::toFloat4() const
{
	return Float4{ w, x, y, z };
}

inline float & GLOWE::NoSSE::Quaternion::operator[](const unsigned short id)
{
	return f[id];
}

inline float GLOWE::NoSSE::Quaternion::operator[](const unsigned short id) const
{
	return f[id];
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::operator+(const Quaternion & right) const
{
	return Quaternion(w + right.w, x + right.x, y + right.y, z + right.z);
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::operator-(const Quaternion & right) const
{
	return Quaternion(w - right.w, x - right.x, y - right.y, z - right.z);
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::operator*(const Quaternion & right) const
{
	return Quaternion(
		right.w * w - right.x * x - right.y * y - right.z * z,
		right.w * x + right.x * w - right.y * z + right.z * y,
		right.w * y + right.x * z + right.y * w - right.z * x,
		right.w * z - right.x * y + right.y * x + right.z * w
	);
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::operator/(const Quaternion & right) const
{
	return (*this) * right.inverse();
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::operator+(const float right) const
{
	return Quaternion(w + right, x + right, y + right, z + right);
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::operator-(const float right) const
{
	return Quaternion(w - right, x - right, y - right, z - right);
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::operator*(const float right) const
{
	return Quaternion(w * right, x * right, y * right, z * right);
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::operator/(const float right) const
{
	return Quaternion(w / right, x / right, y / right, z / right);
}

inline float GLOWE::NoSSE::Quaternion::norm() const
{
	return (w*w) + (x*x) + (y*y) + (z*z);
}

inline float GLOWE::NoSSE::Quaternion::modulus() const
{
	return std::sqrt(norm());
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::inverse() const
{
	return std::move(conjugate() / norm());
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::conjugate() const
{
	return std::move(Quaternion(w, -x, -y, -z));
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::normalize() const
{
	return (*this) / modulus();
}

inline GLOWE::Vector3F GLOWE::NoSSE::Quaternion::asEulerAngles() const
{
	float sinRoll = +2.0f * (w * x + y * z);
	float cosRoll = +1.0f - 2.0f * (x * x + y * y);
	float roll = std::atan2(sinRoll, cosRoll), pitch, yaw;

	float sinPitch = +2.0f * (w * y - z * x);
	if (std::fabs(sinPitch) >= 1.0f)
	{
		pitch = std::copysign(MathHelper::Pi / 2.0f, sinPitch);
	}
	else
	{
		pitch = std::asin(sinPitch);
	}

	float sinYaw = +2.0f * (w * z + x * y);
	float cosYaw = +1.0f - 2.0f * (y * y + z * z);

	yaw = std::atan2(sinYaw, cosYaw);

	return Vector3F(roll, pitch, yaw);
}

inline GLOWE::Vector4F GLOWE::NoSSE::Quaternion::asAxisAngle() const
{
	const float zuza = std::sqrt(1.0f - (w * w));

	return Vector4F(
		2.0f * std::acos(w),
		x / zuza,
		y / zuza,
		z / zuza
	);
}

inline GLOWE::Matrix3x3 GLOWE::NoSSE::Quaternion::asRotationMatrix() const
{
	const float wPow2 = 2.0f * w * w;
	const float xPow2 = 2.0f * x * x;
	const float yPow2 = 2.0f * y * y;
	const float zPow2 = 2.0f * z * z;

	const float xw = 2.0f * x * w;
	const float yw = 2.0f * y * w;
	const float zw = 2.0f * z * w;

	const float xy2 = 2.0f * y * x;
	const float xz2 = 2.0f * x * z;
	const float yz2 = 2.0f * y * z;

	return Matrix3x3(
		Float3x3
		{
			Float3{1.0f - yPow2 - zPow2, xy2 - zw, xz2 + yw},
			Float3{xy2 + zw, 1.0f - xPow2 - zPow2, yz2 - xw},
			Float3{xz2 - yw, yz2 + xw, 1.0f - xPow2 - yPow2}
		}
	);
}

inline GLOWE::Vector3F GLOWE::NoSSE::Quaternion::transformPoint(const GLOWE::Vector3F& vec) const
{
	GLOWE::Vector3F u(x, y, z);

	return 2.0f * u.scalarProduct(vec) * u + (w * w - u.getScalarProduct()) * vec + 2.0f * w * u.vectorProduct(vec);
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::lookAt(const GLOWE::Vector3F& dir, const GLOWE::Vector3F& up, const GLOWE::Vector3F& forward)
{
	// https://gamedev.stackexchange.com/questions/53129/quaternion-look-at-with-up-vector
	Vector3F axis = forward.vectorProduct(dir).normalize();
	float angle = std::acos(forward.scalarProduct(dir));
	if (axis.getMagnitudeSquared() < 0.9f)
	{
		axis = up;
	}

	const Quaternion rot1 = Quaternion::fromAxisAngle(angle, axis);

	const Vector3F newUp = rot1.transformPoint(up), localRight = dir.vectorProduct(up).normalize(), localUp = localRight.vectorProduct(dir);
	axis = newUp.vectorProduct(localUp).normalize();
	if (axis.getMagnitudeSquared() < 0.9f)
	{
		return rot1;
	}
	angle = std::acos(newUp.scalarProduct(localUp));

	return Quaternion::fromAxisAngle(angle, axis) * rot1;
}

inline const GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::getIdentity()
{
	return Quaternion(1.0f, 0.0f, 0.0f, 0.0f);
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::fromEulerAngles(const Vector3F & angles)
{
	const float psi = angles[0] / 2.0f;
	const float theta = angles[1] / 2.0f;
	const float phi = angles[2] / 2.0f;

	const float cosPsi = std::cos(psi);
	const float sinPsi = std::sin(psi);

	const float cosTheta = std::cos(theta);
	const float sinTheta = std::sin(theta);

	const float cosPhi = std::cos(phi);
	const float sinPhi = std::sin(phi);

	const float a = sinPhi * cosPsi;
	const float b = sinPsi * cosTheta;
	const float c = sinTheta * cosPhi;

	return Quaternion(
		(cosPhi * cosTheta * cosPsi) + (sinPhi * sinTheta * sinPsi),
		(b * cosPhi) - (a * sinTheta),
		(c * cosPsi) + (b * sinPhi),
		(a * cosTheta) - (c * sinPsi)
	);
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::fromAxisAngle(const Vector4F & angles)
{
	const float zuza = angles[0] / 2.0f;
	const float halfAngleSin = std::sin(zuza);

	/*
	return Quaternion(
		std::cos(angles[0] / 2.0f),
		halfAngleSin * std::cos(angles[1]),
		halfAngleSin * std::cos(angles[2]),
		halfAngleSin * std::cos(angles[3]));
	*/
	
	return Quaternion(
		std::cos(zuza),
		halfAngleSin * angles[1],
		halfAngleSin * angles[2],
		halfAngleSin * angles[3]);
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::fromAxisAngle(const float angle, const GLOWE::Vector3F & axis)
{
	return Quaternion::fromAxisAngle(Vector4F(angle, axis[0], axis[1], axis[2]));
}

inline GLOWE::NoSSE::Quaternion GLOWE::NoSSE::Quaternion::fromRotationMatrix(const Matrix3x3 & mat)
{
	const float w = std::sqrt(1.0f + mat(0, 0) + mat(1, 1) + mat(2, 2)) / 2.0f;
	const float w4 = 4.0f * w;

	return Quaternion(
		w,
		(mat(2, 1) - mat(1, 2)) / w4,
		(mat(0, 2) - mat(2, 0)) / w4,
		(mat(1, 0) - mat(0, 1)) / w4
	);
}

#endif
