#pragma once
#ifndef GLOWE_SSE1_CAMERA_INCLUDED
#define GLOWE_SSE1_CAMERA_INCLUDED

#include "MatrixClasses.h"
#include "Transform.h"

#if GLOWE_USED_SSE > GLOWE_NO_SSE
namespace GLOWE
{
	namespace SSE1
	{
		class GLOWMATH_EXPORT alignas(16) Camera
		{
		private:
			union
			{
				struct
				{
					Float4x4 view, projection;
				};
				struct
				{
					Float1D<16> serializableView, serializableProjection;
				};
			};
		public:
			Camera();

			GlowStaticSerialize(GlowBases(), GlowField(serializableView), GlowField(serializableProjection));

			void perspectiveProjection(const float width, const float height, const float farF = 1000.0f, const float nearF = 0.01f);
			void perspectiveProjectionFov(const float fov, const float aspectRatio, const float farF = 1000.0f, const float nearF = 0.01f);

			void orthographicProjectionOffScreen(const float left, const float right, const float top, const float bottom, const float farF = 1000.0f, const float nearF = 0.01f);
			void orthographicProjection(const float width, const float height, const float farF = 1000.0f, const float nearF = 0.01f);

			void lookAt(const Float3& eye, const Float3& center, const Float3& up = Float3{ 0.0f, 1.0f, 0.0f });
			void fromTransform(const Transform& transform);

			Matrix4x4 getView() const;
			Matrix4x4 getProj() const;

			void setProj(const Float4x4& mat);
			void setView(const Float4x4& mat);
		};
	}
}
#endif

#endif
