#include "SSE1Camera.h"

#if GLOWE_USED_SSE > GLOWE_NO_SSE
GLOWE::SSE1::Camera::Camera()
	: view(Matrix4x4::getIdentity()), projection(Matrix4x4::getIdentity())
{
}

void GLOWE::SSE1::Camera::perspectiveProjection(const float width, const float height, const float farF, const float nearF)
{
	projection = Float4x4();
	projection[0][0] = 2.0f * nearF / width;
	projection[1][1] = 2.0f * nearF / height;
	projection[2][2] = farF / (farF - nearF);
	projection[2][3] = nearF * farF / (nearF - farF);
	projection[3][2] = 1.0f;
}

void GLOWE::SSE1::Camera::perspectiveProjectionFov(const float fov, const float aspectRatio, const float farF, const float nearF)
{
	const float yScale = 1.0f / std::tan(fov / 2.0f);
	const float xScale = yScale / aspectRatio;

	projection = Float4x4();
	projection[0][0] = xScale;
	projection[1][1] = yScale;
	projection[2][2] = farF / (farF - nearF);
	projection[2][3] = -nearF * farF / (farF - nearF);
	projection[3][2] = 1.0f;
}

void GLOWE::SSE1::Camera::orthographicProjectionOffScreen(const float left, const float right, const float top, const float bottom, const float farF, const float nearF)
{
	// https://docs.microsoft.com/en-us/windows/desktop/direct3d9/d3dxmatrixorthooffcenterlh
	projection = Float4x4();
	projection[0][0] = 2.0f / (right - left);
	projection[1][1] = 2.0f / (top - bottom);
	projection[2][2] = -2.0f / (farF - nearF);
	projection[0][3] = -((right + left) / (right - left));
	projection[1][3] = -((top + bottom) / (top - bottom));
	projection[2][3] = -((farF + nearF) / (farF - nearF));
	projection[3][3] = 1.0f;
}

void GLOWE::SSE1::Camera::orthographicProjection(const float width, const float height, const float farF, const float nearF)
{
	projection = Float4x4();
	projection[0][0] = 2.0f / width;
	projection[1][1] = 2.0f / height;
	projection[2][2] = 1.0f / (farF - nearF);
	projection[2][3] = nearF / (nearF - farF);
	projection[3][3] = 1.0f;
}

void GLOWE::SSE1::Camera::lookAt(const Float3& eye, const Float3& center, const Float3& up)
{
	// https://docs.microsoft.com/en-us/windows/desktop/direct3d9/d3dxmatrixlookatlh

	Vector3F e(eye), c(center), u(up);
	Vector3F zAxis = (c - e).normalize();
	Vector3F xAxis = u.vectorProduct(zAxis).normalize();
	Vector3F yAxis = zAxis.vectorProduct(xAxis);

	view = Float4x4();
	view[0] = Float4{ xAxis[0], yAxis[0], zAxis[0], -xAxis.scalarProduct(e) };
	view[1] = Float4{ xAxis[1], yAxis[1], zAxis[1], -yAxis.scalarProduct(e) };
	view[2] = Float4{ xAxis[2], yAxis[2], zAxis[2], -zAxis.scalarProduct(e) };
	view[3] = Float4{ 0.0f, 0.0f, 0.0f, 1.0f };
}

void GLOWE::SSE1::Camera::fromTransform(const Transform& transform)
{
	Matrix4x4 translation = Transform::createTranslationMatrix(transform.getTranslation());
	RotationMatrix rotation = RotationMatrix::fromQuaternion(transform.getRotation());

	view = (translation * rotation).inverse();
}

GLOWE::Matrix4x4 GLOWE::SSE1::Camera::getView() const
{
	return Matrix4x4(view);
}

GLOWE::Matrix4x4 GLOWE::SSE1::Camera::getProj() const
{
	return Matrix4x4(projection);
}

void GLOWE::SSE1::Camera::setProj(const Float4x4& mat)
{
	projection = mat;
}

void GLOWE::SSE1::Camera::setView(const Float4x4& mat)
{
	view = mat;
}

#endif
