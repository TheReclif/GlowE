#include "Rect.h"

bool GLOWE::Rect::isInside(const Int2& point) const
{
	return point[0] >= topLeft[0] && point[1] >= topLeft[1] && point[0] <= bottomRight[0] && point[1] <= bottomRight[1];
}

bool GLOWE::Rect::isInside(const Float2& point) const
{
	return isInside(Int2{ static_cast<int>(point[0]), static_cast<int>(point[1]) });
}

bool GLOWE::Rect::intersects(const Rect& otherRect) const
{
	/*
	const bool xTest = bottomRight[0] >= otherRect.topLeft[0] && topLeft[0] <= bottomRight[0];
	const bool yTest = bottomRight[1] >= otherRect.topLeft[1] && topLeft[1] <= bottomRight[1];

	return xTest && yTest;
	*/

	// Probably unnecessary checks (bottomRight is always bigger than topLeft, otherwise the rect is ill-formed).
	/*
	return
		bottomRight[0] >= otherRect.topLeft[0] && topLeft[0] <= bottomRight[0]
		&&
		bottomRight[1] >= otherRect.topLeft[1] && topLeft[1] <= bottomRight[1];
		*/
	return
		otherRect.topLeft[0] <= bottomRight[0] && otherRect.bottomRight[0] >= topLeft[0]
		&&
		otherRect.topLeft[1] <= bottomRight[1] && otherRect.bottomRight[1] >= topLeft[1];
}
