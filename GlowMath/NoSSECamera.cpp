#include "NoSSECamera.h"
#include "NoSSEVectorClasses.h"
#include "NoSSEMatrixClasses.h"
#include "RotationMatrix.h"

GLOWE::NoSSE::Camera::Camera()
	: view(Matrix<4, 4>::getIdentity()), projection(Matrix<4, 4>::getIdentity())
{
}

void GLOWE::NoSSE::Camera::perspectiveProjection(const float width, const float height, const float farF, const float nearF)
{
	projection = Float4x4();
	projection[0][0] = 2.0f * nearF / width;
	projection[1][1] = 2.0f * nearF / height;
	projection[2][2] = farF / (farF - nearF);
	projection[2][3] = nearF * farF / (nearF - farF);
	projection[3][2] = 1.0f;
}

void GLOWE::NoSSE::Camera::perspectiveProjectionFov(const float fov, const float aspectRatio, const float farF, const float nearF)
{
	const float yScale = 1.0f / std::tan(fov / 2.0f);
	const float xScale = yScale / aspectRatio;

	projection = Float4x4();
	projection[0][0] = xScale;
	projection[1][1] = yScale;
	projection[2][2] = farF / (farF - nearF);
	projection[2][3] = -nearF * farF / (farF - nearF);
	projection[3][2] = 1.0f;
}

void GLOWE::NoSSE::Camera::orthographicProjectionOffScreen(const float left, const float right, const float top, const float bottom, const float farF, const float nearF)
{
	// https://docs.microsoft.com/en-us/windows/desktop/direct3d9/d3dxmatrixorthooffcenterlh
	projection = Float4x4();
	projection[0][0] = 2.0f / (right - left);
	projection[1][1] = 2.0f / (top - bottom);
	projection[2][2] = -2.0f / (farF - nearF);
	projection[3][0] = -((right + left) / (right - left));
	projection[3][1] = -((top + bottom) / (top - bottom));
	projection[3][2] = -((farF + nearF) / (farF - nearF));
	projection[3][3] = 1.0f;
}

void GLOWE::NoSSE::Camera::orthographicProjection(const float width, const float height, const float farF, const float nearF)
{
	projection = Float4x4();
	projection[0][0] = 2.0f / width;
	projection[1][1] = 2.0f / height;
	projection[2][2] = 1.0f / (farF - nearF);
	projection[2][3] = nearF / (nearF - farF);
	projection[3][3] = 1.0f;
}

void GLOWE::NoSSE::Camera::lookAt(const Float3& eye, const Float3& center, const Float3& up)
{
	// https://docs.microsoft.com/en-us/windows/desktop/direct3d9/d3dxmatrixlookatlh

	NoSSE::Vector<float, 3> e(eye), c(center), u(up);
	NoSSE::Vector<float, 3> zAxis = (c - e).normalize();
	NoSSE::Vector<float, 3> xAxis = u.vectorProduct(zAxis).normalize();
	NoSSE::Vector<float, 3> yAxis = zAxis.vectorProduct(xAxis);

	view = Float4x4();
	view[0] = Float4{ xAxis[0], yAxis[0], zAxis[0], -xAxis.scalarProduct(e) };
	view[1] = Float4{ xAxis[1], yAxis[1], zAxis[1], -yAxis.scalarProduct(e) };
	view[2] = Float4{ xAxis[2], yAxis[2], zAxis[2], -zAxis.scalarProduct(e) };
	view[3] = Float4{ 0.0f, 0.0f, 0.0f, 1.0f };
}

void GLOWE::NoSSE::Camera::fromTransform(const Transform& transform)
{
	Matrix4x4 translation = Transform::createTranslationMatrix(transform.getTranslation());
	RotationMatrix rotation = RotationMatrix::fromQuaternion(transform.getRotation());

	view = (translation * rotation).inverse();
}

GLOWE::NoSSE::Matrix<4, 4> GLOWE::NoSSE::Camera::getView() const
{
	return Matrix<4, 4>(view);
}

GLOWE::NoSSE::Matrix<4, 4> GLOWE::NoSSE::Camera::getProj() const
{
	return Matrix<4, 4>(projection);
}

void GLOWE::NoSSE::Camera::setProj(const Float4x4& mat)
{
	projection = mat;
}

void GLOWE::NoSSE::Camera::setView(const Float4x4& mat)
{
	view = mat;
}
