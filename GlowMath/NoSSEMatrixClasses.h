#pragma once
#ifndef GLOWE_MATH_NOSSE_MATRIXCLASSES_INCLUDED
#define GLOWE_MATH_NOSSE_MATRIXCLASSES_INCLUDED

#include "../GlowSystem/NumberStorage.h"
#include "Math.h"
#include "VectorClasses.h"

namespace GLOWE
{
	namespace NoSSE
	{
		template<unsigned int rows, unsigned int cols>
		class Matrix
		{
		protected:
			//PL: Ustalony układ: wierszami. elems[0] to wiersz pierwszy (zerowy).
			//EN: Used convention: by rows. elems[0] is first row.
			float elems[rows][cols];
		public:
			inline Matrix();
			inline Matrix(const Matrix& arg) = default;
			inline Matrix(Matrix&& arg) noexcept = default;
			inline Matrix(const float arg[rows][cols]);
			inline Matrix(const Float2D<rows, cols>& arg);
			inline Matrix(const float arg);

			inline ~Matrix() noexcept = default;

			inline float& operator()(const unsigned short x, const unsigned short y);
			inline const float& operator()(const unsigned short x, const unsigned short y) const;

			inline Matrix& operator=(const Matrix& right);

			inline operator Float2D<rows, cols>() const
			{
				Float2D<rows, cols> result;

				for (unsigned int x = 0; x < rows; ++x)
				{
					std::memcpy(result[x].data(), elems[x], sizeof(float[cols]));
				}

				return result;
			};

			inline Matrix operator-() const;

			inline Matrix operator+(const float arg) const;
			inline Matrix operator-(const float arg) const;
			inline Matrix operator*(const float arg) const;
			inline Matrix operator/(const float arg) const;

			inline Matrix operator+(const Matrix& right) const;
			inline Matrix operator-(const Matrix& right) const;

			template<unsigned int x>
			inline Matrix<rows, x> operator*(const Matrix<cols, x>& right) const;
			template<unsigned int x>
			inline Matrix<rows, x> operator/(const Matrix<cols, x>& right) const;

			inline VectorF<cols> operator*(const VectorF<cols>& right) const;

			inline Matrix& operator+=(const float arg) const;
			inline Matrix& operator-=(const float arg) const;
			inline Matrix& operator*=(const float arg) const;
			inline Matrix& operator/=(const float arg) const;

			inline Matrix& operator+=(const Matrix& right) const;
			inline Matrix& operator-=(const Matrix& right) const;
			template<unsigned int x, unsigned int y>
			inline Matrix& operator*=(const Matrix<x, y>& right) const;
			template<unsigned int x, unsigned int y>
			inline Matrix& operator/=(const Matrix<x, y>& right) const;

			inline Matrix<cols, rows> transpose() const;
			inline Matrix<cols, rows> toD3D() const { return transpose(); };
			inline Matrix<rows, cols> toOpenGL() const { return *this; };
			inline Matrix inverse() const;

			inline float determinant() const;

			inline float cofactor(const unsigned int row, const unsigned int column) const;
			inline Matrix cofactorMatrix() const;

			inline Matrix adjugateMatrix() const;

			inline float minor(const unsigned int rowToDelete, const unsigned int columnToDelete) const;

			inline bool isInvertable() const;

			template<class Func>
			inline void applyFunction(Func&& func);

			static inline const Matrix& getIdentity();
		};

		template<>
		class Matrix<0, 0> {};

		template<>
		class Matrix<1, 0> {};

		template<>
		class Matrix<0, 1> {};

		template<>
		class Matrix<1, 1>
		{
		protected:
			float elem;
		public:
			inline Matrix() : elem(0.0f) {};
			inline Matrix(const Matrix& arg) = default;
			inline Matrix(Matrix&& arg) noexcept = default;
			inline Matrix(const float arg) : elem(arg) {};

			Matrix& operator=(const Matrix&) = default;

			inline float& operator()(const unsigned short, const unsigned short) { return elem; };
			inline const float& operator()(const unsigned short, const unsigned short) const { return elem; };

			inline operator float() const { return elem; };

			inline Matrix operator-() const { return -elem; };

			inline Matrix operator+(const Matrix& right) const { return elem + right.elem; };
			inline Matrix operator-(const Matrix& right) const { return elem - right.elem; };
			inline Matrix operator*(const Matrix& right) const { return elem * right.elem; };
			inline Matrix operator/(const Matrix& right) const { return elem / right.elem; };

			inline Matrix operator+(const float right) const { return elem + right; };
			inline Matrix operator-(const float right) const { return elem - right; };
			inline Matrix operator*(const float right) const { return elem * right; };
			inline Matrix operator/(const float right) const { return elem / right; };

			inline VectorF<1> operator*(const VectorF<1>& right) const { return VectorF<1>(elem * right[0]); };

			inline Matrix& operator+=(const Matrix& right) { return *this = elem + right.elem; };
			inline Matrix& operator-=(const Matrix& right) { return *this = elem - right.elem; };
			inline Matrix& operator*=(const Matrix& right) { return *this = elem * right.elem; };
			inline Matrix& operator/=(const Matrix& right) { return *this = elem / right.elem; };

			inline Matrix& operator+=(const float right) { return *this = elem + right; };
			inline Matrix& operator-=(const float right) { return *this = elem - right; };
			inline Matrix& operator*=(const float right) { return *this = elem * right; };
			inline Matrix& operator/=(const float right) { return *this = elem / right; };

			inline bool operator==(const Matrix& right) const { return elem == right.elem; };
			inline bool operator!=(const Matrix& right) const { return elem != right.elem; };

			inline Matrix transpose() const { return *this; };
			inline Matrix inverse() const { return 1 / elem; };

			inline float determinant() const { return elem; };
			inline float minor(const unsigned int, const unsigned int) const { return 0.0f; };

			inline bool isInvertable() const { return elem != 0.0f; };

			template<class Func>
			inline void applyFunction(Func&& func) { elem = func(elem); };
		};

		//Matrices 2xX
		template<>
		class Matrix<2, 2>
		{
		protected:
			//PL: Ustalony układ: wierszami. elems[0] to wiersz pierwszy (zerowy).
			//EN: Used convention: by rows. elems[0] is first row.
			float elems[2][2];
		public:
			inline Matrix();
			inline Matrix(const Matrix& arg) = default;
			inline Matrix(Matrix&& arg) noexcept = default;
			inline Matrix(const Float2x2& arg);
			inline Matrix(const float arg[2][2]);
			inline Matrix(
				const float m00, const float m01,
				const float m10, const float m11
			);
			inline Matrix(const float arg); // Initialize every field with given float

			Matrix& operator=(const Matrix&) = default;

			inline float& operator()(const unsigned short x, const unsigned short y);
			inline const float& operator()(const unsigned short x, const unsigned short y) const;

			inline operator Float2x2() const;

			inline Matrix operator-() const;

			inline Matrix operator+(const Matrix& right) const;
			inline Matrix operator-(const Matrix& right) const;
			inline Matrix operator*(const Matrix& right) const;
			inline Matrix operator/(const Matrix& right) const;

			template<unsigned int x>
			inline Matrix<2, x> operator*(const Matrix<2, x>& right) const;
			template<unsigned int x>
			inline Matrix<2, x> operator/(const Matrix<2, x>& right) const;

			inline VectorF<2> operator*(const VectorF<2>& right) const;

			inline Matrix operator+(const float right) const;
			inline Matrix operator-(const float right) const;
			inline Matrix operator*(const float right) const;
			inline Matrix operator/(const float right) const;

			inline Matrix& operator+=(const Matrix& right);
			inline Matrix& operator-=(const Matrix& right);
			inline Matrix& operator*=(const Matrix& right);
			inline Matrix& operator/=(const Matrix& right);

			inline Matrix& operator+=(const float right);
			inline Matrix& operator-=(const float right);
			inline Matrix& operator*=(const float right);
			inline Matrix& operator/=(const float right);

			inline bool operator==(const Matrix& right) const;
			inline bool operator!=(const Matrix& right) const;

			inline Matrix transpose() const;
			inline Matrix toD3D() const { return transpose(); };
			inline Matrix toOpenGL() const { return *this; };
			inline Matrix inverse() const;

			inline float determinant() const;

			inline float cofactor(const unsigned int row, const unsigned int column) const;
			inline Matrix cofactorMatrix() const;

			inline Matrix adjugateMatrix() const;

			inline float minor(const unsigned int rowToDelete, const unsigned int columnToDelete) const;

			inline bool isInvertable() const;

			template<class Func>
			inline void applyFunction(Func&& func);

			static inline const Matrix& getIdentity();
		};

		//Matrices 3xX
		template<>
		class Matrix<3, 3>
		{
		protected:
			//PL: Ustalony układ: wierszami. elems[0] to wiersz pierwszy (zerowy).
			//EN: Used convention: by rows. elems[0] is first row.
			float elems[3][3];
		public:
			inline Matrix();
			inline Matrix(const Matrix& arg) = default;
			inline Matrix(Matrix&& arg) noexcept = default;
			inline Matrix(const Float3x3& arg);
			inline Matrix(const float arg[3][3]);
			inline Matrix(
				const float m00, const float m01, const float m02,
				const float m10, const float m11, const float m12,
				const float m20, const float m21, const float m22
			);
			inline Matrix(const float arg); // Initialize every field with given float

			Matrix& operator=(const Matrix&) = default;

			inline float& operator()(const unsigned short x, const unsigned short y);
			inline const float& operator()(const unsigned short x, const unsigned short y) const;

			inline operator Float3x3() const;

			inline Matrix operator-() const;

			inline Matrix operator+(const Matrix& right) const;
			inline Matrix operator-(const Matrix& right) const;
			inline Matrix operator*(const Matrix& right) const;
			inline Matrix operator/(const Matrix& right) const;

			template<unsigned int y>
			inline Matrix<3, y> operator*(const Matrix<3, y>& right) const;
			template<unsigned int y>
			inline Matrix<3, y> operator/(const Matrix<3, y>& right) const;

			inline VectorF<3> operator*(const VectorF<3>& right) const;

			inline Matrix operator+(const float right) const;
			inline Matrix operator-(const float right) const;
			inline Matrix operator*(const float right) const;
			inline Matrix operator/(const float right) const;

			inline Matrix& operator+=(const Matrix& right);
			inline Matrix& operator-=(const Matrix& right);
			inline Matrix& operator*=(const Matrix& right);
			inline Matrix& operator/=(const Matrix& right);

			inline Matrix& operator+=(const float right);
			inline Matrix& operator-=(const float right);
			inline Matrix& operator*=(const float right);
			inline Matrix& operator/=(const float right);

			inline bool operator==(const Matrix& right) const;
			inline bool operator!=(const Matrix& right) const;

			inline Matrix transpose() const;
			inline Matrix toD3D() const { return transpose(); };
			inline Matrix toOpenGL() const { return *this; };
			inline Matrix inverse() const;

			inline float cofactor(const unsigned int row, const unsigned int column) const;
			inline Matrix cofactorMatrix() const;

			inline Matrix adjugateMatrix() const;

			inline float determinant() const;
			inline float minor(const unsigned int rowToDelete, const unsigned int columnToDelete) const;

			inline bool isInvertable() const;

			template<class Func>
			inline void applyFunction(Func&& func);

			static inline const Matrix& getIdentity();
		};

		template<unsigned int rows, unsigned int cols>
		inline VectorF<rows> operator*(const VectorF<rows>& left, const Matrix<rows, cols>& right);
	}
}
#include "NoSSEMatrixClasses.inl"

#endif
