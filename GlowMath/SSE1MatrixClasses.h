#pragma once
#ifndef GLOWE_SSE1_MATH_MATRIXCLASSES_INCLUDED
#define GLOWE_SSE1_MATH_MATRIXCLASSES_INCLUDED

#include "NoSSEMatrixClasses.h"

#ifdef GLOWE_COMPILE_FOR_X64
#define GlowAbove3XMMRegister(Type) Type
#else
#define GlowAbove3XMMRegister(Type) Type&
#endif

#if GLOWE_USED_SSE > GLOWE_NO_SSE
namespace GLOWE
{
	// Forward declaration
	class Transform;
	class RotationMatrix;

	namespace SSE1
	{
		template<unsigned int rows, unsigned int cols>
		class alignas(16) Matrix
			: public NoSSE::Matrix<rows, cols>
		{
		public:
			using NoSSE::Matrix<rows, cols>::Matrix;
		};

		template<>
		class alignas(16) Matrix<2, 2>
		{
		protected:
			union Row
			{
				__m128 m;
				float f[4];
			};

			Row row0, row1;
		public:
			inline Matrix();
			inline Matrix(const __m128 a, const __m128 b);
			inline Matrix(const Float2x2& arg);
			inline Matrix(const float arg[2][2]);
			inline Matrix(
				const float m00, const float m01,
				const float m10, const float m11
			);
			inline Matrix(const float arg); // Initialize every field with given float

			inline float& operator()(const unsigned short x, const unsigned short y);
			inline float operator()(const unsigned short x, const unsigned short y) const;

			inline operator Float2x2() const;

			inline Matrix operator-() const;

			inline Matrix operator+(GlowAbove3XMMRegister(const Matrix) right) const;
			inline Matrix operator-(GlowAbove3XMMRegister(const Matrix) right) const;
			inline Matrix operator*(GlowAbove3XMMRegister(const Matrix) right) const;
			inline Matrix operator/(GlowAbove3XMMRegister(const Matrix) right) const;

			template<unsigned int x>
			inline Matrix<2, x> operator*(const Matrix<2, x>& right) const;
			template<unsigned int x>
			inline Matrix<2, x> operator/(const Matrix<2, x>& right) const;

			inline GLOWE::Vector2F operator*(GlowAbove3XMMRegister(const GLOWE::Vector2F) right) const;

			inline Matrix operator+(const float right) const;
			inline Matrix operator-(const float right) const;
			inline Matrix operator*(const float right) const;
			inline Matrix operator/(const float right) const;

			inline Matrix& operator+=(GlowAbove3XMMRegister(const Matrix) right);
			inline Matrix& operator-=(GlowAbove3XMMRegister(const Matrix) right);
			inline Matrix& operator*=(GlowAbove3XMMRegister(const Matrix) right);
			inline Matrix& operator/=(GlowAbove3XMMRegister(const Matrix) right);

			inline Matrix& operator+=(const float right);
			inline Matrix& operator-=(const float right);
			inline Matrix& operator*=(const float right);
			inline Matrix& operator/=(const float right);

			inline bool operator==(GlowAbove3XMMRegister(const Matrix) right) const;
			inline bool operator!=(GlowAbove3XMMRegister(const Matrix) right) const;

			inline Matrix transpose() const;
			inline Matrix toD3D() const { return transpose(); };
			inline Matrix toOpenGL() const { return *this; };
			inline Matrix inverse() const;

			inline float determinant() const;

			inline float cofactor(const unsigned int row, const unsigned int column) const;
			inline Matrix cofactorMatrix() const;

			inline Matrix adjugateMatrix() const;

			inline float minor(const unsigned int rowToDelete, const unsigned int columnToDelete) const;

			inline bool isInvertable() const;

			inline GLOWE::Vector2F getRow(const unsigned int row) const;

			static inline const Matrix getIdentity();

			friend class GLOWE::Transform;
			friend class GLOWE::RotationMatrix;
		};

		template<>
		class alignas(16) Matrix<3, 3>
		{
		protected:
			union Row
			{
				__m128 m;
				float f[4];
			};

			Row row0, row1, row2;
		public:
			inline Matrix();
			inline Matrix(const __m128 a, const __m128 b, const __m128 c);
			inline Matrix(const Float3x3& arg);
			inline Matrix(const float arg[3][3]);
			inline Matrix(
				const float m00, const float m01, const float m02,
				const float m10, const float m11, const float m12,
				const float m20, const float m21, const float m22
			);
			inline Matrix(const float arg); // Initialize every field with given float

			inline float& operator()(const unsigned short x, const unsigned short y);
			inline float operator()(const unsigned short x, const unsigned short y) const;

			inline operator Float3x3() const;

			inline Matrix operator-() const;

			inline Matrix operator+(GlowAbove3XMMRegister(const Matrix) right) const;
			inline Matrix operator-(GlowAbove3XMMRegister(const Matrix) right) const;
			inline Matrix operator*(GlowAbove3XMMRegister(const Matrix) right) const;
			inline Matrix operator/(GlowAbove3XMMRegister(const Matrix) right) const;

			template<unsigned int x>
			inline Matrix<3, x> operator*(const Matrix<3, x>& right) const;
			template<unsigned int x>
			inline Matrix<3, x> operator/(const Matrix<3, x>& right) const;

			inline GLOWE::Vector3F operator*(GlowAbove3XMMRegister(const GLOWE::Vector3F) right) const;

			inline Matrix operator+(const float right) const;
			inline Matrix operator-(const float right) const;
			inline Matrix operator*(const float right) const;
			inline Matrix operator/(const float right) const;

			inline Matrix& operator+=(GlowAbove3XMMRegister(const Matrix) right);
			inline Matrix& operator-=(GlowAbove3XMMRegister(const Matrix) right);
			inline Matrix& operator*=(GlowAbove3XMMRegister(const Matrix) right);
			inline Matrix& operator/=(GlowAbove3XMMRegister(const Matrix) right);

			inline Matrix& operator+=(const float right);
			inline Matrix& operator-=(const float right);
			inline Matrix& operator*=(const float right);
			inline Matrix& operator/=(const float right);

			inline bool operator==(GlowAbove3XMMRegister(const Matrix) right) const;
			inline bool operator!=(GlowAbove3XMMRegister(const Matrix) right) const;

			inline Matrix transpose() const;
			inline Matrix toD3D() const { return transpose(); };
			inline Matrix toOpenGL() const { return *this; };
			inline Matrix inverse() const;

			inline float determinant() const;

			inline float cofactor(const unsigned int row, const unsigned int column) const;
			inline Matrix cofactorMatrix() const;

			inline Matrix adjugateMatrix() const;

			inline float minor(const unsigned int rowToDelete, const unsigned int columnToDelete) const;

			inline bool isInvertable() const;

			inline GLOWE::Vector3F getRow(const unsigned int row) const;

			static inline const Matrix getIdentity();

			friend class GLOWE::Transform;
			friend class GLOWE::RotationMatrix;
		};

		template<>
		class alignas(16) Matrix<4, 4>
		{
		protected:
			union Row
			{
				__m128 m;
				float f[4];
			};

			Row row0, row1, row2, row3;
		public:
			inline Matrix();
			inline Matrix(const __m128 a, const __m128 b, const __m128 c, GlowAbove3XMMRegister(const __m128) d);
			inline Matrix(const Float4x4& arg);
			inline Matrix(const float arg[4][4]);
			inline Matrix(
				const float m00, const float m01, const float m02, const float m03,
				const float m10, const float m11, const float m12, const float m13,
				const float m20, const float m21, const float m22, const float m23,
				const float m30, const float m31, const float m32, const float m33
			);
			inline Matrix(const float arg); // Initialize every field with given float

			inline float& operator()(const unsigned short x, const unsigned short y);
			inline float operator()(const unsigned short x, const unsigned short y) const;

			inline operator Float4x4() const;

			inline Matrix operator-() const;

			inline Matrix operator+(GlowAbove3XMMRegister(const Matrix) right) const;
			inline Matrix operator-(GlowAbove3XMMRegister(const Matrix) right) const;
			inline Matrix operator*(GlowAbove3XMMRegister(const Matrix) right) const;
			inline Matrix operator/(GlowAbove3XMMRegister(const Matrix) right) const;

			template<unsigned int x>
			inline Matrix<4, x> operator*(const Matrix<4, x>& right) const;
			template<unsigned int x>
			inline Matrix<4, x> operator/(const Matrix<4, x>& right) const;

			inline GLOWE::Vector4F operator*(GlowAbove3XMMRegister(const GLOWE::Vector4F) right) const;

			inline Matrix operator+(const float right) const;
			inline Matrix operator-(const float right) const;
			inline Matrix operator*(const float right) const;
			inline Matrix operator/(const float right) const;

			inline Matrix& operator+=(GlowAbove3XMMRegister(const Matrix) right);
			inline Matrix& operator-=(GlowAbove3XMMRegister(const Matrix) right);
			inline Matrix& operator*=(GlowAbove3XMMRegister(const Matrix) right);
			inline Matrix& operator/=(GlowAbove3XMMRegister(const Matrix) right);

			inline Matrix& operator+=(const float right);
			inline Matrix& operator-=(const float right);
			inline Matrix& operator*=(const float right);
			inline Matrix& operator/=(const float right);

			inline bool operator==(GlowAbove3XMMRegister(const Matrix) right) const;
			inline bool operator!=(GlowAbove3XMMRegister(const Matrix) right) const;

			inline Matrix transpose() const;
			inline Matrix toD3D() const { return transpose(); };
			inline Matrix toOpenGL() const { return *this; };
			inline Matrix inverse() const;

			inline float determinant() const;

			inline float cofactor(const unsigned int row, const unsigned int column) const;
			inline Matrix cofactorMatrix() const;

			inline Matrix adjugateMatrix() const;

			inline float minor(const unsigned int rowToDelete, const unsigned int columnToDelete) const;

			inline bool isInvertable() const;

			inline GLOWE::Vector4F getRow(const unsigned int row) const;

			static inline const Matrix getIdentity();

			friend class GLOWE::Transform;
			friend class GLOWE::RotationMatrix;
		};

#include "SSE1MatrixClasses.inl"
	}
}
#endif

#endif
