#pragma once
#ifndef GLOWE_MATH_PLANE_INCLUDED
#define GLOWE_MATH_PLANE_INCLUDED

#include "../GlowSystem/NumberStorage.h"

#include "VectorClasses.h"

namespace GLOWE
{
	class AABB;

	class GLOWMATH_EXPORT alignas(16) Plane
	{
	public:
		enum class Where
			: signed char
		{
			Negative = -1,
			OnPlane = 0,
			Positive = 1
		};
	public:
		Float4 abcd;
	public:
		Plane() = default;
		inline Plane(const Float4& arg)
			: abcd(arg)
		{}
		inline Plane(const Vector3F& normal, const Vector3F& point)
		{
			fromNormalAndPoint(normal, point);
		}

		void fromNormalAndPoint(const Vector3F& normal, const Vector3F& point);

		Float3 getNormal() const;

		Plane normalize() const;

		float getDistanceFromPoint(const Vector3F& point) const;

		Where getPointPlacement(const Vector3F& point) const;

		bool isPointInside(const Vector3F& point) const;
		bool isAABBInside(const AABB& aabb) const;

		Vector3F getLineIntersection(const Vector3F& direction, const Vector3F& point) const;
	};
}

#endif
