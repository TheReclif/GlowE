#include "MeshCompiler.h"
#include "Log.h"
#include "PackageHelper.h"

#include <Nullscript/Nullscript.h>

#include <GlowSystem/Color.h>

#include <GlowGraphics/Uniform/InputLayoutDesc.h>

#include <GlowMath/VectorClasses.h>
#include <GlowMath/AxisAlignedBoundingBox.h>

#include <GlowComponents/GeometryRendererComponent.h>

#include <GlowEngine/GameObjectBlueprint.h>

#include <meshoptimizer.h>

struct Vertex
{
	Float3 position, normal, tangent;
	Float2 textureCoord;
};

void computeTangets(const Vector<UInt3>& triangles, Vector<Vertex>& vertices)
{
	auto isVertexInTriangle = [](const unsigned int id, const UInt3 & triangle) -> bool
	{
		return triangle[0] == id || triangle[1] == id || triangle[2] == id;
	};

	Vector<Float3> tan1;
	tan1.resize(vertices.size() * 2, Float3{ 0.0f, 0.0f, 0.0f });
	Float3* tan2 = tan1.data() + vertices.size();

	for (auto& triangle : triangles)
	{
		UInt i1 = triangle[0];
		UInt i2 = triangle[1];
		UInt i3 = triangle[2];

		const Float3& v1 = vertices[i1].position;
		const Float3& v2 = vertices[i2].position;
		const Float3& v3 = vertices[i3].position;

		const Float2& w1 = vertices[i1].textureCoord;
		const Float2& w2 = vertices[i2].textureCoord;
		const Float2& w3 = vertices[i3].textureCoord;

		float x1 = v2[0] - v1[0];
		float x2 = v3[0] - v1[0];
		float y1 = v2[1] - v1[1];
		float y2 = v3[1] - v1[1];
		float z1 = v2[2] - v1[2];
		float z2 = v3[2] - v1[2];

		float s1 = w2[0] - w1[0];
		float s2 = w3[0] - w1[0];
		float t1 = w2[1] - w1[1];
		float t2 = w3[1] - w1[1];

		float r = 1.0F / (s1 * t2 - s2 * t1);
		Vector3F sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r,
			(t2 * z1 - t1 * z2) * r);
		Vector3F tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r,
			(s1 * z2 - s2 * z1) * r);
#define AccumulateVector(WhichTan, WhichDir, Index) \
		{ \
			Vector3F temp = WhichTan[Index]; \
			temp += WhichDir; \
			WhichTan[Index] = temp.toFloat3(); \
		}

		AccumulateVector(tan1, sdir, i1);
		AccumulateVector(tan1, sdir, i2);
		AccumulateVector(tan1, sdir, i3);

		AccumulateVector(tan2, tdir, i1);
		AccumulateVector(tan2, tdir, i2);
		AccumulateVector(tan2, tdir, i3);

#undef AccumulateVector

		/*
		tan1[i1] += sdir;
		tan1[i2] += sdir;
		tan1[i3] += sdir;

		tan2[i1] += tdir;
		tan2[i2] += tdir;
		tan2[i3] += tdir;
		*/
	}

	for (unsigned int i = 0; i < vertices.size(); ++i)
	{
		Vector3F normal = vertices[i].normal, tangent = tan1[i];
		
		Vector3F tangentOut = (tangent - normal * normal.scalarProduct(tangent)).normalize();

		//float handedness = ((normal.vectorProduct(tangent).scalarProduct(tan2[i])) < 0.0f) ? -1.0f : 1.0f;
		if ((normal.vectorProduct(tangent).scalarProduct(tan2[i])) < 0.0f)
		{
			tangentOut *= -1.0f;
		}
		
		vertices[i].tangent = tangentOut.toFloat3();
	}
}

Pair<FilePath, Pair<String, Map<Hash, ResourceMgr::SubresourceInfo>>> compileMtllib(const FilePath& inputPath, const String& packageName)
{
	struct MaterialStruct
	{
		MaterialStruct() = default;

		Color ambient, diffuse, specular;
		float specularExponent, bumpMultiplier, dissolveHaloFactor;
		String ambientTexture, diffuseTexture, specularTexture, bumpTexture;
	};

	Pair<FilePath, Pair<String, Map<Hash, ResourceMgr::SubresourceInfo>>> result;
	Map<Hash, ResourceMgr::SubresourceInfo>& subresources = result.second.second;

	Map<String, MaterialStruct> materials;
	MaterialStruct* currentMaterial = nullptr;

	File file;
	file.open(inputPath, std::ios::in);
	if (!file.checkIsOpen())
	{
		log("Unable to open a mtllib named " + inputPath + ".");
		return result;
	}

	Vector<String> tokens;
	{
		String contents;
		contents.resize(file.getFileSize());
		file.read(contents.getCharArray(), contents.getSize());
		file.close();

		tokens = contents.tokenize(u8"\n");
	}

	const std::locale localeC("C");

	for (const auto& token : tokens)
	{
		IStringStream iss(token);
		iss.imbue(localeC);
		String str;

		iss >> str;
		if (str[0] == '#')
		{
			continue;
		}
		else
		if (str == "newmtl")
		{
			String temp;
			iss >> temp;
			currentMaterial = &(materials.emplace(std::piecewise_construct, std::forward_as_tuple(std::move(temp)), std::tuple<>()).first->second);
			currentMaterial->ambient.rgba[3] = currentMaterial->diffuse.rgba[3] = currentMaterial->specular.rgba[3] = 1.0f;
			currentMaterial->dissolveHaloFactor = NAN;
		}
		else
		if (str == "Ka")
		{
			if (currentMaterial)
			{
				iss >> currentMaterial->ambient.rgba[0] >> currentMaterial->ambient.rgba[1] >> currentMaterial->ambient.rgba[2];
			}
		}
		else
		if (str == "Kd")
		{
			if (currentMaterial)
			{
				iss >> currentMaterial->diffuse.rgba[0] >> currentMaterial->diffuse.rgba[1] >> currentMaterial->diffuse.rgba[2];
			}
		}
		else
		if (str == "Ks")
		{
			if (currentMaterial)
			{
				iss >> currentMaterial->specular.rgba[0] >> currentMaterial->specular.rgba[1] >> currentMaterial->specular.rgba[2];
			}
		}
		else
		if (str == "d")
		{
			if (currentMaterial)
			{
				float dissolve;
				if (!(iss >> dissolve))
				{
					log("-halo parameter present.");
					String tempStr;
					iss >> tempStr; // So that we can ignore -halo part.
					iss >> dissolve;
					currentMaterial->dissolveHaloFactor = dissolve;
				}

				currentMaterial->ambient.rgba[3] = currentMaterial->diffuse.rgba[3] = currentMaterial->specular.rgba[3] = dissolve;
			}
		}
		else
		if (str == "Ns")
		{
			if (currentMaterial)
			{
				iss >> currentMaterial->specularExponent;
			}
		}
		else
		if (str == "map_Ka")
		{
			if (currentMaterial)
			{
				std::getline(iss, currentMaterial->ambientTexture.getString(), '#');
				currentMaterial->ambientTexture.erase(0, 1);
			}
		}
		else
		if (str == "map_Kd")
		{
			if (currentMaterial)
			{
				std::getline(iss, currentMaterial->diffuseTexture.getString(), '#');
				currentMaterial->diffuseTexture.erase(0, 1);
			}
		}
		else
		if (str == "map_Ks")
		{
			if (currentMaterial)
			{
				std::getline(iss, currentMaterial->specularTexture.getString(), '#');
				currentMaterial->specularTexture.erase(0, 1);
			}
		}
		else
		if (str == "map_Bump" || str == "bump")
		{
			if (currentMaterial)
			{
				String temp;
				iss >> temp;

				if (temp == "-bm")
				{
					iss >> currentMaterial->bumpMultiplier;
					std::getline(iss, currentMaterial->bumpTexture.getString(), '#');
					currentMaterial->bumpTexture.erase(0, 1);
				}
				else
				{
					IStringStream tempIss(token);
					tempIss >> str;

					std::getline(tempIss, currentMaterial->bumpTexture.getString(), '#');
					currentMaterial->bumpTexture.erase(0, 1);
					currentMaterial->bumpMultiplier = 1.0f;
				}
			}
		}
	}

	result.first = inputPath.getFilename();
	result.first.replaceExtension("gmat");

	StringLoadSaveHandle handle;

	// TODO: Save the materials.

	/*
		Color ambient, diffuse, specular;
		float specularExponent, bumpMultiplier, dissolveHaloFactor;
		String ambientTexture, diffuseTexture, specularTexture, bumpTexture;
	*/
	const UInt32 matCount = materials.size();
	handle << matCount;
	for (const auto& mat : materials)
	{
		ResourceMgr::SubresourceInfo info(ResourceMgr::SubresourceType::Material);
		const UInt32 texCount = 2, fieldsCount = 10;
		Hash matHash(mat.first);

		// Material hash, in-effect group hash, technique hash, textures count and fields count in description (can be repeated, the latest one will apply).
		handle << matHash << Hash() << Hash(u8"Opaque") << fieldsCount;
		// Properties.
		// CBuffer hash, property hash and value in raw bytes.
		handle << Hash(u8"Material") << Hash(u8"Ambient") << static_cast<UInt32>(sizeof(mat.second.ambient)) << mat.second.ambient;
		handle << Hash(u8"Material") << Hash(u8"Diffuse") << static_cast<UInt32>(sizeof(mat.second.diffuse)) << mat.second.diffuse;
		handle << Hash(u8"Material") << Hash(u8"Specular") << static_cast<UInt32>(sizeof(mat.second.specular)) << mat.second.specular;
		handle << Hash(u8"Material") << Hash(u8"Specular Exponent") << static_cast<UInt32>(sizeof(mat.second.specularExponent)) << mat.second.specularExponent;
		handle << Hash(u8"Material") << Hash(u8"Bump Multiplier") << static_cast<UInt32>(sizeof(mat.second.bumpMultiplier)) << mat.second.bumpMultiplier;
		handle << Hash(u8"Material") << Hash(u8"Dissolve Halo Factor") << static_cast<UInt32>(sizeof(mat.second.dissolveHaloFactor)) << mat.second.dissolveHaloFactor;
		handle << Hash(u8"Material") << Hash(u8"Color Tex Coord Multiplier") << static_cast<UInt32>(sizeof(1.0f)) << 1.0f;
		handle << Hash(u8"Material") << Hash(u8"Color Tex Coord Offset") << static_cast<UInt32>(sizeof(0.0f)) << 0.0f;
		handle << Hash(u8"Material") << Hash(u8"Normal Tex Coord Multiplier") << static_cast<UInt32>(sizeof(1.0f)) << 1.0f;
		handle << Hash(u8"Material") << Hash(u8"Normal Tex Coord Offset") << static_cast<UInt32>(sizeof(0.0f)) << 0.0f;
		// For now we will ignore ambient texture (and probably also specular texture).

		info.externalDependiencies.emplace_back(std::piecewise_construct, std::forward_as_tuple(Hash(u8"Effect")), std::forward_as_tuple(Hash(u8"GlowCore/Shaders/StandardShader.geff"), Hash(u8"Effect"), false));
		if (!mat.second.diffuseTexture.isEmpty())
		{
			info.externalDependiencies.emplace_back(std::piecewise_construct, std::forward_as_tuple(Hash(u8"Diffuse Texture")), std::forward_as_tuple(Hash(mat.second.diffuseTexture), Hash(), false));
		}

		if (!mat.second.bumpTexture.isEmpty())
		{
			info.externalDependiencies.emplace_back(std::piecewise_construct, std::forward_as_tuple(Hash(u8"Normal Texture")), std::forward_as_tuple(Hash(mat.second.bumpTexture), Hash(), false));
		}
		result.second.second.emplace(matHash, std::move(info));
	}

	result.second.first = handle.getString();

	return result;
}

bool compileWavefrontOBJ(const FilePath& inputPath, const String& packageName, const String& contents, Vector<Pair<FilePath, Pair<String, Map<Hash, ResourceMgr::SubresourceInfo>>>>& files)
{
	struct ObjectInObj
	{
		UInt offset, count;
		String materialName;
	};

	Map<String, ObjectInObj> objects;
	Vector<String> tokens = contents.tokenize(u8"\n");
	String currentObject, usedMaterial;
	Vector<UInt3> triangles;
	UInt offset = 0, oldOffset = 0;
	ThreadPool::TaskResult<Pair<FilePath, Pair<String, Map<Hash, ResourceMgr::SubresourceInfo>>>> threadResult;
	ThreadPool materialThread(1);

	FilePath meshPath = inputPath.getFilename(), materialPath = meshPath;
	meshPath.replaceExtension("gmsh");
	materialPath.replaceExtension("gmat");

	Vector<Float3> positions, normals;
	Vector<Float2> textureCoords;
	Vector<Vertex> vertices;
	Map<String, unsigned int> vertexMappings;
	Regex faceVertexRegex(R"TXT((\d+)\/?(\d*)\/?(\d*))TXT");
	bool wasInformedAboutTriangulation = false, wasMaterialLibraryProcessingScheduled = false;
	const std::locale localeC("C");

	for (const auto& token : tokens)
	{
		IStringStream iss(token);
		iss.imbue(localeC);
		String str;
		
		if (!(iss >> str))
		{
			continue;
		}

		if (str == "v")
		{
			// Position.
			Float3 temp;
			if (iss >> temp[0] >> temp[1] >> temp[2])
			{
				positions.push_back(temp);
			}
		}
		else
		if (str == "vn")
		{
			// Normal.
			Float3 temp;
			if (iss >> temp[0] >> temp[1] >> temp[2])
			{
				normals.push_back(temp);
			}
		}
		else
		if (str == "vt")
		{
			// Texture coordinate.
			Float2 temp;
			if (iss >> temp[0] >> temp[1])
			{
				textureCoords.push_back(temp);
			}
		}
		else
		if (str == "o")
		{
			if (!currentObject.isEmpty())
			{
				// Save faces for the object.
				ObjectInObj tempObj;
				tempObj.materialName = std::move(usedMaterial);
				tempObj.offset = 3 * oldOffset;
				tempObj.count = 3 * (offset - oldOffset);
				oldOffset = offset;

				objects.emplace(std::move(currentObject), std::move(tempObj));
			}

			/*std::getline(iss, currentObject.getString());
			currentObject.erase(0, 1);*/
			iss >> std::ws;
			std::getline(iss, currentObject.getString());
			if (currentObject[currentObject.getSize() - 1] == '\r')
			{
				currentObject.resize(currentObject.getSize() - 1);
			}
		}
		else
		if (str == "f")
		{
			String temp;
			UInt3 triangle;
			
			for (unsigned int i = 0; i < 3; ++i)
			{
				iss >> temp;

				if (vertexMappings.count(temp) == 0)
				{
					// Create a new vertex.
					RegexMatch match;
					std::regex_match(temp.getString(), match, faceVertexRegex);

					String nums[3] =
					{
						String(match[1].first, match[1].second),
						String(match[2].first, match[2].second),
						String(match[3].first, match[3].second)
					};

					Vertex vertex;
					if (!nums[0].isEmpty())
					{
						vertex.position = positions[nums[0].toUInt() - 1];
					}

					if (!nums[1].isEmpty())
					{
						vertex.textureCoord = textureCoords[nums[1].toUInt() - 1];
					}

					if (!nums[2].isEmpty())
					{
						vertex.normal = normals[nums[2].toUInt() - 1];
					}

					vertexMappings.emplace(temp, static_cast<UInt32>(vertices.size()));
					vertices.emplace_back(std::move(vertex));
				}

				triangle[i] = vertexMappings.at(temp);
			}

			triangles.push_back(triangle);
			++offset;

			while (iss >> temp)
			{
				if (!wasInformedAboutTriangulation)
				{
					log("The mesh is not triangulated. It will be triangulated using a simplified triangulation algorithm. If the resulting geometry is different than what you see in your 3D modelling program, triangulate it using an appropriate option in the.");
					wasInformedAboutTriangulation = true;
				}

				if (vertexMappings.count(temp) == 0)
				{
					// Create a new vertex.
					RegexMatch match;
					std::regex_match(temp.getString(), match, faceVertexRegex);

					String nums[3] =
					{
						String(match[1].first, match[1].second),
						String(match[2].first, match[2].second),
						String(match[3].first, match[3].second)
					};

					Vertex vertex;
					if (!nums[0].isEmpty())
					{
						vertex.position = positions[nums[0].toUInt() - 1];
					}

					if (!nums[1].isEmpty())
					{
						vertex.textureCoord = textureCoords[nums[1].toUInt() - 1];
					}

					if (!nums[2].isEmpty())
					{
						vertex.normal = normals[nums[2].toUInt() - 1];
					}

					vertexMappings.emplace(temp, static_cast<UInt32>(vertices.size()));
					vertices.emplace_back(std::move(vertex));
				}

				triangle[0] = triangle[1];
				triangle[1] = triangle[2];
				triangle[2] = vertexMappings.at(temp);

				triangles.push_back(triangle);
				++offset;
			}
		}
		else
		if (str == "mtllib")
		{
			String temp;
			std::getline(iss, temp.getString());
			temp.erase(0, 1);
			if (!wasMaterialLibraryProcessingScheduled)
			{
				wasMaterialLibraryProcessingScheduled = true;
				if (temp[temp.getSize() - 1] == '\r')
				{
					temp.resize(temp.getSize() - 1);
				}
				threadResult = materialThread.addTask([&packageName, temp]() { return compileMtllib(temp, packageName); });
			}
			else
			{
				log(u8"Only one mtllib can be compiled.");
			}
		}
		else
		if (str == "usemtl")
		{
			std::getline(iss, usedMaterial.getString());
			usedMaterial.erase(0, 1);
		}
	}

	// Save the last object.
	//if (!currentObject.isEmpty())
	{
		// Save faces for the object.
		ObjectInObj tempObj;
		tempObj.materialName = std::move(usedMaterial);
		tempObj.offset = 3 * oldOffset;
		tempObj.count = 3 * (offset - oldOffset);

		objects.emplace(std::move(currentObject), std::move(tempObj));
	}

	meshopt_setAllocator([](const size_t size) -> void* { return getInstance<GlobalAllocator>().allocateSpace(size); }, [](void* ptr) { getInstance<GlobalAllocator>().deallocate(ptr); });

	// Mesh optimization.
	// Vertex cache optimization.
	for (const auto& x : objects)
	{
		unsigned int* const ptr = reinterpret_cast<unsigned int*>(triangles.data() + (x.second.offset / 3));
		const unsigned int howManyVertices = x.second.count;

		meshopt_optimizeVertexCache(ptr, ptr, howManyVertices, vertices.size());
		meshopt_optimizeOverdraw(ptr, ptr, howManyVertices, reinterpret_cast<const float*>(vertices.data()), vertices.size(), sizeof(Vertex), 1.05f);
	}

	meshopt_optimizeVertexFetch(vertices.data(), reinterpret_cast<unsigned int*>(triangles.data()), triangles.size() * 3, vertices.data(), vertices.size(), sizeof(Vertex));

	// Geometries.
	{
		// Hash("Material"), Pair<Hash, Hash>(Hash(packageName + '/' + materialPath), Hash(x.second.materialName))

		files.emplace_back(std::piecewise_construct, std::forward_as_tuple(meshPath), std::tuple<>());
		StringLoadSaveHandle handle;
		Map<Hash, ResourceMgr::SubresourceInfo>& subresources = files.back().second.second;

		// Save each geometry data (not geometry buffers).
		const UInt32 objectsCount = objects.size();
		handle << objectsCount;
		for (const auto& x : objects)
		{
			ResourceMgr::SubresourceInfo info(ResourceMgr::SubresourceType::Geometry);
			info.internalDependiencies.emplace_back(Hash("Buffers"), std::make_pair(Hash("Buffers"), false));
			Hash objHash(x.first);
			AABB localAabb;
			localAabb.fromIndices(reinterpret_cast<const unsigned int*>(triangles.data()) + x.second.offset, x.second.count, static_cast<const void*>(vertices.data()), sizeof(Vertex), vertices.size());

			handle << objHash << x.second.offset << x.second.count << localAabb.center << localAabb.halfSize;
			subresources.emplace(objHash, std::move(info));
		}

		subresources.emplace(Hash("Buffers"), ResourceMgr::SubresourceType::GeometryBuffers);

		// Save vertex input layout desc.
		bool areNormalsPresent = false, areTexCoordsPresent = false;
		InputLayoutDesc inputLayoutDesc;
		inputLayoutDesc.addVertexElement("Position", Format(Format::Type::Float, 3));
		if (!normals.empty())
		{
			inputLayoutDesc.addVertexElement("Normal", Format(Format::Type::Float, 3));
			inputLayoutDesc.addVertexElement("Tangent", Format(Format::Type::Float, 3));
			computeTangets(triangles, vertices);
			areNormalsPresent = true;
		}
		if (!textureCoords.empty())
		{
			inputLayoutDesc.addVertexElement("TexCoord", Format(Format::Type::Float, 2));
			areTexCoordsPresent = true;
		}

		const UInt32 verticesCount = vertices.size();
		handle << inputLayoutDesc << verticesCount;

		// Save geometry buffers (vertices and indices).
		for (const auto& x : vertices)
		{
			handle << x.position;
			if (areNormalsPresent)
			{
				handle << x.normal << x.tangent;
			}

			if (areTexCoordsPresent)
			{
				handle << x.textureCoord;
			}
		}

		const UInt32 trianglesCount = triangles.size();
		handle << 3 * trianglesCount;
		if (vertices.size() <= std::numeric_limits<unsigned short>::max())
		{
			for (const auto& x : triangles)
			{
				UShort3 newTriangle{ static_cast<unsigned short>(x[0]), static_cast<unsigned short>(x[1]), static_cast<unsigned short>(x[2]) };
				handle << newTriangle;
			}
		}
		else
		{
			for (const auto& x : triangles)
			{
				handle << x;
			}
		}

		files.back().second.first = handle.consumeString();

		FilePath tempPath = inputPath.getFilename();
		tempPath.replaceExtension(u8"ggobp");
		files.emplace_back(std::piecewise_construct, std::forward_as_tuple(tempPath), std::tuple<>());
		Map<Hash, ResourceMgr::SubresourceInfo>& blueprintSubresources = files.back().second.second;

		// Remember about materialPath.

		handle = StringLoadSaveHandle();
		Multimap<Hash, Component::Variables> components;

		Vector<ResourceHandle<void>> usedRes;
		usedRes.reserve(objects.size() * 2);
		ResourceMgr::SubresourceInfo info(ResourceMgr::SubresourceType::GameObjectBlueprint);

		for (const auto& object : objects)
		{
			const auto it = components.emplace(std::piecewise_construct, std::forward_as_tuple(Components::ComponentReflection<Components::GeometryRenderer>::hash()), std::tuple<>());

			Hash resHash = PackageHelper::createInternalPath(meshPath, packageName), subresHash = object.first;
			usedRes.emplace_back(SharedPtr<void>(), resHash, subresHash);
			
			info.externalDependiencies.emplace_back(Hash(), std::tuple<Hash, Hash, bool>(resHash, subresHash, false));
			handle << resHash << subresHash;
			it->second.emplaceOrAssign(u8"Geometry", makeUniquePtr<Hidden::StringSerializableValue>(handle.getString()));
			handle = StringLoadSaveHandle();

			resHash = PackageHelper::createInternalPath(materialPath, packageName);
			subresHash = object.second.materialName;
			usedRes.emplace_back(SharedPtr<void>(), resHash, subresHash);

			info.externalDependiencies.emplace_back(Hash(), std::tuple<Hash, Hash, bool>(resHash, subresHash, false));
			handle << resHash << subresHash;
			it->second.emplaceOrAssign(u8"Material", makeUniquePtr<Hidden::StringSerializableValue>(handle.getString()));
			handle = StringLoadSaveHandle();
		}

		tempPath = inputPath.getFilename();
		tempPath.removeExtension();
		GameObjectBlueprint blueprint(tempPath, components, usedRes);
		handle << blueprint;

		blueprintSubresources.emplace(std::piecewise_construct, std::tuple<>(), std::forward_as_tuple(std::move(info)));
		files.back().second.first = handle.getString();
	}

	if (wasMaterialLibraryProcessingScheduled)
	{
		const auto tempResult = threadResult.get();

		if (!tempResult.first.isEmpty())
		{
			files.push_back(tempResult);
		}
	}

	return true;
}

bool compileMesh(const FilePath& inputPath, const String& packageName, const String& contents, Vector<Pair<FilePath, Pair<String, Map<Hash, ResourceMgr::SubresourceInfo>>>>& files)
{
	if (inputPath.getExtension() == u8".obj")
	{
		// Wavefront OBJ.
		return compileWavefrontOBJ(inputPath, packageName, contents, files);
	}

	return false;
}
