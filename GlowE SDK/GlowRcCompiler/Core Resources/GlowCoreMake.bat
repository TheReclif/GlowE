@ECHO OFF
IF EXIST "GlowCore.gpckg" (
	DEL "GlowCore.gpckg"
)

ECHO Compiling ArrowUp...
.\GlowRcCompiler.exe /compile ArrowUp.png /pack GlowCore.gpckg "Textures/ArrowUp" /noLogo
ECHO Compiling ArrowDown...
.\GlowRcCompiler.exe /compile ArrowDown.png /pack GlowCore.gpckg "Textures/ArrowDown" /noLogo
ECHO Compiling ArrowLeft...
.\GlowRcCompiler.exe /compile ArrowLeft.png /pack GlowCore.gpckg "Textures/ArrowLeft" /noLogo
ECHO Compiling ArrowRight...
.\GlowRcCompiler.exe /compile ArrowRight.png /pack GlowCore.gpckg "Textures/ArrowRight" /noLogo

ECHO Compiling ShaderCacheGenWarning...
.\GlowRcCompiler.exe /compile ShaderCacheGenWarning.png /pack GlowCore.gpckg "Textures/ShaderCacheGenWarning" /noLogo
ECHO Compiling GUIElementShape...
.\GlowRcCompiler.exe /compile GUIElementShape.png /pack GlowCore.gpckg "Textures/GUIElementShape" /noLogo
ECHO Compiling PlainColorTexture...
.\GlowRcCompiler.exe /compile PlainColorTexture.png /pack GlowCore.gpckg "Textures/PlainColorTexture" /noLogo
ECHO Compiling WindowCloseIcon...
.\GlowRcCompiler.exe /compile WindowCloseIcon.png /pack GlowCore.gpckg "Textures/WindowCloseIcon" /noLogo

ECHO Compiling Arrow...
.\GlowRcCompiler.exe /compile Arrow.obj /pack GlowCore.gpckg "BasicGeometry/Arrow" /noLogo
ECHO Compiling Cone...
.\GlowRcCompiler.exe /compile Cone.obj /pack GlowCore.gpckg "BasicGeometry/Cone" /noLogo
ECHO Compiling Cube...
.\GlowRcCompiler.exe /compile Cube.obj /pack GlowCore.gpckg "BasicGeometry/Cube" /noLogo
ECHO Compiling Cylinder...
.\GlowRcCompiler.exe /compile Cylinder.obj /pack GlowCore.gpckg "BasicGeometry/Cylinder" /noLogo
ECHO Compiling Plane...
.\GlowRcCompiler.exe /compile Plane.obj /pack GlowCore.gpckg "BasicGeometry/Plane" /noLogo
ECHO Compiling GUIPlane...
.\GlowRcCompiler.exe /compile GUIPlane.obj /pack GlowCore.gpckg "BasicGeometry/GUIPlane" /noLogo
ECHO Compiling Sphere...
.\GlowRcCompiler.exe /compile Sphere.obj /pack GlowCore.gpckg "BasicGeometry/Sphere" /noLogo
ECHO Compiling SphereSmooth...
.\GlowRcCompiler.exe /compile SphereSmooth.obj /pack GlowCore.gpckg "BasicGeometry/SphereSmooth" /noLogo

ECHO Compiling StandardDebug...
.\GlowRcCompiler.exe /compile StandardDebug.gsil /pack GlowCore.gpckg "Shaders/StandardDebug" /noLogo
ECHO Compiling StandardShader...
.\GlowRcCompiler.exe /compile StandardShader.gsil /pack GlowCore.gpckg "Shaders/StandardShader" /noLogo
ECHO Compiling StandardGUIShader...
.\GlowRcCompiler.exe /compile StandardGUIShader.gsil /pack GlowCore.gpckg "Shaders/StandardGUIShader" /noLogo
ECHO Compiling StandardTextShader...
.\GlowRcCompiler.exe /compile StandardTextShader.gsil /pack GlowCore.gpckg "Shaders/StandardTextShader" /noLogo
ECHO Compiling StandardGUIMask...
.\GlowRcCompiler.exe /compile StandardGUIMask.gsil /pack GlowCore.gpckg "Shaders/StandardGUIMask" /noLogo

ECHO Compiling Arial.ttf...
.\GlowRcCompiler.exe /compile Arial.ttf /pack GlowCore.gpckg "BasicFonts/Arial" /noLogo
ECHO GlowCore built.
