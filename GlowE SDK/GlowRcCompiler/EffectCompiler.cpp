#include "EffectCompiler.h"
#include "Log.h"
#include <GlowGraphics/Uniform/GSILShaderCompiler.h>
#include <GlowGraphics/Uniform/GraphicsWindow.h>
#include <GlowEngine/Effect.h>

bool compileEffect(const FilePath& inputPath, const String&, const String& contents, Vector<Pair<FilePath, Pair<String, Map<Hash, ResourceMgr::SubresourceInfo>>>>& files)
{
	static auto dumpShaderCode = [](const String& str)
	{
		static Mutex mutex;
		DefaultLockGuard lock(mutex);

		String path = "CompiledHLSL.hlsl";
		unsigned int index = 2;
		while (Filesystem::isExisting(path))
		{
			path = "CompiledHLSL" + toString(index++) + ".hlsl";
		}

		log("Dumping compiled HLSL shader code to " + path + ".");
		File dumpFile;
		dumpFile.open(path, std::ios::out);
		if (dumpFile.checkIsOpen())
		{
			dumpFile << str;
			log("HLSL code dumped.");
		}
		else
		{
			log("Unable to dump HLSL code (unable to open a file).");
		}
	};
	static auto saveVectorOfUInts = [](const Vector<unsigned int>& vec, LoadSaveHandle& handle)
	{
		handle << static_cast<UInt32>(vec.size());
		for (const auto x : vec)
		{
			handle << static_cast<UInt32>(x);
		}
	};

	log("Compiling an effect.");

	FilePath tempEff = inputPath, tempHLSL = inputPath;
	tempEff.replaceExtension(".geff");
	tempHLSL.replaceExtension(".ghlsl");
	GSILShader shader(contents);

	log("GSIL shader compiled.");

	UniquePtr<GraphicsDeviceImpl> device = createGraphicsObject<GraphicsDeviceImpl>();
	if (device)
	{
		device->create(GraphicsDeviceSettings());
	}

	if (!device || !device->checkIsCreated())
	{
		log("Unable to create device.");
		return false;
	}

	log("Device created.");

	String codeHLSL;
	codeHLSL.reserve(4096);

	unsigned int variantsToCompile = 0;

	const String hlslHeader = shader.toHLSL();

	Map<Hash, Map<Hash, Map<unsigned int, Map<ShaderType, String>>>> shaderCodes;

	for (const auto& x : shader.techniqueGroups)
	{
		for (const auto& y : x.second)
		{
			for (const auto& z : y.second.passes)
			{
				variantsToCompile += z.second.owner->variants.size();
				const Hash& firstVariant = z.second.owner->variants.begin()->first;

				if (!z.second.computeShader.func.isEmpty())
				{
					String& tempCodeRef = shaderCodes[x.first][y.first][z.first][ShaderType::ComputeShader];
					tempCodeRef = shader.toHLSL(z.second, ShaderType::ComputeShader);
					const String tempCode = hlslHeader + shader.replaceHLSLForVariant(tempCodeRef, z.second, firstVariant, ShaderType::ComputeShader);

					const auto tempShader = createGraphicsObject<ComputeShaderImpl>();
					ShaderImpl::CompilationData data;
					data.entrypoint = z.second.computeShader.func;
					data.version = MathHelper::getShaderVersionFromFloat(z.second.computeShader.version);
					tempShader->compileFromCode(tempCode, *device, data);
					if (!tempShader->checkIsCreated())
					{
						log("Shader compilation error (compute shader).");
						dumpShaderCode(tempCode);
					}
				}

				if (!z.second.geometryShader.func.isEmpty())
				{
					String& tempCodeRef = shaderCodes[x.first][y.first][z.first][ShaderType::GeometryShader];
					tempCodeRef = shader.toHLSL(z.second, ShaderType::GeometryShader);
					const String tempCode = hlslHeader + shader.replaceHLSLForVariant(tempCodeRef, z.second, firstVariant, ShaderType::GeometryShader);

					const auto tempShader = createGraphicsObject<GeometryShaderImpl>();
					ShaderImpl::CompilationData data;
					data.entrypoint = z.second.geometryShader.func;
					data.version = MathHelper::getShaderVersionFromFloat(z.second.geometryShader.version);
					tempShader->compileFromCode(tempCode, *device, data);
					if (!tempShader->checkIsCreated())
					{
						log("Shader compilation error (geometry shader).");
						dumpShaderCode(tempCode);
					}
				}

				if (!z.second.vertexShader.func.isEmpty())
				{
					String& tempCodeRef = shaderCodes[x.first][y.first][z.first][ShaderType::VertexShader];
					tempCodeRef = shader.toHLSL(z.second, ShaderType::VertexShader);
					const String tempCode = hlslHeader + shader.replaceHLSLForVariant(tempCodeRef, z.second, firstVariant, ShaderType::VertexShader);

					const auto tempShader = createGraphicsObject<VertexShaderImpl>();
					ShaderImpl::CompilationData data;
					data.entrypoint = z.second.vertexShader.func;
					data.version = MathHelper::getShaderVersionFromFloat(z.second.vertexShader.version);
					tempShader->compileFromCode(tempCode, *device, data);
					if (!tempShader->checkIsCreated())
					{
						log("Shader compilation error (vertex shader).");
						dumpShaderCode(tempCode);
					}
				}

				if (!z.second.pixelShader.func.isEmpty())
				{
					String& tempCodeRef = shaderCodes[x.first][y.first][z.first][ShaderType::PixelShader];
					tempCodeRef = shader.toHLSL(z.second, ShaderType::PixelShader);
					const String tempCode = hlslHeader + shader.replaceHLSLForVariant(tempCodeRef, z.second, firstVariant, ShaderType::PixelShader);

					const auto tempShader = createGraphicsObject<PixelShaderImpl>();
					ShaderImpl::CompilationData data;
					data.entrypoint = z.second.pixelShader.func;
					data.version = MathHelper::getShaderVersionFromFloat(z.second.pixelShader.version);
					tempShader->compileFromCode(tempCode, *device, data);
					if (!tempShader->checkIsCreated())
					{
						log("Shader compilation error (pixel shader).");
						dumpShaderCode(tempCode);
					}
				}
			}
		}
	}

	log(u8"Variants in the effect: " + toString(variantsToCompile) + '.');

	{
		files.emplace_back(std::piecewise_construct, std::forward_as_tuple(tempEff), std::tuple<>());
		StringLoadSaveHandle handle;
		Map<Hash, ResourceMgr::SubresourceInfo>& subresources = files.back().second.second;
		ResourceMgr::SubresourceInfo info(ResourceMgr::SubresourceType::Effect);

		// The effect itself.
		// DSS.
		handle << (UInt32)shader.depthStencilStates.size();
		for (const auto& x : shader.depthStencilStates)
		{
			handle << x.second;
		}
		
		// Blend states.
		handle << (UInt32)shader.blendStates.size();
		for (const auto& x : shader.blendStates)
		{
			handle << x.second;
		}

		// Rasterizer states.
		handle << (UInt32)shader.rasterizerStates.size();
		for (const auto& x : shader.rasterizerStates)
		{
			handle << x.second;
		}

		// Samplers.
		handle << (UInt32)shader.samplers.size();
		for (const auto& x : shader.samplers)
		{
			handle << x.second;
		}

		// Textures.
		handle << (UInt32)shader.textures.size();
		for (const auto& x : shader.textures)
		{
			handle << x.property.name << x.type;
		}

		// UAVs.
		handle << (UInt32)shader.uavs.size();
		for (const auto& x : shader.uavs)
		{
			handle << x.property.name << x.type;
		}

		// Input layouts.
		handle << (UInt32)shader.inputLayouts.size();
		for (const auto& x : shader.inputLayouts)
		{
			handle << x;
		}

		// CBuffers.
		handle << (UInt32)shader.constantBuffers.size();
		for (const auto& x : shader.constantBuffers)
		{
			Effect::CBuffer cbuff;
			cbuff.layout = x.generateCbufferPropertyLayout().toHashedStructureLayout();
			for (const auto& field : x.fields)
			{
				const Hash name(field.property.name);
				//cbuff.layout.addElement(field.asFormat(), name);
				cbuff.variables[name].first = field.property.type;
				cbuff.variables.at(name).second.resize(field.property.constraints.size());
				for (const auto& x : field.property.constraints)
				{
					cbuff.variables.at(name).second.push_back(x);
				}
			}

			handle << Hash(x.name) << cbuff;
		}

		// Technique groups.
		handle << (UInt32)shader.techniqueGroups.size();
		for (const auto& group : shader.techniqueGroups)
		{
			handle << Hash(group.first) << (UInt32)group.second.size();
			for (const auto& technique : group.second)
			{
				handle << Hash(technique.first) << (UInt32)technique.second.tags.size();
				for (const auto& tag : technique.second.tags)
				{
					handle << tag;
				}

				handle << (UInt32)technique.second.passes.size();
				for (const auto& pass : technique.second.passes)
				{
					auto findFunc = [&](const String& funcName) -> const GSILShader::Function*
					{
						for (const auto& x : shader.functions)
						{
							if (x.name == funcName)
							{
								return &x;
							}
						}

						return nullptr;
					};

					String firstHashPart = group.first + ':' + technique.first + ':' + toString(pass.first) + ':';
					const String storedFirstHashPart = firstHashPart;

					// Hash of GroupName:TechniqueName:PassID (as String).
					handle << (UInt32)pass.first << (UInt32)pass.second.blendStateId << (UInt32)pass.second.dssId << (UInt32)pass.second.rastId;

					if (!pass.second.computeShader.func.isEmpty())
					{
						auto tempFun = findFunc(pass.second.computeShader.func);
						if (tempFun)
						{
							handle << pass.second.computeShader.func << pass.second.computeShader.version;
							handle << static_cast<UInt32>(tempFun->arguments.size());
							for (const auto& x : tempFun->arguments)
							{
								handle << x;
							}
							saveVectorOfUInts(pass.second.computeShader.usedCBuffers, handle);
							saveVectorOfUInts(pass.second.computeShader.usedUAVs, handle);
							saveVectorOfUInts(pass.second.computeShader.usedTextures, handle);
							for (const auto& x : pass.second.computeShader.usedTextures)
							{
								UInt32 tempUInt32 = shader.textures[x].samplerID;
								handle << tempUInt32;
							}
						}
					}
					else
					{
						handle << String() << 0.0f << ((UInt32)0) << ((UInt32)0) << ((UInt32)0) << ((UInt32)0);
					}

					if (!pass.second.vertexShader.func.isEmpty())
					{
						auto tempFun = findFunc(pass.second.vertexShader.func);
						if (tempFun)
						{
							handle << pass.second.vertexShader.func << pass.second.vertexShader.version;
							handle << static_cast<UInt32>(tempFun->arguments.size());
							for (const auto& x : tempFun->arguments)
							{
								handle << x;
							}
							saveVectorOfUInts(pass.second.vertexShader.usedCBuffers, handle);
							saveVectorOfUInts(pass.second.vertexShader.usedUAVs, handle);
							saveVectorOfUInts(pass.second.vertexShader.usedTextures, handle);
							for (const auto& x : pass.second.vertexShader.usedTextures)
							{
								UInt32 tempUInt32 = shader.textures[x].samplerID;
								handle << tempUInt32;
							}
						}
					}
					else
					{
						handle << String() << 0.0f << ((UInt32)0) << ((UInt32)0) << ((UInt32)0) << ((UInt32)0);
					}

					if (!pass.second.geometryShader.func.isEmpty())
					{
						auto tempFun = findFunc(pass.second.geometryShader.func);
						if (tempFun)
						{
							handle << pass.second.geometryShader.func << pass.second.geometryShader.version;
							handle << static_cast<UInt32>(tempFun->arguments.size());
							for (const auto& x : tempFun->arguments)
							{
								handle << x;
							}
							saveVectorOfUInts(pass.second.geometryShader.usedCBuffers, handle);
							saveVectorOfUInts(pass.second.geometryShader.usedUAVs, handle);
							saveVectorOfUInts(pass.second.geometryShader.usedTextures, handle);
							for (const auto& x : pass.second.geometryShader.usedTextures)
							{
								UInt32 tempUInt32 = shader.textures[x].samplerID;
								handle << tempUInt32;
							}
						}
					}
					else
					{
						handle << String() << 0.0f << ((UInt32)0) << ((UInt32)0) << ((UInt32)0) << ((UInt32)0);
					}

					if (!pass.second.pixelShader.func.isEmpty())
					{
						auto tempFun = findFunc(pass.second.pixelShader.func);
						if (tempFun)
						{
							handle << pass.second.pixelShader.func << pass.second.pixelShader.version;
							handle << static_cast<UInt32>(tempFun->arguments.size());
							for (const auto& x : tempFun->arguments)
							{
								handle << x;
							}
							saveVectorOfUInts(pass.second.pixelShader.usedCBuffers, handle);
							saveVectorOfUInts(pass.second.pixelShader.usedUAVs, handle);
							saveVectorOfUInts(pass.second.pixelShader.usedTextures, handle);
							for (const auto& x : pass.second.pixelShader.usedTextures)
							{
								UInt32 tempUInt32 = shader.textures[x].samplerID;
								handle << tempUInt32;
							}
						}
					}
					else
					{
						handle << String() << 0.0f << ((UInt32)0) << ((UInt32)0) << ((UInt32)0) << ((UInt32)0);
					}

					handle << (UInt32)pass.second.owner->variants.size();
					for (const auto& variant : pass.second.owner->variants)
					{
						firstHashPart += variant.first.asNumberString();
						Hash tempHash(firstHashPart);
						handle << tempHash << variant.first;
						//ResourceMgr::SubresourceInfo tempInfo;
						//tempInfo.type = ResourceMgr::SubresourceType::ShaderCollection;
						subresources.emplace(tempHash, ResourceMgr::SubresourceType::ShaderCollection);
						info.internalDependiencies.emplace_back(tempHash, std::make_pair(tempHash, true));
						firstHashPart += ':';

						if (!pass.second.computeShader.func.isEmpty())
						{
							tempHash.hash(firstHashPart + toString(static_cast<UInt32>(ShaderType::ComputeShader)));
							handle << tempHash;
						}
						else
						{
							handle << Hash();
						}

						if (!pass.second.vertexShader.func.isEmpty())
						{
							tempHash.hash(firstHashPart + toString(static_cast<UInt32>(ShaderType::VertexShader)));
							handle << tempHash;
						}
						else
						{
							handle << Hash();
						}

						if (!pass.second.geometryShader.func.isEmpty())
						{
							tempHash.hash(firstHashPart + toString(static_cast<UInt32>(ShaderType::GeometryShader)));
							handle << tempHash;
						}
						else
						{
							handle << Hash();
						}

						if (!pass.second.pixelShader.func.isEmpty())
						{
							tempHash.hash(firstHashPart + toString(static_cast<UInt32>(ShaderType::PixelShader)));
							handle << tempHash;
						}
						else
						{
							handle << Hash();
						}

						firstHashPart = storedFirstHashPart;
					}
				}
			}
		}

		files.back().second.first = handle.getString();
		FilePath tempPath = inputPath;
		tempPath.removeExtension();

		subresources.emplace(u8"Effect", std::move(info));
	}

	log("Effect saved to .geff file (in memory).");
	log("HLSL shaders prepared.");

	{
		files.emplace_back(std::piecewise_construct, std::forward_as_tuple(tempHLSL), std::tuple<>());
		Map<Hash, ResourceMgr::SubresourceInfo>& subresources = files.back().second.second;
		StringLoadSaveHandle handle;
		handle << hlslHeader;

		handle << (UInt32)shader.techniqueGroups.size();
		for (const auto& x : shader.techniqueGroups)
		{
			handle << (UInt32)x.second.size();
			for (const auto& y : x.second)
			{
				handle << (UInt32)y.second.passes.size();
				for (const auto& z : y.second.passes)
				{
					const auto& shaderCode = shaderCodes.at(x.first).at(y.first).at(z.first);
					String firstHashPart(x.first + ':' + y.first + ':' + toString(z.first));

					handle << Hash(firstHashPart) << (UInt32)shaderCode.size();
					for (const auto& w : shaderCode)
					{
						const Hash tempHash(firstHashPart + toString(static_cast<UInt32>(w.first)));
						handle << tempHash << w.first << w.second;
						//ResourceMgr::SubresourceInfo info;
						//info.type = ResourceMgr::SubresourceType::HLSLShaderCode;
						subresources.emplace(tempHash, ResourceMgr::SubresourceType::HLSLShaderCode);
					}

					const bool isComputeShaderPresent = shaderCode.count(ShaderType::ComputeShader);
					const bool isVertexShaderPresent = shaderCode.count(ShaderType::VertexShader);
					const bool isGeometryShaderPresent = shaderCode.count(ShaderType::GeometryShader);
					const bool isPixelShaderPresent = shaderCode.count(ShaderType::PixelShader);

					handle << (UInt32)z.second.owner->variants.size() << (UInt32)z.second.owner->variants.begin()->second.size();
					for (const auto& w : z.second.owner->variants)
					{
						String commonHashPart(firstHashPart + ':' + w.first.asNumberString());
						handle << Hash(commonHashPart);
						commonHashPart += ':';
						if (isComputeShaderPresent)
						{
							handle << Hash(commonHashPart + toString(static_cast<UInt32>(ShaderType::ComputeShader)));
						}
						else
						{
							handle << Hash();
						}

						if (isVertexShaderPresent)
						{
							handle << Hash(commonHashPart + toString(static_cast<UInt32>(ShaderType::VertexShader)));
						}
						else
						{
							handle << Hash();
						}

						if (isGeometryShaderPresent)
						{
							handle << Hash(commonHashPart + toString(static_cast<UInt32>(ShaderType::GeometryShader)));
						}
						else
						{
							handle << Hash();
						}

						if (isPixelShaderPresent)
						{
							handle << Hash(commonHashPart + toString(static_cast<UInt32>(ShaderType::PixelShader)));
						}
						else
						{
							handle << Hash();
						}

						for (const auto& tempInt : w.second)
						{
							handle << (UInt32)tempInt;
						}
					}
				}
			}
		}

		files.back().second.first = handle.getString();
	}

	log("Shaders' HLSL code saved to .ghlsl file (in memory).");
	log("Effect fully compiled.");

	return true;
}
