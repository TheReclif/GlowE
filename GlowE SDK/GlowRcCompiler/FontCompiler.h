#pragma once
#ifndef GLOWRCCOMPILER_FONTCOMPILER_INCLUDED
#define GLOWRCCOMPILER_FONTCOMPILER_INCLUDED

#include <GlowSystem/Utility.h>
#include <GlowSystem/Filesystem.h>
#include <GlowEngine/Resource.h>

using namespace GLOWE;

bool compileFont(const FilePath& inputPath, const String& /*packageName*/, const String& contents, Vector<Pair<FilePath, Pair<String, Map<Hash, ResourceMgr::SubresourceInfo>>>>& files);

#endif
