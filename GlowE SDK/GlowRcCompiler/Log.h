#pragma once
#ifndef GLOWRCCOMPILER_LOG_INCLUDED
#define GLOWRCCOMPILER_LOG_INCLUDED

#include <GlowSystem/ThreadClass.h>
#include <GlowSystem/MemoryMgr.h>
#include <GlowSystem/Log.h>

using namespace GLOWE;

class Log
{
private:
	Mutex mutex;
public:
	bool loggingEnabled = true;
public:
	void operator()(const String& str)
	{
		if (loggingEnabled)
		{
			DefaultLockGuard lock(mutex);
			logMessage(str);
			std::cout << str << std::endl;
		}
	}

	void operator()(const char* str)
	{
		if (loggingEnabled)
		{
			DefaultLockGuard lock(mutex);
			logMessage(str);
			std::cout << str << std::endl;
		}
	}

	static Log& getLogger()
	{
		static Log log;
		return log;
	}
};

void log(const String& str);
void log(const char* str);

#endif
