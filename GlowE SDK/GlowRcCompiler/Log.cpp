#include "Log.h"

void log(const String& str)
{
	Log::getLogger()(str);
}

void log(const char* str)
{
	Log::getLogger()(str);
}
