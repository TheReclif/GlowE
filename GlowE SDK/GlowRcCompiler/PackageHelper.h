#pragma once
#ifndef GLOWRCCOMPILER_PACKAGEHELPER_INCLUDED
#define GLOWRCCOMPILER_PACKAGEHELPER_INCLUDED

#include <GlowSystem/Package.h>
#include <GlowEngine/Resource.h>
#include <GlowSystem/Filesystem.h>

class PackageHelper
{
public:
	GLOWE::String packageName;
	GLOWE::UInt64 version;
	GLOWE::Package package;
	GLOWE::Map<GLOWE::Hash, GLOWE::ResourceMgr::ResourceFile> files;
	GLOWE::Map<GLOWE::Hash, GLOWE::ResourceMgr::ResourceFile> filesOnDisk;

	static bool createPackage(const GLOWE::FilePath& path);
	static bool verifyVersion(const GLOWE::UInt64 ver);
public:
	PackageHelper() = default;
	~PackageHelper();

	bool open(const GLOWE::FilePath& path);
	void applyChanges();
	void close();

	void removeFile(const GLOWE::FilePath& path);

	void addFile(const GLOWE::FilePath& path, const GLOWE::FilePath& internalPath, GLOWE::Map<GLOWE::Hash, GLOWE::ResourceMgr::SubresourceInfo>&& subresources);
	void addFileFromMemory(const GLOWE::String& buffer, const GLOWE::FilePath& internalPath, GLOWE::Map<GLOWE::Hash, GLOWE::ResourceMgr::SubresourceInfo>&& subresources);
	void addFileOnDisk(const GLOWE::FilePath& internalPath, GLOWE::Map<GLOWE::Hash, GLOWE::ResourceMgr::SubresourceInfo>&& subresources);

	void addSubresource(const GLOWE::Hash& file, GLOWE::Pair<GLOWE::Hash, GLOWE::ResourceMgr::SubresourceInfo>&& subresource);

	static inline GLOWE::String createInternalPath(const GLOWE::String& filePath, const GLOWE::String& packageName)
	{
		GLOWE::String result;

		if (packageName.getSize())
		{
			result =  packageName + '/' + filePath;
		}
		else
		{
			result = filePath;
		}

		result.replace("\\", "/");

		return result;
	}
};

#endif
