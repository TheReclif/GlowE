#include "PackageHelper.h"

bool PackageHelper::createPackage(const GLOWE::FilePath & path)
{
	using namespace GLOWE;

	Package package;
	package.open(path, Package::OpenMode::Write);
	if (!package.checkIsOpen())
	{
		return false;
	}

	PackageLoadSaveHandle handle(package, ResourceMgr::packageDefinitionFileName);
	constexpr UInt64 version = versionNumberUInt64;
	FilePath tempPath = path;
	tempPath.removeExtension();
	String tempStr = tempPath.getFilename();
	handle << version << tempStr << ((UInt32)0) << ((UInt32)0);

	return true;
}

bool PackageHelper::verifyVersion(const GLOWE::UInt64 ver)
{
	return GLOWE::versionNumberUInt64 == ver;
}

PackageHelper::~PackageHelper()
{
	close();
}

bool PackageHelper::open(const GLOWE::FilePath & path)
{
	using namespace GLOWE;

	auto loadSubresourceInfo = [](LoadSaveHandle& handle) -> ResourceMgr::SubresourceInfo
	{
		ResourceMgr::SubresourceInfo result;
		UInt32 temp;
		Hash tempHash, tempHash2, tempHash3;
		bool tempBool{};

		handle >> result.type >> temp;

		// Internal deps.
		for (UInt32 x = 0; x < temp; ++x)
		{
			handle >> tempHash >> tempHash2 >> tempBool;
			result.internalDependiencies.emplace_back(std::make_pair(tempHash, std::make_pair(tempHash2, tempBool)));
		}

		handle >> temp;
		// External deps.
		for (UInt32 x = 0; x < temp; ++x)
		{
			handle >> tempHash >> tempHash2 >> tempHash3 >> tempBool;
			result.externalDependiencies.emplace_back(std::make_pair(tempHash, std::tuple<Hash, Hash, bool>(tempHash2, tempHash3, tempBool)));
		}

		return std::move(result);
	};

	if (package.checkIsOpen())
	{
		close();
	}

	package.open(path, GLOWE::Package::OpenMode::Read);
	if (!package.checkIsOpen())
	{
		if (!createPackage(path))
		{
			return false;
		}

		package.open(path, GLOWE::Package::OpenMode::Read);
		if (!package.checkIsOpen())
		{
			return false;
		}
	}
	PackageEntryRead entry;
	entry.open(package, ResourceMgr::packageDefinitionFileName);
	PackageLoadSaveHandle handle(entry);
	UInt32 onDiskCount, inPackagesCount, tempUInt32;
	Hash tempHash, tempHash2;
	FilePath tempPath;

	handle >> version >> packageName >> inPackagesCount >> onDiskCount;
	// TODO: Handling.
	if (!verifyVersion(version))
	{
		WarningThrow(false, "Incompatible package " + path.getString() + ". Package version " + toString(version) + " incompatible with " + toString(GLOWE::versionNumberUInt64) + ".");
		return false;
	}

	Map<Hash, ResourceMgr::SubresourceInfo> subresources;

	// Packaged resources.
	for (UInt32 x = 0; x < inPackagesCount; ++x)
	{
		handle >> tempHash >> tempPath >> tempUInt32;
		for (UInt32 y = 0; y < tempUInt32; ++y)
		{
			handle >> tempHash2;
			subresources.emplace(tempHash2, loadSubresourceInfo(handle));
		}

		files.emplace(std::piecewise_construct, std::forward_as_tuple(tempHash), std::forward_as_tuple(std::move(subresources), tempPath));
		subresources = Map<Hash, ResourceMgr::SubresourceInfo>();
	}

	// Resources on disk.
	for (UInt32 x = 0; x < onDiskCount; ++x)
	{
		handle >> tempHash >> tempPath >> tempUInt32;
		for (UInt32 y = 0; y < tempUInt32; ++y)
		{
			handle >> tempHash2;
			subresources.emplace(tempHash2, loadSubresourceInfo(handle));
		}

		files.emplace(std::piecewise_construct, std::forward_as_tuple(tempHash), std::forward_as_tuple(std::move(subresources), tempPath));
		subresources = Map<Hash, ResourceMgr::SubresourceInfo>();
	}
	package.close();
	package.open(path, Package::OpenMode::Write);
	if (!package.checkIsOpen())
	{
		close();
		return false;
	}

	return true;
}

void PackageHelper::applyChanges()
{
	using namespace GLOWE;

	auto saveSubresourceInfo = [](LoadSaveHandle& handle, const ResourceMgr::SubresourceInfo& info)
	{
		UInt32 temp = info.internalDependiencies.size();

		handle << info.type << temp;

		// Internal deps.
		for (const auto& x : info.internalDependiencies)
		{
			handle << x.first << x.second.first << x.second.second;
		}

		temp = info.externalDependiencies.size();
		handle << temp;

		// External deps.
		for (const auto& x : info.externalDependiencies)
		{
			handle << x.first << std::get<0>(x.second) << std::get<1>(x.second) << std::get<2>(x.second);
		}
	};

	if (package.checkIsOpen())
	{
		{
			PackageLoadSaveHandle handle(package, GLOWE::ResourceMgr::packageDefinitionFileName);
			const UInt32 packagedResCount = files.size(), onDiskResCount = filesOnDisk.size();
			handle << GLOWE::versionNumberUInt64 << packageName << packagedResCount << onDiskResCount;

			for (const auto& x : files)
			{
				const UInt32 temp = x.second.subresources.size();
				handle << x.first << x.second.path << temp;
				for (const auto& y : x.second.subresources)
				{
					handle << y.first;
					saveSubresourceInfo(handle, y.second);
				}
			}

			for (const auto& x : filesOnDisk)
			{
				const UInt32 temp = x.second.subresources.size();
				handle << x.first << x.second.path << temp;
				for (const auto& y : x.second.subresources)
				{
					handle << y.first;
					saveSubresourceInfo(handle, y.second);
				}
			}
		}

		package.applyChanges();
	}
}

void PackageHelper::close()
{
	if (package.checkIsOpen())
	{
		applyChanges();
		package.close();
	}
	packageName.clear();
	version = 0;
	files.clear();
	filesOnDisk.clear();
}

void PackageHelper::removeFile(const GLOWE::FilePath & path)
{
	if (package.checkIsOpen() && package.checkFileForExistence(path))
	{
		package.deleteFile(path);
		auto it = files.find(path.getString());
		if (it != files.end())
		{
			files.erase(it);
		}
	}
	else
	{
		auto it = filesOnDisk.find(path.getString());
		if (it != filesOnDisk.end())
		{
			filesOnDisk.erase(it);
		}
	}
}

void PackageHelper::addFile(const GLOWE::FilePath& path, const GLOWE::FilePath& internalPath, GLOWE::Map<GLOWE::Hash, GLOWE::ResourceMgr::SubresourceInfo>&& subresources)
{
	using namespace GLOWE;

	// Real internal path.
	const String resultingPath = createInternalPath(internalPath, packageName);

	const auto it = files.find(resultingPath);
	if (it != files.end())
	{
		// Replace files.
		files.erase(it);
	}

	//ResourceMgr::ResourceFile resFile(subresources, resultingPath);
	files.emplace(std::piecewise_construct, std::forward_as_tuple(resultingPath.getString()), std::forward_as_tuple(std::move(subresources), internalPath));
	package.addFileFromFile(internalPath, path);
}

void PackageHelper::addFileFromMemory(const GLOWE::String& buffer, const GLOWE::FilePath& internalPath, GLOWE::Map<GLOWE::Hash, GLOWE::ResourceMgr::SubresourceInfo>&& subresources)
{
	using namespace GLOWE;

	// Real internal path.
	const String resultingPath = createInternalPath(internalPath, packageName);

	const auto it = files.find(resultingPath);
	if (it != files.end())
	{
		// Replace files.
		files.erase(it);
	}

	files.emplace(std::piecewise_construct, std::forward_as_tuple(resultingPath), std::forward_as_tuple(std::move(subresources), internalPath));
	package.addFile(internalPath, buffer.getCharArray(), buffer.getSize());
}

void PackageHelper::addFileOnDisk(const GLOWE::FilePath & internalPath, GLOWE::Map<GLOWE::Hash, GLOWE::ResourceMgr::SubresourceInfo>&& subresources)
{
	using namespace GLOWE;

	const auto it = filesOnDisk.find(internalPath.getString());
	if (it != filesOnDisk.end())
	{
		// Replace files.
		filesOnDisk.erase(it);
	}

	filesOnDisk.emplace(std::piecewise_construct, std::forward_as_tuple(internalPath.getString()), std::forward_as_tuple(std::move(subresources), internalPath));
}

void PackageHelper::addSubresource(const GLOWE::Hash & file, GLOWE::Pair<GLOWE::Hash, GLOWE::ResourceMgr::SubresourceInfo>&& subresource)
{
	GLOWE::Map<GLOWE::Hash, GLOWE::ResourceMgr::ResourceFile>::iterator it = files.find(file);
	if (it != files.end())
	{
		it->second.subresources.insert(std::move(subresource));
	}
	else
	{
		it = filesOnDisk.find(file);
		if (it != filesOnDisk.end())
		{
			it->second.subresources.insert(std::move(subresource));
		}
	}
}
