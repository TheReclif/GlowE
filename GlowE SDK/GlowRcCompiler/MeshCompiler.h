#pragma once
#ifndef GLOWRCCOMPILER_MESHCOMPILER_INCLUDED
#define GLOWRCCOMPILER_MESHCOMPILER_INCLUDED

#include <GlowSystem/Utility.h>
#include <GlowSystem/Filesystem.h>
#include <GlowEngine/Resource.h>

using namespace GLOWE;

bool compileMesh(const FilePath& inputPath, const String& packageName, const String& contents, Vector<Pair<FilePath, Pair<String, Map<Hash, ResourceMgr::SubresourceInfo>>>>& files);

#endif
