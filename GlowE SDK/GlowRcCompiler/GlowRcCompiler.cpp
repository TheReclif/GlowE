﻿#include <GlowAppFramework\AppFramework.h>
#include <GlowSystem\Filesystem.h>
#include <GlowSystem\FileIO.h>
#include <GlowEngine\Resource.h>
#include <GlowWindow\Uniform\Window.h>
#include <GlowSystem\Regex.h>
#include <GlowSystem\UUID.h>
#include <GlowGraphics\Uniform\GSILShaderCompiler.h>
#include <GlowEngine\Effect.h>
#include <GlowGraphics\Uniform\ShaderImplementation.h>

#include "PackageHelper.h"
#include "Log.h"
#include "EffectCompiler.h"
#include "MeshCompiler.h"
#include "FontCompiler.h"

#include <FreeImage.h>

using namespace GLOWE;

enum class OutputResourceType
{
	Invalid,
	Effect,
	Texture,
	Mesh,
	Font
};

enum class CompilerMode
{
	None,
	Compile,
	PackageManagment
};

enum class PackageAction
{
	None,
	ShowAll,
	Remove
};

void showHelp()
{
	std::cout << "GlowRcCompiler compiles resources to make them GlowEngine compatible. The output is placed in a file named \"source.[resource specific extension]\".\n";
	std::cout << "Note: When using /pack option with no /saveAfterwards option, the output files won't be saved.\n";
	std::cout << "Usage:\n";
	std::cout << "GlowRcCompiler [/compile:source [/treatAs :resourceName] [/pack :packagePath :pathInPackage [/saveAfterwards]]] [/packManage :packagePath [/remove :pathInPackage] [/showAll]] [/disableLogging] [/noLogo]\n";
	std::cout << "  /compile :source\n";
	std::cout << "				 The compiler will compile the specified file.\n";
	std::cout << "  /treatAs :resourceName\n";
	std::cout << "               The compiler will ignore the file's extension and treat it as resourceName resource.\n";
	std::cout << "  /pack :packagePath :pathInPackage\n";
	std::cout << "               The compiler will add the resource to a package specified by packagePath and with pathInPackage internal path.\n";
	std::cout << "  /saveAfterwards\n";
	std::cout << "               The compiler will save the output files after packaging. If no /pack argument is present, this option has no effect.\n";
	std::cout << "  /packManage :packagePath\n";
	std::cout << "				 The compiler will open the specified package and perform specified actions (specified by /remove and /showAll arguments) with it.\n";
	std::cout << "  /disableLogging\n";
	std::cout << "				 The compiler will not log info data.\n";
	std::cout << "  /noLogo\n";
	std::cout << "				 GlowEngine logo won't be shown. Please don't do this or I'll be very sad (;-;).\n";
	std::cout << std::endl;
}

void showLogo()
{
	std::cout << u8"GlowE version: " << GLOWE::versionName << '\n'; 
	std::cout << GLOWE::version3dAsciiArt << '\n' << std::endl;
}

bool compileTexture2D(const FilePath& inputPath, const String& /*packageName*/, const String& contents, Vector<Pair<FilePath, Pair<String, Map<Hash, ResourceMgr::SubresourceInfo>>>>& files)
{
	log("Compiling a texture 2D.");

	files.emplace_back();
	files.back().first = inputPath;
	files.back().first.replaceExtension("gtex");

	// Don't do anything, just copy the contents to the appropriate string.

	TextureDesc texDesc;
	texDesc.arraySize = 1;
	texDesc.binding = TextureDesc::Binding::Default;
	texDesc.cpuAccess = CPUAccess::None;
	texDesc.usage = Usage::Default;
	texDesc.generateMipmaps = true;
	texDesc.type = TextureDesc::Type::Texture2D;

	FreeImage_Initialise(0);

	String finalContents;
	FIMEMORY* memory = FreeImage_OpenMemory((BYTE*)contents.getCharArray(), contents.getSize());
	FREE_IMAGE_FORMAT fileType = FreeImage_GetFileTypeFromMemory(memory);
	FIBITMAP* bitmap = FreeImage_LoadFromMemory(fileType, memory);
	FreeImage_CloseMemory(memory);

	//FreeImage_FlipHorizontal(bitmap);

	const FREE_IMAGE_COLOR_TYPE colType = FreeImage_GetColorType(bitmap);
	switch (colType)
	{
	case FREE_IMAGE_COLOR_TYPE::FIC_RGBALPHA:
		break;
	case FREE_IMAGE_COLOR_TYPE::FIC_MINISBLACK:
	{
		FIBITMAP* temp = FreeImage_ConvertToStandardType(bitmap);

		FreeImage_Unload(bitmap);
		bitmap = temp;
	}
	break;
	case FREE_IMAGE_COLOR_TYPE::FIC_RGB: // Intended.
	default:
	{
		if (fileType == FREE_IMAGE_FORMAT::FIF_JPEG)
		{
			fileType = FREE_IMAGE_FORMAT::FIF_JP2;
		}

		FIBITMAP* temp = FreeImage_ConvertTo32Bits(bitmap);

		FreeImage_Unload(bitmap);
		bitmap = temp;
	}
	break;
	}

	memory = FreeImage_OpenMemory();
	FreeImage_SaveToMemory(fileType, bitmap, memory, 0);

	BYTE* data = nullptr;
	DWORD size{};

	FreeImage_AcquireMemory(memory, &data, &size);
	finalContents.resize(size);
	std::memcpy(finalContents.getCharArray(), data, size);

	FreeImage_CloseMemory(memory);
	FreeImage_Unload(bitmap);

	FreeImage_DeInitialise();

	StringLoadSaveHandle handle;
	handle << texDesc << finalContents;

	files.back().second.first = handle.getString();
	files.back().second.second.emplace(Hash(), ResourceMgr::SubresourceType::Texture);
	
	return true;
}

OutputResourceType stringToResourceType(const String& arg)
{
	static const Set<String> textureExtensions{ "bmp", "cut", "dds", "exr", "g3", "gif", "hdr", "ico", "iff", "lbm", "j2k", "j2c", "jng", "jp2", "jpg", "jif", "jpeg", "jpe", "jxr", "wdp", "hdp", "koa", "mng", "pbm", "pcd", "pcx", "pfm", "pgm", "pct", "pict", "pic", "png", "ppm", "psd", "ras", "sgi", "tga", "targa", "tif", "tiff", "wap", "wbmp", "wbm", "webp", "xbm", "xpm", "raw", "texture" };
	static const Set<String> meshExtensions{ "obj" }; // When adding a support for new files types, add them here.
	static const Set<String> fontExtensions{ "ttf", "otf", "otc", "ttc", "tte", "fon", "font" };

	String temp = arg;
	temp.toLowercase();
	if (temp[0] == '.')
	{
		temp.erase(0, 1);
	}
	if (temp == "gsil")
	{
		return OutputResourceType::Effect;
	}
	else 
	if (textureExtensions.count(temp) > 0)
	{
		return OutputResourceType::Texture;
	}
	else
	if (meshExtensions.count(temp) > 0)
	{
		return OutputResourceType::Mesh;
	}
	else
	if (fontExtensions.count(temp) > 0)
	{
		return OutputResourceType::Font;
	}

	return OutputResourceType::Invalid;
}

void showAllInPackage(PackageHelper& package)
{
	static auto typeToString = [](const ResourceMgr::SubresourceType type) -> String
	{
		using Type = ResourceMgr::SubresourceType;
		switch (type)
		{
		case Type::Effect:
			return "Effect";
		case Type::GLSLShaderCode:
			return "GLSL Shader Code";
		case Type::HLSLShaderCode:
			return "HLSL Shader Code";
		case Type::Material:
			return "Material";
		case Type::ShaderCollection:
			return "Shader Collection";
		case Type::Texture:
			return "Texture";
		case Type::GameObjectBlueprint:
			return "Game Object Blueprint";
		case Type::Geometry:
			return "Geometry";
		case Type::GeometryBuffers:
			return "Geometry Buffers";
		case Type::Scene:
			return "Scene";
		case Type::Font:
			return "Font";
		default:
			return String();
		}
	};

	std::cout << "Resources inside the package:\n";
	for (const auto& x : package.files)
	{
		std::cout << "Resource " << x.first.asNumberString() << " in " << x.second.path << ".\nSubresources:\n\n";
		for (const auto& y : x.second.subresources)
		{
			std::cout << "Hash: " << y.first.asNumberString() << '\n';
			std::cout << "Type: " << typeToString(y.second.type) << '\n';
			std::cout << "Internal dependencies:\n";
			for (const auto& dep : y.second.internalDependiencies)
			{
				std::cout << "Name hash: " << dep.first << '\n';
				std::cout << "Dependency: " << dep.second.first << "\n\n";
			}
			std::cout << "External dependencies:\n";
			for (const auto& dep : y.second.externalDependiencies)
			{
				std::cout << "Name hash: " << dep.first << '\n';
				std::cout << "Dependency's resource: " << std::get<0>(dep.second) << '\n';
				std::cout << "Dependency's subresource: " << std::get<1>(dep.second) << "\n\n";
			}
			std::cout << '\n';
		}
		std::cout << '\n';
	}

	std::cout.flush();
}

void removeFromPackage(PackageHelper& package, const FilePath& internalPath)
{
	package.removeFile(internalPath);
}

GlowCmdApplication()
try
{
	if (arguments.size() <= 1 || arguments[1] == "\\?")
	{
		showHelp();
		return;
	}

	FilePath inputPath;
	String contents;
	CompilerMode compilerMode = CompilerMode::None;
	PackageAction packAction = PackageAction::None;

	static const auto checkIfArgumentIsValid = [&arguments](const unsigned int id) -> bool
	{
		return id < arguments.size() && arguments[id][0] != '/';
	};

	FilePath packPath, internalPath;
	bool saveAfterwards = false, logoEnabled = true;
	String ext;

	for (unsigned int x = 1; x < arguments.size(); ++x)
	{
		String temp = arguments[x];
		temp.toLowercase();
		if (temp == "/compile")
		{
			if (checkIfArgumentIsValid(x + 1))
			{
				inputPath = arguments[x + 1];
				ext = inputPath.getExtension();
				if (compilerMode == CompilerMode::None)
				{
					compilerMode = CompilerMode::Compile;
				}
				else
				{
					log("Invalid arguments (mode already set). Maybe you accidentally added /compile or /packManage option?");
					return;
				}
			}
			else
			{
				log("/compile argument missing.");
				return;
			}
		}
		else
		if (temp == "/treatas")
		{
			if (compilerMode != CompilerMode::Compile)
			{
				log("/treatAs is not valid here.");
				return;
			}
			if (checkIfArgumentIsValid(x + 1))
			{
				ext = arguments[x + 1];
			}
			else
			{
				log("/treatas argument missing.");
				return;
			}
		}
		else
		if (temp == "/pack")
		{
			if (checkIfArgumentIsValid(x + 1) && checkIfArgumentIsValid(x + 2))
			{
				packPath = arguments[x + 1];
				internalPath = arguments[x + 2];
			}
			else
			{
				log("/pack arguments missing.");
				return;
			}
		}
		else
		if (temp == "/saveafterwards")
		{
			saveAfterwards = true;
		}
		else
		if (temp == "/disablelogging")
		{
			Log::getLogger().loggingEnabled = false;
		}
		else
		if (temp == "/packmanage")
		{
			if (checkIfArgumentIsValid(x + 1))
			{
				packPath = arguments[x + 1];
				if (compilerMode == CompilerMode::None)
				{
					compilerMode = CompilerMode::PackageManagment;
				}
				else
				{
					log("Invalid arguments (mode already set). Maybe you accidently added /compile or /packManage option?");
					return;
				}
			}
			else
			{
				log("/packManage argument missing.");
				return;
			}
		}
		else
		if (temp == "/remove")
		{
			if (compilerMode != CompilerMode::PackageManagment || packAction != PackageAction::None)
			{
				log("Invalid argument /remove.");
				return;
			}

			if (checkIfArgumentIsValid(x + 1))
			{
				internalPath = arguments[x + 1];
				packAction = PackageAction::Remove;
			}
			else
			{
				log("/remove argument missing.");
				return;
			}
		}
		else
		if (temp == "/showall")
		{
			if (compilerMode != CompilerMode::PackageManagment || packAction != PackageAction::None)
			{
				log("Invalid argument /showAll.");
				return;
			}

			packAction = PackageAction::ShowAll;
		}
		else
		if (temp == "/nologo")
		{
			logoEnabled = false;
		}
	}

	if (Log::getLogger().loggingEnabled && logoEnabled)
	{
		showLogo();
	}

	switch (compilerMode)
	{
	case CompilerMode::Compile:
	{
		File file;
		file.open(inputPath, std::ios::in | std::ios::binary);
		if (!file.checkIsOpen())
		{
			log("Unable to open the input file.");
			return;
		}

		File::BOMInformation bomInfo = file.getBOMInfo();
		auto fileSize = file.getFileSize() - bomInfo.getBOMSize();

		switch (bomInfo.encoding)
		{
		case File::BOMInformation::FileEncoding::ASCII:
		case File::BOMInformation::FileEncoding::Utf8:
			contents.resize(fileSize);
			file.read(contents.getCharArray(), fileSize);
			break;
		default:
			log("The compiler is unable to read the file because it's encoded in UTF-16 or UTF-32. Please use UTF-8 or ASCII.");
			return;
		}

		if (contents.isEmpty())
		{
			log("The input file is empty.");
			return;
		}

		file.close();

		Vector<Pair<FilePath, Pair<String, Map<Hash, ResourceMgr::SubresourceInfo>>>> outputFiles;
		bool result;

		PackageHelper package;
		if (!packPath.isEmpty() && !internalPath.isEmpty())
		{
			FilePath packagePath = packPath;
			packagePath.replaceExtension("gpckg");
			if (!package.open(packagePath))
			{
				log("Unable to pack the resource. PackageHelper.open() failed.");
				return;
			}
		}

		switch (stringToResourceType(ext))
		{
		case OutputResourceType::Effect:
			result = compileEffect(inputPath, package.packageName, contents, outputFiles);
			break;
		case OutputResourceType::Texture:
			result = compileTexture2D(inputPath, package.packageName, contents, outputFiles);
			break;
		case OutputResourceType::Mesh:
			result = compileMesh(inputPath, package.packageName, contents, outputFiles);
			break;
		case OutputResourceType::Font:
			result = compileFont(inputPath, package.packageName, contents, outputFiles);
			break;
		default:
			log("Unknown resource type.");
			return;
		}

		if (result)
		{
			if (!packPath.isEmpty() && !internalPath.isEmpty())
			{
				FilePath tempPath = internalPath;

				// Place the resource in the specified package.
				log("Packaging.");
				
				for (auto& x : outputFiles)
				{
					tempPath.replaceExtension(x.first.getExtension());
					package.addFileFromMemory(x.second.first, tempPath, std::move(x.second.second));
					package.applyChanges();
				}
			}
			else
			{
				saveAfterwards = true;
			}

			if (saveAfterwards)
			{
				File file;
				FilePath tempPath = inputPath;
				for (const auto& x : outputFiles)
				{
					tempPath.replaceExtension(x.first.getExtension());
					file.open(tempPath, std::ios::out | std::ios::binary);
					if (file.checkIsOpen())
					{
						file.write(x.second.first.getCharArray(), x.second.first.getSize());
						file.close();
					}
				}
			}
		}
	}
	break;
	case CompilerMode::PackageManagment:
	{
		PackageHelper package;
		if (!package.open(packPath))
		{
			log("Unable to open the specified package.");
			return;
		}

		switch (packAction)
		{
		case PackageAction::Remove:
			removeFromPackage(package, internalPath);
			break;
		case PackageAction::ShowAll:
			showAllInPackage(package);
			break;
		}
	}
	break;
	default:
		log("Compiler mode not set. Maybe you forgot to add /compile or /packManage option?");
		return;
	}

	log("Done.");
}
catch (const Exception& e)
{
	log("ERROR:");
	log(e.what());
}
catch (const std::exception& e)
{
	log("ERROR:");
	log(e.what());
}
