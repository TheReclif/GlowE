#include "FontCompiler.h"
#include "Log.h"

#include <GlowEngine/GUI/Text.h>

bool compileFont(const FilePath& inputPath, const String&, const String& contents, Vector<Pair<FilePath, Pair<String, Map<Hash, ResourceMgr::SubresourceInfo>>>>& files)
{
	FT_Library& lib = getInstance<FontUtility>().getLibrary();
	Font font;
	FT_Face fakeFace;

	FT_Error error = FT_New_Memory_Face(lib, reinterpret_cast<const FT_Byte*>(contents.getCharArray()), contents.getSize(), -1, &fakeFace);
	if (error)
	{
		log("Unable to load a font. Error: " + toString(error));
		return false;
	}
	const UInt32 faces = fakeFace->num_faces;
	FT_Done_Face(fakeFace);
	FilePath tempPath = inputPath;
	tempPath.replaceExtension(".gfont");
	files.emplace_back(std::piecewise_construct, std::forward_as_tuple(std::move(tempPath)), std::tuple<>());
	StringLoadSaveHandle handle;
	Map<Hash, ResourceMgr::SubresourceInfo>& subres = files.back().second.second;

	log("Found " + toString(faces) + " fonts/faces in the font file.");
	handle << faces;
	for (unsigned int currFace = 0; currFace < faces; ++currFace)
	{
		font.loadFromMemory(contents.getCharArray(), contents.getSize(), currFace);
		if (font.checkIsLoaded())
		{
			handle << font.getHash() << currFace;
			subres.emplace(std::piecewise_construct, std::forward_as_tuple(font.getHash()), std::forward_as_tuple(ResourceMgr::SubresourceType::Font));
			log("Font Id:" + toString(currFace) + ". Name: " + font.getName() + '.');
		}
		else
		{
			handle << Hash() << -1;
			log("Unable to load face of id " + toString(currFace) + ".");
		}
	}

	handle << contents;
	files.back().second.first = handle.consumeString();

	return true;
}
