function (copy_runtime_dependencies_dll INPUT_RUNTIME_FILE SEARCH_PATH OUTPUT_DIR)
	file(GET_RUNTIME_DEPENDENCIES LIBRARIES "${INPUT_RUNTIME_FILE}" UNRESOLVED_DEPENDENCIES_VAR UNRES_DEPS_LIST RESOLVED_DEPENDENCIES_VAR RES_DEPS_LIST)
	foreach (DEP_PATH ${UNRES_DEPS_LIST})
		if (EXISTS "${SEARCH_PATH}/${DEP_PATH}")
			file (COPY "${SEARCH_PATH}/${DEP_PATH}" DESTINATION "${OUTPUT_DIR}")
			copy_runtime_dependencies_dll ("${OUTPUT_DIR}/${DEP_PATH}" "${SEARCH_PATH}" "${OUTPUT_DIR}")
		endif ()
	endforeach ()
	foreach (DEP_PATH ${RES_DEPS_LIST})
		get_filename_component(DEP_FILE "${DEP_PATH}" NAME)
		if (NOT EXISTS "${OUTPUT_DIR}/${DEP_FILE}")
			if (EXISTS "${SEARCH_PATH}/${DEP_FILE}")
				file (COPY "${SEARCH_PATH}/${DEP_FILE}" DESTINATION "${OUTPUT_DIR}")
				copy_runtime_dependencies_dll ("${OUTPUT_DIR}/${DEP_FILE}" "${SEARCH_PATH}" "${OUTPUT_DIR}")
			endif ()
		endif ()
	endforeach ()
endfunction ()

function (copy_runtime_dependencies INPUT_RUNTIME_FILE SEARCH_PATH OUTPUT_DIR)
	file(GET_RUNTIME_DEPENDENCIES EXECUTABLES "${INPUT_RUNTIME_FILE}" UNRESOLVED_DEPENDENCIES_VAR UNRES_DEPS_LIST)
	foreach (DEP_PATH ${UNRES_DEPS_LIST})
		file(GLOB_RECURSE FOUND_DLL_LIST RELATIVE "${SEARCH_PATH}" "${DEP_PATH}")
		#if (EXISTS "${SEARCH_PATH}/${DEP_PATH}")
		if (FOUND_DLL_LIST)
			list(GET FOUND_DLL_LIST 0 FOUND_DLL)
			file (COPY "${FOUND_DLL}" DESTINATION "${OUTPUT_DIR}")
			copy_runtime_dependencies_dll ("${FOUND_DLL}" "${SEARCH_PATH}" "${OUTPUT_DIR}")
		endif ()
	endforeach ()
endfunction ()