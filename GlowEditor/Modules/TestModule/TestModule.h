#pragma once
#ifndef GLOWEDITOR_TESTMODULE_INCLUDED
#define GLOWEDITOR_TESTMODULE_INCLUDED

#include <GlowEditor/Code/EditorModule.h>

#include <GlowScripting/BaseScriptBehaviour.h>

class TestModule
	: public GlowEditor::Module, public GLOWE::NoCopyNoMove, public GLOWE::BaseScriptBehaviour
{
public:
	TestModule() = default;
	virtual ~TestModule() = default;

	virtual WindowDesc getWindowDesc() const override;
	virtual int getOrder() const override;
	virtual const char* getDescription() const override;
	virtual GLOWE::UInt64 getVersion() const override;

	virtual void initialize(const GLOWE::Map<GLOWE::String, Module*>& loadedModules) override;
	virtual void initializeGUI(GlowEditor::EditorWindow& window) override;
	virtual void cleanUp() override;
};

GlowEditorModuleExport(TestModule);

#endif
