add_library(TestEditorModule SHARED "")

target_link_libraries(TestEditorModule PUBLIC GlowE GlowEditorLib)

if (ENABLE_JMC)
	set_target_properties(TestEditorModule PROPERTIES VS_JUST_MY_CODE_DEBUGGING ON)
endif ()

add_custom_command(TARGET TestEditorModule POST_BUILD
	COMMAND ${CMAKE_COMMAND} -E make_directory "$<TARGET_FILE_DIR:GlowEditor>/Editor Modules/"
	COMMAND ${CMAKE_COMMAND} -E copy_if_different
			$<TARGET_FILE:TestEditorModule>
			"$<TARGET_FILE_DIR:GlowEditor>/Editor Modules/"
)

target_sources(TestEditorModule
	PRIVATE
		TestModule.cpp
	PUBLIC
		TestModule.h
)
