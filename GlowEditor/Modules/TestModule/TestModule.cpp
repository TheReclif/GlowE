#include "TestModule.h"

#include <GlowComponents/GUIImageComponent.h>
#include <GlowComponents/GUIButtonComponent.h>

#include <GlowEditor/Code/EditorWindow.h>
#include <GlowEditor/Code/Editor.h>

#include "GlowEngine/GameObject.h"

using namespace GLOWE;

GlowEditor::Module::WindowDesc TestModule::getWindowDesc() const
{
	WindowDesc result{};
	result.needsWindow = true;
	result.title = "Test Window";
	return result;
}

int TestModule::getOrder() const
{
	return std::numeric_limits<int>::min();
}

const char* TestModule::getDescription() const
{
	return "GlowEditor Test Module";
}

UInt64 TestModule::getVersion() const
{
	return 1000;
}

void TestModule::initialize(const Map<String, Module*>& loadedModules)
{
}

UInt64 recursiveGetDirSize(const String& where)
{
	FileListing listing(where);

	UInt64 result = 0;
	for (const auto& x : listing)
	{
		if (x == "." || x == "..")
		{
			continue;
		}

		if (Filesystem::isDirectory(where + "/" + x))
		{
			result += recursiveGetDirSize(where + "/" + x);
		}
		else
		{
			result += Filesystem::getFileSizeOnDisk(where + "/" + x);
		}
	}

	return result;
}

void TestModule::initializeGUI(GlowEditor::EditorWindow& window)
{
	// Editor window playground.
	auto& clientArea = *window.getClientContentObject();

	/*
	auto& image = clientArea.addComponent<Components::GUIImage>();
	image.setColor(Color::white);
	image.stretchToParent();

	auto& button = clientArea.addComponent<Components::GUIButton>();
	button.addConstraintToThis(GUIConstraint(GUIConstraint::Type::FixedLeft, 10, &image, GUIConstraint::Edge::LeftUp));
	button.addConstraintToThis(GUIConstraint(GUIConstraint::Type::FixedRight, 10, &image, GUIConstraint::Edge::RightDown));
	button.addConstraintToThis(GUIConstraint(GUIConstraint::Type::FixedUp, 10, &image, GUIConstraint::Edge::LeftUp));
	button.setLocalSize({ 0, 50 });
	button.setOnClickCallback([](Components::GUIButton&)
	{
		String file = GlowEditor::openDirectoryDialog("Choose directory", ".");
		if (file.isEmpty())
		{
			return;
		}

		OutputDebugStringW(file.toWide().c_str());
		OutputDebugStringW(L"\n");

		Multimap<UInt64, String, std::greater<UInt64>> paths;
		FileListing	listing(file);
		for (const auto& x : listing)
		{
			if (x == "." || x == "..")
			{
				continue;
			}
			
			UInt64 size;
			if (Filesystem::isDirectory(file + "/" + x))
			{
				size = recursiveGetDirSize(file + "/" + x);
			}
			else
			{
				size = Filesystem::getFileSizeOnDisk(file + "/" + x);
			}
			paths.emplace(size, x);
		}

		UInt64 sum = 0;
		for (const auto& x : paths)
		{
			OutputDebugStringW(x.second.toWide().c_str());
			OutputDebugStringW(L": ");

			sum += x.first;
			double resultSize = x.first;
			String prefix;
			if (x.first >= GigaByte)
			{
				resultSize = static_cast<double>(x.first) / GigaByte;

				prefix = "G";
			}
			else if (x.first >= MegaByte)
			{
				resultSize = static_cast<double>(x.first) / MegaByte;

				prefix = "M";
			}
			else if (x.first >= KiloByte)
			{
				resultSize = static_cast<double>(x.first) / KiloByte;

				prefix = "K";
			}

			OutputDebugStringA((toString(resultSize) + " " + prefix + "B").getCharArray());
			OutputDebugStringW(L"\n");
		}

		double resultSize = sum;
		String prefix;
		if (sum >= GigaByte)
		{
			resultSize = static_cast<double>(sum) / GigaByte;

			prefix = "G";
		}
		else if (sum >= MegaByte)
		{
			resultSize = static_cast<double>(sum) / MegaByte;

			prefix = "M";
		}
		else if (sum >= KiloByte)
		{
			resultSize = static_cast<double>(sum) / KiloByte;

			prefix = "K";
		}

		OutputDebugStringA(("Total: " + toString(resultSize) + " " + prefix + "B").getCharArray());
		OutputDebugStringW(L"\n");
	});
	button.setText("Choose directory");
	button.getTextField()->setHorizontalAlignment(Components::GUITextField::TextHorizontalAlignment::Middle);
	button.getTextField()->setVerticalAlignment(Components::GUITextField::TextVerticalAlignment::Middle);*/
	
	auto& image = clientArea.addComponent<Components::GUIImage>();
	image.setColor(Color::white);
	image.stretchToParent();

	auto& button = clientArea.addComponent<Components::GUIButton>();
	button.setLocalGUIRect(GUIRect({}, { 0, 150 }));
	button.addConstraintToThis(GUIConstraint(GUIConstraint::Type::FixedUp, 10, &image, GUIConstraint::Edge::LeftUp));
	button.addConstraintToThis(GUIConstraint(GUIConstraint::Type::FixedLeft, 10, &image, GUIConstraint::Edge::LeftUp));
	button.addConstraintToThis(GUIConstraint(GUIConstraint::Type::FixedRight, 10, &image, GUIConstraint::Edge::RightDown));

	button.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ultrices tellus vel placerat iaculis. Etiam faucibus enim sit amet nunc dapibus maximus. Suspendisse lacinia accumsan ipsum, condimentum luctus massa efficitur sed. Vivamus fringilla cursus lacus, ac auctor nunc. Ut laoreet finibus lectus, id eleifend sem volutpat et. Sed vel dolor turpis. Vestibulum tincidunt, sapien sit amet finibus euismod, tellus lacus efficitur nibh, ut venenatis nulla nibh euismod enim. Duis bibendum mauris in aliquam varius. Curabitur condimentum neque sed diam mollis, et ornare ipsum tincidunt.");
	button.getTextField()->setHorizontalAlignment(Components::GUITextField::TextHorizontalAlignment::Middle);
	button.setOnClickCallback([](Components::GUIButton& button)
	{
		static unsigned int counter = 0;
		counter = (counter + 1) % 3;
		button.getTextField()->setVerticalAlignment(static_cast<Components::GUITextField::TextVerticalAlignment>(counter));
	});
}

void TestModule::cleanUp()
{
}
