﻿#include "Editor.h"

#include <GlowEngine/FPSCounterUtility.h>
#include <GlowEngine/GameObject.h>

#include <GlowSystem/Filesystem.h>

GlowEditor::EditorApp* GlowEditor::EditorApp::editorApp = nullptr;

#include <GlowComponents/ScriptBehaviour.h>

#ifdef GLOWE_COMPILE_FOR_WINDOWS
#include <atlbase.h>
#include <shobjidl.h>
#endif

using namespace GLOWE;

class DebugPrintTestScript
	: public GLOWE::Components::ScriptBehaviour
{
private:
	// Variables here.
	const GLOWE::GUIElement* lastElement = nullptr;
	bool debugDrawGUI;
public:
	GLOWE::Components::GUITop* top = nullptr;
	GLOWE::Components::GUITextField* debugTextField = nullptr;
private:
	const GLOWE::GUIElement* getCurrentElement(const GLOWE::Float2& mousePos, const GLOWE::GUIContainer* const container) const
	{
		const auto end = container->rend();
		for (auto x = container->rbegin(); x != end; ++x)
		{
			const auto globalRect = (*x)->getGlobalGUIRect();
			if (globalRect.isPointInside(mousePos))
			{
				const auto asContainer = dynamic_cast<GLOWE::GUIContainer*>(*x);
				if (asContainer)
				{
					return getCurrentElement(mousePos, asContainer);
				}
				else
				{
					return *x;
				}
			}
		}

		return container;
	}
public:
	inline DebugPrintTestScript(GLOWE::GameObject* const newParent)
		: ScriptBehaviour(newParent), debugDrawGUI(false)
	{
	}

	virtual void update() override
	{
		using namespace GLOWE;
		if (debugDrawGUI)
		{
			ComponentLock lock(*top);
			top->dbgDrawRects(Time::ZeroTime, Color::green);
		}

		const auto mousePos = getInstance<InputMgr>().getMousePosition(engine.getWindow());
		const auto currElem = getCurrentElement({ static_cast<float>(mousePos[0]), static_cast<float>(mousePos[1]) }, top);
		if (currElem != lastElement)
		{
			lastElement = currElem;
			if (currElem)
			{
				const auto asComponent = dynamic_cast<const Component*>(currElem);
				auto rect = currElem->getLocalGUIRect();
				const String rectDesc = toString(rect.x) + ": " + toString(rect.y) + ": " + toString(rect.width) + ": " + toString(rect.height);
				rect = currElem->getGlobalGUIRect();
				const String globalRectDesc = toString(rect.x) + ": " + toString(rect.y) + ": " + toString(rect.width) + ": " + toString(rect.height);
				ComponentLock lock(*debugTextField);
				GUIElement::GuiMutexLock guard(GUIElement::guiMutex);
				if (asComponent)
				{
					debugTextField->setText(asComponent->getGameObject()->getPath() + ": " + toString(currElem) + ": " + asComponent->getTypeName() + "\n" + rectDesc+ "\n" + globalRectDesc);
				}
				else
				{
					debugTextField->setText("NO GAME OBJECT HIERARCHY: " + toString(currElem) + "\n" + rectDesc + "\n" + globalRectDesc);
				}
			}
		}
	}
	virtual void processEvent(const GLOWE::GameEvent& event) override
	{
		using namespace GLOWE;
		if (event == GameEvent::Type::System && event.info.systemEvent == Event::Type::KeyPressed)
		{
			if (event.info.systemEvent.keyInfo.key == Keyboard::Key::Tilde)
			{
				debugDrawGUI = !debugDrawGUI;
			}
		}
	}

	// Place GlowSerialize instructions here.
	GlowSerializeScript(DebugPrintTestScript, GlowBases());
};

GlowInitScriptReflection(DebugPrintTestScript);

GlowEditor::EditorApp::~EditorApp()
{
	modulesOrder.clear();
	contexts.clear();
	modules.clear();
	libs.clear();
}

class FPSCounterGUIUpdater
	: public GLOWE::Components::ScriptBehaviour
{
private:
	float fps;
public:
	GLOWE::Components::GUITextField* fpsText;
public:
	GlowSerializeScript(FPSCounterGUIUpdater, GlowBases());

	FPSCounterGUIUpdater(GLOWE::GameObject* const newParent);

	virtual void update() override;
};

GlowInitScriptReflection(FPSCounterGUIUpdater);

void GlowEditor::EditorApp::initialize()
{
	editorApp = this;

	getInstance<Engine>().setCallbackAfterInitialize([this](Engine& engine)
	{
		engine.setShouldPresentThisFramePredicate([this]() -> bool
			{
				return exchange(isMainWindowDirty, false) || windowSystemTop->checkIsAnythingToRedraw();
			});
		engine.getWindow().setIsResizable(true);

		Scene& scene = Scene::createEmptyScene(u8"Editor Scene");
#ifdef DEBUG
		GUIContainer::setRecursiveCycleDetectionEnabled(true);
#endif

		loadLayout();
		loadModules();

		GameObject& fpsCounter = scene.createGameObject("FPS Counter");
		fpsCounter.setParent(windowSystemTop->getGameObject());
		auto& textField = fpsCounter.addComponent<Components::GUITextField>(AutoFindParentOnStart());
		fpsCounter.addComponent<FPSCounterGUIUpdater>().fpsText = &textField;
		textField.setParent(windowSystemTop);
		textField.setFontSize(14);
		textField.setLocalGUIRect(GUIRect({ 0, 0 }, { 120, 20 }));
		textField.setColor(Color::white);

		debugTextField->placeAtBack();
	});
}

void GlowEditor::EditorApp::cleanUp()
{
	saveLayout();
}

void GlowEditor::EditorApp::markMainWindowDirty()
{
	isMainWindowDirty = true;
}

void GlowEditor::EditorApp::loadModules()
{
	Map<Module*, String> moduleNames;
	UnorderedSet<String> pluginsToIgnore;

	if (Filesystem::isExisting("./PluginsIgnore.txt"))
	{
		File file;
		file.open("./PluginsIgnore.txt", File::OpenFlag::In);
		if (file.checkIsOpen())
		{
			String line;
			while (std::getline(file.getStream(), line.getString()))
			{
				pluginsToIgnore.emplace(line);
			}
		}
	}

	FileListing listing;
	listing.create("./Editor Modules/*.dll");

	for (const auto& mod : listing)
	{
		if (pluginsToIgnore.count(mod) > 0)
		{
			GLOWE::logMessage("Ignoring module " + mod);
			continue;
		}
		FilePath path = "Editor Modules/" + mod;
		contexts.emplace_back();
		libs.emplace_back();
		if (!libs.back().load(path.getString().getCharArray()))
		{
			WarningThrow(false, "Unable to load " + path.getString() + ".");
			continue;
		}
		String scriptName;
		const auto getModuleClassNameFunc = static_cast<const char* (__stdcall *)()>(libs.back().getMethod("getModuleClassName"));
		if (getModuleClassNameFunc)
		{
			scriptName = getModuleClassNameFunc();
		}
		else
		{
			path.removeExtension();
			scriptName = path.getFilename();
		}
		const CppScript::Script* tempScr = contexts.back().createScript(scriptName, libs.back());
		if (tempScr && tempScr->getBehaviourPtr())
		{
			Module* const moduleTemp = tempScr->getBehaviourAs<Module>();
			if (moduleTemp)
			{
				const int order = modules.emplace(std::piecewise_construct, std::forward_as_tuple(scriptName), std::forward_as_tuple(moduleTemp)).first->second->getOrder();
				modulesOrder.emplace(order, moduleTemp);
				moduleNames.emplace(moduleTemp, scriptName);
			}
		}
		else
		{
			WarningThrow(false, "Unable to create script of type " + scriptName + ". Check \"GlowEditorModuleExport\" macro in module header file.");
		}
	}

	for (auto& mod : modulesOrder)
	{
		const String& modName = moduleNames.at(mod.second);
		mod.second->initialize(modules);
		const auto wndDesc = mod.second->getWindowDesc();
		if (wndDesc.needsWindow)
		{
			EditorWindow* const wnd = getInstance<EditorWindowMgr>().getWindow(modName);

			if (wnd)
			{
				if (wndDesc.title)
				{
					wnd->setTitle(wndDesc.title);
				}

				mod.second->initializeGUI(*wnd);
			}
			else
			{
				WarningThrow(false, "Unable to find a preallocated window or allocate a window for the following module: " + modName + ".");
			}
		}
	}
}

void GlowEditor::EditorApp::createDefaultLayout()
{
	throw NotImplementedException();
}

bool GlowEditor::EditorApp::loadLayout(const bool retry)
{
	// TODO: Implement
	//throw NotImplementedException();
	GameObject& obj = GameObject::createGameObject("Editor Window System", nullptr, true);
	windowSystemTop = &obj.addComponent<Components::GUITop>();

	GameObject& debugObj = GameObject::createGameObject("Debug Object", &obj, true);
	auto& debugPrintTestScript = debugObj.addComponent<DebugPrintTestScript>();

	debugPrintTestScript.top = windowSystemTop;
	debugPrintTestScript.debugTextField = &debugObj.addComponent<Components::GUITextField>();
	debugPrintTestScript.debugTextField->setFontSize(14);
	debugPrintTestScript.debugTextField->setLocalGUIRect(GUIRect({ 120, 0 }, { 600, 350 }));
	debugPrintTestScript.debugTextField->setColor(Color::white);

	debugTextField = debugPrintTestScript.debugTextField;

	windowSystemTop->setNotOwnedRenderTarget(&getInstance<Engine>().getWindow().getSwapChain().getRenderTarget());
	windowSystemTop->setEventProcessing(true);
	windowSystemTop->setShouldClearRT(true);
	windowSystemTop->setClearColor(backgroundColor);

	return true;
}

void GlowEditor::EditorApp::saveLayout()
{
	throw NotImplementedException();
}

const GLOWE::Map<GLOWE::String, GlowEditor::Module*>& GlowEditor::EditorApp::getModules()
{
	if (editorApp)
	{
		return editorApp->modules;
	}

	throw Exception("EditorApp::editorApp not initialized.");
}

GlowEditor::EditorApp& GlowEditor::EditorApp::instance()
{
	return *editorApp;
}

FPSCounterGUIUpdater::FPSCounterGUIUpdater(GLOWE::GameObject* const newParent)
	: ScriptBehaviour(newParent), fps(0.0f), fpsText(nullptr)
{
	GLOWE::getInstance<GLOWE::FPSCounterUtility>().setSamplingTime(GLOWE::Time::fromSeconds(0.2f));
}

void FPSCounterGUIUpdater::update()
{
	if (fpsText)
	{
		const float currentFPS = GLOWE::getInstance<GLOWE::FPSCounterUtility>().getCurrentFPS();
		if (currentFPS != fps)
		{
			fps = currentFPS;
			GLOWE::GUIElement::GuiMutexLock guard(GLOWE::GUIElement::guiMutex);
			fpsText->setText("FPS: " + GLOWE::toString(currentFPS));
		}
	}
}

GLOWE::String GlowEditor::openFileDialog(const String& windowTitle, const String& directory, const Vector<Pair<String, String>>& filter, const String& defaultFileExt)
{
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	String result;
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
	if (SUCCEEDED(hr))
	{
		CComPtr<IFileOpenDialog> fileDialog;

		hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL, IID_IFileOpenDialog, reinterpret_cast<void**>(&fileDialog));
		if (SUCCEEDED(hr))
		{
			if (!directory.isEmpty())
			{
				CComPtr<IShellItem> defaultFolder;
				hr = SHCreateItemFromParsingName(directory.toWide().c_str(), NULL, IID_IShellItem, reinterpret_cast<void**>(&defaultFolder));
				if (SUCCEEDED(hr))
				{
					fileDialog->SetFolder(defaultFolder);
				}
			}

			if (!filter.empty())
			{
				FILEOPENDIALOGOPTIONS opts;
				hr = fileDialog->GetOptions(&opts);
				if (SUCCEEDED(hr))
				{
					fileDialog->SetOptions(opts | FOS_STRICTFILETYPES);
				}

				Vector<COMDLG_FILTERSPEC> filters;
				Vector<Pair<WideString, WideString>> filterWstrs;
				filterWstrs.reserve(filter.size());
				filters.reserve(filter.size());
				for (const auto& x : filter)
				{
					filterWstrs.emplace_back(x.first.toWide(), x.second.toWide());
					filters.emplace_back(COMDLG_FILTERSPEC{ filterWstrs.back().first.c_str(), filterWstrs.back().second.c_str() });
				}

				fileDialog->SetFileTypes(filters.size(), filters.data());
			}

			if (!defaultFileExt.isEmpty())
			{
				fileDialog->SetDefaultExtension(defaultFileExt.toWide().c_str());
			}

			if (!windowTitle.isEmpty())
			{
				fileDialog->SetTitle(windowTitle.toWide().c_str());
			}

			hr = fileDialog->Show(NULL);
			if (SUCCEEDED(hr))
			{
				CComPtr<IShellItem> shellItem;
				hr = fileDialog->GetResult(&shellItem);
				if (SUCCEEDED(hr))
				{
					PWSTR filePath;
					hr = shellItem->GetDisplayName(SIGDN_FILESYSPATH, &filePath);
					if (SUCCEEDED(hr))
					{
						result = filePath;
					}
				}
			}
		}

		CoUninitialize();
	}

	return result; // TODO: Maybe some errors/exceptions?
#endif
}

GLOWE::String GlowEditor::saveFileDialog(const String& windowTitle, const String& directory, const Vector<Pair<String, String>>& filter, const String& defaultFileName)
{
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	String result;
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
	if (SUCCEEDED(hr))
	{
		CComPtr<IFileSaveDialog> fileDialog;

		hr = CoCreateInstance(CLSID_FileSaveDialog, NULL, CLSCTX_ALL, IID_IFileSaveDialog, reinterpret_cast<void**>(&fileDialog));
		if (SUCCEEDED(hr))
		{
			if (!directory.isEmpty())
			{
				CComPtr<IShellItem> defaultFolder;
				hr = SHCreateItemFromParsingName(directory.toWide().c_str(), NULL, IID_IShellItem, reinterpret_cast<void**>(&defaultFolder));
				if (SUCCEEDED(hr))
				{
					fileDialog->SetFolder(defaultFolder);
				}
			}

			if (!filter.empty())
			{
				FILEOPENDIALOGOPTIONS opts;
				hr = fileDialog->GetOptions(&opts);
				if (SUCCEEDED(hr))
				{
					fileDialog->SetOptions(opts | FOS_STRICTFILETYPES);
				}

				Vector<COMDLG_FILTERSPEC> filters;
				Vector<Pair<WideString, WideString>> filterWstrs;
				filterWstrs.reserve(filter.size());
				filters.reserve(filter.size());
				for (const auto& x : filter)
				{
					filterWstrs.emplace_back(x.first.toWide(), x.second.toWide());
					filters.emplace_back(COMDLG_FILTERSPEC{ filterWstrs.back().first.c_str(), filterWstrs.back().second.c_str() });
				}

				fileDialog->SetFileTypes(filters.size(), filters.data());
			}

			if (!defaultFileName.isEmpty())
			{
				fileDialog->SetFileName(defaultFileName.toWide().c_str());
			}

			if (!windowTitle.isEmpty())
			{
				fileDialog->SetTitle(windowTitle.toWide().c_str());
			}

			hr = fileDialog->Show(NULL);
			if (SUCCEEDED(hr))
			{
				CComPtr<IShellItem> shellItem;
				hr = fileDialog->GetResult(&shellItem);
				if (SUCCEEDED(hr))
				{
					PWSTR filePath;
					hr = shellItem->GetDisplayName(SIGDN_FILESYSPATH, &filePath);
					if (SUCCEEDED(hr))
					{
						result = filePath;
					}
				}
			}
		}

		CoUninitialize();
	}

	return result; // TODO: Maybe some errors/exceptions?
#endif
}

GLOWE::String GlowEditor::openDirectoryDialog(const String& windowTitle, const String& directory)
{
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	String result;
	HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
	if (SUCCEEDED(hr))
	{
		CComPtr<IFileOpenDialog> fileDialog;

		hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_ALL, IID_IFileOpenDialog, reinterpret_cast<void**>(&fileDialog));
		if (SUCCEEDED(hr))
		{
			FILEOPENDIALOGOPTIONS opts;
			hr = fileDialog->GetOptions(&opts);
			if (SUCCEEDED(hr))
			{
				fileDialog->SetOptions(opts | FOS_PICKFOLDERS);
			}

			if (!directory.isEmpty())
			{
				CComPtr<IShellItem> defaultFolder;
				hr = SHCreateItemFromParsingName(directory.toWide().c_str(), NULL, IID_IShellItem, reinterpret_cast<void**>(&defaultFolder));
				if (SUCCEEDED(hr))
				{
					fileDialog->SetFolder(defaultFolder);
				}
			}

			if (!windowTitle.isEmpty())
			{
				fileDialog->SetTitle(windowTitle.toWide().c_str());
			}

			hr = fileDialog->Show(NULL);
			if (SUCCEEDED(hr))
			{
				CComPtr<IShellItem> shellItem;
				hr = fileDialog->GetResult(&shellItem);
				if (SUCCEEDED(hr))
				{
					PWSTR filePath;
					hr = shellItem->GetDisplayName(SIGDN_FILESYSPATH, &filePath);
					if (SUCCEEDED(hr))
					{
						result = filePath;
					}
				}
			}
		}

		CoUninitialize();
	}

	return result; // TODO: Maybe some errors/exceptions?
#endif
}
