#pragma once
#ifndef GLOWEDITOR_EDITORMODULE_INCLUDED
#define GLOWEDITOR_EDITORMODULE_INCLUDED

#include <GlowScripting/CppScriptFoundations.h>

#define GlowEditorModuleExport(name) \
CppScriptExportExtends(name, ::GlowEditor::Module) \
extern "C" inline CPPSCRIPT_DLLEXPORT const char* __stdcall getModuleClassName() { return #name; }
#define GlowEditorDefModule() CppScriptDll()

namespace GlowEditor
{
	class EditorWindow;

	class Module
	{
	public:
		struct WindowDesc
		{
			bool needsWindow;
			const char* title;
		};
	public:
		virtual ~Module() = default;

		// Get initialization order.
		virtual int getOrder() const = 0;
		// Get module description (human readable).
		virtual const char* getDescription() const = 0;
		// Get module version (currently unused).
		virtual GLOWE::UInt64 getVersion() const = 0;
		// Get window info.
		virtual WindowDesc getWindowDesc() const { return WindowDesc { true, nullptr }; };
		
		/// @brief Initialize non-GUI data, components and scripted subsystems. May be called more than once (once for every instance).
		/// @param loadedModules Loaded engine modules
		virtual void initialize(const GLOWE::Map<GLOWE::String, Module*>& loadedModules) = 0;
		/// @brief Initialize components and GUI for the provided container (GUI window). May be called multiple times to initialize all of the windows.
		/// @param window Instance's window
		virtual void initializeGUI(EditorWindow& window) {};
		/// @brief Clean up components. Must clean up everything in case of module reinitialization.
		virtual void cleanUp() = 0;
	};
}

#endif
