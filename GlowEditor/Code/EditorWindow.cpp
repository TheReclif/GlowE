#include "EditorWindow.h"
#include "Editor.h"

#include <GlowEngine/GameObject.h>

using namespace GLOWE;

namespace GlowEditor
{
	class EditorGUITop
		: public Components::GUITop
	{
	private:
		Window* window;
	private:
		virtual void populateRenderCallbacks(RenderingEngine::RenderPass::Callbacks& callbacks) override final
		{
			GUITop::populateRenderCallbacks(callbacks);
			callbacks.shouldRender = [this]() -> bool
			{
				return checkIsAnythingToRedraw();
			};
		}
	public:
		EditorGUITop(GameObject* const parent)
			: GUITop(parent), window()
		{}

		virtual void processEvent(const GameEvent& event) override
		{
			if (window != nullptr && event == GameEvent::Type::System && event.info.systemEvent.eventWindow != window->getWindowHandle())
			{
				return;
			}

			GUITop::processEvent(event);
		}

		void setAssociatedWindow(Window* const wnd)
		{
			window = wnd;
		}

		GlowSerializeComponent(EditorGUITop, GlowBases(GUITop));
	};
}

using GlowEditor::EditorGUITop;

GlowInitCFROutsideNamespace(EditorGUITop);

namespace GlowEditor
{
	class EditorWindowRenderer
		: public ScriptableSubsystem
	{
	private:
		EditorWindowMgr& windowMgr;
	public:
		EditorWindowRenderer(EditorWindowMgr& arg)
			: windowMgr(arg)
		{
		}

		virtual void update() override
		{
			for (const auto& window : windowMgr.windowsToRedraw)
			{
				window->trueWindow.display();
			}
			windowMgr.windowsToRedraw.clear();

			if (!windowMgr.closedWindows.empty())
			{
				for (const auto& name : windowMgr.closedWindows)
				{
					windowMgr.registeredWindows.erase(name);
				}
				windowMgr.closedWindows.clear();
			}
		}
	};
}

GlowEditor::EditorWindow::EditorWindow(const String& windowTitle, ConstructTag&&)
	: clientArea(), title(windowTitle), contentsObject(nullptr)
{
	auto& obj = GameObject::createGameObject(windowTitle);
	wholeWindowArea = &obj.addComponent<EditorGUITop>();
	wholeWindowArea->setEventProcessing(true);
	setBackgroundColor(EditorApp::backgroundColor);
	makeFloating();
}

GlowEditor::EditorWindow::EditorWindow(EditorWindow&& arg) noexcept
	: trueWindow(std::move(arg.trueWindow)), clientArea(exchange(arg.clientArea, nullptr)), wholeWindowArea(exchange(arg.wholeWindowArea, nullptr)), title(std::move(arg.title)), contentsObject(exchange(arg.contentsObject, nullptr))
{
}

GlowEditor::EditorWindow::~EditorWindow()
{
	GameObject::destroyImmediate(wholeWindowArea->getGameObject());
}

GameObject* GlowEditor::EditorWindow::makeFloating()
{
	auto& engine = getInstance<Engine>();
	SwapChainDesc desc;
	desc.bufferCount = 1;
	desc.displayFormat = Format(Format::Type::UnsignedByte, 4, true, true);
	desc.initialPresentInterval = 1;
	desc.size = VideoMode(400, 400, 32);

	trueWindow.create(engine.getGraphicsContext(), desc.size, title, desc);
	wholeWindowArea->setNotOwnedRenderTarget(&trueWindow.getSwapChain().getRenderTarget());
	wholeWindowArea->setShouldClearRT(true);
	wholeWindowArea->setAssociatedWindow(&trueWindow);

	if (contentsObject != nullptr)
	{
		GameObject::destroy(contentsObject);
	}
	contentsObject = &GameObject::createGameObject("Window Client GUI", wholeWindowArea->getGameObject());
	clientArea = &contentsObject->addComponent<Components::GUIContainer>();
	clientArea->stretchToParent();

	return contentsObject;
}

void GlowEditor::EditorWindow::setTitle(const String& newTitle)
{
	title = newTitle;
	if (trueWindow.checkIsCreated())
	{
		trueWindow.setTitle(title);
	}
}

void GlowEditor::EditorWindow::setBackgroundColor(const Color& color)
{
	wholeWindowArea->setClearColor(color);
}

GUIContainer* GlowEditor::EditorWindow::getClientArea() const
{
	return clientArea;
}

GUIContainer* GlowEditor::EditorWindow::getWholeWindow() const
{
	return wholeWindowArea;
}

GameObject* GlowEditor::EditorWindow::getClientContentObject() const
{
	return contentsObject;
}

void GlowEditor::EditorWindowMgr::processWindowEvent(const ConstIt window, const Event& event)
{
	if (event == Event::Type::Closed)
	{
		closedWindows.emplace_back(window);
	}
	window->second.wholeWindowArea->getGameObject()->sendEventToChildrenRecursive(event, nullptr);
	//getInstance<ScriptMgr>().invokeAsyncEventProcessing(getInstance<Engine>())
}

void GlowEditor::EditorWindowMgr::cleanUp()
{
	registeredWindows.clear();
	closedWindows.clear();
	windowsToRedraw.clear();
	isDragged = false;
}

GlowEditor::EditorWindowMgr::EditorWindowMgr()
{
	auto& engine = getInstance<Engine>();
	engine.addSubsystem(this, Engine::SubsystemUpdateRoutine::AfterScriptUpdate);
	engine.addSubsystem(makeUniquePtr<EditorWindowRenderer>(*this), Engine::SubsystemUpdateRoutine::AfterRender);
	engine.addCleanUpCallback([this]
	{
		cleanUp();
	});
}

GlowEditor::EditorWindow* GlowEditor::EditorWindowMgr::getWindow(const String& name)
{
	auto it = registeredWindows.find(name);
	if (it == registeredWindows.end())
	{
		it = registeredWindows.emplace(std::piecewise_construct, std::forward_as_tuple(name), std::forward_as_tuple(name, EditorWindow::ConstructTag())).first;
	}
	return &it->second;
}

void GlowEditor::EditorWindowMgr::update()
{
	auto& eventMgr = getInstance<EventMgr>();
	const auto end = registeredWindows.end();
	for (auto it = registeredWindows.begin(); it != end; ++it)
	{
		auto& window = *it;
		auto& windowRef = window.second.trueWindow;
		if (windowRef.peekEvents())
		{
			auto& queue = eventMgr.getEventsQueue(windowRef);
			for (const auto& event : queue)
			{
				processWindowEvent(it, event);
			}
			queue.clear();
		}

		if (window.second.wholeWindowArea->checkIsAnythingToRedraw())
		{
			windowsToRedraw.emplace_back(&window.second);
			EditorApp::instance().markMainWindowDirty();
		}
	}
}
