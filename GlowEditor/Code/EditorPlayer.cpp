#include "Editor.h"

#include <GlowAppFramework/AppFramework.h>

GlowWindowApplication()
{
	using namespace GlowEditor;
	using namespace GLOWE;
	EditorApp app;
	app.initialize();
	getInstance<Engine>().startLoop();
}
