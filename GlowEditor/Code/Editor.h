#pragma once
#ifndef GLOWEDITOR_EDITOR_INCLUDED
#define GLOWEDITOR_EDITOR_INCLUDED

#include "Utility.h"
#include "EditorModule.h"
#include "EditorWindow.h"
#include "gloweditorlib_export.h"

namespace GlowEditor
{
	class GLOWEDITORLIB_EXPORT EditorException
		: public GLOWE::StringException
	{
	public:
		using GLOWE::StringException::StringException;
	};

	class GLOWEDITORLIB_EXPORT EditorApp
		: private GLOWE::NoCopyNoMove
	{
	private:
		static constexpr const char* defaultWindowLayoutFile = "WndLayout.layout";
	private:
		static EditorApp* editorApp;

		GLOWE::Multimap<int, Module*> modulesOrder;
		GLOWE::List<CppScript::DynamicLibrary> libs;
		GLOWE::List<CppScript::ScriptContext> contexts;
		GLOWE::Map<GLOWE::String, Module*> modules;
		GLOWE::Components::GUITop* windowSystemTop{};
		GLOWE::Components::GUITextField* debugTextField{};
		GLOWE::List<GLOWE::GUIWindowGuide> ownedGuides;
		GLOWE::Multimap<GLOWE::String, GLOWE::String> moduleToWindowMappings;
		GLOWE::Queue<GLOWE::String> availableWindows;

		bool isMainWindowDirty = true;
	private:
		void loadModules();
		void createDefaultLayout();
		bool loadLayout(const bool retry = true);
	public:
		static constexpr GLOWE::Color backgroundColor = GLOWE::Color::from255RGBA(43, 70, 85);
	public:
		EditorApp() = default;
		~EditorApp();

		void initialize(); // Call from engine startup callback.
		void cleanUp();
		void markMainWindowDirty();

		static const GLOWE::Map<GLOWE::String, Module*>& getModules();
		static EditorApp& instance();

		void saveLayout();
	};

	GLOWEDITORLIB_EXPORT GLOWE::String openFileDialog(const GLOWE::String& windowTitle, const GLOWE::String& directory, const GLOWE::Vector<GLOWE::Pair<GLOWE::String, GLOWE::String>>& filter, const GLOWE::String& defaultFileExt);
	GLOWEDITORLIB_EXPORT GLOWE::String saveFileDialog(const GLOWE::String& windowTitle, const GLOWE::String& directory, const GLOWE::Vector<GLOWE::Pair<GLOWE::String, GLOWE::String>>& filter, const GLOWE::String& defaultFileName);
	GLOWEDITORLIB_EXPORT GLOWE::String openDirectoryDialog(const GLOWE::String& windowTitle, const GLOWE::String& directory);
}

#endif
