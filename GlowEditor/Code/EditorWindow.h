#pragma once
#ifndef GLOWEDITOR_WINDOW_INCLUDED
#define GLOWEDITOR_WINDOW_INCLUDED

#include "gloweditorlib_export.h"

#include "../../GlowGraphics/Uniform/GraphicsWindow.h"

#include "../../GlowComponents/GUIContainerComponent.h"

namespace GLOWE
{
	namespace Components
	{
		class GUITop;
	}
}

namespace GlowEditor
{
	class EditorGUITop;

	class GLOWEDITORLIB_EXPORT EditorWindow
		: public GLOWE::NoCopy
	{
	private:
		struct ConstructTag {};
	public:
		enum class Side
		{
			Whole,
			Left,
			Right,
			Top,
			Bottom
		};
	private:
		GLOWE::GraphicsWindow trueWindow;
		GLOWE::GUIContainer* clientArea;
		EditorGUITop* wholeWindowArea;
		GLOWE::String title;
		GLOWE::GameObject* contentsObject;
	public:
		explicit EditorWindow(const GLOWE::String& windowTitle, ConstructTag&&);
		EditorWindow(EditorWindow&& arg) noexcept;
		EditorWindow() = delete;
		~EditorWindow();

		// @brief Makes the window a floating one.
		/// @return GameObject where new GUI must be instantiated.
		GLOWE::GameObject* makeFloating();

		void setTitle(const GLOWE::String& newTitle);
		void setBackgroundColor(const GLOWE::Color& color);
		
		GLOWE::GUIContainer* getClientArea() const;
		GLOWE::GUIContainer* getWholeWindow() const;
		GLOWE::GameObject* getClientContentObject() const;
		
		friend class EditorWindowMgr;
		friend class EditorWindowRenderer;
	};

	class GLOWEDITORLIB_EXPORT EditorWindowMgr
		: public GLOWE::Subsystem, public GLOWE::NoCopyNoMove, public GLOWE::ScriptableSubsystem
	{
	private:
		GLOWE::UnorderedMap<GLOWE::String, EditorWindow> registeredWindows;

		using ConstIt = decltype(registeredWindows)::const_iterator;

		bool isDragged = false;
		GLOWE::Vector<ConstIt> closedWindows;
		GLOWE::Vector<EditorWindow*> windowsToRedraw;
	private:
		void processWindowEvent(const ConstIt window, const GLOWE::Event& event);
		void cleanUp();
	public:
		EditorWindowMgr();

		EditorWindow* getWindow(const GLOWE::String& name);

		void update() override;

		friend class EditorWindowRenderer;
	};
}

#endif
