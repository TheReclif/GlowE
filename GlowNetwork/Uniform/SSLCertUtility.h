#pragma once
#ifndef GLOWE_NETWORK_UNIFORM_SSLCERTUTIL_INCLUDED
#define GLOWE_NETWORK_UNIFORM_SSLCERTUTIL_INCLUDED

#include "../../GlowSystem/Utility.h"

#ifdef GLOWE_COMPILE_FOR_WINDOWS
#include "../Windows/WindowsSSLCertUtility.h"
#else
#include "../Linux/LinuxSSLCertUtility.h"
#endif

namespace GLOWE
{
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	using SSLCertUtil = GLOWE::Windows::SSLCertUtil;
#else
	using SSLCertUtil = GLOWE::Linux::SSLCertUtil;
#endif
}

#endif
