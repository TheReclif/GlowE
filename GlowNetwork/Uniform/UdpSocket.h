#pragma once
#ifndef GLOWE_UNIFORM_NETWORK_UDPSOCKET_INCLUDED
#define GLOWE_UNIFORM_NETWORK_UDPSOCKET_INCLUDED

#include "../../GlowSystem/Utility.h"

#include "BaseSocket.h"
#include "Packet.h"

namespace GLOWE
{
	class GLOWNETWORK_EXPORT UdpSocket
		: public BaseSocket
	{
	private:
		Vector<char> packetBuffer;
	public:
		UdpSocket();
		UdpSocket(UdpSocket&& arg) noexcept;
		UdpSocket(const bool block, const bool useIPv6 = false);
		virtual ~UdpSocket() = default;

		virtual Socket::Status bind(const unsigned short port, const Ip& ip = Ip::any);
		virtual void unbind();

		virtual Socket::Status send(const void* data, const std::size_t dataSize, const Ip& ip, const unsigned short port);
		virtual Socket::Status send(Packet& packet, const Ip& ip, const unsigned short port);

		virtual Socket::Status receive(void* buffer, const std::size_t bufferSize, std::size_t& bytesReceived, Ip& senderIp, unsigned short& senderPort);
		virtual Socket::Status receive(Packet& out, Ip& ip, unsigned short& port);

		virtual unsigned short getLocalPort() const;

		static constexpr unsigned int maxDatagramSize = 65507;
	};
}

#endif
