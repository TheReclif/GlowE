#pragma once
#ifndef GLOWE_NETWORK_HTTP_INCLUDED
#define GLOWE_NETWORK_HTTP_INCLUDED

#include "TcpSocket.h"
#include "SecureSocket.h"

namespace GLOWE
{
	class GLOWNETWORK_EXPORT HTTPRequest
	{
	public:
		using Field = Multimap<String, String>::iterator;
	public:
		enum class Type
		{
			Invalid,
			Get,
			Head,
			Post,
			Put,
			Delete,
			Connect,
			Options,
			Trace
		};
	private:
		Type requestType;
		String requestedResource;
		Multimap<String, String> headerValues;
	public:
		HTTPRequest();
		HTTPRequest(const Type reqType, const Url& requestedRes); // "Host" value will be initialized with Url.getHTTPHost()
		HTTPRequest(const Type reqType, const String& requestedRes);

		void setType(const Type arg);
		void setRequestedResource(const String& arg);
		void setRequestedResource(const Url& arg); // "Host" field will also be modified.

		Field addField(const String& key, const String& value = "");
		void removeField(const Field& field);

		void addPacketInfo(const Packet& packet); // Sets Content-Length.

		String createRequest() const;

		static String getDefaultUserAgent();
	};

	class GLOWNETWORK_EXPORT HTTPResponse
	{
	private:
		UInt code;
		String codeDesc, httpVersion;
		Multimap<String, String> headerValues;
	public:
		bool checkIsSuccess() const;
		bool checkIsValid() const; // Checks if the containing data is valid, that is it has been received successfully. Call after every HTTPBase.receiveResponse.

		UInt getCode() const; // Get HTTP response code. Best one - 200 (OK).

		String getCodeDesc() const; // Get HTTP response code desc, eg. OK for HTTP 200.
		String getHTTPVersion() const; // Almost always HTTP/1.1

		const Multimap<String, String>& getFields() const;

		template<class SocketType>
		friend class HTTPBase;
	};

	// Valid types: TcpSocket and SecureSocket.
	template<class SocketType>
	class HTTPBase
		: private NoCopy
	{
	private:
		static const unsigned short portToUse;

		bool isConnected;

		Url connectedSite;

		SocketType sock; // socket is a name of a function, so we have to use shorter name - sock.

		static_assert(std::is_base_of<TcpSocket, SocketType>::value, "Unsupported SocketType (use TcpSocket or SecureSocket).");
	public:
		HTTPBase();
		HTTPBase(const Url& url, const Time& timeout = Time::fromSeconds(2.0f), const bool useIPv6 = false);
		~HTTPBase() = default;

		// Returns true if it's already connected.
		bool connect(const Url& url, const Time& timeout = Time::fromSeconds(2.0f), const bool useIPv6 = false);

		void disconnect();

		Socket::Status sendRequest(const HTTPRequest& req, const Packet& packet = Packet()) const;

		// May change the socket's internal state (is blocking).
		HTTPResponse receiveResponse();
		HTTPResponse receiveResponse(Packet& packet);

		bool checkIsConnected() const; // It does not check the current state of the socket.

		void setBlocking(const bool arg);

		SocketType& getSocket();
	};

	using HTTP = HTTPBase<TcpSocket>;
	using HTTPS = HTTPBase<SecureSocket>;

#include "HTTP.inl"
}

#endif
