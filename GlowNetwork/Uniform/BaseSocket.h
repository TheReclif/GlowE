#pragma once
#ifndef GLOWE_NETWORK_UNIFORM_BASESOCKET_INCLUDED
#define GLOWE_NETWORK_UNIFORM_BASESOCKET_INCLUDED

#include "../../GlowSystem/Utility.h"

#include "SocketHandle.h"
#include "SocketUtility.h"
#include "SocketImpl.h"

namespace GLOWE
{
	class GLOWNETWORK_EXPORT BaseSocket
		: private NoCopy
	{
	public:
		enum class SocketOptionLevel
		{
			Socket,
			TCP,
			UDP,
			IP,
			IPv6
		};

		enum class SocketOption
		{
			/// @brief Enables creating other sockets using the same address and port. Can be used to implement TCP hole punching. Use with the Socket level.
			ReuseAddress,
			/// @brief Opposite of ReuseAddress. Restricts from creating other sockets using the same address and port. Use with the Socket level.
			ExclusiveAddress,
			/// @brief Disables the delay on connection-oriented protocols' (TCP) sockets while closing. Any data remaining the send buffer may be lost. Use with the Socket level.
			DontLinger,
			/// @brief Enables keep-alive for a socket connection. Valid only for connection-oriented protocols (TCP). Use with the Socket level.
			KeepAlive,
			/// @brief Enables the socket to broadcast data. Valid only for protocols that support broadcasting (UDP). Enabled by default. Use with the Socket level.
			Broadcast,
			/// @brief Indicates that out-of-bound data should be returned in-line with regular data. This option is only valid for connection-oriented protocols that support out-of-band data. Use with the Socket level.
			OutOfBandInline,
			/// @brief Sets the receive timeout, in milliseconds, for blocking receive calls. It's advised NOT TO USE this option as the socket may be in an indeterminate state. Zero (indefinite blocking) by default. Use with the Socket level.
			ReceiveTimeOut,
			/// @brief Sets the send timeout, in milliseconds, for blocking send calls. It's advised NOT TO USE this option as the socket may be in an indeterminate state. Zero (indefinite blocking) by default. Use with the Socket level.
			SendTimeOut,

			/// @brief The service provider implements the expedited data as specified in RFC-1222. Otherwise, the Berkeley Software Distribution (BSD) style (default) is used. This option can be set on the connection only once. Once this option is set on, this option cannot be turned off. This option is not required to be implemented by service providers. Use with the TCP level.
			Expedited,
			/// @brief Enables or disables the Nagle algorithm for TCP sockets. Disabled by default (on Windows). Use with the TCP level.
			NoDelay,

			/// @brief When enabled, UDP datagrams are sent with the checksum of zero. Required for service providers. If a service provider does not have a mechanism to disable UDP checksum calculation, it may simply store this option without taking any action. This option is not supported for IPv6. Use with the UDP level.
			NoChecksum,
			/// @brief When true, UDP datagrams are sent with a checksum. Use with the UDP level.
			ChecksumCoverage,

			/// @brief Allows or blocks broadcast reception. Use with the IP level.
			ReceiveBroadcast,
			/// @brief Controls whether data sent by an application on the local computer (not necessarily by the same socket) in a multicast session will be received by a socket joined to the multicast destination group on the loopback interface. A value of true causes multicast data sent by an application on the local computer to be delivered to a listening socket on the loopback interface. A value of false prevents multicast data sent by an application on the local computer from being delivered to a listening socket on the loopback interface. Enabled by default (on Windows). Use with the IP level.
			MulticastLoop,

			/// @brief Indicates if a socket created for the AF_INET6 address family is restricted to IPv6 communications only. Sockets created for the AF_INET6 address family may be used for both IPv6 and IPv4 communications. Use with the IPV6 level.
			V6Only
		};
	protected:
		const Socket::Type type;
		SocketHandle socketHandle;
		bool isBlocking, usesIPv6;
		UnorderedSet<SocketOption> errorsDuringOptionSetting;
		Map<SocketOption, String> options;
	protected:
		BaseSocket(const Socket::Type& arg);
		BaseSocket(const Socket::Type& argType, const bool block, const bool useIPv6);

		virtual void open(SocketHandle arg);
		virtual void close();
	public:
		BaseSocket() = delete;
		BaseSocket(BaseSocket&& arg) noexcept;
		virtual ~BaseSocket();

		BaseSocket& operator=(BaseSocket&& arg);

		bool checkIsBlocking() const;
		bool checkIfUsesIPv6() const;
		bool checkIfAnyOptionFailed() const;

		/// @brief Manually opens the socket. Avoid using this method unless you set the socket's options and want to check if any of them failed.
		virtual void open();

		const UnorderedSet<SocketOption>& getFailedOptions() const;

		void setBlocking(const bool arg);
		/// @brief Sets whether the socket should use IPv4 or IPv6. Works only before opening the socket, does nothing afterwards.
		/// @param arg true for IPv6, false for IPv4.
		void setIPv6Use(const bool arg);

		/// @brief Manually sets the socket's option. Will fail if the socket is not opened.
		/// @param level Option's level. One can use socketOptionToLevelInt to convert SocketOption to appropriate integer value.
		/// @param option Socket's option. One can use socketOptionToInt to convert SocketOption to its corresponding integer value.
		/// @param arg Pointer to the argument's bytes.
		/// @param dataSize Argument's size in bytes.
		/// @return true if the option was set successfully, false otherwise.
		bool setOption(const int level, const int option, const void* const arg, const unsigned int dataSize);

		void setOption(const SocketOption option, const int arg);
		void setOption(const SocketOption option, const bool arg);

		const SocketHandle& getUnderlayingHandle() const;

		static int socketOptionToInt(const SocketOption option);
		static int socketLevelToInt(const SocketOptionLevel level);
		static int socketOptionToLevelInt(const SocketOption option);

		static constexpr unsigned short anyPort = 0;
	};
}

#endif