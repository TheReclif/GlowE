#pragma once
#ifndef GLOWE_UNIFORM_NETWORK_BASETCPLISTENER_INCLUDED
#define GLOWE_UNIFORM_NETWORK_BASETCPLISTENER_INCLUDED

#include "../../GlowSystem/Utility.h"

#include "BaseSocket.h"
#include "TcpSocket.h"
#include "SocketUtility.h"

namespace GLOWE
{
	class GLOWNETWORK_EXPORT TcpListener
		: public BaseSocket
	{
	public:
		TcpListener();
		TcpListener(TcpListener&& arg) noexcept;
		TcpListener(const bool block, const bool useIPv6 = false);
		virtual ~TcpListener() = default;

		virtual Socket::Status listen(const unsigned short port, const Ip& ip = Ip::any);
		virtual Socket::Status accept(TcpSocket& acceptedSocket);

		virtual void close() override;

		virtual unsigned short getLocalPort() const;
	};
}

#endif
