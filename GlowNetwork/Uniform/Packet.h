#pragma once
#ifndef GLOWE_NETWORK_UNIFORM_PACKET_INCLUDED
#define GLOWE_NETWORK_UNIFORM_PACKET_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/String.h"

#include "SocketUtility.h"

namespace GLOWE
{
	class UdpSocket;
	class TcpSocket;

	class GLOWNETWORK_EXPORT Packet
	{
	private:
		Vector<char> buffer;
		unsigned long long readPos, writePos, sendPos;
	protected:
		virtual void onReceive(const void* data, const std::size_t& dataSize);
		virtual const void* onSend(std::size_t& bytesToSend) const;

		const void* getDataPtr() const;
	public:
		Packet();
		~Packet() = default;

		void reset();
		void resetReadPos();
		void resetWritePos();

		void append(const void* ptr, const std::size_t& bytes);
		const void* read(const std::size_t& howMany);

		bool isValid() const;
		bool eof() const;
		operator bool() const;

		std::size_t getDataSize() const;

		Packet& operator << (const String& in);
		Packet& operator << (const char* in);
		Packet& operator << (const Int8 in);
		Packet& operator << (const UInt8 in);
		Packet& operator << (const Int16 in);
		Packet& operator << (const UInt16 in);
		Packet& operator << (const Int32 in);
		Packet& operator << (const UInt32 in);
		Packet& operator << (const Int64 in);
		Packet& operator << (const UInt64 in);
		Packet& operator << (const float in);
		Packet& operator << (const double in);
		Packet& operator << (const bool in);

		Packet& operator >> (String& out);
		Packet& operator >> (Int8& out);
		Packet& operator >> (UInt8& out);
		Packet& operator >> (Int16& out);
		Packet& operator >> (UInt16& out);
		Packet& operator >> (Int32& out);
		Packet& operator >> (UInt32& out);
		Packet& operator >> (Int64& out);
		Packet& operator >> (UInt64& out);
		Packet& operator >> (float& out);
		Packet& operator >> (double& out);
		Packet& operator >> (bool& out);

		friend class UdpSocket;
		friend class TcpSocket;

		template<class SocketType>
		friend class HTTPBase;

		friend class SecureSocket;
	};
}

#endif