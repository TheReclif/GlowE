#include "UdpSocket.h"

GLOWE::UdpSocket::UdpSocket()
	: BaseSocket(Socket::Type::UDP), packetBuffer(maxDatagramSize)
{
}

GLOWE::UdpSocket::UdpSocket(const bool block, const bool useIPv6)
	: BaseSocket(Socket::Type::UDP, block, useIPv6), packetBuffer(maxDatagramSize)
{
}

GLOWE::UdpSocket::UdpSocket(UdpSocket&& arg) noexcept
	: BaseSocket(std::move(arg)), packetBuffer(std::move(arg.packetBuffer))
{
}

GLOWE::Socket::Status GLOWE::UdpSocket::bind(const unsigned short port, const Ip& ip)
{
	open();

	if (ip == Ip::none || ip == Ip::broadcast)
	{
		return Socket::Status::Error;
	}

	sockaddr_in6 addr;
	SocketImpl::createAddress(ip, port, &addr);
	if (::bind(socketHandle, (sockaddr*)&addr, usesIPv6 ? sizeof(sockaddr_in6) : sizeof(sockaddr_in)) == -1)
	{
		WarningThrow(false, String(u8"Unable to bind UDP socket."));
		return Socket::Status::Error;
	}

	return Socket::Status::Done;
}

void GLOWE::UdpSocket::unbind()
{
	close();
}

GLOWE::Socket::Status GLOWE::UdpSocket::send(const void * data, const std::size_t dataSize, const Ip & ip, const unsigned short port)
{
	open();

	if (dataSize > maxDatagramSize)
	{
		WarningThrow(false, String(u8"Unable to send data using UDP (data size bigger than \"maxDatagramSize\")."));
		return Socket::Status::Error;
	}

	sockaddr_in6 addr;
	SocketImpl::createAddress(ip, port, &addr);

	int bytesSent = sendto(socketHandle, (const char*)data, dataSize, 0, (sockaddr*)&addr, usesIPv6 ? sizeof(sockaddr_in6) : sizeof(sockaddr_in));

	if (bytesSent < 0)
	{
		return SocketImpl::getErrorCode();
	}

	return Socket::Status::Done;
}

GLOWE::Socket::Status GLOWE::UdpSocket::send(Packet & packet, const Ip & ip, const unsigned short port)
{
	std::size_t packetSize = 0;

	return send(packet.onSend(packetSize), packetSize, ip, port);
}

GLOWE::Socket::Status GLOWE::UdpSocket::receive(void * buffer, const std::size_t bufferSize, std::size_t& bytesReceived, Ip & senderIp, unsigned short & senderPort)
{
	bytesReceived = 0;

	if (!buffer)
	{
		WarningThrow(false, String(u8"Output buffer is invalid."));
		return Socket::Status::Error;
	}

	sockaddr_in6 addr;
	SocketImpl::createAddress(usesIPv6 ? Ip::anyIPv6 : Ip::any, 0, &addr);
	socklen_t addrLen = sizeof(addr);
	bytesReceived = recvfrom(socketHandle, (char*)buffer, bufferSize, 0, (sockaddr*)&addr, &addrLen);

	if (bytesReceived < 0)
	{
		bytesReceived = 0;
		return SocketImpl::getErrorCode();
	}

	switch (addr.sin6_family)
	{
	case AF_INET6:
		senderIp = Ip(reinterpret_cast<const UInt16 (&)[8]>(addr.sin6_addr.s6_addr), addr.sin6_scope_id, false);
		senderPort = ntohs(addr.sin6_port);
		break;
	case AF_INET:
	{
		sockaddr_in* const temp = reinterpret_cast<sockaddr_in*>(&addr);
		senderIp = Ip(temp->sin_addr.s_addr, false);
		senderPort = ntohs(temp->sin_port);
	}
	break;
	}

	//senderIp = Ip(ntohl(addr.sin_addr.s_addr));
	//senderPort = addr.sin_port;

	return Socket::Status::Done;
}

GLOWE::Socket::Status GLOWE::UdpSocket::receive(Packet & out, Ip & ip, unsigned short & port)
{
	std::size_t bytesReceived = 0;
	Socket::Status status;
	if ((status = receive(packetBuffer.data(), maxDatagramSize, bytesReceived, ip, port)) != Socket::Status::Done)
	{
		return status;
	}

	out.reset();
	if (bytesReceived > 0)
	{
		out.onReceive(packetBuffer.data(), bytesReceived);
	}

	return Socket::Status::Done;
}

unsigned short GLOWE::UdpSocket::getLocalPort() const
{
	if (socketHandle != SocketImpl::invalidSocket())
	{
		sockaddr_in6 addr;
		socklen_t addrLen = sizeof(addr);
		if (getsockname(socketHandle, (sockaddr*)&addr, &addrLen) != -1)
		{
			return ntohs(addr.sin6_port);
		}
	}

	return 0;
}
