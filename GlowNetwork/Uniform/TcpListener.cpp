#include "TcpListener.h"

GLOWE::TcpListener::TcpListener()
	: BaseSocket(Socket::Type::TCP)
{
}

GLOWE::TcpListener::TcpListener(const bool block, const bool useIPv6)
	: BaseSocket(Socket::Type::TCP, block, useIPv6)
{
}

GLOWE::TcpListener::TcpListener(TcpListener&& arg) noexcept
	: BaseSocket(std::move(arg))
{
}

GLOWE::Socket::Status GLOWE::TcpListener::listen(const unsigned short port, const Ip & ip)
{
	open();

	if (ip == Ip::none || ip == Ip::broadcast)
	{
		WarningThrow(false, String(u8"Invalid IP."));
		return Socket::Status::Error;
	}

	sockaddr_in6 addr;
	SocketImpl::createAddress(ip, port, &addr);
	if (bind(socketHandle, (sockaddr*)&addr, usesIPv6 ? sizeof(sockaddr_in6) : sizeof(sockaddr_in)) == -1)
	{
		WarningThrow(false, String(u8"Unable to bind listener to port ") + toString(port));
		return Socket::Status::Error;
	}

	if (::listen(socketHandle, 0) == -1)
	{
		WarningThrow(false, String(u8"Unable to listen to port ") + toString(port));
		return Socket::Status::Error;
	}

	return Socket::Status::Done;
}

GLOWE::Socket::Status GLOWE::TcpListener::accept(TcpSocket & acceptedSocket)
{
	if (socketHandle == SocketImpl::invalidSocket())
	{
		WarningThrow(false, String(u8"Unable to accept connections - socket is not listening."));
		return Socket::Status::Error;
	}

	sockaddr_in addr;
	socklen_t sockLen = sizeof(addr);
	SocketHandle tempHandle = ::accept(socketHandle, (sockaddr*)&addr, &sockLen);

	if (tempHandle == SocketImpl::invalidSocket())
	{
		return SocketImpl::getErrorCode();
	}

	acceptedSocket.close();
	acceptedSocket.open(tempHandle);

	return Socket::Status::Done;
}

void GLOWE::TcpListener::close()
{
	BaseSocket::close();
}

unsigned short GLOWE::TcpListener::getLocalPort() const
{
	if (socketHandle != SocketImpl::invalidSocket())
	{
		sockaddr_in6 addr;
		socklen_t addrLen = sizeof(addr);
		if (getsockname(socketHandle, (sockaddr*)&addr, &addrLen) != -1)
		{
			return ntohs(addr.sin6_port);
		}
	}

	return 0;
}
