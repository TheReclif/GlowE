#pragma once
#ifndef GLOWE_NETWORK_UNIFORM_BASETCPSOCKET_INCLUDED
#define GLOWE_NETWORK_UNIFORM_BASETCPSOCKET_INCLUDED

#include "BaseSocket.h"
#include "Packet.h"

#include "../../GlowSystem/Time.h"

namespace GLOWE
{
	class TcpListener;

	class GLOWNETWORK_EXPORT TcpSocket
		: public BaseSocket
	{
	protected:
		class GLOWNETWORK_EXPORT AwaitingPacket
		{
		public:
			std::size_t bytesReceived;
			UInt32 packetSize;
			Vector<char> data;
		};

		AwaitingPacket awaitingPacket;
	public:
		TcpSocket();
		TcpSocket(TcpSocket&& arg) noexcept;
		TcpSocket(const bool block, const bool useIPv6 = false);
		virtual ~TcpSocket() = default;

		TcpSocket& operator=(TcpSocket&& arg) noexcept;

		virtual Socket::Status connect(const Ip& ip, const unsigned short port, const Time& timeout = Time::ZeroTime);
		virtual Socket::Status connect(const Url& url, const unsigned short port, const Time& timeout = Time::ZeroTime);
		virtual void disconnect();

		virtual Socket::Status send(const void* data, const std::size_t bytesToSend) const;
		virtual Socket::Status send(const void* data, const std::size_t bytesToSend, std::size_t& bytesSent) const;
		virtual Socket::Status send(Packet& packet) const;

		virtual Socket::Status receive(void* buffer, const std::size_t bufferSize, std::size_t& bytesReceived) const;
		virtual Socket::Status receive(Packet& packet);

		virtual unsigned short getLocalPort() const;
		virtual Ip getRemoteIP() const;
		virtual unsigned short getRemotePort() const;

		/// @brief Checks if the connection has been closed. Not reliable as it returns false if there's any data ramaining to receive.
		/// @return true if connection has been closed (recv <= 0), false otherwise
		virtual bool checkIfConnectionWasClosed() const;

		friend class TcpListener;
	};
}

#endif
