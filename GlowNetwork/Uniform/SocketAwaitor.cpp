#include "SocketAwaitor.h"

void GLOWE::SocketAwaitor::addSocket(const BaseSocket& sock)
{
	listenedSockets.emplace(&sock);
}

void GLOWE::SocketAwaitor::removeSocket(const BaseSocket& sock)
{
	listenedSockets.erase(&sock);
}

GLOWE::Socket::Status GLOWE::SocketAwaitor::wait(const unsigned char howToWait, const Time& timeout)
{
	if (listenedSockets.empty())
	{
		return Socket::Status::NotReady;
	}

	fd_set readSet, writeSet, exceptSet;
	FD_ZERO(&readSet);
	FD_ZERO(&writeSet);
	FD_ZERO(&exceptSet);

	timeval tempTimeout;
	tempTimeout.tv_sec = timeout.asMicroseconds() / 1000000;
	tempTimeout.tv_usec = timeout.asMicroseconds() % 1000000;

	for (const auto& x : listenedSockets)
	{
		const auto fd = x->getUnderlayingHandle();
		if (howToWait & WaitOp::Read)
		{
			FD_SET(fd, &readSet);
		}
		if (howToWait & WaitOp::Write)
		{
			FD_SET(fd, &writeSet);
		}
		if (howToWait & WaitOp::Except)
		{
			FD_SET(fd, &exceptSet);
		}
	}

	availableSockets.clear();
	const auto selectResult = select((*std::prev(listenedSockets.end()))->getUnderlayingHandle() + 1, (howToWait & WaitOp::Read) ? &readSet : nullptr, (howToWait & WaitOp::Write) ? &writeSet : nullptr, (howToWait & WaitOp::Except) ? &exceptSet : nullptr, &tempTimeout);
	if (selectResult > 0)
	{
		unsigned int counter = 0;
		for (const auto& x : listenedSockets)
		{
			const auto fd = x->getUnderlayingHandle();
			if (howToWait & WaitOp::Read)
			{
				if (FD_ISSET(fd, &readSet))
				{
					++counter;
					availableSockets[x] |= WaitOp::Read;
				}
			}
			if (howToWait & WaitOp::Write)
			{
				if (FD_ISSET(fd, &writeSet))
				{
					++counter;
					availableSockets[x] |= WaitOp::Write;
				}
			}
			if (howToWait & WaitOp::Except)
			{
				if (FD_ISSET(fd, &exceptSet))
				{
					++counter;
					availableSockets[x] |= WaitOp::Except;
				}
			}

			if (counter == selectResult)
			{
				break;
			}
		}

		return Socket::Status::Done;
	}
	else
	if (selectResult == -1)
	{
		WarningThrow(false, "select(2) failed with errno: " + toString(errno) + ".");
		return Socket::Status::Error;
	}

	return Socket::Status::NotReady;
}

GLOWE::SocketAwaitor::AvailableSocketsContainer::const_iterator GLOWE::SocketAwaitor::begin() const
{
	return availableSockets.begin();
}

GLOWE::SocketAwaitor::AvailableSocketsContainer::const_iterator GLOWE::SocketAwaitor::end() const
{
	return availableSockets.end();
}

unsigned char GLOWE::SocketAwaitor::getSocketStatus(const BaseSocket& sock) const
{
	const auto it = availableSockets.find(&sock);
	if (it != availableSockets.end())
	{
		return it->second;
	}
	return WaitOp::None;
}
