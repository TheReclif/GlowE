#pragma once
#ifndef GLOWE_NETWORK_SECURESOCKET_INCLUDED
#define GLOWE_NETWORK_SECURESOCKET_INCLUDED

#include "BaseSocket.h"
#include "SocketUtility.h"
#include "TcpSocket.h"

#include <mbedtls/net_sockets.h>
#include <mbedtls/config.h>
#include <mbedtls/debug.h>
#include <mbedtls/ssl.h>
#include <mbedtls/entropy.h>
#include <mbedtls/ctr_drbg.h>
#include <mbedtls/error.h>
#include <mbedtls/certs.h>

#ifdef _MSC_VER
// Need to link with Ws2_32.lib, Mswsock.lib, Advapi32.lib and crypt32.lib (for SSL/TLS and certificates).
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")
#pragma comment (lib, "crypt32.lib")
#endif

namespace GLOWE
{
	// Uses SSL/TLS.
	class GLOWNETWORK_EXPORT SecureSocket
		: public TcpSocket
	{
	private:
		mutable mbedtls_ssl_context ssl;

		mbedtls_entropy_context entropy;
		mbedtls_ctr_drbg_context ctrDrbg;
		mbedtls_ssl_config conf;
		mbedtls_x509_crt cacert;
		mbedtls_net_context dummyNetContext;

		bool isSSLAndTLSReady;

		String hostName;
	private:
		Socket::Status connectWithHostName();
		Socket::Status establishConnection(const Ip& ip, const unsigned short port, const Time& timeout);
		Socket::Status establishConnection(const Url& url, const unsigned short port, const Time& timeout);

		Socket::Status sendNoSSL(const void* data, const std::size_t bytesToSend, std::size_t& bytesSent) const;
		Socket::Status receiveNoSSL(void* buffer, const std::size_t bufferSize, std::size_t& bytesReceived) const;
	public:
		SecureSocket();
		SecureSocket(SecureSocket&&) = delete;
		virtual ~SecureSocket();

		void setHostName(const String& newHostName);

		// Returns NotReady when isSSLAndTLSReady is false.
		virtual Socket::Status connect(const Ip& ip, const unsigned short port, const Time& timeout = Time::ZeroTime) override;
		virtual Socket::Status connect(const Url& url, const unsigned short port, const Time& timeout = Time::ZeroTime);
		virtual void disconnect() override;

		virtual Socket::Status send(const void* data, const std::size_t bytesToSend) const override;
		virtual Socket::Status send(const void* data, const std::size_t bytesToSend, std::size_t& bytesSent) const override;
		virtual Socket::Status send(Packet& packet) const override;

		virtual Socket::Status receive(void* buffer, const std::size_t bufferSize, std::size_t& bytesReceived) const override;
		virtual Socket::Status receive(Packet& packet) override;

		bool isSecureConnectionReady() const;
	};
}

#endif
