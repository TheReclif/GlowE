#pragma once
#ifndef GLOWE_UNIFORM_NETWORK_SOCKETIMPL_INCLUDED
#define GLOWE_UNIFORM_NETWORK_SOCKETIMPL_INCLUDED

#include "../../GlowSystem/Utility.h"

#include "SocketHandle.h"

#ifdef GLOWE_COMPILE_FOR_WINDOWS
#include "../Windows/WindowsSocketImpl.h"
#else
#include "../Linux/LinuxSocketImpl.h"
#endif

namespace GLOWE
{
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	using SocketImpl = GLOWE::Windows::WindowsSocketImpl;
#else
	using SocketImpl = GLOWE::Linux::LinuxSocketImpl;
#endif
}

#endif
