#include "Packet.h"

GLOWE::Packet::Packet()
	: buffer(), readPos(0), writePos(0), sendPos(0)
{
}

void GLOWE::Packet::reset()
{
	buffer.clear();
	resetReadPos();
	resetWritePos();
	sendPos = 0;
}

void GLOWE::Packet::resetReadPos()
{
	readPos = 0;
}

void GLOWE::Packet::resetWritePos()
{
	writePos = 0;
}

void GLOWE::Packet::append(const void * ptr, const std::size_t & bytes)
{
	if (ptr)
	{
		const char* temp = reinterpret_cast<const char*>(ptr);

		buffer.insert(buffer.begin() + writePos, temp, temp + bytes);
		writePos += bytes;

		resetReadPos();
	}
}

const void * GLOWE::Packet::read(const std::size_t & howMany)
{
	if (readPos + howMany <= buffer.size())
	{
		void* temp = buffer.data() + readPos;
		readPos += howMany;
		return temp;
	}

	return nullptr;
}

bool GLOWE::Packet::isValid() const
{
	return readPos <= buffer.size();
}

bool GLOWE::Packet::eof() const
{
	return isValid();
}

GLOWE::Packet::operator bool() const
{
	return isValid();
}

std::size_t GLOWE::Packet::getDataSize() const
{
	return buffer.size();
}

GLOWE::Packet & GLOWE::Packet::operator<<(const String & in)
{
	append(in.getCharArray(), in.getSize() + 1);
	return *this;
}

GLOWE::Packet& GLOWE::Packet::operator<<(const char* in)
{
	append(in, std::strlen(in) + 1);
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator<<(const Int8 in)
{
	append(&in, sizeof(in));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator<<(const UInt8 in)
{
	append(&in, sizeof(in));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator<<(const Int16 in)
{
	append(&in, sizeof(in));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator<<(const UInt16 in)
{
	append(&in, sizeof(in));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator<<(const Int32 in)
{
	append(&in, sizeof(in));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator<<(const UInt32 in)
{
	append(&in, sizeof(in));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator<<(const Int64 in)
{
	append(&in, sizeof(in));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator<<(const UInt64 in)
{
	append(&in, sizeof(in));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator<<(const float in)
{
	append(&in, sizeof(in));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator<<(const double in)
{
	append(&in, sizeof(in));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator<<(const bool in)
{
	append(&in, sizeof(in));
	return *this;
}

void GLOWE::Packet::onReceive(const void * data, const std::size_t & dataSize)
{
	const char* temp = reinterpret_cast<const char*>(data);
	reset();
	buffer.assign(temp, temp + dataSize);
}

const void* GLOWE::Packet::onSend(std::size_t & bytesToSend) const
{
	bytesToSend = getDataSize();
	return buffer.data();
}

const void* GLOWE::Packet::getDataPtr() const
{
	return buffer.data();
}

GLOWE::Packet & GLOWE::Packet::operator >> (String & out)
{
	auto& tempStr = out.getString();
	tempStr.clear();

	const auto buffSize = buffer.size();
	while (buffer[readPos] != '\0' && readPos < buffSize)
	{
		tempStr += buffer[readPos++];
	} 

	++readPos;

	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator >> (Int8 & out)
{
	out = *((Int8*)(read(sizeof(out))));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator >> (UInt8 & out)
{
	out = *((UInt8*)(read(sizeof(out))));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator >> (Int16 & out)
{
	out = *((Int16*)(read(sizeof(out))));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator >> (UInt16 & out)
{
	out = *((UInt16*)(read(sizeof(out))));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator >> (Int32 & out)
{
	out = *((Int32*)(read(sizeof(out))));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator >> (UInt32 & out)
{
	out = *((UInt32*)(read(sizeof(out))));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator >> (Int64 & out)
{
	out = *((Int64*)(read(sizeof(out))));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator >> (UInt64 & out)
{
	out = *((UInt64*)(read(sizeof(out))));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator >> (float & out)
{
	out = *((float*)(read(sizeof(out))));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator >> (double & out)
{
	out = *((double*)(read(sizeof(out))));
	return *this;
}

GLOWE::Packet & GLOWE::Packet::operator >> (bool & out)
{
	out = *((bool*)(read(sizeof(out))));
	return *this;
}
