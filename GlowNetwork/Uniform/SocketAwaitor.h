#pragma once
#ifndef GLOWE_NETWORK_SOCKETAWAITOR_INCLUDED
#define GLOWE_NETWORK_SOCKETAWAITOR_INCLUDED

#include "TcpSocket.h"

namespace GLOWE
{
	// TODO: Port from select to poll.
	class GLOWNETWORK_EXPORT SocketAwaitor
	{
	public:
		enum WaitOp : unsigned char
		{
			None = 0,
			Read = 1,
			Write = 1 << 1,
			Except = 1 << 2
		};
	private:
		struct SocketPred
		{
			inline bool operator()(const BaseSocket* const left, const BaseSocket* const right) const
			{
				return left->getUnderlayingHandle() < right->getUnderlayingHandle();
			}
		};
	public:
		using ListenSocketsContainer = Set<const BaseSocket*, SocketPred>;
		using AvailableSocketsContainer = Map<const BaseSocket*, unsigned char>;
	private:
		ListenSocketsContainer listenedSockets;
		AvailableSocketsContainer availableSockets;
	public:
		SocketAwaitor() = default;
		~SocketAwaitor() = default;

		void addSocket(const BaseSocket& sock);
		void removeSocket(const BaseSocket& sock);

		/// @brief Waits for any of the sockets to become available for writing, reading or if something exceptional happens, depending on the howToWait argument.
		/// @param howToWait Wait flag. Made from ORed WaitOp values.
		/// @param timeout Maximum waiting time. It's impossible to specify infinite time.
		/// @return Socket::Status::Done if any socket became available for any of the specified operations, Socket::Status::NotReady otherwise, Socket::Status::Error if an error has occured. Use getSocketStatus to query particular socket's status.
		Socket::Status wait(const unsigned char howToWait, const Time& timeout);

		AvailableSocketsContainer::const_iterator begin() const;
		AvailableSocketsContainer::const_iterator end() const;

		unsigned char getSocketStatus(const BaseSocket& sock) const;
	};
}

#endif
