#include "TcpSocket.h"

#ifdef GLOWE_COMPILE_FOR_LINUX
static constexpr int recvSendFlags = MSG_NOSIGNAL;
#else
static constexpr int recvSendFlags = 0;
#endif

GLOWE::TcpSocket::TcpSocket()
	: BaseSocket(Socket::Type::TCP), awaitingPacket()
{
}

GLOWE::TcpSocket::TcpSocket(const bool block, const bool useIPv6)
	: BaseSocket(Socket::Type::TCP, block, useIPv6), awaitingPacket()
{
}

GLOWE::TcpSocket::TcpSocket(TcpSocket&& arg) noexcept
	: BaseSocket(std::move(arg)), awaitingPacket(std::move(arg.awaitingPacket))
{
}

GLOWE::TcpSocket& GLOWE::TcpSocket::operator=(TcpSocket&& arg) noexcept
{
	BaseSocket::operator=(std::move(arg));
	awaitingPacket = std::move(arg.awaitingPacket);
	return *this;
}

GLOWE::Socket::Status GLOWE::TcpSocket::connect(const Ip & ip, const unsigned short port, const Time & timeout)
{
	open();

	sockaddr_in6 addr;
	SocketImpl::createAddress(ip, port, &addr);

	if (timeout <= Time::ZeroTime)
	{
		if (::connect(socketHandle, (sockaddr*)&addr, usesIPv6 ? sizeof(sockaddr_in6) : sizeof(sockaddr_in)) == -1)
		{
			return SocketImpl::getErrorCode();
		}
	}
	else
	{
		const bool block = isBlocking;

		if (block)
		{
			setBlocking(false);
		}

		if (::connect(socketHandle, (sockaddr*)&addr, usesIPv6 ? sizeof(sockaddr_in6) : sizeof(sockaddr_in)) >= 0)
		{
			// Connection established.
			setBlocking(block);
			return Socket::Status::Done;
		}

		Socket::Status status = SocketImpl::getErrorCode();

		// Non-blocking mode - we can't block current thread.
		if (!block)
		{
			return status;
		}

		if (status == Socket::Status::NotReady)
		{
			fd_set fdSet;
			FD_ZERO(&fdSet);
			FD_SET(socketHandle, &fdSet);

			timeval tempTimeout;
			// We can't use asSeconds()
			tempTimeout.tv_sec = timeout.asMicroseconds() / 1000000;
			tempTimeout.tv_usec = timeout.asMicroseconds() % 1000000;

			if (select(socketHandle + 1, nullptr, &fdSet, nullptr, &tempTimeout) > 0)
			{
				if (getRemoteIP() != Ip::none)
				{
					status = Socket::Status::Done;
				}
				else
				{
					status = SocketImpl::getErrorCode();
					close();
				}
			}
			else
			{
				status = SocketImpl::getErrorCode();
				close();
			}
		}

		setBlocking(true);

		return status;
	}

	return Socket::Status::Done;
}

GLOWE::Socket::Status GLOWE::TcpSocket::connect(const Url& url, const unsigned short port, const Time& timeout)
{
	Socket::Status result = Socket::Status::Error;
	const auto ips = url.toIPs(usesIPv6);
	for (const auto& ip : ips)
	{
		switch (ip.getProto())
		{
		case Ip::IpProto::IPv4:
			if (!usesIPv6)
			{
				result = TcpSocket::connect(ip, port, timeout);
			}
			else
			{
				result = Socket::Status::Error;
			}
			break;
		case Ip::IpProto::IPv6:
			if (usesIPv6 && !ip.checkIsMappedIPv4())
			{
				result = TcpSocket::connect(ip, port, timeout);
			}
			else
			{
				result = Socket::Status::Error;
			}
			break;
		}

		if (result == Socket::Status::Done)
		{
			break;
		}
	}

	// If we failed to connect using "pure" IPv6, we'll try to connect using mapped IPv4 addresses.
	if (result != Socket::Status::Done && usesIPv6)
	{
		for (const auto& ip : ips)
		{
			if (ip.getProto() == Ip::IpProto::IPv6 && ip.checkIsMappedIPv4())
			{
				result = TcpSocket::connect(ip, port, timeout);
			}

			if (result == Socket::Status::Done)
			{
				break;
			}
		}
	}

	return result;
}

void GLOWE::TcpSocket::disconnect()
{
	close();
	awaitingPacket = AwaitingPacket();
}

GLOWE::Socket::Status GLOWE::TcpSocket::send(const void* data, const std::size_t bytesToSend) const
{
	if (!isBlocking)
	{
		WarningThrow(false, String(u8"Partial sends may occur. GlowNetwork is unable to handle them properly. Better use send(const ()*, const std::size_t& bytesToSend, std::size_t& bytesSent) and handle them yourself or switch to the blocking mode."));
	}

	std::size_t notImportant = 0;

	return send(data, bytesToSend, notImportant);
}

GLOWE::Socket::Status GLOWE::TcpSocket::send(const void* data, const std::size_t bytesToSend, std::size_t & bytesSent) const
{
	if (!data || bytesToSend == 0)
	{
		WarningThrow(false, String(u8"Unable to send data (invalid pointer or bytesToSend equals 0)."));
		return Socket::Status::Error;
	}

	int temp = 0;
	for (bytesSent = 0; bytesSent < bytesToSend; bytesSent += temp)
	{
		temp = ::send(socketHandle, (const char*)(data)+bytesSent, bytesToSend - bytesSent, recvSendFlags);

		if (temp < 0)
		{
			Socket::Status status = SocketImpl::getErrorCode();

			if (bytesSent && (status == Socket::Status::NotReady))
			{
				return Socket::Status::Partial;
			}

			return status;
		}
	}

	return Socket::Status::Done;
}

GLOWE::Socket::Status GLOWE::TcpSocket::send(Packet & packet) const
{
	// First, we send packet's size, then we send its data.

	std::size_t sizeReturned = 0;
	const void* dataToSend = packet.onSend(sizeReturned);

	const UInt32 packetSize = htonl(sizeReturned); // We need a type with "static" size among most of platforms.

	Vector<char> block(sizeReturned + sizeof(packetSize));
	std::memcpy(block.data(), &packetSize, sizeof(packetSize));
	if (sizeReturned > 0)
	{
		std::memcpy(block.data() + sizeof(packetSize), dataToSend, sizeReturned);
	}

	std::size_t bytesSent;
	Socket::Status status = send(block.data() + packet.sendPos, block.size() - packet.sendPos, bytesSent);

	if (status == Socket::Status::Partial)
	{
		packet.sendPos += bytesSent;
	}
	else
	if(status == Socket::Status::Done)
	{
		packet.sendPos = 0;
	}

	return status;
}

GLOWE::Socket::Status GLOWE::TcpSocket::receive(void* buffer, const std::size_t bufferSize, std::size_t & bytesReceived) const
{
	bytesReceived = 0;

	if (!buffer || bufferSize == 0)
	{
		WarningThrow(false, String(u8"Unable to receive data (invalid buffer's pointer or size)."));
		return Socket::Status::Error;
	}

	int received = recv(socketHandle, (char*)(buffer), bufferSize, recvSendFlags);

	if (received > 0)
	{
		bytesReceived = received;
		return Socket::Status::Done;
	}
	else
	if(received == 0)
	{
		return Socket::Status::Disconnected;
	}

	return SocketImpl::getErrorCode();
}

GLOWE::Socket::Status GLOWE::TcpSocket::receive(Packet & packet)
{
	Socket::Status status = Socket::Status::Error;

	packet.reset();

	// We've sent packet's size in send(Packet).
	UInt32 packetSize = 0;
	std::size_t received = 0;
	char* temp = nullptr;
		
	do
	{
		temp = (char*)(&awaitingPacket.packetSize) + awaitingPacket.bytesReceived;
		status = receive(temp, sizeof(awaitingPacket.packetSize) - awaitingPacket.bytesReceived, received);
		awaitingPacket.bytesReceived += received;

		if (status != Socket::Status::Done && status != Socket::Status::NotReady)
		{
			return status;
		}
	} 
	while (awaitingPacket.bytesReceived < sizeof(awaitingPacket.packetSize));

	packetSize = ntohl(awaitingPacket.packetSize);

	char buffer[1024];
	while (awaitingPacket.data.size() < packetSize)
	{
		std::size_t finalSize = std::min((std::size_t)(packetSize - awaitingPacket.data.size()), sizeof(buffer));

		status = receive(buffer, finalSize, received);
		if (status != Socket::Status::Done && status != Socket::Status::NotReady)
		{
			return status;
		}

		if (received > 0)
		{
			awaitingPacket.data.resize(awaitingPacket.data.size() + received);
			std::memcpy(awaitingPacket.data.data() + awaitingPacket.data.size() - received, buffer, received);
		}
	}

	if (!awaitingPacket.data.empty())
	{
		packet.onReceive(awaitingPacket.data.data(), awaitingPacket.data.size());
	}

	awaitingPacket = AwaitingPacket();

	return Socket::Status::Done;
}

unsigned short GLOWE::TcpSocket::getLocalPort() const
{
	if (socketHandle != SocketImpl::invalidSocket())
	{
		sockaddr_in6 addr;
		socklen_t addrLen = sizeof(addr);
		if (getsockname(socketHandle, (sockaddr*)&addr, &addrLen) != -1)
		{
			return ntohs(addr.sin6_port);
		}
	}

	return 0;
}

GLOWE::Ip GLOWE::TcpSocket::getRemoteIP() const
{
	if (socketHandle != SocketImpl::invalidSocket())
	{
		sockaddr_in6 addr;
		socklen_t addrLen = sizeof(addr);
		if (getpeername(socketHandle, (sockaddr*)&addr, &addrLen) != -1)
		{
			switch (addr.sin6_family)
			{
			case AF_INET:
				return Ip(((sockaddr_in*)&addr)->sin_addr.s_addr, false);
			case AF_INET6:
				return Ip(reinterpret_cast<UInt16(&)[8]>(addr.sin6_addr.s6_addr), addr.sin6_scope_id, false);
			}
		}
	}

	return Ip::none;
}

unsigned short GLOWE::TcpSocket::getRemotePort() const
{
	if (socketHandle != SocketImpl::invalidSocket())
	{
		sockaddr_in6 addr;
		socklen_t addrLen = sizeof(addr);
		if (getpeername(socketHandle, (sockaddr*)&addr, &addrLen) != -1)
		{
			return ntohs(addr.sin6_port);
		}
	}

	return 0;
}

bool GLOWE::TcpSocket::checkIfConnectionWasClosed() const
{
	if (socketHandle == SocketImpl::invalidSocket())
	{
		return true;
	}

	fd_set set;
	FD_ZERO(&set);
	FD_SET(socketHandle, &set);

	timeval timeout;
	timeout.tv_sec = timeout.tv_usec = 0;

	const auto selectResult = select(socketHandle + 1, &set, nullptr, nullptr, &timeout);
	if (selectResult <= 0)
	{
		return false;
	}

	char garbage;
	const auto recvResult = recv(socketHandle, &garbage, 1, MSG_PEEK | recvSendFlags);

	return recvResult <= 0;
}
