#pragma once
#ifndef GLOWE_UNIFORM_NETWORK_SOCKETUTILITY_INCLUDED
#define GLOWE_UNIFORM_NETWORK_SOCKETUTILITY_INCLUDED

#include "glownetwork_export.h"

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/Error.h"
#include "../../GlowSystem/String.h"
#include "../../GlowSystem/Time.h"
#include "../../GlowSystem/Regex.h"

#ifdef GLOWE_COMPILE_FOR_LINUX
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <arpa/inet.h>
#endif

namespace GLOWE
{
	class GLOWNETWORK_EXPORT Ip
	{
	public:
		enum class IpProto
		{
			Invalid,
			IPv4,
			IPv6
		};
	private:
		IpProto proto;
		union
		{
			UInt32 ipv4;
			struct
			{
				union
				{
					UInt8 addr[16];
					UInt16 u16Addr[8];
				};
				unsigned long scopeId = 0;
			} ipv6;
		};
	public:
		Ip();
		Ip(const UInt8 (&arg)[4]);
		Ip(const UInt8 arg0, const UInt8 arg1, const UInt8 arg2, const UInt8 arg3);
		explicit Ip(const UInt32 arg, const bool convertToNet = false);
		explicit Ip(const UInt16 (&addr)[8], const unsigned long scopeId = 0, const bool convertToNet = true);
		Ip(const UInt16 b0, const UInt16 b1, const UInt16 b2, const UInt16 b3, const UInt16 b4, const UInt16 b5, const UInt16 b6, const UInt16 b7, const unsigned long scopeId = 0, const bool convertToNet = true);

		/// @brief Converts the IP address to human readable string.
		/// @return Human readable string.
		String toString() const;
		/// @brief Returns the IPv4 address as UInt32.
		/// @param convertToHost If true the resulting address will be in host order, otherwise in net order.
		/// @return IPv4 address or 0 if the IP is invalid or represents an IPv6 address.
		UInt32 toUInt32(const bool convertToHost = true) const;
		/// @brief Returns the IPv6 address as 16 byte wide array.
		/// @param convertToHost If true the resulting address will be in host order, otherwise in net order.
		/// @return IPv6 address or array of zeroes if the IP is invalid or represents an IPv4 address (not mapped to IPv6).
		Array<UInt8, 16> toByteArrayPtr(const bool convertToHost = true) const;

		bool checkIsValid() const;

		/// @brief Returns whether the IPv6 address is a mapped IPv4 address.
		/// @return true if the address is a mapped IPv4 address, false otherwise or if the address is an IPv4 address.
		bool checkIsMappedIPv4() const;

		IpProto getProto() const;
		unsigned long getScopeId(const bool convertToHost = true) const;

		void setScopeId(const unsigned long scopeId);

		operator GLOWE::String() const;

		bool operator==(const Ip& right) const noexcept;
		bool operator!=(const Ip& right) const noexcept;

		static Vector<Ip> resolve(const String& addr, const bool lookForIPv6);

		/*static Ip getPublicIP(const Time& timeout = Time::ZeroTime);
		static Ip getPrivateIP();*/

		static const Ip any;
		static const Ip anyIPv6;
		static const Ip none;
		static const Ip broadcast;
		static const Ip localhost;
		static const Ip localhostIPv6;
	};

	class GLOWNETWORK_EXPORT Url
	{
	private:
		static const Regex regex;

		String url;
		bool isValid;
	public:
		Url();
		Url(const String& arg, const bool checkIfSolvableAddress = false);

		String toString() const;
		Vector<Ip> toIPs(const bool useIPv6 = false) const;

		bool checkIsValid() const;

		String getHTTPHost() const; // Gets "Host" name for a HTTP request, eg. www.glowstickstudio.com in www.glowstickstudio.com/projects/ip
		String getRequestedResource() const; // Gets the URL's requested resource, eg. /projects/ip/ in www.glowstickstudio.com/projects/ip

		bool operator==(const Url& right) const;
		bool operator!=(const Url& right) const;

		static bool validate(const String& arg, const bool checkIfSolvableAddress = false);
	};

	namespace Socket
	{
		enum class Type
		{
			TCP,
			UDP
		};

		enum class Status
		{
			Done,
			NotReady,
			Partial,
			Disconnected,
			Error
		};

		/// @brief Returns the current socket error. On Linux it returns description of errno, so the error may be inaccurate.
		/// @return Socket error description.
		GLOWNETWORK_EXPORT String getErrorDesc();
	}
}

#endif
