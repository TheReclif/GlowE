#include "HTTP.h"

GLOWE::HTTPRequest::HTTPRequest()
	: requestType(Type::Invalid), requestedResource(), headerValues()
{
	headerValues.emplace(u8"Accept-Encoding", u8"chunked;q=1.0,identity;q=0.8,*;q=0.0");
	headerValues.emplace(u8"Host", u8"");
	headerValues.emplace(u8"User-Agent", getDefaultUserAgent());
}

GLOWE::HTTPRequest::HTTPRequest(const Type reqType, const Url& requestedRes)
	: requestType(reqType), requestedResource(requestedRes.getRequestedResource()), headerValues()
{
	headerValues.emplace(u8"Accept-Encoding", u8"chunked;q=1.0,identity;q=0.8,*;q=0.0");
	headerValues.emplace(u8"Host", requestedRes.getHTTPHost());
	headerValues.emplace(u8"User-Agent", getDefaultUserAgent());
}

GLOWE::HTTPRequest::HTTPRequest(const Type reqType, const String & requestedRes)
	: requestType(reqType), requestedResource(requestedRes), headerValues()
{
	headerValues.emplace(u8"Accept-Encoding", u8"chunked;q=1.0,identity;q=0.8,*;q=0.0");
	headerValues.emplace(u8"Host", u8"");
	headerValues.emplace(u8"User-Agent", getDefaultUserAgent());
}

void GLOWE::HTTPRequest::setType(const Type arg)
{
	requestType = arg;
}

void GLOWE::HTTPRequest::setRequestedResource(const String & arg)
{
	requestedResource = arg;
}

void GLOWE::HTTPRequest::setRequestedResource(const Url & arg)
{
	requestedResource = arg.getRequestedResource();
	headerValues.find(u8"Host")->second = arg.getHTTPHost();
}

GLOWE::HTTPRequest::Field GLOWE::HTTPRequest::addField(const String& key, const String& value)
{
	if (key != u8"Host")
	{
		return headerValues.emplace(key, value);
	}
	else
	{
		return Field();
	}
}

void GLOWE::HTTPRequest::removeField(const Field& field)
{
	if (field->first != u8"Host")
	{
		headerValues.erase(field);
	}
}

void GLOWE::HTTPRequest::addPacketInfo(const Packet& packet)
{
	Field field = headerValues.find(u8"Content-Length");
	String temp = toString(packet.getDataSize());

	if (field != headerValues.end())
	{
		field->second = std::move(temp);
	}
	else
	{
		headerValues.emplace(u8"Content-Length", std::move(temp));
	}
}

GLOWE::String GLOWE::HTTPRequest::createRequest() const
{
	String result;

	switch (requestType)
	{
	case Type::Get:
		result = u8"GET ";
		break;
	case Type::Head:
		result = u8"HEAD ";
		break;
	case Type::Post:
		result = u8"POST ";
		break;
	case Type::Put:
		result = u8"PUT ";
		break;
	case Type::Delete:
		result = u8"DELETE ";
		break;
	case Type::Options:
		result = u8"OPTIONS ";
		break;
	case Type::Trace:
		result = u8"TRACE ";
		break;
	default:
		return String();
		break;
	}

	if (requestedResource.isEmpty())
	{
		result += u8"/";
	}
	else
	{
		result += requestedResource;
	}
	result += u8" HTTP/1.1\r\n";

	for (const auto& x : headerValues)
	{
		result += x.first;
		result += u8": ";
		if (x.second == String() && x.first == u8"Host")
		{
			result += u8"{HOST}";
		}
		else
		{
			result += x.second;
		}
		result += u8"\r\n";
	}

	result += u8"\r\n";

	return result;
}

GLOWE::String GLOWE::HTTPRequest::getDefaultUserAgent()
{
	return String(u8"GlowNetwork/" GLOWE_VERSION_NUMBER);
}

bool GLOWE::HTTPResponse::checkIsSuccess() const
{
	return ((code >= 200) && (code < 300));
}

bool GLOWE::HTTPResponse::checkIsValid() const
{
	return code != 0;
}

GLOWE::UInt GLOWE::HTTPResponse::getCode() const
{
	return code;
}

GLOWE::String GLOWE::HTTPResponse::getCodeDesc() const
{
	return codeDesc;
}

GLOWE::String GLOWE::HTTPResponse::getHTTPVersion() const
{
	return httpVersion;
}

const GLOWE::Multimap<GLOWE::String, GLOWE::String>& GLOWE::HTTPResponse::getFields() const
{
	return headerValues;
}
