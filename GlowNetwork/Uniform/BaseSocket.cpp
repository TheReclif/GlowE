#include "BaseSocket.h"

GLOWE::BaseSocket::BaseSocket(const Socket::Type & arg)
	: type(arg), isBlocking(true), usesIPv6(false), socketHandle(SocketImpl::invalidSocket()), errorsDuringOptionSetting(), options()
{
}

GLOWE::BaseSocket::BaseSocket(const Socket::Type & argType, const bool block, const bool useIPv6)
	: type(argType), isBlocking(block), usesIPv6(useIPv6), socketHandle(SocketImpl::invalidSocket()), errorsDuringOptionSetting(), options()
{
}

GLOWE::BaseSocket::BaseSocket(BaseSocket&& arg) noexcept
	: type(arg.type), isBlocking(arg.isBlocking), usesIPv6(arg.usesIPv6), socketHandle(GLOWE::exchange(arg.socketHandle, SocketImpl::invalidSocket())), errorsDuringOptionSetting(std::move(arg.errorsDuringOptionSetting)), options(std::move(arg.options))
{
}

void GLOWE::BaseSocket::open()
{
	if (socketHandle != SocketImpl::invalidSocket())
	{
		return;
	}

	// https://stackoverflow.com/questions/6729366/what-is-the-difference-between-af-inet-and-pf-inet-in-socket-programming
	// You can use AF_INET or PF_INET, it doesn't matter.
	const SocketHandle tempHandle = socket(usesIPv6 ? AF_INET6 : AF_INET, type == Socket::Type::TCP ? SOCK_STREAM : SOCK_DGRAM, 0);

	if (tempHandle == SocketImpl::invalidSocket())
	{
		WarningThrow(false, String(u8"socket function failed."));
		return;
	}

	open(tempHandle);
}

void GLOWE::BaseSocket::open(SocketHandle arg)
{
	if (arg == SocketImpl::invalidSocket() || socketHandle != SocketImpl::invalidSocket())
	{
		return;
	}

	socketHandle = arg;

	setBlocking(isBlocking);
	const int temp = 1;

	if (type == Socket::Type::TCP)
	{
		if (setsockopt(socketHandle, IPPROTO_TCP, TCP_NODELAY, (char*)&temp, sizeof(temp)) == -1)
		{
			WarningThrow(false, String(u8"Unable to disable Nagle's algorithm. TCP data will be buffered."));
		}

#ifdef GLOWE_COMPILE_FOR_MAC
		if (setsockopt(socketHandle, SOL_SOCKET, SO_NOSIGPIPE, (char*)temp, sizeof(temp) == -1)
		{
			WarningThrow(false, String(u8"Unable to disable SIGPIPE signal."));
		}
#endif

	}
	else
	{
		if (setsockopt(socketHandle, SOL_SOCKET, SO_BROADCAST, (char*)&temp, sizeof(temp)))
		{
			WarningThrow(false, u8"Unable to enable UDP broadcast.");
		}
	}

	errorsDuringOptionSetting.clear();
	for (const auto& op : options)
	{
		if (!setOption(socketOptionToLevelInt(op.first), socketOptionToInt(op.first), op.second.getCharArray(), op.second.getSize()))
		{
			errorsDuringOptionSetting.emplace(op.first);
		}
	}
}

void GLOWE::BaseSocket::close()
{
	if (socketHandle != SocketImpl::invalidSocket())
	{
		SocketImpl::closeSocket(socketHandle);
		socketHandle = SocketImpl::invalidSocket();
	}
}

int GLOWE::BaseSocket::socketOptionToInt(const SocketOption option)
{
	using Opt = SocketOption;
	switch (option)
	{
	case Opt::Broadcast:
		return SO_BROADCAST;
	case Opt::ChecksumCoverage:
#ifdef GLOWE_COMPILE_FOR_WINDOWS
		return UDP_CHECKSUM_COVERAGE;
#else
		return 0;
#endif
	case Opt::DontLinger:
#ifdef GLOWE_COMPILE_FOR_WINDOWS
		return SO_DONTLINGER;
#else
		return SO_LINGER; // TODO: Implement SO_LINGER as an emulation of the missing SO_DONTLINGER.
#endif
	case Opt::ExclusiveAddress:
#ifdef GLOWE_COMPILE_FOR_WINDOWS
		return SO_EXCLUSIVEADDRUSE;
#else
		return 0;
#endif
	case Opt::Expedited:
#ifdef GLOWE_COMPILE_FOR_WINDOWS
		return TCP_EXPEDITED_1122;
#else
		return 0;
#endif
	case Opt::KeepAlive:
		return SO_KEEPALIVE;
	case Opt::MulticastLoop:
		return IP_MULTICAST_LOOP;
	case Opt::NoChecksum:
#ifdef GLOWE_COMPILE_FOR_WINDOWS
		return UDP_NOCHECKSUM;
#else
		return SO_NO_CHECK; // TODO: Check.
#endif
	case Opt::NoDelay:
		return TCP_NODELAY;
	case Opt::OutOfBandInline:
		return SO_OOBINLINE;
	case Opt::ReceiveBroadcast:
#ifdef GLOWE_COMPILE_FOR_WINDOWS
		return IP_RECEIVE_BROADCAST;
#else
		return 0;
#endif
	case Opt::ReceiveTimeOut:
		return SO_RCVTIMEO;
	case Opt::ReuseAddress:
		return SO_REUSEADDR;
	case Opt::SendTimeOut:
		return SO_SNDTIMEO;
	case Opt::V6Only:
		return IPV6_V6ONLY;
	default:
		return -1;
	}
}

int GLOWE::BaseSocket::socketLevelToInt(const SocketOptionLevel level)
{
	switch (level)
	{
	case SocketOptionLevel::Socket:
		return SOL_SOCKET;
	case SocketOptionLevel::TCP:
		return IPPROTO_TCP;
	case SocketOptionLevel::UDP:
		return IPPROTO_UDP;
	case SocketOptionLevel::IP:
		return IPPROTO_IP;
	case SocketOptionLevel::IPv6:
		return IPPROTO_IPV6;
	default:
		return -1;
	}
}

int GLOWE::BaseSocket::socketOptionToLevelInt(const SocketOption option)
{
	SocketOptionLevel level;
	switch (option)
	{
	case SocketOption::Broadcast:
		level = SocketOptionLevel::Socket;
		break;
	case SocketOption::ChecksumCoverage:
		level = SocketOptionLevel::UDP;
		break;
	case SocketOption::DontLinger:
		level = SocketOptionLevel::Socket;
		break;
	case SocketOption::ExclusiveAddress:
		level = SocketOptionLevel::Socket;
		break;
	case SocketOption::Expedited:
		level = SocketOptionLevel::TCP;
		break;
	case SocketOption::KeepAlive:
		level = SocketOptionLevel::Socket;
		break;
	case SocketOption::MulticastLoop:
		level = SocketOptionLevel::IP;
		break;
	case SocketOption::NoChecksum:
#ifdef GLOWE_COMPILE_FOR_WINDOWS
		level = SocketOptionLevel::UDP;
#else
		level = SocketOptionLevel::Socket;
#endif
		break;
	case SocketOption::NoDelay:
		level = SocketOptionLevel::TCP;
		break;
	case SocketOption::OutOfBandInline:
		level = SocketOptionLevel::Socket;
		break;
	case SocketOption::ReceiveBroadcast:
		level = SocketOptionLevel::IP;
		break;
	case SocketOption::ReceiveTimeOut:
		level = SocketOptionLevel::Socket;
		break;
	case SocketOption::ReuseAddress:
		level = SocketOptionLevel::Socket;
		break;
	case SocketOption::SendTimeOut:
		level = SocketOptionLevel::Socket;
		break;
	case SocketOption::V6Only:
		level = SocketOptionLevel::IPv6;
		break;
	}

	return socketLevelToInt(level);
}

GLOWE::BaseSocket::~BaseSocket()
{
	close();
}

GLOWE::BaseSocket& GLOWE::BaseSocket::operator=(BaseSocket&& arg)
{
	if (type != arg.type)
	{
		ErrorThrow(false);
		throw Exception("type != arg.type in BaseSocket::operator=(BaseSocket&&)");
	}
	isBlocking = arg.isBlocking;
	usesIPv6 = arg.usesIPv6;
	socketHandle = GLOWE::exchange(arg.socketHandle, SocketImpl::invalidSocket());
	errorsDuringOptionSetting = std::move(arg.errorsDuringOptionSetting);
	options = std::move(arg.options);
	return *this;
}

bool GLOWE::BaseSocket::checkIsBlocking() const
{
	return isBlocking;
}

bool GLOWE::BaseSocket::checkIfUsesIPv6() const
{
	return usesIPv6;
}

bool GLOWE::BaseSocket::checkIfAnyOptionFailed() const
{
	return errorsDuringOptionSetting.size() > 0;
}

const GLOWE::UnorderedSet<GLOWE::BaseSocket::SocketOption>& GLOWE::BaseSocket::getFailedOptions() const
{
	return errorsDuringOptionSetting;
}

void GLOWE::BaseSocket::setIPv6Use(const bool arg)
{
	if (socketHandle == SocketImpl::invalidSocket())
	{
		usesIPv6 = arg;
	}
}

void GLOWE::BaseSocket::setBlocking(const bool arg)
{
	if (socketHandle != SocketImpl::invalidSocket())
	{
		SocketImpl::setBlocking(socketHandle, arg);
	}

	isBlocking = arg;
}

bool GLOWE::BaseSocket::setOption(const int level, const int option, const void* const arg, const unsigned int dataSize)
{
	return setsockopt(socketHandle, level, option, static_cast<const char*>(arg), dataSize) == 0;
}

void GLOWE::BaseSocket::setOption(const SocketOption option, const int arg)
{
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	const DWORD finalArg = arg;
#else
	const int finalArg = arg;
#endif

	options[option] = String(reinterpret_cast<const char*>(&finalArg), sizeof(finalArg));

	if (socketHandle != SocketImpl::invalidSocket())
	{
		if (!setOption(socketOptionToLevelInt(option), socketOptionToInt(option), &finalArg, sizeof(finalArg)))
		{
			errorsDuringOptionSetting.emplace(option);
		}
		else
		{
			errorsDuringOptionSetting.erase(option);
		}
	}
}

void GLOWE::BaseSocket::setOption(const SocketOption option, const bool arg)
{
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	const DWORD finalArg = arg ? TRUE : FALSE;
#else
	const int finalArg = arg ? 1 : 0;
#endif

	options[option] = String(reinterpret_cast<const char*>(&finalArg), sizeof(finalArg));

	if (socketHandle != SocketImpl::invalidSocket())
	{
		if (!setOption(socketOptionToLevelInt(option), socketOptionToInt(option), &finalArg, sizeof(finalArg)))
		{
			errorsDuringOptionSetting.emplace(option);
		}
		else
		{
			errorsDuringOptionSetting.erase(option);
		}
	}
}

const GLOWE::SocketHandle& GLOWE::BaseSocket::getUnderlayingHandle() const
{
	return socketHandle;
}
