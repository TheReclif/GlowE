#pragma once
#ifndef GLOWE_UNIFORM_NETWORK_SOCKETHANDLE_INCLUDED
#define GLOWE_UNIFORM_NETWORK_SOCKETHANDLE_INCLUDED

#include "../../GlowSystem/Utility.h"

namespace GLOWE
{
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	using SocketHandle = SOCKET;
#else
	using SocketHandle = int;
#endif
}

#endif
