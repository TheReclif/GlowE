#include "SocketUtility.h"

const GLOWE::Ip GLOWE::Ip::any = GLOWE::Ip(0, 0, 0, 0);
const GLOWE::Ip GLOWE::Ip::none = GLOWE::Ip();
const GLOWE::Ip GLOWE::Ip::broadcast = GLOWE::Ip(255, 255, 255, 255);
const GLOWE::Ip GLOWE::Ip::localhost = GLOWE::Ip(127, 0, 0, 1);

const GLOWE::Ip GLOWE::Ip::anyIPv6 = GLOWE::Ip(0, 0, 0, 0, 0, 0, 0, 0);
const GLOWE::Ip GLOWE::Ip::localhostIPv6 = GLOWE::Ip(0, 0, 0, 0, 0, 0, 0, 1);

const GLOWE::Regex GLOWE::Url::regex = GLOWE::Regex("([\\w\\.]+)(\\/?[\\w\\.\\/]*)");

GLOWE::Vector<GLOWE::Ip> GLOWE::Ip::resolve(const String & addr, const bool lookForIPv6)
{
	Vector<Ip> resultIps;
	if (addr == u8"255.255.255.255")
	{
		// UDP IPv4 broadcast address.
		resultIps.emplace_back(INADDR_BROADCAST, false);
		//ipv4 = INADDR_BROADCAST;
		//proto = IpProto::IPv4;
	}
	else if (addr == u8"0.0.0.0")
	{
		// IPv4 any address.
		resultIps.emplace_back(INADDR_ANY, false);
		//ipv4 = INADDR_ANY;
		///proto = IpProto::IPv4;
	}
	else
	{
		addrinfo adrInfo;
		std::memset(&adrInfo, 0, sizeof(adrInfo));
		adrInfo.ai_family = lookForIPv6 ? AF_INET6 : AF_INET;
		adrInfo.ai_flags = lookForIPv6 ? (AI_V4MAPPED | AI_ALL) : 0;
		addrinfo* result = nullptr, *resAlloc = nullptr;

		const int status = getaddrinfo(addr.getCharArray(), nullptr, &adrInfo, &resAlloc);

		if (status == 0)
		{
			result = resAlloc;

			while (result)
			{
				if (result->ai_family == AF_INET)
				{
					const UInt32 ipv4 = reinterpret_cast<sockaddr_in*>(result->ai_addr)->sin_addr.s_addr;
					resultIps.emplace_back(ipv4, false);
				}
				else
				if (result->ai_family == AF_INET6)
				{
					resultIps.emplace_back();
					std::memcpy(resultIps.back().ipv6.addr, reinterpret_cast<sockaddr_in6*>(result->ai_addr)->sin6_addr.s6_addr, 16);
					resultIps.back().ipv6.scopeId = reinterpret_cast<sockaddr_in6*>(result->ai_addr)->sin6_scope_id;
					resultIps.back().proto = IpProto::IPv6;
				}
				result = result->ai_next;
			}

			freeaddrinfo(resAlloc);
		}
	}

	return resultIps;
}

GLOWE::Ip::Ip()
	: ipv4(0), proto(IpProto::Invalid)
{
}

GLOWE::Ip::Ip(const UInt8 (&arg)[4])
	: ipv4(htonl((arg[0] << 24) | (arg[1] << 16) | (arg[2] << 8) | arg[3])), proto(IpProto::IPv4)
{
}

GLOWE::Ip::Ip(const UInt8 arg0, const UInt8 arg1, const UInt8 arg2, const UInt8 arg3)
	: ipv4(htonl((arg0 << 24) | (arg1 << 16) | (arg2 << 8) | arg3)), proto(IpProto::IPv4)
{
}

GLOWE::Ip::Ip(const UInt32 arg, const bool convertToNet)
	: ipv4(convertToNet ? htonl(arg) : arg), proto(IpProto::IPv4)
{
}

GLOWE::Ip::Ip(const UInt16 (&addr)[8], const unsigned long scopeId, const bool convertToNet)
	: Ip(addr[0], addr[1], addr[2], addr[3], addr[4], addr[5], addr[6], addr[7], scopeId, convertToNet)
{
}

GLOWE::Ip::Ip(const UInt16 b0, const UInt16 b1, const UInt16 b2, const UInt16 b3, const UInt16 b4, const UInt16 b5, const UInt16 b6, const UInt16 b7, const unsigned long scopeId, const bool convertToNet)
	: proto(IpProto::IPv6)
{
	if (convertToNet)
	{
		ipv6.scopeId = htonl(scopeId);
		// https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-dpdx/2b400962-05ef-4e69-9185-927755b322a5
		ipv6.u16Addr[0] = htons(b0);
		ipv6.u16Addr[1] = htons(b1);
		ipv6.u16Addr[2] = htons(b2);
		ipv6.u16Addr[3] = htons(b3);
		ipv6.u16Addr[4] = htons(b4);
		ipv6.u16Addr[5] = htons(b5);
		ipv6.u16Addr[6] = htons(b6);
		ipv6.u16Addr[7] = htons(b7);
	}
	else
	{
		ipv6.scopeId = scopeId;
		ipv6.u16Addr[0] = b0;
		ipv6.u16Addr[1] = b1;
		ipv6.u16Addr[2] = b2;
		ipv6.u16Addr[3] = b3;
		ipv6.u16Addr[4] = b4;
		ipv6.u16Addr[5] = b5;
		ipv6.u16Addr[6] = b6;
		ipv6.u16Addr[7] = b7;
	}
}

GLOWE::String GLOWE::Ip::toString() const
{
	switch (proto)
	{
	case IpProto::IPv4:
	{
		char tempBuffer[INET_ADDRSTRLEN + 1] = "";
		in_addr addr;
		addr.s_addr = ipv4;
		inet_ntop(AF_INET, &addr, tempBuffer, INET_ADDRSTRLEN);
		return String(tempBuffer);
	}
	case IpProto::IPv6:
	{
		char tempBuffer[INET6_ADDRSTRLEN + 1] = "";
		in6_addr addr;
		std::memcpy(addr.s6_addr, ipv6.addr, 16);
		inet_ntop(AF_INET6, &addr, tempBuffer, INET6_ADDRSTRLEN);
		return String(tempBuffer);
	}
	default:
		return String();
	}
}

GLOWE::UInt32 GLOWE::Ip::toUInt32(const bool convertToHost) const
{
	if (proto == IpProto::IPv4)
	{
		return convertToHost ? ntohl(ipv4) : ipv4;
	}
	
	return 0;
}

GLOWE::Array<GLOWE::UInt8, 16> GLOWE::Ip::toByteArrayPtr(const bool convertToHost) const
{
	Array<UInt8, 16> result{};

	if (proto == IpProto::IPv6)
	{
		if (convertToHost)
		{
			for (unsigned int x = 0; x < 8; ++x)
			{
				reinterpret_cast<UInt16&>(result[x * 2]) = ntohs(ipv6.u16Addr[x]);
			}
		}
		else
		{
			std::memcpy(result.data(), ipv6.addr, 16);
		}
	}
	else
	{
		result.fill(0);
	}

	return result;
}

bool GLOWE::Ip::checkIsValid() const
{
	return proto != IpProto::Invalid;
}

bool GLOWE::Ip::checkIsMappedIPv4() const
{
	if (proto != IpProto::IPv6)
	{
		return false;
	}

	return ipv6.u16Addr[0] == 0
		&& ipv6.u16Addr[1] == 0
		&& ipv6.u16Addr[2] == 0
		&& ipv6.u16Addr[3] == 0
		&& ipv6.u16Addr[4] == 0
		&& ipv6.u16Addr[5] == 0xFFFF;
}

GLOWE::Ip::IpProto GLOWE::Ip::getProto() const
{
	return proto;
}

unsigned long GLOWE::Ip::getScopeId(const bool convertToHost) const
{
	if (proto != IpProto::IPv6)
	{
		return 0;
	}

	return convertToHost ? ntohl(ipv6.scopeId) : ipv6.scopeId;
}

void GLOWE::Ip::setScopeId(const unsigned long scopeId)
{
	if (proto == IpProto::IPv6)
	{
		ipv6.scopeId = htonl(scopeId);
	}
}

GLOWE::Ip::operator GLOWE::String() const
{
	return this->toString();
}

bool GLOWE::Ip::operator==(const Ip & right) const noexcept
{
	if (proto != right.proto)
	{
		return false;
	}

	switch (proto)
	{
	case IpProto::IPv4:
		return ipv4 == right.ipv4;
	case IpProto::IPv6:
		return std::memcmp(&ipv6, &right.ipv6, 16);
	default:
		return true;
	}
}

bool GLOWE::Ip::operator!=(const Ip & right) const noexcept
{
	return !(*this == right);
}

bool GLOWE::Url::validate(const String & arg, const bool checkIfSolvableAddress)
{
	RegexMatch match;

	if (std::regex_match(arg.getString(), match, regex))
	{
		const RegexSubMatch subMatch = match[1];
		if ((!checkIfSolvableAddress) || (Ip::resolve(String(subMatch.first, subMatch.second), true).size() > 0))
		{
			return true;
		}
	}

	return false;
}

GLOWE::Url::Url()
	: url(), isValid(false)
{
}

GLOWE::Url::Url(const String & arg, const bool checkIfSolvableAddress)
	: url(), isValid(false)
{
	if (validate(arg, checkIfSolvableAddress))
	{
		url = arg;
		isValid = true;
	}
}

GLOWE::String GLOWE::Url::toString() const
{
	return url;
}

GLOWE::Vector<GLOWE::Ip> GLOWE::Url::toIPs(const bool useIPv6) const
{
	return Ip::resolve(url, useIPv6);
}

bool GLOWE::Url::checkIsValid() const
{
	return isValid;
}

GLOWE::String GLOWE::Url::getHTTPHost() const
{
	RegexMatch match;
	RegexSubMatch subMatch;

	std::regex_match(url.getString(), match, regex);
	subMatch = match[1];

	return String(subMatch);
}

GLOWE::String GLOWE::Url::getRequestedResource() const
{
	RegexMatch match;
	RegexSubMatch subMatch;

	std::regex_match(url.getString(), match, regex);
	subMatch = match[2];

	return String(subMatch);
}

bool GLOWE::Url::operator==(const Url & right) const
{
	return url == right.url;
}

bool GLOWE::Url::operator!=(const Url & right) const
{
	return url != right.url;
}
