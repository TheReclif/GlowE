#include "SecureSocket.h"
#include "SSLCertUtility.h"

static constexpr char customCtrDrbgData[] = "Zuzi";

GLOWE::String mbedtlsErrToStr(const int err)
{
	char buff[2048] = "";
	mbedtls_strerror(err, buff, sizeof(buff));
	return buff;
}

bool GLOWE::SecureSocket::isSecureConnectionReady() const
{
	return isSSLAndTLSReady;
}

GLOWE::Socket::Status GLOWE::SecureSocket::sendNoSSL(const void* data, const std::size_t bytesToSend, std::size_t& bytesSent) const
{
	return TcpSocket::send(data, bytesToSend, bytesSent);
}

GLOWE::Socket::Status GLOWE::SecureSocket::receiveNoSSL(void* buffer, const std::size_t bufferSize, std::size_t& bytesReceived) const
{
	return TcpSocket::receive(buffer, bufferSize, bytesReceived);
}

GLOWE::Socket::Status GLOWE::SecureSocket::connectWithHostName()
{
	int result = 1;

	if ((result = mbedtls_ssl_set_hostname(&ssl, hostName.getCharArray())) != 0)
	{
		WarningThrow(false, "Unable to set the SSL hostname. Error: " + mbedtlsErrToStr(result));
		return Socket::Status::Error;
	}

	while ((result = mbedtls_ssl_handshake(&ssl)) != 0)
	{
		if (result != MBEDTLS_ERR_SSL_WANT_READ && result != MBEDTLS_ERR_SSL_WANT_WRITE)
		{
			WarningThrow(false, "Unable to perform a SSL handshake. Error: " + mbedtlsErrToStr(result));
			return Socket::Status::Error;
		}
	}

	UInt32 flags;
	if ((flags = mbedtls_ssl_get_verify_result(&ssl)) != 0)
	{
		char vrfyBuf[512];

		mbedtls_x509_crt_verify_info(vrfyBuf, sizeof(vrfyBuf), " ", flags);

		WarningThrow(false, String("X.509 certificate verification failed. Error (may be long): ") + vrfyBuf);
		return Socket::Status::Error;
	}

	return Socket::Status::Done;
}

GLOWE::Socket::Status GLOWE::SecureSocket::establishConnection(const Ip& ip, const unsigned short port, const Time& timeout)
{
	if (!isSSLAndTLSReady)
	{
		return Socket::Status::NotReady;
	}

	return TcpSocket::connect(ip, port, timeout);
}

GLOWE::Socket::Status GLOWE::SecureSocket::establishConnection(const Url& url, const unsigned short port, const Time& timeout)
{
	if (!isSSLAndTLSReady)
	{
		return Socket::Status::NotReady;
	}

	return TcpSocket::connect(url, port, timeout);
}

GLOWE::SecureSocket::SecureSocket()
	: isSSLAndTLSReady(false), hostName()
{
	mbedtls_ssl_init(&ssl);
	mbedtls_ssl_config_init(&conf);
	mbedtls_x509_crt_init(&cacert);
	mbedtls_ctr_drbg_init(&ctrDrbg);
	mbedtls_entropy_init(&entropy);

	mbedtls_debug_set_threshold(1);
	
	int result = 1;
	if ((result = mbedtls_ctr_drbg_seed(&ctrDrbg, mbedtls_entropy_func, &entropy, reinterpret_cast<const unsigned char*>(customCtrDrbgData), strlen(customCtrDrbgData))) != 0)
	{
		WarningThrow(false, "Unable to seed the random number generator (from mbedTLS). Error: " + mbedtlsErrToStr(result));
		return;
	}

	const String& certs = getInstance<SSLCertUtil>().getCerts();
	result = mbedtls_x509_crt_parse(&cacert, reinterpret_cast<const unsigned char*>(certs.getCharArray()), certs.getSize() + 1); //reinterpret_cast<const unsigned char*>(mbedtls_test_cas_pem), mbedtls_test_cas_pem_len
	if (result < 0)
	{
		WarningThrow(false, "Unable to load the CA root certificate. Error: " + mbedtlsErrToStr(-result));
		return;
	}

	if ((result = mbedtls_ssl_config_defaults(&conf,
		MBEDTLS_SSL_IS_CLIENT,
		MBEDTLS_SSL_TRANSPORT_STREAM,
		MBEDTLS_SSL_PRESET_DEFAULT)) != 0)
	{
		WarningThrow(false, "Unable to set up the SSL/TLS structure. Error: " + mbedtlsErrToStr(result));
		return;
	}

	mbedtls_ssl_conf_authmode(&conf, MBEDTLS_SSL_VERIFY_OPTIONAL);
	mbedtls_ssl_conf_ca_chain(&conf, &cacert, nullptr);
	mbedtls_ssl_conf_rng(&conf, mbedtls_ctr_drbg_random, &ctrDrbg);
	static void (*debugFunc)(void*, int, const char*, int, const char*) = [](void*, int level, const char* file, int line, const char* str)
	{
		String temp = file;
		temp += ':';
		temp += toString(line);
		temp += ": ";
		temp += str;
		logMessage("mbedTLS debug info: " + temp);
	};
	mbedtls_ssl_conf_dbg(&conf, debugFunc, nullptr);

	if ((result = mbedtls_ssl_setup(&ssl, &conf)) != 0)
	{
		WarningThrow(false, "SSL setup failed. Error: " + mbedtlsErrToStr(result));
		return;
	}

	static mbedtls_ssl_send_t* sendWrapper = [](void* thisPtr, const unsigned char* buffer, size_t len) -> int
	{
		SecureSocket* secPtr = static_cast<SecureSocket*>(thisPtr);
		size_t bytesSent = 0;
		Socket::Status status = secPtr->sendNoSSL(buffer, len, bytesSent);
		if (status != Socket::Status::Done && status != Socket::Status::Partial)
		{
			return -1;
		}

		return bytesSent;
	};
	static mbedtls_ssl_recv_t* recvWrapper = [](void* thisPtr, unsigned char* buffer, size_t len) -> int
	{
		SecureSocket* secPtr = static_cast<SecureSocket*>(thisPtr);
		size_t bytesReceived = 0;
		Socket::Status status = secPtr->receiveNoSSL(buffer, len, bytesReceived);
		if (status != Socket::Status::Done && status != Socket::Status::Partial)
		{
			return -1;
		}

		return bytesReceived;
	};

	mbedtls_ssl_set_bio(&ssl, this, sendWrapper, recvWrapper, nullptr);

	isSSLAndTLSReady = true;
}

GLOWE::SecureSocket::~SecureSocket()
{
	disconnect();

	mbedtls_x509_crt_free(&cacert);
	mbedtls_ssl_free(&ssl);
	mbedtls_ssl_config_free(&conf);
	mbedtls_ctr_drbg_free(&ctrDrbg);
	mbedtls_entropy_free(&entropy);
}

void GLOWE::SecureSocket::setHostName(const String& newHostName)
{
	hostName = newHostName;
}

GLOWE::Socket::Status GLOWE::SecureSocket::connect(const Ip& ip, const unsigned short port, const Time& timeout)
{
	const Socket::Status stat = establishConnection(ip, port, timeout);
	if (stat != Socket::Status::Done)
	{
		return stat;
	}
	return connectWithHostName();
}

GLOWE::Socket::Status GLOWE::SecureSocket::connect(const Url& url, const unsigned short port, const Time& timeout)
{
	hostName = url.getHTTPHost();
	const Socket::Status stat = establishConnection(url, port, timeout);
	if (stat != Socket::Status::Done)
	{
		return stat;
	}
	return connectWithHostName();
}

void GLOWE::SecureSocket::disconnect()
{
	if (isSSLAndTLSReady)
	{
		mbedtls_ssl_close_notify(&ssl);
		mbedtls_ssl_session_reset(&ssl);
	}

	TcpSocket::disconnect();
}

GLOWE::Socket::Status GLOWE::SecureSocket::send(const void* data, const std::size_t bytesToSend) const
{
	if (!isBlocking)
	{
		WarningThrow(false, String(u8"Partial sends may occur. GlowNetwork is unable to handle them properly. Better use send(const ()*, const std::size_t& bytesToSend, std::size_t& bytesSent) and handle them yourself or switch to the blocking mode."));
	}

	std::size_t notImportant = 0;

	return send(data, bytesToSend, notImportant);
}

GLOWE::Socket::Status GLOWE::SecureSocket::send(const void* data, const std::size_t bytesToSend, std::size_t& bytesSent) const
{
	if (!data || bytesToSend == 0)
	{
		WarningThrow(false, String(u8"Unable to send data (invalid pointer or bytesToSend equals 0)."));
		return Socket::Status::Error;
	}

	int temp = 0;
	for (bytesSent = 0; bytesSent < bytesToSend; bytesSent += temp)
	{
		//temp = ::send(socketHandle, (const char*)(data)+bytesSent, bytesToSend - bytesSent, recvSendFlags);
		temp = mbedtls_ssl_write(&ssl, reinterpret_cast<const unsigned char*>(data) + bytesSent, bytesToSend - bytesSent);

		if (temp < 0)
		{
			Socket::Status status = SocketImpl::getErrorCode();

			if (bytesSent && (status == Socket::Status::NotReady))
			{
				return Socket::Status::Partial;
			}

			return status;
		}
	}

	return Socket::Status::Done;
}

GLOWE::Socket::Status GLOWE::SecureSocket::send(Packet& packet) const
{
	// First, we send packet's size, then we send its data.

	std::size_t sizeReturned = 0;
	const void* dataToSend = packet.onSend(sizeReturned);

	UInt32 packetSize = htonl(sizeReturned); // We need a type with "static" size among most of platforms.

	Vector<char> block(sizeReturned + sizeof(packetSize));
	std::memcpy(block.data(), &packetSize, sizeof(packetSize));
	if (sizeReturned > 0)
	{
		std::memcpy(block.data() + sizeof(packetSize), dataToSend, sizeReturned);
	}

	std::size_t bytesSent;
	Socket::Status status = send(block.data() + packet.sendPos, block.size() - packet.sendPos, bytesSent);

	if (status == Socket::Status::Partial)
	{
		packet.sendPos += bytesSent;
	}
	else
	if (status == Socket::Status::Done)
	{
		packet.sendPos = 0;
	}

	return status;
}

GLOWE::Socket::Status GLOWE::SecureSocket::receive(void* buffer, const std::size_t bufferSize, std::size_t& bytesReceived) const
{
	bytesReceived = 0;

	if (!buffer || bufferSize == 0)
	{
		WarningThrow(false, String(u8"Unable to receive data (invalid buffer's pointer or size)."));
		return Socket::Status::Error;
	}

	//int received = recv(socketHandle, (char*)(buffer), bufferSize, recvSendFlags);
	int received = mbedtls_ssl_read(&ssl, reinterpret_cast<unsigned char*>(buffer), bufferSize);

	if (received > 0)
	{
		bytesReceived = received;
		return Socket::Status::Done;
	}
	else
	if (received == 0)
	{
		return Socket::Status::Disconnected;
	}

	return SocketImpl::getErrorCode();
}

GLOWE::Socket::Status GLOWE::SecureSocket::receive(Packet& packet)
{
	Socket::Status status = Socket::Status::Error;

	packet.reset();

	// We've sent packet's size in send(Packet).
	UInt32 packetSize = 0;
	std::size_t received = 0;
	char* temp = nullptr;

	do
	{
		temp = (char*)(&awaitingPacket.packetSize) + awaitingPacket.bytesReceived;
		status = receive(temp, sizeof(awaitingPacket.packetSize) - awaitingPacket.bytesReceived, received);
		awaitingPacket.bytesReceived += received;

		if (status != Socket::Status::Done)
		{
			return status;
		}
	} while (awaitingPacket.bytesReceived < sizeof(awaitingPacket.packetSize));

	packetSize = ntohl(awaitingPacket.packetSize);

	char buffer[1024];
	while (awaitingPacket.data.size() < packetSize)
	{
		std::size_t finalSize = std::min((std::size_t)(packetSize - awaitingPacket.data.size()), sizeof(buffer));

		status = receive(buffer, finalSize, received);
		if (status != Socket::Status::Done)
		{
			return status;
		}

		if (received > 0)
		{
			awaitingPacket.data.resize(awaitingPacket.data.size() + received);
			std::memcpy(awaitingPacket.data.data() + awaitingPacket.data.size() - received, buffer, received);
		}
	}

	if (!awaitingPacket.data.empty())
	{
		packet.onReceive(awaitingPacket.data.data(), awaitingPacket.data.size());
	}

	awaitingPacket = AwaitingPacket();

	return Socket::Status::Done;
}
