#pragma once
#ifndef GLOWE_NETWORK_HTTP_INL_INCLUDED
#define GLOWE_NETWORK_HTTP_INL_INCLUDED

#include "HTTP.h"

template<>
const unsigned short GLOWE::HTTP::portToUse = 80;
template<>
const unsigned short GLOWE::HTTPS::portToUse = 443;

template<class SocketType>
GLOWE::HTTPBase<SocketType>::HTTPBase()
	: isConnected(false), connectedSite(), sock()
{
}

template<class SocketType>
GLOWE::HTTPBase<SocketType>::HTTPBase(const Url& url, const Time& timeout, const bool useIPv6)
	: HTTPBase()
{
	this->connect(url, timeout, useIPv6);
}

template<class SocketType>
bool GLOWE::HTTPBase<SocketType>::connect(const Url& url, const Time& timeout, const bool useIPv6)
{
	if (!isConnected)
	{
		sock.setIPv6Use(useIPv6);
		Socket::Status status = sock.connect(url, portToUse, timeout);
		if (status == Socket::Status::Done)
		{
			isConnected = true;
			connectedSite = url;
		}
	}

	return isConnected;
}

template<class SocketType>
void GLOWE::HTTPBase<SocketType>::disconnect()
{
	sock.disconnect();
	isConnected = false;
	connectedSite = Url();
}

template<class SocketType>
GLOWE::Socket::Status GLOWE::HTTPBase<SocketType>::sendRequest(const HTTPRequest& req, const Packet& packet) const
{
	Socket::Status status = Socket::Status::Error;

	if (isConnected)
	{
		String requestBody = req.createRequest();
		std::size_t a = requestBody.find(u8"{HOST}");

		if (a != String::invalidPos)
		{
			requestBody.replace(a, sizeof(u8"{HOST}") - 1, connectedSite.toString());
		}

		const char* msgData = (const char*)(packet.onSend(a));

		requestBody.append(msgData, a);

		status = sock.send(requestBody.getCharArray(), requestBody.getSize());
	}

	return status;
}

template<class SocketType>
GLOWE::HTTPResponse GLOWE::HTTPBase<SocketType>::receiveResponse()
{
	Packet garbage;
	return receiveResponse(garbage);
}

template<class SocketType>
GLOWE::HTTPResponse GLOWE::HTTPBase<SocketType>::receiveResponse(Packet& packet)
{
	HTTPResponse result;

	packet.reset();
	String buff, clrfToFind = "\r\n\r\n";
	buff.reserve(2048);
	char stackBuff[2048];

	std::size_t receivedBytes = 0;

	auto parseHeader = [&]() -> bool
	{
		std::size_t findPos = buff.find(clrfToFind);
		if (findPos == String::invalidPos)
		{
			return false;
		}

		String headerContents = buff.getSubstring(0, findPos);
		buff.erase(0, findPos + 4);

		IStringStream str(headerContents);

		str >> result.httpVersion >> result.code;
		str.ignore();
		std::getline(str, result.codeDesc.getString(), '\r');

		while ((str.ignore(), std::getline(str, headerContents.getString(), '\r'))) // !headerContents.isEmpty()
		{
			const std::size_t colonFind = headerContents.find(':');
			const std::size_t whitespaceFind = headerContents.find(' ');

			result.headerValues.insert(std::make_pair(headerContents.getSubstring(0, colonFind), headerContents.getSubstring(whitespaceFind + 1)));
		}

		return true;
	};

	sock.setBlocking(false);

	Socket::Status stat;
	do
	{
		stat = sock.receive(stackBuff, sizeof(stackBuff), receivedBytes);

		if (stat == Socket::Status::Disconnected || stat == Socket::Status::Error)
		{
			return result;
		}

		buff.append(stackBuff, receivedBytes);
	} while (!parseHeader());

	const auto foundEncoding = result.headerValues.find("Transfer-Encoding");
	String contentEncod = "identity";
	if (foundEncoding != result.headerValues.end())
	{
		contentEncod = foundEncoding->second;
	}

	if (contentEncod.find("identity") != String::invalidPos)
	{
		const auto foundContentLen = result.headerValues.find("Content-Length");
		if (foundContentLen == result.headerValues.end())
		{
			sock.disconnect();
			return result;
		}
		std::size_t contentLength = foundContentLen->second.toUInt();
		{
			const auto bytesAlreadyInBuffer = std::min(contentLength, buff.getSize());
			packet.append(buff.getCharArray(), bytesAlreadyInBuffer);
			buff.erase(0, bytesAlreadyInBuffer);
			contentLength -= bytesAlreadyInBuffer;
		}

		while (contentLength && (stat = sock.receive(stackBuff, sizeof(stackBuff), receivedBytes)) != Socket::Status::Error && stat != Socket::Status::Disconnected)
		{
			contentLength -= std::min(receivedBytes, contentLength);
			packet.append(stackBuff, receivedBytes);
		}
	}
	else
	if (contentEncod.find("chunked") != String::invalidPos)
	{
		bool isEnd = false;
		std::size_t toRead = 0;
		const std::locale& loc = getInstance<Locale>().getLocale();
		String numberBuff;
		numberBuff.reserve(9);

		while (!isEnd)
		{
			while ((stat = sock.receive(stackBuff, sizeof(stackBuff), receivedBytes)) != Socket::Status::Error && stat != Socket::Status::Disconnected && receivedBytes)
			{
				buff.append(stackBuff, receivedBytes);
			}

			// Parse chunk(s).
			for (std::size_t read = 0; read < buff.getSize(); ++read)
			{
				if (toRead > 0)
				{
					const std::size_t temp = std::min(buff.getSize() - read, toRead);

					packet.append(buff.getCharArray() + read, temp);
					read += temp;
					toRead -= temp;
				}
				else
				{
					if (std::isdigit(buff[read], loc) || (buff[read] >= 'a' && buff[read] <= 'f') || (buff[read] >= 'A' && buff[read] <= 'F'))
					{
						numberBuff.append(buff[read]);
					}
					else
					{
						++read;
						toRead = numberBuff.toUInt(16);
						numberBuff.clear();
						if (toRead == 0)
						{
							// End of chunked transmission.
							isEnd = true;
							break;
						}
					}
				}
			}

			buff.clear();
		}
	}
	else
	{
		sock.disconnect();
		return result;
	}

	sock.setBlocking(true);

	return result;
}

template<class SocketType>
bool GLOWE::HTTPBase<SocketType>::checkIsConnected() const
{
	return isConnected;
}

template<class SocketType>
void GLOWE::HTTPBase<SocketType>::setBlocking(const bool arg)
{
	sock.setBlocking(arg);
}

template<class SocketType>
SocketType& GLOWE::HTTPBase<SocketType>::getSocket()
{
	return sock;
}

#endif
