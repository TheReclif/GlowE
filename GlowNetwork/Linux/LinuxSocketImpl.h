#pragma once
#ifndef GLOWE_LINUX_SOCKETIMPL_INCLUDED
#define GLOWE_LINUX_SOCKETIMPL_INCLUDED

#include "../../GlowSystem/Utility.h"

#include "../Uniform/SocketHandle.h"
#include "../Uniform/SocketUtility.h"

#ifdef GLOWE_COMPILE_FOR_LINUX
namespace GLOWE
{
	namespace Linux
	{
		class GLOWNETWORK_EXPORT LinuxSocketImpl
		{
		public:
			static void createAddress(const Ip& ip, const unsigned short port, sockaddr_in6* const addrOut);
			static SocketHandle invalidSocket();
			static void closeSocket(SocketHandle socket);
			static void setBlocking(SocketHandle socket, const bool& block);
			static Socket::Status getErrorCode();
		};
	}
}
#endif

#endif
