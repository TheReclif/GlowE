#include "LinuxSSLCertUtility.h"

#include "../../GlowSystem/Filesystem.h"

#ifdef GLOWE_COMPILE_FOR_LINUX
GLOWE::Linux::SSLCertUtil::SSLCertUtil()
	: certBlob()
{
	// TODO: Check if the listing works.
	FileListing listing("/etc/ssl/cert/*");

	for (const auto& path : listing)
	{
		File file;
		file.open(path, std::ios::in);

		if (file.checkIsOpen())
		{
			const auto fileSize = file.getFileSize();
			const auto blobSize = certBlob.getSize();
			certBlob.resize(blobSize + fileSize + 1);
			file.read(certBlob.getCharArray() + blobSize, fileSize);
		}
	}
}

const GLOWE::String& GLOWE::Linux::SSLCertUtil::getCerts() const
{
	return certBlob;
}

bool GLOWE::Linux::SSLCertUtil::checkIsValid() const
{
	return !certBlob.isEmpty();
}
#endif
