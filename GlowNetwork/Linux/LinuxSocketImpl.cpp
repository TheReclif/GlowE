#include "LinuxSocketImpl.h"

#ifdef GLOWE_COMPILE_FOR_LINUX
GLOWE::String GLOWE::Socket::getErrorDesc()
{
	static std::mutex mutex;
	std::lock_guard<std::mutex> guard(mutex);
	return std::strerror(errno);
}

void GLOWE::Linux::LinuxSocketImpl::createAddress(const Ip& ip, const unsigned short port, sockaddr_in6* const addrOut)
{
	std::memset(addrOut, 0, sizeof(sockaddr_in6));
	switch (ip.getProto())
	{
	case Ip::IpProto::IPv4:
	{
		sockaddr_in& result = *reinterpret_cast<sockaddr_in*>(addrOut);
		result.sin_family = AF_INET;
		result.sin_addr.s_addr = ip.toUInt32(false);
		result.sin_port = htons(port);
	}
	break;
	case Ip::IpProto::IPv6:
	{
		addrOut->sin6_family = AF_INET6;
		addrOut->sin6_scope_id = ip.getScopeId(false);
		addrOut->sin6_port = htons(port);
		const auto addr = ip.toByteArrayPtr(false);
		std::memcpy(addrOut->sin6_addr.s6_addr, addr.data(), 16);
	}
	break;
	}
}

GLOWE::SocketHandle GLOWE::Linux::LinuxSocketImpl::invalidSocket()
{
	return -1;
}

void GLOWE::Linux::LinuxSocketImpl::closeSocket(SocketHandle socket)
{
	close(socket);
}

void GLOWE::Linux::LinuxSocketImpl::setBlocking(SocketHandle socket, const bool& block)
{
	u_long arg = block ? 0 : 1;
	if (ioctl(socket, FIONBIO, (char*)&arg) != 0)
	{
		WarningThrow(false, "Unable to change socket's blocking mode.");
	}
}

GLOWE::Socket::Status GLOWE::Linux::LinuxSocketImpl::getErrorCode()
{
	switch (errno)
	{
	case EALREADY:
	case EAGAIN:
	case EINPROGRESS:
		return Socket::Status::NotReady;
	case ECONNABORTED:
	case ECONNRESET:
	case ETIMEDOUT:
	case ENETRESET:
	case ENOTCONN:
		return Socket::Status::Disconnected;
	case EISCONN:
	case 0:
		return Socket::Status::Done;
	default:
		return Socket::Status::Error;
	}
}

#endif
