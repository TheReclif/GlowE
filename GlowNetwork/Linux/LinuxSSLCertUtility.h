#pragma once
#ifndef GLOWE_LINUX_SSLCERTUTIL_INCLUDED
#define GLOWE_LINUX_SSLCERTUTIL_INCLUDED

#include "../Uniform/SocketUtility.h"

#ifdef GLOWE_COMPILE_FOR_LINUX
namespace GLOWE
{
	namespace Linux
	{
		class GLOWNETWORK_EXPORT SSLCertUtil
			: public Subsystem
		{
		private:
			String certBlob;
		public:
			SSLCertUtil();
			virtual ~SSLCertUtil() = default;

			const String& getCerts() const;

			bool checkIsValid() const; // Checks if the SSL/TLS services are available.
		};
	}
}
#endif

#endif
