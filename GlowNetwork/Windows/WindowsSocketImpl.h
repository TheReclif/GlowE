#pragma once
#ifndef GLOWE_NETWORK_WINDOWSSOCKETINIT_INCLUDED
#define GLOWE_NETWORK_WINDOWSSOCKETINIT_INCLUDED

#include "../../GlowSystem/Utility.h"

#include "../Uniform/SocketHandle.h"
#include "../Uniform/SocketUtility.h"

#ifdef GLOWE_COMPILE_FOR_WINDOWS
namespace GLOWE
{
	namespace Windows
	{
		class GLOWNETWORK_EXPORT WindowsSocketImpl
		{
		public:
			static void createAddress(const Ip& ip, const unsigned short port, sockaddr_in6* const addrOut);
			static SocketHandle invalidSocket();
			static void closeSocket(SocketHandle socket);
			static void setBlocking(SocketHandle socket, const bool block);
			static Socket::Status getErrorCode();
		};

		class GLOWNETWORK_EXPORT WindowsSocketInitializer final
		{
		public:
			WindowsSocketInitializer();
			~WindowsSocketInitializer();
		};

		extern WindowsSocketInitializer __socketsInit;
	}
}
#endif

#endif
