#include "WindowsSocketImpl.h"

#ifdef GLOWE_COMPILE_FOR_WINDOWS
GLOWE::Windows::WindowsSocketInitializer __socketsInit;

GLOWE::String GLOWE::Socket::getErrorDesc()
{
	const auto err = WSAGetLastError();
	wchar_t tempArr[2048] = L"";
	FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, nullptr, err, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), tempArr, sizeof(tempArr) / sizeof(wchar_t), nullptr);
	return tempArr;
}

GLOWE::Windows::WindowsSocketInitializer::WindowsSocketInitializer()
{
	WSADATA tempData;
	WSAStartup(MAKEWORD(2, 2), &tempData);
}

GLOWE::Windows::WindowsSocketInitializer::~WindowsSocketInitializer()
{
	WSACleanup();
}

void GLOWE::Windows::WindowsSocketImpl::createAddress(const Ip& ip, const unsigned short port, sockaddr_in6* const addrOut)
{
	std::memset(addrOut, 0, sizeof(sockaddr_in6));
	switch (ip.getProto())
	{
	case Ip::IpProto::IPv4:
	{
		sockaddr_in& result = *reinterpret_cast<sockaddr_in*>(addrOut);
		result.sin_family = AF_INET;
		result.sin_addr.s_addr = ip.toUInt32(false);
		result.sin_port = htons(port);
	}
	break;
	case Ip::IpProto::IPv6:
	{
		addrOut->sin6_family = AF_INET6;
		addrOut->sin6_scope_id = ip.getScopeId(false);
		addrOut->sin6_port = htons(port);
		const auto addr = ip.toByteArrayPtr(false);
		std::memcpy(addrOut->sin6_addr.s6_addr, addr.data(), 16);
	}
	break;
	}
}

GLOWE::SocketHandle GLOWE::Windows::WindowsSocketImpl::invalidSocket()
{
	return INVALID_SOCKET;
}

void GLOWE::Windows::WindowsSocketImpl::closeSocket(SocketHandle socket)
{
	closesocket(socket);
}

void GLOWE::Windows::WindowsSocketImpl::setBlocking(SocketHandle socket, const bool block)
{
	u_long arg = block ? 0 : 1;
	ioctlsocket(socket, FIONBIO, &arg);
}

GLOWE::Socket::Status GLOWE::Windows::WindowsSocketImpl::getErrorCode()
{
	switch (WSAGetLastError())
	{
	case WSAEWOULDBLOCK:
	case WSAEALREADY:
		return Socket::Status::NotReady;
	case WSAECONNABORTED:
	case WSAECONNRESET:
	case WSAETIMEDOUT:
	case WSAENETRESET:
	case WSAENOTCONN:
		return Socket::Status::Disconnected;
	case WSAEISCONN:
		return Socket::Status::Done;
	default:
		return Socket::Status::Error;
	}
}

#endif
