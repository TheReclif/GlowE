#include "WindowsSSLCertUtility.h"
#include "..\..\GlowSystem\Error.h"

#include <wincrypt.h>

GLOWE::Windows::SSLCertUtil::SSLCertUtil()
{
	String base64Encoded;
	PCCERT_CONTEXT info = nullptr;
	unsigned int certsLoaded = 0;

	HCERTSTORE systemStore = CertOpenSystemStoreW(NULL, L"CA");
	if (!systemStore)
	{
		WarningThrow(false, "Unable to open the CA cert store. SSL/TLS services may be unavailable.");
		return;
	}
	else
	{
		while (info = CertEnumCertificatesInStore(systemStore, info))
		{
			if (info->dwCertEncodingType & X509_ASN_ENCODING) // PKCS_7_ASN_ENCODING
			{
				DWORD size = 0;
				if (CryptBinaryToStringA(info->pbCertEncoded, info->cbCertEncoded, CRYPT_STRING_BASE64HEADER, nullptr, &size) && size > 0)
				{
					base64Encoded.resize(size);
					CryptBinaryToStringA(info->pbCertEncoded, info->cbCertEncoded, CRYPT_STRING_BASE64HEADER, base64Encoded.getCharArray(), &size);
					base64Encoded.resize(size - 1);
					certsBlob += base64Encoded;
					++certsLoaded;
				}
				else
				{
					WarningThrow(false, "Unable to convert one of the certificates to the PEM format. Some of the SSL/TLS connections may fail.");
				}
			}
			else
			{
				WarningThrow(false, "One of the processed certificates is not encoded in X.509 encoding, thus it won't be loaded. Some of the SSL/TLS connections may fail.");
			}
		}

		CertCloseStore(systemStore, CERT_CLOSE_STORE_CHECK_FLAG);
	}

	systemStore = CertOpenSystemStoreW(NULL, L"Root");
	if (!systemStore)
	{
		WarningThrow(false, "Unable to open the Root cert store. SSL/TLS services may be unavailable.");
	}
	else
	{
		while (info = CertEnumCertificatesInStore(systemStore, info))
		{
			if (info->dwCertEncodingType & X509_ASN_ENCODING) // PKCS_7_ASN_ENCODING
			{
				DWORD size = 0;
				if (CryptBinaryToStringA(info->pbCertEncoded, info->cbCertEncoded, CRYPT_STRING_BASE64HEADER, nullptr, &size) && size > 0)
				{
					base64Encoded.resize(size);
					CryptBinaryToStringA(info->pbCertEncoded, info->cbCertEncoded, CRYPT_STRING_BASE64HEADER, base64Encoded.getCharArray(), &size);
					base64Encoded.resize(size - 1);
					certsBlob += base64Encoded;
					++certsLoaded;
				}
				else
				{
					WarningThrow(false, "Unable to convert one of the certificates to the PEM format. Some of the SSL/TLS connections may fail.");
				}
			}
			else
			{
				WarningThrow(false, "One of the processed certificates is not encoded in X.509 encoding, thus it won't be loaded. Some of the SSL/TLS connections may fail.");
			}
		}

		CertCloseStore(systemStore, CERT_CLOSE_STORE_CHECK_FLAG);
	}

	if (certsLoaded > 0)
	{
		logMessage("SSLCertUtil initialised. SSL/TLS services available.");
		logMessage("SSLCertUtil loaded " + toString(certsLoaded) + " certificates.");
	}
	else
	{
		WarningThrow(false, "SSLCertUtil was unable to load any certificates. SSL/TLS services unavailable.")
	}
}

const GLOWE::String& GLOWE::Windows::SSLCertUtil::getCerts() const
{
	return certsBlob;
}

bool GLOWE::Windows::SSLCertUtil::checkIsValid() const
{
	return !certsBlob.isEmpty();
}
