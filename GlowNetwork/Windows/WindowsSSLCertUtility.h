#pragma once
#ifndef GLOWE_NETWORK_WINDOWS_SSLCERTUTIL_INCLUDED
#define GLOWE_NETWORK_WINDOWS_SSLCERTUTIL_INCLUDED

#include "glownetwork_export.h"

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/String.h"

#ifdef GLOWE_COMPILE_FOR_WINDOWS
namespace GLOWE
{
	namespace Windows
	{
		class GLOWNETWORK_EXPORT SSLCertUtil
			: public Subsystem
		{
		private:
			String certsBlob;
		public:
			SSLCertUtil();
			virtual ~SSLCertUtil() = default;

			const String& getCerts() const;

			bool checkIsValid() const; // Checks if the SSL/TLS services are available.
		};
	}
}
#endif

#endif
