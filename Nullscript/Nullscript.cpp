#include "Nullscript.h"

namespace NS
{
	void Interpreter::LexicalRule::apply(GLOWE::Vector<GLOWE::UniquePtr<RawToken> >& tokens)
	{
		size_t off;

		GLOWE::RegexMatch res;
		GLOWE::Vector<GLOWE::UniquePtr<RawToken> >tmp_raws;

		GLOWE::Vector<GLOWE::UniquePtr<RawToken> >::iterator ss_pos;
		int last_i;

		last_i = -1;
		if (static_cast<bool>(create))
		{
			ss_pos = std::find_if(
				tokens.begin(),
				tokens.end(),
				[this, &last_i, &tokens](const GLOWE::UniquePtr<RawToken> &a)
			{
				return a && a->getType() == typeid(RawString) && (&a - tokens.data() > last_i);
			}
			);
			last_i = ss_pos - tokens.begin();
			//use rule, apply in every RawString that is remaining
			while (ss_pos != tokens.end())
			{
				RawString *tmp1 = reinterpret_cast<RawString*>(ss_pos->get());

				if (std::regex_search(tmp1->str.getString(), res, regex_rule) && res[0].length())
				{
					//match regex rule

					GLOWE::UniquePtr<RawToken>tmp_token = std::move(*ss_pos);
					RawString &tmp = *reinterpret_cast<RawString*>(tmp_token.get());
					off = ss_pos - tokens.begin();

					tmp_raws = std::move(tmp.split(res.position(0), res.position(0) + res.length()));

					//push matched case
					ss_pos->reset(create(
						tmp.str.getSubstring(res.position(0), res.length()))
					);

					//insert first part of unmatched string if exists
					if (res.position(0))
					{
						tokens.insert(tokens.begin() + off, std::move(tmp_raws.front()));
						++off;
					}
					//insert second part of unmatched string if exists
					//if ((res.position(0) != 0) < tmp_raws.size())
					if (tmp_raws.size() > 1 || (tmp_raws.size() == 1 && res.position(0) == 0))
					{
						tokens.insert(tokens.begin() + off + 1, std::move(tmp_raws.back()));
					}
				}
				else
				{
					//match key rules
					size_t f = 0;
					for (const auto& j : key_rule)
					{
						f = tmp1->str.find(j);
						if (f != tmp1->str.invalidPos)
						{

							GLOWE::UniquePtr<RawToken>tmp_token = std::move(*ss_pos);
							RawString &tmp = *reinterpret_cast<RawString*>(tmp_token.get());
							off = std::distance(tokens.begin(), ss_pos);


							tmp_raws = std::move(tmp.split(f, f + j.getSize()));

							tokens[off] = std::move(
								GLOWE::UniquePtr<RawToken>(
									create(
										tmp.str.getSubstring(f, j.getSize()))
									)
							);

							if (res.position(0))
							{
								tokens.insert(tokens.begin() + off, std::move(tmp_raws.front()));
								++off;
							}

							if (res.position(0) + res.length() < (signed)tmp.str.getSize())
							{
								tokens.insert(tokens.begin() + off + 1, std::move(tmp_raws.back()));
							}

							break;
						}
					}
				}
				tmp_raws.clear();
				ss_pos = std::find_if(
					tokens.begin(),
					tokens.end(),
					[this, &last_i, &tokens](const GLOWE::UniquePtr<RawToken> &a)
				{
					return a && a->getType() == typeid(RawString) && (&a - tokens.data() > last_i);
				}
				);
				last_i = ss_pos - tokens.begin();
			}
		}
	}

	std::type_index Interpreter::LexicalRule::getType() const noexcept
	{
		return typeid(LexicalRule);
	}

	Interpreter::LexicalRule::LexicalRule(const GLOWE::Regex& reg_val, const std::function<RawToken*(const GLOWE::String&)>& func)
	{
		regex_rule = reg_val;
		create = func;
	}
	Interpreter::LexicalRule::LexicalRule(const GLOWE::Vector<GLOWE::String>& key_val, const std::function<RawToken*(const GLOWE::String&)>& func)
	{
		key_rule = key_val;
		create = func;
	}
	Interpreter::LexicalRule::LexicalRule(const GLOWE::Regex& reg_val, const GLOWE::Vector<GLOWE::String>& key_val, const std::function<RawToken*(const GLOWE::String&)>& func)
	{
		regex_rule = reg_val;
		key_rule = key_val;
		create = func;
	}


	void Interpreter::FormatRule::apply(GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&tokens)
	{
		typedef GLOWE::Vector<GLOWE::UniquePtr<RawToken> > tokensVec;
		tokensVec sub;
		auto ss_pos = std::find_if(
			tokens.begin(),
			tokens.end(),
			[this](const GLOWE::UniquePtr<RawToken>& a)
		{
			// Check
			if (!a)
				return false;

			for (unsigned i = 0; i < key_rule.size(); ++i)
				if (!a->comp(key_rule[i].get()))
					return false;
			return true;
		});
		while (ss_pos != tokens.end())
		{
			unsigned off = ss_pos - tokens.begin();

			sub.clear();

			std::move(ss_pos,
				std::next(ss_pos, key_rule.size()),
				std::back_inserter(sub));

			tokens.erase(ss_pos,
				std::next(ss_pos, key_rule.size()));

			sub = std::move(exec(sub));

			tokens.insert(std::next(tokens.cbegin(), off),
				std::move_iterator<tokensVec::iterator>(sub.begin()),
				std::move_iterator<tokensVec::iterator>(sub.end()));

			ss_pos = std::find_if(
				tokens.begin(),
				tokens.end(),
				[this](const GLOWE::UniquePtr<RawToken>& a)
			{
				// Check
				if (!a)
					return false;

				for (unsigned i = 0; i < key_rule.size(); ++i)
					if (!a->comp(key_rule[i].get()))
						return false;
				return true;
			});
		}
	}

	std::type_index Interpreter::FormatRule::getType() const noexcept
	{
		return typeid(FormatRule);
	}

	void Interpreter::ComplexRule::apply(GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&tokens)
	{
		if (static_cast<bool>(exec))
			exec(tokens);
	}

	std::type_index Interpreter::ComplexRule::getType() const noexcept
	{
		return typeid(ComplexRule);
	}

	Interpreter::ComplexRule::ComplexRule(const std::function<void(GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&)>& func)
	{
		exec = func;
	}

	GLOWE::Pair<size_t, GLOWE::UniquePtr<ParseStructEntity> > Interpreter::ComplexPrecompileRule::apply(Interpreter& in, const GLOWE::Vector<GLOWE::UniquePtr<RawToken> >& source, size_t off)
	{
		return std::move(exec(in, source, off));
	}
	bool Interpreter::ComplexPrecompileRule::match(const GLOWE::Vector<GLOWE::UniquePtr<RawToken> >& source, size_t off)
	{
		return search(source, off);
	}


	Interpreter::ComplexPrecompileRule::ComplexPrecompileRule(const apply_func& ex, const match_func& sc)
	{
		exec = ex;
		search = sc;
	}

	void Interpreter::add_rule(const GLOWE::String& n, const Rule& rule)
	{
		rules.push_back(std::make_pair(n, GLOWE::SharedPtr<Rule>(const_cast<Rule*>(&rule), [](Rule*) {})));
	}
	void Interpreter::add_rule(const GLOWE::String& n, Rule* rule)
	{
		rules.push_back(std::make_pair(n, GLOWE::SharedPtr<Rule>(const_cast<Rule*>(rule))));
	}

	void Interpreter::add_precompileRule(const GLOWE::String& n, const PrecompileRule& rule)
	{
		precompileRules.push_back(std::make_pair(n, GLOWE::SharedPtr<PrecompileRule>(const_cast<PrecompileRule*>(&rule), [](PrecompileRule*) {})));
	}
	void Interpreter::add_precompileRule(const GLOWE::String& n, PrecompileRule* rule)
	{
		precompileRules.push_back(std::make_pair(n, GLOWE::SharedPtr<PrecompileRule>(const_cast<PrecompileRule*>(rule))));
	}

	void Interpreter::add_compileRule(const GLOWE::String& n, const CompileRule& rule)
	{
		compileRules.push_back(std::make_pair(n, GLOWE::SharedPtr<CompileRule>(const_cast<CompileRule*>(&rule), [](CompileRule*) {})));
	}
	void Interpreter::add_compileRule(const GLOWE::String& n, CompileRule* rule)
	{
		compileRules.push_back(std::make_pair(n, GLOWE::SharedPtr<CompileRule>(const_cast<CompileRule*>(rule))));
	}

	void CompiledScript::run()
	{
		//
	}

	CompiledScript::CompiledScript(GLOWE::Vector<GLOWE::UniquePtr<CompiledToken> >&& tok)
	{
		tokens = std::move(tok);
	}

	CompiledScript::CompiledScript(CompiledScript&& arg) noexcept
		: tokens(std::move(arg.tokens))
	{
	}

	GLOWE::Vector<GLOWE::UniquePtr<RawToken> > Interpreter::pre_tokenize(const GLOWE::String& source)
	{
		GLOWE::Vector<GLOWE::UniquePtr<RawToken> >raw_tokens;

		raw_tokens.emplace_back(new GLOWE::Deletable<RawString>(source));

		for (const auto& i : rules)
		{
			i.second->apply(raw_tokens);
		}
		return std::move(raw_tokens);
	}

	GLOWE::Vector<GLOWE::UniquePtr<ParseStructEntity> >Interpreter::precompile(const GLOWE::Vector<GLOWE::UniquePtr<RawToken> >& source, size_t off)
	{
		GLOWE::Vector<GLOWE::UniquePtr<ParseStructEntity> > ret;

		GLOWE::Pair<size_t, GLOWE::UniquePtr<ParseStructEntity> >tmpp;

		bool matched = true;
		while (matched)
		{
			matched = false;
			for (const auto& rule : precompileRules)
			{
				if (rule.second->match(source, off))
				{
					matched = true;

					tmpp = std::move(rule.second->apply(*this, source, off));
					ret.push_back(std::move(tmpp.second));
					off += tmpp.first;
					break;
				}
			}
		}
		return std::move(ret);
	}

	CompiledScript Interpreter::compile(const GLOWE::Vector<GLOWE::UniquePtr<RawToken> >& tokens)
	{
		GLOWE::Vector<GLOWE::UniquePtr<CompiledToken> >c_tokens;

		return std::move(c_tokens);
	}

	CompiledScript Interpreter::compile(const GLOWE::String& source)
	{
		return CompiledScript(compile(pre_tokenize(source)));
	}
}

NS::Interpreter::Interpreter(const rules_type& _rules, const precompile_rules_type& _precompile_rules, const compile_rules_type& _compile_rules)
{
	rules = _rules;
	precompileRules = _precompile_rules;
	compileRules = _compile_rules;
}
