#include "ntokens.h"

namespace NS
{
    bool RawToken::comp(const RawToken* a) const noexcept
    {
        return a->getType() == getType();
    }

    bool RawToken::comp(RawToken* a) const noexcept
    {
        return a->getType() == getType();
    }

	std::type_index RawString::getType() const noexcept
    {
        return typeid(RawString);
    }
    RawToken* RawString::copy()
    {
		return new GLOWE::Deletable<RawString>(str);
    }

    GLOWE::Vector<GLOWE::UniquePtr<RawToken> >RawString::split(const size_t pos1,const size_t pos2)
    {
        GLOWE::Vector<GLOWE::UniquePtr<RawToken>> ret;
        if (pos1 != 0)
        {
            ret.emplace_back(GLOWE::makeUniquePtr<RawString>(str.getSubstring(0, pos1)));
        }
        if (pos2 < str.getSize())
        {
			ret.emplace_back(GLOWE::makeUniquePtr<RawString>(str.getSubstring(pos2, str.getSize() - pos2)));
        }
        return ret;
    }

    RawString::RawString(const GLOWE::String& name)
    {
        str = name;
    }


	std::type_index RawName::getType() const noexcept
    {
        return typeid(RawName);
    }

    RawToken* RawName::copy()
    {
		return new GLOWE::Deletable<RawName>(name);
    }

    RawName::RawName(const GLOWE::String& str)
    {
        name = str;
    }

	std::type_index RawValue::getType() const noexcept
    {
        return typeid(RawValue);
    }
	std::type_index RawValNumber::getType() const noexcept
    {
        return typeid(RawValNumber);
    }
    RawToken* RawValNumber::copy()
    {
		return new GLOWE::Deletable<RawValNumber>(val);
    }

    RawValNumber::RawValNumber()
    {
        val = 0;
    }
    RawValNumber::RawValNumber(const double as)
    {
        val = as;
    }
    RawValNumber::RawValNumber(const GLOWE::String& a)
    {
        val = a.toDouble();
    }

	std::type_index RawValString::getType() const noexcept
    {
        return typeid(RawValString);
    }
    RawToken* RawValString::copy()
    {
		return new GLOWE::Deletable<RawValString>(val);
    }

    RawValString::RawValString(const GLOWE::String& str)
    {
        val = str;
    }

	std::type_index RawOperator::getType() const noexcept
    {
        return typeid(RawOperator);
    }
    RawToken* RawOperator::copy()
    {
		return new GLOWE::Deletable<RawOperator>(op);
    }

    RawOperator::RawOperator(const GLOWE::String& a)
    {
        op = a;
    }

	std::type_index RawSeparator::getType() const noexcept
    {
        return typeid(RawSeparator);
    }
    RawToken* RawSeparator::copy()
    {
		return new GLOWE::Deletable<RawSeparator>(br);
    }

    RawSeparator::RawSeparator()
    {
        br = false;
    }

    RawSeparator::RawSeparator(const bool a)
    {
        br = a;
    }

	std::type_index RawScopePart::getType() const noexcept
    {
        return typeid(RawScopePart);
    }
    RawToken* RawScopePart::copy()
    {
        RawScopePart* a = new GLOWE::Deletable<RawScopePart>();
        a->state=state;
        a->type=type;
        return a;
    }

    RawScopePart::RawScopePart()
    {
        state = 0;
        type = 0;
    }

    RawScopePart::RawScopePart(const char& c)
    {
        type = 0;
        state = 0;
        switch (c)
        {
            case '{':
            {
                type = 0;
                state = 0;
                break;
            }
            case '}':
            {
                type = 0;
                state = 1;
                break;
            }
            case '[':
            {
                type = 1;
                state = 0;
                break;
            }
            case ']':
            {
                type = 1;
                state = 1;
                break;
            }
            case '(':
            {
                type = 2;
                state = 0;
                break;
            }
            case ')':
            {
                type = 2;
                state = 1;
                break;
            }

        }
    }

	std::type_index RawScope::getType() const noexcept
    {
        return typeid(RawScope);
    }
    RawToken* RawScope::copy()
    {
        RawScope* a = new GLOWE::Deletable<RawScope>();
        for(const auto& i:tokens)
        {
            a->tokens.emplace_back(i->copy());
        }
        return a;
    }

    RawScope::RawScope()
    {
        type = 0;
    }

    RawScope::RawScope(GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&& t)
    {
        tokens = std::move(t);
    }

	std::type_index FunctionToken::getType() const noexcept
    {
        return typeid(FunctionToken);
    }
    void FunctionToken::exec(Scope& s,size_t& p)
    {
        func(s);
    }
	std::type_index JumpToken::getType() const noexcept
    {
        return typeid(JumpToken);
    }
    void JumpToken::exec(Scope& s,size_t& p)
    {
        p+=jmp;
    }
}
