#pragma once
#ifndef NULLSCRIPT_H
#define NULLSCRIPT_H

#include "ntokens.h"

namespace NS
{
	class GLOWSYSTEM_EXPORT CompiledScript
		: public GLOWE::NoCopy
	{
	protected:
		GLOWE::Vector<GLOWE::UniquePtr<CompiledToken>> tokens;
	public:
		virtual void run();

		CompiledScript(GLOWE::Vector<GLOWE::UniquePtr<CompiledToken>>&&);

		CompiledScript() = default;
		CompiledScript(CompiledScript&& arg) noexcept;

		virtual ~CompiledScript() = default;
	};

	class GLOWSYSTEM_EXPORT Interpreter
		: public GLOWE::NoCopy
	{
	public:
		class GLOWSYSTEM_EXPORT Rule
		{
		public:
			virtual void apply(GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&) = 0;
			virtual std::type_index getType() const noexcept = 0;

			virtual ~Rule() = default;
		};
		class GLOWSYSTEM_EXPORT LexicalRule
			: public Rule
		{
		public:
			GLOWE::Regex regex_rule;
			GLOWE::Vector<GLOWE::String> key_rule;

			std::function<RawToken*(const GLOWE::String&)> create;

			virtual void apply(GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&);
			virtual std::type_index getType() const noexcept;

			LexicalRule() = default;

			LexicalRule(const GLOWE::Regex&, const std::function<RawToken*(const GLOWE::String&)>&);
			LexicalRule(const GLOWE::Vector<GLOWE::String>&, const std::function<RawToken*(const GLOWE::String&)>&);
			LexicalRule(const GLOWE::Regex&, const GLOWE::Vector<GLOWE::String>&, const std::function<RawToken*(const GLOWE::String&)>&);

			LexicalRule(const LexicalRule&) = default;
			LexicalRule(LexicalRule&&) noexcept = default;

			virtual ~LexicalRule() = default;
		};
		class GLOWSYSTEM_EXPORT FormatRule
			: public Rule, public GLOWE::NoCopy
		{
		public:
			GLOWE::Vector<GLOWE::UniquePtr<RawToken> > key_rule;
			std::function<GLOWE::Vector<GLOWE::UniquePtr<RawToken> >(const GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&)> exec;

			virtual void apply(GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&);
			virtual std::type_index getType() const noexcept;

			FormatRule() = default;

			FormatRule(const std::function<GLOWE::Vector<GLOWE::UniquePtr<RawToken> >(const GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&)>&);

			FormatRule(FormatRule&&) noexcept = default;

			virtual ~FormatRule() = default;
		};
		class GLOWSYSTEM_EXPORT ComplexRule
			: public Rule
		{
		public:
			std::function<void(GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&)> exec;

			virtual void apply(GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&);
			virtual std::type_index getType() const noexcept;

			ComplexRule() = default;

			ComplexRule(const std::function<void(GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&)>&);

			ComplexRule(const ComplexRule&) = default;
			ComplexRule(ComplexRule&&) noexcept = default;

			virtual ~ComplexRule() = default;
		};

		class GLOWSYSTEM_EXPORT PrecompileRule
		{
		public:
			virtual GLOWE::Pair<size_t, GLOWE::UniquePtr<ParseStructEntity> > apply(Interpreter&, const GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&, size_t) = 0;
			virtual bool match(const GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&, size_t) = 0;
			virtual std::type_index getType() const noexcept = 0;

			virtual ~PrecompileRule() = default;
		};

		class GLOWSYSTEM_EXPORT ComplexPrecompileRule
			: public PrecompileRule
		{
		public:
			typedef std::function<GLOWE::Pair<size_t, GLOWE::UniquePtr<ParseStructEntity> >(Interpreter&, const GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&, size_t)> apply_func;
			typedef std::function<bool(const GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&, size_t)> match_func;

			apply_func exec;
			match_func search;

			virtual GLOWE::Pair<size_t, GLOWE::UniquePtr<ParseStructEntity> > apply(Interpreter&, const GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&, size_t);
			virtual bool match(const GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&, size_t);

			ComplexPrecompileRule() = default;
			ComplexPrecompileRule(const ComplexPrecompileRule&) = default;
			ComplexPrecompileRule(ComplexPrecompileRule&&) noexcept = default;

			ComplexPrecompileRule(const apply_func&, const match_func&);

			virtual ~ComplexPrecompileRule() = default;
		};

		class GLOWSYSTEM_EXPORT CompileRule
		{
		public:
			virtual GLOWE::Vector<GLOWE::SharedPtr<CompiledToken> > apply(GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&) = 0;
			virtual std::type_index getType() const noexcept = 0;

			virtual ~CompileRule() = default;
		};

		/*
		class GLOWSYSTEM_EXPORT FormatCompileRule
			: public CompileRule, public GLOWE::NoCopy
		{
		public:
			GLOWE::Vector<GLOWE::UniquePtr<RawToken> >key_rule;

			std::function<GLOWE::Vector<GLOWE::SharedPtr<CompiledToken> >(GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&, GLOWE::Vector<GLOWE::UniquePtr<RawToken> >::iterator) >func;


			virtual GLOWE::Vector<GLOWE::SharedPtr<CompiledToken> > apply(GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&);
			virtual std::type_index getType() const noexcept;

			virtual ~FormatCompileRule() = default;
		};
		*/

		typedef GLOWE::Vector<GLOWE::Pair<GLOWE::String, GLOWE::SharedPtr<Rule> > > rules_type;
		typedef GLOWE::Vector<GLOWE::Pair<GLOWE::String, GLOWE::SharedPtr<PrecompileRule> > > precompile_rules_type;
		typedef GLOWE::Vector<GLOWE::Pair<GLOWE::String, GLOWE::SharedPtr<CompileRule> > > compile_rules_type;

		rules_type rules;
		precompile_rules_type precompileRules;
		compile_rules_type compileRules;

		void add_rule(const GLOWE::String&, const Rule&);
		void add_rule(const GLOWE::String&, Rule*);

		void add_precompileRule(const GLOWE::String&, const PrecompileRule&);
		void add_precompileRule(const GLOWE::String&, PrecompileRule*);

		void add_compileRule(const GLOWE::String&, const CompileRule&);
		void add_compileRule(const GLOWE::String&, CompileRule*);


		GLOWE::Vector<GLOWE::UniquePtr<RawToken> > pre_tokenize(const GLOWE::String&);

		GLOWE::Vector<GLOWE::UniquePtr<ParseStructEntity> >precompile(const GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&, size_t = 0);

		CompiledScript compile(const GLOWE::String&);
		CompiledScript compile(const GLOWE::Vector<GLOWE::UniquePtr<RawToken> >&);

		Interpreter() = default;
		Interpreter(Interpreter&&) noexcept = default;

		Interpreter(const rules_type&, const precompile_rules_type&, const compile_rules_type&);

		virtual ~Interpreter() = default;
	};
}

#endif
