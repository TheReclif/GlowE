#pragma once
#ifndef NTOKENS_H
#define NTOKENS_H

#include "../GlowSystem/Utility.h"
#include "../GlowSystem/String.h"
#include "../GlowSystem/Regex.h"

namespace NS
{
    struct GLOWSYSTEM_EXPORT RawToken
    {
        virtual std::type_index getType() const noexcept = 0;
        virtual bool comp(const RawToken*) const noexcept;
        virtual bool comp(RawToken*) const noexcept;
        virtual RawToken* copy() = 0;

        virtual ~RawToken() = default;
    };
    struct GLOWSYSTEM_EXPORT RawString
		: public RawToken
    {
		GLOWE::String str;

        virtual std::type_index getType() const noexcept;

        virtual GLOWE::Vector<GLOWE::UniquePtr<RawToken> >split(const size_t,const size_t);
        virtual RawToken* copy();

        RawString() = default;

        RawString(const GLOWE::String&);

        RawString(const RawString&) = default;
        RawString(RawString&&) noexcept = default;
        virtual ~RawString() = default;
    };

    struct GLOWSYSTEM_EXPORT RawName
		: public RawToken
    {
		GLOWE::String name;

        virtual std::type_index getType() const noexcept;
        virtual RawToken* copy();

        RawName() = default;

        RawName(const GLOWE::String&);

        RawName(const RawName&) = default;
        RawName(RawName&&) noexcept = default;

        virtual ~RawName() = default;
    };

    struct GLOWSYSTEM_EXPORT RawValue
		: public RawToken
    {
        virtual std::type_index getType() const noexcept;

        virtual ~RawValue() = default;
    };
    struct GLOWSYSTEM_EXPORT RawValNumber
		: public RawValue
    {
        double val;

        virtual std::type_index getType() const noexcept;
        virtual RawToken* copy();

        RawValNumber();

        RawValNumber(const double);
        RawValNumber(const GLOWE::String&);

        RawValNumber(const RawValNumber&) = default;
        RawValNumber(RawValNumber&&) noexcept = default;

        virtual ~RawValNumber() = default;
    };
    struct GLOWSYSTEM_EXPORT RawValString
		: public RawValue
    {
		GLOWE::String val;

        virtual std::type_index getType() const noexcept;
        virtual RawToken* copy();

        RawValString() = default;

        RawValString(const GLOWE::String&);

        RawValString(const RawValString&) = default;
        RawValString(RawValString&&) noexcept = default;

        virtual ~RawValString() = default;
    };

    struct GLOWSYSTEM_EXPORT RawOperator
		: public RawToken
    {
		GLOWE::String op;

        virtual std::type_index getType() const noexcept;
        virtual RawToken* copy();

        RawOperator() = default;

        RawOperator(const GLOWE::String&);

        RawOperator(const RawOperator&) = default;
        RawOperator(RawOperator&&) noexcept = default;

        virtual ~RawOperator() = default;

    };

    struct GLOWSYSTEM_EXPORT RawSeparator
		: public RawToken
    {
        bool br;

        virtual std::type_index getType() const noexcept;
        virtual RawToken* copy();

        RawSeparator();

        RawSeparator(const bool);

        RawSeparator(const RawSeparator&) = default;
        RawSeparator(RawSeparator&&) noexcept = default;

        virtual ~RawSeparator() = default;
    };

    struct GLOWSYSTEM_EXPORT RawScopePart
		: public RawToken
    {
        unsigned int state;
        unsigned int type;

        virtual std::type_index getType() const noexcept;
        virtual RawToken* copy();

        RawScopePart();
        RawScopePart(const char&);

        RawScopePart(const RawScopePart&) = default;
        RawScopePart(RawScopePart&&) noexcept = default;

        virtual ~RawScopePart() = default;
    };

    struct GLOWSYSTEM_EXPORT RawScope
		: public RawToken, public GLOWE::NoCopy
    {
        unsigned int type;

        GLOWE::Vector<GLOWE::UniquePtr<RawToken>> tokens;
        virtual RawToken* copy();

        virtual std::type_index getType() const noexcept;

        RawScope();
        RawScope(GLOWE::Vector<GLOWE::UniquePtr<RawToken>>&&);

        //RawScope(const RawScope&) = default;
        RawScope(RawScope&&) noexcept = default;

        virtual ~RawScope() = default;
    };

    template<class T>
	RawToken* createRaw(const GLOWE::String& source)
    {
		thread_local GLOWE::GlobalAllocator& alloc = GLOWE::getInstance<GLOWE::GlobalAllocator>();
		return alloc.allocate<T>(source);
    }

    class Value
    {
        //
    };

    class GLOWSYSTEM_EXPORT Scope
		: public GLOWE::NoCopy
    {
    public:
        GLOWE::Map<GLOWE::String, GLOWE::UniquePtr<Value>> variables;
        GLOWE::Vector<GLOWE::UniquePtr<Value>> tmp;

        Scope* parent;
    };

    class GLOWSYSTEM_EXPORT CompiledToken
    {
    public:
        virtual std::type_index getType() const noexcept = 0;

        virtual void exec(Scope&, size_t&) = 0;

        virtual ~CompiledToken() = default;
    };

    class GLOWSYSTEM_EXPORT FunctionToken
		: public CompiledToken
    {
    public:
        virtual std::type_index getType() const noexcept;
        std::function<void(Scope&)> func;

        virtual void exec(Scope&, size_t&);
    };

    class GLOWSYSTEM_EXPORT JumpToken
		: public CompiledToken
    {
    public:
        int jmp;

        virtual std::type_index getType() const noexcept;
        virtual void exec(Scope&, size_t&);
    };

    struct ParseStructEntity
    {
        //
    };

    struct GLOWSYSTEM_EXPORT ParseValue
		: public ParseStructEntity, public GLOWE::NoCopy
    {
        GLOWE::UniquePtr<Value> value;
    };

    struct GLOWSYSTEM_EXPORT ParseTreeCellToken
		: public ParseStructEntity, public GLOWE::NoCopy
    {
        bool strict;
		GLOWE::UniquePtr<CompiledToken> func_t;

        GLOWE::Vector<GLOWE::UniquePtr<ParseStructEntity>> children;
    };

    class GLOWSYSTEM_EXPORT ParseStruct
		: public ParseStructEntity, public GLOWE::NoCopy
    {
    public:
		GLOWE::Vector<GLOWE::UniquePtr<ParseStructEntity>> entities;
    };
}
#endif // NTOKENS_H
