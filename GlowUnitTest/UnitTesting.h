#pragma once
#ifndef GLOWE_UNITTESTING_INCLUDED
#define GLOWE_UNITTESTING_INCLUDED

//Testing only
//#define GlowUnitTest
//#define GlowTestGlowWindow

#include "../GlowSystem/Utility.h"
#include "../GlowSystem/Time.h"
#include "../GlowSystem/String.h"

#ifdef GlowTestGlowWindow
#include "../GlowWindow/Uniform/Window.h"
#include "../GlowWindow/Uniform/WindowImplementation.h"
#endif

// GlowAssert is redefined for unit tests.
#undef GlowAssert
#define GlowAssert(...) \
if(!(__VA_ARGS__)) \
{ \
	throw GLOWE::UnitTestAssertFailedException(u8"Assertion failed in " __FILE__ " at line " GlowU8StringifyMacroValue(__LINE__) ": " GlowU8Stringify(__VA_ARGS__)); \
}

#define __GlowCreateTestCaseClass(testName) \
__glowTestCase_##testName

#define __GlowRegisterTestName(testName) \
__glowRegistrator_##testName

#define __GlowCreateClockName(clockName) \
__glowClock_##clockName

#define __GlowRegisterTest(testName) \
static const GLOWE::UnitTestRegistrator<__GlowCreateTestCaseClass(testName)> __GlowRegisterTestName(testName)(GLOWE::String(GlowU8Stringify(testName)));

#define GlowMeasureTime(sectionName) \
GLOWE::UnitTestClock __GlowCreateClockName(sectionName)(GlowU8Stringify(sectionName));

#define GlowTestCase(testName) \
class __GlowCreateTestCaseClass(testName) : public GLOWE::UnitTest \
{ \
public: \
	__GlowCreateTestCaseClass(testName)(); \
	virtual void operator()() override; \
}; \
\
__GlowRegisterTest(testName);\
\
__GlowCreateTestCaseClass(testName)::__GlowCreateTestCaseClass(testName)() \
	: UnitTest(0, 0, GLOWE::String(GlowU8Stringify(testName))) \
{ \
	 \
} \
\
void __GlowCreateTestCaseClass(testName)::operator()()

namespace GLOWE
{
	class GLOWSYSTEM_EXPORT UnitTest
	{
	protected:
		UnitTest(Time newTimeout, unsigned int newFTS, const String& newID);

		Time timeout;
		unsigned int failuresToSkip;
		String testID;

		OStringStream streamOut;
	public:
		UnitTest() = delete;
		~UnitTest() = default;

		virtual void operator()() = 0;
		void setTimeout(const Time& arg);
		void setFailuresToSkip(unsigned int arg);

		const String& getTestID() const;
		String getStreamOut() const;

		void clearStreamOut();
	};

	class GLOWSYSTEM_EXPORT UnitTestException
		: public Exception
	{
	public:
		using Exception::Exception;
	};

	class GLOWSYSTEM_EXPORT UnitTestAssertFailedException
		: public UnitTestException
	{
	public:
		using UnitTestException::UnitTestException;
	};

	template<class T>
	class UnitTestRegistrator
	{
	private:
		UnitTestRegistrator();
	public:
		UnitTestRegistrator(const String& str);
		~UnitTestRegistrator() = default;
	};

	class GLOWSYSTEM_EXPORT UnitTestRegistry
		: public Subsystem
	{
	private:
		Map<String, UnitTest*> functions;
	public:
		UnitTestRegistry() = default;
		~UnitTestRegistry();

		void addNewFunction(const String& name, UnitTest * functor);

		bool doesFunctionExist(const String& name) const;

		UnitTest* getFunction(const String& name) const;

		const Map<String, UnitTest*>& getAllFunctions() const;
	};

	class GLOWSYSTEM_EXPORT UnitTestCore
	{
	private:
		Time lastTestTime;
		unsigned int failuresCount;

		// These tests won't be executed during executeAllTests method
		// Or "testAll" option
		Vector<String> excludedTests;
	private:
		void executeTest(UnitTest* test, const bool toggleLogging = true);
	public:
		UnitTestCore();
		~UnitTestCore() = default;

		int main(int argc, char** argv);

		void executeAllTests();

		void nextArgument(const char* arg);

		const bool hasAnyTestFailed() const;
	};

	class GLOWSYSTEM_EXPORT UnitTestClock
	{
	private:
		UnitTestClock();

		GLOWE::Clock clock;
		GLOWE::String name;
	public:
		UnitTestClock(const String& str);
		~UnitTestClock();
	};

	template<class T>
	inline UnitTestRegistrator<T>::UnitTestRegistrator(const String& str)
	{
		UnitTestRegistry& registry = getInstance<UnitTestRegistry>();

		registry.addNewFunction(str, getInstance<GlobalAllocator>().allocate<T>());
	}
}

#ifdef GlowUnitTest

#ifdef GLOWE_COMPILE_FOR_WINDOWS
#ifdef __GNUC__
int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
#else
int __stdcall wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd)
#endif
{
	GLOWE::UnitTestCore core;
	int argc = 0;
	LPWSTR tempCmdLine = GetCommandLineW();
	LPWSTR* argvRaw = CommandLineToArgvW(tempCmdLine, &argc);

#ifdef GlowTestGlowWindow
	GLOWE::Windows::WindowsWindowImplementation::setInitialWindowState(nShowCmd);
#endif

	if (!AllocConsole())
	{
		return 1;
	}

	FILE* __glowCout = nullptr, *__glowCin = nullptr;

#ifdef _MSC_VER
	freopen_s(&__glowCout, "CONOUT$", "w", stdout);
	freopen_s(&__glowCin, "CONIN$", "r", stdin);
#else
	__glowCout = std::freopen("CONOUT$", "w", stdout);
	__glowCin = std::freopen("CONIN$", "r", stdin);
#endif

	SetConsoleTitleW(L"GlowE UTF");

	GLOWE::Vector<GLOWE::String> a;

	for (int x = 0; x < argc; ++x)
	{
		a.push_back(argvRaw[x]);
	}

	LocalFree(argvRaw);

	std::unique_ptr<char*[]> argv(new char*[argc]);

	for (int x = 0; x < argc; ++x)
	{
		argv[x] = a[x].getCharArray();
	}

	int result = core.main(argc, argv.get());

	std::fclose(__glowCout);
	std::fclose(__glowCin);

	FreeConsole();

	return result;
}
#else
int main(int argc, char** argv)
{
	GLOWE::UnitTestCore core;
	return core.main(argc, argv);
}
#endif

#endif

#endif
