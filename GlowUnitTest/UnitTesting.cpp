#include "UnitTesting.h"

void GLOWE::UnitTestCore::executeTest(UnitTest * test, const bool toggleLogging)
{
	if (!test)
	{
		return;
	}

	Clock clock;

	UnitTest& tempTest = *test;

	if (toggleLogging)
	{
		std::cout << u8"Entering test case: " << tempTest.getTestID().getString() << u8"\n";
	}
	
	try
	{
		clock.reset();
		tempTest();
		lastTestTime = clock.getElapsedTime();
	}
	catch (const UnitTestException& temp)
	{
		if (toggleLogging)
		{
			std::cout << u8"UnitTestException caught. Exception.what(): " << temp.what();
			std::cout << u8"\nAborting execution." << std::endl;
		}
		++failuresCount;
	}
	catch (const std::exception& a)
	{
		if (toggleLogging)
		{
			std::cout << u8"std::exception caught. Exception.what(): " << a.what() << std::endl;
		}
		++failuresCount;
	}
	catch (...)
	{
		if (toggleLogging)
		{
			std::cout << u8"Unknown exception caught." << std::endl;
		}
		++failuresCount;
	}

	String outputStreamContents = tempTest.getStreamOut();
	tempTest.clearStreamOut();

	if (!outputStreamContents.isEmpty())
	{
		std::cout << tempTest.getTestID() << u8"'s output stream:\n" << outputStreamContents << '\n' << std::endl;
	}

	if (toggleLogging)
	{
		std::cout << u8"Execution time: " << lastTestTime.asMicroseconds() << " microseconds.\n";
		std::cout << u8"Leaving test case: " << tempTest.getTestID().getString() << '\n';
	}
}

void GLOWE::UnitTestCore::executeAllTests()
{
	using std::cout;
	using std::endl;

	Map<String, UnitTest*> allTests(getInstance<UnitTestRegistry>().getAllFunctions());
	Map<String, Time> testTimes;
	Vector<String> failedTestsNames;

	for (const auto& x : excludedTests)
	{
		if (allTests.count(x))
		{
			allTests.erase(x);
		}
	}

	for (const auto& x : allTests)
	{
		executeTest(x.second, false);
		if (hasAnyTestFailed())
		{
			--failuresCount;
			failedTestsNames.push_back(x.first);
		}
		else
		{
			testTimes.insert(std::make_pair(x.first, lastTestTime));
		}
	}

	cout << u8"Test result for " << toString(allTests.size()) << u8" unit tests:\n\n";

	if (failedTestsNames.empty())
	{
		cout << u8"All unit tests have passed successfully.\n\n";
	}
	else if(failedTestsNames.size() == allTests.size())
	{
		cout << u8"All unit tests have failed.\n\n";
	}
	else
	{
		cout << toString(failedTestsNames.size()) << u8" unit tests failed.\n\nFailed tests:\n\n";
		for (const auto& x : failedTestsNames)
		{
			cout << x << u8"\n";
		}

		cout << endl;
	}

	cout << u8"Unit tests' execution times:\n";

	for (const auto& x : testTimes)
	{
		cout << x.first << u8": " << x.second.asMicroseconds() << u8" microsecs or " << x.second.asMilliseconds() << u8" milisecs.\n";
	}

	cout << endl;
}

GLOWE::UnitTestCore::UnitTestCore()
	: failuresCount(0U)
{
}

int GLOWE::UnitTestCore::main(int argc, char ** argv)
{
#ifdef _DEBUG
#ifdef _MSC_VER
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF); //_CRTDBG_LEAK_CHECK_DF
#endif
#endif

	GLOWE::UnitTestRegistry& registry = getInstance<GLOWE::UnitTestRegistry>();

	std::cout << u8"GlowEngine Unit Test Framework Utility" << '\n';
	std::cout << u8"Version number: " << GLOWE_VERSION_NUMBER << '\n';
	std::cout << u8"Version name: " << GLOWE_VERSION_NAME << '\n';
	std::cout << u8"Version development name: " << GLOWE_VERSION_DEVELOPMENT_NAME << "\n\n" << std::endl;

	std::cout << u8"Starting memory status:\n";
	MemoryProfiler::writeAllMemoryUsagesToStream(std::cout);

	if (argc <= 1)
	{
		GLOWE::UnitTest* testMain = registry.getFunction(u8"Main");

		std::cout << u8"No arguments provided, running \"Main\" test." << std::endl;
		if (testMain)
		{
			try
			{
				(*testMain)();

				String outputStreamContents = testMain->getStreamOut();
				if (!outputStreamContents.isEmpty())
				{
					std::cout << testMain->getTestID() << u8"'s output stream:\n" << outputStreamContents << std::endl << std::endl;
				}
			}
			catch (const UnitTestException& temp)
			{
				std::cout << u8"UnitTestException caught. Exception.what(): " << temp.what();
				std::cout << u8"\nAborting execution." << std::endl;
				return 1;
			}
			catch (const std::exception& temp)
			{
				std::cout << u8"std::exception caught. Exception.what(): " << temp.what();
				std::cout << u8"\nAborting execution." << std::endl;
				return 1;
			}
			catch (...)
			{
				std::cout << u8"Unknown exception caught.\nAborting execution." << std::endl;
				return 1;
			}
		}
		else
		{
			String commandName;

			std::cout << u8"Unable to find test \"Main\".\n";
			std::cout << u8"Command processor mode enabled." << std::endl;

			while (std::getline(std::cin, commandName.getString()) && !commandName.isEmpty())
			{
				if (commandName[0] != '-')
				{
					commandName.insert(0, '-');
				}
				nextArgument(commandName.getCharArray());
			}
		}
	}
	else
	{
		for (int x = 1; x < argc && !hasAnyTestFailed(); ++x)
		{
			nextArgument(argv[x]);
		}

		if (hasAnyTestFailed())
		{
			std::cout << u8"Exception caught. Execution aborted." << std::endl;
			return 1;
		}
	}

	std::cout << u8"End memory status:\n";
	MemoryProfiler::writeAllMemoryUsagesToStream(std::cout);

	std::cout << u8"Done.\nPress any key and Enter to continue." << std::endl;

	String __garbage;
	std::cin >> __garbage;

	return 0;
}

void GLOWE::UnitTestCore::nextArgument(const char * arg)
{
	GLOWE::UnitTestRegistry& registry = getInstance<GLOWE::UnitTestRegistry>();

	String argTemp(arg), strTemp;
	String::BasicString& strArg = argTemp.getString();
	String::BasicString& str = strTemp.getString();

	if (!strArg.empty())
	{
		strArg.erase(0, 1);
	}

	if (strArg.find(u8"testCase") != String::BasicString::npos || strArg.find(u8"test_case") != String::BasicString::npos && strArg.find(u8"=") != String::BasicString::npos)
	{
		size_t temp = strArg.find('=');
		str = strArg.substr(temp + 1);

		if (registry.doesFunctionExist(strTemp))
		{
			executeTest(registry.getFunction(strTemp));
			return;
		}
	}
	else if (strArg.find(u8"excludeTest") != String::BasicString::npos || strArg.find(u8"exclude_test") != String::BasicString::npos)
	{
		size_t temp = strArg.find('=');
		str = strArg.substr(temp + 1);
		excludedTests.push_back(str);
		return;
	}
	else if (strArg.find(u8"testAll") != String::BasicString::npos || strArg.find(u8"test_all") != String::BasicString::npos)
	{
		executeAllTests();
		return;
	}

	std::cout << arg << u8": unrecognized argument." << std::endl;
}

const bool GLOWE::UnitTestCore::hasAnyTestFailed() const
{
	return failuresCount != 0;
}

GLOWE::UnitTestRegistry::~UnitTestRegistry()
{
	GlobalAllocator& all = getInstance<GlobalAllocator>();
	for (auto& x : functions)
	{
		all.deallocate(x.second);
	}
}

void GLOWE::UnitTestRegistry::addNewFunction(const String & name, UnitTest * functor)
{
	functions[name] = functor;
}

bool GLOWE::UnitTestRegistry::doesFunctionExist(const String & name) const
{
	return functions.count(name) != 0;
}

GLOWE::UnitTest * GLOWE::UnitTestRegistry::getFunction(const String & name) const
{
	if (doesFunctionExist(name))
	{
		return functions.at(name);
	}

	return nullptr;
}

const GLOWE::Map<GLOWE::String, GLOWE::UnitTest*>& GLOWE::UnitTestRegistry::getAllFunctions() const
{
	return functions;
}

GLOWE::UnitTest::UnitTest(Time newTimeout, unsigned int newFTS, const String & newID)
	: timeout(newTimeout), failuresToSkip(newFTS), testID(newID)
{
}

void GLOWE::UnitTest::setTimeout(const Time & arg)
{
	timeout = arg;
}

void GLOWE::UnitTest::setFailuresToSkip(unsigned int arg)
{
	failuresToSkip = arg;
}

const GLOWE::String& GLOWE::UnitTest::getTestID() const
{
	return testID;
}

GLOWE::String GLOWE::UnitTest::getStreamOut() const
{
	return streamOut.str();
}

void GLOWE::UnitTest::clearStreamOut()
{
	streamOut.clear();
}

GLOWE::UnitTestClock::UnitTestClock(const String& str)
	: name(str)
{
	clock.reset();
}

GLOWE::UnitTestClock::~UnitTestClock()
{
	Time temp = clock.getElapsedTime();
	std::cout << name << u8" results: " << temp.asMicroseconds() << u8" microseconds.\n";
}
