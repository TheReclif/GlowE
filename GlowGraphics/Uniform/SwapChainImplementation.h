#pragma once
#ifndef GLOWE_SWAPCHAINIMPL_INCLUDED
#define GLOWE_SWAPCHAINIMPL_INCLUDED

#include "Format.h"

#include "../../GlowWindow/Uniform/VideoMode.h"

namespace GLOWE
{
	class WindowImplementation;
	class Window;
	class RenderTargetImpl;
	class GraphicsDeviceImpl;

	struct GLOWGRAPHICS_EXPORT SwapChainDesc
	{
		/// @brief Back buffer count. 1 for double buffering, 2 for triple buffering. 0 is also supported, but why use it?
		unsigned char bufferCount = 1;
		/// @brief MSAA level. Defaults to 0, which disables this technique.
		unsigned char msaa = 0;
		Format displayFormat = Format(Format::Type::UnsignedByte, 4, true, true);
		VideoMode size;
		/// @brief How many vblanks to wait for. 0 means almost instantaneous present, 1 is typical vsync, 2 and more is also supported, but not as useful.
		unsigned int initialPresentInterval = 0;
	};

	class GLOWGRAPHICS_EXPORT GraphicsDeviceDiedException
		: public Exception
	{
	public:
		inline GraphicsDeviceDiedException()
			: Exception("Graphics device died/removed")
		{}
	};

	class GLOWGRAPHICS_EXPORT SwapChain
		: public NoCopy
	{
	protected:
		SwapChainDesc usedDesc;
	private:
		bool needsSizeUpdate = false;
	protected:
		virtual void resizeBuffers(const VideoMode& videoMode) = 0;
	public:
		virtual ~SwapChain() = default;

		virtual void create(const SwapChainDesc& desc, const WindowImplementation* const window, GraphicsDeviceImpl& device) = 0;
		virtual void create(const SwapChainDesc& desc, const Window& window, GraphicsDeviceImpl& device) = 0;
		virtual void destroy() = 0;

		virtual void setPresentInterval(const unsigned int vsync) = 0;

		void resize(const VideoMode& newMode);
		void updateSize();

		virtual RenderTargetImpl& getRenderTarget() = 0;
		virtual const RenderTargetImpl& getRenderTarget() const = 0;

		virtual void present() const = 0;

		virtual bool checkIsValid() const = 0;

		const SwapChainDesc& getDesc() const;
	};
}

#endif
