#include "SamplerDesc.h"

GLOWE::SamplerDesc::SamplerDesc()
	: minFilter(Filter::Nearest), magFilter(Filter::Nearest), mipFilter(Filter::Nearest), anisotropyLevel(1),
	minLOD(-1000.0f), maxLOD(1000.0f), mipBiasLOD(0.0f),
	enableComparison(false), addressU(TextureAddressingMode::Clamp), addressV(TextureAddressingMode::Clamp),
	addressW(TextureAddressingMode::Clamp), borderColor(1.0f, 1.0f, 1.0f, 1.0f), compFunc(ComparisonFunction::Never)
{
}

GLOWE::Hash GLOWE::SamplerDesc::getHash() const
{
	//return Hash((const char*)this, sizeof(*this));
	String temp = toString((std::underlying_type<Filter>::type)minFilter)
		+ toString((std::underlying_type<Filter>::type)magFilter)
		+ '|'
		+ toString((std::underlying_type<Filter>::type)mipFilter)
		+ '|'
		+ toString(anisotropyLevel)
		+ '|'
		+ toString(minLOD)
		+ '|'
		+ toString(maxLOD)
		+ '|'
		+ toString(mipBiasLOD)
		+ '|'
		+ toString(enableComparison)
		+ '|'
		+ toString((std::underlying_type<TextureAddressingMode>::type)addressU)
		+ toString((std::underlying_type<TextureAddressingMode>::type)addressV)
		+ toString((std::underlying_type<TextureAddressingMode>::type)addressW)
		+ '|'
		+ toString((unsigned int)(borderColor.rgba[0] * 255.0f))
		+ '|'
		+ toString((unsigned int)(borderColor.rgba[1] * 255.0f))
		+ '|'
		+ toString((unsigned int)(borderColor.rgba[2] * 255.0f))
		+ '|'
		+ toString((unsigned int)(borderColor.rgba[3] * 255.0f))
		+ '|'
		+ toString((std::underlying_type<ComparisonFunction>::type)compFunc);

	return Hash(temp);
}

void GLOWE::SamplerDesc::serialize(LoadSaveHandle& handle) const
{
	handle << minFilter << magFilter << mipFilter << anisotropyLevel << minLOD << maxLOD << mipBiasLOD << enableComparison << addressU << addressV << addressW << borderColor << compFunc;
}

void GLOWE::SamplerDesc::deserialize(LoadSaveHandle& handle)
{
	handle >> minFilter >> magFilter >> mipFilter >> anisotropyLevel >> minLOD >> maxLOD >> mipBiasLOD >> enableComparison >> addressU >> addressV >> addressW >> borderColor >> compFunc;
}
