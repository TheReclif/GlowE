#include "GraphicsWindow.h"
#include "GraphicsFactory.h"

void GLOWE::GraphicsWindow::display()
{
	if (swapChain)
	{
		swapChain->present();
	}
}

GLOWE::SwapChain& GLOWE::GraphicsWindow::getSwapChain()
{
	return *swapChain;
}

const GLOWE::SwapChain& GLOWE::GraphicsWindow::getSwapChain() const
{
	return *swapChain;
}

void GLOWE::GraphicsWindow::onResize(const size_t newHeight, const size_t newWidth)
{
	if (swapChain && swapChain->checkIsValid())
	{
		swapChain->resize(VideoMode(newWidth, newHeight, 32));
	}
}

void GLOWE::GraphicsWindow::create(GraphicsDeviceImpl& gc, const VideoMode& size, const String& title, const SwapChainDesc& desc, const Int2& position, const WindowMode& wndMode)
{
	Window::create(size, title, position, wndMode);

	VideoMode trueSize = size;
	if (size.x < 8 || size.y < 8)
	{
		trueSize.x = trueSize.y = 8; // Choose a reasonable default window size.
	}
	SettingsMgr& settingsMgr = getInstance<SettingsMgr>();

	swapChain = createGraphicsObject<SwapChain>();
	swapChain->create(desc, *this, gc);
}

void GLOWE::GraphicsWindow::destroy()
{
	if (swapChain)
	{
		swapChain->destroy();
		swapChain = nullptr;
	}
	Window::destroy();
}

void GLOWE::GraphicsWindow::resize(const VideoMode& newSize)
{
	Window::resize(newSize);
	swapChain->resize(newSize);
}

void GLOWE::GraphicsWindow::resize(const VideoMode& newSize, const WindowMode& newMode)
{
	Window::resize(newSize, newMode);
	swapChain->resize(newSize);
}
