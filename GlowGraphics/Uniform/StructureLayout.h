#pragma once
#ifndef GLOWE_UNIFORM_STRUCTURELAYOUT_INCLUDED
#define GLOWE_UNIFORM_STRUCTURELAYOUT_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/Hash.h"
#include "../../GlowSystem/SerializableDataCreator.h"
#include "../../GlowSystem/String.h"

#include "Format.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT StructureLayout
		: public BinarySerializable, public Hashable
	{
	protected:
		Vector<Format> elems;
		std::size_t currentSize;
		Hash hash;
	protected:
		void rehash();
	public:
		StructureLayout();
		virtual ~StructureLayout() = default;

		void addElement(const Format& elem);

		Format getElem(const unsigned int element) const;

		std::size_t getSize() const;
		std::size_t getElemsCount() const;

		std::size_t getOffsetToElem(const unsigned int element) const;
		std::size_t getElemSize(const unsigned int element) const;

		void* getAddressOfElement(const unsigned element, void* const mem) const;

		virtual void serialize(LoadSaveHandle& handle) const override;
		virtual void deserialize(LoadSaveHandle& handle) override;

		virtual Hash getHash() const override;
	};

	class GLOWGRAPHICS_EXPORT HashedStructureLayout
		: protected StructureLayout
	{
	protected:
		Map<Hash, UInt32> hashes;
	protected:
		void rehash();
	public:
		HashedStructureLayout() = default;
		virtual ~HashedStructureLayout() = default;

		StructureLayout toStructureLayout() const;

		void addElement(const Format& elem, const Hash& name);

		Hash getHash(const unsigned int element) const;
		int getElemPos(const Hash& name) const; // Returns -1 if not found.

		bool checkIsElemPresent(const Hash& name) const;

		Format getElem(const Hash& name) const;

		std::size_t getOffsetToElem(const Hash& name) const;
		std::size_t getElemSize(const Hash& name) const;

		void* getAddressOfElement(const Hash& name, void* const mem) const;

		virtual void serialize(LoadSaveHandle& handle) const override;
		virtual void deserialize(LoadSaveHandle& handle) override;

		using StructureLayout::getElem;
		using StructureLayout::getSize;
		using StructureLayout::getElemsCount;
		using StructureLayout::getOffsetToElem;
		using StructureLayout::getElemSize;
		using StructureLayout::getAddressOfElement;
		using StructureLayout::getHash;
	};

	class GLOWGRAPHICS_EXPORT NamedStructureLayout
		: protected StructureLayout
	{
	protected:
		Map<String, UInt32> names;
	protected:
		void rehash();
	public:
		NamedStructureLayout() = default;
		virtual ~NamedStructureLayout() = default;

		StructureLayout toStructureLayout() const;
		HashedStructureLayout toHashedStructureLayout() const;

		void addElement(const Format& elem, const String& name);

		String getName(const unsigned int element) const;
		int getElemPos(const String& name) const; // Returns -1 if not found.

		bool checkIsElemPresent(const String& name) const;

		Format getElem(const String& name) const;

		std::size_t getOffsetToElem(const String& name) const;
		std::size_t getElemSize(const String& name) const;

		void* getAddressOfElement(const String& name, void* const mem) const;

		virtual void serialize(LoadSaveHandle& handle) const override;
		virtual void deserialize(LoadSaveHandle& handle) override;

		using StructureLayout::getElem;
		using StructureLayout::getSize;
		using StructureLayout::getElemsCount;
		using StructureLayout::getOffsetToElem;
		using StructureLayout::getElemSize;
		using StructureLayout::getAddressOfElement;
		using StructureLayout::getHash;
	};
}

#endif
