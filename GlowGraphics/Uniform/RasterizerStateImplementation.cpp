#include "RasterizerStateImplementation.h"

GLOWE::RasterizerStateImpl::RasterizerStateImpl()
	: desc(), hash()
{
}

GLOWE::RasterizerStateImpl::RasterizerStateImpl(const RasterizerDesc & arg)
	: desc(arg), hash(desc.getHash())
{
}

GLOWE::Hash GLOWE::RasterizerStateImpl::getHash() const
{
	return hash;
}
