#pragma once
#ifndef GLOWE_GRAPHICS_UNIFORM_INPUTLAYOUTIMPLEMENTATION_INCLUDED
#define GLOWE_GRAPHICS_UNIFORM_INPUTLAYOUTIMPLEMENTATION_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/Hash.h"

#include "InputLayoutDesc.h"

namespace GLOWE
{
	class GraphicsDeviceImpl;
	class VertexShaderImpl;

	class GLOWGRAPHICS_EXPORT InputLayoutImpl
		: public Hashable, public NoCopy
	{
	protected:
		InputLayoutDesc desc;
	public:
		InputLayoutImpl() = default;
		InputLayoutImpl(const InputLayoutDesc& arg);
		virtual ~InputLayoutImpl() = default;

		virtual void create(const InputLayoutDesc& inputDesc, const GraphicsDeviceImpl& device, const VertexShaderImpl& shader) = 0;
		virtual void destroy() = 0;

		virtual bool checkIsValid() const = 0;

		virtual Hash getHash() const override;
	};
}

#endif
