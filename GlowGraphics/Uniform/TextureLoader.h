#pragma once
#ifndef GLOWE_UNIFORM_TEXTURELOADER_INCLUDED
#define GLOWE_UNIFORM_TEXTURELOADER_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/Package.h"

#include "TextureImplementation.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT TextureLoader
		: public Subsystem
	{
	public:
		enum class FileType
		{
			PNG,
			JPG,
			JP2,
			TARGA,
			TIFF,
			BMP,
			EXR,
			HDR,
			ICO
		};
	public:
		TextureLoader();
		~TextureLoader();

		UniquePtr<TextureImpl> loadTexture(const String& filename, const TextureDesc& texDesc, const GraphicsDeviceImpl& device) const;
		UniquePtr<TextureImpl> loadTextureFromPackage(const String& filename, Package& package, const TextureDesc& texDesc, const GraphicsDeviceImpl& device) const;
		UniquePtr<TextureImpl> loadTextureFromMemory(const void* mem, const std::size_t size, const TextureDesc& texDesc, const GraphicsDeviceImpl& device) const;

		void saveImageToFile(const String& filename, const void* const data, const unsigned int width, const unsigned int height, const Format& pixelFormat, const FileType fileType) const;

		/*
		Texture&& loadTextureFromGlowTex(const void* mem, const std::size_t memSize, const GraphicsDeviceImpl& device) const; // Can also load texture arrays and 3D textures.

		Texture&& loadTexture(const Vector<String>& filenames, const TextureDesc& texDesc, const GraphicsDeviceImpl& device) const;
		Texture&& loadTextureFromPackage(const Vector<String>& filenames, Package& package, const TextureDesc& texDesc, const GraphicsDeviceImpl& device) const;
		Texture&& loadTextureFromMemory(const Vector<void*>& mem, const TextureDesc& texDesc, const GraphicsDeviceImpl& device) const;
		*/
	};
}

#endif
