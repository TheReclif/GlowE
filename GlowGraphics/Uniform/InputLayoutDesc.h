#pragma once
#ifndef GLOWE_UNIFORM_INPUTLAYOUTDESC_INCLUDED
#define GLOWE_UNIFORM_INPUTLAYOUTDESC_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/Hash.h"
#include "../../GlowSystem/SerializableDataCreator.h"
#include "../../GlowSystem/String.h"

#include "StructureLayout.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT InputLayoutDesc
		: public BinarySerializable, public Hashable
	{
	private:
		NamedStructureLayout vertexLayout, instanceLayout;
		Vector<UInt32> instanceStepRates, vertexInputSlots, instanceInputSlots;
		Hash hash;
	private:
		void rehash();
	public:
		InputLayoutDesc() = default;
		virtual ~InputLayoutDesc() = default;

		void addVertexElement(const String& semanticName, const Format& elem, const unsigned int inputSlot = 0);
		void addInstanceElement(const String& semanticName, const Format& elem, const UInt32 stepRate = 1, const unsigned int inputSlot = 1);

		const NamedStructureLayout& getVertexLayout() const;
		const NamedStructureLayout& getInstanceLayout() const;

		const Vector<UInt32>& getStepRates() const;
		const Vector<UInt32>& getVertexInputSlots() const;
		const Vector<UInt32>& getInstanceInputSlots() const;

		virtual void serialize(LoadSaveHandle& handle) const override;
		virtual void deserialize(LoadSaveHandle& handle) override;

		InputLayoutDesc& append(const InputLayoutDesc& desc);

		InputLayoutDesc operator+(const InputLayoutDesc& desc) const;

		InputLayoutDesc& operator+=(const InputLayoutDesc& desc);

		virtual Hash getHash() const override;
	};
}

#endif
