#pragma once
#ifndef GLOWE_UNIFORM_FORMAT_INCLUDED
#define GLOWE_UNIFORM_FORMAT_INCLUDED

#include "glowgraphics_export.h"

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/Hash.h"
#include "../../GlowSystem/String.h"
#include "../../GlowSystem/SerializableDataCreator.h"
#include "../../GlowSystem/LoadSaveHandle.h"
#include "../../GlowSystem/FileIO.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT Format
		: public Hashable, public BinarySerializable, public Serializable
	{
	public:
		enum class Type
		{
			Byte,
			UnsignedByte,
			Short,
			UnsignedShort,
			Int,
			UnsignedInt,
			HalfFloat,
			Float,
			Struct,
			Custom = Struct
		};

		enum ElemsCount
		{
			R10B10G10A2 = 40000000,
			R11B11G10,
			D16, // Unorm
			D24S8, // Unorm, UInt
			D32, // Float
			DXT1,
			DXT3,
			DXT5
		};
	public:
		Type type;
		UInt elemsCount;

		// Which types can be normalized:
		// UnsignedByte, Byte, Short, UnsignedShort.
		// UnsignedInt can be normalized if the elemsCount attribute equals R10B10G10A2. In this situation the isNormalized attribute is not ignored.
		bool isNormalized, isSRGB;
	public:
		Format();
		Format(const Type t, const UInt count, const bool norm = false, const bool srgb = false);

		GlowSerialize(GlowBases(), GlowField(type), GlowField(elemsCount), GlowField(isNormalized), GlowField(isSRGB));

		UInt getSize() const;

		virtual void serialize(LoadSaveHandle& handle) const override;
		virtual void deserialize(LoadSaveHandle& handle) override;

		virtual Hash getHash() const override;

		bool operator==(const Format& other) const;
		bool operator!=(const Format& other) const;
	};
}

#endif
