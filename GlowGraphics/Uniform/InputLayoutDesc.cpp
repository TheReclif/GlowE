#include "InputLayoutDesc.h"

void GLOWE::InputLayoutDesc::rehash()
{
	String temp;
	temp = vertexLayout.getHash().asNumberString() + '|';
	temp.reserve(80);
	temp += instanceLayout.getHash().asNumberString();

	for (const auto x : instanceStepRates)
	{
		temp += toString(x) + '|';
	}

	for (const auto x : vertexInputSlots)
	{
		temp += toString(x) + '|';
	}

	for (const auto x : instanceInputSlots)
	{
		temp += toString(x) + '|';
	}

	hash = Hash(temp);
}

void GLOWE::InputLayoutDesc::addVertexElement(const String & semanticName, const Format & elem, const unsigned int inputSlot)
{
	vertexLayout.addElement(elem, semanticName);
	vertexInputSlots.push_back(inputSlot);
	rehash();
}

void GLOWE::InputLayoutDesc::addInstanceElement(const String& semanticName, const Format& elem, const UInt32 stepRate, const unsigned int inputSlot)
{
	instanceLayout.addElement(elem, semanticName);
	instanceStepRates.push_back(stepRate);
	instanceInputSlots.push_back(inputSlot);
	rehash();
}

const GLOWE::NamedStructureLayout& GLOWE::InputLayoutDesc::getVertexLayout() const
{
	return vertexLayout;
}

const GLOWE::NamedStructureLayout& GLOWE::InputLayoutDesc::getInstanceLayout() const
{
	return instanceLayout;
}

const GLOWE::Vector<GLOWE::UInt32>& GLOWE::InputLayoutDesc::getStepRates() const
{
	return instanceStepRates;
}

const GLOWE::Vector<GLOWE::UInt32>& GLOWE::InputLayoutDesc::getVertexInputSlots() const
{
	return vertexInputSlots;
}

const GLOWE::Vector<GLOWE::UInt32>& GLOWE::InputLayoutDesc::getInstanceInputSlots() const
{
	return instanceInputSlots;
}

void GLOWE::InputLayoutDesc::serialize(LoadSaveHandle& handle) const
{
	handle << vertexLayout << instanceLayout;

	handle << static_cast<UInt32>(instanceStepRates.size());
	for (const auto& x : instanceStepRates)
	{
		handle << x;
	}

	handle << static_cast<UInt32>(vertexInputSlots.size());
	for (const auto& x : vertexInputSlots)
	{
		handle << x;
	}

	handle << static_cast<UInt32>(instanceInputSlots.size());
	for (const auto& x : instanceInputSlots)
	{
		handle << x;
	}
}

void GLOWE::InputLayoutDesc::deserialize(LoadSaveHandle& handle)
{
	handle >> vertexLayout >> instanceLayout;

	UInt32 tempU32{};
	handle >> tempU32;
	instanceStepRates.resize(tempU32);
	for (auto& x : instanceStepRates)
	{
		handle >> x;
	}

	handle >> tempU32;
	vertexInputSlots.resize(tempU32);
	for (auto& x : vertexInputSlots)
	{
		handle >> x;
	}

	handle >> tempU32;
	instanceInputSlots.resize(tempU32);
	for (auto& x : instanceInputSlots)
	{
		handle >> x;
	}

	rehash();
}

GLOWE::InputLayoutDesc& GLOWE::InputLayoutDesc::append(const InputLayoutDesc& desc)
{
	const unsigned int vertexElemsCount = desc.vertexLayout.getElemsCount();
	for (unsigned int x = 0; x < vertexElemsCount; ++x)
	{
		vertexLayout.addElement(desc.vertexLayout.getElem(x), desc.vertexLayout.getName(x));
	}

	const unsigned int instanceElemsCount = desc.instanceLayout.getElemsCount();
	for (unsigned int x = 0; x < instanceElemsCount; ++x)
	{
		instanceLayout.addElement(desc.instanceLayout.getElem(x), desc.instanceLayout.getName(x));
	}

	instanceStepRates.insert(instanceStepRates.end(), desc.instanceStepRates.begin(), desc.instanceStepRates.end());
	vertexInputSlots.insert(vertexInputSlots.end(), desc.vertexInputSlots.begin(), desc.vertexInputSlots.end());
	instanceInputSlots.insert(instanceInputSlots.end(), desc.instanceInputSlots.begin(), desc.instanceInputSlots.end());

	rehash();

	return *this;
}

GLOWE::InputLayoutDesc GLOWE::InputLayoutDesc::operator+(const InputLayoutDesc& desc) const
{
	InputLayoutDesc temp = *this;
	temp.append(desc);
	return temp;
}

GLOWE::InputLayoutDesc& GLOWE::InputLayoutDesc::operator+=(const InputLayoutDesc& desc)
{
	return append(desc);
}

GLOWE::Hash GLOWE::InputLayoutDesc::getHash() const
{
	return hash;
}
