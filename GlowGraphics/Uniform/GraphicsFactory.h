#pragma once
#ifndef GLOWE_UNIFORM_GRAPHICSFACTORY_INCLUDED
#define GLOWE_UNIFORM_GRAPHICSFACTORY_INCLUDED

#include "BlendStateImplementation.h"
#include "BufferImplementation.h"
#include "DepthStencilStateImplementation.h"
#include "GraphicsDeviceImplementation.h"
#include "InputLayoutImplementation.h"
#include "RasterizerStateImplementation.h"
#include "RenderTargetImplementation.h"
#include "SamplerImplementation.h"
#include "ShaderCollectionImplementation.h"
#include "ShaderImplementation.h"
#include "ShaderReflectImplementation.h"
#include "TextureImplementation.h"
#include "ShaderResourceViewImpl.h"
#include "UnorderedAccessViewImpl.h"
#include "SwapChainImplementation.h"

#include "../../GlowSystem/String.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT GraphicsFactoryCreationFailedException
		: public Exception
	{
	private:
		String info;
	public:
		GraphicsFactoryCreationFailedException(const String& cause);

		virtual const char* what() const noexcept override;
	};

	class GLOWGRAPHICS_EXPORT GraphicsFactoryImpl
		: public NoCopy, public Subsystem
	{
	public:
		using GetFactoryFunc = GraphicsFactoryImpl*(*)();
	public:
		virtual ~GraphicsFactoryImpl() = default;

		virtual UniquePtr<BlendStateImpl> createBlendState() const = 0;
		virtual UniquePtr<BufferImpl> createBuffer() const = 0;
		virtual UniquePtr<DepthStencilStateImpl> createDepthStencilState() const = 0;
		virtual UniquePtr<GraphicsDeviceImpl> createGraphicsDevice() const = 0;
		virtual UniquePtr<InputLayoutImpl> createInputLayout() const = 0;
		virtual UniquePtr<RasterizerStateImpl> createRasterizerState() const = 0;
		virtual UniquePtr<RenderTargetImpl> createRenderTarget() const = 0;
		virtual UniquePtr<SamplerImpl> createSampler() const = 0;
		virtual UniquePtr<ShaderCollectionImpl> createShaderCollection() const = 0;

		virtual UniquePtr<GLOWE::VertexShaderImpl> createVertexShader() const = 0;
		virtual UniquePtr<GLOWE::PixelShaderImpl> createPixelShader() const = 0;
		virtual UniquePtr<GLOWE::GeometryShaderImpl> createGeometryShader() const = 0;
		virtual UniquePtr<GLOWE::ComputeShaderImpl> createComputeShader() const = 0;

		virtual UniquePtr<ShaderReflectImpl> createShaderReflection() const = 0;
		virtual UniquePtr<TextureImpl> createTexture() const = 0;
		virtual UniquePtr<ShaderResourceViewImpl> createShaderResourceView() const = 0;
		virtual UniquePtr<UnorderedAccessViewImpl> createUnorderedAccessView() const = 0;
		virtual UniquePtr<SwapChain> createSwapChain() const = 0;
	};

	namespace Hidden
	{
		GLOWGRAPHICS_EXPORT void enableEngineRenderBackend();
	}

	template<>
	GLOWGRAPHICS_EXPORT GraphicsFactoryImpl& OneInstance<GraphicsFactoryImpl>::instance();

	template<class T>
	UniquePtr<T> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<BlendStateImpl> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<BufferImpl> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<DepthStencilStateImpl> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<GraphicsDeviceImpl> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<InputLayoutImpl> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<RasterizerStateImpl> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<RenderTargetImpl> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<SamplerImpl> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<ShaderCollectionImpl> createGraphicsObject();

	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<GLOWE::VertexShaderImpl> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<GLOWE::PixelShaderImpl> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<GLOWE::GeometryShaderImpl> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<GLOWE::ComputeShaderImpl> createGraphicsObject();

	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<ShaderReflectImpl> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<TextureImpl> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<ShaderResourceViewImpl> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<UnorderedAccessViewImpl> createGraphicsObject();
	template<>
	GLOWGRAPHICS_EXPORT UniquePtr<SwapChain> createGraphicsObject();
}

#endif
