#include "InputLayoutImplementation.h"

GLOWE::InputLayoutImpl::InputLayoutImpl(const InputLayoutDesc & arg)
	: desc(arg)
{
}

GLOWE::Hash GLOWE::InputLayoutImpl::getHash() const
{
	return desc.getHash();
}
