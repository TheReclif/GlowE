#pragma once
#ifndef GLOWE_UNIFORM_DEPTHSTENCILIMPLEMENTATION_INCLUDED
#define GLOWE_UNIFORM_DEPTHSTENCILIMPLEMENTATION_INCLUDED

#include "../../GlowSystem/Utility.h"

#include "DepthStencilDesc.h"

namespace GLOWE
{
	class GraphicsDeviceImpl;

	class GLOWGRAPHICS_EXPORT DepthStencilStateImpl
		: public Hashable, public NoCopy
	{
	protected:
		DepthStencilDesc desc;
		Hash hash;
	public:
		DepthStencilStateImpl();
		DepthStencilStateImpl(const DepthStencilDesc& arg);
		virtual ~DepthStencilStateImpl() = default;

		virtual void create(const DepthStencilDesc& arg, const GraphicsDeviceImpl& device) = 0;
		virtual void destroy() = 0;

		virtual bool checkIsValid() const = 0;

		const DepthStencilDesc& getDesc() const;

		virtual Hash getHash() const override;
	};
}

#endif
