#pragma once
#ifndef GLOWE_GRAPHICS_WINDOW_INCLUDED
#define GLOWE_GRAPHICS_WINDOW_INCLUDED

#include "GraphicsDeviceImplementation.h"
#include "SwapChainImplementation.h"

#include "../../GlowWindow/Uniform/Window.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT GraphicsWindow
		: public Window
	{
	private:
		UniquePtr<SwapChain> swapChain;
	protected:
		virtual void onResize(const size_t newHeight, const size_t newWidth) override;
	public:
		void resize(const VideoMode& newSize);
		void resize(const VideoMode& newSize, const WindowMode& newMode);

		void create(GraphicsDeviceImpl& gc, const VideoMode& size, const String& title, const SwapChainDesc& desc, const Int2& position = Int2{ WindowImplementation::AutoCenter, WindowImplementation::AutoCenter }, const WindowMode& wndMode = WindowMode::Default);
		void open(GraphicsDeviceImpl& gc, const VideoMode& size, const String& title, const SwapChainDesc& desc, const Int2& position = Int2{ WindowImplementation::AutoCenter, WindowImplementation::AutoCenter }, const WindowMode& wndMode = WindowMode::Default)
		{
			create(gc, size, title, desc, position, wndMode);
		}

		void destroy();
		void close()
		{
			destroy();
		}

		virtual void display() override;

		SwapChain& getSwapChain();
		const SwapChain& getSwapChain() const;
	};
}

#endif
