#include "Cullable.h"
#include "LightingMgr.h"

GLOWE::Cullable::Cullable()
	: wasRegistered(false)
{
	onCullableActivate();
}

GLOWE::Cullable::~Cullable()
{
	onCullableDeactivate();
}

void GLOWE::Cullable::onCullableActivate()
{
	onCullableDeactivate();
	wasRegistered = true;
	cullableHandle = getInstance<CullingMgr>().registerCullable(this);
}

void GLOWE::Cullable::onCullableDeactivate()
{
	if (wasRegistered)
	{
		getInstance<CullingMgr>().unregisterCullable(cullableHandle);
		wasRegistered = false;
	}
}

void GLOWE::Cullable::performCullTest(const Frustum& frustum)
{
	cullTestResult = performCulling(frustum);
}

bool GLOWE::Cullable::getCullTestResult() const
{
	return cullTestResult;
}

GLOWE::CullingMgr::CullableHandle GLOWE::CullingMgr::registerCullable(Cullable* cullable)
{
	cullables.emplace_back(cullable);

	return std::prev(cullables.end());
}

void GLOWE::CullingMgr::unregisterCullable(const CullableHandle& handle)
{
	cullables.erase(handle);
}

void GLOWE::CullingMgr::performCulling(const Frustum& frustum)
{
	for (auto& cullable : cullables)
	{
		cullable->performCullTest(frustum);
	}
}

GLOWE::ThreadPool::TaskResult<void> GLOWE::CullingMgr::scheduleCulling(ThreadPool& threadPool, const Frustum& frustum)
{
	SharedPtr<Vector<ThreadPool::TaskResult<void>>> results = makeSharedPtr<Vector<ThreadPool::TaskResult<void>>>();
	results->reserve(cullables.size());
	SharedPtr<Mutex> mutex = makeSharedPtr<Mutex>();
	UniqueLock<Mutex> lock(*mutex);

	for (auto& cullable : cullables)
	{
		results->emplace_back(threadPool.addTask([](Cullable* cullable, const Frustum& frustum)
		{
			cullable->performCullTest(frustum);
		}, cullable, std::cref(frustum)));
	}

	return threadPool.addTask([mutex, results]()
	{
		Vector<ThreadPool::TaskResult<void>>& res = *results;

		UniqueLock<Mutex> lock(*mutex);
		for (const auto& x : res)
		{
			x.wait();
		}
	});
}

GLOWE::ThreadPool::TaskResult<void> GLOWE::CullingMgr::scheduleCullingAndLightCulling(ThreadPool& threadPool, const Frustum& frustum)
{
	SharedPtr<Vector<ThreadPool::TaskResult<void>>> results = makeSharedPtr<Vector<ThreadPool::TaskResult<void>>>();
	results->reserve(cullables.size());
	SharedPtr<Mutex> mutex = makeSharedPtr<Mutex>();
	UniqueLock<Mutex> lock(*mutex);

	ThreadPool::SharedTaskResult<void> lightCullResult = getInstance<LightingMgr>().scheduleLightsCulling(threadPool, frustum);

	for (auto& cullable : cullables)
	{
		results->emplace_back(threadPool.addTask([lightCullResult](Cullable* cullable, const Frustum& frustum)
		{
			lightCullResult.wait();
			cullable->performCullTest(frustum);
		}, cullable, std::cref(frustum)));
	}

	return threadPool.addTask([mutex, results]()
	{
		Vector<ThreadPool::TaskResult<void>>& res = *results;

		UniqueLock<Mutex> lock(*mutex);
		for (const auto& x : res)
		{
			x.wait();
		}
	});
}
