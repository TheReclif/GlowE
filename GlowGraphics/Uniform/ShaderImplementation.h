#pragma once
#ifndef GLOWE_UNIFORM_SHADERIMPLEMENTATION_INCLUDED
#define GLOWE_UNIFORM_SHADERIMPLEMENTATION_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/String.h"
#include "../../GlowSystem/Hash.h"

#include "InputLayoutDesc.h"

namespace GLOWE
{
	class GraphicsDeviceImpl;

	// Provided code must be either GLSL (for OpenGL) or HLSL (for DirectX).
	// HLSL compilation flags will depend on GLOWE_DEBUG define.
	// Shader compiler will automatically report any warnings and errors to log file.
	class GLOWGRAPHICS_EXPORT ShaderImpl
		: public Hashable, public NoCopy
	{
	public:
		class GLOWGRAPHICS_EXPORT CompilationData
		{
		public:
			String entrypoint; // Default: Main.
			UInt2 version; // Use { 0, 0 } for the best compilation profile.
		public:
			CompilationData();
		};
	protected:
		Hash hash; // Derived classes must change this variable after every shader creation. Hash should be computed from shader's code, regardless its size.
	public:
		ShaderImpl() = default;
		virtual ~ShaderImpl() = default;

		// At the moment of writing, GlowEngine cannot create shader from bytecode because OpenGL and GLSL doesn't support such feature.
		// GlowE's features must be supported by all of the supported graphics APIs, so it's not possible to include this feature.
		virtual void compileFromCode(const String& code, const GraphicsDeviceImpl& device, const CompilationData& compilationData = CompilationData()) = 0; // Not only compiles; it also creates internal shader representation, eg. ID3D11Shader.
		virtual void destroy() = 0;

		virtual bool checkIsCreated() const = 0; // Will fail if the shader is not compiled nor created.

		virtual Hash getHash() const override;
	};

	class GLOWGRAPHICS_EXPORT PixelShaderImpl
		: public ShaderImpl
	{};

	class GLOWGRAPHICS_EXPORT GeometryShaderImpl
		: public ShaderImpl
	{};

	class GLOWGRAPHICS_EXPORT ComputeShaderImpl
		: public ShaderImpl
	{};

	class GLOWGRAPHICS_EXPORT VertexShaderImpl
		: public ShaderImpl
	{
	private:
		InputLayoutDesc instanceLayoutDesc;
	public:
		virtual void destroy() override;

		virtual const InputLayoutDesc& getInstanceInputLayout() const;

		friend class Effect;
	};
}

#endif
