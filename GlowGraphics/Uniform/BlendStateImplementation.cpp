#include "BlendStateImplementation.h"

GLOWE::BlendStateImpl::BlendStateImpl()
	: desc(), hash()
{
}

GLOWE::BlendStateImpl::BlendStateImpl(const BlendDesc & arg)
	: desc(arg), hash(desc.getHash())
{
}

const GLOWE::BlendDesc& GLOWE::BlendStateImpl::getDesc() const
{
	return desc;
}

GLOWE::Hash GLOWE::BlendStateImpl::getHash() const
{
	return hash;
}
