#include "GraphicsFactory.h"

#include "../../GlowSystem/SettingsMgr.h"

#include "../../GlowScripting/CppScriptFoundations.h"

static CppScript::DynamicLibrary dynamicLib;
static bool useEngineRenderBackend = false;

static GLOWE::UniquePtr<GLOWE::GraphicsFactoryImpl> initFactoryD3D11()
{
	// Just in case.
	dynamicLib.unload();

	const char* const dllName = useEngineRenderBackend ? u8"GlowEDirect3D11EngineBackend.dll" : u8"GlowEDirect3D11RenderBackend.dll";

	if (!dynamicLib.load(dllName))
	{
		throw GLOWE::GraphicsFactoryCreationFailedException(GLOWE::String(u8"Unable to load \"") + dllName + "\".");
	}

	const GLOWE::GraphicsFactoryImpl::GetFactoryFunc getFactoryFunc = reinterpret_cast<GLOWE::GraphicsFactoryImpl::GetFactoryFunc>(dynamicLib["createFactory"]);
	if (!getFactoryFunc)
	{
		throw GLOWE::GraphicsFactoryCreationFailedException(u8"DLL does not contain \"createFactory\" function or it cannot be found. The DLL may be corrupted or invalid.");
	}

	return GLOWE::UniquePtr<GLOWE::GraphicsFactoryImpl>(getFactoryFunc());
}

static GLOWE::UniquePtr<GLOWE::GraphicsFactoryImpl> initializeFactory()
{
	using namespace GLOWE;

	auto& settings = getInstance<SettingsMgr>();
	String apiToUse = settings.getSetting(u8"UsedRenderBackend", u8"Graphics", "opengl");
	apiToUse.toLowercase();
	
	if (apiToUse == "direct3d11")
	{
		return initFactoryD3D11();
	}

	throw GraphicsFactoryCreationFailedException(u8"Unrecognised render backend name or fallback didn't work (see previous warnings).");
}

template<>
GLOWE::GraphicsFactoryImpl& GLOWE::OneInstance<GLOWE::GraphicsFactoryImpl>::instance()
{
	static auto& factory = SubsystemsCore::getGlobalCore().registerSubsystem(initializeFactory());
	return factory;
}

GLOWE::GraphicsFactoryCreationFailedException::GraphicsFactoryCreationFailedException(const String& cause)
	: Exception(), info(u8"Unable to create a graphics factory. " + cause)
{
}

const char* GLOWE::GraphicsFactoryCreationFailedException::what() const noexcept
{
	return info.getCharArray();
}

template<>
GLOWE::UniquePtr<GLOWE::BlendStateImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createBlendState();
}

template<>
GLOWE::UniquePtr<GLOWE::BufferImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createBuffer();
}

template<>
GLOWE::UniquePtr<GLOWE::DepthStencilStateImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createDepthStencilState();
}

template<>
GLOWE::UniquePtr<GLOWE::GraphicsDeviceImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createGraphicsDevice();
}

template<>
GLOWE::UniquePtr<GLOWE::InputLayoutImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createInputLayout();
}

template<>
GLOWE::UniquePtr<GLOWE::RasterizerStateImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createRasterizerState();
}

template<>
GLOWE::UniquePtr<GLOWE::RenderTargetImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createRenderTarget();
}

template<>
GLOWE::UniquePtr<GLOWE::SamplerImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createSampler();
}

template<>
GLOWE::UniquePtr<GLOWE::ShaderCollectionImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createShaderCollection();
}

template<>
GLOWE::UniquePtr<GLOWE::VertexShaderImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createVertexShader();
}

template<>
GLOWE::UniquePtr<GLOWE::PixelShaderImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createPixelShader();
}

template<>
GLOWE::UniquePtr<GLOWE::GeometryShaderImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createGeometryShader();
}

template<>
GLOWE::UniquePtr<GLOWE::ComputeShaderImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createComputeShader();
}

template<>
GLOWE::UniquePtr<GLOWE::ShaderReflectImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createShaderReflection();
}

template<>
GLOWE::UniquePtr<GLOWE::TextureImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createTexture();
}

template<>
GLOWE::UniquePtr<GLOWE::ShaderResourceViewImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createShaderResourceView();
}

template<>
GLOWE::UniquePtr<GLOWE::UnorderedAccessViewImpl> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createUnorderedAccessView();
}

template<>
GLOWE::UniquePtr<GLOWE::SwapChain> GLOWE::createGraphicsObject()
{
	return getInstance<GraphicsFactoryImpl>().createSwapChain();
}

void GLOWE::Hidden::enableEngineRenderBackend()
{
	useEngineRenderBackend = true;
}
