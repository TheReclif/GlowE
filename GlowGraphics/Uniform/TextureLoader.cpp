#include "TextureLoader.h"
#include "GraphicsDeviceImplementation.h"
#include "GraphicsFactory.h"

#include <FreeImage.h>

template <class T>
static inline void INPLACESWAP(T& a, T& b)
{
	a ^= b; b ^= a; a ^= b;
}

static BOOL SwapRedBlue32(FIBITMAP* dib)
{
	if (FreeImage_GetImageType(dib) != FIT_BITMAP)
	{
		return FALSE;
	}

	const unsigned bytesperpixel = FreeImage_GetBPP(dib) / 8;
	if (bytesperpixel > 4 || bytesperpixel < 3)
	{
		return FALSE;
	}

	const unsigned height = FreeImage_GetHeight(dib);
	const unsigned pitch = FreeImage_GetPitch(dib);
	const unsigned lineSize = FreeImage_GetLine(dib);

	BYTE* line = FreeImage_GetBits(dib);
	for (unsigned y = 0; y < height; ++y, line += pitch)
	{
		for (BYTE* pixel = line; pixel < line + lineSize; pixel += bytesperpixel)
		{
			INPLACESWAP(pixel[0], pixel[2]);
		}
	}
	
	return TRUE;
}

GLOWE::TextureLoader::TextureLoader()
{
	FreeImage_Initialise(0);
}

GLOWE::TextureLoader::~TextureLoader()
{
	FreeImage_DeInitialise();
}

// TODO: GIF, TIFF, ICO (multipage) support.
// Search: FreeImage_LoadMultiBitmapFromMemory
// This function takes FIBITMAP's ownership. It means that it will destroy bitmap (assuming that the pointer is non-zero).
GLOWE::UniquePtr<GLOWE::TextureImpl> loadTextureFreeImage(FIBITMAP* bitmap, const GLOWE::TextureDesc& texDesc, const GLOWE::GraphicsDeviceImpl& device)
{
	using namespace GLOWE;

	UniquePtr<TextureImpl> result(createGraphicsObject<TextureImpl>());

	if (bitmap)
	{
		TextureDesc desc = texDesc;
		Format texFormat;

		// FREE_IMAGE_TYPE
		FREE_IMAGE_COLOR_TYPE colType;
		FREE_IMAGE_TYPE imageType;
		unsigned int bpp;
		unsigned int width;
		unsigned int height;
		unsigned int scanWidth;

		const auto updateTexInfo = [&]()
		{
			colType = FreeImage_GetColorType(bitmap);
			imageType = FreeImage_GetImageType(bitmap);
			bpp = FreeImage_GetBPP(bitmap);
			width = FreeImage_GetWidth(bitmap);
			height = FreeImage_GetHeight(bitmap);
			scanWidth = FreeImage_GetPitch(bitmap);
		};
		updateTexInfo();

		switch (colType)
		{
		case FREE_IMAGE_COLOR_TYPE::FIC_RGBALPHA:
			break;
		case FREE_IMAGE_COLOR_TYPE::FIC_MINISBLACK:
		//case FREE_IMAGE_COLOR_TYPE::FIC_MINISWHITE: // Only minisblack may be valid.
		{
			FIBITMAP* temp = FreeImage_ConvertToStandardType(bitmap);

			if (temp)
			{
				FreeImage_Unload(bitmap);
				bitmap = temp;
				updateTexInfo();
			}
		}
			break;
		case FREE_IMAGE_COLOR_TYPE::FIC_CMYK:
			WarningThrow(false, "Found CMYK in the loaded image which will cause a conversion - use RGBA instead");
		case FREE_IMAGE_COLOR_TYPE::FIC_PALETTE: // Fallthrough.
			if (imageType == FREE_IMAGE_TYPE::FIT_BITMAP)
			{
				FIBITMAP* temp = FreeImage_ConvertTo32Bits(bitmap);

				if (temp)
				{
					FreeImage_Unload(bitmap);
					bitmap = temp;
					updateTexInfo();
				}
			}
			break;
		case FREE_IMAGE_COLOR_TYPE::FIC_RGB:
		//default:
		{
			// We need to add an alpha channel.
			if (imageType == FREE_IMAGE_TYPE::FIT_BITMAP && ((bpp / 8) == 3))
			{
				FIBITMAP* temp = FreeImage_ConvertTo32Bits(bitmap);

				if (temp)
				{
					FreeImage_Unload(bitmap);
					bitmap = temp;
					updateTexInfo();
				}
			}
		}
			break;
		}

		switch (imageType)
		{
		case FREE_IMAGE_TYPE::FIT_BITMAP:
			texFormat.type = Format::Type::UnsignedByte;
			texFormat.elemsCount = bpp / 8;
			break;
		case FREE_IMAGE_TYPE::FIT_FLOAT:
			texFormat.type = Format::Type::Float;
			texFormat.elemsCount = 1;
			break;
		case FREE_IMAGE_TYPE::FIT_INT16:
			texFormat.type = Format::Type::Short;
			texFormat.elemsCount = 1;
			break;
		case FREE_IMAGE_TYPE::FIT_UINT16:
			texFormat.type = Format::Type::UnsignedShort;
			texFormat.elemsCount = 1;
			break;
		case FREE_IMAGE_TYPE::FIT_INT32:
			texFormat.type = Format::Type::Int;
			texFormat.elemsCount = 1;
			break;
		case FREE_IMAGE_TYPE::FIT_UINT32:
			texFormat.type = Format::Type::UnsignedInt;
			texFormat.elemsCount = 1;
			break;
		case FREE_IMAGE_TYPE::FIT_RGB16:
			texFormat.type = Format::Type::UnsignedShort;
			texFormat.elemsCount = 3;
			break;
		case FREE_IMAGE_TYPE::FIT_RGBA16:
			texFormat.type = Format::Type::UnsignedShort;
			texFormat.elemsCount = 4;
			break;
		case FREE_IMAGE_TYPE::FIT_RGBAF:
			texFormat.type = Format::Type::Float;
			texFormat.elemsCount = 4;
			break;
		case FREE_IMAGE_TYPE::FIT_RGBF:
			texFormat.type = Format::Type::Float;
			texFormat.elemsCount = 3;
			break;
		default:
			WarningThrow(false, u8"Image type not supported.");
			return std::move(result);
		}

		Vector<BYTE> bytes(height * scanWidth);

		if (device.getAPIInfo().usedAPI == GraphicsAPIInfo::GraphicsAPI::DirectX)
		{
			SwapRedBlue32(bitmap);
		}

		FreeImage_ConvertToRawBits(bytes.data(), bitmap, scanWidth, bpp, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, TRUE);

		FreeImage_Unload(bitmap);

		if (desc.format.elemsCount == 0)
		{
			desc.format = texFormat;
		}

		desc.width = width;
		desc.height = height;

		result->create(desc, device, { bytes.data() });
	}

	return result;
}

GLOWE::UniquePtr<GLOWE::TextureImpl> GLOWE::TextureLoader::loadTexture(const String & filename, const TextureDesc& texDesc, const GraphicsDeviceImpl& device) const
{	
	FIBITMAP* bitmap = FreeImage_Load(FreeImage_GetFileType(filename.getCharArray()), filename.getCharArray());

	if (!bitmap)
	{
		WarningThrow(true, u8"Cannot load texture from file: " + filename + u8".");
	}
	
	return loadTextureFreeImage(bitmap, texDesc, device);
}

GLOWE::UniquePtr<GLOWE::TextureImpl> GLOWE::TextureLoader::loadTextureFromPackage(const String& filename, Package& package, const TextureDesc& texDesc, const GraphicsDeviceImpl& device) const
{
	String buffer;
	package.readFile(filename, buffer);

	if (buffer.getSize() == 0)
	{
		return nullptr;
	}

	FIMEMORY* mem = FreeImage_OpenMemory((BYTE*)buffer.getCharArray(), buffer.getSize());
	FIBITMAP* bitmap = FreeImage_LoadFromMemory(FreeImage_GetFileTypeFromMemory(mem), mem);
	FreeImage_CloseMemory(mem);

	if (!bitmap)
	{
		WarningThrow(true, u8"Cannot load texture from package (filename in package): " + filename + u8".");
	}

	return loadTextureFreeImage(bitmap, texDesc, device);
}

GLOWE::UniquePtr<GLOWE::TextureImpl> GLOWE::TextureLoader::loadTextureFromMemory(const void* mem, const std::size_t size, const TextureDesc& texDesc, const GraphicsDeviceImpl& device) const
{
	if (size == 0)
	{
		return nullptr;
	}

	FIMEMORY* memory = FreeImage_OpenMemory(reinterpret_cast<BYTE*>(const_cast<void* const>(mem)), size);
	FIBITMAP* bitmap = FreeImage_LoadFromMemory(FreeImage_GetFileTypeFromMemory(memory), memory);
	FreeImage_CloseMemory(memory);

	if (!bitmap)
	{
		WarningThrow(true, u8"Cannot load texture from memory.");
	}

	return loadTextureFreeImage(bitmap, texDesc, device);
}

void GLOWE::TextureLoader::saveImageToFile(const String& filename, const void* const data, const unsigned int width, const unsigned int height, const Format& pixelFormat, const FileType fileType) const
{
	if (filename.isEmpty())
	{
		throw Exception("Filename can't be empty");
	}

	if (data == nullptr || width == 0 || height == 0)
	{
		throw Exception("Invalid data: data pointer is null or the image contains 0 pixels");
	}

	if (pixelFormat.elemsCount == 0 || pixelFormat.type != Format::Type::UnsignedByte)
	{
		throw Exception("Invalid Format: must be UnsignedByte and elementsCount must be greater than 0");
	}

	FREE_IMAGE_FORMAT format;
	switch (fileType)
	{
	case FileType::BMP:
		format = FREE_IMAGE_FORMAT::FIF_BMP;
		break;
	case FileType::EXR:
		format = FREE_IMAGE_FORMAT::FIF_EXR;
		break;
	case FileType::HDR:
		format = FREE_IMAGE_FORMAT::FIF_HDR;
		break;
	case FileType::ICO:
		format = FREE_IMAGE_FORMAT::FIF_ICO;
		break;
	case FileType::JP2:
		format = FREE_IMAGE_FORMAT::FIF_JP2;
		break;
	case FileType::JPG:
		format = FREE_IMAGE_FORMAT::FIF_JPEG;
		break;
	case FileType::PNG:
		format = FREE_IMAGE_FORMAT::FIF_PNG;
		break;
	case FileType::TARGA:
		format = FREE_IMAGE_FORMAT::FIF_TARGA;
		break;
	case FileType::TIFF:
		format = FREE_IMAGE_FORMAT::FIF_TIFF;
		break;
	default:
		throw Exception("Invalid output image type");
	}
	
	unsigned int bpp = pixelFormat.getSize() * 8;
	FIBITMAP* bitmap = FreeImage_ConvertFromRawBits(reinterpret_cast<BYTE*>(const_cast<void* const>(data)), width, height, width * (bpp / 8), bpp, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, TRUE);

	if (!bitmap)
	{
		throw Exception("Unable to save imave: FreeImage_ConvertFromRawBits failed");
	}

	const auto res = FreeImage_Save(format, bitmap, filename.getCharArray());
	FreeImage_Unload(bitmap);
	if (res != TRUE)
	{
		throw Exception("Unable to save image: FreeImage_Save failed");
	}
}
