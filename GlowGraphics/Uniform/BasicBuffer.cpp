#include "BasicBuffer.h"

GLOWE::BasicBuffer::BasicBuffer(const StructureLayout & arg, const std::size_t howManyElems)
	: layout(arg), memory(howManyElems * arg.getSize())
{
}

void GLOWE::BasicBuffer::setLayout(const StructureLayout & arg)
{
	layout = arg;
}

void GLOWE::BasicBuffer::resize(const std::size_t newElemsCount)
{
	memory.resize(newElemsCount * layout.getSize());
}

void GLOWE::BasicBuffer::modifyElement(const unsigned int elem, const unsigned int elemInLayout, const void* data, const unsigned int dataSize)
{
	if (layout.getElemSize(elemInLayout) < dataSize)
	{
		WarningThrow(false, String(u8"Type size exceeding element size in StructureLayout."));
		return;
	}

	void* temp = layout.getAddressOfElement(elemInLayout, memory.data() + (layout.getSize() * elem));
	std::memcpy(temp, data, dataSize);
}

void GLOWE::BasicBuffer::modifyElements(const unsigned int elem, const unsigned int elemInLayout, const void** arr, const unsigned int dataSize)
{
	if (layout.getElemSize(elemInLayout) < dataSize)
	{
		WarningThrow(false, String(u8"Type size exceeding element size in StructureLayout."));
		throw Exception();
	}

	void* temp = memory.data() + layout.getOffsetToElem(elemInLayout);
	const auto elemSize = layout.getSize();

	for (unsigned int x = 0; x < (memory.size() / elemSize); ++x)
	{
		std::memcpy(((char*)(temp)+(x * elemSize)), arr[x], dataSize);
	}
}

void GLOWE::BasicBuffer::setContents(const void * buff)
{
	std::memcpy(memory.data(), buff, memory.size());
}

const GLOWE::StructureLayout & GLOWE::BasicBuffer::getLayout(void) const
{
	return layout;
}

std::size_t GLOWE::BasicBuffer::getBufferSize(void) const
{
	return memory.size();
}

unsigned int GLOWE::BasicBuffer::getElemsCount(void) const
{
	return getBufferSize() / layout.getSize();
}

const void * GLOWE::BasicBuffer::getData(void) const
{
	return (const void*)(memory.data());
}

void GLOWE::BasicBuffer::serialize(LoadSaveHandle& handle) const
{
	handle << layout;
	const UInt32 size = memory.size();
	handle << size;
	handle.write(memory.data(), size);
}

void GLOWE::BasicBuffer::deserialize(LoadSaveHandle& handle)
{
	handle >> layout;
	UInt32 size;
	handle >> size;
	memory.resize(size);
	handle.read(memory.data(), size);
}
