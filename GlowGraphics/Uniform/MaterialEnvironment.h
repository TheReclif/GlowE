#pragma once
#ifndef GLOWE_GRAPHICS_UNIFORM_MATERIALENVIRONMENT_INCLUDED
#define GLOWE_GRAPHICS_UNIFORM_MATERIALENVIRONMENT_INCLUDED

#include "glowgraphics_export.h"

#include "../../GlowSystem/Hash.h"
#include "../../GlowSystem/MemoryMgr.h"
#include "../../GlowSystem/Error.h"

namespace GLOWE
{
	class MaterialEnvironment
	{
	private:
		// unsigned int is a version counter.
		UnorderedMap<Hash, Pair<Vector<char>, unsigned int>> variables;
	public:
		GLOWGRAPHICS_EXPORT void setVariable(const Hash& name, const void* data, const unsigned int size);
		template<class T>
		void setVariable(const Hash& name, const T& arg);
		template<class T>
		T getVariable(const Hash& name) const;
		template<class T>
		const T& getVariableByConstRef(const Hash& name) const;

		friend class Material;
	};

	template<class T>
	inline void MaterialEnvironment::setVariable(const Hash& name, const T& arg)
	{
		auto& temp = variables[name];
		if (sizeof(T) > temp.first.size())
		{
			temp.first.resize(sizeof(T));
			//WarningThrow(false, "Resizing a variable in MaterialEnvironment.");
		}

		(*reinterpret_cast<T*>(temp.first.data())) = arg;
		++temp.second;
	}

	template<class T>
	inline T MaterialEnvironment::getVariable(const Hash& name) const
	{
		auto it = variables.find(name);
		if (it != variables.end() && sizeof(T) <= it->second.first.size())
		{
			return T(*reinterpret_cast<const T*>(it->second.first.data()));
		}
		return T();
	}

	template<class T>
	inline const T& MaterialEnvironment::getVariableByConstRef(const Hash& name) const
	{
		const auto& a = variables.at(name);

		if (sizeof(T) <= a.first.size())
		{
			return *(reinterpret_cast<const T*>(a.first.data()));
		}

		WarningThrow(false, "Variable's byte array not big enough. sizeof(T) = " + toString(sizeof(T)) + " | Size of byte array = " + toString(a.first.size()));
		throw Exception("Variable's byte array not big enough.");
	}
}

#endif
