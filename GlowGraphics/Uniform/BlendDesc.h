#pragma once
#ifndef GLOWE_UNIFORM_BLENDDESC_INCLUDED
#define GLOWE_UNIFORM_BLENDDESC_INCLUDED

#include "glowgraphics_export.h"

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/SerializableDataCreator.h"
#include "../../GlowSystem/Hash.h"
#include "../../GlowSystem/FileIO.h"
#include "../../GlowSystem/Color.h"
#include "../../GlowSystem/LoadSaveHandle.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT BlendDesc
		: public BinarySerializable, public Hashable
	{
	public:
		class GLOWGRAPHICS_EXPORT RenderTargetBlendDesc
		{
		public:
			enum class BlendOption
			{
				Zero,
				One,
				SrcColor,
				InvSrcColor,
				SrcAlpha,
				InvSrcAlpha,
				DestAlpha,
				InvDestAlpha,
				DestColor,
				InvDestColor,
				SrcAlphaClamp,
				BlendFactor,
				InvBlendFactor,
				Src1Color,
				InvSrc1Color,
				Src1Alpha,
				InvSrc1Alpha
			};

			enum class BlendOperation
			{
				Add,
				Sub,
				RevSub, // Reverse Subtract
				Min,
				Max
			};
		public:
			bool enableBlend;
			BlendOption srcBlend, destBlend;
			BlendOperation blendOperation;
			BlendOption srcBlendAlpha, destBlendAlpha;
			BlendOperation blendOperationAlpha;
			UInt8 writeMask;
		public:
			RenderTargetBlendDesc();
		};
	public:
		bool enableAlphaToCoverage;
		bool enableIndependentBlend;
		RenderTargetBlendDesc renderTargetDescs[8];
		Color blendFactor;
		unsigned int sampleMask;
	public:
		BlendDesc();

		virtual void serialize(LoadSaveHandle& handle) const override;
		virtual void deserialize(LoadSaveHandle& handle) override;

		virtual Hash getHash() const override;
	};
}

#endif
