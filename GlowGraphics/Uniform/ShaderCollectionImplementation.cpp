#include "ShaderCollectionImplementation.h"

bool GLOWE::ShaderCollectionImpl::checkIsValid() const
{
	return isLinked;
}

GLOWE::Hash GLOWE::ShaderCollectionDesc::getHash() const
{
	void* ptrs[4] = { vertexShader.get(), pixelShader.get(), geometryShader.get(), computeShader.get() };

	return Hash((const char*)ptrs, sizeof(ptrs));
}
