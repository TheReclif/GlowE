#include "BlendDesc.h"

GLOWE::BlendDesc::RenderTargetBlendDesc::RenderTargetBlendDesc()
	: enableBlend(false), srcBlend(BlendOption::One), destBlend(BlendOption::Zero), blendOperation(BlendOperation::Add), srcBlendAlpha(BlendOption::One), destBlendAlpha(BlendOption::Zero), blendOperationAlpha(BlendOperation::Add), writeMask(0xF)
{
}

GLOWE::BlendDesc::BlendDesc()
	: enableAlphaToCoverage(false), enableIndependentBlend(false), renderTargetDescs(), blendFactor(), sampleMask(0xffffffff)
{
}

void GLOWE::BlendDesc::serialize(LoadSaveHandle& handle) const
{
	handle << enableAlphaToCoverage << enableIndependentBlend << blendFactor << sampleMask;

	for (unsigned int x = 0; x < 8; ++x)
	{
		handle << renderTargetDescs[x].enableBlend << renderTargetDescs[x].srcBlend << renderTargetDescs[x].destBlend << renderTargetDescs[x].blendOperation << renderTargetDescs[x].srcBlendAlpha << renderTargetDescs[x].destBlendAlpha << renderTargetDescs[x].blendOperationAlpha << renderTargetDescs[x].writeMask;
	}
}

void GLOWE::BlendDesc::deserialize(LoadSaveHandle& handle)
{
	handle >> enableAlphaToCoverage >> enableIndependentBlend >> blendFactor >> sampleMask;

	for (unsigned int x = 0; x < 8; ++x)
	{
		handle >> renderTargetDescs[x].enableBlend >> renderTargetDescs[x].srcBlend >> renderTargetDescs[x].destBlend >> renderTargetDescs[x].blendOperation >> renderTargetDescs[x].srcBlendAlpha >> renderTargetDescs[x].destBlendAlpha >> renderTargetDescs[x].blendOperationAlpha >> renderTargetDescs[x].writeMask;
	}
}

GLOWE::Hash GLOWE::BlendDesc::getHash() const
{
	return Hash((const char*)this, sizeof(*this));
}
