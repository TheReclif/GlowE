#pragma once
#ifndef GLOWE_UNIFORM_SHADERCOLLECTIONIMPLEMENTATION_INCLUDED
#define GLOWE_UNIFORM_SHADERCOLLECTIONIMPLEMENTATION_INCLUDED

#include "../../GlowSystem/Utility.h"

#include "ShaderImplementation.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT ShaderCollectionDesc
		: public Hashable
	{
	public:
		SharedPtr<VertexShaderImpl> vertexShader;
		SharedPtr<PixelShaderImpl> pixelShader;
		SharedPtr<GeometryShaderImpl> geometryShader;
		SharedPtr<ComputeShaderImpl> computeShader;
	public:
		virtual Hash getHash() const override;
	};

	class GLOWGRAPHICS_EXPORT ShaderCollectionImpl
		: public NoCopy
	{
	protected:
		bool isLinked;
	public:
		ShaderCollectionImpl() = default;
		virtual ~ShaderCollectionImpl() = default;

		virtual void create(const ShaderCollectionDesc& desc) = 0; // Only for OpenGL, in DirectX 11 it just calls AddRef.
		virtual void destroy() = 0;

		virtual const ShaderCollectionDesc& getDesc() const = 0;

		bool checkIsValid() const;
	};
}

#endif
