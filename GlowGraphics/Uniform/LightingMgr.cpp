#include "LightingMgr.h"
#include "../../GlowMath/Cone.h"
#include "../../GlowMath/Sphere.h"
#include "../../GlowMath/AxisAlignedBoundingBox.h"

GLOWE::LightingMgr::PointLightHandle GLOWE::LightingMgr::addPointLight(const PointLight& light)
{
	DefaultLockGuard lock(pointLightsMutex);
	pointLights.emplace_back(light, true);
	return std::prev(pointLights.end());
}

GLOWE::LightingMgr::DirectionalLightHandle GLOWE::LightingMgr::addDirectionalLight(const DirectionalLight& light)
{
	DefaultLockGuard lock(dirLightsMutex);
	return dirLights.emplace(light);
}

GLOWE::LightingMgr::SpotLightHandle GLOWE::LightingMgr::addSpotLight(const SpotLight& light)
{
	DefaultLockGuard lock(spotLightsMutex);
	spotLights.emplace_back(light, true);
	return std::prev(spotLights.end());
}

void GLOWE::LightingMgr::removePointLight(const PointLightHandle& it)
{
	DefaultLockGuard lock(pointLightsMutex);
	pointLights.erase(it);
}

void GLOWE::LightingMgr::removeDirectionalLight(const DirectionalLightHandle& it)
{
	DefaultLockGuard lock(dirLightsMutex);
	dirLights.erase(it);
}

void GLOWE::LightingMgr::removeSpotLight(const SpotLightHandle& it)
{
	DefaultLockGuard lock(spotLightsMutex);
	spotLights.erase(it);
}

GLOWE::LightingMgr::PointLightHandle GLOWE::LightingMgr::updatePointLight(const PointLightHandle& lightHandle, const PointLight& lightInfo)
{
	DefaultLockGuard lock(pointLightsMutex);
	pointLights.erase(lightHandle);
	pointLights.emplace_back(lightInfo, true);
	return std::prev(pointLights.end());
}

GLOWE::LightingMgr::DirectionalLightHandle GLOWE::LightingMgr::updateDirectionalLight(const DirectionalLightHandle& lightHandle, const DirectionalLight& lightInfo)
{
	DefaultLockGuard lock(dirLightsMutex);
	dirLights.erase(lightHandle);
	return dirLights.emplace(lightInfo);
}

GLOWE::LightingMgr::SpotLightHandle GLOWE::LightingMgr::updateSpotLight(const SpotLightHandle& lightHandle, const SpotLight& lightInfo)
{
	DefaultLockGuard lock(spotLightsMutex);
	spotLights.erase(lightHandle);
	spotLights.emplace_back(lightInfo, true);
	return std::prev(spotLights.end());
}

GLOWE::LightingMgr::RegisteredLightHandle GLOWE::LightingMgr::registerLight(LightBase* const light)
{
	DefaultLockGuard lock(registeredLightsMutex);
	registeredLights.emplace_back(light);
	return std::prev(registeredLights.end());
}

void GLOWE::LightingMgr::unregisterLight(const RegisteredLightHandle& arg)
{
	DefaultLockGuard lock(registeredLightsMutex);
	registeredLights.erase(arg);
}

GLOWE::Vector<const GLOWE::PointLight*> GLOWE::LightingMgr::getBestPointLights(const AABB& aabb) const
{
	Vector<const PointLight*> result;
	result.reserve(8);
	Multimap<float, const PointLight*> sortedLights;
	
	for (const auto& light : pointLights)
	{
		if (light.second)
		{
			const Vector3F vecToSphere = aabb.getVectorToSphere(Sphere(light.first.position, light.first.range));
			const float distance = vecToSphere.getMagnitudeSquared();

			if (distance <= (light.first.range * light.first.range))
			{
				sortedLights.emplace(distance * (1.0f / (light.first.importance / 1000.0f)), &light.first);
			}
		}
	}

	for (const auto& x : sortedLights)
	{
		if (result.size() == 8)
		{
			break;
		}
		result.push_back(x.second);
	}

	return result;
}

GLOWE::Vector<const GLOWE::DirectionalLight*> GLOWE::LightingMgr::getBestDirLights() const
{
	Vector<const DirectionalLight*> result;
	result.reserve(std::min(8U, static_cast<unsigned int>(dirLights.size())));

	for (const auto& light : dirLights)
	{
		if (result.size() == 8)
		{
			break;
		}

		result.push_back(&light);
	}

	return result;
}

GLOWE::Vector<const GLOWE::SpotLight*> GLOWE::LightingMgr::getBestSpotLights(const AABB& aabb) const
{
	Vector<const SpotLight*> result;
	result.reserve(8);
	Multimap<float, const SpotLight*> sortedLights;

	for (const auto& light : spotLights)
	{
		if (light.second)
		{
			// First we calculate the closest point on the line, then we clamp this point to AABB and then it is our best point.
			// But we test the distance from the closest point to AABB from light position - less calculations and more error-prone.

			const Vector3F start = Vector3F(light.first.position);
			Vector3F fromLightToPosVec = aabb.getClosestPoint(light.first.position) - start;
			const float distance = fromLightToPosVec.getMagnitudeSquared();

			// Changed from MathHelper::Epsilon.
			if (distance <= 0.01f)
			{
				sortedLights.emplace(0.0f, &light.first);
			}
			else
			if (distance <= (light.first.range * light.first.range))
			{
				/*
				const Vector3F end = start + (Vector3F(light.first.direction) * light.first.range), ab = end - start; // Closest to center, and then closest to AABB.
				const float t = MathHelper::clamp((Vector3F(aabb.center) - start).scalarProduct(ab) / ab.getScalarProduct(), 0.25f, 1.0f);
				const Vector3F pointClosest = aabb.getClosestPoint(start + (ab * t));

				fromLightToPosVec = (pointClosest - start).normalize(); // Normalize so that we can calculate dot product.

				const float dotProd = MathHelper::clamp(fromLightToPosVec.scalarProduct(light.first.direction), -1.0f, 1.0f);

				if (dotProd >= light.first.outerCutOff)
				{
					sortedLights.emplace(distance * (1.0f / (light.first.importance / 1000.0f)), &light.first);
				}
				*/

				/*
				const Vector3F end = start + (Vector3F(light.first.direction) * light.first.range), ab = end - start; // Closest to center, and then closest to AABB.
				constexpr unsigned int iterations = 7;
				// Accurate enough, but expensive.
				// TODO: Rewrite somehow.
				for (unsigned int i = 1; i <= iterations; ++i)
				{
					const float t = static_cast<float>(i) / static_cast<float>(iterations);
					const Vector3F pointClosest = aabb.getClosestPoint(start + (ab * t));

					fromLightToPosVec = (pointClosest - start).normalize(); // Normalize so that we can calculate dot product.

					const float dotProd = MathHelper::clamp(fromLightToPosVec.scalarProduct(light.first.direction), -1.0f, 1.0f);

					if (dotProd >= light.first.outerCutOff)
					{
						sortedLights.emplace(distance * (1.0f / (light.first.importance / 1000.0f)), &light.first);
						break;
					}
				}
				*/

				const Vector3F end = start + (Vector3F(light.first.direction) * light.first.range), ab = end - start; // Closest to center, and then closest to AABB.
				float t = 0.125f;
				Vector3F pointClosest = aabb.getClosestPoint(start + (ab * t));

				fromLightToPosVec = (pointClosest - start).normalize(); // Normalize so that we can calculate dot product.

				float dotProd = clamp(fromLightToPosVec.scalarProduct(light.first.direction), -1.0f, 1.0f);

				if (dotProd >= light.first.outerCutOff)
				{
					sortedLights.emplace(distance * (1.0f / (light.first.importance / 1000.0f)), &light.first);
				}
				else
				{
					t = clamp((Vector3F(aabb.center) - start).scalarProduct(ab) / ab.getScalarProduct(), 0.25f, 1.0f);
					pointClosest = aabb.getClosestPoint(start + (ab * t));

					fromLightToPosVec = (pointClosest - start).normalize(); // Normalize so that we can calculate dot product.

					dotProd = clamp(fromLightToPosVec.scalarProduct(light.first.direction), -1.0f, 1.0f);

					if (dotProd >= light.first.outerCutOff)
					{
						sortedLights.emplace(distance * (1.0f / (light.first.importance / 1000.0f)), &light.first);
					}
				}
			}
		}
	}

	for (const auto& x : sortedLights)
	{
		if (result.size() == 8)
		{
			break;
		}
		result.push_back(x.second);
	}

	return result;
}

void GLOWE::LightingMgr::performLightsCulling(const Frustum& frustum)
{
	for (auto light : registeredLights)
	{
		light->updateLight();
	}

	for (auto& pointLight : pointLights)
	{
		Sphere sphere(pointLight.first.position, pointLight.first.range);

		pointLight.second = sphere.isInsideFrustum(frustum);
	}

	for (auto& spotLight : spotLights)
	{
		Cone cone(spotLight.first.position, spotLight.first.direction, spotLight.first.range, spotLight.first.outerCutOff);

		spotLight.second = cone.isInsideFrustum(frustum);
	}
}

GLOWE::ThreadPool::TaskResult<void> GLOWE::LightingMgr::scheduleLightsCulling(ThreadPool& threadPool, const Frustum& frustum)
{
	// We keep in mind the fact that lights culling one light per job is somewhat inefficient for a small amount of lights.
	if ((pointLights.size() + spotLights.size()) <= 16)
	{
		return threadPool.addTask([this](const Frustum& frustum)
		{
			performLightsCulling(frustum);
		}, std::cref(frustum));
	}

	auto results = makeTrivialUniquePtr<Vector<ThreadPool::TaskResult<void>>>();
	results->reserve(registeredLights.size() + pointLights.size() + spotLights.size() + 1);

	for (auto light : registeredLights)
	{
		results->emplace_back(threadPool.addTask([light]() { light->updateLight(); }));
	}

	results->emplace_back(threadPool.addTask([](const Vector<ThreadPool::TaskResult<void>>* vecPtr, const unsigned int howMany)
	{
		for (unsigned int x = 0; x < howMany; ++x)
		{
			(*vecPtr)[x].wait();
		}
	}, results.get(), static_cast<unsigned int>(registeredLights.size())));

	ThreadPool::TaskResult<void>& lightUpdateResult = results->back();

	for (auto& pointLight : pointLights)
	{
		results->emplace_back(threadPool.addTask([&pointLight, &lightUpdateResult](const Frustum& frustum)
		{
			lightUpdateResult.wait();
			Sphere sphere(pointLight.first.position, pointLight.first.range);

			pointLight.second = sphere.isInsideFrustum(frustum);
		}, std::cref(frustum)));
	}

	for (auto& spotLight : spotLights)
	{
		results->emplace_back(threadPool.addTask([&spotLight, &lightUpdateResult](const Frustum& frustum)
		{
			lightUpdateResult.wait();
			Cone cone(spotLight.first.position, spotLight.first.direction, spotLight.first.range, spotLight.first.outerCutOffAngle);

			spotLight.second = cone.isInsideFrustum(frustum);
		}, std::cref(frustum)));
	}

	return threadPool.addTask([](Vector<ThreadPool::TaskResult<void>>* r)
	{
		UniquePtr<Vector<ThreadPool::TaskResult<void>>, UniqueDeleter> results(r);

		for (const auto& x : *results)
		{
			x.wait();
		}
	}, results.release());
}
