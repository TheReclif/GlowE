#include "DepthStencilStateImplementation.h"

GLOWE::DepthStencilStateImpl::DepthStencilStateImpl()
	: desc(), hash()
{
}

GLOWE::DepthStencilStateImpl::DepthStencilStateImpl(const DepthStencilDesc & arg)
	: desc(arg), hash(desc.getHash())
{
}

const GLOWE::DepthStencilDesc & GLOWE::DepthStencilStateImpl::getDesc() const
{
	return desc;
}

GLOWE::Hash GLOWE::DepthStencilStateImpl::getHash() const
{
	return hash;
}
