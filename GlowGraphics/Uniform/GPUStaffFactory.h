#pragma once
#ifndef GLOWE_UNIFORM_CRINGE_GPUSTAFFFACTORY_INCLUDED
#define GLOWE_UNIFORM_CRINGE_GPUSTAFFFACTORY_INCLUDED

#include "../../GlowSystem/Hash.h"
#include "../../GlowSystem/MemoryMgr.h"

#include "GraphicsFactory.h"

// TODO: Reevaluate how important is this now that we have a working GraphicsFactory.
namespace GLOWE
{
	template<class T>
	class GPUStuffFactory
		: public Subsystem
	{
	private:
		Map<Hash, WeakPtr<T>> objects;

		template<class... Args>
		Hash unpackHash(const Hashable& hashable, Args&&...)
		{
			return hashable.getHash();
		}
	public:
		template<class... Args>
		SharedPtr<T> create(Args&&... args)
		{
			Hash hash = unpackHash(args...);

			WeakPtr<T>& temp = objects[hash];
			SharedPtr<T> result = temp.lock();

			if (!result)
			{
				result = createGraphicsObject<T>();
				result->create(args...);
				temp = result;
			}

			return result;
		}
	};
}

#endif
