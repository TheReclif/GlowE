#pragma once
#ifndef GLOWE_UNIFORM_SRVIMPLEMENTATION_INCLUDED
#define GLOWE_UNIFORM_SRVIMPLEMENTATION_INCLUDED

#include "glowgraphics_export.h"

#include "../../GlowSystem/Utility.h"

namespace GLOWE
{
	class BufferImpl;
	class TextureImpl;
	class GraphicsDeviceImpl;

	class GLOWGRAPHICS_EXPORT ShaderResourceViewImpl
		: public NoCopy
	{
	public:
		ShaderResourceViewImpl() = default;
		virtual ~ShaderResourceViewImpl() = default;

		virtual void create(const BufferImpl& buff, const GraphicsDeviceImpl& device) = 0;
		virtual void create(const TextureImpl& tex, const GraphicsDeviceImpl& device) = 0;
		virtual void destroy() = 0;

		virtual bool checkIsValid() const = 0;
	};
}

#endif
