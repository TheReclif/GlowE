#pragma once
#ifndef GLOWE_UNIFORM_RASTERIZERDESC_INCLUDED
#define GLOWE_UNIFORM_RASTERIZERDESC_INCLUDED

#include "glowgraphics_export.h"

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/Hash.h"
#include "../../GlowSystem/SerializableDataCreator.h"
#include "../../GlowSystem/FileIO.h"
#include "../../GlowSystem/LoadSaveHandle.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT RasterizerDesc
		: public BinarySerializable, public Hashable
	{
	public:
		enum class CullingMode
		{
			None,
			Front,
			Back
		};

		enum class FillMode
		{
			Solid,
			Wireframe
		};
	public:
		CullingMode cullingMode;
		FillMode fillMode;

		bool frontCounterClockwise; // true - a triangle will be considered front-facing if its vertices are counter-clockwise on the render target and considered back-facing if they are clockwise. false - opposite.
		Int32 depthBias;
		float depthBiasClamp;
		float slopeScaledDepthBias;
		bool enableDepthClip;
		bool enableScissor;
		bool enableMultisample;
		bool enableAntialiasedLines;
	public:
		RasterizerDesc();

		virtual void serialize(LoadSaveHandle& handle) const override;
		virtual void deserialize(LoadSaveHandle& handle) override;

		virtual Hash getHash() const override;
	};
}

#endif
