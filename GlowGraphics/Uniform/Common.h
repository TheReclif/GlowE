#pragma once
#ifndef GLOWE_UNIFORM_COMMONS_INCLUDED
#define GLOWE_UNIFORM_COMMONS_INCLUDED

#include "glowgraphics_export.h"

namespace GLOWE
{
	enum class ComparisonFunction
	{
		Never,
		Less,
		Equal,
		LessEqual,
		Greater,
		GreaterEqual,
		NotEqual,
		Always
	};

	enum class Usage
	{
		/// @brief Read and write by the GPU.
		Default,
		/// @brief Only read by the GPU. No access from the CPU.
		Immutable,
		/// @brief Read only by the GPU and write only by the CPU.
		Dynamic,
		/// @brief Full data transfer between the GPU and the CPU. No rendering is allowed with this usage (or rather, it's undefined whether it works or not).
		Staging
	};

	enum class CPUAccess
	{
		None,
		Read,
		Write,
		ReadWrite
	};

	enum class ShaderType
	{
		Unknown,
		PixelShader,
		VertexShader,
		GeometryShader,
		GeometryShaderSO, // For future use.
		HullShader,
		DomainShader,
		ComputeShader
	};
}

#endif

