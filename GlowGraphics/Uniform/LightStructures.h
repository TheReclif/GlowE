#pragma once
#ifndef GLOWE_UNIFORM_GRAPHICS_LIGHTSTRUCTURES_INCLUDED
#define GLOWE_UNIFORM_GRAPHICS_LIGHTSTRUCTURES_INCLUDED

#include "glowgraphics_export.h"

#include "../../GlowSystem/NumberStorage.h"
#include "../../GlowSystem/SerializableDataCreator.h"

namespace GLOWE
{
	struct GLOWGRAPHICS_EXPORT DirectionalLight
	{
		Float4 ambient;
		Float4 diffuse;
		Float4 specular;

		Float3 direction;
		float importance; // When sorting lights per object the following formula is used to sort by the most important lights: range * (importance / 1000.0f). The default importance is 1000.0f.

		GlowStaticSerialize(GlowBases(), GlowField(ambient), GlowField(diffuse), GlowField(specular), GlowField(direction), GlowField(importance));
	};

	struct GLOWGRAPHICS_EXPORT PointLight
	{
		Float4 ambient;
		Float4 diffuse;
		Float4 specular;

		Float3 position;
		float range;

		Float3 attenuation;
		float importance; // When sorting lights per object the following formula is used to sort by the most important lights: range * (importance / 1000.0f). The default importance is 1000.0f.

		GlowStaticSerialize(GlowBases(), GlowField(ambient), GlowField(diffuse), GlowField(specular), GlowField(position), GlowField(range), GlowField(attenuation), GlowField(importance));
	};

	struct GLOWGRAPHICS_EXPORT SpotLight
	{
		Float4 ambient;
		Float4 diffuse;
		Float4 specular;

		Float3 position;
		float range;

		Float3 direction;
		float innerCutOff;

		Float3 attenuation;
		float outerCutOff;

		float importance; // When sorting lights per object the following formula is used to sort by the most important lights: range * (importance / 1000.0f). The default importance is 1000.0f.
		float outerCutOffAngle;
		Float2 _padd;

		GlowStaticSerialize(GlowBases(), GlowField(ambient), GlowField(diffuse), GlowField(specular), GlowField(position), GlowField(range), GlowField(direction), GlowField(innerCutOff), GlowField(outerCutOff), GlowField(outerCutOffAngle), GlowField(attenuation), GlowField(importance));
	};
}

#endif
