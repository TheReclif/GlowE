#pragma once
#ifndef GLOWE_GRAPHICS_UNIFORM_CULLABLE_INCLUDED
#define GLOWE_GRAPHICS_UNIFORM_CULLABLE_INCLUDED

#include "glowgraphics_export.h"

#include "../../GlowSystem/MemoryMgr.h"
#include "../../GlowSystem/ThreadPool.h"

namespace GLOWE
{
	class Cullable;
	class Frustum;

	class GLOWGRAPHICS_EXPORT CullingMgr
		: public Subsystem
	{
	private:
		List<Cullable*> cullables;
	public:
		using CullableHandle = List<Cullable*>::const_iterator;;
	public:
		CullableHandle registerCullable(Cullable* cullable);
		void unregisterCullable(const CullableHandle& handle);

		void performCulling(const Frustum& frustum);
		ThreadPool::TaskResult<void> scheduleCulling(ThreadPool& threadPool, const Frustum& frustum);
		ThreadPool::TaskResult<void> scheduleCullingAndLightCulling(ThreadPool& threadPool, const Frustum& frustum);
	};

	class GLOWGRAPHICS_EXPORT Cullable
	{
	private:
		bool cullTestResult, wasRegistered;
		CullingMgr::CullableHandle cullableHandle;
	private:
		virtual bool performCulling(const Frustum& frustum) = 0;
	public:
		Cullable();
		virtual ~Cullable();

		void onCullableActivate();
		void onCullableDeactivate();

		void performCullTest(const Frustum& frustum);

		bool getCullTestResult() const;
	};
}

#endif
