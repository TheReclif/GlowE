#pragma once
#ifndef GLOWE_UNIFORM_UAVIMPLEMENTATION_INCLUDED
#define GLOWE_UNIFORM_UAVIMPLEMENTATION_INCLUDED

#include "../../GlowSystem/Utility.h"

#include "GraphicsDeviceImplementation.h"
#include "BufferImplementation.h"
#include "TextureImplementation.h"

namespace GLOWE
{
	/// @brief Base class for unordered access view implementations.
	class GLOWGRAPHICS_EXPORT UnorderedAccessViewImpl
		: public NoCopy
	{
	public:
		UnorderedAccessViewImpl() = default;
		virtual ~UnorderedAccessViewImpl() = default;

		/// @brief Creates an unordered access view for a buffer.
		/// @param buff Reference to the buffer.
		/// @param device Buffer's graphics device.
		virtual void create(const BufferImpl & buff, const GraphicsDeviceImpl& device) = 0;
		/// @brief Creates an unordered access view for a texture.
		/// @param tex Reference to the texture.
		/// @param device Texture's graphics device.
		virtual void create(const TextureImpl & tex, const GraphicsDeviceImpl& device) = 0;
		/// @brief Destroys the unordered access view.
		virtual void destroy() = 0;

		/// @brief Returns whether the UAV is valid, i.e. it was properly initialized and not destroyed in the meantime.
		/// @return true if valid, false otherwise.
		virtual bool checkIsValid() const = 0;
	};
}
#endif
