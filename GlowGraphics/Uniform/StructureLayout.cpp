#include "StructureLayout.h"

void GLOWE::StructureLayout::rehash()
{
	String temp, temp2;

	for (const auto& x : elems)
	{
		temp2 = x.getHash();
		temp.append(temp2.getCharArray(), 2);
	}

	hash = temp;
}

GLOWE::StructureLayout::StructureLayout()
	: elems(), currentSize(0), hash()
{
}

void GLOWE::StructureLayout::addElement(const Format & elem)
{
	elems.push_back(elem);
	currentSize += elem.getSize();
	rehash();
}

GLOWE::Format GLOWE::StructureLayout::getElem(const unsigned int element) const
{
	return elems.at(element);
}

std::size_t GLOWE::StructureLayout::getSize() const
{
	return currentSize;
}

std::size_t GLOWE::StructureLayout::getElemsCount() const
{
	return elems.size();
}

std::size_t GLOWE::StructureLayout::getOffsetToElem(const unsigned int element) const
{
	std::size_t result = 0;
	const std::size_t size = elems.size();

	for (unsigned int x = 0; x < element && x < size; ++x)
	{
		result += elems[x].getSize();
	}

	return result;
}

std::size_t GLOWE::StructureLayout::getElemSize(const unsigned int element) const
{
	return elems.at(element).getSize();
}

void* GLOWE::StructureLayout::getAddressOfElement(const unsigned element, void* const mem) const
{
	return static_cast<void*>((char*)(mem) + getOffsetToElem(element));
}

void GLOWE::StructureLayout::serialize(LoadSaveHandle& handle) const
{
	const UInt32 size = elems.size();
	handle << size;
	for (const auto& x : elems)
	{
		handle << x;
	}
}

void GLOWE::StructureLayout::deserialize(LoadSaveHandle& handle)
{
	UInt32 size = 0;
	handle >> size;
	elems.resize(size);
	for (auto& x : elems)
	{
		handle >> x;
	}
	rehash();
}

GLOWE::Hash GLOWE::StructureLayout::getHash() const
{
	return hash;
}

void GLOWE::NamedStructureLayout::rehash()
{
	String temp, temp2;

	for (const auto& x : elems)
	{
		temp2 = x.getHash();
		temp.append(temp2.getCharArray(), 2);
	}

	for (const auto& x : names)
	{
		temp += x.first;
	}

	hash = temp;
}

GLOWE::StructureLayout GLOWE::NamedStructureLayout::toStructureLayout() const
{
	StructureLayout temp;
	for (const auto& x : elems)
	{
		temp.addElement(x);
	}
	return temp;
}

GLOWE::HashedStructureLayout GLOWE::NamedStructureLayout::toHashedStructureLayout() const
{
	HashedStructureLayout result;

	for (unsigned int x = 0; x < elems.size(); ++x)
	{
		result.addElement(elems[x], Hash(getName(x)));
	}

	return result;
}

void GLOWE::NamedStructureLayout::addElement(const Format & elem, const String & name)
{
	names.emplace(name, static_cast<UInt32>(elems.size()));
	elems.push_back(elem);
	currentSize += elem.getSize();
	rehash();
}

GLOWE::String GLOWE::NamedStructureLayout::getName(const unsigned int element) const
{
	for (const auto& x : names)
	{
		if (x.second == element)
		{
			return x.first;
		}
	}

	return String();
}

int GLOWE::NamedStructureLayout::getElemPos(const String & name) const
{
	auto it = names.find(name);
	if (it != names.end())
	{
		return it->second;
	}

	return -1;
}

bool GLOWE::NamedStructureLayout::checkIsElemPresent(const String& name) const
{
	return names.count(name);
}

GLOWE::Format GLOWE::NamedStructureLayout::getElem(const String & name) const
{
	return elems[names.at(name)];
}

std::size_t GLOWE::NamedStructureLayout::getOffsetToElem(const String & name) const
{
	return getOffsetToElem(getElemPos(name));
}

std::size_t GLOWE::NamedStructureLayout::getElemSize(const String & name) const
{
	return getElemSize(getElemPos(name));
}

void * GLOWE::NamedStructureLayout::getAddressOfElement(const String & name, void * const mem) const
{
	return getAddressOfElement(getElemPos(name), mem);
}

void GLOWE::NamedStructureLayout::serialize(LoadSaveHandle& handle) const
{
	const UInt32 size = elems.size();
	handle << size;

	for (const auto& x : elems)
	{
		handle << x;
	}
	for (const auto& x : names)
	{
		handle << x.first << x.second;
	}
}

void GLOWE::NamedStructureLayout::deserialize(LoadSaveHandle& handle)
{
	UInt32 size = 0, temp;
	handle >> size;

	elems.resize(size);
	for (unsigned int x = 0; x < size; ++x)
	{
		handle >> elems[x];
	}
	for (unsigned int x = 0; x < size; ++x)
	{
		Pair<String, UInt32> tempPair;
		handle >> tempPair.first >> tempPair.second;
		names.insert(std::move(tempPair));
	}

	rehash();
}

void GLOWE::HashedStructureLayout::rehash()
{
	String temp, temp2;

	for (const auto& x : elems)
	{
		temp2 = x.getHash();
		temp.append(temp2.getCharArray(), 2);
	}

	for (const auto& x : hashes)
	{
		temp2 = x.first;
		temp.append(temp2.getCharArray(), 3);
	}

	hash = temp;
}

GLOWE::StructureLayout GLOWE::HashedStructureLayout::toStructureLayout() const
{
	StructureLayout result;
	for (const auto& x : elems)
	{
		result.addElement(x);
	}
	return result;
}

void GLOWE::HashedStructureLayout::addElement(const Format & elem, const Hash & name)
{
	hashes.emplace(name, static_cast<UInt32>(elems.size()));
	elems.push_back(elem);
	currentSize += elem.getSize();
	rehash();
}

GLOWE::Hash GLOWE::HashedStructureLayout::getHash(const unsigned int element) const
{
	for (const auto& x : hashes)
	{
		if (x.second == element)
		{
			return x.first;
		}
	}

	return Hash();
}

int GLOWE::HashedStructureLayout::getElemPos(const Hash & name) const
{
	auto it = hashes.find(name);
	if (it != hashes.end())
	{
		return it->second;
	}

	return -1;
}

bool GLOWE::HashedStructureLayout::checkIsElemPresent(const Hash& name) const
{
	return hashes.count(name);
}

GLOWE::Format GLOWE::HashedStructureLayout::getElem(const Hash & name) const
{
	return elems[hashes.at(name)];
}

std::size_t GLOWE::HashedStructureLayout::getOffsetToElem(const Hash & name) const
{
	return getOffsetToElem(getElemPos(name));
}

std::size_t GLOWE::HashedStructureLayout::getElemSize(const Hash & name) const
{
	return getElemSize(getElemPos(name));
}

void * GLOWE::HashedStructureLayout::getAddressOfElement(const Hash & name, void * const mem) const
{
	return getAddressOfElement(getElemPos(name), mem);
}

void GLOWE::HashedStructureLayout::serialize(LoadSaveHandle& handle) const
{
	const UInt32 size = elems.size();
	handle << size;
	if (size == 0)
	{
		return;
	}

	for (const auto& x : elems)
	{
		handle << x;
	}
	for (const auto& x : hashes)
	{
		handle << x.first << x.second;
	}
}

void GLOWE::HashedStructureLayout::deserialize(LoadSaveHandle& handle)
{
	UInt32 size;
	handle >> size;
	elems.resize(size);
	for (unsigned int x = 0; x < size; ++x)
	{
		handle >> elems[x];
	}
	for (unsigned int x = 0; x < size; ++x)
	{
		Pair<Hash, UInt32> pair;
		handle >> pair.first >> pair.second;
		hashes.insert(pair);
	}

	rehash();
}

/*
#if defined(GLOWE_COMPILE_FOR_DIRECTX) && GLOWE_DIRECTX_VERSION == 11
GLOWE::StructureLayout GLOWE::DirectX11::convertStructLayoutToConstBufferLayout(const StructureLayout & arg)
{
	StructureLayout result;
	Format templateFormat;
	templateFormat.type = Format::Type::Byte;
	const std::size_t elems = arg.getElemsCount();

	constexpr unsigned int blockSize = 16; // In bytes.
	unsigned int currentBlockSize = blockSize, temp = 0;

	for (unsigned int x = 0; x < elems; ++x)
	{
		temp = arg.getElemSize(x);
		if (temp > currentBlockSize)
		{
			templateFormat.elemsCount = currentBlockSize;
			currentBlockSize = blockSize;
			result.addElement(templateFormat);
		}

		currentBlockSize -= temp;
		result.addElement(arg.getElem(x));

		if (currentBlockSize == 0)
		{
			currentBlockSize = blockSize;
		}
	}

	if (result.getSize() % 16 != 0)
	{
		result.addElement(Format(Format::Type::Byte, 16 - (result.getSize() % 16)));
	}

	return result;
}

GLOWE::NamedStructureLayout GLOWE::DirectX11::convertStructLayoutToConstBufferLayout(const NamedStructureLayout & arg)
{
	NamedStructureLayout result;
	Format templateFormat;
	templateFormat.type = Format::Type::Byte;
	const std::size_t elems = arg.getElemsCount();

	constexpr unsigned int blockSize = 16; // In bytes.
	unsigned int currentBlockSize = blockSize, temp = 0, counter = 0;

	for (unsigned int x = 0; x < elems; ++x)
	{
		temp = arg.getElemSize(x);
		if (temp > currentBlockSize)
		{
			templateFormat.elemsCount = currentBlockSize;
			currentBlockSize = blockSize;
			result.addElement(templateFormat, toString(counter++));
		}

		currentBlockSize -= temp;
		result.addElement(arg.getElem(x), arg.getName(x));

		if (currentBlockSize == 0)
		{
			currentBlockSize = blockSize;
		}
	}

	if (result.getSize() % 16 != 0)
	{
		result.addElement(Format(Format::Type::Byte, 16 - (result.getSize() % 16)), toString(counter));
	}

	return result;
}

GLOWE::HashedStructureLayout GLOWE::DirectX11::convertStructLayoutToConstBufferLayout(const HashedStructureLayout & arg)
{
	HashedStructureLayout result;

	Format templateFormat;
	templateFormat.type = Format::Type::Byte;
	const std::size_t elems = arg.getElemsCount();

	constexpr unsigned int blockSize = 16; // In bytes.
	unsigned int temp = 0, counter = 0;
	int currentBlockSize = blockSize;

	for (unsigned int x = 0; x < elems; ++x)
	{
		temp = arg.getElemSize(x);
		if (temp > currentBlockSize)
		{
			templateFormat.elemsCount = currentBlockSize;
			currentBlockSize = blockSize;
			result.addElement(templateFormat, toString(counter++));
		}

		currentBlockSize -= temp;
		result.addElement(arg.getElem(x), arg.getHash(x));

		if (currentBlockSize == 0)
		{
			currentBlockSize = blockSize;
		}
	}

	if (result.getSize() % 16 != 0)
	{
		result.addElement(Format(Format::Type::Byte, 16 - (result.getSize() % 16)), toString(counter));
	}

	return result;
}

#endif
*/
