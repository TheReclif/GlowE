#include "Geometry.h"
#include "GraphicsFactory.h"

void GLOWE::Geometry::apply(BasicRendererImpl& renderer, const GraphicsDeviceImpl& device, const ShaderCollectionImpl& shaderColl)
{
	if (buffers)
	{
		buffers->apply(renderer, device, shaderColl);
	}
}

void GLOWE::Geometry::draw(BasicRendererImpl& renderer) const
{
	if (buffers)
	{
		if (buffers->checkIsIndexed())
		{
			renderer.drawIndexed(count, offset);
		}
		else
		{
			renderer.draw(count, offset);
		}
	}
}

void GLOWE::Geometry::drawInstanced(const unsigned int instancesCount, BasicRendererImpl& renderer) const
{
	if (buffers)
	{
		if (buffers->checkIsIndexed())
		{
			renderer.drawIndexedInstanced(count, instancesCount, 0, offset);
		}
		else
		{
			renderer.drawInstanced(count, instancesCount, 0, offset);
		}
	}
}

GLOWE::GeometryBuffers::GeometryBuffers()
	: vertexBuffer(createGraphicsObject<BufferImpl>()), indicesBuffer(createGraphicsObject<BufferImpl>()), vertexLayout(), inputLayouts()
{
}

void GLOWE::GeometryBuffers::apply(BasicRendererImpl& renderer, const GraphicsDeviceImpl& device, const ShaderCollectionImpl& shaderColl)
{
	renderer.setVertexBuffer(vertexBuffer.get());
	renderer.setIndexBuffer(indicesBuffer.get());

	void* const temp = static_cast<void*>(shaderColl.getDesc().vertexShader.get());

	Lockable::UniqueLock lock(*this);
	auto it = inputLayouts.find(temp);
	if (it == inputLayouts.end())
	{
		const auto& vertexShader = shaderColl.getDesc().vertexShader;
		if (vertexShader == nullptr)
		{
			WarningThrow(false, "Vertex shader is nullptr (check your shader compilation log for errors).");
			return;
		}
		it = inputLayouts.emplace(std::piecewise_construct, std::forward_as_tuple(temp), std::forward_as_tuple(createGraphicsObject<InputLayoutImpl>())).first;
		it->second->create(vertexLayout + vertexShader->getInstanceInputLayout(), device, *vertexShader);
	}
	lock.unlock();

	renderer.setInputLayout(it->second.get());
}

bool GLOWE::GeometryBuffers::checkIsIndexed() const
{
	return static_cast<bool>(indicesBuffer);
}

unsigned int GLOWE::ComplexGeometryBuffers::addBuffer(BufferImpl* notOwnedBuffer, const InputLayoutDesc& inDesc)
{
	//InputLayoutDesc desc;
	inputLayoutDesc.append(inDesc);
	buffers.emplace_back(notOwnedBuffer);
	return buffers.size() - 1;
}

unsigned int GLOWE::ComplexGeometryBuffers::addBuffer(const SharedPtr<BufferImpl>& ownedBuffer, const InputLayoutDesc& inDesc)
{
	inputLayoutDesc.append(inDesc);
	ownedBuffers.emplace_back(ownedBuffer);
	buffers.emplace_back(ownedBuffer.get());
	return buffers.size() - 1;
}

unsigned int GLOWE::ComplexGeometryBuffers::addBuffer(const SharedPtr<GeometryBuffers>& ownedBuffs)
{
	inputLayoutDesc.append(ownedBuffs->vertexLayout);
	ownedBuffers.emplace_back(ownedBuffs);
	buffers.emplace_back(ownedBuffs->vertexBuffer.get());
	return buffers.size() - 1;
}

void GLOWE::ComplexGeometryBuffers::setIndicesBuffer(const BufferImpl* buff)
{
	indicesBuffer = buff;
}

void GLOWE::ComplexGeometryBuffers::setIndicesBuffer(const SharedPtr<GeometryBuffers>& ownedBuffs)
{
	indicesBuffer = ownedBuffs->indicesBuffer.get();
}

void GLOWE::ComplexGeometryBuffers::replaceBuffer(const unsigned int pos, const BufferImpl* newBuffer)
{
	if (buffers.size() > pos)
	{
		buffers[pos] = newBuffer;
	}
}

void GLOWE::ComplexGeometryBuffers::apply(BasicRendererImpl& renderer, const GraphicsDeviceImpl& device, const ShaderCollectionImpl& shaderColl)
{
	renderer.setVertexBuffers(buffers);
	if (indicesBuffer)
	{
		renderer.setIndexBuffer(indicesBuffer);
	}

	void* const temp = static_cast<void*>(shaderColl.getDesc().vertexShader.get());

	Lockable::UniqueLock lock(*this);
	auto it = inputLayouts.find(temp);
	if (it == inputLayouts.end())
	{
		const auto& vertexShader = shaderColl.getDesc().vertexShader;
		if (vertexShader == nullptr)
		{
			WarningThrow(false, "Vertex shader is nullptr (check your shader compilation log for errors).");
			return;
		}
		it = inputLayouts.emplace(std::piecewise_construct, std::forward_as_tuple(temp), std::forward_as_tuple(createGraphicsObject<InputLayoutImpl>())).first;
		it->second->create(inputLayoutDesc/* + vertexShader->getInstanceInputLayout()*/, device, *vertexShader);
	}
	lock.unlock();

	renderer.setInputLayout(it->second.get());
}

bool GLOWE::ComplexGeometryBuffers::checkIsIndexed() const
{
	return indicesBuffer;
}

GLOWE::GeometryBuilder::GeometryBuilder()
	: attribDatas(1)
{
}

void GLOWE::GeometryBuilder::addVertexBuffer()
{
	attribDatas.emplace_back();
}

void GLOWE::GeometryBuilder::addVertexAttribute(const String& name, const void* const data, const Format type, const UInt64 howMany, const int bufferId)
{
	if (howMany == 0)
	{
		throw Exception("howMany == 0");
	}
	if (size == 0)
	{
		size = howMany;
	}
	if (howMany != size)
	{
		throw Exception("Buffer dimensions does not match");
	}
	if (bufferId >= attribDatas.size())
	{
		throw Exception("bufferId out of bounds (you probably forgot to call addVertexBuffer when using more than one vertex buffer)");
	}

	inputLayoutDesc.addVertexElement(name, type, bufferId >= 0 ? bufferId : 0);
	attribDatas[bufferId].emplace_back(static_cast<const unsigned char*>(data), static_cast<const unsigned char*>(data) + (howMany * type.getSize()));
}

void GLOWE::GeometryBuilder::setIndices(const unsigned short* const indices, const UInt64 size)
{
	if (size > 0)
	{
		indexData.assign(indices, indices + size);
	}
	else
	{
		indexData.clear();
	}
	indicesAreShort = true;
}

void GLOWE::GeometryBuilder::setIndices(const unsigned int* const indices, const UInt64 size)
{
	if (size > 0)
	{
		indexData.assign(indices, indices + size);
	}
	else
	{
		indexData.clear();
	}
	indicesAreShort = false;
}

void GLOWE::GeometryBuilder::build(Geometry& output, const GraphicsDeviceImpl& device, const Options& cfg) const
{
	output.buffers = nullptr;
	output.offset = output.count = 0;
	output.localAABB = AABB();

	switch (attribDatas.size())
	{
	case 0:
		return;
	case 1:
	{
		UniquePtr<GeometryBuffers> buffers = makeUniquePtr<GeometryBuffers>();
		buffers->vertexLayout = inputLayoutDesc;
		const auto& structLayout = inputLayoutDesc.getVertexLayout();
		Vector<char> tempBuffer;
		tempBuffer.reserve(size * structLayout.getSize());

		for (UInt64 x = 0; x < size; ++x)
		{
			for (UInt64 y = 0; y < attribDatas[0].size(); ++y)
			{
				tempBuffer.insert(tempBuffer.end(), (x * structLayout.getElemSize(y)) + attribDatas[0][y].data(), (x * structLayout.getElemSize(y) + structLayout.getElemSize(y)) + attribDatas[0][y].data());
			}
		}

		BufferDesc desc;
		desc.binds = BufferDesc::Binding::VertexBuffer;
		desc.cpuAccess = cfg.cpuAccess;
		desc.usage = cfg.usage;
		desc.layout = structLayout.toStructureLayout();
		buffers->vertexBuffer = createGraphicsObject<BufferImpl>();
		buffers->vertexBuffer->create(desc, tempBuffer.size(), device, tempBuffer.data());

		if (!indexData.empty())
		{
			desc.binds = BufferDesc::Binding::IndexBuffer;
			desc.layout = StructureLayout();
			desc.layout.addElement(Format(indicesAreShort ? Format::Type::UnsignedShort : Format::Type::UnsignedInt, 1));
			buffers->indicesBuffer = createGraphicsObject<BufferImpl>();
			buffers->indicesBuffer->create(desc, indexData.size(), device, indexData.data());
		}
	}
	default:
	{
		throw NotImplementedException();
		/*UniquePtr<ComplexGeometryBuffers> buffers = makeUniquePtr<ComplexGeometryBuffers>();
		// TODO: Init stuff here
		for (const auto& buff : attribDatas)
		{

		}*/
	}
		break;
	}
	
	if (cfg.buildAABB)
	{
		auto positionAttribPos = inputLayoutDesc.getVertexLayout().getElemPos("position");
		if (positionAttribPos < 0)
		{
			throw Exception("buildAABB specified, but no attribute called \"position\"");
		}
		if (inputLayoutDesc.getVertexLayout().getElem(positionAttribPos) != Format(Format::Type::Float, 3))
		{
			throw Exception("\"position\" attribute is not of type Float3");
		}

		if (!indexData.empty())
		{
			if (indicesAreShort)
			{
				output.localAABB.fromIndices(reinterpret_cast<const unsigned short*>(indexData.data()), indexData.size(), attribDatas[positionAttribPos].data(), sizeof(Float3), size);
			}
			else
			{
				output.localAABB.fromIndices(reinterpret_cast<const unsigned int*>(indexData.data()), indexData.size(), attribDatas[positionAttribPos].data(), sizeof(Float3), size);
			}
		}
		else
		{
			output.localAABB.fromVertices(attribDatas[positionAttribPos].data(), sizeof(Float3), size);
		}
	}
}
