#pragma once
#ifndef GLOWE_UNIFORM_BLENDSTATEIMPLEMENTATION_INCLUDED
#define GLOWE_UNIFORM_BLENDSTATEIMPLEMENTATION_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/Hash.h"

#include "BlendDesc.h"

namespace GLOWE
{
	class GraphicsDeviceImpl;

	class GLOWGRAPHICS_EXPORT BlendStateImpl
		: public Hashable, public NoCopy
	{
	protected:
		BlendDesc desc;
		Hash hash;
	public:
		BlendStateImpl();
		BlendStateImpl(const BlendDesc& arg);
		virtual ~BlendStateImpl() = default;

		virtual void create(const BlendDesc& desc, const GraphicsDeviceImpl& device) = 0;
		virtual void destroy() = 0;

		virtual bool checkIsValid() const = 0;

		const BlendDesc& getDesc() const;

		virtual Hash getHash() const override;
	};
}

#endif
