#pragma once
#ifndef GLOWE_GRAPHICS_UNIFORM_BASICRENDERERIMPLEMENTATION_INCLUDED
#define GLOWE_GRAPHICS_UNIFORM_BASICRENDERERIMPLEMENTATION_INCLUDED

#include "../../GlowSystem/Utility.h"

#include "Common.h"
#include "Viewport.h"

#include "RenderTargetImplementation.h"
#include "ShaderResourceViewImpl.h"
#include "BlendStateImplementation.h"
#include "DepthStencilStateImplementation.h"
#include "RasterizerStateImplementation.h"
#include "InputLayoutImplementation.h"
#include "BufferImplementation.h"
#include "UnorderedAccessViewImpl.h"
#include "TextureImplementation.h"
#include "ShaderCollectionImplementation.h"
#include "SamplerImplementation.h"
#include "GraphicsDeviceImplementation.h"

#include "../../GlowMath/Rect.h"

#include "../../GlowSystem/MemoryMgr.h"
#include "../../GlowSystem/Color.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT BasicRendererImpl
	{
	public:
		enum class PrimitiveTopology : unsigned char
		{
			Unspecified,
			PointList,
			LineList,
			LineListAdjacency,
			LineStrip,
			LineStripAdjacency,
			TriangleList,
			TriangleListAdjacency,
			TriangleStrip,
			TriangleStripAdjacency
		};
	private:
		PrimitiveTopology currentTopology = PrimitiveTopology::Unspecified;
		Vector<Rect> scissorRects;
		bool scissorRectsNeedUpdate = false;
	private:
		virtual void setPrimitiveTopologyImpl(const PrimitiveTopology topology) = 0;

		virtual void drawImpl(const unsigned int verticesCount, const unsigned int startingVertex = 0) = 0;
		virtual void drawIndexedImpl(const unsigned int indicesCount, const unsigned int startingIndex = 0, const unsigned int startingVertex = 0) = 0;
		virtual void drawInstancedImpl(const unsigned int verticesPerInstance, const unsigned int instancesCount, const unsigned int startingVertex = 0, const unsigned int startingInstanceLoc = 0) = 0;
		virtual void drawIndexedInstancedImpl(const unsigned int indicesPerInstance, const unsigned int instancesCount, const int baseVertexLocation = 0, const unsigned int startingVertex = 0, const unsigned int startingInstanceLoc = 0) = 0;

		virtual void dispatchImpl(const unsigned int x, const unsigned int y, const unsigned int z) = 0;

		virtual void setScissorRectsImpl(const Rect* const rects, const unsigned int size) = 0;

		void updateScissors();
	public:
		/// @brief Pass to setUAVs and setRenderTargetAndUAVs to properly handle UAVs' start slot (e.g. for pixel shaders in Direct3D 11 the start slot must equal render targets' count).
		static constexpr unsigned int SetUAVsForPixelShader = ~(0U);
	public:
		BasicRendererImpl() = default;
		virtual ~BasicRendererImpl() = default;

		virtual void create(const GraphicsDeviceImpl& device) = 0;
		virtual void destroy() = 0;

		void setPrimitiveTopology(const PrimitiveTopology topology);

		virtual void customFunction(const std::function<void(BasicRendererImpl&)>& func) = 0;

		virtual void setBlendState(const BlendStateImpl* state) = 0;
		virtual void setDepthStencilState(const DepthStencilStateImpl* dss) = 0;
		virtual void setRasterizerState(const RasterizerStateImpl* rs) = 0;
		virtual void setInputLayout(const InputLayoutImpl* layout) = 0;

		virtual void copyBuffers(const BufferImpl* src, const BufferImpl* dst) = 0;
		virtual void updateBuffer(const BufferImpl* buffer, const void* data, const std::size_t dataSize, const std::size_t offset = 0) = 0;
		virtual void readBuffer(const BufferImpl* buffer, void* data, const std::size_t dataSize, const std::size_t offset = 0) = 0;

		virtual void copyTextures(const TextureImpl* src, const TextureImpl* dst) = 0;
		virtual void copyToTexture(const TextureImpl* src, const TextureImpl* dst, const UInt3& dstPos, const UInt3& srcPos, const UInt3& howMany, const unsigned int srcSubresourceId = 0, const unsigned int dstSubresourceId = 0) = 0;
		virtual void updateTexture(const TextureImpl* src, const void* data, const UInt3& pos, const UInt3& howMany, const unsigned int dstSubresourceId = 0) = 0;
		virtual void readTexture(const TextureImpl* src, void* data, const UInt3& pos, const UInt3& howMany, const unsigned int srcSubresourceId = 0) = 0;

		virtual void setVertexBuffer(const BufferImpl* buffer) = 0;
		virtual void setVertexBuffers(const Vector<const BufferImpl*>& buffers) = 0;
		virtual void setVertexBuffers(const BufferImpl** buffers, const unsigned int size) = 0;

		virtual void setIndexBuffer(const BufferImpl* buffer) = 0;

		virtual void setShaderCollection(const ShaderCollectionImpl* coll) = 0;

		virtual void setSRV(const ShaderResourceViewImpl* srv, const unsigned int texUnit, const ShaderType stage) = 0;
		virtual void setSRV(const ShaderResourceViewImpl** srvArr, const unsigned int firstTexUnit, const unsigned int arraySize, const ShaderType stage) = 0;

		virtual void setSamplers(const SamplerImpl** arr, const unsigned int startSlot, const unsigned int arraySize, const ShaderType stage) = 0;

		virtual void setConstantBuffers(const BufferImpl* cb, const unsigned int buffSlot, const ShaderType stage) = 0;
		virtual void setConstantBuffers(const BufferImpl** cb, const unsigned int buffSlot, const unsigned int arraySize, const ShaderType stage) = 0;

		virtual void setRenderTarget(const RenderTargetImpl* rt) = 0;
		virtual void setRenderTargetAndUAVs(const RenderTargetImpl* rt, const Vector<const UnorderedAccessViewImpl*>& uavs, const unsigned int uavStartSlot = 0) = 0;
		virtual void setRenderTargetAndUAVs(const RenderTargetImpl* rt, const UnorderedAccessViewImpl* const * uavs, const unsigned int arraySize, const unsigned int uavStartSlot = 0) = 0;
		virtual void setUAVs(const UnorderedAccessViewImpl* const* uavs, const unsigned int arraySize, const unsigned int uavStartSlot = 0) = 0;

		virtual void setViewport(const Viewport& vp) = 0;
		virtual void setViewports(const Vector<Viewport>& vpsArray) = 0;
		virtual void setViewports(const Viewport* vpsArray, const unsigned int size) = 0;

		virtual void clearColor(const RenderTargetImpl* rt, const Color& refreshCol = Color(10.0f / 255.0f, 135.0f / 255.0f, 135.0f / 255.0f)) = 0;
		virtual void clearDepthStencil(const RenderTargetImpl* rt, const float refreshDepth = 1.0f, const UInt8 refreshStencil = 0) = 0;

		virtual void generateMipmaps(const ShaderResourceViewImpl* tex) = 0;

		void setScissorRect(const Rect& rect);
		void setScissorRects(const Vector<Rect>& rects);
		void setScissorRects(const Rect* rects, const unsigned int size);

		void draw(const unsigned int verticesCount, const unsigned int startingVertex = 0);
		void drawIndexed(const unsigned int indicesCount, const unsigned int startingIndex = 0, const unsigned int startingVertex = 0);
		void drawInstanced(const unsigned int verticesPerInstance, const unsigned int instancesCount, const unsigned int startingVertex = 0, const unsigned int startingInstanceLoc = 0);
		void drawIndexedInstanced(const unsigned int indicesPerInstance, const unsigned int instancesCount, const int baseVertexLocation = 0, const unsigned int startingVertex = 0, const unsigned int startingInstanceLoc = 0);

		void dispatch(const unsigned int x, const unsigned int y, const unsigned int z);

		PrimitiveTopology getCurrentPrimitiveTopology() const;
	};
}

#endif
