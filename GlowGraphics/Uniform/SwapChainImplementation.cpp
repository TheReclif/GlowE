#include "SwapChainImplementation.h"

const GLOWE::SwapChainDesc& GLOWE::SwapChain::getDesc() const
{
	return usedDesc;
}

void GLOWE::SwapChain::resize(const VideoMode& newMode)
{
	needsSizeUpdate = true;
	usedDesc.size = newMode;
}

void GLOWE::SwapChain::updateSize()
{
	if (needsSizeUpdate)
	{
		needsSizeUpdate = false;
		resizeBuffers(usedDesc.size);
	}
}
