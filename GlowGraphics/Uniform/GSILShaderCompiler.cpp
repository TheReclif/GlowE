#include "GSILLexer.h"
#include "GSILParser.h"
#include "GSILParserBaseVisitor.h"

#include "GSILShaderCompiler.h"
#include "InputLayoutImplementation.h"

#include "../../GlowSystem/ThreadPool.h"

// Converts property in cbuffer name to acceptable name (eg. colorTexture -> Color Texture (converted the first letter to upper one and inserted a space between the words)).
GLOWE::String convertPropertyName(const GLOWE::String& arg)
{
	using namespace GLOWE;

	String result(arg);

	const std::locale& loc = getInstance<Locale>().getLocale();
	result.replace("_", String());
	result[0] = std::toupper(result[0], loc);
	for (unsigned int x = 1; x < result.getSize(); ++x)
	{
		if (std::isupper(result[x], loc) && std::islower(result[x - 1], loc))
		{
			result.insert(x, ' ');
		}
	}

	return result;
}

GLOWE::GSILShader::GSILShader(const String & gsilCode)
	: GSILShader()
{
	fromGSILCode(gsilCode);
}

void GLOWE::GSILShader::registerInputLayouts()
{
	Map<Hash, unsigned int> registeredInputLayouts;

	for (auto& group : techniqueGroups)
	{
		for (auto& technique : group.second)
		{
			Vector<Pair<int, int>> ranges;

			bool defaultVariant = true;

			for (const auto& tag : technique.second.tags)
			{
				switch (tag.type)
				{
				case Technique::Tag::Type::SupportsDirectionalLights:
				case Technique::Tag::Type::SupportsPointLights:
				case Technique::Tag::Type::SupportsSpotLights:
					defaultVariant = false;
					ranges.emplace_back(0, tag.supportsLightingInfo.supportedLights);
					break;
				case Technique::Tag::Type::IsTexturePresent:
					defaultVariant = false;
					ranges.emplace_back(0, 1);
					break;
				default:
					break;
				}
			}

			String tempStr;
			const std::function<void(const unsigned int, Vector<int>&)> calculatePermutations = [&tempStr, &ranges, &calculatePermutations, &technique](const unsigned int range, Vector<int>& vars)
			{
				for (unsigned int x = ranges[range].first; x <= ranges[range].second; ++x)
				{
					vars.push_back(x);
					if ((range + 1) < ranges.size())
					{
						// Another permutation is needed.
						calculatePermutations(range + 1, vars);
					}
					else
					{
						// Last range - save permutation.
						tempStr.clear();
						tempStr.reserve(sizeof(int) * vars.size() + vars.size());

						for (const auto var : vars)
						{
							tempStr += toString(var);
							tempStr += '|';
						}

						tempStr.erase(tempStr.getSize() - 1); // Erase the last element.

						technique.second.variants.emplace(tempStr, vars);
					}
					vars.pop_back();
				}
			};

			if (ranges.size())
			{
				Vector<int> tempVec;
				tempVec.reserve(ranges.size());
				calculatePermutations(0, tempVec);
			}

			if (defaultVariant)
			{
				technique.second.variants.emplace(); // Add an empty variant (no varying uniforms are present).
			}

			Set<unsigned int> writtenTextures, writtenCBuffers, writtenFuncs, writtenUavs;
			std::function<void(Pass::ShaderDefinition&, const Function&)> processFunc = [this, &writtenTextures, &writtenCBuffers, &writtenFuncs, &writtenUavs, &processFunc](Pass::ShaderDefinition& shaderDesc, const Function& func)
			{
				for (const auto& x : func.usedFunctions)
				{
					if (writtenFuncs.count(x) == 0)
					{
						writtenFuncs.emplace(x);
						shaderDesc.usedFunctions.emplace_back(x);
						processFunc(shaderDesc, functions[x]);
					}
				}

				for (const auto& x : func.usedCBuffers)
				{
					if (writtenCBuffers.count(x) == 0)
					{
						writtenCBuffers.emplace(x);
						shaderDesc.usedCBuffers.emplace_back(x);
					}
				}

				for (const auto& x : func.usedTextures)
				{
					if (writtenTextures.count(x) == 0)
					{
						writtenTextures.emplace(x);
						shaderDesc.usedTextures.emplace_back(x);
					}
				}

				for (const auto& x : func.usedUAVs)
				{
					if (writtenUavs.count(x) == 0)
					{
						writtenUavs.emplace(x);
						shaderDesc.usedUAVs.emplace_back(x);
					}
				}
			};

			for (auto& pass : technique.second.passes)
			{
				if (!pass.second.computeShader.func.isEmpty())
				{
					const Function* func{};
					for (auto& x : functions)
					{
						if (x.name == pass.second.computeShader.func)
						{
							func = &x;
							break;
						}
					}

					if (func)
					{
						processFunc(pass.second.computeShader, *func);
					}
				}

				if (!pass.second.vertexShader.func.isEmpty())
				{
					writtenTextures.clear();
					writtenCBuffers.clear();
					writtenFuncs.clear();

					const Function* func{};
					for (auto& x : functions)
					{
						if (x.name == pass.second.vertexShader.func)
						{
							func = &x;
							break;
						}
					}

					if (func)
					{
						processFunc(pass.second.vertexShader, *func);
						const Structure* inStruc = nullptr;
						for (const auto& arg : func->arguments)
						{
							for (const auto& struc : structures)
							{
								if (struc.isInOut && arg.find(struc.name) != String::invalidPos)
								{
									inStruc = &struc;
									break;
								}
							}
							if (inStruc)
							{
								break;
							}
						}

						InputLayoutDesc layoutDesc;
						for (const auto& field : inStruc->fields)
						{
							if (field.isInstanceData)
							{
								layoutDesc.addInstanceElement(field.name, field.asFormat());
							}
							else
								if (!field.isSV)
								{
									layoutDesc.addVertexElement(field.name, field.asFormat());
								}
						}

						const Hash tempHash = layoutDesc.getHash();
						auto layoutIt = registeredInputLayouts.find(tempHash);
						if (layoutIt == registeredInputLayouts.end())
						{
							layoutIt = registeredInputLayouts.emplace(tempHash, static_cast<unsigned int>(inputLayouts.size())).first;
							inputLayouts.emplace_back(std::move(layoutDesc));
						}
						pass.second.inputLayoutId = layoutIt->second;
					}
				}

				if (!pass.second.pixelShader.func.isEmpty())
				{
					writtenTextures.clear();
					writtenCBuffers.clear();
					writtenFuncs.clear();

					const Function* func{};
					for (auto& x : functions)
					{
						if (x.name == pass.second.pixelShader.func)
						{
							func = &x;
							break;
						}
					}

					if (func)
					{
						processFunc(pass.second.pixelShader, *func);
					}
				}

				if (!pass.second.geometryShader.func.isEmpty())
				{
					writtenTextures.clear();
					writtenCBuffers.clear();
					writtenFuncs.clear();

					const Function* func{};
					for (auto& x : functions)
					{
						if (x.name == pass.second.geometryShader.func)
						{
							func = &x;
							break;
						}
					}

					if (func)
					{
						processFunc(pass.second.geometryShader, *func);
					}
				}
			}
		}
	}
}

void GLOWE::GSILShader::prepareFunctionHLSL(GLOWE::String& code, const Function& func) const
{
	using namespace GLOWE;

	static auto addStaticReplace = [](Vector<Pair<Regex, const char*>>& replaces, const char* reg, const char* replacement)
	{
		replaces.emplace_back(reg, replacement);
	};

	static auto getReplaces = []() -> Vector<Pair<Regex, const char*>>
	{
		Vector<Pair<Regex, const char*>> result;

		addStaticReplace(result, u8R"TXT(Float(\d*))TXT", u8"float$1");
		addStaticReplace(result, u8R"TXT(UInt(\d*))TXT", u8"uint$1");
		addStaticReplace(result, u8R"TXT(Int(\d*))TXT", u8"int$1");
		addStaticReplace(result, u8R"TXT(Bool(\d*))TXT", u8"bool$1");
		addStaticReplace(result, u8R"TXT(Double(\d*))TXT", u8"double$1");
		addStaticReplace(result, u8R"TXT((?:DMatrix|DMat)(\d+)x(\d+))TXT", u8"double$1x$2");
		addStaticReplace(result, u8R"TXT((?:Matrix|Mat)(\d+)x(\d+))TXT", u8"float$1x$2");
		addStaticReplace(result, u8R"TXT((\w+)\.sample\(([\s\S]*?)\)(?!\s*\)))TXT", u8"$1.Sample($1, $2)");

		return result;
	};

	static Regex hlslSnippetRegex(R"TXT(HLSL\s*\(\s*"(.*)"\s*\))TXT"), glslSnippetRegex(R"TXT(GLSL\s*\(\s*".*"\s*\))TXT"), mslSnippetRegex(R"TXT(MSL\s*\(\s*".*"\s*\))TXT");
	static Vector<Pair<Regex, const char*>> replaces(getReplaces());
	RegexMatch match;
	String attribsCode;

	bool areAttributesPresent = false;
	for (const auto& x : func.attributes)
	{
		for (const auto y : x)
		{
			if (std::isprint(y) || std::isspace(y))
			{
				areAttributesPresent = true;
				break;
			}
		}

		if (areAttributesPresent)
		{
			break;
		}
	}

	if (areAttributesPresent)
	{
		static Regex csThreadsRegex(R"TXT(\s*Threads\s*\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)\s*)TXT"),
			gsMaxVertsRegex(R"TXT(\s*MaxVertexCount\s*\(\s*(\d)\s*\)\s*)TXT");
		attribsCode = '[';
		for (const auto& x : func.attributes)
		{
			if (std::regex_match(x.getString(), match, csThreadsRegex))
			{
				attribsCode += "numthreads(";
				attribsCode += String(match[1].first, match[1].second);
				attribsCode += ',';
				attribsCode += String(match[2].first, match[2].second);
				attribsCode += ',';
				attribsCode += String(match[3].first, match[3].second);
				attribsCode += ')';
			}
			else
			if (x == "EarlyZ")
			{
				attribsCode += "earlydepthstencil";
			}
			else
			if (std::regex_match(x.getString(), match, gsMaxVertsRegex))
			{
				attribsCode += "maxvertexcount(";
				attribsCode += String(match[1].first, match[1].second);
				attribsCode += ')';
			}
			else
			{
				WarningThrow(false, "Unknown attribute \"" + x + '\"');
				continue;
			}

			attribsCode += "]\n[";
		}
		if (attribsCode.getSize() >= 2)
		{
			attribsCode.resize(attribsCode.getSize() - 2); // Erase the last "\n[".
		}
		else
		{
			attribsCode.clear(); // If all of the attributes were invalid/unknown or were applied elsewhere, there is no need for the attributes code.
		}
	}

	code = std::regex_replace(code.getString(), hlslSnippetRegex, "$1");
	code = std::regex_replace(code.getString(), glslSnippetRegex, "");
	code = std::regex_replace(code.getString(), mslSnippetRegex, "");

	for (const auto& pair : replaces)
	{
		code = std::regex_replace(code.getString(), pair.first, pair.second);
	}

	// Place appropiate sampler names as a first argument.
	{
		String temp;
		
		for (const auto& x : textures)
		{
			temp = u8"(" + x.name + u8R"TXT()\.Sample\(\1)TXT";
			Regex regex(temp.getString());
			temp = u8"$1.Sample(" + samplers[x.samplerID].first;
			code = std::regex_replace(code.getString(), regex, temp.getCharArray());
		}

		for (const auto& x : constantBuffers)
		{
			temp = u8"(" + x.name + u8R"TXT()\.(\w+))TXT";
			Regex regex(temp.getString());
			code = std::regex_replace(code.getString(), regex, u8"$1_$2");
		}
	}

	if (!attribsCode.isEmpty())
	{
		static Regex generalFunctionDeclarationRegex(u8R"TXT((\w+)\s+(\w+)(?=\s*\())TXT");
		decltype(match[1].first) firstIt = code.begin(), secondIt = code.end();
		while (std::regex_search(firstIt, secondIt, match, generalFunctionDeclarationRegex))
		{
			if (func.returnType == String(match[1].first, match[1].second) && func.name == String(match[2].first, match[2].second))
			{
				code.insert(match.position(), attribsCode);
				break;
			}
			firstIt = match.suffix().first;
		}
	}
}

// Header building method.
GLOWE::String GLOWE::GSILShader::toHLSL() const
{
	Set<unsigned int> mainFunctions;
	String code = "#pragma pack_matrix( row_major )\n", tempStr;
	//String code, tempStr;
	code.reserve(initialCodeBufferSize);

	auto addTypeName = [&code, this](const Structure::Field& field)
	{
		using Type = Structure::Field::Type;

		switch (field.type)
		{
		case Type::Bool:
			code += "bool";
			if (field.typesCount > 0)
			{
				code += toString(field.typesCount);
			}
			code += ' ';
			code += field.name;
			if (field.arraySize > 0)
			{
				code += '[';
				code += toString(field.arraySize);
				code += ']';
			}
			break;
		case Type::Int:
			code += "int";
			if (field.typesCount > 0)
			{
				code += toString(field.typesCount);
			}
			code += ' ';
			code += field.name;
			if (field.arraySize > 0)
			{
				code += '[';
				code += toString(field.arraySize);
				code += ']';
			}
			break;
		case Type::UInt:
			code += "uint";
			if (field.typesCount > 0)
			{
				code += toString(field.typesCount);
			}
			code += ' ';
			code += field.name;
			if (field.arraySize > 0)
			{
				code += '[';
				code += toString(field.arraySize);
				code += ']';
			}
			break;
		case Type::Struct:
			code += structures[field.typesCount].name;
			code += ' ';
			code += field.name;
			if (field.arraySize > 0)
			{
				code += '[';
				code += toString(field.arraySize);
				code += ']';
			}
			break;
		case Type::Float:
			code += "float";
			if (field.typesCount > 0)
			{
				code += toString(field.typesCount);
				if (field.typesCountMat > 0)
				{
					code += 'x';
					code += toString(field.typesCountMat);
				}
			}
			code += ' ';
			code += field.name;
			if (field.arraySize > 0)
			{
				code += '[';
				code += toString(field.arraySize);
				code += ']';
			}
			break;
		case Type::Double:
			code += "double";
			if (field.typesCount > 0)
			{
				code += toString(field.typesCount);
				if (field.typesCountMat > 0)
				{
					code += 'x';
					code += toString(field.typesCountMat);
				}
			}
			code += ' ';
			code += field.name;
			if (field.arraySize > 0)
			{
				code += '[';
				code += toString(field.arraySize);
				code += ']';
			}
			break;
		default:
			return;
		}
	};
	auto svToSemanticsName = [](const String& name) -> String
	{
		String result(u8"SV_");
		String::ConstIterator it = name.begin();

		std::locale loc;
		for (const String::ConstIterator end = name.end(); it != end && !std::isdigit(*it, loc); ++it)
		{
		}

		String temp(name.begin(), it);
		temp.toLowercase();
		temp[0] = std::toupper(temp[0], loc);

		result += temp;
		result += String(it, name.end());

		return result;
	};

	// Add structures.
	for (const auto& x : structures)
	{
		code += u8"struct ";
		code += x.name;
		code += "\n{\n";
		for (const auto& field : x.fields)
		{
			code += "    ";
			const bool isSV = field.isSV;
			code += tempStr;
			addTypeName(field);
			if (x.isInOut)
			{
				if (isSV)
				{
					code += " : ";
					code += svToSemanticsName(field.name);
				}
				else
				{
					code += " : ";
					code += field.name;
				}
			}
			code += ';';
			code += '\n';
		}
		code += "};\n";
	}

	return code;
}

GLOWE::String GLOWE::GSILShader::toHLSL(const Pass& pass, const ShaderType& shaderType) const
{
	const Pass::ShaderDefinition* _shaderDef{};
	switch (shaderType)
	{
	case ShaderType::VertexShader:
		_shaderDef = &pass.vertexShader;
		break;
	case ShaderType::PixelShader:
		_shaderDef = &pass.pixelShader;
		break;
	case ShaderType::ComputeShader:
		_shaderDef = &pass.computeShader;
		break;
	case ShaderType::GeometryShader:
	case ShaderType::GeometryShaderSO:
		_shaderDef = &pass.geometryShader;
		break;
	default:
		return String();
	}

	static Regex regex(u8R"TXT((?:([\w\s]+)\s+)?(\w+[\w\s<>]*)\s+(\w+)\s*(?:\[\s*(\d+)\s*\])?)TXT");
	RegexMatch match;
	const Pass::ShaderDefinition& shaderDef = *_shaderDef;
	constexpr unsigned int initialBufferSize = 1024U;
	String code, tempStr;
	code.reserve(initialBufferSize);

	const auto addTypeName = [&code, this](const Structure::Field& field, const String& cbufferName)
	{
		using Type = Structure::Field::Type;

		switch (field.type)
		{
		case Type::Bool:
			code += "bool";
			if (field.typesCount > 0)
			{
				code += toString(field.typesCount);
			}
			code += ' ';
			code += cbufferName + '_' + field.name;
			if (field.arraySize > 0)
			{
				code += '[';
				code += toString(field.arraySize);
				code += ']';
			}
			break;
		case Type::Int:
			code += "int";
			if (field.typesCount > 0)
			{
				code += toString(field.typesCount);
			}
			code += ' ';
			code += cbufferName + '_' + field.name;
			if (field.arraySize > 0)
			{
				code += '[';
				code += toString(field.arraySize);
				code += ']';
			}
			break;
		case Type::UInt:
			code += "uint";
			if (field.typesCount > 0)
			{
				code += toString(field.typesCount);
			}
			code += ' ';
			code += cbufferName + '_' + field.name;
			if (field.arraySize > 0)
			{
				code += '[';
				code += toString(field.arraySize);
				code += ']';
			}
			break;
		case Type::Struct:
			code += structures[field.typesCount].name;
			code += ' ';
			code += cbufferName + '_' + field.name;
			if (field.arraySize > 0)
			{
				code += '[';
				code += toString(field.arraySize);
				code += ']';
			}
			break;
		case Type::Float:
			code += "float";
			if (field.typesCount > 0)
			{
				code += toString(field.typesCount);
				if (field.typesCountMat > 0)
				{
					code += 'x';
					code += toString(field.typesCountMat);
				}
			}
			code += ' ';
			code += cbufferName + '_' + field.name;
			if (field.arraySize > 0)
			{
				code += '[';
				code += toString(field.arraySize);
				code += ']';
			}
			break;
		case Type::Double:
			code += "double";
			if (field.typesCount > 0)
			{
				code += toString(field.typesCount);
				if (field.typesCountMat > 0)
				{
					code += 'x';
					code += toString(field.typesCountMat);
				}
			}
			code += ' ';
			code += cbufferName + '_' + field.name;
			if (field.arraySize > 0)
			{
				code += '[';
				code += toString(field.arraySize);
				code += ']';
			}
			break;
		default:
			return;
		}
	};
	const auto writeTextureType = [&code](const TextureDesc::Type& type, const bool isMultisampled)
	{
		using Type = TextureDesc::Type;
		switch (type)
		{
		case Type::Texture1D:
			code += u8"Texture1D";
			break;
		case Type::Texture1DArray:
			code += u8"Texture1DArray";
			break;
		case Type::Texture2D:
			if (!isMultisampled)
			{
				code += u8"Texture2D";
			}
			else
			{
				code += u8"Texture2DMS";
			}
			break;
		case Type::Texture2DArray:
			if (!isMultisampled)
			{
				code += u8"Texture2DArray";
			}
			else
			{
				code += u8"Texture2DMSArray";
			}
			break;
		case Type::Texture3D:
			code += u8"Texture3D";
			break;
		case Type::TextureCube:
			code += u8"TextureCube";
			break;
		}
	};

	const Function* _func = nullptr;
	for (const auto& x : functions)
	{
		if (x.name == shaderDef.func)
		{
			_func = &x;
			break;
		}
	}

	if (!_func)
	{
		return String();
	}

	const Function& func = *_func;

	// Now we need to create a header (every shader in a pass has its own header).
	// First of all, write all used samplers, textures, etc.
	{
		Set<unsigned int> writtenSamplers, writtenTextures, writtenCbuffers, writtenUAVs;

		for (const auto& x : func.usedFunctions)
		{
			const Function& tempFun = functions[x];
			for (const auto& y : tempFun.usedTextures)
			{
				if (writtenTextures.count(y) == 0)
				{
					const Texture& tempTex = textures[y];
					if (writtenSamplers.count(tempTex.samplerID) == 0)
					{
						// Write a sampler.
						const Pair<String, SamplerDesc>& desc = samplers[tempTex.samplerID];
						if (desc.second.enableComparison)
						{
							code += u8"SamplerComparisonState ";
						}
						else
						{
							code += u8"SamplerState ";
						}

						code += desc.first;
						code += u8" : register(s" + toString(writtenSamplers.size()) + u8");\n";
						writtenSamplers.insert(tempTex.samplerID);
					}

					// Write a texture.
					writeTextureType(tempTex.type, tempTex.isMultiSample);
					code += ' ';
					code += tempTex.name;
					code += " : register(t" + toString(writtenTextures.size()) + u8");\n";
					writtenTextures.insert(y);
				}
			}
			for (const auto& y : tempFun.usedUAVs)
			{
				if (writtenUAVs.count(y) == 0)
				{
					const UAV& uav = uavs[y];
					code += u8"RW";
					switch (uav.type)
					{
					case UAV::Type::Buffer:
						code += "Buffer";
						break;
					case UAV::Type::Texture1D:
						code += "Texture1D";
						break;
					case UAV::Type::Texture2D:
						code += "Texture2D";
						break;
					case UAV::Type::Texture3D:
						code += "Texture3D";
						break;
					case UAV::Type::Texture1DArray:
						code += "Texture1DArray";
						break;
					case UAV::Type::Texture2DArray:
						code += "Texture2DArray";
						break;
					default:
						WarningThrow(false, "Unrecognised UAV type.");
						return String();
					}
					code += '<';
					switch (uav.format.type)
					{
					case Format::Type::Float:
						code += "float";
						break;
					case Format::Type::Int:
						code += "int";
						break;
					case Format::Type::UnsignedInt:
						code += "uint";
						break;
					case Format::Type::Byte:
						code += "byte";
						break;
					case Format::Type::UnsignedByte:
						code += "ubyte";
						break;
					default:
						WarningThrow(false, "Unsupported data type for an UAV.");
						return String();
					}
					if (uav.format.elemsCount > 1)
					{
						code += static_cast<char>('0' + uav.format.elemsCount);
					}
					code += "> ";
					code += uav.name;
					code += " : register(u" + toString(writtenUAVs.size()) + ");\n";

					writtenUAVs.insert(y);
				}
			}
			for (const auto& y : tempFun.usedCBuffers)
			{
				if (writtenCbuffers.count(y) == 0)
				{
					const Structure& struc = constantBuffers[y];
					code += u8"cbuffer ";
					code += struc.name;
					code += " : register(b" + toString(writtenCbuffers.size()) + ")\n{\n";
					for (const auto& field : struc.fields)
					{
						code += "    ";
						using Interpolation = Structure::Field::InterpolationSpecifier;
						switch (field.interpolation)
						{
						case Interpolation::NoInterpolation:
							code += "nointerpolation ";
							break;
						case Interpolation::NoPerspective:
							code += "noperspective ";
							break;
						default:
							break;
						}
						code += tempStr;
						addTypeName(field, struc.name);
						code += ';';
						code += '\n';
					}
					code += "};\n";

					writtenCbuffers.insert(y);
				}
			}
		}

		for (const auto& y : func.usedTextures)
		{
			if (writtenTextures.count(y) == 0)
			{
				const Texture& tempTex = textures[y];
				if (writtenSamplers.count(tempTex.samplerID) == 0)
				{
					// Write a sampler.
					const Pair<String, SamplerDesc>& desc = samplers[tempTex.samplerID];
					if (desc.second.enableComparison)
					{
						code += u8"SamplerComparisonState ";
					}
					else
					{
						code += u8"SamplerState ";
					}

					code += desc.first;
					code += u8" : register(s" + toString(writtenSamplers.size()) + u8");\n";
					writtenSamplers.insert(tempTex.samplerID);
				}

				// Write a texture.
				writeTextureType(tempTex.type, tempTex.isMultiSample);
				code += ' ';
				code += tempTex.name;
				code += " : register(t" + toString(writtenTextures.size()) + u8");\n";
				writtenTextures.insert(y);
			}
		}
		for (const auto& y : func.usedUAVs)
		{
			if (writtenUAVs.count(y) == 0)
			{
				const UAV& uav = uavs[y];
				code += u8"RW";
				switch (uav.type)
				{
				case UAV::Type::Buffer:
					code += "Buffer";
					break;
				case UAV::Type::Texture1D:
					code += "Texture1D";
					break;
				case UAV::Type::Texture2D:
					code += "Texture2D";
					break;
				case UAV::Type::Texture3D:
					code += "Texture3D";
					break;
				case UAV::Type::Texture1DArray:
					code += "Texture1DArray";
					break;
				case UAV::Type::Texture2DArray:
					code += "Texture2DArray";
					break;
				default:
					WarningThrow(false, "Unrecognised UAV type.");
					return String();
				}
				code += '<';
				switch (uav.format.type)
				{
				case Format::Type::Float:
					code += "float";
					break;
				case Format::Type::Int:
					code += "int";
					break;
				case Format::Type::UnsignedInt:
					code += "uint";
					break;
				case Format::Type::Byte:
					code += "byte";
					break;
				case Format::Type::UnsignedByte:
					code += "ubyte";
					break;
				default:
					WarningThrow(false, "Unsupported data type for an UAV.");
					return String();
				}
				if (uav.format.elemsCount > 1)
				{
					code += static_cast<char>('0' + uav.format.elemsCount);
				}
				code += "> ";
				code += uav.name;
				code += " : register(u" + toString(writtenUAVs.size()) + ");\n";

				writtenUAVs.insert(y);
			}
		}
		for (const auto& y : func.usedCBuffers)
		{
			if (writtenCbuffers.count(y) == 0)
			{
				const Structure& struc = constantBuffers[y];
				code += u8"cbuffer ";
				code += struc.name;
				code += " : register(b" + toString(writtenCbuffers.size()) + ")\n{\n";
				for (const auto& field : struc.fields)
				{
					code += "    ";
					using Interpolation = Structure::Field::InterpolationSpecifier;
					switch (field.interpolation)
					{
					case Interpolation::NoInterpolation:
						code += "nointerpolation ";
						break;
					case Interpolation::NoPerspective:
						code += "noperspective ";
						break;
					default:
						break;
					}
					code += tempStr;
					addTypeName(field, struc.name);
					code += ';';
					code += '\n';
				}
				code += "};\n";

				writtenCbuffers.insert(y);
			}
		}
	}

	// Unfortunately we need to iterate over usedFunctions one more time to add them to the code.
	{
		Set<unsigned int> writtenFunctions;
		const std::function<void(const Function&)> writeAllUsedFuncs = [this, &code, &writtenFunctions, &tempStr, &writeAllUsedFuncs](const Function& func)
		{
			for (const auto& x : func.usedFunctions)
			{
				if (writtenFunctions.count(x) == 0)
				{
					writtenFunctions.insert(x);
					writeAllUsedFuncs(functions[x]);
				}
			}

			tempStr.clear();
			tempStr += func.returnType;
			tempStr += ' ';
			tempStr += func.name;
			tempStr += '(';
			for (const auto& arg : func.arguments)
			{
				tempStr += arg;
				tempStr += ", ";
			}

			tempStr.erase(tempStr.getSize() - 2); // Erase last ", ".
			tempStr += ")\n{";
			tempStr += func.code;
			tempStr += "\n}\n";
			prepareFunctionHLSL(tempStr, func);
			code += tempStr;
		};
		//writeAllUsedFuncs(func);

		for (const auto& x : func.usedFunctions)
		{
			writeAllUsedFuncs(functions[x]);
		}
	}

	code += func.returnType;
	code += ' ';
	code += func.name;
	code += '(';
	bool argumentWritten = false;

	const Vector<int>& replacementsForVariant = pass.owner->variants.begin()->second;

	if (func.arguments.size() > 0)
	{
		for (const auto& x : func.arguments)
		{
			if (!std::regex_search(x.getString(), match, regex))
			{
				WarningThrow(false, "Invalid function argument syntax");
				continue;
			}
			String hlslModifiers;
			bool placeInputStructure = true;
			if (match[1].matched)
			{
				const auto tokenizedModifiers = String(match[1].first, match[1].second).tokenize(" ");
				for (const auto& modifier : tokenizedModifiers)
				{
					if (modifier == "Uniform")
					{
						placeInputStructure = false;
					}
					else
					if (modifier == "Point")
					{
						hlslModifiers += "point ";
					}
					else
					if (modifier == "Line")
					{
						hlslModifiers += "line ";
					}
					else
					if (modifier == "LineAdj")
					{
						hlslModifiers += "lineadj ";
					}
					else
					if (modifier == "Triangle")
					{
						hlslModifiers += "triangle ";
					}
					else
					if (modifier == "TriangleAdj")
					{
						hlslModifiers += "triangleadj ";
					}
					else
					if (modifier == "Out")
					{
						hlslModifiers += "out ";
					}
					else
					if (modifier == "InOut")
					{
						hlslModifiers += "inout ";
					}
				}
			}
			if (placeInputStructure)
			{
				// Input structure.
				code += hlslModifiers;
				code += ' ';
				code += String(match[2].first, match[2].second);
				code += ' ';
				code += String(match[3].first, match[3].second);
				if (match[4].matched)
				{
					code += '[';
					code += String(match[4].first, match[4].second);
					code += ']';
				}
				code += u8", ";
				argumentWritten = true;
			}
		}
	}

	if (argumentWritten)
	{
		code.erase(code.getSize() - 2);
	}

	code += ")\n{";
	code += func.code;
	code += "\n}\n";

	prepareFunctionHLSL(code, func);

	return code;
}

// Shader building method.
GLOWE::String GLOWE::GSILShader::toHLSL(const Pass & pass, const Hash& variant, const ShaderType& shaderType) const
{
	return replaceHLSLForVariant(toHLSL(pass, shaderType), pass, variant, shaderType);
}

GLOWE::String GLOWE::GSILShader::replaceHLSLForVariant(const String& code, const Pass& pass, const Hash& variant, const ShaderType& shaderType) const
{
	static auto findFuncBody = [](const String& code, const String& funcName, Pair<String::ConstIterator, String::ConstIterator>& output) -> bool
	{
		size_t beg = 0, end = 0;

		for (size_t it = code.find(funcName); it != String::invalidPos && (beg == end); it = code.find(funcName, it + 1))
		{
			bool wasFuncBodyFound = false, endLoop = false;
			int bracketCount = 0, roundCount = 0;

			for (unsigned int x = it; x < code.getSize() && !endLoop; ++x)
			{
				switch (code[x])
				{
				case '(':
					++roundCount;
					break;
				case ')':
					--roundCount;
					break;
				case '{':
					if (!wasFuncBodyFound)
					{
						wasFuncBodyFound = true;
						beg = x;
					}
					++bracketCount;
					break;
				case '}':
					--bracketCount;
					if (bracketCount == 0)
					{
						end = x + 1;
						endLoop = true;
					}
					break;
				default:
					continue;
				}

				if (roundCount < 0 || bracketCount < 0)
				{
					beg = end = 0;
					endLoop = true;
				}
			}
		}

		if (beg != end)
		{
			output.first = std::next(code.begin(), beg);
			output.second = std::next(code.begin(), end);
		}

		return beg != end;
	};

	const Pass::ShaderDefinition* _shaderDef{};
	switch (shaderType)
	{
	case ShaderType::VertexShader:
		_shaderDef = &pass.vertexShader;
		break;
	case ShaderType::PixelShader:
		_shaderDef = &pass.pixelShader;
		break;
	case ShaderType::ComputeShader:
		_shaderDef = &pass.computeShader;
		break;
	case ShaderType::GeometryShader:
	case ShaderType::GeometryShaderSO:
		_shaderDef = &pass.geometryShader;
		break;
	default:
		return String();
	}
	const Pass::ShaderDefinition& shaderDef = *_shaderDef;

	const Function* _func = nullptr;
	for (const auto& x : functions)
	{
		if (x.name == shaderDef.func)
		{
			_func = &x;
			break;
		}
	}

	if (!_func)
	{
		return String();
	}

	String result = code;

	const Function& func = *_func;

	const Vector<int>& replacementsForVariant = pass.owner->variants.at(variant);
	Vector<Pair<String, String>> replacements;

	static Regex regex(u8R"TXT((?:([\w\s]+)\s+)?(\w+[\w\s<>]*)\s+(\w+)\s*(?:\[\s*(\d+)\s*\])?)TXT");
	//Regex funcRegex((u8R"TXT(\w+\s+)TXT" + func.name + u8R"TXT(\s*\((?:\w|\s|\n)*\)\s*\{((?:.|\n)+)\})TXT").getCharArray());
	RegexMatch match;

	if (func.arguments.size() > 0)
	{
		unsigned int currentReplace = 0;

		for (const auto& x : func.arguments)
		{
			std::regex_search(x.getString(), match, regex);
			if (match[1].matched)
			{
				// Uniform.
				const auto tokens = String(match[1].first, match[1].second).tokenize(" ");
				for (const auto& modifier : tokens)
				{
					if (modifier == "Uniform")
					{
						if (!variant.isHashed())
						{
							replacements.emplace_back(String(match[3].first, match[3].second), shaderDef.replacements[currentReplace]);
						}
						else
						{
							replacements.emplace_back(String(match[3].first, match[3].second), toString(replacementsForVariant[currentReplace]));
						}
						++currentReplace;
						break;
					}
				}
			}
		}
	}

	Pair<String::ConstIterator, String::ConstIterator> its;

	if (replacements.size() && findFuncBody(result, func.name, its))
	{
		String funcCode(its.first, its.second);
		for (const auto& replacement : replacements)
		{
			funcCode.replace(replacement.first, replacement.second);
		}

		result.replace(its.first, its.second, funcCode.begin(), funcCode.end());
	}

	return result;
}

GLOWE::String GLOWE::GSILShader::replaceHLSLForFuncName(const String& code, const String& func, const Vector<Pair<String, String>>& replacements)
{
	static auto findFuncBody = [](const String& code, const String& funcName, Pair<String::ConstIterator, String::ConstIterator>& output) -> bool
	{
		size_t beg = 0, end = 0;

		for (size_t it = code.find(funcName); it != String::invalidPos && (beg == end); it = code.find(funcName, it + 1))
		{
			bool wasFuncBodyFound = false, endLoop = false;
			int bracketCount = 0, roundCount = 0;

			for (unsigned int x = it; x < code.getSize() && !endLoop; ++x)
			{
				switch (code[x])
				{
				case '(':
					++roundCount;
					break;
				case ')':
					--roundCount;
					break;
				case '{':
					if (!wasFuncBodyFound)
					{
						wasFuncBodyFound = true;
						beg = x;
					}
					++bracketCount;
					break;
				case '}':
					--bracketCount;
					if (bracketCount == 0)
					{
						end = x + 1;
						endLoop = true;
					}
					break;
				default:
					continue;
				}

				if (roundCount < 0 || bracketCount < 0)
				{
					beg = end = 0;
					endLoop = true;
				}
			}
		}

		if (beg != end)
		{
			output.first = std::next(code.begin(), beg);
			output.second = std::next(code.begin(), end);
		}

		return beg != end;
	};

	String result = code;

	Pair<String::ConstIterator, String::ConstIterator> its;

	if (replacements.size() && findFuncBody(result, func, its))
	{
		String funcCode(its.first, its.second);
		for (const auto& replacement : replacements)
		{
			funcCode.replace(replacement.first, replacement.second);
		}

		result.replace(its.first, its.second, funcCode.begin(), funcCode.end());
	}

	return result;
}

class GSILErrorListener final
	: public antlr4::BaseErrorListener
{
public:
	bool error = false;
	GLOWE::Deque<GLOWE::String> errorQueue;

	void raiseError(size_t line, size_t charPositionInLine, const std::string& msg)
	{
		error = true;
		errorQueue.emplace_back(GLOWE::toString(line) + ":" + GLOWE::toString(charPositionInLine) + ": " + msg);
	}

	void raiseError(antlr4::Token* token, const std::string& msg)
	{
		raiseError(token->getLine(), token->getCharPositionInLine(), msg);
	}

	void syntaxError(antlr4::Recognizer*, antlr4::Token*, size_t line, size_t charPositionInLine, const std::string& msg, std::exception_ptr) override
	{
		raiseError(line, charPositionInLine, msg);
	}
};

class GSILVisitor final
	: public GSIL::GSILParserBaseVisitor
{
private:
	GSILErrorListener& errorListener;

	GLOWE::GSILShader& shader;
	const GLOWE::String& shaderCode;
	GLOWE::StringView currentTechniqueGroup = "";
	GLOWE::GSILShader::Pass* currentPass = nullptr;

	struct StatePropertyName
	{
		GLOWE::StringView name = "";
		bool variableArrayPresent = false;
		int variableArrayInt = 1;
	};
public:
	GSILVisitor(GLOWE::GSILShader& sh, const GLOWE::String& code, GSILErrorListener& errListener)
		: shader(sh), shaderCode(code), errorListener(errListener)
	{}

	void visitPropertyDefinition(const GLOWE::String& name, const GSIL::GSILParser::PropertyDefinitionContext* propertyCtx, GSIL::GSILParser::ShaderInputAccessSpecifierContext* inputAccessCtx, GLOWE::GSILShader::Property& property) const
	{
		if (propertyCtx)
		{
			property.name = visitStringLiteral(propertyCtx->EditorName);
		}
		else
		{
			property.name = convertPropertyName(name);
		}

		if (inputAccessCtx)
		{
			if (inputAccessCtx->EngineSupplied())
			{
				property.type = GLOWE::GSILShader::Property::Type::EngineSupplied;
			}
			if (inputAccessCtx->Hidden())
			{
				property.type = GLOWE::GSILShader::Property::Type::Hidden;
			}
		}
	}

	GLOWE::StringView visitStringLiteral(const antlr4::Token* token) const
	{
		auto val = getTokenString(token);
		val.remove_prefix(1);
		val.remove_suffix(1);
		return val;
	}

	GLOWE::StringView getTokenString(const antlr4::Token* token) const
	{
		const GLOWE::StringView v = shaderCode;
		const auto beg = token->getStartIndex(), end = token->getStopIndex();
		return v.substr(beg, end - beg + 1);
	}

	GLOWE::StringView getTokenString(const antlr4::tree::TerminalNode* node) const
	{
		return getTokenString(node->getSymbol());
	}

	GLOWE::StringView getContextString(const antlr4::ParserRuleContext* ctx) const
	{
		const GLOWE::StringView v = shaderCode;
		const auto beg = ctx->getStart()->getStartIndex(), end = ctx->getStop()->getStopIndex();
		return v.substr(beg, end - beg + 1);
	}

	int visitIntegralIdentifier(const antlr4::Token* token) const
	{
		auto val = getTokenString(token);
		int base = 10;
		if (val.size() >= 2)
		{
			const auto prefix = val.substr(0, 2);
			if (prefix == "0x" || prefix == "0X")
			{
				base = 16;
				val.remove_prefix(2);
			}
		}
		return val.toInt(base);
	}

	int visitIntegralIdentifier(const antlr4::tree::TerminalNode* node) const
	{
		return visitIntegralIdentifier(node->getSymbol());
	}

	std::any visitPassShaderKeyValuePair(GSIL::GSILParser::PassShaderKeyValuePairContext* ctx) override
	{
		using namespace GLOWE;

		const auto shaderName = getTokenString(ctx->ShaderName());
		const auto compileShader = ctx->compileShader();
		GSILShader::Pass::ShaderDefinition* shaderDef;

		if (shaderName == "VertexShader")
		{
			shaderDef = &currentPass->vertexShader;
		}
		else if (shaderName == "PixelShader")
		{
			shaderDef = &currentPass->pixelShader;
		}
		else if (shaderName == "ComputeShader")
		{
			shaderDef = &currentPass->computeShader;
		}
		else if (shaderName == "GeometryShader")
		{
			shaderDef = &currentPass->geometryShader;
		}
		else if (shaderName == "HullShader")
		{
			shaderDef = &currentPass->hullShader;
		}
		else if (shaderName == "DomainShader")
		{
			shaderDef = &currentPass->domainShader;
		}
		else
		{
			errorListener.raiseError(ctx->ShaderName()->getSymbol(), "invalid shader name");
			return {};
		}

		shaderDef->func = getTokenString(compileShader->MainFunc);
		shaderDef->version = getTokenString(compileShader->Version).toFloat();

		for (const auto x : compileShader->Replacements)
		{
			shaderDef->replacements.emplace_back(getTokenString(x->Replacement));
		}

		return {};
	}

	std::any visitPassStateKeyValuePair(GSIL::GSILParser::PassStateKeyValuePairContext* ctx) override
	{
		using namespace GLOWE;

		const auto val = getTokenString(ctx->Identifier());
		if (ctx->stateNames()->BlendState())
		{
			if (currentPass->blendStateId != -1)
			{
				errorListener.raiseError(ctx->getStart(), "duplicate blend state assignment");
			}

			for (unsigned int x = 0; x < shader.blendStates.size(); ++x)
			{
				if (val == shader.blendStates[x].first)
				{
					currentPass->blendStateId = x;
					break;
				}
			}

			if (currentPass->blendStateId == -1)
			{
				errorListener.raiseError(ctx->getStart(), "invalid blend state name");
			}
		}
		else
		if (ctx->stateNames()->DepthStencilState())
		{
			if (currentPass->dssId != -1)
			{
				errorListener.raiseError(ctx->getStart(), "duplicate depth stencil state assignment");
			}

			for (unsigned int x = 0; x < shader.depthStencilStates.size(); ++x)
			{
				if (val == shader.depthStencilStates[x].first)
				{
					currentPass->dssId = x;
					break;
				}
			}

			if (currentPass->dssId == -1)
			{
				errorListener.raiseError(ctx->getStart(), "invalid depth stencil state name");
			}
		}
		else
		if (ctx->stateNames()->RasterizerState())
		{
			if (currentPass->rastId != -1)
			{
				errorListener.raiseError(ctx->getStart(), "duplicate rasterizer state assignment");
			}

			for (unsigned int x = 0; x < shader.rasterizerStates.size(); ++x)
			{
				if (val == shader.rasterizerStates[x].first)
				{
					currentPass->rastId = x;
					break;
				}
			}

			if (currentPass->rastId == -1)
			{
				errorListener.raiseError(ctx->getStart(), "invalid rasterizer state name");
			}
		}

		return {};
	}

	std::any visitStatePropertyName(GSIL::GSILParser::StatePropertyNameContext* ctx) override
	{
		StatePropertyName result;
		result.name = getTokenString(ctx->Name);
		result.variableArrayPresent = ctx->variableArrayPart() != nullptr;
		if (result.variableArrayPresent)
		{
			result.variableArrayInt = visitIntegralIdentifier(ctx->variableArrayPart()->IntegralLiteral());
		}
		return result;
	}

	std::any visitStatePropertyValue(GSIL::GSILParser::StatePropertyValueContext* ctx) override
	{
		if (ctx->IntegralLiteral())
		{
			return visitIntegralIdentifier(ctx->IntegralLiteral());
		}
		if (ctx->False())
		{
			return false;
		}
		if (ctx->True())
		{
			return true;
		}
		if (ctx->Identifier())
		{
			return getTokenString(ctx->Identifier());
		}
		if (ctx->propertyColorValue())
		{
			int r = visitIntegralIdentifier(ctx->propertyColorValue()->Red),
			g = visitIntegralIdentifier(ctx->propertyColorValue()->Green),
			b = visitIntegralIdentifier(ctx->propertyColorValue()->Blue),
			a = 255;
			if (ctx->propertyColorValue()->Alpha)
			{
				a = visitIntegralIdentifier(ctx->propertyColorValue()->Alpha);
			}

			if (r > 255 || g > 255 || b > 255 || a > 255 || r < 0 || g < 0 || b < 0 || a < 0)
			{
				errorListener.raiseError(ctx->propertyColorValue()->getStart(), "invalid color value; should be in [0, 255] range");
			}

			return GLOWE::Color::from255RGBA(r, g, b, a);
		}
		return {};
	}

	std::any visitRasterizerStateDefinition(GSIL::GSILParser::RasterizerStateDefinitionContext* ctx) override
	{
		using namespace GLOWE;
		RasterizerDesc desc;

		for (const auto x : ctx->Def->Properties)
		{
			if (x->Property->variableArrayPart())
			{
				errorListener.raiseError(x->Property->variableArrayPart()->getStart(), "unexpected variable array part for RasterizerState");
				continue;
			}

			const auto name = std::any_cast<StatePropertyName>(visitStatePropertyName(x->Property));
			const auto valAny = visitStatePropertyValue(x->Value);
			
			if (name.name == u8"CullingMode")
			{
				const auto val = std::any_cast<StringView>(valAny);
				if (val == u8"None")
				{
					desc.cullingMode = RasterizerDesc::CullingMode::None;
				}
				else
				if (val == u8"Front")
				{
					desc.cullingMode = RasterizerDesc::CullingMode::Front;
				}
				else
				if (val == u8"Back")
				{
					desc.cullingMode = RasterizerDesc::CullingMode::Back;
				}
			}
			else
			if (name.name == u8"FillMode")
			{
				const auto val = std::any_cast<StringView>(valAny);
				if (val == u8"Solid")
				{
					desc.fillMode = RasterizerDesc::FillMode::Solid;
				}
				else
				if (val == u8"Wireframe")
				{
					desc.fillMode = RasterizerDesc::FillMode::Wireframe;
				}
			}
			else
			if (name.name == u8"FrontCounterClockwise")
			{
				desc.frontCounterClockwise = std::any_cast<bool>(valAny);
			}
			else
			if (name.name == u8"DepthBias")
			{
				desc.depthBias = std::any_cast<int>(valAny);
			}
			else
			if (name.name == u8"DepthBiasClamp")
			{
				desc.depthBiasClamp = std::any_cast<float>(valAny);
			}
			else
			if (name.name == u8"SlopeScaledDepthBias")
			{
				desc.slopeScaledDepthBias = std::any_cast<float>(valAny);
			}
			else
			if (name.name == u8"EnableDepthClip")
			{
				desc.enableDepthClip = std::any_cast<bool>(valAny);
			}
			else
			if (name.name == u8"EnableScissor")
			{
				desc.enableScissor = std::any_cast<bool>(valAny);
			}
			else
			if (name.name == u8"EnableMultisample")
			{
				desc.enableMultisample = std::any_cast<bool>(valAny);
			}
			else
			if (name.name == u8"EnableAntialiasedLines")
			{
				desc.enableAntialiasedLines = std::any_cast<bool>(valAny);
			}
		}

		shader.rasterizerStates.emplace_back(getTokenString(ctx->Def->Name), desc);

		return {};
	}

	std::any visitDepthStencilStateDefinition(GSIL::GSILParser::DepthStencilStateDefinitionContext* ctx) override
	{
		using namespace GLOWE;
		DepthStencilDesc desc;

		for (const auto x : ctx->Def->Properties)
		{
			if (x->Property->variableArrayPart())
			{
				errorListener.raiseError(x->Property->variableArrayPart()->getStart(), "unexpected variable array part for RasterizerState");
				continue;
			}

			const auto name = std::any_cast<StatePropertyName>(visitStatePropertyName(x->Property));
			const auto valAny = visitStatePropertyValue(x->Value);

			auto stringToStencilOp = [](StringView arg, DepthStencilDesc::StencilOperation& op)
			{
#define StrCase(str) \
if(arg == GlowU8Stringify(str)) \
{ \
	op = DepthStencilDesc::StencilOperation::str; \
} \
else

				StrCase(Keep)
				StrCase(Zero)
				StrCase(Replace)
				StrCase(Increment)
				StrCase(Decrement)
				StrCase(IncrementClamp)
				StrCase(DecrementClamp)
				StrCase(Invert) {}
#undef StrCase
			};
			auto stringToCompFunc = [](StringView arg, ComparisonFunction& op)
			{
#define StrCase(str) \
if(arg == GlowU8Stringify(str)) \
{ \
	op = ComparisonFunction::str; \
} \
else

				StrCase(Never)
				StrCase(Less)
				StrCase(Equal)
				StrCase(LessEqual)
				StrCase(Greater)
				StrCase(GreaterEqual)
				StrCase(NotEqual)
				StrCase(Always) {}
#undef StrCase
			};

			if (name.name == u8"EnableDepthTest")
			{
				desc.enableDepthTest = std::any_cast<bool>(valAny);
			}
			else
			if (name.name == u8"EnableStencilTest")
			{
				desc.enableStencilTest = std::any_cast<bool>(valAny);
			}
			else
			if (name.name == u8"DepthWriteMask")
			{
				const auto val = std::any_cast<StringView>(valAny);
				if (val == u8"All")
				{
					desc.depthWriteMask = DepthStencilDesc::DepthWriteMask::All;
				}
				else
				if (val == u8"Zero")
				{
					desc.depthWriteMask = DepthStencilDesc::DepthWriteMask::Zero;
				}
			}
			else
			if (name.name == u8"DepthCompFunc")
			{
				const auto val = std::any_cast<StringView>(valAny);
				stringToCompFunc(val, desc.depthCompFunc);
			}
			else
			if (name.name == u8"StencilReadMask")
			{
				desc.stencilReadMask = std::any_cast<int>(valAny);
			}
			else
			if (name.name == u8"StencilWriteMask")
			{
				desc.stencilWriteMask = std::any_cast<int>(valAny);
			}
			else
			if (name.name == u8"FrontFaceFailOp")
			{
				const auto val = std::any_cast<StringView>(valAny);
				stringToStencilOp(val, desc.frontFace.failOp);
			}
			else
			if (name.name == u8"FrontFaceDepthFailOp")
			{
				const auto val = std::any_cast<StringView>(valAny);
				stringToStencilOp(val, desc.frontFace.depthFailOp);
			}
			else
			if (name.name == u8"FrontFaceSuccOp")
			{
				const auto val = std::any_cast<StringView>(valAny);
				stringToStencilOp(val, desc.frontFace.succOp);
			}
			else
			if (name.name == u8"FrontFaceComparisonFunc")
			{
				const auto val = std::any_cast<StringView>(valAny);
				stringToCompFunc(val, desc.frontFace.comparisonFunc);
			}
			else
			if (name.name == u8"BackFaceFailOp")
			{
				const auto val = std::any_cast<StringView>(valAny);
				stringToStencilOp(val, desc.backFace.failOp);
			}
			else
			if (name.name == u8"BackFaceDepthFailOp")
			{
				const auto val = std::any_cast<StringView>(valAny);
				stringToStencilOp(val, desc.backFace.depthFailOp);
			}
			else
			if (name.name == u8"BackFaceSuccOp")
			{
				const auto val = std::any_cast<StringView>(valAny);
				stringToStencilOp(val, desc.backFace.succOp);
			}
			else
			if (name.name == u8"BackFaceComparisonFunc")
			{
				const auto val = std::any_cast<StringView>(valAny);
				stringToCompFunc(val, desc.backFace.comparisonFunc);
			}
		}

		shader.depthStencilStates.emplace_back(getTokenString(ctx->Def->Name), desc);

		return {};
	}

	std::any visitSamplerDefinition(GSIL::GSILParser::SamplerDefinitionContext* ctx) override
	{
		using namespace GLOWE;
		SamplerDesc desc;

		for (const auto x : ctx->Def->Properties)
		{
			if (x->Property->variableArrayPart())
			{
				errorListener.raiseError(x->Property->variableArrayPart()->getStart(), "unexpected variable array part for RasterizerState");
				continue;
			}

			const auto name = std::any_cast<StatePropertyName>(visitStatePropertyName(x->Property));
			const auto valAny = visitStatePropertyValue(x->Value);

			if (name.name == u8"AnisotropyLevel")
			{
				desc.anisotropyLevel = std::any_cast<int>(valAny);
			}
			else
			if (name.name == u8"MinFilter")
			{
				const auto val = std::any_cast<StringView>(valAny);
				if (val == u8"Linear")
				{
					desc.minFilter = SamplerDesc::Filter::Linear;
				}
				else
				if (val == u8"Nearest")
				{
					desc.minFilter = SamplerDesc::Filter::Nearest;
				}
			}
			else
			if (name.name == u8"MagFilter")
			{
				const auto val = std::any_cast<StringView>(valAny);
				if (val == u8"Linear")
				{
					desc.magFilter = SamplerDesc::Filter::Linear;
				}
				else
				if (val == u8"Nearest")
				{
					desc.magFilter = SamplerDesc::Filter::Nearest;
				}
			}
			else
			if (name.name == u8"MipFilter")
			{
				const auto val = std::any_cast<StringView>(valAny);
				if (val == u8"Linear")
				{
					desc.mipFilter = SamplerDesc::Filter::Linear;
				}
				else
				if (val == u8"Nearest")
				{
					desc.mipFilter = SamplerDesc::Filter::Nearest;
				}
			}
			else
			if (name.name == u8"MinLOD")
			{
				desc.minLOD = std::any_cast<float>(valAny);
			}
			else
			if (name.name == u8"MaxLOD")
			{
				desc.maxLOD = std::any_cast<float>(valAny);
			}
			else
			if (name.name == u8"MipBiasLOD")
			{
				desc.mipBiasLOD = std::any_cast<float>(valAny);
			}
			else
			if (name.name == u8"EnableComparison")
			{
				desc.enableComparison = std::any_cast<bool>(valAny);
			}
			else
			if (name.name == u8"AddressingModeU")
			{
				const auto val = std::any_cast<StringView>(valAny);
				if (val == u8"Wrap")
				{
					desc.addressU = SamplerDesc::TextureAddressingMode::Wrap;
				}
				else
				if (val == u8"Clamp")
				{
					desc.addressU = SamplerDesc::TextureAddressingMode::Clamp;
				}
				else
				if (val == u8"Border")
				{
					desc.addressU = SamplerDesc::TextureAddressingMode::Border;
				}
				else
				if (val == u8"Mirror")
				{
					desc.addressU = SamplerDesc::TextureAddressingMode::Mirror;
				}
				else
				if (val == u8"MirrorOnce")
				{
					desc.addressU = SamplerDesc::TextureAddressingMode::MirrorOnce;
				}
			}
			else
			if (name.name == u8"AddressingModeV")
			{
				const auto val = std::any_cast<StringView>(valAny);
				if (val == u8"Wrap")
				{
					desc.addressV = SamplerDesc::TextureAddressingMode::Wrap;
				}
				else
				if (val == u8"Clamp")
				{
					desc.addressV = SamplerDesc::TextureAddressingMode::Clamp;
				}
				else
				if (val == u8"Border")
				{
					desc.addressV = SamplerDesc::TextureAddressingMode::Border;
				}
				else
				if (val == u8"Mirror")
				{
					desc.addressV = SamplerDesc::TextureAddressingMode::Mirror;
				}
				else
				if (val == u8"MirrorOnce")
				{
					desc.addressV = SamplerDesc::TextureAddressingMode::MirrorOnce;
				}
			}
			else
			if (name.name == u8"AddressingModeW")
			{
				const auto val = std::any_cast<StringView>(valAny);
				if (val == u8"Wrap")
				{
					desc.addressW = SamplerDesc::TextureAddressingMode::Wrap;
				}
				else
				if (val == u8"Clamp")
				{
					desc.addressW = SamplerDesc::TextureAddressingMode::Clamp;
				}
				else
				if (val == u8"Border")
				{
					desc.addressW = SamplerDesc::TextureAddressingMode::Border;
				}
				else
				if (val == u8"Mirror")
				{
					desc.addressW = SamplerDesc::TextureAddressingMode::Mirror;
				}
				else
				if (val == u8"MirrorOnce")
				{
					desc.addressW = SamplerDesc::TextureAddressingMode::MirrorOnce;
				}
			}
			else
			if (name.name == u8"BorderColor")
			{
				desc.borderColor = std::any_cast<Color>(valAny);
			}
			else
			if (name.name == u8"ComparisonFunction")
			{
				const auto val = std::any_cast<StringView>(valAny);
				if (val == u8"Never")
				{
					desc.compFunc = ComparisonFunction::Never;
				}
				else
				if (val == u8"Less")
				{
					desc.compFunc = ComparisonFunction::Less;
				}
				else
				if (val == u8"Equal")
				{
					desc.compFunc = ComparisonFunction::Equal;
				}
				else
				if (val == u8"LessEqual")
				{
					desc.compFunc = ComparisonFunction::LessEqual;
				}
				else
				if (val == u8"Greater")
				{
					desc.compFunc = ComparisonFunction::Greater;
				}
				else
				if (val == u8"GreaterEqual")
				{
					desc.compFunc = ComparisonFunction::GreaterEqual;
				}
				else
				if (val == u8"NotEqual")
				{
					desc.compFunc = ComparisonFunction::NotEqual;
				}
				else
				if (val == u8"Always")
				{
					desc.compFunc = ComparisonFunction::Always;
				}
			}
		}

		shader.samplers.emplace_back(getTokenString(ctx->Def->Name), desc);

		return {};
	}

	std::any visitBlendStateDefinition(GSIL::GSILParser::BlendStateDefinitionContext* ctx) override
	{
		using namespace GLOWE;
		BlendDesc desc;

		for (const auto x : ctx->Def->Properties)
		{
			const auto name = std::any_cast<StatePropertyName>(visitStatePropertyName(x->Property));
			const auto valAny = visitStatePropertyValue(x->Value);
			
			auto stringToBlendOption = [](StringView arg, BlendDesc::RenderTargetBlendDesc::BlendOption& op)
			{
#define StrCase(str) \
if(arg == GlowU8Stringify(str)) \
{ \
	op = BlendDesc::RenderTargetBlendDesc::BlendOption::str; \
} \
else

				StrCase(Zero)
				StrCase(One)
				StrCase(SrcColor)
				StrCase(InvSrcColor)
				StrCase(SrcAlpha)
				StrCase(InvSrcAlpha)
				StrCase(DestAlpha) 
				StrCase(InvDestAlpha)
				StrCase(DestColor)
				StrCase(InvDestColor)
				StrCase(SrcAlphaClamp)
				StrCase(BlendFactor)
				StrCase(InvBlendFactor)
				StrCase(Src1Color)
				StrCase(InvSrc1Color)
				StrCase(Src1Alpha)
				StrCase(InvSrc1Alpha) {}
#undef StrCase
			};
			auto stringToBlendOperation = [](StringView arg, BlendDesc::RenderTargetBlendDesc::BlendOperation& op)
			{
#define StrCase(str) \
if(arg == GlowU8Stringify(str)) \
{ \
	op = BlendDesc::RenderTargetBlendDesc::BlendOperation::str; \
} \
else

				StrCase(Add)
				StrCase(Sub)
				StrCase(RevSub)
				StrCase(Min)
				StrCase(Max) {}
#undef StrCase
			};

			const int id = name.variableArrayPresent ? name.variableArrayInt : 0;

			if (name.name == u8"EnableAlphaToCoverage")
			{
				desc.enableAlphaToCoverage = std::any_cast<bool>(valAny);
			}
			else
			if (name.name == u8"EnableIndependentBlend")
			{
				desc.enableIndependentBlend = std::any_cast<bool>(valAny);
			}
			else
			if (name.name == u8"EnableBlend")
			{
				desc.renderTargetDescs[id].enableBlend = std::any_cast<bool>(valAny);
			}
			else
			if (name.name == u8"SrcBlend")
			{
				stringToBlendOption(std::any_cast<StringView>(valAny), desc.renderTargetDescs[id].srcBlend);
			}
			else
			if (name.name == u8"DestBlend")
			{
				stringToBlendOption(std::any_cast<StringView>(valAny), desc.renderTargetDescs[id].destBlend);
			}
			else
			if (name.name == u8"BlendOperation")
			{
				stringToBlendOperation(std::any_cast<StringView>(valAny), desc.renderTargetDescs[id].blendOperation);
			}
			else
			if (name.name == u8"SrcBlendAlpha")
			{
				stringToBlendOption(std::any_cast<StringView>(valAny), desc.renderTargetDescs[id].srcBlendAlpha);
			}
			else
			if (name.name == u8"DestBlendAlpha")
			{
				stringToBlendOption(std::any_cast<StringView>(valAny), desc.renderTargetDescs[id].destBlendAlpha);
			}
			else
			if (name.name == u8"BlendOperationAlpha")
			{
				stringToBlendOperation(std::any_cast<StringView>(valAny), desc.renderTargetDescs[id].blendOperationAlpha);
			}
			else
			if (name.name == u8"WriteMask")
			{
				desc.renderTargetDescs[id].writeMask = std::any_cast<int>(valAny);
			}
			else
			if (name.name == u8"BlendFactor")
			{
				desc.blendFactor = std::any_cast<Color>(valAny);
			}
			else
			if (name.name == u8"SampleMask")
			{
				desc.sampleMask = std::any_cast<int>(valAny);
			}
		}

		shader.blendStates.emplace_back(getTokenString(ctx->Def->Name), desc);

		return {};
	}

	std::any visitCbufferDefinition(GSIL::GSILParser::CbufferDefinitionContext* ctx) override
	{
		using namespace GLOWE;
		GSILShader::Structure cbuffer;
		cbuffer.name = getTokenString(ctx->Name);
		cbuffer.isInOut = false;
		cbuffer.owningShader = &shader;

		for (const auto x : ctx->Fields)
		{
			cbuffer.fields.emplace_back(std::any_cast<GSILShader::Structure::Field>(visitStructField(x)));
		}

		shader.constantBuffers.emplace_back(std::move(cbuffer));

		return {};
	}

	std::any visitStructDefinition(GSIL::GSILParser::StructDefinitionContext* ctx) override
	{
		using namespace GLOWE;
		GSILShader::Structure struc;
		struc.name = getTokenString(ctx->Name);
		struc.isInOut = ctx->Specifiers != nullptr;
		struc.owningShader = &shader;

		for (const auto x : ctx->Fields)
		{
			struc.fields.emplace_back(std::any_cast<GSILShader::Structure::Field>(visitStructField(x)));
		}

		shader.structures.emplace_back(std::move(struc));

		return {};
	}

	std::any visitStructField(GSIL::GSILParser::StructFieldContext* ctx) override
	{
		using namespace GLOWE;
		GSILShader::Structure::Field field;

		field.type = GSILShader::Structure::Field::Type::Unknown;
		field.name = getTokenString(ctx->simpleVariableDefinition()->Name);
		field.property.name = convertPropertyName(field.name);

		const int arrSize = ctx->simpleVariableDefinition()->ArrayParts.empty() ? 0 : visitIntegralIdentifier(ctx->simpleVariableDefinition()->ArrayParts[0]->IntegralLiteral());
		if (ctx->simpleVariableDefinition()->ArrayParts.size() > 1)
		{
			errorListener.raiseError(ctx->simpleVariableDefinition()->getStart(), "invalid array dimension count; max supported is 1");
		}
		if (arrSize < 0)
		{
			errorListener.raiseError(ctx->simpleVariableDefinition()->getStart(), "invalid array size: " + getTokenString(ctx->simpleVariableDefinition()->ArrayParts[0]->IntegralLiteral()));
		}
		else
		{
			field.arraySize = arrSize;
		}

		const auto type = ctx->simpleVariableDefinition()->Type;
		if (type->Matrix())
		{
			constexpr auto aPos = sizeof("Matrix") - 1, bPos = aPos + 2;
			const auto val = getTokenString(type->Matrix());
			field.typesCount = val[aPos] - '0';
			field.typesCountMat = val[bPos] - '0';
			field.type = GSILShader::Structure::Field::Type::Float;
		}
		else
		if (type->DMatrix())
		{
			constexpr auto aPos = sizeof("DMatrix") - 1, bPos = aPos + 2;
			const auto val = getTokenString(type->DMatrix());
			field.typesCount = val[aPos] - '0';
			field.typesCountMat = val[bPos] - '0';
			field.type = GSILShader::Structure::Field::Type::Double;
		}
		else
		if (type->Identifier())
		{
			const auto val = getTokenString(type->Identifier());
			for (unsigned int x = 0; x < shader.structures.size(); ++x)
			{
				if (val == shader.structures[x].name)
				{
					field.type = GSILShader::Structure::Field::Type::Struct;
					field.typesCount = x;
					field.typesCountMat = shader.structures[x].generateBasicLayout().getSize();
					break;
				}
			}
		}
		else
		{
			auto val = getTokenString(type->VectorType());
			unsigned int size = 1;
			if (std::isdigit(val.back()))
			{
				size = val.back() - '0';
				val.remove_suffix(1);
			}
			field.typesCount = size;
			field.typesCountMat = 0;
			
			if (val == "Double")
			{
				field.type = GSILShader::Structure::Field::Type::Double;
			}
			else if (val == "Bool")
			{
				field.type = GSILShader::Structure::Field::Type::Bool;
			}
			else if (val == "Float")
			{
				field.type = GSILShader::Structure::Field::Type::Float;
			}
			else if (val == "Int")
			{
				field.type = GSILShader::Structure::Field::Type::Int;
			}
			else // if (val == "UInt")
			{
				field.type = GSILShader::Structure::Field::Type::UInt;
			}
		}

		if (field.type == GSILShader::Structure::Field::Type::Unknown)
		{
			errorListener.raiseError(type->getStart(), "invalid type");
		}
		
		for (const auto& x : ctx->InputSpecifier)
		{
			if (x->InterpolationModeSpecifier())
			{
				using InterpolationSpecifier = GSILShader::Structure::Field::InterpolationSpecifier;
				const auto val = getTokenString(x->InterpolationModeSpecifier());
				if (val == "NoInterpolation")
				{
					field.interpolation = InterpolationSpecifier::NoInterpolation;
				}
				else
				if (val == "NoPerspective")
				{
					field.interpolation = InterpolationSpecifier::NoPerspective;
				}
				else
				{
					field.interpolation = InterpolationSpecifier::Smooth;
				}
			}
			if (x->SVSpecifier())
			{
				field.isSV = true;
			}
			if (x->Instanced())
			{
				field.isInstanceData = true;
			}
			if (x->shaderInputAccessSpecifier())
			{
				if (x->shaderInputAccessSpecifier()->EngineSupplied())
				{
					field.property.type = GSILShader::Property::Type::EngineSupplied;
				}
				if (x->shaderInputAccessSpecifier()->Hidden())
				{
					field.property.type = GSILShader::Property::Type::Hidden;
				}
			}
		}

		return field;
	}

	std::any visitTextureDefinition(GSIL::GSILParser::TextureDefinitionContext* ctx) override
	{
		using namespace GLOWE;
		using Type = TextureDesc::Type;

		GSILShader::Texture texDesc;
		
		texDesc.name = getTokenString(ctx->Name);
		visitPropertyDefinition(texDesc.name, ctx->propertyDefinition(), ctx->shaderInputAccessSpecifier(), texDesc.property);
		texDesc.isMultiSample = false;

		constexpr auto typePos = sizeof("Texture") - 1;
		const auto str = getTokenString(ctx->TextureType()).substr(typePos);
		if (str == u8"1D")
		{
			texDesc.type = Type::Texture1D;
		}
		else
		if (str == u8"2D")
		{
			texDesc.type = Type::Texture2D;
		}
		else
		if (str == u8"3D")
		{
			texDesc.type = Type::Texture3D;
		}
		else
		if (str == u8"Cube")
		{
			texDesc.type = Type::TextureCube;
		}
		else
		if (str == u8"1DArray")
		{
			texDesc.type = Type::Texture1DArray;
		}
		else
		if (str == u8"2DArray")
		{
			texDesc.type = Type::Texture2DArray;
		}
		else
		if (str == u8"1DMultiSample")
		{
			texDesc.type = Type::Texture1D;
			texDesc.isMultiSample = true;
		}
		else
		if (str == u8"2DMultiSample")
		{
			texDesc.type = Type::Texture2D;
			texDesc.isMultiSample = true;
		}
		else
		{
			errorListener.raiseError(ctx->TextureType()->getSymbol(), "invalid texture type");
		}
		
		bool samplerFound = false;
		const auto samplerName = getTokenString(ctx->SamplerName);
		for (unsigned int y = 0; y < shader.samplers.size(); ++y)
		{
			if (samplerName == shader.samplers[y].first)
			{
				texDesc.samplerID = y;
				samplerFound = true;
				break;
			}
		}

		if (!samplerFound)
		{
			errorListener.raiseError(ctx->SamplerName, "invalid sampler name");
		}

		shader.textures.emplace_back(texDesc);

		return {};
	}

	std::any visitUavDefinition(GSIL::GSILParser::UavDefinitionContext* ctx) override
	{
		using namespace GLOWE;
		GSILShader::UAV desc;

		desc.name = getTokenString(ctx->Name);
		visitPropertyDefinition(desc.name, ctx->propertyDefinition(), ctx->shaderInputAccessSpecifier(), desc.property);

		const auto uavType = getTokenString(ctx->UAVType()).substr(sizeof("RW") - 1);
		if (uavType == "Texture1D")
		{
			desc.type = GSILShader::UAV::Type::Texture1D;
		}
		else
		if (uavType == "Texture2D")
		{
			desc.type = GSILShader::UAV::Type::Texture2D;
		}
		else
		if (uavType == "Texture3D")
		{
			desc.type = GSILShader::UAV::Type::Texture3D;
		}
		else
		if (uavType == "Texture1DArray")
		{
			desc.type = GSILShader::UAV::Type::Texture1DArray;
		}
		else
		if (uavType == "Texture2DArray")
		{
			desc.type = GSILShader::UAV::Type::Texture2DArray;
		}
		else
		if (uavType == "Buffer")
		{
			desc.type = GSILShader::UAV::Type::Buffer;
		}
		else
		{
			errorListener.raiseError(ctx->UAVType()->getSymbol(), "invalid UAV type");
		}

		auto uavFormat = getTokenString(ctx->Type->getStart());
		if (!uavFormat.empty())
		{
			if (std::isdigit(uavFormat.back()))
			{
				desc.format.elemsCount = uavFormat.back() - '0';
				uavFormat.remove_suffix(1);
			}
			else
			{
				desc.format.elemsCount = 1;
			}

			if (uavFormat == "Float")
			{
				desc.format.type = Format::Type::Float;
			}
			else
			if (uavFormat == "Int")
			{
				desc.format.type = Format::Type::Int;
			}
			else
			if (uavFormat == "UInt")
			{
				desc.format.type = Format::Type::UnsignedInt;
			}
			else
			if (uavFormat == "Byte")
			{
				desc.format.type = Format::Type::Byte;
			}
			else
			if (uavFormat == "UByte")
			{
				desc.format.type = Format::Type::UnsignedByte;
			}
		}
		
		shader.uavs.emplace_back(desc);

		return {};
	}

	std::any visitTagDefinition(GSIL::GSILParser::TagDefinitionContext* ctx) override
	{
		using namespace GLOWE;

		GSILShader::Technique::Tag tag;

		const auto tagName = getTokenString(ctx->Key);
		if (tagName == u8"SupportsDirectionalLights")
		{
			tag.type = GSILShader::Technique::Tag::Type::SupportsDirectionalLights;
			if (ctx->Value->IntegralLiteral())
			{
				tag.supportsLightingInfo.supportedLights = getTokenString(ctx->Value->IntegralLiteral()).toUInt();
			}
			else
			{
				errorListener.raiseError(ctx->Value->getStart(), "invalid tag value, should be an integral literal");
			}
			// TODO: Check for duplicates and emit a warning.
		}
		else
		if (tagName == u8"SupportsPointLights")
		{
			tag.type = GSILShader::Technique::Tag::Type::SupportsPointLights;
			if (ctx->Value->IntegralLiteral())
			{
				tag.supportsLightingInfo.supportedLights = getTokenString(ctx->Value->IntegralLiteral()).toUInt();
			}
			else
			{
				errorListener.raiseError(ctx->Value->getStart(), "invalid tag value, should be an integral literal");
			}
			// TODO: Check for duplicates and emit a warning.
		}
		else
		if (tagName == u8"SupportsSpotLights")
		{
			tag.type = GSILShader::Technique::Tag::Type::SupportsSpotLights;
			if (ctx->Value->IntegralLiteral())
			{
				tag.supportsLightingInfo.supportedLights = getTokenString(ctx->Value->IntegralLiteral()).toUInt();
			}
			else
			{
				errorListener.raiseError(ctx->Value->getStart(), "invalid tag value, should be an integral literal");
			}
			// TODO: Check for duplicates and emit a warning.
		}
		else
		if (tagName == u8"IsTexturePresent")
		{
			unsigned int id{};
			tag.type = GSILShader::Technique::Tag::Type::IsTexturePresent;

			const auto optional = getTokenString(ctx->Value->getStart());
			if (optional == u8"Color")
			{
				id = 0;
			}
			else
			if (optional == u8"Normal")
			{
				id = 1;
			}
			else
			if (ctx->Value->IntegralLiteral())
			{
				id = optional.toUInt();
			}
			else
			{
				errorListener.raiseError(ctx->Value->getStart(), "invalid tag value, should be an integral literal or texture name");
			}
			tag.isTexturePresentInfo.texID = id;
			// TODO: Check for duplicates and emit a warning.
		}
		else
		{
			errorListener.raiseError(ctx->Key, "invalid tag name");
		}

		return tag;
	}
	
	std::any visitTechniqueDefinition(GSIL::GSILParser::TechniqueDefinitionContext* ctx) override
	{
		using namespace GLOWE;
		GSILShader::Technique desc;

		for (const auto x : ctx->Tags)
		{
			desc.tags.emplace_back(std::any_cast<GSILShader::Technique::Tag>(visitTagDefinition(x)));
		}

		for (const auto x : ctx->Passes)
		{
			const auto passPair = desc.passes.emplace(getTokenString(x->Id).toUInt(), desc);
			if (!passPair.second)
			{
				errorListener.raiseError(x->Id, "duplicate pass ID");
				continue;
			}

			auto& pass = passPair.first->second;
			currentPass = &pass;
			for (const auto y : x->Values)
			{
				visit(y);
			}
		}

		// Just to make sure.
		currentPass = nullptr;
		shader.techniqueGroups[currentTechniqueGroup].emplace(getTokenString(ctx->Name), std::move(desc));

		return {};
	}

	std::any visitTechniqueGroupDefinition(GSIL::GSILParser::TechniqueGroupDefinitionContext* ctx) override
	{
		currentTechniqueGroup = getTokenString(ctx->Name);

		for (const auto x : ctx->Techniques)
		{
			visitTechniqueDefinition(x);
		}

		currentTechniqueGroup = "";

		return {};
	}

	std::any visitFunctionDefinition(GSIL::GSILParser::FunctionDefinitionContext* ctx) override
	{
		using namespace GLOWE;
		GSILShader::Function desc;

		desc.name = getTokenString(ctx->Name);
		desc.returnType = getTokenString(ctx->ReturnType->Type);

		for (const auto x : ctx->Attributes)
		{
			desc.attributes.emplace_back(getContextString(x->Attribute));
		}

		if (ctx->Parameters->FirstParameter)
		{
			desc.arguments.emplace_back(getContextString(ctx->Parameters->FirstParameter));
		}

		for (const auto x : ctx->Parameters->MoreParameters)
		{
			desc.arguments.emplace_back(getContextString(x->Parameter));
		}

		desc.code = getContextString(ctx->Code->Content);

		for (unsigned int x = 0; x < shader.textures.size(); ++x)
		{
			if (desc.code.find(shader.textures[x].name) != String::invalidPos)
			{
				desc.usedTextures.push_back(x);
			}
		}

		for (unsigned int x = 0; x < shader.uavs.size(); ++x)
		{
			if (desc.code.find(shader.uavs[x].name) != String::invalidPos)
			{
				desc.usedUAVs.push_back(x);
			}
		}

		for (unsigned int x = 0; x < shader.constantBuffers.size(); ++x)
		{
			if (desc.code.find(shader.constantBuffers[x].name) != String::invalidPos)
			{
				desc.usedCBuffers.push_back(x);
			}
			else
			{
				for (const auto& tmp : shader.constantBuffers[x].fields)
				{
					if (desc.code.find(tmp.name) != String::invalidPos)
					{
						desc.usedCBuffers.push_back(x);
						break;
					}
				}
			}
		}

		for (unsigned int x = 0; x < shader.functions.size(); ++x)
		{
			if (desc.name != shader.functions[x].name && desc.code.find(shader.functions[x].name) != String::invalidPos)
			{
				desc.usedFunctions.push_back(x);
			}
		}
		
		shader.functions.emplace_back(desc);

		return {};
	}
};

void GLOWE::GSILShader::fromGSILCode(const String& gsilCode)
{
	using namespace antlr4;

	try
	{
		GSILErrorListener errorListener;
		ANTLRInputStream input(gsilCode);

		GSIL::GSILLexer lexer(&input);
		lexer.removeErrorListeners();
		lexer.addErrorListener(&errorListener);
		CommonTokenStream tokens(&lexer);
		tokens.fill();

		GSIL::GSILParser parser(&tokens);
		parser.removeErrorListeners();
		parser.addErrorListener(&errorListener);

		const auto tree = parser.compilationUnit();
		GSILVisitor visitor(*this, gsilCode, errorListener);

		if (errorListener.error)
		{
			throw CompilationErrorException(std::move(errorListener.errorQueue));
		}

		visitor.visit(tree);

		if (errorListener.error)
		{
			throw CompilationErrorException(std::move(errorListener.errorQueue));
		}

		registerInputLayouts();
	}
	catch (const antlr4::RuntimeException& e)
	{
		throw StringException(e.what());
	}
}

void GLOWE::GSILShader::fromGSILCode(const String& gsilCode, String& tokensOutput)
{
	fromGSILCode(gsilCode);
}

bool GLOWE::GSILShader::checkIsValid() const
{
	return !techniqueGroups.empty();
}

GLOWE::StructureLayout GLOWE::GSILShader::Structure::generateBasicLayout() const
{
	using Type = Structure::Field::Type;
	using FType = Format::Type;

	auto translateType = [](const Structure::Field::Type arg) -> Format::Type
	{
		switch (arg)
		{
		case Type::Float:
		case Type::Double:
			return FType::Float;
		case Type::Int:
			return FType::Int;
		case Type::UInt:
		case Type::Bool:
			return FType::UnsignedInt;
		case Type::Struct:
			return FType::Struct;
		default:
			return FType::Byte;
		}
	};

	StructureLayout layout;

	for (const auto& x : fields)
	{
		if (x.type == Type::Unknown)
		{
			continue; // Ignore invalid fields.
		}
		else
		if (x.type == Type::Struct)
		{
			unsigned int size = owningShader->structures
				[x.typesCount].generateBasicLayout().getSize();
			layout.addElement(Format(FType::Struct, size));
		}
		else
		{
			Format format(translateType(x.type), 0);

			unsigned int arrSiz = std::max(x.arraySize, 1U), vecSize = std::max(x.typesCount, 1U), matSize = std::max(x.typesCountMat, 1U);

			format.elemsCount = arrSiz * vecSize * matSize;

			if (x.type == Type::Double)
			{
				format.elemsCount *= 2U;
			}

			layout.addElement(format);
		}
	}

	return layout;
}

GLOWE::StructureLayout GLOWE::GSILShader::Structure::generateBasicCbufferLayout() const
{
	StructureLayout result = generateBasicCbufferLayoutInternal();
	if ((result.getSize() % 16) != 0)
	{
		result.addElement(Format(Format::Type::Byte, (16 - (result.getSize() % 16)) % 16));
	}

	return result;
}

GLOWE::StructureLayout GLOWE::GSILShader::Structure::generateBasicCbufferLayoutInternal() const
{
	using Type = Structure::Field::Type;
	using FType = Format::Type;

	StructureLayout layout;

	auto translateType = [](const Structure::Field::Type arg) -> Format::Type
	{
		switch (arg)
		{
		case Type::Float:
		case Type::Double:
			return FType::Float;
		case Type::Int:
			return FType::Int;
		case Type::UInt:
		case Type::Bool:
			return FType::UnsignedInt;
		case Type::Struct:
			return FType::Struct;
		default:
			return FType::Byte;
		}
	};
	auto calcSingleElementFormat = [&translateType](const Structure::Field& field) -> Format
	{
		Format format;
		format.type = translateType(field.type);
		const unsigned int rowSize = field.typesCount, colSize = field.typesCountMat; // rowSize == vecSize

		if (colSize > 0)
		{
			// Matrix.
			if (field.type == Structure::Field::Type::Double)
			{
				format.elemsCount = (rowSize - 1) * (colSize <= 2 ? 4 : 8) + colSize * 2;
			}
			else
			{
				format.elemsCount = (rowSize - 1) * 4 + colSize;
			}
		}
		else
		{
			// Scalar/vector.
			format.elemsCount = std::max(rowSize, 1U);
		}

		return format;
	};

	constexpr unsigned int blockSize = 16;
	unsigned int currentBlock = blockSize;

	// (16 - (d % 16)) % 16

	for (const auto& x : fields)
	{
		if (x.type == Type::Unknown)
		{
			continue; // Ignore invalid fields.
		}
		else
		if (x.type == Type::Struct)
		{
			if (currentBlock != blockSize)
			{
				layout.addElement(Format(FType::Byte, currentBlock));
			}

			const unsigned int size = owningShader->structures
				[x.typesCount].generateBasicCbufferLayoutInternal().getSize();
			unsigned int resultSize = 0;
			if (x.arraySize > 0)
			{
				resultSize = (((blockSize - (size % blockSize)) % blockSize) + size) * (x.arraySize - 1);
			}
			
			resultSize += size;
			layout.addElement(Format(FType::Struct, resultSize));
			currentBlock = (blockSize - (size % blockSize)) % blockSize;
			if (currentBlock == 0)
			{
				currentBlock = blockSize;
			}
		}
		else
		{
			const Format format = calcSingleElementFormat(x);

			// Handle arrays.
			if (x.arraySize > 0)
			{
				// Arrays.
				Format resultFormat(FType::Byte, (((blockSize - (format.getSize() % blockSize)) % blockSize) + format.getSize()) * (x.arraySize - 1) + format.getSize());

				// Padding.
				if (currentBlock != blockSize)
				{
					layout.addElement(Format(FType::Byte, currentBlock));
				}

				layout.addElement(resultFormat);
				currentBlock = (blockSize - (format.getSize() % blockSize)) % blockSize;
				if (currentBlock == 0)
				{
					currentBlock = blockSize;
				}
			}
			else
			{
				// Normal elements.
				if (format.getSize() <= blockSize)
				{
					// Normal size.
					if (format.getSize() > currentBlock)
					{
						layout.addElement(Format(FType::Byte, currentBlock));
						currentBlock = blockSize;
					}

					layout.addElement(format);
					currentBlock -= format.getSize();
				}
				else
				{
					// Oversized.
					if (currentBlock != blockSize)
					{
						layout.addElement(Format(FType::Byte, currentBlock));
					}
					layout.addElement(format);
					currentBlock = (blockSize - (format.getSize() % blockSize)) % blockSize;
				}

				if (currentBlock == 0)
				{
					currentBlock = blockSize;
				}
			}
		}
	}

	return layout;
}

GLOWE::NamedStructureLayout GLOWE::GSILShader::Structure::generateLayout() const
{
	using Type = Structure::Field::Type;
	using FType = Format::Type;

	auto translateType = [](const Structure::Field::Type arg) -> Format::Type
	{
		switch (arg)
		{
		case Type::Float:
		case Type::Double:
			return FType::Float;
		case Type::Int:
			return FType::Int;
		case Type::UInt:
			return FType::UnsignedInt;
		case Type::Struct:
			return FType::Struct;
		case Type::Bool:
		default:
			return FType::Byte;
		}
	};

	NamedStructureLayout layout;

	for (const auto& x : fields)
	{
		if (x.type == Type::Unknown)
		{
			continue; // Ignore invalid fields.
		}
		else
		if (x.type == Type::Struct)
		{
			unsigned int size = owningShader->structures
			[x.typesCount].generateBasicLayout().getSize();
			layout.addElement(Format(FType::Byte, size), x.name);
		}
		else
		{
			Format format(translateType(x.type), 0);

			unsigned int arrSiz = std::max(x.arraySize, 1U), vecSize = std::max(x.typesCount, 1U), matSize = std::max(x.typesCountMat, 1U);

			format.elemsCount = arrSiz * vecSize * matSize;

			if (x.type == Type::Double)
			{
				format.elemsCount *= 2U;
			}

			layout.addElement(format, x.name);
		}
	}

	return layout;
}

GLOWE::NamedStructureLayout GLOWE::GSILShader::Structure::generateCbufferLayoutInternal(const bool usePropertyNames) const
{
	using Type = Structure::Field::Type;
	using FType = Format::Type;

	NamedStructureLayout layout;

	auto translateType = [](const Structure::Field::Type arg) -> Format::Type
	{
		switch (arg)
		{
		case Type::Float:
		case Type::Double:
			return FType::Float;
		case Type::Int:
			return FType::Int;
		case Type::UInt:
		case Type::Bool:
			return FType::UnsignedInt;
		case Type::Struct:
			return FType::Struct;
		default:
			return FType::Byte;
		}
	};
	auto calcSingleElementFormat = [&translateType](const Structure::Field& field) -> Format
	{
		Format format;
		format.type = translateType(field.type);
		const unsigned int rowSize = field.typesCount, colSize = field.typesCountMat; // rowSize == vecSize

		if (colSize > 0)
		{
			// Matrix.
			if (field.type == Structure::Field::Type::Double)
			{
				format.elemsCount = (rowSize - 1) * (colSize <= 2 ? 4 : 8) + colSize * 2;
			}
			else
			{
				format.elemsCount = (rowSize - 1) * 4 + colSize;
			}
		}
		else
		{
			// Scalar/vector.
			format.elemsCount = std::max(rowSize, 1U);
		}

		return format;
	};

	constexpr unsigned int blockSize = 16;
	unsigned int currentBlock = blockSize;
	unsigned int counter = 1;

	// (16 - (d % 16)) % 16

	for (const auto& x : fields)
	{
		if (x.type == Type::Unknown)
		{
			continue; // Ignore invalid fields.
		}
		else
		if (x.type == Type::Struct)
		{
			if (currentBlock != blockSize)
			{
				layout.addElement(Format(FType::Byte, currentBlock), toString(counter++));
			}

			const unsigned int size = owningShader->structures
				[x.typesCount].generateBasicCbufferLayoutInternal().getSize(); // We use simple StructureLayout to avoid String copying (we just need size of the processed structure).
			unsigned int resultSize = 0;
			if (x.arraySize > 0)
			{
				resultSize = (((blockSize - (size % blockSize)) % blockSize) + size) * (x.arraySize - 1);
			}

			resultSize += size;
			layout.addElement(Format(FType::Struct, resultSize), usePropertyNames ? x.property.name : x.name);
			currentBlock = (blockSize - (size % blockSize)) % blockSize;
			if (currentBlock == 0)
			{
				currentBlock = blockSize;
			}
		}
		else
		{
			const Format format = calcSingleElementFormat(x);

			// Handle arrays.
			if (x.arraySize > 0)
			{
				// Arrays.
				Format resultFormat(FType::Byte, (((blockSize - (format.getSize() % blockSize)) % blockSize) + format.getSize()) * (x.arraySize - 1) + format.getSize());

				// Padding.
				if (currentBlock != blockSize)
				{
					layout.addElement(Format(FType::Byte, currentBlock), toString(counter++));
				}

				layout.addElement(resultFormat, usePropertyNames ? x.property.name : x.name);
				currentBlock = (blockSize - (format.getSize() % blockSize)) % blockSize;
				if (currentBlock == 0)
				{
					currentBlock = blockSize;
				}
			}
			else
			{
				// Normal elements.
				if (format.getSize() <= blockSize)
				{
					// Normal size.
					if (format.getSize() > currentBlock)
					{
						layout.addElement(Format(FType::Byte, currentBlock), toString(counter++));
						currentBlock = blockSize;
					}

					layout.addElement(format, usePropertyNames ? x.property.name : x.name);
					currentBlock -= format.getSize();
				}
				else
				{
					// Oversized.
					if (currentBlock != blockSize)
					{
						layout.addElement(Format(FType::Byte, currentBlock), toString(counter++));
					}
					layout.addElement(format, usePropertyNames ? x.property.name : x.name);
					currentBlock = (blockSize - (format.getSize() % blockSize)) % blockSize;
				}

				if (currentBlock == 0)
				{
					currentBlock = blockSize;
				}
			}
		}
	}

	return layout;
}

GLOWE::NamedStructureLayout GLOWE::GSILShader::Structure::generateCbufferLayout() const
{
	NamedStructureLayout result = generateCbufferLayoutInternal(false);
	if ((result.getSize() % 16) != 0)
	{
		result.addElement(Format(Format::Type::Byte, (16 - (result.getSize() % 16)) % 16), u8"__AdditionalPadding");
	}

	return result;
}

GLOWE::NamedStructureLayout GLOWE::GSILShader::Structure::generateCbufferPropertyLayout() const
{
	NamedStructureLayout result = generateCbufferLayoutInternal(true);
	if ((result.getSize() % 16) != 0)
	{
		result.addElement(Format(Format::Type::Byte, (16 - (result.getSize() % 16)) % 16), u8"__AdditionalPadding");
	}

	return result;
}

GLOWE::Format GLOWE::GSILShader::Structure::Field::asFormat() const
{
	Format result;

	const unsigned int multiplied = (arraySize == 0 ? 1 : arraySize) * (typesCountMat == 0 ? 1 : typesCountMat) * typesCount;
	result.elemsCount = multiplied;

	switch (type)
	{
	case Type::Struct:
		result.elemsCount = typesCountMat;
		result.type = Format::Type::UnsignedByte;
		break;
	case Type::UInt:
		result.type = Format::Type::UnsignedInt;
		break;
	case Type::Int:
		result.type = Format::Type::Int;
		break;
	case Type::Float:
		result.type = Format::Type::Float;
		break;
	case Type::Double:
		result.type = Format::Type::Float;
		result.elemsCount *= 2;
		break;
	default:
		break;
	}

	return result;
}

void GLOWE::GSILShader::serialize(LoadSaveHandle& handle) const
{
	static auto saveProperty = [&](const Property& prop)
	{
		handle << prop.type << prop.name << ((unsigned int)prop.constraints.size());
		for (const auto& x : prop.constraints)
		{
			handle << x.first << x.second;
		}
	};
	static auto saveShaderDef = [&](const Pass::ShaderDefinition& def)
	{
		handle << def.func << def.version << ((unsigned int)def.replacements.size());
		for (const auto& x : def.replacements)
		{
			handle << x;
		}
		handle << ((unsigned int)def.usedFunctions.size());
		for (const auto& x : def.usedFunctions)
		{
			handle << x;
		}
		handle << ((unsigned int)def.usedTextures.size());
		for (const auto& x : def.usedTextures)
		{
			handle << x;
		}
		handle << ((unsigned int)def.usedCBuffers.size());
		for (const auto& x : def.usedCBuffers)
		{
			handle << x;
		}
		handle << ((unsigned int)def.usedUAVs.size());
		for (const auto& x : def.usedUAVs)
		{
			handle << x;
		}
	};
	static auto saveStruct = [&](const Structure& arg)
	{
		static auto saveField = [&](const Structure::Field& field)
		{
			handle << field.arraySize << field.interpolation << field.isSV << field.isInstanceData << field.name << field.type << field.typesCount << field.typesCountMat;
			saveProperty(field.property);
		};

		handle << ((unsigned int)arg.fields.size());
		for (const auto & x : arg.fields)
		{
			saveField(x);
		}
		handle << arg.isInOut << arg.name;
	};

	handle.write<unsigned int>(textures.size());
	for (const auto& x : textures)
	{
		handle << x.isMultiSample << x.name;
		saveProperty(x.property);
		handle << x.samplerID << x.type;
	}

	handle.write<unsigned int>(uavs.size());
	for (const auto& x : uavs)
	{
		handle << x.format << x.name;
		saveProperty(x.property);
		handle << x.type;
	}

	handle.write<unsigned int>(samplers.size());
	for (const auto& x : samplers)
	{
		handle << x.first << x.second;
	}

	handle.write<unsigned int>(blendStates.size());
	for (const auto& x : blendStates)
	{
		handle << x.first << x.second;
	}

	handle.write<unsigned int>(depthStencilStates.size());
	for (const auto& x : depthStencilStates)
	{
		handle << x.first << x.second;
	}

	handle.write<unsigned int>(rasterizerStates.size());
	for (const auto& x : rasterizerStates)
	{
		handle << x.first << x.second;
	}

	handle.write<unsigned int>(techniqueGroups.size());
	for (const auto& x : techniqueGroups)
	{
		handle << x.first << ((unsigned int)x.second.size());
		for (const auto& y : x.second)
		{
			handle << y.first << ((unsigned int)y.second.passes.size()) << ((unsigned int)y.second.tags.size()) << ((unsigned int)y.second.variants.size());
			for (const auto& z : y.second.passes)
			{
				handle << z.first;

				saveShaderDef(z.second.vertexShader);
				saveShaderDef(z.second.pixelShader);
				saveShaderDef(z.second.computeShader);
				saveShaderDef(z.second.geometryShader);
				saveShaderDef(z.second.hullShader);
				saveShaderDef(z.second.domainShader);
				handle << z.second.blendStateId << z.second.dssId << z.second.rastId;
			}

			for (const auto& z : y.second.tags)
			{
				handle << z;
			}

			for (const auto& z : y.second.variants)
			{
				handle << z.first << z.second;
			}
		}
	}

	handle.write<unsigned int>(functions.size());
	for (const auto& x : functions)
	{
		handle << ((unsigned int)x.arguments.size());
		for (const auto& arg : x.arguments)
		{
			handle << arg;
		}

		handle << x.code << x.name << x.returnType << ((unsigned int)x.usedCBuffers.size());
		for (const auto& cbuff : x.usedCBuffers)
		{
			handle << cbuff;
		}

		handle << ((unsigned int)x.usedFunctions.size());
		for (const auto & func : x.usedFunctions)
		{
			handle << func;
		}

		handle << ((unsigned int)x.usedTextures.size());
		for (const auto& tex : x.usedTextures)
		{
			handle << tex;
		}

		handle << ((unsigned int)x.usedUAVs.size());
		for (const auto& tex : x.usedUAVs)
		{
			handle << tex;
		}
	}

	handle.write<unsigned int>(constantBuffers.size());
	for (const auto& x : constantBuffers)
	{
		saveStruct(x);
	}

	handle.write<unsigned int>(structures.size());
	for (const auto& x : structures)
	{
		saveStruct(x);
	}
}

void GLOWE::GSILShader::deserialize(LoadSaveHandle & handle)
{
	static auto loadProperty = [&](Property& prop)
	{
		unsigned int size;
		handle >> prop.type >> prop.name >> size;
		
		for (unsigned int x = 0; x < size; ++x)
		{
			Hash hash;
			String str;
			handle >> hash >> str;
			prop.constraints.emplace(hash, str);
		}
	};
	static auto loadShaderDef = [&](Pass::ShaderDefinition& def)
	{
		unsigned int size;
		handle >> def.func >> def.version >> size;
		def.replacements.resize(size);
		for (auto& x : def.replacements)
		{
			handle >> x;
		}
		handle >> size;
		def.usedFunctions.resize(size);
		for (auto& x : def.usedFunctions)
		{
			handle >> x;
		}
		handle >> size;
		def.usedTextures.resize(size);
		for (auto& x : def.usedTextures)
		{
			handle >> x;
		}
		handle >> size;
		def.usedCBuffers.resize(size);
		for (auto& x : def.usedCBuffers)
		{
			handle >> x;
		}
		handle >> size;
		def.usedUAVs.resize(size);
		for (auto& x : def.usedUAVs)
		{
			handle >> x;
		}
	};
	static auto loadStruct = [&](Structure& arg)
	{
		static auto loadField = [&](Structure::Field& field)
		{
			handle >> field.arraySize >> field.interpolation >> field.isSV >> field.isInstanceData >> field.name >> field.type >> field.typesCount >> field.typesCountMat;
			loadProperty(field.property);
		};

		unsigned int size;
		handle << size;
		arg.fields.resize(size);
		for (auto & x : arg.fields)
		{
			loadField(x);
		}
		handle >> arg.isInOut >> arg.name;
		arg.owningShader = this;
	};

	unsigned int size;

	handle >> size;
	textures.resize(size);
	for (auto& x : textures)
	{
		handle >> x.isMultiSample >> x.name;
		loadProperty(x.property);
		handle >> x.samplerID >> x.type;
	}

	handle >> size;
	uavs.resize(size);
	for (auto& x : uavs)
	{
		handle >> x.format >> x.name;
		loadProperty(x.property);
		handle >> x.type;
	}

	handle >> size;
	samplers.resize(size);
	for (auto& x : samplers)
	{
		handle >> x.first >> x.second;
	}

	handle >> size;
	blendStates.resize(size);
	for (auto& x : blendStates)
	{
		handle >> x.first >> x.second;;
	}

	handle >> size;
	depthStencilStates.resize(size);
	for (auto& x : depthStencilStates)
	{
		handle >> x.first >> x.second;
	}

	handle >> size;
	rasterizerStates.resize(size);
	for (auto& x : rasterizerStates)
	{
		handle >> x.first >> x.second;
	}

	handle >> size;
	for (unsigned int x = 0; x < size; ++x)
	{
		String tempStr;
		unsigned int sizeY;
		handle >> tempStr >> sizeY;
		Map<String, Technique>& group = techniqueGroups[tempStr];
		for (unsigned int y = 0; y < sizeY; ++y)
		{
			handle >> tempStr;
			Technique& tech = group[tempStr];
			unsigned int sizeZ, sizeTags, sizeVariants;
			handle >> sizeZ >> sizeTags >> sizeVariants;
			tech.tags.resize(sizeTags);
			for (unsigned int z = 0; z < sizeZ; ++z)
			{
				unsigned int id;
				handle >> id;
				Pass& pass = tech.passes.emplace(id, tech).first->second;

				loadShaderDef(pass.vertexShader);
				loadShaderDef(pass.pixelShader);
				loadShaderDef(pass.computeShader);
				loadShaderDef(pass.geometryShader);
				loadShaderDef(pass.hullShader);
				loadShaderDef(pass.domainShader);
				handle >> pass.blendStateId >> pass.dssId >> pass.rastId;
			}

			for (unsigned int z = 0; z < sizeTags; ++z)
			{
				handle >> tech.tags[z];
			}

			for (unsigned int z = 0; z < sizeVariants; ++z)
			{
				Pair<Hash, int> tempPair;

				handle >> tempPair.first >> tempPair.second;
				tech.variants.insert(tempPair);
			}
		}
	}

	handle >> size;
	functions.resize(size);
	for (auto& x : functions)
	{
		handle >> size;
		x.arguments.resize(size);
		for (auto& arg : x.arguments)
		{
			handle >> arg;
		}

		handle >> x.code >> x.name >> x.returnType >> size;
		x.usedCBuffers.resize(size);
		for (auto& cbuff : x.usedCBuffers)
		{
			handle >> cbuff;
		}

		handle >> size;
		x.usedFunctions.resize(size);
		for (auto & func : x.usedFunctions)
		{
			handle >> func;
		}

		handle >> size;
		x.usedTextures.resize(size);
		for (auto& tex : x.usedTextures)
		{
			handle >> tex;
		}

		handle >> size;
		x.usedUAVs.resize(size);
		for (auto& tex : x.usedUAVs)
		{
			handle >> tex;
		}
	}

	handle >> size;
	constantBuffers.resize(size);
	for (auto& x : constantBuffers)
	{
		loadStruct(x);
	}

	handle >> size;
	structures.resize(size);
	for (auto& x : structures)
	{
		loadStruct(x);
	}
}

GLOWE::GSILShader::Technique::Technique(const Technique& arg)
	: passes(arg.passes), variants(arg.variants), tags(arg.tags)
{
	for (auto& pass : passes)
	{
		pass.second.owner = this;
	}
}

GLOWE::GSILShader::Technique::Technique(Technique&& arg)
	: passes(std::move(arg.passes)), variants(std::move(arg.variants)), tags(std::move(arg.tags))
{
	for (auto& pass : passes)
	{
		pass.second.owner = this;
	}
}

GLOWE::String GLOWE::GSILShader::CompilationErrorException::createErrorMsg(const Deque<String>& errors)
{
	String result = "GSIL shader compilation failed:\n";

	for (const auto& x : errors)
	{
		result += x;
		result += '\n';
	}

	return result;
}

const GLOWE::Deque<GLOWE::String>& GLOWE::GSILShader::CompilationErrorException::getErrors() const
{
	return queue;
}

GLOWE::GSILShader::Pass::Pass(const Technique& technique)
	: owner(&technique)
{
}
