#pragma once
#ifndef GLOWE_UNIFORM_SAMPLERIMPLEMENTATION_INCLUDED
#define GLOWE_UNIFORM_SAMPLERIMPLEMENTATION_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/Hash.h"
#include "../../GlowSystem/Color.h"

#include "SamplerDesc.h"

namespace GLOWE
{
	class GraphicsDeviceImpl;

	class GLOWGRAPHICS_EXPORT SamplerImpl
		: public NoCopy
	{
	protected:
		SamplerDesc desc;
		Hash hash;
	public:
		SamplerImpl();
		SamplerImpl(const SamplerDesc& arg);
		virtual ~SamplerImpl() = default;

		virtual void create(const SamplerDesc& desc, const GraphicsDeviceImpl& device) = 0;
		virtual void destroy() = 0;

		virtual bool checkIsValid() const = 0;

		SamplerDesc getDesc() const;
	};
}

#endif
