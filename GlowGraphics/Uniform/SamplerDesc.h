#pragma once
#ifndef GLOWE_UNIFORM_SAMPLERDESC_INCLUDED
#define GLOWE_UNIFORM_SAMPLERDESC_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/Hash.h"
#include "../../GlowSystem/String.h"
#include "../../GlowSystem/Color.h"
#include "../../GlowSystem/LoadSaveHandle.h"

#include "Common.h"

namespace GLOWE
{
	// Mipmap filter implementation for OpenGL:
	// https://stackoverflow.com/questions/32165819/texture-filtering-directx-to-webgl
	// (Just a bunch of some usefull data)
	class GLOWGRAPHICS_EXPORT SamplerDesc
		: public Hashable, public BinarySerializable
	{
	public:
		enum class Filter
		{
			Nearest,
			Linear
		};
		enum class TextureAddressingMode
		{
			Wrap,
			Clamp,
			Border,
			Mirror,
			MirrorOnce // Direct3D Counterpart: D3D11_TEXTURE_ADDRESS_MIRROR_ONCE/D3D12_TEXTURE_ADDRESS_MODE_MIRROR_ONCE. OpenGL counterpart: GL_MIRROR_CLAMP_TO_EDGE.
		};
	public:
		Filter minFilter, magFilter, mipFilter;
		UInt8 anisotropyLevel;
		float minLOD, maxLOD, mipBiasLOD;
		bool enableComparison;
		TextureAddressingMode addressU, addressV, addressW;
		Color borderColor;
		ComparisonFunction compFunc;
	public:
		SamplerDesc();

		virtual Hash getHash() const override;

		virtual void serialize(LoadSaveHandle& handle) const override;
		virtual void deserialize(LoadSaveHandle& handle) override;
	};
}

#endif
