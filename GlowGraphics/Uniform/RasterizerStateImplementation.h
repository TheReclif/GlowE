#pragma once
#ifndef GLOWE_UNIFORM_RASTERIZERSTATEIMPLEMENTATION_INCLUDED
#define GLOWE_UNIFORM_RASTERIZERSTATEIMPLEMENTATION_INCLUDED

#include "../../GlowSystem/Utility.h"

#include "RasterizerDesc.h"

namespace GLOWE
{
	class GraphicsDeviceImpl;

	class GLOWGRAPHICS_EXPORT RasterizerStateImpl
		: public Hashable, public NoCopy
	{
	protected:
		RasterizerDesc desc;
		Hash hash;
	public:
		RasterizerStateImpl();
		RasterizerStateImpl(const RasterizerDesc& arg);
		virtual ~RasterizerStateImpl() = default;

		virtual void create(const RasterizerDesc& desc, const GraphicsDeviceImpl& device) = 0;
		virtual void destroy() = 0;

		virtual bool checkIsValid() const = 0;

		virtual Hash getHash() const override;
	};
}

#endif
