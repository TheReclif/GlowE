#include "TextureImplementation.h"
#include "TextureLoader.h"

GLOWE::TextureDesc::TextureDesc()
	: width(0), height(0), depth(0), multisamplingLevel(0), arraySize(1), mipmapsCount(1), usage(Usage::Default), format(Format::Type::Float, 0), cpuAccess(CPUAccess::None), binding(Binding::Default), type(Type::Texture2D), generateMipmaps(true)
{
}

void GLOWE::TextureDesc::serialize(LoadSaveHandle& handle) const
{
	handle << width << height << depth << multisamplingLevel << arraySize << mipmapsCount << usage << format << cpuAccess << binding << type << generateMipmaps;
}

void GLOWE::TextureDesc::deserialize(LoadSaveHandle& handle)
{
	handle >> width >> height >> depth >> multisamplingLevel >> arraySize >> mipmapsCount >> usage >> format >> cpuAccess >> binding >> type >> generateMipmaps;
}

void GLOWE::TextureImpl::scheduleMipmapGeneration()
{
	if (desc.generateMipmaps)
	{
		shouldBeMipmapsGenerated = true;
	}
}

void GLOWE::TextureImpl::abortMipmapGeneration()
{
	shouldBeMipmapsGenerated = false;
}

bool GLOWE::TextureImpl::checkShouldGenerateMipmaps() const
{
	return shouldBeMipmapsGenerated;
}

const GLOWE::TextureDesc& GLOWE::TextureImpl::getDesc() const
{
	return desc;
}
