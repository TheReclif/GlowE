#pragma once
#ifndef GLOWE_UNIFORM_SHADERREFLECTIMPLEMENTATION_INCLUDED
#define GLOWE_UNIFORM_SHADERREFLECTIMPLEMENTATION_INCLUDED

#include "glowgraphics_export.h"

#include "../../GlowSystem/Utility.h"

namespace GLOWE
{
	class String;
	class ShaderImpl;

	class GLOWGRAPHICS_EXPORT ShaderReflectImpl
		: public NoCopy
	{
	public:
		virtual ~ShaderReflectImpl() = default;

		virtual void create(const VertexShaderImpl& arg) = 0;
		virtual void create(const PixelShaderImpl& arg) = 0;
		virtual void create(const ComputeShaderImpl& arg) = 0;
		virtual void create(const GeometryShaderImpl& arg) = 0;

		virtual unsigned int getResourcesCount() const = 0;
		virtual unsigned int getSRVCount() const = 0;
		virtual unsigned int getTexCount() const = 0;
		virtual unsigned int getCBufferCount() const = 0;
		virtual unsigned int getBuffCount() const = 0;

		virtual String getResourceName(const unsigned int id) const = 0;
		virtual String getCBufferName(const unsigned int id) const = 0;

		virtual unsigned int getResourceID(const String& name) const = 0;
		virtual unsigned int getCBufferID(const String& name) const = 0;
	};
}

#endif
