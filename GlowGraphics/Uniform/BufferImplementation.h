#pragma once
#ifndef GLOWE_UNIFORM_BUFFERIMPLEMENTATION_INCLUDED
#define GLOWE_UNIFORM_BUFFERIMPLEMENTATION_INCLUDED

#include "BasicBuffer.h"
#include "Common.h"

namespace GLOWE
{
	class GraphicsDeviceImpl;
	class BasicRendererImpl;

	class GLOWGRAPHICS_EXPORT BufferDesc
		: public Hashable
	{
	public:
		enum class Binding
		{
			None,
			VertexBuffer,
			IndexBuffer,
			ConstantBuffer,
			StreamOutputBuffer, // Or transform feedback buffer (OpenGL).
			UnorderedAccess,
			ShaderResource
		};
	public:
		Usage usage;
		CPUAccess cpuAccess;
		Binding binds;
		StructureLayout layout;
	public:
		BufferDesc(); // Creates empty, invalid desc.

		virtual Hash getHash() const override;
	};

	class GLOWGRAPHICS_EXPORT BufferImpl
		: public NoCopy
	{
	protected:
		BufferDesc usedDesc;
		unsigned long long bufferSize;
	public:
		BufferImpl() = default;
		virtual ~BufferImpl() = default;

		virtual void create(const BufferDesc& desc, const unsigned long long buffSize, const GraphicsDeviceImpl& device, const void* data = nullptr) = 0;
		virtual void destroy() = 0;

		virtual void update(BasicRendererImpl& renderer, const void* data, const unsigned long long dataSize = 0, const unsigned long long offset = 0) const = 0;

		virtual bool checkIsCreated() const = 0;
		virtual bool checkIsValid() const = 0;

		BufferDesc getDesc() const;
		unsigned long long getSize() const;
	};
}

#endif
