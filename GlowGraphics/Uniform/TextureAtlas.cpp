#include "TextureAtlas.h"
#include "GraphicsFactory.h"

void GLOWE::TextureAtlas::resize(BasicRendererImpl& renderer, const UInt2& newDimensions)
{
	bool ex = false;
	if (size[0] > newDimensions[0] || size[1] > newDimensions[1] || !isDuringResize.compare_exchange_strong(ex, true))
	{
		return;
	}

	size = newDimensions;

	renderer.customFunction([this, newDimensions](BasicRendererImpl& renderer)
	{
		// Renderer is an immediate renderer.
		DefaultLockGuard guard(mutex);
		TextureDesc texDesc = atlas->getDesc();
		const UInt3 oldSize{ texDesc.width, texDesc.height, 0 };
		texDesc.width = size[0];
		texDesc.height = size[1];

		secondTexture->destroy(); // Just in case.
		secondTexture->create(texDesc, *gc);

		renderer.copyToTexture(atlas.get(), secondTexture.get(), { 0, 0, 0 }, { 0, 0, 0 }, oldSize);
		//std::swap(atlas, secondTexture);
		atlas.swap(secondTexture);
		secondTexture->destroy();
		isDuringResize = false;

		for (const auto& x : texturesToAdd)
		{
			renderer.copyToTexture(x.first, atlas.get(), { x.second->pos[0], x.second->pos[1], 0 }, { 0, 0, 0 }, { x.second->size[0], x.second->size[1], 0 });
		}

		for (const auto& x : dataTexturesToAdd)
		{
			renderer.updateTexture(atlas.get(), x.first.data(), { x.second->pos[0], x.second->pos[1], 0 }, { x.second->size[0], x.second->size[1], 0 });
		}

		texturesToAdd.clear();
		dataTexturesToAdd.clear();
	});
}

GLOWE::TextureAtlas::TextureAtlas()
	: atlas(createGraphicsObject<TextureImpl>()), secondTexture(createGraphicsObject<TextureImpl>()), gc(nullptr)
{
}

void GLOWE::TextureAtlas::create(const GraphicsDeviceImpl* const graphicsDevice, const TextureDesc& desc)
{
	destroy();
	gc = graphicsDevice;

	atlas->create(desc, *gc);
	size[0] = desc.width;
	size[1] = desc.height;
	atlasFormat = desc.format;

	SubtextureDesc tempDesc;
	tempDesc.size = size;
	tempDesc.pos = { 0, 0 };

	freeBlocks.emplace(size[0] * size[1], tempDesc);
}

void GLOWE::TextureAtlas::create(const GraphicsDeviceImpl* const graphicsDevice, const TextureDesc& desc, const void* data)
{
	destroy();
	gc = graphicsDevice;

	atlas->create(desc, *gc, { const_cast<void*>(data) });
	size[0] = desc.width;
	size[1] = desc.height;
	atlasFormat = desc.format;

	SubtextureDesc tempDesc;
	tempDesc.size = size;
	tempDesc.pos = { 0, 0 };

	freeBlocks.emplace(size[0] * size[1], tempDesc);
}

void GLOWE::TextureAtlas::destroy()
{
	isDuringResize = false;
	atlas->destroy();
	secondTexture->destroy();
	subtextures.clear();
	texturesToAdd.clear();
	freeBlocks.clear();
}

const GLOWE::TextureAtlas::SubtextureDesc* GLOWE::TextureAtlas::addTexture(const Hash& texHash, const TextureImpl& tex, BasicRendererImpl& renderer)
{
	DefaultLockGuard guard(mutex);
	const TextureDesc texDesc = tex.getDesc();
	if (texDesc.width == 0 || texDesc.height == 0)
	{
		return nullptr;
	}
	const UInt2 targetSize{ texDesc.width, texDesc.height };
	SubtextureDesc chosenDesc;

	const auto end = freeBlocks.end();
	auto lastAddedElemLeft = end, lastAddedElemRight = end;
	do
	{
		for (auto it = freeBlocks.lower_bound(targetSize[0] * targetSize[1]); it != end; ++it)
		{
			if (it->second.size[0] >= targetSize[0] && it->second.size[1] >= targetSize[1])
			{
				SubtextureDesc leftBlock, rightBlock;

				leftBlock.pos = { it->second.pos[0], it->second.pos[1] + targetSize[1] };
				leftBlock.size = { targetSize[0], it->second.size[1] - targetSize[1] };
				rightBlock.pos = { it->second.pos[0] + targetSize[0], it->second.pos[1] };
				rightBlock.size = { it->second.size[0] - targetSize[0], it->second.size[1] };

				if (leftBlock.size[1] > 0)
				{
					freeBlocks.emplace(leftBlock.size[0] * leftBlock.size[1], leftBlock);
				}
				if (rightBlock.size[0] > 0)
				{
					freeBlocks.emplace(rightBlock.size[0] * rightBlock.size[1], rightBlock);
				}

				chosenDesc.pos = it->second.pos;
				chosenDesc.size = targetSize;

				freeBlocks.erase(it);
				break;
			}
		}

		if (chosenDesc.size[0] == 0)
		{
			const UInt2 newSize{ size[0] * 2, size[1] * 2 };

			if (isDuringResize)
			{
				size = newSize;
			}
			else
			{
				resize(renderer, newSize);
			}

			if (lastAddedElemLeft != end)
			{
				freeBlocks.erase(lastAddedElemLeft);
				freeBlocks.erase(lastAddedElemRight);
			}

			SubtextureDesc leftBlock, rightBlock;

			leftBlock.pos = { 0, newSize[1] / 2 };
			leftBlock.size = { newSize[0] / 2, newSize[1] / 2 };

			rightBlock.pos = { newSize[0] / 2, 0 };
			rightBlock.size = { newSize[0] / 2, newSize[1] };

			lastAddedElemLeft = freeBlocks.emplace((newSize[0] / 2) * (newSize[1] / 2), leftBlock);
			lastAddedElemRight = freeBlocks.emplace((newSize[0] / 2) * newSize[1], rightBlock);
		}
	} while (chosenDesc.size[0] == 0);

	GLOWE::TextureAtlas::SubtextureDesc& result = subtextures.emplace(texHash, chosenDesc).first->second;

	if (isDuringResize)
	{
		texturesToAdd.emplace_back(&tex, &result);
	}
	else
	{
		renderer.copyToTexture(&tex, atlas.get(), { chosenDesc.pos[0], chosenDesc.pos[1], 0 }, { 0, 0, 0 }, { chosenDesc.size[0], chosenDesc.size[1], 0 });
	}

	return &result;
}

const GLOWE::TextureAtlas::SubtextureDesc* GLOWE::TextureAtlas::addTexture(const Hash& texHash, const UInt2& texSize, const void* data, BasicRendererImpl& renderer)
{
	if (texSize[0] == 0 || texSize[1] == 0)
	{
		return nullptr;
	}

	DefaultLockGuard guard(mutex);
	const UInt2 targetSize(texSize);
	SubtextureDesc chosenDesc;

	const auto end = freeBlocks.end();
	auto lastAddedElemLeft = end, lastAddedElemRight = end;
	do
	{
		for (auto it = freeBlocks.lower_bound(targetSize[0] * targetSize[1]); it != end; ++it)
		{
			if (it->second.size[0] >= targetSize[0] && it->second.size[1] >= targetSize[1])
			{
				SubtextureDesc leftBlock, rightBlock;

				leftBlock.pos = { it->second.pos[0], it->second.pos[1] + targetSize[1] };
				leftBlock.size = { targetSize[0], it->second.size[1] - targetSize[1] };
				rightBlock.pos = { it->second.pos[0] + targetSize[0], it->second.pos[1] };
				rightBlock.size = { it->second.size[0] - targetSize[0], it->second.size[1] };

				if (leftBlock.size[1] > 0)
				{
					freeBlocks.emplace(leftBlock.size[0] * leftBlock.size[1], leftBlock);
				}
				if (rightBlock.size[0] > 0)
				{
					freeBlocks.emplace(rightBlock.size[0] * rightBlock.size[1], rightBlock);
				}

				chosenDesc.pos = it->second.pos;
				chosenDesc.size = targetSize;

				freeBlocks.erase(it);
				break;
			}
		}

		if (chosenDesc.size[0] == 0)
		{
			const UInt2 newSize{ size[0] * 2, size[1] * 2 };

			if (isDuringResize)
			{
				size = newSize;
			}
			else
			{
				resize(renderer, newSize);
			}

			if (lastAddedElemLeft != end)
			{
				freeBlocks.erase(lastAddedElemLeft);
				freeBlocks.erase(lastAddedElemRight);
			}

			SubtextureDesc leftBlock, rightBlock;

			leftBlock.pos = { 0, newSize[1] / 2 };
			leftBlock.size = { newSize[0] / 2, newSize[1] / 2 };

			rightBlock.pos = { newSize[0] / 2, 0 };
			rightBlock.size = { newSize[0] / 2, newSize[1] };

			lastAddedElemLeft = freeBlocks.emplace((newSize[0] / 2) * (newSize[1] / 2), leftBlock);
			lastAddedElemRight = freeBlocks.emplace((newSize[0] / 2) * newSize[1], rightBlock);
		}
	} while (chosenDesc.size[0] == 0);

	GLOWE::TextureAtlas::SubtextureDesc& result = subtextures.emplace(texHash, chosenDesc).first->second;

	if (isDuringResize)
	{
		Vector<char> tempBuffer(texSize[0] * texSize[1] * atlasFormat.getSize());
		std::memcpy(tempBuffer.data(), data, tempBuffer.size());
		dataTexturesToAdd.emplace_back(std::move(tempBuffer), &result);
	}
	else
	{
		renderer.updateTexture(atlas.get(), data, { result.pos[0], result.pos[1], 0 }, { result.size[0], result.size[1], 0 });
	}

	return &result;
}

void GLOWE::TextureAtlas::overrideBlocks(const Vector<Pair<unsigned int, SubtextureDesc>>& newFreeBlocks, const Vector<Pair<Hash, SubtextureDesc>>& newSubtextures)
{
	freeBlocks.clear();
	subtextures.clear();

	for (const auto& x : newFreeBlocks)
	{
		freeBlocks.emplace(x);
	}

	for (const auto& x : newSubtextures)
	{
		subtextures.emplace(x);
	}
}

void GLOWE::TextureAtlas::getBlocksData(Vector<Pair<unsigned int, SubtextureDesc>>& freeBlocksSave, Vector<Pair<Hash, SubtextureDesc>>& subtexturesSave) const
{
	freeBlocksSave.clear();
	freeBlocksSave.reserve(freeBlocks.size());

	for (const auto& x : freeBlocks)
	{
		freeBlocksSave.emplace_back(x);
	}

	subtexturesSave.clear();
	subtexturesSave.reserve(subtextures.size());

	for (const auto& x : subtextures)
	{
		subtexturesSave.emplace_back(x);
	}
}

const GLOWE::TextureAtlas::SubtextureDesc* GLOWE::TextureAtlas::getSubtexture(const Hash& subtexHash) const
{
	auto it = subtextures.find(subtexHash);

	return it != subtextures.end() ? &it->second : nullptr;
}

const GLOWE::SharedPtr<GLOWE::TextureImpl>& GLOWE::TextureAtlas::getTexture() const
{
	return atlas;
}
