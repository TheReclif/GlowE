#pragma once
#ifndef GLOWE_UNIFORM_TEXTUREIMPLEMENTATION_INCLUDED
#define GLOWE_UNIFORM_TEXTUREIMPLEMENTATION_INCLUDED

#include "../../GlowSystem/Utility.h"

#include "ShaderResourceViewImpl.h"
#include "Format.h"
#include "Common.h"

namespace GLOWE
{
	inline unsigned int calculateMipLevelsCount(const unsigned int width) noexcept
	{
		unsigned int pos = 31;

		if (width == 0)
		{
			return 0;
		}

		while (!(width & (1 << pos)))
		{
			--pos;
		}

		return pos + 1;
	}
	inline unsigned int calculateMipLevelsCount(const unsigned int width, const unsigned int height) noexcept
	{
		const unsigned int temp = std::max(width, height);
		unsigned int pos = 31;

		if (width == 0 || height == 0)
		{
			return 0;
		}

		while (!(temp & (1 << pos)))
		{
			--pos;
		}

		return pos + 1;
	}
	inline unsigned int calculateMipLevelsCount(const unsigned int width, const unsigned int height, const unsigned int depth) noexcept
	{
		const unsigned int temp = std::max(width, std::max(height, depth));
		unsigned int pos = 31;

		if (width == 0 || height == 0 || depth == 0)
		{
			return 0;
		}

		while (!(temp & (1 << pos)))
		{
			--pos;
		}

		return pos + 1;
	}

	inline unsigned int getNextMipMapSize(const unsigned int width) noexcept
	{
		return std::max(1U, width / 2U);
	}
	inline UInt2 getNextMipMapSize(const unsigned int width, const unsigned int height) noexcept
	{
		return UInt2{ std::max(1U, width / 2U), std::max(1U, height / 2U) };
	}
	inline UInt2 getNextMipMapSize(const UInt2& prevMipmap)
	{
		return UInt2{ std::max(1U, prevMipmap[0] / 2U), std::max(1U, prevMipmap[1] / 2U) };
	}
	inline UInt3 getNextMipMapSize(const unsigned int width, const unsigned int height, const unsigned int depth) noexcept
	{
		return UInt3{ std::max(1U, width / 2U), std::max(1U, height / 2U), std::max(1U, depth / 2U) };
	}
	inline UInt3 getNextMipMapSize(const UInt3& prevMipmap)
	{
		return UInt3{ std::max(1U, prevMipmap[0] / 2U), std::max(1U, prevMipmap[1] / 2U), std::max(1U, prevMipmap[2] / 2U) };
	}

	class GLOWGRAPHICS_EXPORT TextureDesc
		: public BinarySerializable, public Serializable
	{
	public:
		enum class Binding
		{
			ShaderResource,
			DepthStencil,
			RenderTarget,
			UnorderedAccess,

			Default = ShaderResource
		};
		enum class Type
		{
			Texture1D,
			Texture1DArray,
			Texture2D,
			Texture2DArray,
			Texture3D,
			/// @brief A special array of 6 2D textures. To create one, initialize data with textures in this sequence: +X, -X, +Y, -Y, +Z, -Z (for Direct3D 11).
			TextureCube
		};
	public:
		UInt width, height, depth, multisamplingLevel, arraySize, mipmapsCount; // Depth is the third dimension (for Texture3D).
		Usage usage;
		Format format;
		CPUAccess cpuAccess;
		Binding binding;
		Type type;
		bool generateMipmaps;
	public:
		TextureDesc();

		GlowSerialize(GlowBases(), GlowField(width), GlowField(height), GlowField(depth), GlowField(multisamplingLevel), GlowField(arraySize), GlowField(mipmapsCount), GlowField(usage), GlowField(format), GlowField(cpuAccess), GlowField(binding), GlowField(type), GlowField(generateMipmaps));

		virtual void serialize(LoadSaveHandle& handle) const override;
		virtual void deserialize(LoadSaveHandle& handle) override;
	};

	class GraphicsDeviceImpl;
	class ShaderResourceViewImpl;

	class GLOWGRAPHICS_EXPORT TextureImpl
		: public NoCopy
	{
	protected:
		TextureDesc desc;
		bool shouldBeMipmapsGenerated;
	public:
		TextureImpl() : desc(), shouldBeMipmapsGenerated(false) {}
		TextureImpl(const TextureDesc& arg) : desc(arg), shouldBeMipmapsGenerated(arg.generateMipmaps) {}
		virtual ~TextureImpl() = default;

		// Remarks:
		// - Data size cannot be smaller than width (Texture1D), width * height (Texture2D) or width * height * depth (Texture3D).
		// - If generateMipmaps option is ticked, the generation does not occur here. The one responsible for it is BasicRenderer (because it encapsulates ID3D11DeviceContext).
		// - Data structure is as follows: mipmapsCount * arraySize pointers with pointers describing mip maps for every array element.
		virtual void create(const TextureDesc& arg, const GraphicsDeviceImpl& device, const Vector<void*> data = Vector<void*>()) = 0;
		virtual void destroy() = 0;

		virtual bool checkIsValid() const = 0;

		virtual const ShaderResourceViewImpl& getSRV() const = 0;

		void scheduleMipmapGeneration();
		void abortMipmapGeneration();
		bool checkShouldGenerateMipmaps() const;

		const TextureDesc& getDesc() const;
	};
}

#endif
