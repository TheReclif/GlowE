#include "SamplerImplementation.h"

GLOWE::SamplerImpl::SamplerImpl()
	: desc(), hash()
{
}

GLOWE::SamplerImpl::SamplerImpl(const SamplerDesc & arg)
	: desc(arg), hash(desc.getHash())
{
}

GLOWE::SamplerDesc GLOWE::SamplerImpl::getDesc() const
{
	return desc;
}
