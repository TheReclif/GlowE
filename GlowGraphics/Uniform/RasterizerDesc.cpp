#include "RasterizerDesc.h"

GLOWE::RasterizerDesc::RasterizerDesc()
	: cullingMode(CullingMode::Back), fillMode(FillMode::Solid), frontCounterClockwise(false), depthBias(0), depthBiasClamp(0.0f), slopeScaledDepthBias(0.0f), enableDepthClip(true), enableScissor(false), enableMultisample(false), enableAntialiasedLines(false)
{
}

void GLOWE::RasterizerDesc::serialize(LoadSaveHandle& handle) const
{
	handle << cullingMode << fillMode << frontCounterClockwise << depthBias << depthBiasClamp << slopeScaledDepthBias << enableDepthClip << enableScissor << enableMultisample << enableAntialiasedLines;
}

void GLOWE::RasterizerDesc::deserialize(LoadSaveHandle& handle)
{
	handle >> cullingMode >> fillMode >> frontCounterClockwise >> depthBias >> depthBiasClamp >> slopeScaledDepthBias >> enableDepthClip >> enableScissor >> enableMultisample >> enableAntialiasedLines;
}

GLOWE::Hash GLOWE::RasterizerDesc::getHash() const
{
	return Hash((const char*)this, sizeof(*this));
}
