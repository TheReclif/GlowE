#pragma once
#ifndef GLOWE_GRAPHICS_UNIFORM_LIGHTINGMGR_INCLUDED
#define GLOWE_GRAPHICS_UNIFORM_LIGHTINGMGR_INCLUDED

#include "LightStructures.h"

#include "../../GlowSystem/MemoryMgr.h"
#include "../../GlowSystem/Hash.h"
#include "../../GlowSystem/ThreadPool.h"

#include "../../GlowMath/Frustum.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT LightingMgr
		: public Subsystem
	{
	public:
		class GLOWGRAPHICS_EXPORT LightBase
		{
		public:
			virtual ~LightBase() = default;
			virtual void updateLight() = 0;
		};
	private:
		struct DirLightImportanceComparator
		{
			constexpr bool operator()(const DirectionalLight& l, const DirectionalLight& r) const
			{
				return l.importance > r.importance;
			}
		};
	private:
		// All sorted by intensity.
		List<Pair<PointLight, bool>> pointLights;
		Multiset<DirectionalLight, DirLightImportanceComparator> dirLights;
		List<Pair<SpotLight, bool>> spotLights;
		List<LightBase*> registeredLights;
		Mutex pointLightsMutex, dirLightsMutex, spotLightsMutex, registeredLightsMutex;
	public:
		using PointLightHandle = List<Pair<PointLight, bool>>::iterator;
		using DirectionalLightHandle = Multiset<DirectionalLight, DirLightImportanceComparator>::iterator;
		using SpotLightHandle = List<Pair<SpotLight, bool>>::iterator;
		using RegisteredLightHandle = List<LightBase*>::iterator;
	public:
		PointLightHandle addPointLight(const PointLight& light);
		DirectionalLightHandle addDirectionalLight(const DirectionalLight& light);
		SpotLightHandle addSpotLight(const SpotLight& light);

		void removePointLight(const PointLightHandle& it);
		void removeDirectionalLight(const DirectionalLightHandle& it);
		void removeSpotLight(const SpotLightHandle& it);

		PointLightHandle updatePointLight(const PointLightHandle& lightHandle, const PointLight& lightInfo);
		DirectionalLightHandle updateDirectionalLight(const DirectionalLightHandle& lightHandle, const DirectionalLight& lightInfo);
		SpotLightHandle updateSpotLight(const SpotLightHandle& lightHandle, const SpotLight& lightInfo);

		RegisteredLightHandle registerLight(LightBase* const light);
		void unregisterLight(const RegisteredLightHandle& arg);

		Vector<const PointLight*> getBestPointLights(const AABB& aabb) const; // Can be expensive.
		Vector<const DirectionalLight*> getBestDirLights() const;
		Vector<const SpotLight*> getBestSpotLights(const AABB& aabb) const; // Somewhat expensive.

		// Need to be done once per camera/frustum.
		void performLightsCulling(const Frustum& frustum);
		ThreadPool::TaskResult<void> scheduleLightsCulling(ThreadPool& threadPool, const Frustum& frustum);
	};
}

#endif
