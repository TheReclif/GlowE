#include "RenderingMgr.h"

void GLOWE::CommandList::setAsHighPriority()
{
	priority = Priority::High;
}

void GLOWE::CommandList::clear()
{
	commands.clear();
}

void GLOWE::CommandList::reserve(const unsigned int newCapacity)
{
	commands.reserve(newCapacity);
}

// TODO: Can cause infinite loop.
bool GLOWE::CommandList::operator<(const CommandList & right) const
{
	if (priority == Priority::High)
	{
		return true;
	}
	if (priority == Priority::Normal && right.priority == Priority::Normal)
	{
		return false;
	}

	return true;
}

bool GLOWE::CommandList::operator>(const CommandList & right) const
{
	return right < (*this);
}

void GLOWE::CommandList::setPrimitiveTopologyImpl(const PrimitiveTopology topology)
{
	commands.emplace_back(Command::Operation::SetPrimitiveTopology);
	commands.back().topologyData = topology;
}

GLOWE::CommandList::CommandList()
	: commands(), priority()
{
	commands.reserve(64); // It gives an increse of over 100 up to 300 FPS.
}

GLOWE::CommandList::CommandList(const unsigned int capacity)
	: commands(), priority()
{
	commands.reserve(capacity);
}

GLOWE::CommandList::CommandList(CommandList&& arg) noexcept
	: commands(std::move(arg.commands)), priority(exchange(arg.priority, Priority::Normal))
{
}

GLOWE::CommandList& GLOWE::CommandList::operator=(CommandList&& arg) noexcept
{
	commands = std::move(arg.commands);
	priority = exchange(arg.priority, Priority::Normal);

	return *this;
}

void GLOWE::CommandList::setBlendState(const BlendStateImpl* state)
{
	commands.emplace_back(Command::Operation::SetBlendState);
	commands.back().blendStateData = state;
}

void GLOWE::CommandList::customFunction(const std::function<void(BasicRendererImpl&)>& func)
{
	commands.emplace_back(Command::Operation::CustomFunc);
	commands.back().customFuncData = func;
}

void GLOWE::CommandList::setDepthStencilState(const DepthStencilStateImpl* dss)
{
	commands.emplace_back(Command::Operation::SetDSS);
	commands.back().dssData = dss;
}

void GLOWE::CommandList::setRasterizerState(const RasterizerStateImpl* rs)
{
	commands.emplace_back(Command::Operation::SetRS);
	commands.back().rsData = rs;
}

void GLOWE::CommandList::setInputLayout(const InputLayoutImpl* layout)
{
	commands.emplace_back(Command::Operation::SetInputLayout);
	commands.back().inputLayoutData = layout;
}

void GLOWE::CommandList::copyBuffers(const BufferImpl* src, const BufferImpl* dst)
{
	commands.emplace_back(Command::Operation::CopyBuffers);
	Command& temp = commands.back();
	temp.bufferCopyData.src = src;
	temp.bufferCopyData.dst = dst;
}

void GLOWE::CommandList::updateBuffer(const BufferImpl* buffer, const void* data, const std::size_t dataSize, const std::size_t offset)
{
	commands.emplace_back(Command::Operation::UpdateBuffer);
	Command& temp = commands.back();
	void* const tempBuffer = getInstance<RenderingMgr>().getFrameAllocator().allocate(dataSize);
	std::memcpy(tempBuffer, data, dataSize);
	temp.bufferUpdateData.buffer = buffer;
	temp.bufferUpdateData.ownedData = tempBuffer;
	temp.bufferUpdateData.dataSize = dataSize;
	temp.bufferUpdateData.offset = offset;
	temp.bufferUpdateData.isOwner = true;
}

void GLOWE::CommandList::updateBuffer(const BufferImpl* buffer, const void* data, const std::size_t dataSize, const DontCopyFlag&, const std::size_t offset)
{
	commands.emplace_back(Command::Operation::UpdateBuffer);
	Command& temp = commands.back();
	temp.bufferUpdateData.buffer = buffer;
	temp.bufferUpdateData.data = data;
	temp.bufferUpdateData.dataSize = dataSize;
	temp.bufferUpdateData.offset = offset;
	temp.bufferUpdateData.isOwner = false;
}

void GLOWE::CommandList::readBuffer(const BufferImpl* buffer, void* data, const std::size_t dataSize, const std::size_t offset)
{
	commands.emplace_back(Command::Operation::ReadBuffer);
	Command& temp = commands.back();
	temp.readBufferData.buffer = buffer;
	temp.readBufferData.data = data;
	temp.readBufferData.dataSize = dataSize;
	temp.readBufferData.offset = offset;
}

void GLOWE::CommandList::copyTextures(const TextureImpl* src, const TextureImpl* dst)
{
	commands.emplace_back(Command::Operation::CopyTextures);
	Command& temp = commands.back();
	temp.copyTexturesData.src = src;
	temp.copyTexturesData.dst = dst;
}

void GLOWE::CommandList::copyToTexture(const TextureImpl* src, const TextureImpl* dst, const UInt3& dstPos, const UInt3& srcPos, const UInt3& howMany, const unsigned int srcSubresourceId, const unsigned int dstSubresourceId)
{
	commands.emplace_back(Command::Operation::CopyToTexture);
	Command& temp = commands.back();
	temp.copyToTextureData.src = src;
	temp.copyToTextureData.dst = dst;
	temp.copyToTextureData.dstElemId = dstSubresourceId;
	temp.copyToTextureData.srcElemId = srcSubresourceId;
	temp.copyToTextureData.dstPos = dstPos;
	temp.copyToTextureData.srcPos = srcPos;
	temp.copyToTextureData.size = howMany;
}

void GLOWE::CommandList::updateTexture(const TextureImpl* dst, const void* data, const UInt3& pos, const UInt3& howMany, const unsigned int dstSubresourceId)
{
	commands.emplace_back(Command::Operation::UpdateTexture);
	Command& cmd = commands.back();
	const unsigned int reqSize = howMany[0] * std::max(1U, howMany[1]) * std::max(1U, howMany[2]) * dst->getDesc().format.getSize();
	//void* const tempBuffer = getInstance<GlobalAllocator>().allocateSpaceWithAlignment(reqSize, 16);
	void* const tempBuffer = getInstance<RenderingMgr>().getFrameAllocator().allocate(reqSize);
	std::memcpy(tempBuffer, data, reqSize);
	cmd.updateTextureData.ownedData = tempBuffer;
	cmd.updateTextureData.dst = dst;
	cmd.updateTextureData.pos = pos;
	cmd.updateTextureData.size = howMany;
	cmd.updateTextureData.subres = dstSubresourceId;
	cmd.updateTextureData.isOwner = true;
}

void GLOWE::CommandList::updateTexture(const TextureImpl* src, const void* data, const UInt3& pos, const UInt3& howMany, const DontCopyFlag&, const unsigned int dstSubresourceId)
{
	commands.emplace_back(Command::Operation::UpdateTexture);
	Command& cmd = commands.back();
	cmd.updateTextureData.data = data;
	cmd.updateTextureData.dst = src;
	cmd.updateTextureData.pos = pos;
	cmd.updateTextureData.size = howMany;
	cmd.updateTextureData.subres = dstSubresourceId;
	cmd.updateTextureData.isOwner = false;
}

void GLOWE::CommandList::readTexture(const TextureImpl* src, void* data, const UInt3& pos, const UInt3& howMany, const unsigned int srcSubresourceId)
{
	commands.emplace_back(Command::Operation::ReadTexture);
	Command& cmd = commands.back();
	cmd.readTextureData.data = data;
	cmd.readTextureData.dst = src;
	cmd.readTextureData.howMany = howMany;
	cmd.readTextureData.pos = pos;
	cmd.readTextureData.subres = srcSubresourceId;
}

void GLOWE::CommandList::setVertexBuffer(const BufferImpl* buffer)
{
	setVertexBuffers(&buffer, 1);
}

void GLOWE::CommandList::setVertexBuffers(const Vector<const BufferImpl*>& buffers)
{
	commands.emplace_back(Command::Operation::SetVertexBuffers);
	commands.back().setVertexBuffersData.assign(buffers.begin(), buffers.end());
}

void GLOWE::CommandList::setVertexBuffers(const BufferImpl** buffers, const unsigned int size)
{
	commands.emplace_back(Command::Operation::SetVertexBuffers);
	commands.back().setVertexBuffersData.assign(buffers, buffers + size);
}

void GLOWE::CommandList::setIndexBuffer(const BufferImpl* buffer)
{
	commands.emplace_back(Command::Operation::SetIndexBuffer);
	commands.back().setIndexBufferData = buffer;
}

void GLOWE::CommandList::setShaderCollection(const ShaderCollectionImpl* coll)
{
	commands.emplace_back(Command::Operation::SetShaderCollection);
	commands.back().setShaderCollectionData = coll;
}

void GLOWE::CommandList::drawImpl(const unsigned int verticesCount, const unsigned int startingVertex)
{
	commands.emplace_back(Command::Operation::Draw);
	Command& temp = commands.back();
	temp.drawData.verticesCount = verticesCount;
	temp.drawData.startingVertex = startingVertex;
}

void GLOWE::CommandList::drawIndexedImpl(const unsigned int indicesCount, const unsigned int startingIndex, const unsigned int startingVertex)
{
	commands.emplace_back(Command::Operation::DrawIndexed);
	Command& temp = commands.back();
	temp.drawIndexedData.indicesCount = indicesCount;
	temp.drawIndexedData.startingIndex = startingIndex;
	temp.drawIndexedData.startingVertex = startingVertex;
}

void GLOWE::CommandList::drawInstancedImpl(const unsigned int verticesPerInstance, const unsigned int instancesCount, const unsigned int startingVertex, const unsigned int startingInstanceLoc)
{
	commands.emplace_back(Command::Operation::DrawInstanced);
	Command& temp = commands.back();
	temp.drawInstancedData.instancesCount = instancesCount;
	temp.drawInstancedData.startingInstanceLoc = startingInstanceLoc;
	temp.drawInstancedData.startingVertex = startingVertex;
	temp.drawInstancedData.verticesPerInstance = verticesPerInstance;
}

void GLOWE::CommandList::drawIndexedInstancedImpl(const unsigned int indicesPerInstance, const unsigned int instancesCount, const int baseVertexLocation, const unsigned int startingVertex, const unsigned int startingInstanceLoc)
{
	commands.emplace_back(Command::Operation::DrawIndexedInstanced);
	Command& temp = commands.back();
	temp.drawIndexedInstancedData.instancesCount = instancesCount;
	temp.drawIndexedInstancedData.startingInstanceLoc = startingInstanceLoc;
	temp.drawIndexedInstancedData.startingVertex = startingVertex;
	temp.drawIndexedInstancedData.indicesPerInstance = indicesPerInstance;
	temp.drawIndexedInstancedData.baseVertexLocation = baseVertexLocation;
}

void GLOWE::CommandList::setSRV(const ShaderResourceViewImpl* srv, const unsigned int texUnit, const ShaderType stage)
{
	commands.emplace_back(Command::Operation::SetSRVs);
	Command& temp = commands.back();
	temp.setSRVsData.srvs.assign({ srv });
	temp.setSRVsData.texUnit = texUnit;
	temp.setSRVsData.stage = stage;
}

void GLOWE::CommandList::setSRV(const ShaderResourceViewImpl** srvArr, const unsigned int firstTexUnit, const unsigned int arraySize, const ShaderType stage)
{
	commands.emplace_back(Command::Operation::SetSRVs);
	Command& temp = commands.back();
	temp.setSRVsData.srvs.assign(srvArr, srvArr + arraySize);
	temp.setSRVsData.texUnit = firstTexUnit;
	temp.setSRVsData.stage = stage;
}

void GLOWE::CommandList::setConstantBuffers(const BufferImpl* cb, const unsigned int buffSlot, const ShaderType stage)
{
	commands.emplace_back(Command::Operation::SetConstantBuffers);
	Command& temp = commands.back();
	temp.setConstantBuffersData.buffers.assign({ cb });
	temp.setConstantBuffersData.buffSlot = buffSlot;
	temp.setConstantBuffersData.stage = stage;
}

void GLOWE::CommandList::setConstantBuffers(const BufferImpl** cb, const unsigned int buffSlot, const unsigned int arraySize, const ShaderType stage)
{
	commands.emplace_back(Command::Operation::SetConstantBuffers);
	Command& temp = commands.back();
	temp.setConstantBuffersData.buffers.assign(cb, cb + arraySize);
	temp.setConstantBuffersData.buffSlot = buffSlot;
	temp.setConstantBuffersData.stage = stage;
}

void GLOWE::CommandList::setSamplers(const SamplerImpl** arr, const unsigned int startSlot, const unsigned int arraySize, const ShaderType stage)
{
	commands.emplace_back(Command::Operation::SetSamplers);
	Command& temp = commands.back();
	temp.setSamplersData.samplers.assign(arr, arr + arraySize);
	temp.setSamplersData.samplerSlot = startSlot;
	temp.setSamplersData.stage = stage;
}

void GLOWE::CommandList::setRenderTarget(const RenderTargetImpl* rt)
{
	commands.emplace_back(Command::Operation::SetRenderTarget);
	commands.back().setRenderTargetData = rt;
}

void GLOWE::CommandList::setRenderTargetAndUAVs(const RenderTargetImpl* rt, const Vector<const UnorderedAccessViewImpl*>& uavs, const unsigned int uavStartSlot)
{
	commands.emplace_back(Command::Operation::SetRenderTargetAndUAVs);
	Command& cmd = commands.back();
	cmd.setRenderTargetAndUAVsData.rt = rt;
	cmd.setRenderTargetAndUAVsData.uavs.assign(uavs.begin(), uavs.end());
	cmd.setRenderTargetAndUAVsData.uavStartSlot = uavStartSlot;
}

void GLOWE::CommandList::setRenderTargetAndUAVs(const RenderTargetImpl* rt, const UnorderedAccessViewImpl* const* uavs, const unsigned int arraySize, const unsigned int uavStartSlot)
{
	commands.emplace_back(Command::Operation::SetRenderTargetAndUAVs);
	Command& cmd = commands.back();
	cmd.setRenderTargetAndUAVsData.rt = rt;
	cmd.setRenderTargetAndUAVsData.uavs.assign(uavs, uavs + arraySize);
	cmd.setRenderTargetAndUAVsData.uavStartSlot = uavStartSlot;
}

void GLOWE::CommandList::setUAVs(const UnorderedAccessViewImpl* const* uavs, const unsigned int arraySize, const unsigned int uavStartSlot)
{
	commands.emplace_back(Command::Operation::SetUAVs);
	Command& cmd = commands.back();
	cmd.setRenderTargetAndUAVsData.rt = nullptr;
	cmd.setRenderTargetAndUAVsData.uavs.assign(uavs, uavs + arraySize);
	cmd.setRenderTargetAndUAVsData.uavStartSlot = uavStartSlot;
}

void GLOWE::CommandList::setViewport(const Viewport & vp)
{
	setViewports(&vp, 1);
}

void GLOWE::CommandList::setViewports(const Vector<Viewport>& vpsArray)
{
	commands.emplace_back(Command::Operation::SetViewports);
	commands.back().setViewportsData.assign(vpsArray.begin(), vpsArray.end());
}

void GLOWE::CommandList::setViewports(const Viewport* vpsArray, const unsigned int size)
{
	commands.emplace_back(Command::Operation::SetViewports);
	commands.back().setViewportsData.assign(vpsArray, vpsArray + size);
}

void GLOWE::CommandList::setScissorRectsImpl(const Rect* rects, const unsigned int size)
{
	commands.emplace_back(Command::Operation::SetScissorRects);
	commands.back().setScissorRectsData.assign(rects, rects + size);
}

void GLOWE::CommandList::clearColor(const RenderTargetImpl* rt, const Color& refreshCol)
{
	commands.emplace_back(Command::Operation::ClearColor);
	Command& temp = commands.back();
	temp.clearColorData.rt = rt;
	temp.clearColorData.color = refreshCol;
}

void GLOWE::CommandList::clearDepthStencil(const RenderTargetImpl* rt, const float refreshDepth, const UInt8 refreshStencil)
{
	commands.emplace_back(Command::Operation::ClearDS);
	Command& temp = commands.back();
	temp.clearDSData.rt = rt;
	temp.clearDSData.refreshDepth = refreshDepth;
	temp.clearDSData.refreshStencil = refreshStencil;
}

void GLOWE::CommandList::dispatchImpl(const unsigned int x, const unsigned int y, const unsigned int z)
{
	commands.emplace_back(Command::Operation::Dispatch);
	Command& temp = commands.back();
	temp.dispatchData.x = x;
	temp.dispatchData.y = y;
	temp.dispatchData.z = z;
}

void GLOWE::CommandList::generateMipmaps(const ShaderResourceViewImpl* tex)
{
	commands.emplace_back(Command::Operation::GenerateMipmaps);
	commands.back().generateMipmapsData = tex;
}

GLOWE::CommandList::Command::Command(const Operation op)
	: operation(op), readBufferData()
{
	switch (operation)
	{
	case Operation::SetVertexBuffers:
		//setVertexBuffersData.~Vector<const Buffer*>();
		new (&setVertexBuffersData) FrameAllocVector<const BufferImpl*>(FrameResizableStdAllocator<const BufferImpl*>(&getInstance<RenderingMgr>().getFrameAllocator()));
		break;
	case Operation::SetSRVs:
		//setSRVsData.srvs.~Vector<const ShaderResourceView*>();
		new (&setSRVsData.srvs) FrameAllocVector<const ShaderResourceViewImpl*>(FrameResizableStdAllocator<const ShaderResourceViewImpl*>(&getInstance<RenderingMgr>().getFrameAllocator()));
		break;
	case Operation::SetConstantBuffers:
		//setConstantBuffersData.buffers.~Vector<const Buffer*>();
		new (&setConstantBuffersData.buffers) FrameAllocVector<const BufferImpl*>(FrameResizableStdAllocator<const BufferImpl*>(&getInstance<RenderingMgr>().getFrameAllocator()));
		break;
	case Operation::SetViewports:
		//setViewportsData.~Vector<Viewport>();
		new (&setViewportsData) FrameAllocVector<Viewport>(FrameResizableStdAllocator<Viewport>(&getInstance<RenderingMgr>().getFrameAllocator()));
		break;
	case Operation::SetScissorRects:
		new (&setScissorRectsData) FrameAllocVector<Rect>(FrameResizableStdAllocator<Rect>(&getInstance<RenderingMgr>().getFrameAllocator()));
		break;
	case Operation::SetSamplers:
		//setSamplersData.samplers.~Vector<const Sampler*>();
		new (&setSamplersData.samplers) FrameAllocVector<const SamplerImpl*>(FrameResizableStdAllocator<const SamplerImpl*>(&getInstance<RenderingMgr>().getFrameAllocator()));
		break;
	case Operation::SetUAVs:
	case Operation::SetRenderTargetAndUAVs:
		new (&setRenderTargetAndUAVsData.uavs) FrameAllocVector<const UnorderedAccessViewImpl*>(FrameResizableStdAllocator<const UnorderedAccessViewImpl*>(&getInstance<RenderingMgr>().getFrameAllocator()));
		break;
	case Operation::CustomFunc:
		new (&customFuncData) std::function<void(BasicRendererImpl&)>();
		break;
	}
}

GLOWE::CommandList::Command::Command(const Command & cmd)
	: operation(cmd.operation), topologyData()
{
	switch (operation)
	{
	case Operation::ClearColor:
		clearColorData = cmd.clearColorData;
		break;
	case Operation::CustomFunc:
		customFuncData = cmd.customFuncData;
		break;
	case Operation::ClearDS:
		clearDSData = cmd.clearDSData;
		break;
	case Operation::CopyBuffers:
		bufferCopyData = cmd.bufferCopyData;
		break;
	case Operation::CopyTextures:
		copyTexturesData = cmd.copyTexturesData;
		break;
	case Operation::CopyToTexture:
		copyToTextureData = cmd.copyToTextureData;
		break;
	case Operation::Draw:
		drawData = cmd.drawData;
		break;
	case Operation::DrawIndexed:
		drawIndexedData = cmd.drawIndexedData;
		break;
	case Operation::DrawInstanced:
		drawInstancedData = cmd.drawInstancedData;
		break;
	case Operation::DrawIndexedInstanced:
		drawIndexedInstancedData = cmd.drawIndexedInstancedData;
		break;
	case Operation::ReadBuffer:
		readBufferData = cmd.readBufferData;
		break;
	case Operation::ReadTexture:
		readTextureData = cmd.readTextureData;
		break;
	case Operation::SetBlendState:
		blendStateData = cmd.blendStateData;
		break;
	case Operation::SetConstantBuffers:
		//setConstantBuffersData = cmd.setConstantBuffersData;
		new (&setConstantBuffersData) decltype(setConstantBuffersData)(cmd.setConstantBuffersData);
		break;
	case Operation::SetDSS:
		dssData = cmd.dssData;
		break;
	case Operation::SetIndexBuffer:
		setIndexBufferData = cmd.setIndexBufferData;
		break;
	case Operation::SetInputLayout:
		inputLayoutData = cmd.inputLayoutData;
		break;
	case Operation::SetPrimitiveTopology:
		topologyData = cmd.topologyData;
		break;
	case Operation::SetRenderTarget:
		setRenderTargetData = cmd.setRenderTargetData;
		break;
	case Operation::SetRenderTargetAndUAVs:
	case Operation::SetUAVs:
		//setRenderTargetAndUAVsData = cmd.setRenderTargetAndUAVsData;
		new (&setRenderTargetAndUAVsData) decltype(setRenderTargetAndUAVsData)(cmd.setRenderTargetAndUAVsData);
		break;
	case Operation::SetRS:
		rsData = cmd.rsData;
		break;
	case Operation::SetShaderCollection:
		setShaderCollectionData = cmd.setShaderCollectionData;
		break;
	case Operation::SetSRVs:
		//setSRVsData = cmd.setSRVsData;
		new (&setSRVsData) decltype(setSRVsData)(cmd.setSRVsData);
		break;
	case Operation::SetSamplers:
		//setSamplersData = cmd.setSamplersData;
		new (&setSamplersData) decltype(setSamplersData)(cmd.setSamplersData);
		break;
	case Operation::SetVertexBuffers:
		//setVertexBuffersData = cmd.setVertexBuffersData;
		new (&setVertexBuffersData) decltype(setVertexBuffersData)(cmd.setVertexBuffersData);
		break;
	case Operation::SetViewports:
		//setViewportsData = cmd.setViewportsData;
		new (&setViewportsData) decltype(setViewportsData)(cmd.setViewportsData);
		break;
	case Operation::SetScissorRects:
		new (&setScissorRectsData) decltype(setScissorRectsData)(cmd.setScissorRectsData);
		break;
	case Operation::UpdateBuffer:
		{
			bufferUpdateData = cmd.bufferUpdateData;
			if (bufferUpdateData.isOwner)
			{
				//void* const tempBuffer = getInstance<GlobalAllocator>().allocateSpaceWithAlignment(cmd.bufferUpdateData.dataSize, 16);
				void* const tempBuffer = getInstance<RenderingMgr>().getFrameAllocator().allocate(cmd.bufferUpdateData.dataSize);
				std::memcpy(tempBuffer, cmd.bufferUpdateData.data, cmd.bufferUpdateData.dataSize);
				bufferUpdateData.ownedData = tempBuffer;
			}
		}
		break;
	case Operation::UpdateTexture:
		{
			updateTextureData = cmd.updateTextureData;
			if (updateTextureData.isOwner)
			{
				const unsigned int tempSize = cmd.updateTextureData.size[0] * std::max(1U, cmd.updateTextureData.size[1]) * std::max(1U, cmd.updateTextureData.size[2]) * cmd.updateTextureData.dst->getDesc().format.getSize();
				//void* const tempBuffer = getInstance<GlobalAllocator>().allocateSpaceWithAlignment(tempSize, 16);
				void* const tempBuffer = getInstance<RenderingMgr>().getFrameAllocator().allocate(tempSize);
				std::memcpy(tempBuffer, cmd.updateTextureData.data, tempSize);
				updateTextureData.ownedData = tempBuffer;
			}
		}
		break;
	case Operation::Dispatch:
		dispatchData = cmd.dispatchData;
		break;
	case Operation::GenerateMipmaps:
		generateMipmapsData = cmd.generateMipmapsData;
		break;
	}
}

GLOWE::CommandList::Command::Command(Command&& cmd) noexcept
	: operation(exchange(cmd.operation, Operation::Unknown)), topologyData()
{
	switch (operation)
	{
	case Operation::ClearColor:
		clearColorData = std::move(cmd.clearColorData);
		break;
	case Operation::ClearDS:
		clearDSData = std::move(cmd.clearDSData);
		break;
	case Operation::CustomFunc:
		//customFuncData = std::move(cmd.customFuncData);
		new (&customFuncData) decltype(customFuncData)(std::move(cmd.customFuncData));
		break;
	case Operation::CopyBuffers:
		bufferCopyData = std::move(cmd.bufferCopyData);
		break;
	case Operation::CopyTextures:
		copyTexturesData = std::move(cmd.copyTexturesData);
		break;
	case Operation::CopyToTexture:
		copyToTextureData = std::move(cmd.copyToTextureData);
		break;
	case Operation::Draw:
		drawData = std::move(cmd.drawData);
		break;
	case Operation::DrawIndexed:
		drawIndexedData = std::move(cmd.drawIndexedData);
		break;
	case Operation::DrawInstanced:
		drawInstancedData = std::move(cmd.drawInstancedData);
		break;
	case Operation::DrawIndexedInstanced:
		drawIndexedInstancedData = std::move(cmd.drawIndexedInstancedData);
		break;
	case Operation::ReadBuffer:
		readBufferData = std::move(cmd.readBufferData);
		break;
	case Operation::ReadTexture:
		readTextureData = std::move(cmd.readTextureData);
		break;
	case Operation::SetBlendState:
		blendStateData = std::move(cmd.blendStateData);
		break;
	case Operation::SetConstantBuffers:
		//setConstantBuffersData = std::move(cmd.setConstantBuffersData);
		new (&setConstantBuffersData) decltype(setConstantBuffersData)(std::move(cmd.setConstantBuffersData));
		break;
	case Operation::SetDSS:
		dssData = std::move(cmd.dssData);
		break;
	case Operation::SetIndexBuffer:
		setIndexBufferData = std::move(cmd.setIndexBufferData);
		break;
	case Operation::SetInputLayout:
		inputLayoutData = std::move(cmd.inputLayoutData);
		break;
	case Operation::SetPrimitiveTopology:
		topologyData = std::move(cmd.topologyData);
		break;
	case Operation::SetRenderTarget:
		setRenderTargetData = std::move(cmd.setRenderTargetData);
		break;
	case Operation::SetRenderTargetAndUAVs:
	case Operation::SetUAVs:
		new (&setRenderTargetAndUAVsData) decltype(setRenderTargetAndUAVsData)(std::move(cmd.setRenderTargetAndUAVsData));
		break;
	case Operation::SetRS:
		rsData = std::move(cmd.rsData);
		break;
	case Operation::SetShaderCollection:
		setShaderCollectionData = std::move(cmd.setShaderCollectionData);
		break;
	case Operation::SetSRVs:
		//setSRVsData = std::move(cmd.setSRVsData);
		new (&setSRVsData) decltype(setSRVsData)(std::move(cmd.setSRVsData));
		break;
	case Operation::SetVertexBuffers:
		//setVertexBuffersData = std::move(cmd.setVertexBuffersData);
		new (&setVertexBuffersData) decltype(setVertexBuffersData)(std::move(cmd.setVertexBuffersData));
		break;
	case Operation::SetSamplers:
		//setSamplersData = std::move(cmd.setSamplersData);
		new (&setSamplersData) decltype(setSamplersData)(std::move(cmd.setSamplersData));
		break;
	case Operation::SetViewports:
		//setViewportsData = std::move(cmd.setViewportsData);
		new (&setViewportsData) decltype(setViewportsData)(std::move(cmd.setViewportsData));
		break;
	case Operation::SetScissorRects:
		new (&setScissorRectsData) decltype(setScissorRectsData)(std::move(cmd.setScissorRectsData));
		break;
	case Operation::UpdateBuffer:
		bufferUpdateData = exchange(cmd.bufferUpdateData, decltype(bufferUpdateData)());
		cmd.bufferUpdateData.isOwner = false;
		break;
	case Operation::UpdateTexture:
		updateTextureData = exchange(cmd.updateTextureData, decltype(updateTextureData)());
		cmd.updateTextureData.isOwner = false;
		break;
	case Operation::Dispatch:
		dispatchData = std::move(cmd.dispatchData);
		break;
	case Operation::GenerateMipmaps:
		generateMipmapsData = exchange(cmd.generateMipmapsData, nullptr);
		break;
	}
}

GLOWE::CommandList::Command::~Command()
{
	/*
	switch (operation)
	{
	case Operation::SetVertexBuffers:
		setVertexBuffersData.~FrameAllocVector<const Buffer*>();
		break;
	case Operation::SetSRVs:
		setSRVsData.srvs.~FrameAllocVector<const ShaderResourceView*>();
		break;
	case Operation::CustomFunc:
		customFuncData.~function();
		break;
	case Operation::SetConstantBuffers:
		setConstantBuffersData.buffers.~FrameAllocVector<const Buffer*>();
		break;
	case Operation::SetRenderTargetAndUAVs:
	case Operation::SetUAVs:
		setRenderTargetAndUAVsData.uavs.~FrameAllocVector<const UnorderedAccessView*>();
		break;
	case Operation::SetViewports:
		setViewportsData.~FrameAllocVector<Viewport>();
		break;
	case Operation::SetScissorRects:
		setScissorRectsData.~FrameAllocVector<Rect>();
		break;
	case Operation::SetSamplers:
		setSamplersData.samplers.~FrameAllocVector<const Sampler*>();
		break;
	}
	*/
}

GLOWE::CommandList::Command & GLOWE::CommandList::Command::operator=(const Command & cmd)
{
	//this->~Command();
	operation = cmd.operation;
	switch (operation)
	{
	case Operation::ClearColor:
		clearColorData = cmd.clearColorData;
		break;
	case Operation::CustomFunc:
		customFuncData = cmd.customFuncData;
		break;
	case Operation::ClearDS:
		clearDSData = cmd.clearDSData;
		break;
	case Operation::CopyBuffers:
		bufferCopyData = cmd.bufferCopyData;
		break;
	case Operation::CopyTextures:
		copyTexturesData = cmd.copyTexturesData;
		break;
	case Operation::CopyToTexture:
		copyToTextureData = cmd.copyToTextureData;
		break;
	case Operation::Draw:
		drawData = cmd.drawData;
		break;
	case Operation::DrawIndexed:
		drawIndexedData = cmd.drawIndexedData;
		break;
	case Operation::DrawInstanced:
		drawInstancedData = cmd.drawInstancedData;
		break;
	case Operation::DrawIndexedInstanced:
		drawIndexedInstancedData = cmd.drawIndexedInstancedData;
		break;
	case Operation::ReadBuffer:
		readBufferData = cmd.readBufferData;
		break;
	case Operation::ReadTexture:
		readTextureData = cmd.readTextureData;
		break;
	case Operation::SetBlendState:
		blendStateData = cmd.blendStateData;
		break;
	case Operation::SetConstantBuffers:
		//setConstantBuffersData = cmd.setConstantBuffersData;
		new (&setConstantBuffersData) decltype(setConstantBuffersData)(cmd.setConstantBuffersData);
		break;
	case Operation::SetDSS:
		dssData = cmd.dssData;
		break;
	case Operation::SetIndexBuffer:
		setIndexBufferData = cmd.setIndexBufferData;
		break;
	case Operation::SetInputLayout:
		inputLayoutData = cmd.inputLayoutData;
		break;
	case Operation::SetPrimitiveTopology:
		topologyData = cmd.topologyData;
		break;
	case Operation::SetRenderTarget:
		setRenderTargetData = cmd.setRenderTargetData;
		break;
	case Operation::SetRenderTargetAndUAVs:
	case Operation::SetUAVs:
		new (&setRenderTargetAndUAVsData) decltype(setRenderTargetAndUAVsData)(cmd.setRenderTargetAndUAVsData);
		break;
	case Operation::SetRS:
		rsData = cmd.rsData;
		break;
	case Operation::SetShaderCollection:
		setShaderCollectionData = cmd.setShaderCollectionData;
		break;
	case Operation::SetSRVs:
		//setSRVsData = cmd.setSRVsData;
		new (&setSRVsData) decltype(setSRVsData)(cmd.setSRVsData);
		break;
	case Operation::SetVertexBuffers:
		//setVertexBuffersData = cmd.setVertexBuffersData;
		new (&setVertexBuffersData) decltype(setVertexBuffersData)(cmd.setVertexBuffersData);
		break;
	case Operation::SetSamplers:
		//setSamplersData = cmd.setSamplersData;
		new (&setSamplersData) decltype(setSamplersData)(cmd.setSamplersData);
		break;
	case Operation::SetViewports:
		//setViewportsData = cmd.setViewportsData;
		new (&setViewportsData) decltype(setViewportsData)(cmd.setViewportsData);
		break;
	case Operation::SetScissorRects:
		new (&setScissorRectsData) decltype(setScissorRectsData)(cmd.setScissorRectsData);
		break;
	case Operation::UpdateBuffer:
	{
		bufferUpdateData = cmd.bufferUpdateData;
		if (bufferUpdateData.isOwner)
		{
			//void* const tempBuffer = getInstance<GlobalAllocator>().allocateSpaceWithAlignment(cmd.bufferUpdateData.dataSize, 16);
			void* const tempBuffer = getInstance<RenderingMgr>().getFrameAllocator().allocate(cmd.bufferUpdateData.dataSize);
			std::memcpy(tempBuffer, cmd.bufferUpdateData.data, cmd.bufferUpdateData.dataSize);
			bufferUpdateData.ownedData = tempBuffer;
		}
	}
	break;
	case Operation::UpdateTexture:
	{
		updateTextureData = cmd.updateTextureData;
		if (updateTextureData.isOwner)
		{
			const unsigned int tempSize = cmd.updateTextureData.size[0] * std::max(1U, cmd.updateTextureData.size[1]) * std::max(1U, cmd.updateTextureData.size[2]) * cmd.updateTextureData.dst->getDesc().format.getSize();
			//void* const tempBuffer = getInstance<GlobalAllocator>().allocateSpaceWithAlignment(tempSize, 16);
			void* const tempBuffer = getInstance<RenderingMgr>().getFrameAllocator().allocate(tempSize);
			std::memcpy(tempBuffer, cmd.updateTextureData.data, tempSize);
			updateTextureData.ownedData = tempBuffer;
		}
	}
	break;
	case Operation::Dispatch:
		dispatchData = cmd.dispatchData;
		break;
	case Operation::GenerateMipmaps:
		generateMipmapsData = cmd.generateMipmapsData;
		break;
	}
	return *this;
}

GLOWE::CommandList::Command& GLOWE::CommandList::Command::operator=(Command&& cmd) noexcept
{
	//this->~Command();
	operation = exchange(cmd.operation, Operation::Unknown);
	switch (operation)
	{
	case Operation::ClearColor:
		clearColorData = std::move(cmd.clearColorData);
		break;
	case Operation::CustomFunc:
		customFuncData = std::move(cmd.customFuncData);
		break;
	case Operation::ClearDS:
		clearDSData = std::move(cmd.clearDSData);
		break;
	case Operation::CopyBuffers:
		bufferCopyData = std::move(cmd.bufferCopyData);
		break;
	case Operation::CopyTextures:
		copyTexturesData = std::move(cmd.copyTexturesData);
		break;
	case Operation::CopyToTexture:
		copyToTextureData = std::move(cmd.copyToTextureData);
		break;
	case Operation::Draw:
		drawData = std::move(cmd.drawData);
		break;
	case Operation::DrawIndexed:
		drawIndexedData = std::move(cmd.drawIndexedData);
		break;
	case Operation::DrawInstanced:
		drawInstancedData = std::move(cmd.drawInstancedData);
		break;
	case Operation::DrawIndexedInstanced:
		drawIndexedInstancedData = std::move(cmd.drawIndexedInstancedData);
		break;
	case Operation::ReadBuffer:
		readBufferData = std::move(cmd.readBufferData);
		break;
	case Operation::ReadTexture:
		readTextureData = std::move(cmd.readTextureData);
		break;
	case Operation::SetBlendState:
		blendStateData = std::move(cmd.blendStateData);
		break;
	case Operation::SetConstantBuffers:
		//setConstantBuffersData = std::move(cmd.setConstantBuffersData);
		new (&setConstantBuffersData) decltype(setConstantBuffersData)(std::move(cmd.setConstantBuffersData));
		break;
	case Operation::SetDSS:
		dssData = std::move(cmd.dssData);
		break;
	case Operation::SetIndexBuffer:
		setIndexBufferData = std::move(cmd.setIndexBufferData);
		break;
	case Operation::SetInputLayout:
		inputLayoutData = std::move(cmd.inputLayoutData);
		break;
	case Operation::SetPrimitiveTopology:
		topologyData = std::move(cmd.topologyData);
		break;
	case Operation::SetRenderTarget:
		setRenderTargetData = std::move(cmd.setRenderTargetData);
		break;
	case Operation::SetRenderTargetAndUAVs:
	case Operation::SetUAVs:
		new (&setRenderTargetAndUAVsData) decltype(setRenderTargetAndUAVsData)(std::move(cmd.setRenderTargetAndUAVsData));
		break;
	case Operation::SetRS:
		rsData = std::move(cmd.rsData);
		break;
	case Operation::SetShaderCollection:
		setShaderCollectionData = std::move(cmd.setShaderCollectionData);
		break;
	case Operation::SetSRVs:
		//setSRVsData = std::move(cmd.setSRVsData);
		new (&setSRVsData) decltype(setSRVsData)(std::move(cmd.setSRVsData));
		break;
	case Operation::SetVertexBuffers:
		//setVertexBuffersData = std::move(cmd.setVertexBuffersData);
		new (&setVertexBuffersData) decltype(setVertexBuffersData)(std::move(cmd.setVertexBuffersData));
		break;
	case Operation::SetSamplers:
		//setSamplersData = std::move(cmd.setSamplersData);
		new (&setSamplersData) decltype(setSamplersData)(std::move(cmd.setSamplersData));
		break;
	case Operation::SetViewports:
		//setViewportsData = std::move(cmd.setViewportsData);
		new (&setViewportsData) decltype(setViewportsData)(std::move(cmd.setViewportsData));
		break;
	case Operation::SetScissorRects:
		new (&setScissorRectsData) decltype(setScissorRectsData)(std::move(cmd.setScissorRectsData));
		break;
	case Operation::UpdateBuffer:
		bufferUpdateData = exchange(cmd.bufferUpdateData, decltype(bufferUpdateData)());
		cmd.bufferUpdateData.isOwner = false;
		break;
	case Operation::UpdateTexture:
		updateTextureData = exchange(cmd.updateTextureData, decltype(updateTextureData)());
		cmd.updateTextureData.isOwner = false;
		break;
	case Operation::Dispatch:
		dispatchData = std::move(cmd.dispatchData);
		break;
	case Operation::GenerateMipmaps:
		generateMipmapsData = exchange(cmd.generateMipmapsData, nullptr);
		break;
	}
	return *this;
}

void GLOWE::RenderingMgr::processCommand(const CommandList::Command & cmd)
{
	using Operation = CommandList::Command::Operation;

	switch (cmd.operation)
	{
	case Operation::ClearColor:
		renderer->clearColor(cmd.clearColorData.rt, cmd.clearColorData.color);
		break;
	case Operation::CustomFunc:
		renderer->customFunction(cmd.customFuncData);
		break;
	case Operation::ClearDS:
		renderer->clearDepthStencil(cmd.clearDSData.rt, cmd.clearDSData.refreshDepth, cmd.clearDSData.refreshStencil);
		break;
	case Operation::CopyBuffers:
		renderer->copyBuffers(cmd.bufferCopyData.src, cmd.bufferCopyData.dst);
		break;
	case Operation::CopyTextures:
		renderer->copyTextures(cmd.copyTexturesData.src, cmd.copyTexturesData.dst);
		break;
	case Operation::CopyToTexture:
		renderer->copyToTexture(cmd.copyToTextureData.src, cmd.copyToTextureData.dst, cmd.copyToTextureData.dstPos, cmd.copyToTextureData.srcPos, cmd.copyToTextureData.size, cmd.copyToTextureData.srcElemId, cmd.copyToTextureData.dstElemId);
		break;
	case Operation::Draw:
		renderer->draw(cmd.drawData.verticesCount, cmd.drawData.startingVertex);
		break;
	case Operation::DrawIndexed:
		renderer->drawIndexed(cmd.drawIndexedData.indicesCount, cmd.drawIndexedData.startingIndex, cmd.drawIndexedData.startingVertex);
		break;
	case Operation::DrawInstanced:
		renderer->drawInstanced(cmd.drawInstancedData.verticesPerInstance, cmd.drawInstancedData.instancesCount, cmd.drawInstancedData.startingVertex, cmd.drawInstancedData.startingInstanceLoc);
		break;
	case Operation::DrawIndexedInstanced:
		renderer->drawIndexedInstanced(cmd.drawIndexedInstancedData.indicesPerInstance, cmd.drawIndexedInstancedData.instancesCount, cmd.drawIndexedInstancedData.baseVertexLocation, cmd.drawIndexedInstancedData.startingVertex, cmd.drawIndexedInstancedData.startingInstanceLoc);
		break;
	case Operation::ReadBuffer:
		renderer->readBuffer(cmd.readBufferData.buffer, cmd.readBufferData.data, cmd.readBufferData.dataSize, cmd.readBufferData.offset);
		break;
	case Operation::ReadTexture:
		renderer->readTexture(cmd.readTextureData.dst, cmd.readTextureData.data, cmd.readTextureData.pos, cmd.readTextureData.howMany, cmd.readTextureData.subres);
		break;
	case Operation::SetBlendState:
		renderer->setBlendState(cmd.blendStateData);
		break;
	case Operation::SetConstantBuffers:
		renderer->setConstantBuffers(const_cast<const BufferImpl**>(cmd.setConstantBuffersData.buffers.data()), cmd.setConstantBuffersData.buffSlot, cmd.setConstantBuffersData.buffers.size(), cmd.setConstantBuffersData.stage);
		break;
	case Operation::SetDSS:
		renderer->setDepthStencilState(cmd.dssData);
		break;
	case Operation::SetIndexBuffer:
		renderer->setIndexBuffer(cmd.setIndexBufferData);
		break;
	case Operation::SetInputLayout:
		renderer->setInputLayout(cmd.inputLayoutData);
		break;
	case Operation::SetPrimitiveTopology:
		renderer->setPrimitiveTopology(cmd.topologyData);
		break;
	case Operation::SetRenderTarget:
		renderer->setRenderTarget(cmd.setRenderTargetData);
		break;
	case Operation::SetRenderTargetAndUAVs:
		renderer->setRenderTargetAndUAVs(cmd.setRenderTargetAndUAVsData.rt, cmd.setRenderTargetAndUAVsData.uavs.data(), cmd.setRenderTargetAndUAVsData.uavs.size(), cmd.setRenderTargetAndUAVsData.uavStartSlot);
		break;
	case Operation::SetUAVs:
		renderer->setUAVs(cmd.setRenderTargetAndUAVsData.uavs.data(), cmd.setRenderTargetAndUAVsData.uavs.size(), cmd.setRenderTargetAndUAVsData.uavStartSlot);
		break;
	case Operation::SetRS:
		renderer->setRasterizerState(cmd.rsData);
		break;
	case Operation::SetShaderCollection:
		renderer->setShaderCollection(cmd.setShaderCollectionData);
		break;
	case Operation::SetSRVs:
		renderer->setSRV(const_cast<const ShaderResourceViewImpl**>(cmd.setSRVsData.srvs.data()), cmd.setSRVsData.texUnit, cmd.setSRVsData.srvs.size(), cmd.setSRVsData.stage);
		break;
	case Operation::SetSamplers:
		renderer->setSamplers(const_cast<const SamplerImpl**>(cmd.setSamplersData.samplers.data()), cmd.setSamplersData.samplerSlot, cmd.setSamplersData.samplers.size(), cmd.setSamplersData.stage);
		break;
	case Operation::SetVertexBuffers:
		renderer->setVertexBuffers(const_cast<const BufferImpl**>(cmd.setVertexBuffersData.data()), cmd.setVertexBuffersData.size());
		break;
	case Operation::SetViewports:
		renderer->setViewports(cmd.setViewportsData.data(), cmd.setViewportsData.size());
		break;
	case Operation::SetScissorRects:
		renderer->setScissorRects(cmd.setScissorRectsData.data(), cmd.setScissorRectsData.size());
		break;
	case Operation::UpdateBuffer:
		renderer->updateBuffer(cmd.bufferUpdateData.buffer, cmd.bufferUpdateData.data, cmd.bufferUpdateData.dataSize, cmd.bufferUpdateData.offset);
		break;
	case Operation::UpdateTexture:
		renderer->updateTexture(cmd.updateTextureData.dst, cmd.updateTextureData.data, cmd.updateTextureData.pos, cmd.updateTextureData.size, cmd.updateTextureData.subres);
		break;
	case Operation::Dispatch:
		renderer->dispatch(cmd.dispatchData.x, cmd.dispatchData.y, cmd.dispatchData.z);
		break;
	case Operation::GenerateMipmaps:
		renderer->generateMipmaps(cmd.generateMipmapsData);
		break;
	}
}

void GLOWE::RenderingMgr::initialize(BasicRendererImpl& arg, const bool anotherThread)
{
	auto threadProc = [this](Thread::Pointer pointer)
	{
		getInstance<ConcurrentGC>().attachThread();
		DefaultUniqueLock lock(mutex);

		while (true)
		{
			cv.wait(lock, [this, pointer]() -> bool { return !commandLists.empty() || pointer->shouldThreadStop(); });
			if (pointer->shouldThreadStop())
			{
				break;
			}

			const CommandList& cmd = commandLists.front();
			lock.unlock();
			for (const auto& x : cmd.commands)
			{
				processCommand(x);
			}

			lock.lock();
			commandLists.pop();
			const bool isQueueEmpty = commandLists.empty();
			if (isQueueEmpty)
			{
				lock.unlock();
				completionCv.notify_all();
				lock.lock();
			}
		}

		getInstance<ConcurrentGC>().detachThread();
	};

	renderer = &arg;
	if (isOnAnotherThread = anotherThread) // =, not ==.
	{
		renderingThread.beginThread(threadProc);
		renderingThread.setThreadDescription(L"Rendering Thread");
		renderingThread.setStopCallback([this](Thread&) { cv.notify_one(); });
	}
}

void GLOWE::RenderingMgr::addCommandList(CommandList&& list)
{
	if (list.commands.size() == 0)
	{
		return;
	}

	DefaultUniqueLock lock(mutex);

	if (isOnAnotherThread)
	{
		commandLists.push(std::move(list));
		lock.unlock();
		cv.notify_one();
	}
	else
	{
		for (const auto& x : list.commands)
		{
			processCommand(x);
		}
	}
}

void GLOWE::RenderingMgr::waitForCompletion()
{
	DefaultUniqueLock lock(mutex);
	completionCv.wait(lock, [this]() -> bool { return commandLists.empty(); });
}

GLOWE::ResizeableFrameAllocator& GLOWE::RenderingMgr::getFrameAllocator()
{
	return frameAllocator;
}
