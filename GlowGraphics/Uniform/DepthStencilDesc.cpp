#include "DepthStencilDesc.h"

GLOWE::DepthStencilDesc::DepthStencilDesc()
	: enableDepthTest(true), enableStencilTest(false), depthWriteMask(DepthWriteMask::All), depthCompFunc(ComparisonFunction::Less), stencilReadMask(defaultStencilReadMask), stencilWriteMask(defaultStencilWriteMask), frontFace(), backFace(), stencilRef(0xFFU)
{
}

void GLOWE::DepthStencilDesc::serialize(LoadSaveHandle& handle) const
{
	handle << enableDepthTest << enableStencilTest << depthWriteMask << depthCompFunc << stencilReadMask << stencilWriteMask << frontFace << backFace << stencilRef;
}

void GLOWE::DepthStencilDesc::deserialize(LoadSaveHandle& handle)
{
	handle >> enableDepthTest >> enableStencilTest >> depthWriteMask >> depthCompFunc >> stencilReadMask >> stencilWriteMask >> frontFace >> backFace >> stencilRef;
}

GLOWE::Hash GLOWE::DepthStencilDesc::getHash() const
{
	return Hash((const char*)this, sizeof(*this));
}

GLOWE::DepthStencilDesc::StencilOperationDesc::StencilOperationDesc()
	: failOp(StencilOperation::Keep), depthFailOp(StencilOperation::Keep), succOp(StencilOperation::Keep), comparisonFunc(ComparisonFunction::Always)
{
}
