#pragma once
#ifndef GLOWE_UNIFORM_RENDERINGMGR_INCLUDED
#define GLOWE_UNIFORM_RENDERINGMGR_INCLUDED

#include "BasicRendererImplementation.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT CommandList
		: public BasicRendererImpl, public NoCopy
	{
	public:
		enum class Priority : unsigned char
		{
			Normal = 0,
			High
		};

		struct DontCopyFlag {};
	private:
		class GLOWGRAPHICS_EXPORT Command
		{
		public:
			enum class Operation : unsigned char
			{
				Unknown,

				SetPrimitiveTopology,

				SetBlendState,

				CustomFunc,

				SetDSS,
				SetRS,
				SetInputLayout,

				CopyBuffers,
				UpdateBuffer,
				ReadBuffer,

				CopyTextures,
				CopyToTexture,
				UpdateTexture,
				ReadTexture,

				SetVertexBuffers,
				SetIndexBuffer,

				SetShaderCollection,

				Draw,
				DrawIndexed,
				DrawInstanced,
				DrawIndexedInstanced,

				SetSRVs,
				SetConstantBuffers,
				SetRenderTarget,
				SetRenderTargetAndUAVs,
				SetUAVs,
				SetViewports,
				SetScissorRects,
				SetSamplers,

				ClearColor,
				ClearDS,

				Dispatch,

				GenerateMipmaps
			};
		public:
			union
			{
				PrimitiveTopology topologyData;
				const BlendStateImpl* blendStateData;
				const DepthStencilStateImpl* dssData;
				const RasterizerStateImpl* rsData;
				const InputLayoutImpl* inputLayoutData;
				struct
				{
					const BufferImpl* src, *dst;
				} bufferCopyData;
				struct
				{
					const BufferImpl* buffer;
					union
					{
						const void* data;
						void* ownedData;
					};
					std::size_t dataSize, offset;
					bool isOwner;
				} bufferUpdateData;
				struct
				{
					const BufferImpl* buffer;
					void* data;
					std::size_t dataSize, offset;
				} readBufferData;

				struct
				{
					const TextureImpl* dst, *src;
				} copyTexturesData;
				struct
				{
					const TextureImpl* dst, *src;
					UInt3 dstPos, srcPos, size;
					unsigned int srcElemId, dstElemId;
				} copyToTextureData;
				struct
				{
					const TextureImpl* dst;
					UInt3 pos, size;
					union
					{
						const void* data;
						void* ownedData;
					};
					unsigned int subres;
					bool isOwner;
				} updateTextureData;
				struct
				{
					const TextureImpl* dst;
					void* data;
					UInt3 pos, howMany;
					unsigned int subres;
				} readTextureData;

				FrameAllocVector<const BufferImpl*> setVertexBuffersData;
				const BufferImpl* setIndexBufferData;
				const ShaderCollectionImpl* setShaderCollectionData;
				struct
				{
					unsigned int verticesCount, startingVertex;
				} drawData;
				struct
				{
					unsigned int indicesCount, startingIndex, startingVertex;
				} drawIndexedData;
				struct
				{
					unsigned int verticesPerInstance, startingVertex, instancesCount, startingInstanceLoc;
				} drawInstancedData;
				struct
				{
					int baseVertexLocation;
					unsigned int indicesPerInstance, startingVertex, instancesCount, startingInstanceLoc;
				} drawIndexedInstancedData;
				struct
				{
					FrameAllocVector<const ShaderResourceViewImpl*> srvs;
					unsigned int texUnit;
					ShaderType stage;
				} setSRVsData;
				struct
				{
					FrameAllocVector<const BufferImpl*> buffers;
					unsigned int buffSlot;
					ShaderType stage;
				} setConstantBuffersData;
				const RenderTargetImpl* setRenderTargetData;
				struct
				{
					const RenderTargetImpl* rt;
					FrameAllocVector<const UnorderedAccessViewImpl*> uavs;
					unsigned int uavStartSlot;
				} setRenderTargetAndUAVsData;
				FrameAllocVector<Viewport> setViewportsData;
				FrameAllocVector<Rect> setScissorRectsData;
				struct
				{
					FrameAllocVector<const SamplerImpl*> samplers;
					unsigned int samplerSlot;
					ShaderType stage;
				} setSamplersData;
				struct
				{
					const RenderTargetImpl* rt;
					Color color;
				} clearColorData;
				struct
				{
					const RenderTargetImpl* rt;
					float refreshDepth;
					UInt8 refreshStencil;
				} clearDSData;
				struct
				{
					unsigned int x, y, z;
				} dispatchData;
				const ShaderResourceViewImpl* generateMipmapsData;
				std::function<void(BasicRendererImpl&)> customFuncData;
			};
		private:
			Operation operation;
		public:
			Command() = delete;
			Command(const Operation op);
			Command(const Command& cmd);
			Command(Command&& cmd) noexcept;
			~Command();

			Command& operator=(const Command& cmd);
			Command& operator=(Command&& cmd) noexcept;

			friend class RenderingMgr;
		};
	private:
		Vector<Command> commands;
		Priority priority;
	private:
		virtual void drawImpl(const unsigned int verticesCount, const unsigned int startingVertex = 0) override;
		virtual void drawIndexedImpl(const unsigned int indicesCount, const unsigned int startingIndex = 0, const unsigned int startingVertex = 0) override;
		virtual void drawInstancedImpl(const unsigned int verticesPerInstance, const unsigned int instancesCount, const unsigned int startingVertex = 0, const unsigned int startingInstanceLoc = 0) override;
		virtual void drawIndexedInstancedImpl(const unsigned int indicesPerInstance, const unsigned int instancesCount, const int baseVertexLocation = 0, const unsigned int startingVertex = 0, const unsigned int startingInstanceLoc = 0) override;

		virtual void dispatchImpl(const unsigned int x, const unsigned int y, const unsigned int z) override;

		virtual void setScissorRectsImpl(const Rect* rects, const unsigned int size) override;

		virtual void setPrimitiveTopologyImpl(const PrimitiveTopology topology) override;
	public:
		CommandList();
		explicit CommandList(const unsigned int capacity);
		CommandList(CommandList&& arg) noexcept;
		virtual ~CommandList() {};

		CommandList& operator=(CommandList&& arg) noexcept;

		virtual void create(const GraphicsDeviceImpl& device) override {};
		virtual void destroy() override {};

		virtual void setBlendState(const BlendStateImpl* state) override;

		virtual void customFunction(const std::function<void(BasicRendererImpl&)>& func) override;

		virtual void setDepthStencilState(const DepthStencilStateImpl* dss) override;
		virtual void setRasterizerState(const RasterizerStateImpl* rs) override;
		virtual void setInputLayout(const InputLayoutImpl* layout) override;

		virtual void copyBuffers(const BufferImpl* src, const BufferImpl* dst) override;
		virtual void updateBuffer(const BufferImpl* buffer, const void* data, const std::size_t dataSize, const std::size_t offset = 0) override;
		void updateBuffer(const BufferImpl* buffer, const void* data, const std::size_t dataSize, const DontCopyFlag&, const std::size_t offset = 0);
		virtual void readBuffer(const BufferImpl* buffer, void* data, const std::size_t dataSize, const std::size_t offset = 0) override;

		virtual void copyTextures(const TextureImpl* src, const TextureImpl* dst) override;
		virtual void copyToTexture(const TextureImpl* src, const TextureImpl* dst, const UInt3& dstPos, const UInt3& srcPos, const UInt3& howMany, const unsigned int srcSubresourceId = 0, const unsigned int dstSubresourceId = 0) override;
		virtual void updateTexture(const TextureImpl* src, const void* data, const UInt3& pos, const UInt3& howMany, const unsigned int dstSubresourceId = 0) override;
		void updateTexture(const TextureImpl* src, const void* data, const UInt3& pos, const UInt3& howMany, const DontCopyFlag&, const unsigned int dstSubresourceId = 0);
		virtual void readTexture(const TextureImpl* src, void* data, const UInt3& pos, const UInt3& howMany, const unsigned int srcSubresourceId = 0) override;

		virtual void setVertexBuffer(const BufferImpl* buffer) override;
		virtual void setVertexBuffers(const Vector<const BufferImpl*>& buffers) override;
		virtual void setVertexBuffers(const BufferImpl** buffers, const unsigned int size) override;

		virtual void setIndexBuffer(const BufferImpl* buffer) override;

		virtual void setShaderCollection(const ShaderCollectionImpl* coll) override;

		virtual void setSRV(const ShaderResourceViewImpl* srv, const unsigned int texUnit, const ShaderType stage) override;
		virtual void setSRV(const ShaderResourceViewImpl** srvArr, const unsigned int firstTexUnit, const unsigned int arraySize, const ShaderType stage) override;

		virtual void setConstantBuffers(const BufferImpl* cb, const unsigned int buffSlot, const ShaderType stage) override;
		virtual void setConstantBuffers(const BufferImpl** cb, const unsigned int buffSlot, const unsigned int arraySize, const ShaderType stage) override;

		virtual void setRenderTarget(const RenderTargetImpl* rt) override;
		virtual void setRenderTargetAndUAVs(const RenderTargetImpl* rt, const Vector<const UnorderedAccessViewImpl*>& uavs, const unsigned int uavStartSlot = 0) override;
		virtual void setRenderTargetAndUAVs(const RenderTargetImpl* rt, const UnorderedAccessViewImpl* const* uavs, const unsigned int arraySize, const unsigned int uavStartSlot = 0) override;
		virtual void setUAVs(const UnorderedAccessViewImpl* const* uavs, const unsigned int arraySize, const unsigned int uavStartSlot = 0) override;

		virtual void setViewport(const Viewport& vp) override;
		virtual void setViewports(const Vector<Viewport>& vpsArray) override;
		virtual void setViewports(const Viewport* vpsArray, const unsigned int size) override;

		virtual void setSamplers(const SamplerImpl** arr, const unsigned int startSlot, const unsigned int arraySize, const ShaderType stage) override;

		virtual void clearColor(const RenderTargetImpl* rt, const Color& refreshCol) override; // TODO: Inconsistent with the base class' signature (optional color argument).
		virtual void clearDepthStencil(const RenderTargetImpl* rt, const float refreshDepth = 1.0f, const UInt8 refreshStencil = 0) override;

		virtual void generateMipmaps(const ShaderResourceViewImpl* tex) override;

		void setAsHighPriority();
		void clear();
		void reserve(const unsigned int newCapacity);

		bool operator<(const CommandList& right) const;
		bool operator>(const CommandList& right) const;

		friend class RenderingMgr;
	};

	class GLOWGRAPHICS_EXPORT RenderingMgr
		: public Subsystem, public NoCopy
	{
	private:
		Thread renderingThread;
		ConditionalVariable cv, completionCv;

		Mutex mutex;

		bool isOnAnotherThread;

		BasicRendererImpl* renderer;

		Queue<CommandList> commandLists;

		ResizeableFrameAllocator frameAllocator;
	private:
		void processCommand(const CommandList::Command& cmd);
	public:
		RenderingMgr() = default;
		virtual ~RenderingMgr() = default;

		void initialize(BasicRendererImpl& arg, const bool anotherThread);

		//void addCommandList(const CommandList& list);
		void addCommandList(CommandList&& list);

		void waitForCompletion();

		ResizeableFrameAllocator& getFrameAllocator();
	};
}

#endif
