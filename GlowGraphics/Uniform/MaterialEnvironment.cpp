#include "MaterialEnvironment.h"

void GLOWE::MaterialEnvironment::setVariable(const Hash& name, const void* data, const unsigned int size)
{
	auto& temp = variables[name];
	if (size > temp.first.size())
	{
		temp.first.resize(size);
		//WarningThrow(false, "Resizing a variable in MaterialEnvironment.");
		// Prbly useless.
	}

	std::memcpy(temp.first.data(), data, size);
	++temp.second;
}
