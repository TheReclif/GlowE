#include "ShaderImplementation.h"

GLOWE::Hash GLOWE::ShaderImpl::getHash() const
{
	return hash;
}

GLOWE::ShaderImpl::CompilationData::CompilationData()
	: entrypoint(u8"main"), version{ 0, 0 }
{
}

void GLOWE::VertexShaderImpl::destroy()
{
	instanceLayoutDesc = InputLayoutDesc();
}

const GLOWE::InputLayoutDesc& GLOWE::VertexShaderImpl::getInstanceInputLayout() const
{
	return instanceLayoutDesc;
}
