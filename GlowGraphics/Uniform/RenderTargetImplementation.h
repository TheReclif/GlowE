#pragma once
#ifndef GLOWE_UNIFORM_RENDERTARGETIMPLEMENTATION_INCLUDED
#define GLOWE_UNIFORM_RENDERTARGETIMPLEMENTATION_INCLUDED

#include "glowgraphics_export.h"

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/MemoryMgr.h"

namespace GLOWE
{
	class TextureImpl;
	class GraphicsDeviceImpl;

	class GLOWGRAPHICS_EXPORT RenderTargetImpl
		: public NoCopy
	{
	protected:
		Float2 rtvSize;
	public:
		RenderTargetImpl() = default;
		virtual ~RenderTargetImpl() = default;

		virtual void create(const SharedPtr<TextureImpl>& texToUse, const SharedPtr<TextureImpl>& depthStencilTex, const GraphicsDeviceImpl& device) = 0;
		virtual void create(const Vector<SharedPtr<TextureImpl>>& textures, const SharedPtr<TextureImpl>& depthStencilTex, const GraphicsDeviceImpl& device) = 0;
		virtual void destroy() = 0;

		virtual bool checkIsValid() const = 0;

		Float2 getSize() const;
	};
}

#endif
