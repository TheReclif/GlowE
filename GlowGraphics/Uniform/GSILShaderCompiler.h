#pragma once
#ifndef GLOWE_GRAPHICS_UNIFORM_GSILSHADERCOMPILER_INCLUDED
#define GLOWE_GRAPHICS_UNIFORM_GSILSHADERCOMPILER_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/String.h"

#include "BlendDesc.h"
#include "DepthStencilDesc.h"
#include "RasterizerDesc.h"
#include "SamplerDesc.h"
#include "BasicRendererImplementation.h"
#include "StructureLayout.h"
#include "TextureImplementation.h"
#include "InputLayoutDesc.h"

// Example GSIL shaders (they show all existing GSIL instructions):
// ---
/// HLSL { float getBlendCoef() { return 0.4f; }};
// The above will place this code only when compiling to HLSL.
// The same applies to the GLSL option.
// Not implemented yet (probably won't be implemented in the future).
// ---
/// Technique Simple
/// {
///		Pass
///		{
///			VertexShader = Shader(3.0, VSMain);
///			PixelShader = Shader(3.0, PSMain);
///			BlendState = myBlendState;
///		}
/// }
// It will create a technique called "Simple" in the default "" effect group with one (default) pass.
// The pass compiles VertexShader and PixelShader with Shader Model 3.0.
// (3.0 stands for DirectX 11 level of features).
// Also, it will instrument the engine to set a blend state to previously defined myBlendState state.
// ---
// Let's consider the following block of code:
/// void VSMain(in VSInput input, uniform int texturesQuality, uniform bool useFilter)
/// {
///		...
/// }
/// Technique ...
/// {
///		Pass 0
///		{
///			VertexShader = Shader(3.0, VSMain, 1, true);
///		}
/// }
// This code will replace texturesQuality and useFilter with 1 and true respectively.
// ---
/// Texture2D mainTex(SamplerName) : Name(Main Texture);
// The above will instruct the engine to treat mainTex as a Material property.
// ---
// In order to sample texture use Texture.sample(argument).
/// Texture2D myTex(MySampler) : Name(MyTex);
/// ...
/// Float4 color = myTex.sample(Float2(0.0f, 1.0f));
// ---


namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT GSILShader
		: public BinarySerializable
	{
	public:
		static constexpr unsigned int initialCodeBufferSize = 1024U;

		class GLOWGRAPHICS_EXPORT CompilationErrorException
			: public StringException
		{
		private:
			Deque<String> queue;

			static String createErrorMsg(const Deque<String>& errors);
		public:
			CompilationErrorException(Deque<String>&& errQueue)
				: StringException(createErrorMsg(errQueue)), queue(std::move(errQueue))
			{}

			const Deque<String>& getErrors() const;
		};

		class GLOWGRAPHICS_EXPORT Property
		{
		public:
			enum class Type : unsigned char
			{
				Normal,
				Hidden,
				EngineSupplied
			};

			Type type = Type::Normal;
			String name;
			Map<Hash, String> constraints;
		};

		class Technique;

		class GLOWGRAPHICS_EXPORT Pass
		{
		public:
			class GLOWGRAPHICS_EXPORT ShaderDefinition
			{
			public:
				String func;
				float version;
				Vector<String> replacements;
				Vector<unsigned int> usedFunctions, usedTextures, usedCBuffers, usedUAVs;
			};

			const Technique* owner;

			ShaderDefinition vertexShader, pixelShader, computeShader, geometryShader, hullShader, domainShader;
			int blendStateId = -1, dssId = -1, rastId = -1, inputLayoutId = -1;
		public:
			Pass() = delete;
			Pass(const Technique& technique);
		};

		class GLOWGRAPHICS_EXPORT Technique
		{
		public:
			struct Tag
			{
				enum class Type
				{
					SupportsDirectionalLights,
					SupportsPointLights,
					SupportsSpotLights,
					IsTexturePresent
				};

				struct SupportsLightingInfo
				{
					unsigned int supportedLights;
				};

				struct IsTexturePresentInfo
				{
					unsigned int texID;
				};

				union
				{
					SupportsLightingInfo supportsLightingInfo;
					IsTexturePresentInfo isTexturePresentInfo;
				};

				Type type;
			};
		public:
			Map<unsigned int, Pass> passes;
			Map<Hash, Vector<int>> variants; // Hashes like "0|1|5|4|1".
			Vector<Tag> tags;
		public:
			Technique() = default;
			Technique(const Technique& arg);
			Technique(Technique&& arg);
		};

		class GLOWGRAPHICS_EXPORT Texture
		{
		public:
			String name;
			Property property;
			TextureDesc::Type type;
			bool isMultiSample;
			unsigned short samplerID; // ID in samplers vector.
		};

		class GLOWGRAPHICS_EXPORT UAV
		{
		public:
			enum class Type : unsigned char
			{
				Unknown,
				Buffer,
				Texture1D,
				Texture2D,
				Texture3D,
				Texture1DArray,
				Texture2DArray
			};
		public:
			String name;
			Property property;
			Type type;
			Format format;
		};

		class GLOWGRAPHICS_EXPORT Function
		{
		public:
			String name, returnType, code;
			Vector<String> arguments, attributes;
			Vector<unsigned int> usedTextures, usedFunctions, usedCBuffers, usedUAVs;
		};

		class GLOWGRAPHICS_EXPORT Structure
		{
		public:
			class GLOWGRAPHICS_EXPORT Field
			{
			public:
				enum class Type : unsigned char
				{
					Unknown,
					Bool,
					Float,
					Int,
					UInt,
					Double,
					Struct
				};
				enum class InterpolationSpecifier : unsigned char
				{
					NoInterpolation,
					NoPerspective,
					Smooth
				};
			public:
				unsigned int arraySize = 1;
				unsigned int typesCount = 0, typesCountMat = 0; // 4, 0 for float4; 4, 4 for mat4x4.
				// For struct types typesCount will be its type's index in structures vector and typesCountMat it sizeof.

				bool isSV = false, isInstanceData = false;
				InterpolationSpecifier interpolation = InterpolationSpecifier::Smooth;
				Type type;
				String name;
				Property property;

				Format asFormat() const;
			};
		private:
			StructureLayout generateBasicCbufferLayoutInternal() const;
			NamedStructureLayout generateCbufferLayoutInternal(const bool usePropertyNames) const;
		public:
			bool isInOut;
			String name;
			Vector<Field> fields;
			const GSILShader* owningShader;

			StructureLayout generateBasicLayout() const;
			StructureLayout generateBasicCbufferLayout() const;

			NamedStructureLayout generateLayout() const;
			NamedStructureLayout generateCbufferLayout() const;
			NamedStructureLayout generateCbufferPropertyLayout() const; // Uses property.name instead of name as field name.
		};
	public:
		Vector<GSILShader::Texture> textures;
		Vector<UAV> uavs;
		Vector<Pair<String, SamplerDesc>> samplers;
		Vector<Pair<String, BlendDesc>> blendStates;
		Vector<Pair<String, DepthStencilDesc>> depthStencilStates;
		Vector<Pair<String, RasterizerDesc>> rasterizerStates;
		Map<String, Map<String, Technique>> techniqueGroups; // The default group is "".

		Vector<InputLayoutDesc> inputLayouts;
		Vector<Function> functions;
		Vector<Structure> constantBuffers;
		Vector<Structure> structures;
	private:
		void registerInputLayouts();
		void prepareFunctionHLSL(GLOWE::String& code, const Function& func) const;
	public:
		GSILShader() = default;
		GSILShader(const String& gsilCode);
		~GSILShader() = default;

		void fromGSILCode(const String& gsilCode);
		void fromGSILCode(const String& gsilCode, String& tokensOutput); // Will produce raw output from interpreter. May contain mangled names.

		String toHLSL() const; // Warning: this function makes a header to lighten the next function's load.
		String toHLSL(const Pass& pass, const ShaderType& shaderType) const; // Does not replace anything for any specific variant. Also, it does not include the header.
		String toHLSL(const Pass& pass, const Hash& variant, const ShaderType& shaderType) const; // Warning: this function generates only the code for given shader type and pass' main function.
		String replaceHLSLForVariant(const String& code, const Pass& pass, const Hash& variant, const ShaderType& shaderType) const; // Replaces uniforms in an output from the toHLSL(Pass, ShaderType) function.
		
		static String replaceHLSLForFuncName(const String& code, const String& func, const Vector<Pair<String, String>>& replacements);

		bool checkIsValid() const;

		virtual void serialize(LoadSaveHandle& handle) const override;
		virtual void deserialize(LoadSaveHandle& handle) override;

		friend class Effect;
	};
}

#endif
