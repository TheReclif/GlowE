#pragma once
#ifndef GLOWE_UNIFORM_DEPTHSTENCILDESC_INCLUDED
#define GLOWE_UNIFORM_DEPTHSTENCILDESC_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/FileIO.h"
#include "../../GlowSystem/SerializableDataCreator.h"
#include "../../GlowSystem/LoadSaveHandle.h"

#include "Common.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT DepthStencilDesc
		: public BinarySerializable, public Hashable
	{
	private:
		static constexpr UInt8 defaultStencilReadMask = 0xFF;
		static constexpr UInt8 defaultStencilWriteMask = 0xFF;

	public:
		enum class DepthWriteMask
		{
			Zero,
			All
		};

		enum class StencilOperation
		{
			Keep,
			Zero,
			Replace,
			Increment,
			Decrement,
			IncrementClamp,
			DecrementClamp,
			Invert
		};

		class GLOWGRAPHICS_EXPORT StencilOperationDesc
		{
		public:
			StencilOperation failOp, depthFailOp, succOp;
			ComparisonFunction comparisonFunc;
		public:
			StencilOperationDesc();
		};
	public:
		bool enableDepthTest, enableStencilTest;
		DepthWriteMask depthWriteMask;
		ComparisonFunction depthCompFunc;
		UInt8 stencilReadMask, stencilWriteMask;
		StencilOperationDesc frontFace, backFace;
		unsigned int stencilRef;
	public:
		DepthStencilDesc();

		virtual void serialize(LoadSaveHandle& handle) const override;
		virtual void deserialize(LoadSaveHandle& handle) override;

		virtual Hash getHash() const override;
	};
}

#endif
