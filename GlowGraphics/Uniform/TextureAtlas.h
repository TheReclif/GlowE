#pragma once
#ifndef GLOWE_UNIFORM_TEXTUREATLAS_INCLUDED
#define GLOWE_UNIFORM_TEXTUREATLAS_INCLUDED

#include "../../GlowSystem/MemoryMgr.h"
#include "../../GlowSystem/ThreadClass.h"

#include "../../GlowMath/Rect.h"

#include "TextureImplementation.h"
#include "BasicRendererImplementation.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT TextureAtlas
	{
	public:
		struct SubtextureDesc
		{
			UInt2 pos, size;
		};
	private:
		Atomic<bool> isDuringResize;
		Mutex mutex;
		SharedPtr<TextureImpl> atlas, secondTexture;
		Map<Hash, SubtextureDesc> subtextures;
		Vector<Pair<const TextureImpl*, SubtextureDesc*>> texturesToAdd;
		Vector<Pair<Vector<char>, SubtextureDesc*>> dataTexturesToAdd;
		Multimap<unsigned int, SubtextureDesc> freeBlocks;
		UInt2 size;
		Format atlasFormat;
		const GraphicsDeviceImpl* gc;
	private:
		void resize(BasicRendererImpl& renderer, const UInt2& newDimensions);
	public:
		TextureAtlas();

		void create(const GraphicsDeviceImpl* const graphicsDevice, const TextureDesc& desc);
		void create(const GraphicsDeviceImpl* const graphicsDevice, const TextureDesc& desc, const void* data);
		void destroy();

		const SubtextureDesc* addTexture(const Hash& texHash, const TextureImpl& tex, BasicRendererImpl& renderer);
		const SubtextureDesc* addTexture(const Hash& texHash, const UInt2& texSize, const void* data, BasicRendererImpl& renderer);

		// Used to initialize TextureAtlas from file or from saved state.
		void overrideBlocks(const Vector<Pair<unsigned int, SubtextureDesc>>& newFreeBlocks, const Vector<Pair<Hash, SubtextureDesc>>& newSubtextures);
		// Used to save info used by FontCache.
		void getBlocksData(Vector<Pair<unsigned int, SubtextureDesc>>& freeBlocksSave, Vector<Pair<Hash, SubtextureDesc>>& subtexturesSave) const;

		const SubtextureDesc* getSubtexture(const Hash& subtexHash) const;
		const SharedPtr<TextureImpl>& getTexture() const; // Call every time you want to set a texture in a Material.
	};
}

#endif
