#include "BasicRendererImplementation.h"

GLOWE::BasicRendererImpl::PrimitiveTopology GLOWE::BasicRendererImpl::getCurrentPrimitiveTopology() const
{
	return currentTopology;
}

void GLOWE::BasicRendererImpl::setPrimitiveTopology(const PrimitiveTopology topology)
{
	if (currentTopology != topology)
	{
		setPrimitiveTopologyImpl(topology);
		currentTopology = topology;
	}
}

void GLOWE::BasicRendererImpl::updateScissors()
{
	if (scissorRectsNeedUpdate)
	{
		setScissorRectsImpl(scissorRects.data(), scissorRects.size());
		scissorRectsNeedUpdate = false;
	}
}

void GLOWE::BasicRendererImpl::setScissorRect(const Rect& rect)
{
	scissorRects.resize(1);
	scissorRects[0] = rect;
	scissorRectsNeedUpdate = true;
}

void GLOWE::BasicRendererImpl::setScissorRects(const Vector<Rect>& rects)
{
	scissorRects = rects;
	scissorRectsNeedUpdate = true;
}

void GLOWE::BasicRendererImpl::setScissorRects(const Rect* rects, const unsigned int size)
{
	scissorRects.assign(rects, rects + size);
	scissorRectsNeedUpdate = true;
}

void GLOWE::BasicRendererImpl::draw(const unsigned int verticesCount, const unsigned int startingVertex)
{
	updateScissors();
	drawImpl(verticesCount, startingVertex);
}

void GLOWE::BasicRendererImpl::drawIndexed(const unsigned int indicesCount, const unsigned int startingIndex, const unsigned int startingVertex)
{
	updateScissors();
	drawIndexedImpl(indicesCount, startingIndex, startingVertex);
}

void GLOWE::BasicRendererImpl::drawInstanced(const unsigned int verticesPerInstance, const unsigned int instancesCount, const unsigned int startingVertex, const unsigned int startingInstanceLoc)
{
	updateScissors();
	drawInstancedImpl(verticesPerInstance, instancesCount, startingVertex, startingInstanceLoc);
}

void GLOWE::BasicRendererImpl::drawIndexedInstanced(const unsigned int indicesPerInstance, const unsigned int instancesCount, const int baseVertexLocation, const unsigned int startingVertex, const unsigned int startingInstanceLoc)
{
	updateScissors();
	drawIndexedInstancedImpl(indicesPerInstance, instancesCount, baseVertexLocation, startingVertex, startingInstanceLoc);
}

void GLOWE::BasicRendererImpl::dispatch(const unsigned int x, const unsigned int y, const unsigned int z)
{
	updateScissors();
	dispatchImpl(x, y, z);
}
