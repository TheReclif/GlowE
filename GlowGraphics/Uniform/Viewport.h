#pragma once
#ifndef GLOWE_UNIFORM_VIEWPORT_INCLUDED
#define GLOWE_UNIFORM_VIEWPORT_INCLUDED

#include "glowgraphics_export.h"

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/SerializableDataCreator.h"

namespace GLOWE
{
	struct GLOWGRAPHICS_EXPORT Viewport
	{
		float topX, topY, width, height, minDepth, maxDepth;

		Viewport();

		GlowStaticSerialize(GlowBases(), GlowField(topX), GlowField(topY), GlowField(width), GlowField(height), GlowField(minDepth), GlowField(maxDepth));
	};
}

#endif
