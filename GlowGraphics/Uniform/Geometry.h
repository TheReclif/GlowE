#pragma once
#ifndef GLOWE_GRAPHICS_UNIFORM_GEOMETRY_INCLUDED
#define GLOWE_GRAPHICS_UNIFORM_GEOMETRY_INCLUDED

#include "InputLayoutDesc.h"
#include "ShaderCollectionImplementation.h"
#include "InputLayoutImplementation.h"
#include "BasicRendererImplementation.h"
#include "GraphicsDeviceImplementation.h"

#include "../../GlowMath/AxisAlignedBoundingBox.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT GeometryBuffersImpl
	{
	public:
		virtual ~GeometryBuffersImpl() = default;

		virtual void apply(BasicRendererImpl& renderer, const GraphicsDeviceImpl& device, const ShaderCollectionImpl& shaderColl) = 0;
		virtual bool checkIsIndexed() const = 0;
	};

	class GLOWGRAPHICS_EXPORT GeometryBuffers
		: public Lockable, public GeometryBuffersImpl
	{
	public:
		UniquePtr<BufferImpl> vertexBuffer, indicesBuffer;
		InputLayoutDesc vertexLayout;
		UnorderedMap<void*, UniquePtr<InputLayoutImpl>> inputLayouts; // One per vertex shader.
	public:
		GeometryBuffers();

		virtual void apply(BasicRendererImpl& renderer, const GraphicsDeviceImpl& device, const ShaderCollectionImpl& shaderColl) override;
		virtual bool checkIsIndexed() const override;
	};

	class GLOWGRAPHICS_EXPORT ComplexGeometryBuffers
		: public Lockable, public GeometryBuffersImpl
	{
	private:
		Vector<const BufferImpl*> buffers;
		List<SharedPtr<void>> ownedBuffers;
		const BufferImpl* indicesBuffer; // Optional.
		InputLayoutDesc inputLayoutDesc;
		UnorderedMap<void*, UniquePtr<InputLayoutImpl>> inputLayouts; // One per vertex shader.
	public:
		unsigned int addBuffer(BufferImpl* notOwnedBuffer, const InputLayoutDesc& inDesc);
		unsigned int addBuffer(const SharedPtr<BufferImpl>& ownedBuffer, const InputLayoutDesc& inDesc);
		unsigned int addBuffer(const SharedPtr<GeometryBuffers>& ownedBuffs);

		void setIndicesBuffer(const BufferImpl* buff);
		void setIndicesBuffer(const SharedPtr<GeometryBuffers>& ownedBuffs);
		
		// Won't clear the ownership! Won't change input layouts! UB when passing a buffer with different vertex layout.
		void replaceBuffer(const unsigned int pos, const BufferImpl* newBuffer);

		virtual void apply(BasicRendererImpl& renderer, const GraphicsDeviceImpl& device, const ShaderCollectionImpl& shaderColl) override;
		virtual bool checkIsIndexed() const override;
	};

	class GLOWGRAPHICS_EXPORT Geometry
	{
	public:
		SharedPtr<GeometryBuffersImpl> buffers = nullptr;
		UInt offset = 0, count = 0;
		AABB localAABB;
	public:
		void apply(BasicRendererImpl& renderer, const GraphicsDeviceImpl& device, const ShaderCollectionImpl& shaderColl);
		void draw(BasicRendererImpl& renderer) const;
		void drawInstanced(const unsigned int instancesCount, BasicRendererImpl& renderer) const;
	};

	class GLOWGRAPHICS_EXPORT GeometryBuilder
	{
	public:
		struct Options
		{
			/// @brief Builds the local AABB if true. Will throw an exception if there is no suitable "position" attribute.
			bool buildAABB = true;
			bool autoCalculateTangentSpace = true;
			CPUAccess cpuAccess = CPUAccess::None;
			Usage usage = Usage::Immutable;
		};
	private:
		UInt64 size = 0;
		InputLayoutDesc inputLayoutDesc;
		Vector<Vector<Vector<unsigned char>>> attribDatas;
		Vector<unsigned char> indexData;
		bool indicesAreShort = false; // If true then every index is an unsigned int.
	public:
		GeometryBuilder();

		void addVertexBuffer();
		void addVertexAttribute(const String& name, const void* const data, const Format type, const UInt64 howMany, const int bufferId = -1);
		template<unsigned int ArrSize>
		void addVertexAttribute(const String& name, const Vector<Float1D<ArrSize>>& data, const int bufferId = -1)
		{
			addVertexAttribute(name, data.data(), Format(Format::Type::Float, ArrSize), data.size(), bufferId);
		}

		void setIndices(const unsigned short* const indices, const UInt64 size);
		inline void setIndices(const Vector<unsigned short>& indices)
		{
			setIndices(indices.data(), indices.size());
		}
		void setIndices(const unsigned int* const indices, const UInt64 size);
		inline void setIndices(const Vector<unsigned int>& indices)
		{
			setIndices(indices.data(), indices.size());
		}

		void build(Geometry& output, const GraphicsDeviceImpl& device, const Options& cfg = Options()) const;
	};
}

#endif
