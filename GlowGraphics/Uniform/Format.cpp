#include "Format.h"

GLOWE::Format::Format()
	: type(Type::Float), elemsCount(4), isNormalized(false), isSRGB(false)
{
}

GLOWE::Format::Format(const Type t, const UInt count, const bool norm, const bool srgb)
	: type(t), elemsCount(count), isNormalized(norm), isSRGB(srgb)
{
}

GLOWE::UInt GLOWE::Format::getSize() const
{
	UInt typeSize = 4;

	switch (type)
	{
	case Type::Byte:
	case Type::UnsignedByte:
	case Type::Struct:
		typeSize = 1;
		break;
	case Type::Short:
	case Type::UnsignedShort:
	case Type::HalfFloat:
		typeSize = 2;
		break;
	}

	return typeSize * elemsCount;
}

void GLOWE::Format::serialize(LoadSaveHandle& handle) const
{
	handle << type << elemsCount << isNormalized << isSRGB;
}

void GLOWE::Format::deserialize(LoadSaveHandle& handle)
{
	handle >> type >> elemsCount >> isNormalized >> isSRGB;
}

GLOWE::Hash GLOWE::Format::getHash() const
{
	String temp = toString((UInt)type) + toString(elemsCount) + toString(isNormalized ? 1 : 0) + toString(isSRGB ? 1 : 0);
	return temp;
}

bool GLOWE::Format::operator==(const Format& other) const
{
	return type == other.type
		&& elemsCount == other.elemsCount
		&& isNormalized == other.isNormalized
		&& isSRGB == other.isSRGB;
}

bool GLOWE::Format::operator!=(const Format& other) const
{
	return !(*this == other);
}
