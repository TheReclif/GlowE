#include "BufferImplementation.h"

GLOWE::BufferDesc::BufferDesc()
	: usage(Usage::Default), cpuAccess(CPUAccess::None), binds(Binding::None), layout()
{
}

GLOWE::Hash GLOWE::BufferDesc::getHash() const
{
	String temp;
	temp += toString((unsigned char)usage);
	temp += toString((unsigned char)cpuAccess);
	temp += toString((unsigned char)binds);
	temp += layout.getHash();

	return Hash(temp);
}

GLOWE::BufferDesc GLOWE::BufferImpl::getDesc() const
{
	return usedDesc;
}

unsigned long long GLOWE::BufferImpl::getSize() const
{
	return bufferSize;
}
