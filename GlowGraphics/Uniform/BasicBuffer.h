#pragma once
#ifndef GLOWE_UNIFORM_BASICBUFFER_INCLUDED
#define GLOWE_UNIFORM_BASICBUFFER_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/SerializableDataCreator.h"
#include "../../GlowSystem/Error.h"

#include "StructureLayout.h"

namespace GLOWE
{
	class GLOWGRAPHICS_EXPORT BasicBuffer
		: public BinarySerializable
	{
	private:
		StructureLayout layout;
		Vector<char> memory;
	public:
		BasicBuffer(void) = default;
		explicit BasicBuffer(const StructureLayout& arg, const std::size_t howManyElems = 0);

		void setLayout(const StructureLayout& arg);

		void resize(const std::size_t newElemsCount);

		template<class Type>
		void modifyElement(const unsigned int elem, const unsigned int elemInLayout, const Type& arg);
		void modifyElement(const unsigned int elem, const unsigned int elemInLayout, const void* data, const unsigned int dataSize);

		template<class Type>
		void modifyElements(const unsigned int elemInLayout, const Type* arr); // Array size must equal or exceed the count of elements.
		void modifyElements(const unsigned int elem, const unsigned int elemInLayout, const void** arr, const unsigned int dataSize); // Each data segment in arr must be at least dataSize big.

		void setContents(const void* buff); // buff must have at least memory.size() bytes, otherwise an overflow error will occur.

		template<class Type>
		const Type& getElement(const unsigned int elem, const unsigned int elemInLayout) const;
		template<class Type>
		Vector<Type> getElements(const unsigned int elemInLayout) const; // Returns whole array (newly created).

		// In bytes
		const StructureLayout& getLayout(void) const;

		std::size_t getBufferSize(void) const;

		unsigned int getElemsCount(void) const;

		const void* getData(void) const;

		virtual void serialize(LoadSaveHandle& handle) const override;
		virtual void deserialize(LoadSaveHandle& handle) override;
	};

	template<class Type>
	inline void BasicBuffer::modifyElement(const unsigned int elem, const unsigned int elemInLayout, const Type& arg)
	{
		static_assert(!std::is_polymorphic<Type>::value, "Copying polymorphic type (with vtable pointer) may cause untraceable runtime errors.");

		if (layout.getElemSize(elemInLayout) < sizeof(Type))
		{
			WarningThrow(false, String(u8"Type size exceeding element size in StructureLayout."));
			return;
		}

		void* temp = layout.getAddressOfElement(elemInLayout, memory.data() + (layout.getSize()* elem));
		//(*((Type*)temp)) = arg;
		std::memcpy(temp, &arg, sizeof(Type));
	}

	template<class Type>
	inline void BasicBuffer::modifyElements(const unsigned int elemInLayout, const Type * arr)
	{
		static_assert(!std::is_polymorphic<Type>::value, "Copying polymorphic type (with vtable pointer) may cause untraceable runtime errors.");

		if (layout.getElemSize(elemInLayout) < sizeof(Type))
		{
			WarningThrow(false, String(u8"Type size exceeding element size in StructureLayout."));
			throw Exception();
		}

		void* temp = memory.data() + layout.getOffsetToElem(elemInLayout);
		const auto elemSize = layout.getSize();

		for (unsigned int x = 0; x < (memory.size() / elemSize); ++x)
		{
			//*((Type*)((char*)(temp)+(x * elemSize))) = arr[x];
			std::memcpy((char*)(temp) + (x * elemSize), &(arr[x]), sizeof(Type));
		}
	}

	template<class Type>
	inline const Type& BasicBuffer::getElement(const unsigned int elem, const unsigned int elemInLayout) const
	{
		static_assert(!std::is_polymorphic<Type>::value, "Returning polymorphic type (with vtable pointer) may cause untraceable runtime errors.");

		if (layout.getElemSize(elemInLayout) < sizeof(Type))
		{
			WarningThrow(false, String(u8"Type size exceeding element size in StructureLayout."));
			throw Exception();
		}

		return *((const Type*)((memory.data() + layout.getOffsetToElem(elemInLayout)) + (elem * layout.getSize())));
	}

	template<class Type>
	inline Vector<Type> BasicBuffer::getElements(const unsigned int elemInLayout) const
	{
		static_assert(!std::is_polymorphic<Type>::value, "Returning polymorphic type (with vtable pointer) may cause untraceable runtime errors.");

		if (layout.getElemSize(elemInLayout) < sizeof(Type))
		{
			WarningThrow(false, String(u8"Type size exceeding element size in StructureLayout."));
			return Vector<Type>();
		}

		const char* const temp = memory.data() + layout.getOffsetToElem(elemInLayout);
		const auto count = getElemsCount();
		const auto size = layout.getSize();
		Vector<Type> result(count);

		for (unsigned int x = 0; x < count; ++x)
		{
			result[x] = *((const Type*)(temp + x * size));
		}

		return result;
	}
}

#endif
