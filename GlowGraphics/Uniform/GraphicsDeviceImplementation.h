#pragma once
#ifndef GLOWE_GRAPHICSCONTEXTIMPL_INCLUDED
#define GLOWE_GRAPHICSCONTEXTIMPL_INCLUDED

#include "Format.h"

#include "../../GlowSystem/Utility.h"
#include "../../GlowWindow/Uniform/VideoMode.h"
#include "../../GlowWindow/Uniform/WndHandle.h"

namespace GLOWE
{
	class BasicRendererImpl;
	class RenderTargetImpl;

	struct GLOWGRAPHICS_EXPORT GraphicsDeviceSettings
	{
		bool useMultithreadedRendering = true;
	};

	struct GLOWGRAPHICS_EXPORT GraphicsAPIInfo
	{
		enum class GraphicsAPI
		{
			Unspecified,
			Uninitialized = Unspecified,
			Fake,
			DirectX,
			OpenGL
		};

		GraphicsAPI usedAPI;
		int versionFirst, versionSecond;
	};

	class GLOWGRAPHICS_EXPORT GraphicsDeviceImpl
		: public NoCopy
	{
	public:
		virtual ~GraphicsDeviceImpl() = default;

		virtual void create(const GraphicsDeviceSettings& desc) = 0;
		virtual void destroy() = 0;

		virtual bool checkIsCreated() const = 0;

		virtual GraphicsAPIInfo getAPIInfo() const = 0;

		virtual BasicRendererImpl& getDefaultRenderer() = 0;
		virtual const BasicRendererImpl& getDefaultRenderer() const = 0;

		virtual UInt64 queryAvailableMemory() const = 0;
		virtual UInt64 queryOccupiedMemory() const = 0;
	};
}

#endif