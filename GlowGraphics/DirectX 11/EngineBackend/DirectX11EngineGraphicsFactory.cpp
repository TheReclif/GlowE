#include "DirectX11EngineGraphicsFactory.h"
#include "DirectX11ShaderCacheImplementation.h"

extern "C" GLOWE::GraphicsFactoryImpl * createFactory()
{
	// new for Deletable will use GlowE's allocator and allow the delete operator to deallocate the memory using GlowE's memory manager.
	return new GLOWE::Deletable<GLOWE::DirectX11::EngineGraphicsFactory>();
}

GLOWE::UniquePtr<GLOWE::ShaderCacheImpl> GLOWE::DirectX11::EngineGraphicsFactory::createShaderCache() const
{
	return makeUniquePtr<DirectX11::ShaderCache>();
}

GLOWE::UniquePtr<GLOWE::BlendStateImpl> GLOWE::DirectX11::EngineGraphicsFactory::createBlendState() const
{
    return factory.createBlendState();
}

GLOWE::UniquePtr<GLOWE::BufferImpl> GLOWE::DirectX11::EngineGraphicsFactory::createBuffer() const
{
    return factory.createBuffer();
}

GLOWE::UniquePtr<GLOWE::DepthStencilStateImpl> GLOWE::DirectX11::EngineGraphicsFactory::createDepthStencilState() const
{
    return factory.createDepthStencilState();
}

GLOWE::UniquePtr<GLOWE::GraphicsDeviceImpl> GLOWE::DirectX11::EngineGraphicsFactory::createGraphicsDevice() const
{
    return factory.createGraphicsDevice();
}

GLOWE::UniquePtr<GLOWE::InputLayoutImpl> GLOWE::DirectX11::EngineGraphicsFactory::createInputLayout() const
{
    return factory.createInputLayout();
}

GLOWE::UniquePtr<GLOWE::RasterizerStateImpl> GLOWE::DirectX11::EngineGraphicsFactory::createRasterizerState() const
{
    return factory.createRasterizerState();
}

GLOWE::UniquePtr<GLOWE::RenderTargetImpl> GLOWE::DirectX11::EngineGraphicsFactory::createRenderTarget() const
{
    return factory.createRenderTarget();
}

GLOWE::UniquePtr<GLOWE::SamplerImpl> GLOWE::DirectX11::EngineGraphicsFactory::createSampler() const
{
    return factory.createSampler();
}

GLOWE::UniquePtr<GLOWE::ShaderCollectionImpl> GLOWE::DirectX11::EngineGraphicsFactory::createShaderCollection() const
{
    return factory.createShaderCollection();
}

GLOWE::UniquePtr<GLOWE::VertexShaderImpl> GLOWE::DirectX11::EngineGraphicsFactory::createVertexShader() const
{
    return factory.createVertexShader();
}

GLOWE::UniquePtr<GLOWE::PixelShaderImpl> GLOWE::DirectX11::EngineGraphicsFactory::createPixelShader() const
{
    return factory.createPixelShader();
}

GLOWE::UniquePtr<GLOWE::GeometryShaderImpl> GLOWE::DirectX11::EngineGraphicsFactory::createGeometryShader() const
{
    return factory.createGeometryShader();
}

GLOWE::UniquePtr<GLOWE::ComputeShaderImpl> GLOWE::DirectX11::EngineGraphicsFactory::createComputeShader() const
{
    return factory.createComputeShader();
}

GLOWE::UniquePtr<GLOWE::ShaderReflectImpl> GLOWE::DirectX11::EngineGraphicsFactory::createShaderReflection() const
{
    return factory.createShaderReflection();
}

GLOWE::UniquePtr<GLOWE::TextureImpl> GLOWE::DirectX11::EngineGraphicsFactory::createTexture() const
{
    return factory.createTexture();
}

GLOWE::UniquePtr<GLOWE::ShaderResourceViewImpl> GLOWE::DirectX11::EngineGraphicsFactory::createShaderResourceView() const
{
    return factory.createShaderResourceView();
}

GLOWE::UniquePtr<GLOWE::UnorderedAccessViewImpl> GLOWE::DirectX11::EngineGraphicsFactory::createUnorderedAccessView() const
{
    return factory.createUnorderedAccessView();
}

GLOWE::UniquePtr<GLOWE::SwapChain> GLOWE::DirectX11::EngineGraphicsFactory::createSwapChain() const
{
    return factory.createSwapChain();
}
