#pragma once
#ifndef GLOWE_DIRECTX11_ENGINE_GRAPHICSFACTORY_INCLUDED
#define GLOWE_DIRECTX11_ENGINE_GRAPHICSFACTORY_INCLUDED

#include "../../../GlowEngine/EngineGraphicsFactory.h"

#include "../GraphicsBackend/DirectX11GraphicsFactory.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class EngineGraphicsFactory
			: public GLOWE::EngineGraphicsFactory
		{
		private:
			GLOWE::DirectX11::GraphicsFactory factory;
		public:
			virtual ~EngineGraphicsFactory() = default;
			// Engine stuff.
			virtual UniquePtr<ShaderCacheImpl> createShaderCache() const override;
			// Graphics stuff.
			virtual UniquePtr<BlendStateImpl> createBlendState() const override;

			virtual UniquePtr<BufferImpl> createBuffer() const override;

			virtual UniquePtr<DepthStencilStateImpl> createDepthStencilState() const override;

			virtual UniquePtr<GraphicsDeviceImpl> createGraphicsDevice() const override;

			virtual UniquePtr<InputLayoutImpl> createInputLayout() const override;

			virtual UniquePtr<RasterizerStateImpl> createRasterizerState() const override;

			virtual UniquePtr<RenderTargetImpl> createRenderTarget() const override;

			virtual UniquePtr<SamplerImpl> createSampler() const override;

			virtual UniquePtr<ShaderCollectionImpl> createShaderCollection() const override;

			virtual UniquePtr<GLOWE::VertexShaderImpl> createVertexShader() const override;

			virtual UniquePtr<GLOWE::PixelShaderImpl> createPixelShader() const override;

			virtual UniquePtr<GLOWE::GeometryShaderImpl> createGeometryShader() const override;

			virtual UniquePtr<GLOWE::ComputeShaderImpl> createComputeShader() const override;

			virtual UniquePtr<ShaderReflectImpl> createShaderReflection() const override;

			virtual UniquePtr<TextureImpl> createTexture() const override;

			virtual UniquePtr<ShaderResourceViewImpl> createShaderResourceView() const override;

			virtual UniquePtr<UnorderedAccessViewImpl> createUnorderedAccessView() const override;

			virtual UniquePtr<GLOWE::SwapChain> createSwapChain() const override;
		};
	}
}

#endif
