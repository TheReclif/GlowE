#pragma once
#ifndef GLOWE_DIRECTX_11_SHADERCACHEIMPL_INCLUDED
#define GLOWE_DIRECTX_11_SHADERCACHEIMPL_INCLUDED

#include "../../../GlowEngine/Resource.h"
#include "../../../GlowEngine/ShaderCacheImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class ShaderCache
			: public ShaderCacheImpl
		{
		private:
			struct BinaryCodeCollection
			{
				String vertexShader, computeShader, geometryShader, pixelShader; // Hull and domain.
			};
		private:
			Map<Hash, BinaryCodeCollection> binaries;
			Package loadedCache;
			Mutex cacheMutex, binariesMutex, writeCacheMutex;

			struct ShaderCollInfo
			{
				ShaderImpl::CompilationData computeShader, vertexShader, geometryShader, pixelShader;
				Vector<String> computeShaderArgs, vertexShaderArgs, geometryShaderArgs, pixelShaderArgs;
				Map<Hash, Map<ShaderType, Hash>> variants;
			};

			Vector<ShaderCollInfo> parsedColls;
			Map<Hash, std::tuple<ShaderType, String>> shaderCodes;
			Map<Hash, Hash> specificToCollectiveMappings, specificToShaderCode;
			Map<Hash, SharedPtr<String>> collectiveToHeaders;
			Map<Hash, Vector<UInt32>> variants;
			Map<Hash, Pair<const ShaderCollInfo*, const Pair<const Hash, Map<ShaderType, Hash>>*>> allPossibleCollections;
			Map<Hash, Hash> collectionToFileName;
		public:
			// Must be called after the initialization of ResourceMgr.
			ShaderCache();
			virtual ~ShaderCache() = default;

			virtual void loadCollection(const GraphicsDeviceImpl& device, const Hash& hash, ShaderCollectionImpl& coll) override;
			virtual bool loadCache() override;
			virtual ThreadPool::TaskResult<bool> loadCacheAsync(ThreadPool& threadPool) override;

			virtual void createCache() const override;
			virtual ThreadPool::TaskResult<void> createCacheAsync(ThreadPool& threadPool) override;

			virtual bool checkIsLoaded() const override;

			virtual ThreadPool::TaskResult<bool> createAndLoadAsync(ThreadPool& threadPool) override;

			virtual Hash getCollectionFileHash(const Hash& collHash) const override;
		};
	}
}

#endif
