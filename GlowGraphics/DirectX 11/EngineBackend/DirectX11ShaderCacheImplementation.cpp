#include "DirectX11ShaderCacheImplementation.h"
#include "../GraphicsBackend/DirectX11ComputeShaderImplementation.h"
#include "../GraphicsBackend/DirectX11GeometryShaderImplementation.h"
#include "../GraphicsBackend/DirectX11GeometryShaderSOImplementation.h"
#include "../GraphicsBackend/DirectX11VertexShaderImplementation.h"
#include "../GraphicsBackend/DirectX11PixelShaderImplementation.h"
#include "../GraphicsBackend/DirectX11GraphicsDeviceImplementation.h"

#include "../../../GlowSystem/Filesystem.h"
#include "../../../GlowSystem/AsyncIO.h"

#include "../../Uniform/GSILShaderCompiler.h"
#include "../../Uniform/ShaderImplementation.h"

#include "../../../GlowEngine/Effect.h"
#include "../../../GlowEngine/Engine.h"

static constexpr const char cacheFilePath[] = u8"D3D11ShaderCache.cache", cacheInfoPath[] = u8"Cache.info";

void GLOWE::DirectX11::ShaderCache::createCache() const
{
	if (Filesystem::isExisting(cacheFilePath))
	{
		Filesystem::remove(cacheFilePath);
	}

	Package package;
	package.open(cacheFilePath, Package::OpenMode::Write);
	if (!package.checkIsOpen())
	{
		WarningThrow(false, "Unable to create a package for a shader cache.");
		return;
	}

	ResourceMgr& resMgr = getInstance<ResourceMgr>();
	const GraphicsDeviceImpl& device = static_cast<const DirectX11::GraphicsDevice&>(*resMgr.getDevice());

	const Vector<Pair<Hash, Hash>>& codes = resMgr.getSubresourcesOfType(ResourceMgr::SubresourceType::HLSLShaderCode);
	const Vector<Pair<Hash, Hash>>& colls = resMgr.getSubresourcesOfType(ResourceMgr::SubresourceType::ShaderCollection);

	Set<Hash> processedCollFiles, processedShaderFiles;

	Vector<ShaderCollInfo> localParsedColls;
	Map<Hash, std::tuple<ShaderType, String>> localShaderCodes;
	Map<Hash, Hash> localSpecificToCollectiveMappings, localSpecificToShaderCode;
	Map<Hash, SharedPtr<String>> localCollectiveToHeaders;
	Map<Hash, Vector<UInt32>> localVariants;

	// TODO
	// Hashe wariant�w postaci "grupa:technika:pass:wariant".
	for (const auto& x : colls)
	{
		if (processedCollFiles.count(x.first) == 0)
		{
			processedCollFiles.emplace(x.first);

			const ResourceHandle<String> fileContents = resMgr.load<String>(x.first, x.second);
			MemoryLoadSaveHandle handle(fileContents->getCharArray(), fileContents->getSize());

			UInt32 tempUInt32{};

			{
				DepthStencilDesc dssTemp;
				BlendDesc blendTemp;
				RasterizerDesc rastTemp;
				SamplerDesc sampTemp;

				handle >> tempUInt32;
				for (unsigned int y = 0; y < tempUInt32; ++y)
				{
					handle >> dssTemp;
				}

				handle >> tempUInt32;
				for (unsigned int y = 0; y < tempUInt32; ++y)
				{
					handle >> blendTemp;
				}

				handle >> tempUInt32;
				for (unsigned int y = 0; y < tempUInt32; ++y)
				{
					handle >> rastTemp;
				}

				handle >> tempUInt32;
				for (unsigned int y = 0; y < tempUInt32; ++y)
				{
					handle >> sampTemp;
				}
			}

			handle >> tempUInt32;
			for (unsigned int y = 0; y < tempUInt32; ++y)
			{
				UInt64 tempU64;
				handle >> tempU64;
				handle.skipBytes(tempU64 + sizeof(TextureDesc::Type));
			}

			handle >> tempUInt32;
			for (unsigned int y = 0; y < tempUInt32; ++y)
			{
				UInt64 tempU64;
				handle >> tempU64;
				handle.skipBytes(tempU64 + sizeof(GSILShader::UAV::Type));
			}

			{
				InputLayoutDesc inputLayoutTemp;
				handle >> tempUInt32;
				for (unsigned int y = 0; y < tempUInt32; ++y)
				{
					handle >> inputLayoutTemp;
				}
			}

			handle >> tempUInt32;
			for (unsigned int y = 0; y < tempUInt32; ++y)
			{
				Effect::CBuffer tempCBuff;
				handle.skipBytes(trueSizeof<Hash>());
				handle >> tempCBuff;
			}

			handle >> tempUInt32;
			for (unsigned int y = 0; y < tempUInt32; ++y)
			{
				UInt32 tempTechSize{};
				handle.skipBytes(trueSizeof<Hash>());
				handle >> tempTechSize;
				for (unsigned int z = 0; z < tempTechSize; ++z)
				{
					UInt32 tempPassSize{};
					handle.skipBytes(trueSizeof<Hash>());
					handle >> tempPassSize;
					handle.skipBytes(sizeof(Effect::Technique::Tag) * tempPassSize);
					handle >> tempPassSize;
					for (unsigned int w = 0; w < tempPassSize; ++w)
					{
						float tempVer{};
						UInt32 tempVarSize{};
						//ShaderImpl::CompilationData computeShaderCompData, vertexShaderCompData, geometryShaderCompData, pixelShaderCompData;
						localParsedColls.emplace_back();
						ShaderCollInfo& shaderCollInfo = localParsedColls.back();

						// Load the required data and skip the rest.
						handle.skipBytes(sizeof(UInt32) * 4);

						handle >> shaderCollInfo.computeShader.entrypoint >> tempVer;
						shaderCollInfo.computeShader.version = MathHelper::getShaderVersionFromFloat(tempVer);

						handle >> tempVarSize;
						for (unsigned int i = 0; i < tempVarSize; ++i)
						{
							shaderCollInfo.computeShaderArgs.emplace_back();
							handle >> shaderCollInfo.computeShaderArgs.back();
						}

						handle >> tempVarSize;
						handle.skipBytes(sizeof(UInt32) * tempVarSize);
						handle >> tempVarSize;
						handle.skipBytes(sizeof(UInt32) * tempVarSize);
						handle >> tempVarSize;
						handle.skipBytes(2 * sizeof(UInt32) * tempVarSize);

						handle >> shaderCollInfo.vertexShader.entrypoint >> tempVer;
						shaderCollInfo.vertexShader.version = MathHelper::getShaderVersionFromFloat(tempVer);

						handle >> tempVarSize;
						for (unsigned int i = 0; i < tempVarSize; ++i)
						{
							shaderCollInfo.vertexShaderArgs.emplace_back();
							handle >> shaderCollInfo.vertexShaderArgs.back();
						}

						handle >> tempVarSize;
						handle.skipBytes(sizeof(UInt32) * tempVarSize);
						handle >> tempVarSize;
						handle.skipBytes(sizeof(UInt32)* tempVarSize);
						handle >> tempVarSize;
						handle.skipBytes(2 * sizeof(UInt32) * tempVarSize);

						handle >> shaderCollInfo.geometryShader.entrypoint >> tempVer;
						shaderCollInfo.geometryShader.version = MathHelper::getShaderVersionFromFloat(tempVer);

						handle >> tempVarSize;
						for (unsigned int i = 0; i < tempVarSize; ++i)
						{
							shaderCollInfo.geometryShaderArgs.emplace_back();
							handle >> shaderCollInfo.geometryShaderArgs.back();
						}

						handle >> tempVarSize;
						handle.skipBytes(sizeof(UInt32) * tempVarSize);
						handle >> tempVarSize;
						handle.skipBytes(sizeof(UInt32)* tempVarSize);
						handle >> tempVarSize;
						handle.skipBytes(2 * sizeof(UInt32) * tempVarSize);

						handle >> shaderCollInfo.pixelShader.entrypoint >> tempVer;
						shaderCollInfo.pixelShader.version = MathHelper::getShaderVersionFromFloat(tempVer);

						handle >> tempVarSize;
						for (unsigned int i = 0; i < tempVarSize; ++i)
						{
							shaderCollInfo.pixelShaderArgs.emplace_back();
							handle >> shaderCollInfo.pixelShaderArgs.back();
						}

						handle >> tempVarSize;
						handle.skipBytes(sizeof(UInt32) * tempVarSize);
						handle >> tempVarSize;
						handle.skipBytes(sizeof(UInt32)* tempVarSize);
						handle >> tempVarSize;
						handle.skipBytes(2 * sizeof(UInt32) * tempVarSize);

						handle >> tempVarSize;
						for (unsigned int currVariant = 0; currVariant < tempVarSize; ++currVariant)
						{
							Hash variantHash, computeHash, vertexHash, geoHash, pixelHash;
							handle >> variantHash;
							handle.skipBytes(trueSizeof<Hash>()); // True variant hash - skip it.

							handle >> computeHash >> vertexHash >> geoHash >> pixelHash;

							auto& temp = shaderCollInfo.variants[variantHash];

							if (computeHash.isHashed())
							{
								temp.emplace(ShaderType::ComputeShader, computeHash);
							}

							if (vertexHash.isHashed())
							{
								temp.emplace(ShaderType::VertexShader, vertexHash);
							}

							if (geoHash.isHashed())
							{
								temp.emplace(ShaderType::GeometryShader, geoHash);
							}

							if (pixelHash.isHashed())
							{
								temp.emplace(ShaderType::PixelShader, pixelHash);
							}
						}
					}
				}
			}
		}
	}

	// TODO
	// Hashe wariant�w postaci "wariant". Naprawi�.
	for (const auto& x : codes)
	{
		// TODO
		// �ci�gawka z EffectCompiler.cpp linie 418 wzwy�.

		if (processedShaderFiles.count(x.first) == 0)
		{
			processedShaderFiles.emplace(x.first);

			const ResourceHandle<String> fileContents = resMgr.load<String>(x.first, x.second);
			MemoryLoadSaveHandle handle(fileContents->getCharArray(), fileContents->getSize());

			SharedPtr<String> hlslHeader = makeSharedPtr<String>();
			UInt32 techGroupSize{}, techSize{}, passSize{};

			handle >> *hlslHeader;
			handle >> techGroupSize;
			for (unsigned int i = 0; i < techGroupSize; ++i)
			{
				handle >> techSize;
				for (unsigned int y = 0; y < techSize; ++y)
				{
					handle >> passSize;
					for (unsigned int z = 0; z < passSize; ++z)
					{
						Hash computePackageHash, vertexPackageHash, geometryPackageHash, pixelPackageHash;
						UInt32 codesSize{}, varSize{};
						handle.skipBytes(trueSizeof<Hash>()); // So far we can skip this hash (I... think).
						handle >> codesSize;
						// TODO
						for (unsigned int w = 0; w < codesSize; ++w)
						{
							Hash tmpHash;
							ShaderType shaderType;
							String tempStr;

							handle >> tmpHash >> shaderType >> tempStr;

							localShaderCodes.emplace(std::piecewise_construct, std::forward_as_tuple(tmpHash), std::forward_as_tuple(shaderType, tempStr));

							switch (shaderType)
							{
							case ShaderType::ComputeShader:
								computePackageHash = tmpHash;
								break;
							case ShaderType::GeometryShader:
								geometryPackageHash = tmpHash;
								break;
							case ShaderType::VertexShader:
								vertexPackageHash = tmpHash;
								break;
							case ShaderType::PixelShader:
								pixelPackageHash = tmpHash;
								break;
							}
						}

						handle >> codesSize >> varSize;
						for (unsigned int w = 0; w < codesSize; ++w)
						{
							// TODO
							Hash varCollHash, compHash, vertHash, geoHash, pixelHash;
							handle >> varCollHash >> compHash >> vertHash >> geoHash >> pixelHash;
							localCollectiveToHeaders.emplace(varCollHash, hlslHeader);
							Vector<UInt32>& replacements = localVariants[varCollHash];
							if (compHash.isHashed())
							{
								localSpecificToCollectiveMappings.emplace(compHash, varCollHash);
								localSpecificToShaderCode.emplace(compHash, computePackageHash);
							}
							if (vertHash.isHashed())
							{
								localSpecificToCollectiveMappings.emplace(vertHash, varCollHash);
								localSpecificToShaderCode.emplace(vertHash, vertexPackageHash);
							}
							if (geoHash.isHashed())
							{
								localSpecificToCollectiveMappings.emplace(geoHash, varCollHash);
								localSpecificToShaderCode.emplace(geoHash, geometryPackageHash);
							}
							if (pixelHash.isHashed())
							{
								localSpecificToCollectiveMappings.emplace(pixelHash, varCollHash);
								localSpecificToShaderCode.emplace(pixelHash, pixelPackageHash);
							}
							replacements.resize(varSize);
							for (unsigned int j = 0; j < varSize; ++j)
							{
								handle >> replacements[j];
							}
						}
					}
				}
			}
		}
	}

	PackageLoadSaveHandle cacheBasicInfo(package, cacheInfoPath);

	cacheBasicInfo << static_cast<UInt32>(localParsedColls.size());
	for (const auto& x : localParsedColls)
	{
		cacheBasicInfo << x.computeShader.entrypoint << x.computeShader.version;
		cacheBasicInfo << static_cast<UInt32>(x.computeShaderArgs.size());
		for (const auto& str : x.computeShaderArgs)
		{
			cacheBasicInfo << str;
		}
		
		cacheBasicInfo << x.geometryShader.entrypoint << x.geometryShader.version;
		cacheBasicInfo << static_cast<UInt32>(x.geometryShaderArgs.size());
		for (const auto& str : x.geometryShaderArgs)
		{
			cacheBasicInfo << str;
		}

		cacheBasicInfo << x.vertexShader.entrypoint << x.vertexShader.version;
		cacheBasicInfo << static_cast<UInt32>(x.vertexShaderArgs.size());
		for (const auto& str : x.vertexShaderArgs)
		{
			cacheBasicInfo << str;
		}

		cacheBasicInfo << x.pixelShader.entrypoint << x.pixelShader.version;
		cacheBasicInfo << static_cast<UInt32>(x.pixelShaderArgs.size());
		for (const auto& str : x.pixelShaderArgs)
		{
			cacheBasicInfo << str;
		}

		cacheBasicInfo << static_cast<UInt32>(x.variants.size());
		for (const auto& y : x.variants)
		{
			cacheBasicInfo << y.first << static_cast<UInt32>(y.second.size());
			for (const auto& z : y.second)
			{
				cacheBasicInfo << z.first << z.second;
			}
		}
	}

	cacheBasicInfo << static_cast<UInt32>(localShaderCodes.size());
	for (const auto& x : localShaderCodes)
	{
		cacheBasicInfo << x.first << std::get<0>(x.second) << std::get<1>(x.second);
	}

	cacheBasicInfo << static_cast<UInt32>(localSpecificToCollectiveMappings.size());
	for (const auto& x : localSpecificToCollectiveMappings)
	{
		cacheBasicInfo << x.first << x.second;
	}

	cacheBasicInfo << static_cast<UInt32>(localSpecificToShaderCode.size());
	for (const auto& x : localSpecificToShaderCode)
	{
		cacheBasicInfo << x.first << x.second;
	}

	Set<SharedPtr<String>> headersToSave;
	for (const auto& x : localCollectiveToHeaders)
	{
		if (headersToSave.count(x.second) == 0)
		{
			headersToSave.emplace(x.second);
		}
	}

	cacheBasicInfo << static_cast<UInt32>(headersToSave.size());
	for (const auto& x : headersToSave)
	{
		cacheBasicInfo << x.get() << *x;
	}

	cacheBasicInfo << static_cast<UInt32>(localCollectiveToHeaders.size());
	for (const auto& x : localCollectiveToHeaders)
	{
		cacheBasicInfo << x.first << x.second.get();
	}

	cacheBasicInfo << static_cast<UInt32>(localVariants.size());
	for (const auto& x : localVariants)
	{
		cacheBasicInfo << x.first << static_cast<UInt32>(x.second.size());
		for (const auto& y : x.second)
		{
			cacheBasicInfo << y;
		}
	}

	cacheBasicInfo << static_cast<UInt32>(colls.size());
	for (const auto& x : colls)
	{
		cacheBasicInfo << x.second << x.first;
	}
}

GLOWE::ThreadPool::TaskResult<void> GLOWE::DirectX11::ShaderCache::createCacheAsync(ThreadPool& threadPool)
{
	return threadPool.addTask([this] { createCache(); });
}

bool GLOWE::DirectX11::ShaderCache::checkIsLoaded() const
{
	return loadedCache.checkIsOpen();
}

GLOWE::ThreadPool::TaskResult<bool> GLOWE::DirectX11::ShaderCache::createAndLoadAsync(ThreadPool& threadPool)
{
	return threadPool.addTask([this]() -> bool
	{
		createCache();
		return loadCache();
	});
}

GLOWE::Hash GLOWE::DirectX11::ShaderCache::getCollectionFileHash(const Hash& collHash) const
{
	return collectionToFileName.at(collHash);
}

GLOWE::DirectX11::ShaderCache::ShaderCache()
{
}

void GLOWE::DirectX11::ShaderCache::loadCollection(const GraphicsDeviceImpl& device, const Hash& hash, ShaderCollectionImpl& coll)
{
	DefaultUniqueLock binariesLock(binariesMutex);
	ShaderCollectionDesc collDesc;

	{
		const auto it = binaries.find(hash);
		if (it != binaries.end())
		{
			binariesLock.unlock();
			if (!it->second.computeShader.isEmpty())
			{
				const auto tempShared = makeSharedPtr<ComputeShader>();
				tempShared->fromByteCode(it->second.computeShader.getCharArray(), it->second.computeShader.getSize(), device);
				collDesc.computeShader = std::move(tempShared);
			}
			if (!it->second.geometryShader.isEmpty())
			{
				const auto tempShared = makeSharedPtr<GeometryShader>();
				tempShared->fromByteCode(it->second.geometryShader.getCharArray(), it->second.geometryShader.getSize(), device);
				collDesc.geometryShader = std::move(tempShared);
			}
			if (!it->second.vertexShader.isEmpty())
			{
				const auto tempShared = makeSharedPtr<VertexShader>();
				tempShared->fromByteCode(it->second.vertexShader.getCharArray(), it->second.vertexShader.getSize(), device);
				collDesc.vertexShader = std::move(tempShared);
			}
			if (!it->second.pixelShader.isEmpty())
			{
				const auto tempShared = makeSharedPtr<PixelShader>();
				tempShared->fromByteCode(it->second.pixelShader.getCharArray(), it->second.pixelShader.getSize(), device);
				collDesc.pixelShader = std::move(tempShared);
			}

			coll.create(collDesc);
			return;
		}
	}

	const String filename = hash.asNumberString();
	DefaultUniqueLock cacheLock(cacheMutex);
	if (loadedCache.checkFileForExistence(filename))
	{
		String buffer;
		loadedCache.readFile(filename, buffer);
		cacheLock.unlock();
		BinaryCodeCollection& binariesLocal = binaries.emplace(std::piecewise_construct, std::forward_as_tuple(hash), std::tuple<>()).first->second;
		//binariesLock.unlock();
		MemoryLoadSaveHandle handle(buffer.getCharArray(), buffer.getSize());

		UInt32 shadersCount{};
		handle >> shadersCount;
		for (unsigned int x = 0; x < shadersCount; ++x)
		{
			constexpr UInt32 sentinel = ~static_cast<UInt32>(0);
			UInt32 tempU32 = sentinel;
			UInt64 tempU64{};
			handle >> tempU32 >> tempU64;
			if (tempU32 != sentinel)
			{
				switch (static_cast<ShaderType>(tempU32))
				{
				case ShaderType::ComputeShader:
				{
					const auto tempShared = makeSharedPtr<ComputeShader>();
					tempShared->fromByteCode(handle.getReadPtr(), tempU64, device);
					collDesc.computeShader = std::move(tempShared);
					binariesLocal.computeShader = String(static_cast<const char*>(handle.getReadPtr()), tempU64);
					handle.skipBytes(tempU64);
				}
				break;
				case ShaderType::GeometryShader:
				{
					const auto tempShared = makeSharedPtr<GeometryShader>();
					tempShared->fromByteCode(handle.getReadPtr(), tempU64, device);
					collDesc.geometryShader = std::move(tempShared);
					binariesLocal.geometryShader = String(static_cast<const char*>(handle.getReadPtr()), tempU64);
					handle.skipBytes(tempU64);
				}
				break;
				case ShaderType::VertexShader:
				{
					const auto tempShared = makeSharedPtr<VertexShader>();
					tempShared->fromByteCode(handle.getReadPtr(), tempU64, device);
					collDesc.vertexShader = std::move(tempShared);
					binariesLocal.vertexShader = String(static_cast<const char*>(handle.getReadPtr()), tempU64);
					handle.skipBytes(tempU64);
				}
				break;
				case ShaderType::PixelShader:
				{
					const auto tempShared = makeSharedPtr<PixelShader>();
					tempShared->fromByteCode(handle.getReadPtr(), tempU64, device);
					collDesc.pixelShader = std::move(tempShared);
					binariesLocal.pixelShader = String(static_cast<const char*>(handle.getReadPtr()), tempU64);
					handle.skipBytes(tempU64);
				}
				break;
				}
			}
		}

		coll.create(collDesc);
	}
	else
	{
		// TODO: Compile the collection and emplace a task to put it into the cache.
		auto tempIt = allPossibleCollections.find(hash);
		if (tempIt == allPossibleCollections.end())
		{
			return;
		}

		const ShaderCollInfo& parsedColl = *tempIt->second.first;
		const auto& variant = *tempIt->second.second;

		ShaderCollectionDesc collDesc;
		StringLoadSaveHandle packCollHandle;
		static Regex argRegex(u8R"TXT(Uniform\s+\w+\s+(\w+))TXT");
		RegexMatch regMatch;
		const auto& variantGlobal = variants.at(variant.first);
		Vector<Pair<String, String>> localReplacements;
		localReplacements.reserve(variantGlobal.size());
		unsigned int currReplacement = 0;
		for (const auto& argument : parsedColl.vertexShaderArgs)
		{
			if (std::regex_search(argument.getString(), regMatch, argRegex))
			{
				localReplacements.emplace_back(std::piecewise_construct, std::forward_as_tuple(regMatch[1].first, regMatch[1].second), std::forward_as_tuple(toString(variantGlobal[currReplacement])));
				++currReplacement;
			}
		}

		packCollHandle << static_cast<UInt32>(variant.second.size());
		for (const auto& individualShader : variant.second)
		{
			constexpr UInt32 errorSentinel = ~static_cast<UInt32>(0);

			switch (individualShader.first)
			{
			case ShaderType::ComputeShader:
			{
				const Hash myCollectiveHash(specificToCollectiveMappings.at(individualShader.second));
				SharedPtr<ComputeShader> shader = makeSharedPtr<ComputeShader>();
				//const auto& tempCodeRef = shaderCodes.at(myCollectiveHash);
				const auto& tempCodeRef = shaderCodes.at(specificToShaderCode.at(individualShader.second));
				const String tempCode = *collectiveToHeaders.at(myCollectiveHash) + GSILShader::replaceHLSLForFuncName(std::get<1>(tempCodeRef), parsedColl.computeShader.entrypoint, localReplacements);

				shader->compileFromCode(tempCode, device, parsedColl.computeShader);
				ID3DBlob* const byteCode = shader->getBytecode();
				if (byteCode)
				{
					packCollHandle << static_cast<UInt32>(ShaderType::ComputeShader) << static_cast<UInt64>(byteCode->GetBufferSize());
					packCollHandle.write(byteCode->GetBufferPointer(), byteCode->GetBufferSize());
					collDesc.computeShader = shader;
				}
				else
				{
					packCollHandle << errorSentinel << static_cast<UInt64>(0);
					WarningThrow(true, u8"byteCode was null.");
				}
			}
			break;
			case ShaderType::VertexShader:
			{
				const Hash myCollectiveHash(specificToCollectiveMappings.at(individualShader.second));
				SharedPtr<VertexShader> shader = makeSharedPtr<VertexShader>();
				//const auto& tempCodeRef = shaderCodes.at(myCollectiveHash);
				const auto& tempCodeRef = shaderCodes.at(specificToShaderCode.at(individualShader.second));
				const String tempCode = *collectiveToHeaders.at(myCollectiveHash) + GSILShader::replaceHLSLForFuncName(std::get<1>(tempCodeRef), parsedColl.vertexShader.entrypoint, localReplacements);

				shader->compileFromCode(tempCode, device, parsedColl.vertexShader);
				ID3DBlob* const byteCode = shader->getBytecode();
				if (byteCode)
				{
					packCollHandle << static_cast<UInt32>(ShaderType::VertexShader) << static_cast<UInt64>(byteCode->GetBufferSize());
					packCollHandle.write(byteCode->GetBufferPointer(), byteCode->GetBufferSize());
					collDesc.vertexShader = shader;
				}
				else
				{
					packCollHandle << errorSentinel << static_cast<UInt64>(0);
					WarningThrow(true, u8"byteCode was null.");
				}
			}
			break;
			case ShaderType::GeometryShader:
			{
				const Hash myCollectiveHash(specificToCollectiveMappings.at(individualShader.second));
				SharedPtr<GeometryShader> shader = makeSharedPtr<GeometryShader>();
				//const auto& tempCodeRef = shaderCodes.at(myCollectiveHash);
				const auto& tempCodeRef = shaderCodes.at(specificToShaderCode.at(individualShader.second));
				const String tempCode = *collectiveToHeaders.at(myCollectiveHash) + GSILShader::replaceHLSLForFuncName(std::get<1>(tempCodeRef), parsedColl.geometryShader.entrypoint, localReplacements);

				shader->compileFromCode(tempCode, device, parsedColl.geometryShader);
				ID3DBlob* const byteCode = shader->getBytecode();
				if (byteCode)
				{
					packCollHandle << static_cast<UInt32>(ShaderType::GeometryShader) << static_cast<UInt64>(byteCode->GetBufferSize());
					packCollHandle.write(byteCode->GetBufferPointer(), byteCode->GetBufferSize());
					collDesc.geometryShader = shader;
				}
				else
				{
					packCollHandle << errorSentinel << static_cast<UInt64>(0);
					WarningThrow(true, u8"byteCode was null.");
				}
			}
			break;
			case ShaderType::PixelShader:
			{
				const Hash myCollectiveHash(specificToCollectiveMappings.at(individualShader.second));
				SharedPtr<PixelShader> shader = makeSharedPtr<PixelShader>();
				//const auto& tempCodeRef = shaderCodes.at(myCollectiveHash); // TODO: Exception.
				const auto& tempCodeRef = shaderCodes.at(specificToShaderCode.at(individualShader.second));
				const String tempCode = *collectiveToHeaders.at(myCollectiveHash) + GSILShader::replaceHLSLForFuncName(std::get<1>(tempCodeRef), parsedColl.pixelShader.entrypoint, localReplacements);

				shader->compileFromCode(tempCode, device, parsedColl.pixelShader);
				ID3DBlob* const byteCode = shader->getBytecode();
				if (byteCode)
				{
					packCollHandle << static_cast<UInt32>(ShaderType::PixelShader) << static_cast<UInt64>(byteCode->GetBufferSize());
					packCollHandle.write(byteCode->GetBufferPointer(), byteCode->GetBufferSize());
					collDesc.pixelShader = shader;
				}
				else
				{
					packCollHandle << errorSentinel << static_cast<UInt64>(0);
					WarningThrow(true, u8"byteCode was null.");
				}
			}
			break;
			}
		}

		coll.create(collDesc);

		SharedPtr<String> sharedString = makeSharedPtr<String>(packCollHandle.consumeString());
		getInstance<AsyncIOMgr>().writeToPackage(cacheFilePath, hash.asNumberString(), sharedString->getCharArray(), sharedString->getSize(), [sharedString](Package& package) { package.applyChanges(); });

		/*
		getInstance<Engine>().getEngineThreads().addTask([this, packCollHandle](const Hash hash)
		{
			// TODO
			Package pack;
			DefaultLockGuard lock(writeCacheMutex);
			const String& str = packCollHandle.getString();
			pack.open(cacheFilePath, Package::OpenMode::Write);
			if (pack.checkIsOpen())
			{
				PackageLoadSaveHandle handle(pack, hash.asNumberString());
				handle.write(str.getCharArray(), str.getSize());
				pack.close();
			}
		}, Hash(hash));
		*/
	}
}

bool GLOWE::DirectX11::ShaderCache::loadCache()
{
	if (checkIsLoaded())
	{
		return true;
	}

	if (!Filesystem::isExisting(cacheFilePath))
	{
		return false;
	}

	File cacheFile;
	cacheFile.open(cacheFilePath, std::ios::in | std::ios::binary);

	if (!cacheFile.checkIsOpen())
	{
		return false;
	}

	String temp;
	temp.resize(cacheFile.getFileSize());
	cacheFile.read(temp.getCharArray(), temp.getSize());
	cacheFile.close();
	loadedCache.openFromMemory(temp);

	if (!loadedCache.checkFileForExistence(cacheInfoPath))
	{
		loadedCache.close();
		return false;
	}

	loadedCache.readFile(cacheInfoPath, temp);

	MemoryLoadSaveHandle handle(temp.getCharArray(), temp.getSize());
	Map<String*, SharedPtr<String>> tempStringMappings;
	
	UInt32 tempU32{};
	handle >> tempU32;
	for (unsigned int x = 0; x < tempU32; ++x)
	{
		UInt32 tempArgsSize{};
		parsedColls.emplace_back();
		ShaderCollInfo& collInfo = parsedColls.back();

		handle >> collInfo.computeShader.entrypoint >> collInfo.computeShader.version;
		handle >> tempArgsSize;
		collInfo.computeShaderArgs.resize(tempArgsSize);
		for (unsigned int y = 0; y < tempArgsSize; ++y)
		{
			handle >> collInfo.computeShaderArgs[y];
		}

		handle >> collInfo.geometryShader.entrypoint >> collInfo.geometryShader.version;
		handle >> tempArgsSize;
		collInfo.geometryShaderArgs.resize(tempArgsSize);
		for (unsigned int y = 0; y < tempArgsSize; ++y)
		{
			handle >> collInfo.geometryShaderArgs[y];
		}

		handle >> collInfo.vertexShader.entrypoint >> collInfo.vertexShader.version;
		handle >> tempArgsSize;
		collInfo.vertexShaderArgs.resize(tempArgsSize);
		for (unsigned int y = 0; y < tempArgsSize; ++y)
		{
			handle >> collInfo.vertexShaderArgs[y];
		}

		handle >> collInfo.pixelShader.entrypoint >> collInfo.pixelShader.version;
		handle >> tempArgsSize;
		collInfo.pixelShaderArgs.resize(tempArgsSize);
		for (unsigned int y = 0; y < tempArgsSize; ++y)
		{
			handle >> collInfo.pixelShaderArgs[y];
		}

		handle >> tempArgsSize;
		for (unsigned int y = 0; y < tempArgsSize; ++y)
		{
			UInt32 tempReplacementSize{};
			Hash tmpHash;
			handle >> tmpHash >> tempReplacementSize;
			auto& tempMapRef = collInfo.variants.emplace(std::piecewise_construct, std::forward_as_tuple(tmpHash), std::tuple<>()).first->second;
			for (unsigned int z = 0; z < tempReplacementSize; ++z)
			{
				ShaderType tempShaderType;
				handle >> tempShaderType >> tmpHash;
				tempMapRef.emplace(tempShaderType, tmpHash);
			}
		}
	}

	handle >> tempU32;
	for (unsigned int x = 0; x < tempU32; ++x)
	{
		Hash tempHash;
		std::tuple<ShaderType, String> tempTuple;

		handle >> tempHash >> std::get<0>(tempTuple) >> std::get<1>(tempTuple);

		shaderCodes.emplace(tempHash, std::move(tempTuple));
	}

	handle >> tempU32;
	for (unsigned int x = 0; x < tempU32; ++x)
	{
		Hash tempHash1, tempHash2;
		handle >> tempHash1 >> tempHash2;
		specificToCollectiveMappings.emplace(tempHash1, tempHash2);
	}

	handle >> tempU32;
	for (unsigned int x = 0; x < tempU32; ++x)
	{
		Hash tempHash1, tempHash2;
		handle >> tempHash1 >> tempHash2;
		specificToShaderCode.emplace(tempHash1, tempHash2);
	}

	Map<String*, SharedPtr<String>> headerMappings;

	// Headers and mappings first.
	handle >> tempU32;
	for (unsigned int x = 0; x < tempU32; ++x)
	{
		String* tempStrId{};
		handle >> tempStrId; // It's supposed to load a pointer.
		auto& tempSharedPtr = headerMappings.emplace(tempStrId, makeSharedPtr<String>()).first->second;
		handle >> *tempSharedPtr;
	}

	handle >> tempU32;
	for (unsigned int x = 0; x < tempU32; ++x)
	{
		String* tempStrId{};
		Hash tempHash;
		handle >> tempHash >> tempStrId;
		collectiveToHeaders.emplace(tempHash, headerMappings.at(tempStrId));
	}

	handle >> tempU32;
	for (unsigned int x = 0; x < tempU32; ++x)
	{
		Hash tempHash;
		UInt32 tempReplacementsCount{};
		handle >> tempHash >> tempReplacementsCount;
		auto& tempVec = variants.emplace(std::piecewise_construct, std::forward_as_tuple(tempHash), std::forward_as_tuple(tempReplacementsCount)).first->second;
		for (unsigned int y = 0; y < tempReplacementsCount; ++y)
		{
			handle >> tempVec[y];
		}
	}

	handle >> tempU32;
	for (unsigned int x = 0; x < tempU32; ++x)
	{
		Hash resHash, fileHash;
		handle >> resHash >> fileHash;
		collectionToFileName.emplace(resHash, fileHash);
	}

	for (const auto& x : parsedColls)
	{
		for (const auto& y : x.variants)
		{
			allPossibleCollections.emplace(std::piecewise_construct, std::forward_as_tuple(y.first), std::forward_as_tuple(&x, &y));
		}
	}

	return true;
}

GLOWE::ThreadPool::TaskResult<bool> GLOWE::DirectX11::ShaderCache::loadCacheAsync(ThreadPool& threadPool)
{
	return threadPool.addTask([this] { return loadCache(); });
}
