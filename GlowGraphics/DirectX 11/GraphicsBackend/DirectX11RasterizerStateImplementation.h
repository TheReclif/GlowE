#pragma once
#ifndef GLOWE_DIRECTX_11_RASTERIZERSTATEIMPLEMENTATION_INCLUDED
#define GLOWE_DIRECTX_11_RASTERIZERSTATEIMPLEMENTATION_INCLUDED

#include "DirectX11Common.h"

#include "../../../GlowSystem/Utility.h"

#include "../../Uniform/RasterizerStateImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT RasterizerState
			: public RasterizerStateImpl
		{
		private:
			ComPtr<ID3D11RasterizerState> rastState;
		public:
			RasterizerState();
			RasterizerState(const RasterizerDesc& rastDesc, const GraphicsDeviceImpl& device);
			virtual ~RasterizerState();

			virtual void create(const RasterizerDesc& arg, const GraphicsDeviceImpl& device) override;
			virtual void destroy() override;

			virtual bool checkIsValid() const override;

			ID3D11RasterizerState* getRastState() const;
		};
	}
}

#endif
