#pragma once
#ifndef GLOWE_DIRECTX_11_RENDERTARGETIMPLEMENTATION_INCLUDED
#define GLOWE_DIRECTX_11_RENDERTARGETIMPLEMENTATION_INCLUDED

#include "../../Uniform/RenderTargetImplementation.h"

#include "DirectX11Common.h"

#include "DirectX11TextureImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT RenderTarget
			: public RenderTargetImpl
		{
		private:
			Vector<ComPtr<ID3D11RenderTargetView>> rtv;
			ComPtr<ID3D11DepthStencilView> dsv;
			Vector<SharedPtr<TextureImpl>> ownedTextures;
		public:
			RenderTarget() = default;
			virtual ~RenderTarget();

			virtual void create(const SharedPtr<TextureImpl>& texToUse, const SharedPtr<TextureImpl>& depthStencilTex, const GraphicsDeviceImpl& device) override;
			virtual void create(const Vector<SharedPtr<TextureImpl>>& textures, const SharedPtr<TextureImpl>& depthStencilTex, const GraphicsDeviceImpl& device) override;
			virtual void destroy() override;

			virtual bool checkIsValid() const override;

			void create(const Texture& depthStencilTex, ID3D11Texture2D* backbuffer, const GraphicsDeviceImpl& device);

			ID3D11RenderTargetView* getRTV(const unsigned int which = 0) const;
			ID3D11DepthStencilView* getDSV() const;
			
			const Vector<ComPtr<ID3D11RenderTargetView>>& getRTVs() const;
		};
	}
}

#endif
