#include "DirectX11BasicRendererImplementation.h"
#include "DirectX11GraphicsDeviceImplementation.h"

#include "DirectX11BlendStateImplementation.h"
#include "DirectX11BufferImplementation.h"
#include "DirectX11DepthStencilStateImplementation.h"
#include "DirectX11InputLayoutImplementation.h"
#include "DirectX11RasterizerStateImplementation.h"
#include "DirectX11RasterizerStateImplementation.h"
#include "DirectX11ShaderCollectionImplementation.h"
#include "DirectX11ShaderResourceViewImplementation.h"
#include "DirectX11RenderTargetImplementation.h"
#include "DirectX11SamplerImplementation.h"
#include "DirectX11UnorderedAccessViewImplementation.h"
#include "DirectX11TextureImplementation.h"

GLOWE::DirectX11::BasicRenderer::BasicRenderer(const GraphicsDeviceImpl& device)
	: BasicRendererImpl(), devCon(nullptr)
{
	create(device);
}

GLOWE::DirectX11::BasicRenderer::~BasicRenderer()
{
}

void GLOWE::DirectX11::BasicRenderer::create(const GraphicsDeviceImpl& device)
{
	destroy();

	ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();

	if (d3dDevice)
	{
		d3dDevice->GetImmediateContext(&devCon);
	}
}

void GLOWE::DirectX11::BasicRenderer::destroy()
{
	devCon = nullptr;
}

void GLOWE::DirectX11::BasicRenderer::customFunction(const std::function<void(BasicRendererImpl&)>& func)
{
	if (func)
	{
		func(*this);
	}
}

void GLOWE::DirectX11::BasicRenderer::setPrimitiveTopologyImpl(const PrimitiveTopology topology)
{
	auto translatePrimitiveType = [&topology]() -> D3D_PRIMITIVE_TOPOLOGY
	{
		switch (topology)
		{
		case PrimitiveTopology::LineList:
			return D3D_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_LINELIST;
			break;
		case PrimitiveTopology::LineListAdjacency:
			return D3D_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_LINELIST_ADJ;
			break;
		case PrimitiveTopology::LineStrip:
			return D3D_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP;
			break;
		case PrimitiveTopology::LineStripAdjacency:
			return D3D_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP_ADJ;
			break;
		case PrimitiveTopology::PointList:
			return D3D_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_POINTLIST;
			break;
		case PrimitiveTopology::TriangleList:
			return D3D_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
			break;
		case PrimitiveTopology::TriangleListAdjacency:
			return D3D_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST_ADJ;
			break;
		case PrimitiveTopology::TriangleStrip:
			return D3D_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
			break;
		case PrimitiveTopology::TriangleStripAdjacency:
			return D3D_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP_ADJ;
			break;
		default:
			return D3D_PRIMITIVE_TOPOLOGY::D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
			break;
		}
	};

	devCon->IASetPrimitiveTopology(translatePrimitiveType());
}

void GLOWE::DirectX11::BasicRenderer::setScissorRectsImpl(const Rect* const rects, const unsigned int size)
{
	if (size && rects)
	{
		Vector<D3D11_RECT> finalRects(size);

		for (unsigned int x = 0; x < size; ++x)
		{
			finalRects[x].left = rects[x].topLeft[0];
			finalRects[x].top = rects[x].topLeft[1];
			finalRects[x].right = rects[x].bottomRight[0];
			finalRects[x].bottom = rects[x].bottomRight[1];
		}

		devCon->RSSetScissorRects(size, finalRects.data());
	}
	else
	{
		devCon->RSSetScissorRects(0, nullptr);
	}
}

void GLOWE::DirectX11::BasicRenderer::setBlendState(const BlendStateImpl* state)
{
	if (state == currentBlendState)
	{
		return;
	}

	currentBlendState = state;
	if (state)
	{
		devCon->OMSetBlendState(static_cast<const BlendState*>(state)->getBlendState(), state->getDesc().blendFactor.rgba, state->getDesc().sampleMask);
	}
	else
	{
		devCon->OMSetBlendState(nullptr, nullptr, 0xffffffff);
	}
}

void GLOWE::DirectX11::BasicRenderer::setDepthStencilState(const DepthStencilStateImpl* dss)
{
	if (dss == currentDSState)
	{
		return;
	}

	currentDSState = dss;
	if (dss)
	{
		devCon->OMSetDepthStencilState(static_cast<const DepthStencilState*>(dss)->getDepthStencilState(), dss->getDesc().stencilRef);
	}
	else
	{
		devCon->OMSetDepthStencilState(nullptr, 0xffffffff);
	}
}

void GLOWE::DirectX11::BasicRenderer::setRasterizerState(const RasterizerStateImpl* rs)
{
	if (rs == currentRastState)
	{
		return;
	}

	currentRastState = rs;
	devCon->RSSetState(rs ? static_cast<const RasterizerState*>(rs)->getRastState() : nullptr);
}

void GLOWE::DirectX11::BasicRenderer::setInputLayout(const InputLayoutImpl* layout)
{
	if (layout == currentInputLayout)
	{
		return;
	}

	currentInputLayout = layout;
	devCon->IASetInputLayout(layout ? static_cast<const InputLayout*>(layout)->getInputLayout() : nullptr);
}

void GLOWE::DirectX11::BasicRenderer::copyBuffers(const BufferImpl* src, const BufferImpl* dst)
{
	if (!src || !dst || dst->getDesc().usage == Usage::Immutable || dst->getDesc().usage == Usage::Dynamic)
	{
		return;
	}

	devCon->CopyResource(static_cast<const Buffer*>(dst)->getBuffer(), static_cast<const Buffer*>(src)->getBuffer());
}

void GLOWE::DirectX11::BasicRenderer::updateBuffer(const BufferImpl* buffer, const void* data, const std::size_t dataSize, const std::size_t offset)
{
	if (!buffer || !data/* || (buffer->getDesc().cpuAccess != CPUAccess::ReadWrite && buffer->getDesc().cpuAccess != CPUAccess::Write)*/)
	{
		return;
	}

	D3D11_MAPPED_SUBRESOURCE temp;
	HRESULT hr = devCon->Map(static_cast<const Buffer*>(buffer)->getBuffer(), 0, D3D11_MAP_WRITE_DISCARD, 0, &temp);
	if (FAILED(hr))
	{
		return;
	}
	memcpy((char*)(temp.pData) + offset, data, dataSize);
	devCon->Unmap(static_cast<const Buffer*>(buffer)->getBuffer(), 0);
}

// Warning! Don't use!
void GLOWE::DirectX11::BasicRenderer::readBuffer(const BufferImpl* buffer, void* data, const std::size_t dataSize, const std::size_t offset)
{
	if (!buffer || !data/* || (buffer->getDesc().cpuAccess != CPUAccess::ReadWrite && buffer->getDesc().cpuAccess != CPUAccess::Read)*/)
	{
		return;
	}

	D3D11_MAPPED_SUBRESOURCE temp;
	HRESULT hr = devCon->Map(static_cast<const Buffer*>(buffer)->getBuffer(), 0, D3D11_MAP_READ, 0, &temp);
	if (FAILED(hr))
	{
		return;
	}
	memcpy(data, (char*)(temp.pData) + offset, dataSize);
	devCon->Unmap(static_cast<const Buffer*>(buffer)->getBuffer(), 0);
}

void GLOWE::DirectX11::BasicRenderer::copyTextures(const TextureImpl* src, const TextureImpl* dst)
{
	if (!src || !dst || dst->getDesc().usage == Usage::Immutable || dst->getDesc().usage == Usage::Dynamic)
	{
		return;
	}

	devCon->CopyResource(static_cast<const Texture*>(dst)->getTexture(), static_cast<const Texture*>(src)->getTexture());
}

void GLOWE::DirectX11::BasicRenderer::copyToTexture(const TextureImpl* src, const TextureImpl* dst, const UInt3& dstPos, const UInt3& srcPos, const UInt3& howMany, const unsigned int srcSubresourceId, const unsigned int dstSubresourceId)
{
	if (!src || !dst || dst->getDesc().usage == Usage::Immutable || dst->getDesc().usage == Usage::Dynamic)
	{
		return;
	}

	D3D11_BOX box;
	box.left = srcPos[0];
	box.top = srcPos[1];
	box.front = srcPos[2];
	box.right = box.left + howMany[0];
	box.bottom = box.top + howMany[1];
	box.back = box.front + howMany[2];
	devCon->CopySubresourceRegion(static_cast<const Texture*>(dst)->getTexture(), dstSubresourceId, dstPos[0], dstPos[1], dstPos[2], static_cast<const Texture*>(src)->getTexture(), srcSubresourceId, &box);
}

void GLOWE::DirectX11::BasicRenderer::updateTexture(const TextureImpl* src, const void* data, const UInt3& pos, const UInt3& howMany, const unsigned int dstSubresourceId)
{
	if (!src || !data)
	{
		return;
	}

	D3D11_BOX box;
	box.left = pos[0];
	box.right = pos[0] + howMany[0];
	box.top = pos[1];
	box.bottom = pos[1] + std::max(1U, howMany[1]);
	box.front = pos[2];
	box.back = pos[2] + std::max(1U, howMany[2]);

	//const unsigned int rowPitch = src->getDesc().format.getSize() * src->getDesc().width;
	//devCon->UpdateSubresource(src->getTexture(), dstSubresourceId, &box, data, rowPitch, rowPitch * src->getDesc().height);
	const unsigned int rowPitch = howMany[0] * src->getDesc().format.getSize();
	devCon->UpdateSubresource(static_cast<const Texture*>(src)->getTexture(), dstSubresourceId, &box, data, rowPitch, howMany[2] > 0 ? rowPitch * howMany[1] : 0);
}

void GLOWE::DirectX11::BasicRenderer::readTexture(const TextureImpl* src, void* data, const UInt3& pos, const UInt3& howMany, const unsigned int srcSubresourceId)
{
	if (!src || !data)
	{
		return;
	}

	ID3D11Resource* const res = static_cast<const Texture*>(src)->getTexture();
	D3D11_MAPPED_SUBRESOURCE mappedSubres;
	ZeroMemory(&mappedSubres, sizeof(mappedSubres));
	HRESULT hr = devCon->Map(res, srcSubresourceId, D3D11_MAP::D3D11_MAP_READ, 0, &mappedSubres);
	if (FAILED(hr))
	{
		WarningThrow(false, "Unable to map a texture.");
		return;
	}

	const UInt32 formatSize = src->getDesc().format.getSize(), toReadSize = howMany[0] * formatSize;
	const UInt64 offset = pos[0] * src->getDesc().format.getSize() + pos[1] * mappedSubres.RowPitch + pos[2] * mappedSubres.DepthPitch;

	for (unsigned int y = 0; y < std::max(howMany[1], 1U); ++y)
	{
		for (unsigned int z = 0; z < std::max(howMany[2], 1U); ++z)
		{
			std::memcpy(static_cast<char*>(data) + y * mappedSubres.RowPitch + z * mappedSubres.DepthPitch, static_cast<char*>(mappedSubres.pData) + offset + y * mappedSubres.RowPitch + z * mappedSubres.DepthPitch, toReadSize);
		}
	}

	devCon->Unmap(res, srcSubresourceId);
}

void GLOWE::DirectX11::BasicRenderer::setVertexBuffers(const BufferImpl** buffers, const unsigned int size)
{
	const unsigned int buffSize = size;
	if (buffSize == vertexBuffersCount)
	{
		bool flag = false;
		for (unsigned int x = 0; x < buffSize; ++x)
		{
			if (buffers[x] != currentVertexBuffers[x])
			{
				flag = true;
				break;
			}
		}
		if (!flag)
		{
			return;
		}
	}

	vertexBuffersCount = buffSize;

	if (size && buffers)
	{
		Vector<ID3D11Buffer*> buffs(size);
		Vector<UINT> strides(size), offsets(size);

		for (unsigned int x = 0; x < size; ++x)
		{
			buffs[x] = static_cast<const Buffer*>(buffers[x])->getBuffer();
			strides[x] = buffers[x]->getDesc().layout.getSize();
		}

		devCon->IASetVertexBuffers(0, size, buffs.data(), strides.data(), offsets.data());

		for (unsigned int x = 0; x < buffSize; ++x)
		{
			currentVertexBuffers[x] = buffers[x];
		}
	}
	else
	{
		devCon->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	}
}

void GLOWE::DirectX11::BasicRenderer::setVertexBuffer(const BufferImpl* buffer)
{
	if (buffer == currentVertexBuffers[0] && (vertexBuffersCount == (buffer ? 1 : 0)))
	{
		return;
	}

	if (buffer)
	{
		if (buffer->getDesc().binds == BufferDesc::Binding::VertexBuffer)
		{
			UINT strides = buffer->getDesc().layout.getSize(), offsets = 0;
			ID3D11Buffer* temp = static_cast<const Buffer*>(buffer)->getBuffer();
			devCon->IASetVertexBuffers(0, 1, &temp, &strides, &offsets);
			vertexBuffersCount = 1;
			currentVertexBuffers[0] = buffer;
		}
	}
	else
	{
		devCon->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
		vertexBuffersCount = 0;
		currentVertexBuffers[0] = nullptr;
	}
}

void GLOWE::DirectX11::BasicRenderer::setVertexBuffers(const Vector<const BufferImpl*>& buffers)
{
	const unsigned int buffSize = buffers.size();
	if (buffSize == vertexBuffersCount)
	{
		bool flag = false;
		for (unsigned int x = 0; x < buffSize; ++x)
		{
			if (buffers[x] != currentVertexBuffers[x])
			{
				flag = true;
				break;
			}
		}
		if (!flag)
		{
			return;
		}
	}

	vertexBuffersCount = buffSize;
	if (buffSize)
	{
		Vector<ID3D11Buffer*> buffs(buffers.size());
		Vector<UINT> strides(buffers.size()), offsets(buffers.size());

		for (unsigned int x = 0; x < buffers.size(); ++x)
		{
			buffs[x] = static_cast<const Buffer*>(buffers[x])->getBuffer();
			strides[x] = buffers[x]->getDesc().layout.getSize();
		}

		devCon->IASetVertexBuffers(0, buffers.size(), buffs.data(), strides.data(), offsets.data());

		for (unsigned int x = 0; x < buffSize; ++x)
		{
			currentVertexBuffers[x] = buffers[x];
		}
	}
	else
	{
		devCon->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	}
}

void GLOWE::DirectX11::BasicRenderer::setIndexBuffer(const BufferImpl* buffer)
{
	if (buffer == currentIndexBuffer)
	{
		return;
	}

	currentIndexBuffer = buffer;
	if (buffer)
	{
		if (buffer->getDesc().binds == BufferDesc::Binding::IndexBuffer)
		{
			devCon->IASetIndexBuffer(static_cast<const Buffer*>(buffer)->getBuffer(), getDXFormat(buffer->getDesc().layout.getElem(0)), 0);
		}
	}
	else
	{
		devCon->IASetIndexBuffer(nullptr, DXGI_FORMAT::DXGI_FORMAT_UNKNOWN, 0);
	}
}

void GLOWE::DirectX11::BasicRenderer::setShaderCollection(const ShaderCollectionImpl* coll)
{
	if (coll && coll->checkIsValid())
	{
		const auto vertexShader = static_cast<const ShaderCollection*>(coll)->getVertexShader();
		if (vertexShader != currentVertexShader)
		{
			devCon->VSSetShader(vertexShader, nullptr, 0);
			currentVertexShader = vertexShader;
		}

		const auto pixelShader = static_cast<const ShaderCollection*>(coll)->getPixelShader();
		if (pixelShader != currentPixelShader)
		{
			devCon->PSSetShader(pixelShader, nullptr, 0);
			currentPixelShader = pixelShader;
		}

		const auto geometryShader = static_cast<const ShaderCollection*>(coll)->getGeometryShader();
		if (geometryShader != currentGeometryShader)
		{
			devCon->GSSetShader(geometryShader, nullptr, 0);
			currentGeometryShader = geometryShader;
		}

		const auto computeShader = static_cast<const ShaderCollection*>(coll)->getComputeShader();
		if (computeShader != currentComputeShader)
		{
			devCon->CSSetShader(computeShader, nullptr, 0);
			currentComputeShader = computeShader;
		}

		const auto hullShader = static_cast<const ShaderCollection*>(coll)->getHullShader();
		if (hullShader != currentHullShader)
		{
			devCon->HSSetShader(hullShader, nullptr, 0);
			currentHullShader = hullShader;
		}

		const auto domainShader = static_cast<const ShaderCollection*>(coll)->getDomainShader();
		if (domainShader != currentDomainShader)
		{
			devCon->DSSetShader(domainShader, nullptr, 0);
			currentDomainShader = domainShader;
		}
	}
}

void GLOWE::DirectX11::BasicRenderer::drawImpl(const unsigned int verticesCount, const unsigned int startingVertex)
{
	devCon->Draw(verticesCount, startingVertex);
}

void GLOWE::DirectX11::BasicRenderer::drawIndexedImpl(const unsigned int indicesCount, const unsigned int startingIndex, const unsigned int startingVertex)
{
	devCon->DrawIndexed(indicesCount, startingIndex, startingVertex);
}

void GLOWE::DirectX11::BasicRenderer::drawInstancedImpl(const unsigned int verticesPerInstance, const unsigned int instancesCount, const unsigned int startingVertex, const unsigned int startingInstanceLoc)
{
	devCon->DrawInstanced(verticesPerInstance, instancesCount, startingVertex, startingInstanceLoc);
}

void GLOWE::DirectX11::BasicRenderer::drawIndexedInstancedImpl(const unsigned int indicesPerInstance, const unsigned int instancesCount, const int baseVertexLocation, const unsigned int startingVertex, const unsigned int startingInstanceLoc)
{
	devCon->DrawIndexedInstanced(indicesPerInstance, instancesCount, startingVertex, baseVertexLocation, startingInstanceLoc);
}

void GLOWE::DirectX11::BasicRenderer::setSRV(const ShaderResourceViewImpl* srv, const unsigned int texUnit, const ShaderType stage)
{
	ID3D11ShaderResourceView* d3dSRV = srv ? static_cast<const ShaderResourceView*>(srv)->getSRV() : nullptr;
	UINT tempNum = srv ? 1 : 0;

	switch (stage)
	{
	case ShaderType::ComputeShader:
		devCon->CSSetShaderResources(texUnit, tempNum, &d3dSRV);
		break;
	case ShaderType::DomainShader:
		devCon->DSSetShaderResources(texUnit, tempNum, &d3dSRV);
		break;
	case ShaderType::GeometryShader:
		devCon->GSSetShaderResources(texUnit, tempNum, &d3dSRV);
		break;
	case ShaderType::HullShader:
		devCon->HSSetShaderResources(texUnit, tempNum, &d3dSRV);
		break;
	case ShaderType::PixelShader:
		devCon->PSSetShaderResources(texUnit, tempNum, &d3dSRV);
		break;
	case ShaderType::VertexShader:
		devCon->VSSetShaderResources(texUnit, tempNum, &d3dSRV);
		break;
	}
}

void GLOWE::DirectX11::BasicRenderer::setSRV(const ShaderResourceViewImpl** srvArr, const unsigned int firstTexUnit, const unsigned int arraySize, const ShaderType stage)
{
	Vector<ID3D11ShaderResourceView*> d3dSRVs(arraySize);

	for (unsigned int x = 0; x < arraySize; ++x)
	{
		if (srvArr[x])
		{
			d3dSRVs[x] = static_cast<const ShaderResourceView*>(srvArr[x])->getSRV();
		}
	}

	switch (stage)
	{
	case ShaderType::ComputeShader:
		devCon->CSSetShaderResources(firstTexUnit, arraySize, d3dSRVs.data());
		break;
	case ShaderType::DomainShader:
		devCon->DSSetShaderResources(firstTexUnit, arraySize, d3dSRVs.data());
		break;
	case ShaderType::GeometryShader:
		devCon->GSSetShaderResources(firstTexUnit, arraySize, d3dSRVs.data());
		break;
	case ShaderType::HullShader:
		devCon->HSSetShaderResources(firstTexUnit, arraySize, d3dSRVs.data());
		break;
	case ShaderType::PixelShader:
		devCon->PSSetShaderResources(firstTexUnit, arraySize, d3dSRVs.data());
		break;
	case ShaderType::VertexShader:
		devCon->VSSetShaderResources(firstTexUnit, arraySize, d3dSRVs.data());
		break;
	}
}

void GLOWE::DirectX11::BasicRenderer::setSamplers(const SamplerImpl** arr, const unsigned int startSlot, const unsigned int arraySize, const ShaderType stage)
{
	Vector<ID3D11SamplerState*> samplers(arraySize);

	for (unsigned int x = 0; x < arraySize; ++x)
	{
		if (arr[x])
		{
			samplers[x] = static_cast<const Sampler*>(arr[x])->getSampler();
		}
	}

	switch (stage)
	{
	case ShaderType::ComputeShader:
		devCon->CSSetSamplers(startSlot, arraySize, samplers.data());
		break;
	case ShaderType::DomainShader:
		devCon->DSSetSamplers(startSlot, arraySize, samplers.data());
		break;
	case ShaderType::GeometryShader:
		devCon->GSSetSamplers(startSlot, arraySize, samplers.data());
		break;
	case ShaderType::HullShader:
		devCon->HSSetSamplers(startSlot, arraySize, samplers.data());
		break;
	case ShaderType::PixelShader:
		devCon->PSSetSamplers(startSlot, arraySize, samplers.data());
		break;
	case ShaderType::VertexShader:
		devCon->VSSetSamplers(startSlot, arraySize, samplers.data());
		break;
	}
}

void GLOWE::DirectX11::BasicRenderer::setConstantBuffers(const BufferImpl* cb, const unsigned int buffSlot, const ShaderType stage)
{
	ID3D11Buffer* temp = cb ? static_cast<const Buffer*>(cb)->getBuffer() : nullptr;
	UINT size = temp ? 1 : 0;

	switch (stage)
	{
	case ShaderType::ComputeShader:
		devCon->CSSetConstantBuffers(buffSlot, size, &temp);
		break;
	case ShaderType::DomainShader:
		devCon->DSSetConstantBuffers(buffSlot, size, &temp);
		break;
	case ShaderType::GeometryShader:
		devCon->GSSetConstantBuffers(buffSlot, size, &temp);
		break;
	case ShaderType::HullShader:
		devCon->HSSetConstantBuffers(buffSlot, size, &temp);
		break;
	case ShaderType::PixelShader:
		devCon->PSSetConstantBuffers(buffSlot, size, &temp);
		break;
	case ShaderType::VertexShader:
		devCon->VSSetConstantBuffers(buffSlot, size, &temp);
		break;
	}
}

void GLOWE::DirectX11::BasicRenderer::setConstantBuffers(const BufferImpl** cb, const unsigned int buffSlot, const unsigned int arraySize, const ShaderType stage)
{
	Vector<ID3D11Buffer*> temp(arraySize);

	for (unsigned int x = 0; x < arraySize; ++x)
	{
		if (cb[x])
		{
			temp[x] = static_cast<const Buffer*>(cb[x])->getBuffer();
		}
	}

	switch (stage)
	{
	case ShaderType::ComputeShader:
		devCon->CSSetConstantBuffers(buffSlot, arraySize, temp.data());
		break;
	case ShaderType::DomainShader:
		devCon->DSSetConstantBuffers(buffSlot, arraySize, temp.data());
		break;
	case ShaderType::GeometryShader:
		devCon->GSSetConstantBuffers(buffSlot, arraySize, temp.data());
		break;
	case ShaderType::HullShader:
		devCon->HSSetConstantBuffers(buffSlot, arraySize, temp.data());
		break;
	case ShaderType::PixelShader:
		devCon->PSSetConstantBuffers(buffSlot, arraySize, temp.data());
		break;
	case ShaderType::VertexShader:
		devCon->VSSetConstantBuffers(buffSlot, arraySize, temp.data());
		break;
	}
}

void GLOWE::DirectX11::BasicRenderer::setRenderTarget(const RenderTargetImpl* rt)
{
	if (rt)
	{
		const auto& rtvs = static_cast<const RenderTarget*>(rt)->getRTVs();
		RTsCount = rtvs.size();
		if (RTsCount == 0)
		{
			devCon->OMSetRenderTargets(0, nullptr, nullptr);
		}
		else
		{
			devCon->OMSetRenderTargets(RTsCount, reinterpret_cast<ID3D11RenderTargetView* const *>(rtvs.data()), static_cast<const RenderTarget*>(rt)->getDSV());
		}
	}
	else
	{
		RTsCount = 0;
		devCon->OMSetRenderTargets(0, nullptr, nullptr);
	}
}

void GLOWE::DirectX11::BasicRenderer::setRenderTargetAndUAVs(const RenderTargetImpl* rt, const Vector<const UnorderedAccessViewImpl*>& uavs, const unsigned int uavStartSlot)
{
	ID3D11RenderTargetView* const* rtvs{};
	ID3D11DepthStencilView* dsv{};
	unsigned int renderTargetSizes{};
	auto uavsCount = uavs.size();
	if (uavsCount > 64)
	{
		uavsCount = 64;
	}
	if (rt)
	{
		const auto& tempVec = static_cast<const RenderTarget*>(rt)->getRTVs();
		rtvs = reinterpret_cast<ID3D11RenderTargetView* const*>(tempVec.data());
		dsv = static_cast<const RenderTarget*>(rt)->getDSV();
		renderTargetSizes = tempVec.size();
	}
	else
	{
		rtvs = nullptr;
		dsv = nullptr;
		renderTargetSizes = 0;
	}

	ID3D11UnorderedAccessView* views[64];

	for (unsigned int x = 0; x < uavsCount; ++x)
	{
		views[x] = static_cast<const UnorderedAccessView*>(uavs[x])->getUAV();
	}

	RTsCount = renderTargetSizes;
	devCon->OMSetRenderTargetsAndUnorderedAccessViews(renderTargetSizes, rtvs, dsv, uavStartSlot == SetUAVsForPixelShader ? renderTargetSizes : uavStartSlot, uavsCount, views, nullptr);
}

void GLOWE::DirectX11::BasicRenderer::setRenderTargetAndUAVs(const RenderTargetImpl* rt, const UnorderedAccessViewImpl* const * uavs, const unsigned int arraySize, const unsigned int uavStartSlot)
{
	ID3D11RenderTargetView* const* rtvs{};
	ID3D11DepthStencilView* dsv{};
	unsigned int renderTargetSizes{};
	const unsigned int finalArraySize = arraySize > 64 ? 64 : arraySize;
	if (rt)
	{
		const auto& tempVec = static_cast<const RenderTarget*>(rt)->getRTVs();
		rtvs = reinterpret_cast<ID3D11RenderTargetView* const*>(tempVec.data());
		dsv = static_cast<const RenderTarget*>(rt)->getDSV();
		renderTargetSizes = tempVec.size();
	}
	else
	{
		rtvs = nullptr;
		dsv = nullptr;
		renderTargetSizes = 0;
	}

	ID3D11UnorderedAccessView* views[64];

	for (unsigned int x = 0; x < finalArraySize; ++x)
	{
		views[x] = static_cast<const UnorderedAccessView*>(uavs[x])->getUAV();
	}

	RTsCount = renderTargetSizes;
	devCon->OMSetRenderTargetsAndUnorderedAccessViews(renderTargetSizes, rtvs, dsv, uavStartSlot == SetUAVsForPixelShader ? renderTargetSizes : uavStartSlot, finalArraySize, views, nullptr);
}

void GLOWE::DirectX11::BasicRenderer::setUAVs(const UnorderedAccessViewImpl* const* uavs, const unsigned int arraySize, const unsigned int uavStartSlot)
{
	ID3D11UnorderedAccessView* views[64];
	const unsigned int finalArraySize = arraySize > 64 ? 64 : arraySize;

	for (unsigned int x = 0; x < finalArraySize; ++x)
	{
		views[x] = static_cast<const UnorderedAccessView*>(uavs[x])->getUAV();
	}

	devCon->OMSetRenderTargetsAndUnorderedAccessViews(D3D11_KEEP_RENDER_TARGETS_AND_DEPTH_STENCIL, nullptr, nullptr, uavStartSlot == SetUAVsForPixelShader ? RTsCount : uavStartSlot, finalArraySize, views, nullptr);
}

void GLOWE::DirectX11::BasicRenderer::setViewport(const Viewport & vp)
{
	D3D11_VIEWPORT temp;
	temp.Height = vp.height;
	temp.MaxDepth = vp.maxDepth;
	temp.MinDepth = vp.minDepth;
	temp.TopLeftX = vp.topX;
	temp.TopLeftY = vp.topY;
	temp.Width = vp.width;

	devCon->RSSetViewports(1, &temp);
}

void GLOWE::DirectX11::BasicRenderer::setViewports(const Vector<Viewport>& vpsArray)
{
	const unsigned int tempSize = vpsArray.size();
	Vector<D3D11_VIEWPORT> temp(tempSize);

	for (unsigned int x = 0; x < tempSize; ++x)
	{
		temp[x].Height = vpsArray[x].height;
		temp[x].MaxDepth = vpsArray[x].maxDepth;
		temp[x].MinDepth = vpsArray[x].minDepth;
		temp[x].TopLeftX = vpsArray[x].topX;
		temp[x].TopLeftY = vpsArray[x].topY;
		temp[x].Width = vpsArray[x].width;
	}

	devCon->RSSetViewports(tempSize, temp.data());
}

void GLOWE::DirectX11::BasicRenderer::setViewports(const Viewport* vpsArray, const unsigned int size)
{
	if (!vpsArray)
	{
		return;
	}

	Vector<D3D11_VIEWPORT> temp(size);

	for (unsigned int x = 0; x < size; ++x)
	{
		temp[x].Height = vpsArray[x].height;
		temp[x].MaxDepth = vpsArray[x].maxDepth;
		temp[x].MinDepth = vpsArray[x].minDepth;
		temp[x].TopLeftX = vpsArray[x].topX;
		temp[x].TopLeftY = vpsArray[x].topY;
		temp[x].Width = vpsArray[x].width;
	}

	devCon->RSSetViewports(size, temp.data());
}

void GLOWE::DirectX11::BasicRenderer::clearColor(const RenderTargetImpl* rt, const Color & refreshCol)
{
	ID3D11RenderTargetView* rtv = rt ? static_cast<const RenderTarget*>(rt)->getRTV() : nullptr;
	if (rtv)
	{
		devCon->ClearRenderTargetView(rtv, refreshCol.rgba);
	}
}

void GLOWE::DirectX11::BasicRenderer::clearDepthStencil(const RenderTargetImpl* rt, const float refreshDepth, const UInt8 refreshStencil)
{
	ID3D11DepthStencilView* dsv = rt ? static_cast<const RenderTarget*>(rt)->getDSV() : nullptr;
	if (dsv)
	{
		devCon->ClearDepthStencilView(dsv, D3D11_CLEAR_FLAG::D3D11_CLEAR_DEPTH | D3D11_CLEAR_FLAG::D3D11_CLEAR_STENCIL, refreshDepth, refreshStencil);
	}
}

void GLOWE::DirectX11::BasicRenderer::dispatchImpl(const unsigned int x, const unsigned int y, const unsigned int z)
{
	devCon->Dispatch(x, y, z);
}

void GLOWE::DirectX11::BasicRenderer::generateMipmaps(const ShaderResourceViewImpl* tex)
{
	if (tex && tex->checkIsValid())
	{
		devCon->GenerateMips(static_cast<const ShaderResourceView*>(tex)->getSRV());
	}
}

ID3D11DeviceContext * GLOWE::DirectX11::BasicRenderer::getContext() const
{
	return devCon;
}
