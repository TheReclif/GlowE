#pragma once
#ifndef GLOWE_DIRECTX_11_VERTEXSHADERIMPLEMENTATION_INCLUDED
#define GLOWE_DIRECTX_11_VERTEXSHADERIMPLEMENTATION_INCLUDED

#include "DirectX11Common.h"

#include "../../Uniform/ShaderImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT VertexShader
			: public VertexShaderImpl
		{
		private:
			ComPtr<ID3D11VertexShader> shader;
			ComPtr<ID3DBlob> byteCode;
		public:
			VertexShader();
			virtual ~VertexShader();

			virtual void compileFromCode(const String& code, const GraphicsDeviceImpl& device, const CompilationData& compilationData = CompilationData()) override;
			virtual void destroy() override;

			virtual bool checkIsCreated() const override;

			ID3DBlob* getBytecode() const;
			ID3D11VertexShader* getShader() const;

			void fromByteCode(const void* bytes, const UInt32 size, const GraphicsDeviceImpl& device);
		};
	}
}

#endif
