#include "DirectX11RenderTargetImplementation.h"
#include "DirectX11GraphicsDeviceImplementation.h"

GLOWE::DirectX11::RenderTarget::~RenderTarget()
{
	destroy();
}

void GLOWE::DirectX11::RenderTarget::create(const SharedPtr<TextureImpl>& texToUse, const SharedPtr<TextureImpl>& depthStencilTex, const GraphicsDeviceImpl& device)
{
	ID3D11Resource* d3dTex = texToUse ? dynamic_cast<const Texture&>(*texToUse).getTexture() : nullptr;
	ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();

	if (!d3dDevice)
	{
		return;
	}

	destroy();

	using Type = TextureDesc::Type;

	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	TextureDesc texDesc;
	if (d3dTex)
	{
		texDesc = texToUse->getDesc();

		if (texDesc.binding != TextureDesc::Binding::RenderTarget)
		{
			WarningThrow(false, String(u8"Unable to create RenderTarget. Invalid binding."));
			return;
		}

		rtvDesc.Format = getDXFormat(texDesc.format);

		switch (texDesc.type)
		{
		case Type::Texture1D:
			rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE1D;
			rtvDesc.Texture1D.MipSlice = 0;
			break;
		case Type::Texture1DArray:
			rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE1DARRAY;
			rtvDesc.Texture1DArray.ArraySize = texDesc.arraySize;
			rtvDesc.Texture1DArray.FirstArraySlice = 0;
			rtvDesc.Texture1DArray.MipSlice = 0;
			break;
		case Type::Texture2D:
			if (texDesc.multisamplingLevel == 0)
			{
				rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2D;
				rtvDesc.Texture2D.MipSlice = 0;
			}
			else
			{
				rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2DMS;
				rtvDesc.Texture2DMS.UnusedField_NothingToDefine = 0;
			}
			break;
		case Type::TextureCube:
		case Type::Texture2DArray:
			if (texDesc.multisamplingLevel == 0)
			{
				rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
				rtvDesc.Texture2DArray.ArraySize = texDesc.arraySize;
				rtvDesc.Texture2DArray.FirstArraySlice = 0;
				rtvDesc.Texture2DArray.MipSlice = 0;
			}
			else
			{
				rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2DMSARRAY;
				rtvDesc.Texture2DMSArray.ArraySize = texDesc.arraySize;
				rtvDesc.Texture2DMSArray.FirstArraySlice = 0;
			}
			break;
		case Type::Texture3D:
			rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE3D;
			rtvDesc.Texture3D.MipSlice = 0;
			rtvDesc.Texture3D.FirstWSlice = 0;
			rtvDesc.Texture3D.WSize = -1;
			break;
		}

		ComPtr<ID3D11RenderTargetView> tempRtv = nullptr;

		HRESULT hr = d3dDevice->CreateRenderTargetView(d3dTex, &rtvDesc, &tempRtv);
		if (FAILED(hr))
		{
			ErrorThrow(true);
			return;
		}

		rtv.push_back(std::move(tempRtv));

		rtvSize = Float2{ static_cast<float>(texDesc.width), static_cast<float>(texDesc.height) };
	}

	d3dTex = depthStencilTex ? dynamic_cast<const Texture&>(*depthStencilTex).getTexture() : nullptr;

	if (d3dTex)
	{
		texDesc = depthStencilTex->getDesc();
		if (texDesc.binding != TextureDesc::Binding::DepthStencil)
		{
			WarningThrow(false, String(u8"Unable to create DepthStencil for RenderTarget. Invalid binding."));
			return;
		}

		D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
		dsvDesc.Format = getDXFormat(texDesc.format);
		dsvDesc.Flags = 0;

		switch (texDesc.type)
		{
		case Type::Texture1D:
			dsvDesc.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE1D;
			dsvDesc.Texture1D.MipSlice = 0;
			break;
		case Type::Texture1DArray:
			dsvDesc.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE1DARRAY;
			dsvDesc.Texture1DArray.ArraySize = texDesc.arraySize;
			dsvDesc.Texture1DArray.FirstArraySlice = 0;
			dsvDesc.Texture1DArray.MipSlice = 0;
			break;
		case Type::Texture2D:
			if (texDesc.multisamplingLevel == 0)
			{
				dsvDesc.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE2D;
				dsvDesc.Texture2D.MipSlice = 0;
			}
			else
			{
				dsvDesc.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE2DMS;
				dsvDesc.Texture2DMS.UnusedField_NothingToDefine = 0;
			}
			break;
		case Type::Texture2DArray:
			if (texDesc.multisamplingLevel == 0)
			{
				dsvDesc.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
				dsvDesc.Texture2DArray.ArraySize = texDesc.arraySize;
				dsvDesc.Texture2DArray.FirstArraySlice = 0;
				dsvDesc.Texture2DArray.MipSlice = 0;
			}
			else
			{
				dsvDesc.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE2DMSARRAY;
				dsvDesc.Texture2DMSArray.ArraySize = texDesc.arraySize;
				dsvDesc.Texture2DMSArray.FirstArraySlice = 0;
			}
			break;
		}

		HRESULT hr = d3dDevice->CreateDepthStencilView(d3dTex, &dsvDesc, &dsv);
		if (FAILED(hr))
		{
			ErrorThrow(true);
			return;
		}

		rtvSize = Float2{ static_cast<float>(texDesc.width), static_cast<float>(texDesc.height) };
	}

	if (texToUse)
	{
		ownedTextures.emplace_back(texToUse);
	}
	if (depthStencilTex)
	{
		ownedTextures.emplace_back(depthStencilTex);
	}
}

void GLOWE::DirectX11::RenderTarget::create(const Vector<SharedPtr<TextureImpl>>& textures, const SharedPtr<TextureImpl>& depthStencilTex, const GraphicsDeviceImpl& device)
{
	TextureDesc texDesc;
	ID3D11Resource* d3dTex = nullptr;
	ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();

	if (!d3dDevice)
	{
		return;
	}

	destroy();

	using Type = TextureDesc::Type;

	for (const auto& texToUse : textures)
	{
		texDesc = texToUse->getDesc();

		d3dTex = dynamic_cast<const Texture*>(texToUse.get())->getTexture();

		if (!d3dTex)
		{
			WarningThrow(false, "d3dTex == nullptr");
			return;
		}

		if (texDesc.binding != TextureDesc::Binding::RenderTarget)
		{
			WarningThrow(false, "Unable to create RenderTarget. Invalid binding.");
			return;
		}

		D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
		rtvDesc.Format = getDXFormat(texDesc.format);

		switch (texDesc.type)
		{
		case Type::Texture1D:
			rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE1D;
			rtvDesc.Texture1D.MipSlice = 0;
			break;
		case Type::Texture1DArray:
			rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE1DARRAY;
			rtvDesc.Texture1DArray.ArraySize = texDesc.arraySize;
			rtvDesc.Texture1DArray.FirstArraySlice = 0;
			rtvDesc.Texture1DArray.MipSlice = 0;
			break;
		case Type::Texture2D:
			if (texDesc.multisamplingLevel == 0)
			{
				rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2D;
				rtvDesc.Texture2D.MipSlice = 0;
			}
			else
			{
				rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2DMS;
				rtvDesc.Texture2DMS.UnusedField_NothingToDefine = 0;
			}
			break;
		case Type::TextureCube:
		case Type::Texture2DArray:
			if (texDesc.multisamplingLevel == 0)
			{
				rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
				rtvDesc.Texture2DArray.ArraySize = texDesc.arraySize;
				rtvDesc.Texture2DArray.FirstArraySlice = 0;
				rtvDesc.Texture2DArray.MipSlice = 0;
			}
			else
			{
				rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2DMSARRAY;
				rtvDesc.Texture2DMSArray.ArraySize = texDesc.arraySize;
				rtvDesc.Texture2DMSArray.FirstArraySlice = 0;
			}
			break;
		case Type::Texture3D:
			rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE3D;
			rtvDesc.Texture3D.MipSlice = 0;
			rtvDesc.Texture3D.FirstWSlice = 0;
			rtvDesc.Texture3D.WSize = -1;
			break;
		}

		ComPtr<ID3D11RenderTargetView> tempRtv = nullptr;

		HRESULT hr = d3dDevice->CreateRenderTargetView(d3dTex, &rtvDesc, &tempRtv);
		if (FAILED(hr))
		{
			ErrorThrow(true);
			return;
		}

		rtv.push_back(std::move(tempRtv));
	}

	if (textures.size() > 0)
	{
		texDesc = textures[0]->getDesc();
		rtvSize = Float2{ static_cast<float>(texDesc.width), static_cast<float>(texDesc.height) };
	}

	d3dTex = depthStencilTex ? dynamic_cast<const Texture&>(*depthStencilTex).getTexture() : nullptr;

	if (d3dTex)
	{
		texDesc = depthStencilTex->getDesc();
		if (texDesc.binding != TextureDesc::Binding::DepthStencil)
		{
			WarningThrow(false, String(u8"Unable to create DepthStencil for RenderTarget. Invalid binding."));
			return;
		}

		D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
		dsvDesc.Format = getDXFormat(texDesc.format);
		dsvDesc.Flags = 0;

		switch (texDesc.type)
		{
		case Type::Texture1D:
			dsvDesc.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE1D;
			dsvDesc.Texture1D.MipSlice = 0;
			break;
		case Type::Texture1DArray:
			dsvDesc.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE1DARRAY;
			dsvDesc.Texture1DArray.ArraySize = texDesc.arraySize;
			dsvDesc.Texture1DArray.FirstArraySlice = 0;
			dsvDesc.Texture1DArray.MipSlice = 0;
			break;
		case Type::Texture2D:
			if (texDesc.multisamplingLevel == 0)
			{
				dsvDesc.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE2D;
				dsvDesc.Texture2D.MipSlice = 0;
			}
			else
			{
				dsvDesc.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE2DMS;
				dsvDesc.Texture2DMS.UnusedField_NothingToDefine = 0;
			}
			break;
		case Type::Texture2DArray:
			if (texDesc.multisamplingLevel == 0)
			{
				dsvDesc.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
				dsvDesc.Texture2DArray.ArraySize = texDesc.arraySize;
				dsvDesc.Texture2DArray.FirstArraySlice = 0;
				dsvDesc.Texture2DArray.MipSlice = 0;
			}
			else
			{
				dsvDesc.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE2DMSARRAY;
				dsvDesc.Texture2DMSArray.ArraySize = texDesc.arraySize;
				dsvDesc.Texture2DMSArray.FirstArraySlice = 0;
			}
			break;
		}

		HRESULT hr = d3dDevice->CreateDepthStencilView(d3dTex, &dsvDesc, &dsv);
		if (FAILED(hr))
		{
			ErrorThrow(true);
			return;
		}

		rtvSize = Float2{ static_cast<float>(texDesc.width), static_cast<float>(texDesc.height) };
	}

	if (depthStencilTex)
	{
		ownedTextures.emplace_back(depthStencilTex);
	}

	for (const auto& x : textures)
	{
		ownedTextures.emplace_back(x);
	}
}

void GLOWE::DirectX11::RenderTarget::destroy()
{
	rtv.clear();

	dsv = nullptr;
	rtvSize = Float2();
	ownedTextures.clear();
}

bool GLOWE::DirectX11::RenderTarget::checkIsValid() const
{
	return rtv.size() > 0;
}

void GLOWE::DirectX11::RenderTarget::create(const Texture& depthStencilTex, ID3D11Texture2D* backbuffer, const GraphicsDeviceImpl& device)
{
	ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();

	if (!d3dDevice)
	{
		return;
	}

	destroy();

	TextureDesc texDesc = depthStencilTex.getDesc();
	ComPtr<ID3D11RenderTargetView> tempRtv = nullptr;
	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	rtvDesc.Format = DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM;
	
	if (texDesc.multisamplingLevel == 0)
	{
		rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2D;
		rtvDesc.Texture2D.MipSlice = 0;
	}
	else
	{
		rtvDesc.ViewDimension = D3D11_RTV_DIMENSION::D3D11_RTV_DIMENSION_TEXTURE2DMS;
		rtvDesc.Texture2DMS.UnusedField_NothingToDefine = 0;
	}

	HRESULT hr = d3dDevice->CreateRenderTargetView(backbuffer, &rtvDesc, &tempRtv);
	if (FAILED(hr))
	{
		ErrorThrow(true);
		return;
	}

	rtv.push_back(std::move(tempRtv));

	D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
	dsvDesc.Flags = 0;
	dsvDesc.Format = getDXFormat(texDesc.format);
	if (texDesc.multisamplingLevel == 0)
	{
		dsvDesc.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE2D;
		dsvDesc.Texture2D.MipSlice = 0;
	}
	else
	{
		dsvDesc.ViewDimension = D3D11_DSV_DIMENSION::D3D11_DSV_DIMENSION_TEXTURE2DMS;
		dsvDesc.Texture2DMS.UnusedField_NothingToDefine = 0;
	}

	hr = d3dDevice->CreateDepthStencilView(depthStencilTex.getTexture(), &dsvDesc, &dsv);
	if (FAILED(hr))
	{
		ErrorThrow(true);
		return;
	}

	rtvSize = Float2{ static_cast<float>(texDesc.width), static_cast<float>(texDesc.height) };
}

ID3D11RenderTargetView * GLOWE::DirectX11::RenderTarget::getRTV(const unsigned int which) const
{
	if (which < rtv.size())
	{
		return rtv[which];
	}

	return nullptr;
}

ID3D11DepthStencilView * GLOWE::DirectX11::RenderTarget::getDSV() const
{
	return dsv;
}

const GLOWE::Vector<ComPtr<ID3D11RenderTargetView>>& GLOWE::DirectX11::RenderTarget::getRTVs() const
{
	return rtv;
}
