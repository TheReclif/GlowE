#include "DirectX11BufferImplementation.h"
#include "DirectX11GraphicsDeviceImplementation.h"

GLOWE::DirectX11::Buffer::Buffer(const BufferDesc& desc, const unsigned long long buffSize, const GraphicsDeviceImpl& device, const void* data)
	: Buffer()
{
	create(desc, buffSize, device, data);
}

GLOWE::DirectX11::Buffer::~Buffer()
{
	destroy();
}

void GLOWE::DirectX11::Buffer::create(const BufferDesc& desc, const unsigned long long buffSize, const GraphicsDeviceImpl& device, const void* data)
{
	if (buffSize == 0)
	{
		return;
	}

	destroy();

	auto translateBind = [](const BufferDesc& desc) -> UINT
	{
		using Binding = decltype(desc.binds);

		switch (desc.binds)
		{
		case Binding::ConstantBuffer:
			return D3D11_BIND_FLAG::D3D11_BIND_CONSTANT_BUFFER;
			break;
		case Binding::IndexBuffer:
			return D3D11_BIND_FLAG::D3D11_BIND_INDEX_BUFFER;
			break;
		case Binding::VertexBuffer:
			return D3D11_BIND_FLAG::D3D11_BIND_VERTEX_BUFFER;
			break;
		case Binding::StreamOutputBuffer:
			return D3D11_BIND_FLAG::D3D11_BIND_STREAM_OUTPUT;
			break;
		case Binding::UnorderedAccess:
			return D3D11_BIND_FLAG::D3D11_BIND_UNORDERED_ACCESS;
			break;
		case Binding::ShaderResource:
			return D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE;
			break;
		default:
			return 0;
			break;
		}
	};
	auto translateCPUAccess = [](const BufferDesc& desc) -> UINT
	{
		switch (desc.cpuAccess)
		{
		case CPUAccess::Read:
			return D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_READ;
			break;
		case CPUAccess::Write:
			return D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_WRITE;
			break;
		case CPUAccess::ReadWrite:
			return D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_READ | D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_WRITE;
			break;
		default:
			return 0;
			break;
		}
	};
	auto translateUsage = [](const BufferDesc& desc) -> D3D11_USAGE
	{
		switch (desc.usage)
		{
		case Usage::Default:
			return D3D11_USAGE::D3D11_USAGE_DEFAULT;
			break;
		case Usage::Dynamic:
			return D3D11_USAGE::D3D11_USAGE_DYNAMIC;
			break;
		case Usage::Immutable:
			return D3D11_USAGE::D3D11_USAGE_IMMUTABLE;
			break;
		case Usage::Staging:
			return D3D11_USAGE::D3D11_USAGE_STAGING;
			break;
		default:
			return D3D11_USAGE::D3D11_USAGE_DEFAULT;
			break;
		}
	};

	D3D11_BUFFER_DESC d3dDesc;
	D3D11_SUBRESOURCE_DATA subResData;
	d3dDesc.MiscFlags = 0;
	d3dDesc.StructureByteStride = 0;
	d3dDesc.BindFlags = translateBind(desc);
	d3dDesc.CPUAccessFlags = translateCPUAccess(desc);
	d3dDesc.Usage = translateUsage(desc);

	if (desc.binds == BufferDesc::Binding::ShaderResource)
	{
		d3dDesc.StructureByteStride = desc.layout.getSize();
	}

	d3dDesc.ByteWidth = buffSize;

	subResData.SysMemPitch = 0;
	subResData.SysMemSlicePitch = 0;
	subResData.pSysMem = data;

	ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();

	if (!d3dDevice)
	{
		return;
	}

	HRESULT hr = d3dDevice->CreateBuffer(&d3dDesc, (data == nullptr ? nullptr : &subResData), &buffer);
	if (FAILED(hr))
	{
		ErrorThrow(true);
		return;
	}

	usedDesc = desc;
	bufferSize = buffSize;
}

void GLOWE::DirectX11::Buffer::destroy()
{
	if (buffer)
	{
		buffer = nullptr;
		usedDesc = BufferDesc();
		bufferSize = 0;
	}
}

bool GLOWE::DirectX11::Buffer::checkIsValid() const
{
	return buffer;
}

void GLOWE::DirectX11::Buffer::update(BasicRendererImpl& renderer, const void * data, const unsigned long long dataSize, const unsigned long long offset) const
{
	const unsigned int finalSize = dataSize == 0 ? bufferSize : dataSize;
	if (buffer)
	{
		renderer.updateBuffer(this, data, finalSize, offset);
	}
}

bool GLOWE::DirectX11::Buffer::checkIsCreated() const
{
	return buffer != nullptr;
}

ID3D11Buffer * GLOWE::DirectX11::Buffer::getBuffer() const
{
	return buffer;
}
