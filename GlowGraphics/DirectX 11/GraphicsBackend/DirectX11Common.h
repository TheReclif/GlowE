#pragma once
#ifndef GLOWE_DIRECTX11_COMMON_INCLUDED
#define GLOWE_DIRECTX11_COMMON_INCLUDED

#ifndef NOMINMAX
#define NOMINMAX
#endif

#include <functional>
#include <atlbase.h>

#include "glowedirect3d11renderbackend_export.h"

#include "../../Uniform/Format.h"
#include "../../Uniform/GraphicsFactory.h"

#pragma warning( push )
#pragma warning( disable : 4005 )
#pragma warning( disable : 4838 )

//DXGI

#include <DXGI.h>

//Direct3D

#include <D3D11.h>
#include <D3Dcompiler.h>

#pragma warning( pop )

#ifdef _MSC_VER
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "dxguid.lib")
#endif

using ID3D11Blob = ID3D10Blob;

DXGI_FORMAT getDXFormat(const GLOWE::Format& format);

extern "C" __declspec(dllexport) GLOWE::GraphicsFactoryImpl * createFactory();

static_assert(std::is_same<decltype(&createFactory), GLOWE::GraphicsFactoryImpl::GetFactoryFunc>::value, "createFactory's type must be the same as GraphicsFactoryImpl::GetFactoryFunc.");

template<class T>
using ComPtr = CComPtr<T>;

#endif
