#include "DirectX11SwapChain.h"

#include "../../../GlowWindow/Uniform/Window.h"

void GLOWE::DirectX11::SwapChain::changeMSAAState(const unsigned int msaaLevel)
{
	if (usedDesc.msaa == msaaLevel)
	{
		return;
	}

	//TODO: Do it and/or test

	parentDevice->getDefaultRenderer().setRenderTarget(nullptr);

	swapChain = nullptr;
	defaultRenderTarget.destroy();
	defaultDepthStencilTexture.destroy();

	DXGI_SWAP_CHAIN_DESC swapChainDesc;

	HRESULT result = ERROR_SUCCESS;

	swapChainDesc.Windowed = true;
	swapChainDesc.BufferDesc.Width = usedDesc.size.x;
	swapChainDesc.BufferDesc.Height = usedDesc.size.y;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = usedDesc.size.refreshRate; //TODO: Testy
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferDesc.Format = getDXFormat(usedDesc.displayFormat);
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapChainDesc.Flags = 0;
	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = associatedWindow;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	ID3D11Device* const device = parentDevice->getDevice();

	if (msaaLevel > 0)
	{
		UINT sampleQuality = 0;

		device->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, msaaLevel, &sampleQuality);

		if (sampleQuality > 0)
		{
			swapChainDesc.SampleDesc.Count = msaaLevel;
			swapChainDesc.SampleDesc.Quality = sampleQuality - 1;
			logMessage(u8"MSAA enabled.");
			logMessage(String(u8"MSAA level: ") + toString(msaaLevel));
		}
		else
		{
			WarningThrow(true, (String(u8"MSAA level ") + toString(msaaLevel) + String(u8" is no supported. MSAA is disabled.")));
			swapChainDesc.SampleDesc.Count = 1;
			swapChainDesc.SampleDesc.Quality = 0;
			logMessage(u8"MSAA disabled.");
		}
	}
	else
	{
		swapChainDesc.SampleDesc.Count = 1;
		swapChainDesc.SampleDesc.Quality = 0;
		logMessage(u8"MSAA disabled.");
	}

	ComPtr<IDXGIDevice> dxgiDevice = nullptr;
	result = device->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&dxgiDevice));
	if (FAILED(result))
	{
		ErrorThrow(true);
		return;
	}

	ComPtr<IDXGIAdapter> dxgiAdapter = nullptr;
	result = dxgiDevice->GetParent(__uuidof(IDXGIAdapter), reinterpret_cast<void**>(&dxgiAdapter));
	if (FAILED(result))
	{
		ErrorThrow(true);
		return;
	}

	ComPtr<IDXGIFactory> dxgiFactory = nullptr;
	result = dxgiAdapter->GetParent(__uuidof(IDXGIFactory), reinterpret_cast<void**>(&dxgiFactory));
	if (FAILED(result))
	{
		ErrorThrow(true);
		return;
	}

	result = dxgiFactory->CreateSwapChain(device, &swapChainDesc, &swapChain);
	if (FAILED(result))
	{
		ErrorThrow(true);
		return;
	}

	TextureDesc texDesc;
	Format format;
	format.elemsCount = Format::ElemsCount::D24S8;
	texDesc.arraySize = 1;
	texDesc.binding = TextureDesc::Binding::DepthStencil;
	texDesc.usage = Usage::Default;
	texDesc.format = format;
	texDesc.multisamplingLevel = msaaLevel;
	defaultDepthStencilTexture.create(texDesc, *parentDevice);

	ComPtr<ID3D11Texture2D> backBuffer = nullptr;
	swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBuffer));
	defaultRenderTarget.create(defaultDepthStencilTexture, backBuffer, *parentDevice);
}

void GLOWE::DirectX11::SwapChain::resizeBuffers(const VideoMode& videoMode)
{
	VideoMode mode = videoMode;

	if (videoMode.x < 8 || videoMode.y < 8)
	{
		mode.x = 8;
		mode.y = 8;
	}

	parentDevice->getDefaultRenderer().setRenderTarget(nullptr);
	defaultRenderTarget.destroy();

	TextureDesc texDesc = defaultDepthStencilTexture.getDesc();
	texDesc.height = mode.y;
	texDesc.width = mode.x;
	defaultDepthStencilTexture.destroy();
	swapChain->ResizeBuffers(0, mode.x, mode.y, DXGI_FORMAT_UNKNOWN, 0);

	ComPtr<ID3D11Texture2D> backBuffer = nullptr;
	defaultDepthStencilTexture.create(texDesc, *parentDevice);

	swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBuffer)); // NOLINT(clang-diagnostic-language-extension-token)
	if (backBuffer)
	{
		defaultRenderTarget.create(defaultDepthStencilTexture, backBuffer, *parentDevice);
	}
	else
	{
		WarningThrow(true, "Unable to obtain the back buffer.");
	}
}

GLOWE::DirectX11::SwapChain::~SwapChain()
{
	destroy();
}

void GLOWE::DirectX11::SwapChain::create(const SwapChainDesc& desc, const WindowHandle window, GraphicsDeviceImpl& device)
{
	destroy();

	associatedWindow = window;
	if (associatedWindow == nullptr)
	{
		throw Exception("associatedWindow == nullptr");
	}
	parentDevice = dynamic_cast<GraphicsDevice*>(&device);
	if (parentDevice == nullptr)
	{
		throw Exception("parentDevice == nullptr");
	}

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	HRESULT result = ERROR_SUCCESS;

	swapChainDesc.Windowed = true;
	swapChainDesc.BufferDesc.Width = desc.size.x;
	swapChainDesc.BufferDesc.Height = desc.size.y;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = desc.size.refreshRate; //TODO: Testy
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferDesc.Format = getDXFormat(desc.displayFormat);
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapChainDesc.Flags = 0;
	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = associatedWindow;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	const auto d3d11Device = parentDevice->getDevice();

	if (desc.msaa > 0)
	{
		UINT sampleQuality = 0;

		d3d11Device->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, desc.msaa, &sampleQuality);

		if (sampleQuality > 0)
		{
			swapChainDesc.SampleDesc.Count = desc.msaa;
			swapChainDesc.SampleDesc.Quality = sampleQuality - 1;
			logMessage(u8"MSAA enabled.");
			logMessage(String(u8"MSAA level: ") + toString(desc.msaa));
		}
		else
		{
			WarningThrow(true, (String(u8"MSAA level ") + toString(desc.msaa) + String(u8" is no supported. MSAA is disabled.")));
			swapChainDesc.SampleDesc.Count = 1;
			swapChainDesc.SampleDesc.Quality = 0;
			logMessage(u8"MSAA disabled.");
		}
	}
	else
	{
		swapChainDesc.SampleDesc.Count = 1;
		swapChainDesc.SampleDesc.Quality = 0;
		logMessage(u8"MSAA disabled.");
	}

	ComPtr<IDXGIDevice> dxgiDevice = nullptr;
	result = d3d11Device->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&dxgiDevice)); // NOLINT(clang-diagnostic-language-extension-token)
	if (FAILED(result))
	{
		ErrorThrow(true);
		return;
	}

	ComPtr<IDXGIAdapter> dxgiAdapter = nullptr;
	result = dxgiDevice->GetParent(__uuidof(IDXGIAdapter), reinterpret_cast<void**>(&dxgiAdapter)); // NOLINT(clang-diagnostic-language-extension-token)
	if (FAILED(result))
	{
		ErrorThrow(true);
		return;
	}

	ComPtr<IDXGIFactory> dxgiFactory = nullptr;
	result = dxgiAdapter->GetParent(__uuidof(IDXGIFactory), reinterpret_cast<void**>(&dxgiFactory)); // NOLINT(clang-diagnostic-language-extension-token)
	if (FAILED(result))
	{
		ErrorThrow(true);
		return;
	}

	result = dxgiFactory->CreateSwapChain(d3d11Device, &swapChainDesc, &swapChain);
	if (FAILED(result))
	{
		ErrorThrow(true);
		return;
	}

	TextureDesc texDesc;
	Format format;
	format.elemsCount = Format::ElemsCount::D24S8;
	texDesc.arraySize = 1;
	texDesc.binding = TextureDesc::Binding::DepthStencil;
	texDesc.usage = Usage::Default;
	texDesc.format = format;
	texDesc.multisamplingLevel = desc.msaa;
	texDesc.generateMipmaps = false;
	texDesc.mipmapsCount = 1;
	texDesc.width = desc.size.x;
	texDesc.height = desc.size.y;
	defaultDepthStencilTexture.create(texDesc, device);

	ComPtr<ID3D11Texture2D> backBuffer = nullptr;
	swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&backBuffer)); // NOLINT(clang-diagnostic-language-extension-token)
	defaultRenderTarget.create(defaultDepthStencilTexture, backBuffer, device);

	usedDesc = desc;
}

void GLOWE::DirectX11::SwapChain::create(const SwapChainDesc& desc, const WindowImplementation* const window, GraphicsDeviceImpl& device)
{
	GlowAssert(window != nullptr);
	create(desc, window->getWindowHandle(), device);
}

void GLOWE::DirectX11::SwapChain::create(const SwapChainDesc& desc, const Window& window, GraphicsDeviceImpl& device)
{
	create(desc, window.getWindowHandle(), device);
}

void GLOWE::DirectX11::SwapChain::destroy()
{
	if (swapChain)
	{
		swapChain = nullptr;
		defaultDepthStencilTexture.destroy();
		defaultRenderTarget.destroy();
	}
}

void GLOWE::DirectX11::SwapChain::setPresentInterval(const unsigned int vsync)
{
	usedDesc.initialPresentInterval = vsync;
}

GLOWE::RenderTargetImpl& GLOWE::DirectX11::SwapChain::getRenderTarget()
{
	return defaultRenderTarget;
}

const GLOWE::RenderTargetImpl& GLOWE::DirectX11::SwapChain::getRenderTarget() const
{
	return defaultRenderTarget;
}

void GLOWE::DirectX11::SwapChain::present() const
{
	if (!swapChain)
	{
		throw Exception("Swap chain not created");
	}
	HRESULT hr = swapChain->Present(clamp(usedDesc.initialPresentInterval, 0U, 4U), 0);
	if (FAILED(hr))
	{
		throw GraphicsDeviceDiedException();
	}
}

bool GLOWE::DirectX11::SwapChain::checkIsValid() const
{
	return swapChain != nullptr;
}
