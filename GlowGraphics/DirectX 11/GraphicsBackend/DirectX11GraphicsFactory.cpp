#include "DirectX11GraphicsFactory.h"

#include "DirectX11BlendStateImplementation.h"
#include "DirectX11BufferImplementation.h"
#include "DirectX11DepthStencilStateImplementation.h"
#include "DirectX11InputLayoutImplementation.h"
#include "DirectX11RasterizerStateImplementation.h"
#include "DirectX11RenderTargetImplementation.h"
#include "DirectX11SamplerImplementation.h"
#include "DirectX11ShaderCollectionImplementation.h"
#include "DirectX11ShaderReflectImplementation.h"
#include "DirectX11TextureImplementation.h"
#include "DirectX11UnorderedAccessViewImplementation.h"
#include "DirectX11ShaderResourceViewImplementation.h"
#include "DirectX11SwapChain.h"

#include "DirectX11VertexShaderImplementation.h"
#include "DirectX11PixelShaderImplementation.h"
#include "DirectX11GeometryShaderImplementation.h"
#include "DirectX11GeometryShaderSOImplementation.h"
#include "DirectX11ComputeShaderImplementation.h"

GLOWE::UniquePtr<GLOWE::BlendStateImpl> GLOWE::DirectX11::GraphicsFactory::createBlendState() const
{
    return makeUniquePtr<BlendState>();
}

GLOWE::UniquePtr<GLOWE::BufferImpl> GLOWE::DirectX11::GraphicsFactory::createBuffer() const
{
    return makeUniquePtr<Buffer>();
}

GLOWE::UniquePtr<GLOWE::DepthStencilStateImpl> GLOWE::DirectX11::GraphicsFactory::createDepthStencilState() const
{
    return makeUniquePtr<DepthStencilState>();
}

GLOWE::UniquePtr<GLOWE::GraphicsDeviceImpl> GLOWE::DirectX11::GraphicsFactory::createGraphicsDevice() const
{
    return makeUniquePtr<GraphicsDevice>();
}

GLOWE::UniquePtr<GLOWE::InputLayoutImpl> GLOWE::DirectX11::GraphicsFactory::createInputLayout() const
{
    return makeUniquePtr<InputLayout>();
}

GLOWE::UniquePtr<GLOWE::RasterizerStateImpl> GLOWE::DirectX11::GraphicsFactory::createRasterizerState() const
{
    return makeUniquePtr<RasterizerState>();
}

GLOWE::UniquePtr<GLOWE::RenderTargetImpl> GLOWE::DirectX11::GraphicsFactory::createRenderTarget() const
{
    return makeUniquePtr<RenderTarget>();
}

GLOWE::UniquePtr<GLOWE::SamplerImpl> GLOWE::DirectX11::GraphicsFactory::createSampler() const
{
    return makeUniquePtr<Sampler>();
}

GLOWE::UniquePtr<GLOWE::ShaderCollectionImpl> GLOWE::DirectX11::GraphicsFactory::createShaderCollection() const
{
    return makeUniquePtr<ShaderCollection>();
}

GLOWE::UniquePtr<GLOWE::VertexShaderImpl> GLOWE::DirectX11::GraphicsFactory::createVertexShader() const
{
    return makeUniquePtr<VertexShader>();
}

GLOWE::UniquePtr<GLOWE::PixelShaderImpl> GLOWE::DirectX11::GraphicsFactory::createPixelShader() const
{
    return makeUniquePtr<PixelShader>();
}

GLOWE::UniquePtr<GLOWE::GeometryShaderImpl> GLOWE::DirectX11::GraphicsFactory::createGeometryShader() const
{
    return makeUniquePtr<GeometryShader>();
}

GLOWE::UniquePtr<GLOWE::ComputeShaderImpl> GLOWE::DirectX11::GraphicsFactory::createComputeShader() const
{
    return makeUniquePtr<ComputeShader>();
}

GLOWE::UniquePtr<GLOWE::ShaderReflectImpl> GLOWE::DirectX11::GraphicsFactory::createShaderReflection() const
{
    return makeUniquePtr<ShaderReflect>();
}

GLOWE::UniquePtr<GLOWE::TextureImpl> GLOWE::DirectX11::GraphicsFactory::createTexture() const
{
    return makeUniquePtr<Texture>();
}

GLOWE::UniquePtr<GLOWE::ShaderResourceViewImpl> GLOWE::DirectX11::GraphicsFactory::createShaderResourceView() const
{
    return makeUniquePtr<ShaderResourceView>();
}

GLOWE::UniquePtr<GLOWE::UnorderedAccessViewImpl> GLOWE::DirectX11::GraphicsFactory::createUnorderedAccessView() const
{
    return makeUniquePtr<UnorderedAccessView>();
}

GLOWE::UniquePtr<GLOWE::SwapChain> GLOWE::DirectX11::GraphicsFactory::createSwapChain() const
{
    return makeUniquePtr<DirectX11::SwapChain>();
}
