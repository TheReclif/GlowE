#include "DirectX11InputLayoutImplementation.h"
#include "DirectX11VertexShaderImplementation.h"

#include "DirectX11GraphicsDeviceImplementation.h"
#include "../../../GlowSystem/Regex.h"

GLOWE::DirectX11::InputLayout::InputLayout(const InputLayoutDesc& inputDesc, const GraphicsDeviceImpl& device, const VertexShaderImpl& shader)
	: inputLayout(nullptr)
{
	create(inputDesc, device, shader);
}

GLOWE::DirectX11::InputLayout::~InputLayout()
{
	destroy();
}

void GLOWE::DirectX11::InputLayout::create(const InputLayoutDesc& inputDesc, const GraphicsDeviceImpl& device, const VertexShaderImpl& shader)
{
	auto submatchToString = [](const std::sub_match<String::ConstIterator>& arg) -> String
	{
		return String(arg.first, arg.second);
	};

	const NamedStructureLayout& vertexLayout = inputDesc.getVertexLayout(), &instanceLayout = inputDesc.getInstanceLayout();

	const unsigned int vertexCount = vertexLayout.getElemsCount(), instanceCount = instanceLayout.getElemsCount();
	D3D11_INPUT_ELEMENT_DESC d3dDesc;
	Vector<D3D11_INPUT_ELEMENT_DESC> inputElems;
	inputElems.reserve(vertexCount + instanceCount);

	const Vector<UInt32>& stepRates = inputDesc.getStepRates(), &vertexInputSlots = inputDesc.getVertexInputSlots(), &instanceInputSlots = inputDesc.getInstanceInputSlots();

	ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();
	ID3DBlob* byteCode = static_cast<const VertexShader&>(shader).getBytecode();

	if (!d3dDevice || !byteCode)
	{
		return;
	}

	Vector<String> FUCKINGTEMPORARIES;
	FUCKINGTEMPORARIES.reserve(vertexCount + instanceCount);

	Regex semanticIndexRegex(u8"([A-Za-z]+)(\\d*)");
	RegexMatch match;
	String tempStr;

	d3dDesc.InputSlotClass = D3D11_INPUT_CLASSIFICATION::D3D11_INPUT_PER_VERTEX_DATA;
	d3dDesc.InstanceDataStepRate = 0;

	for (unsigned int x = 0; x < vertexCount; ++x)
	{
		tempStr = vertexLayout.getName(x);
		std::regex_match(tempStr.getString(), match, semanticIndexRegex);

		if (match[1].length() == 0)
		{
			ErrorThrow(true);
			return;
		}

		FUCKINGTEMPORARIES.push_back(submatchToString(match[1]));

		tempStr = submatchToString(match[2]);
		if (tempStr.isEmpty())
		{
			d3dDesc.SemanticIndex = 0;
		}
		else
		{
			d3dDesc.SemanticIndex = tempStr.toUInt();
		}

		d3dDesc.Format = getDXFormat(vertexLayout.getElem(x));
		d3dDesc.InputSlot = vertexInputSlots[x];
		d3dDesc.AlignedByteOffset = vertexLayout.getOffsetToElem(x);
		d3dDesc.SemanticName = FUCKINGTEMPORARIES.back().getCharArray();

		inputElems.push_back(d3dDesc);
	}

	d3dDesc.InputSlotClass = D3D11_INPUT_CLASSIFICATION::D3D11_INPUT_PER_INSTANCE_DATA;

	for (unsigned int x = 0; x < instanceCount; ++x)
	{
		tempStr = instanceLayout.getName(x);
		std::regex_match(tempStr.getString(), match, semanticIndexRegex);

		if (match[1].length() == 0)
		{
			ErrorThrow(true);
			return;
		}

		FUCKINGTEMPORARIES.push_back(submatchToString(match[1]));

		tempStr = submatchToString(match[2]);
		if (tempStr.isEmpty())
		{
			d3dDesc.SemanticIndex = 0;
		}
		else
		{
			d3dDesc.SemanticIndex = tempStr.toUInt();
		}

		d3dDesc.InstanceDataStepRate = stepRates[x];
		d3dDesc.Format = getDXFormat(instanceLayout.getElem(x));
		d3dDesc.InputSlot = instanceInputSlots[x];
		d3dDesc.AlignedByteOffset = instanceLayout.getOffsetToElem(x);
		d3dDesc.SemanticName = FUCKINGTEMPORARIES.back().getCharArray();

		inputElems.push_back(d3dDesc);
	}

	HRESULT hr = d3dDevice->CreateInputLayout(inputElems.data(), inputElems.size(), byteCode->GetBufferPointer(), byteCode->GetBufferSize(), &inputLayout);
	if (FAILED(hr))
	{
		ErrorThrow(true);
		return;
	}

	desc = inputDesc;
}

void GLOWE::DirectX11::InputLayout::destroy()
{
	if (inputLayout)
	{
		inputLayout = nullptr;
		desc = InputLayoutDesc();
	}
}

bool GLOWE::DirectX11::InputLayout::checkIsValid() const
{
	return inputLayout;
}

ID3D11InputLayout * GLOWE::DirectX11::InputLayout::getInputLayout() const
{
	return inputLayout;
}
