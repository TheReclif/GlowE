#include "DirectX11GeometryShaderSOImplementation.h"
#include "DirectX11GraphicsDeviceImplementation.h"

GLOWE::DirectX11::GeometryShaderSO::~GeometryShaderSO()
{
	destroy();
}

void GLOWE::DirectX11::GeometryShaderSO::compileFromCode(const String& code, const GraphicsDeviceImpl& device, const CompilationData& compilationData)
{
	if (code.isEmpty())
	{
		WarningThrow(true, "No code to compile");
		return;
	}

	auto translateTarget = [](const UInt2& arg) -> const char*
	{
		switch (arg[0])
		{
		case 0:
		case 5:
			return u8"gs_5_0";
			break;
		case 4:
			if (arg[1] == 1)
			{
				return u8"gs_4_1";
			}
			else
			{
				return u8"gs_4_0";
			}
			break;
		default:
			WarningThrow(true, u8"Unrecognizable shader version. We'll try to compile it with the best version we've got.");
			return u8"gs_5_0";
			break;
		}
	};

	destroy();

	ComPtr<ID3DBlob> error = nullptr;
	const char* target = translateTarget(compilationData.version);
	UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifdef GLOWE_DEBUG
	flags |= D3DCOMPILE_DEBUG;
	flags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#else
	flags |= D3DCOMPILE_OPTIMIZATION_LEVEL3;
#endif

	HRESULT hr = D3DCompile(code.getCharArray(), code.getSize(), nullptr, nullptr, nullptr, compilationData.entrypoint.getCharArray(), target, flags, 0, &byteCode, &error);
	if (FAILED(hr))
	{
		if (error)
		{
			WarningThrow(true, u8"Geometry shader compilation failed. Error description:\n" + String(static_cast<char*>(error->GetBufferPointer()), error->GetBufferSize()));
			return;
		}
		else
		{
			WarningThrow(true, u8"Geometry shader compilation failed. Unknown error.");
			return;
		}
	}

	ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();
	if (d3dDevice)
	{
		//hr = d3dDevice->CreateGeometryShaderWithStreamOutput()
		throw NotImplementedException();
		if (FAILED(hr))
		{
			WarningThrow(true, u8"Geometry shader (ID3D11GeometryShader) creation failed.");
			return;
		}
	}
	else
	{
		WarningThrow(true, "Invalid (null) device.");
		return;
	}

	hash = Hash(code);
}

void GLOWE::DirectX11::GeometryShaderSO::destroy()
{
	shader = nullptr;
	byteCode = nullptr;
	hash = Hash();
}

bool GLOWE::DirectX11::GeometryShaderSO::checkIsCreated() const
{
	return shader != nullptr;
}

ID3DBlob * GLOWE::DirectX11::GeometryShaderSO::getBytecode() const
{
	return byteCode;
}

ID3D11GeometryShader * GLOWE::DirectX11::GeometryShaderSO::getShader() const
{
	return shader;
}

void GLOWE::DirectX11::GeometryShaderSO::fromByteCode(const void * bytes, const UInt32 size, const GraphicsDeviceImpl& device)
{
	destroy();
	ID3D11Device* dev = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();
	//dev->CreateComputeShader(bytes, size, nullptr, &shader);
	//dev->CreateGeometryShaderWithStreamOutput();;
	D3DCreateBlob(size, &byteCode);
	memcpy(byteCode->GetBufferPointer(), bytes, size);
}
