#pragma once
#ifndef GLOWE_GRAPHICS_DIRECTX_11_SRVIMPLEMENTATION_INCLUDED
#define GLOWE_GRAPHICS_DIRECTX_11_SRVIMPLEMENTATION_INCLUDED

#include "../../Uniform/ShaderResourceViewImpl.h"

#include "DirectX11Common.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT ShaderResourceView
			: public ShaderResourceViewImpl
		{
		private:
			ComPtr<ID3D11ShaderResourceView> srv;
		public:
			ShaderResourceView() = default;
			ShaderResourceView(ShaderResourceView&& arg) noexcept : srv(std::move(arg.srv)) {}
			virtual ~ShaderResourceView();

			virtual void create(const BufferImpl& buff, const GraphicsDeviceImpl& device) override;
			virtual void create(const TextureImpl& tex, const GraphicsDeviceImpl& device) override;
			virtual void destroy() override;

			virtual bool checkIsValid() const override;

			ID3D11ShaderResourceView* getSRV() const;
		};
	}
}

#endif
