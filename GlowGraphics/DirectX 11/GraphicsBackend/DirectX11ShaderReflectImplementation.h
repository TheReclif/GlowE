#pragma once
#ifndef GLOWE_DIRECTX_11_SHADERREFLECT_INCLUDED
#define GLOWE_DIRECTX_11_SHADERREFLECT_INCLUDED

#include "DirectX11Common.h"

#include "../../../GlowSystem/Utility.h"
#include "../../../GlowSystem/String.h"
#include "../../../GlowSystem/Error.h"

#include "../../Uniform/ShaderImplementation.h"
#include "../../Uniform/ShaderReflectImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT ShaderReflect
			: public ShaderReflectImpl
		{
		private:
			Vector<D3D11_SHADER_INPUT_BIND_DESC> resources;
			Vector<D3D11_SHADER_BUFFER_DESC> cBuffers;
			Map<String, unsigned int> nameToElem;
			unsigned int textureCount, bufferCount;
		private:
			void init(ID3DBlob* blob);
		public:
			ShaderReflect() = default;
			virtual ~ShaderReflect() = default;

			virtual void create(const ComputeShaderImpl& arg) override;
			virtual void create(const GeometryShaderImpl& arg) override;
			virtual void create(const PixelShaderImpl& arg) override;
			virtual void create(const VertexShaderImpl& arg) override;

			virtual unsigned int getResourcesCount() const override;
			virtual unsigned int getSRVCount() const override;
			virtual unsigned int getTexCount() const override;
			virtual unsigned int getCBufferCount() const override;
			virtual unsigned int getBuffCount() const override;

			virtual String getResourceName(const unsigned int id) const override;
			virtual String getCBufferName(const unsigned int id) const override;

			virtual unsigned int getResourceID(const String& name) const override;
			virtual unsigned int getCBufferID(const String& name) const override;
		};
	}
}

#endif
