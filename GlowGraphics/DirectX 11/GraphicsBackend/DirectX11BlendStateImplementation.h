#pragma once
#ifndef GLOWE_DIRECTX_11_BLENDSTATEIMPLEMENTATION_INCLUDED
#define GLOWE_DIRECTX_11_BLENDSTATEIMPLEMENTATION_INCLUDED

#include "DirectX11Common.h"

#include "../../../GlowSystem/Utility.h"

#include "../../Uniform/BlendStateImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT BlendState
			: public BlendStateImpl
		{
		private:
			ComPtr<ID3D11BlendState> blendState;
		public:
			BlendState();
			BlendState(const BlendDesc& arg, const GraphicsDeviceImpl& device);
			virtual ~BlendState();

			virtual void create(const BlendDesc& desc, const GraphicsDeviceImpl& device) override;
			virtual void destroy() override;

			virtual bool checkIsValid() const override;

			ID3D11BlendState* getBlendState() const;
		};
	}
}

#endif
