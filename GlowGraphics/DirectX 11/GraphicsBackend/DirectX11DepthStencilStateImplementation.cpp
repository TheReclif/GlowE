#include "DirectX11DepthStencilStateImplementation.h"
#include "DirectX11GraphicsDeviceImplementation.h"

GLOWE::DirectX11::DepthStencilState::DepthStencilState()
	: DepthStencilStateImpl(), depthStencilState(nullptr)
{
}

GLOWE::DirectX11::DepthStencilState::DepthStencilState(const DepthStencilDesc & arg, const GraphicsDeviceImpl& device)
	: DepthStencilStateImpl(), depthStencilState(nullptr)
{
	create(arg, device);
}

GLOWE::DirectX11::DepthStencilState::~DepthStencilState()
{
	destroy();
}

void GLOWE::DirectX11::DepthStencilState::create(const DepthStencilDesc & arg, const GraphicsDeviceImpl& device)
{
	destroy();

	using CompFunc = ComparisonFunction;
	using StencilOp = DepthStencilDesc::StencilOperation;
	using StencilOpDesc = DepthStencilDesc::StencilOperationDesc;

	D3D11_DEPTH_STENCIL_DESC d3dDSDesc;

	auto glowCompFuncToD3D = [](const CompFunc arg) -> D3D11_COMPARISON_FUNC
	{
		switch (arg)
		{
		case CompFunc::Never:
			return D3D11_COMPARISON_NEVER;
			break;
		case CompFunc::Less:
			return D3D11_COMPARISON_LESS;
			break;
		case CompFunc::Equal:
			return D3D11_COMPARISON_EQUAL;
			break;
		case CompFunc::LessEqual:
			return D3D11_COMPARISON_LESS_EQUAL;
			break;
		case CompFunc::Greater:
			return D3D11_COMPARISON_GREATER;
			break;
		case CompFunc::NotEqual:
			return D3D11_COMPARISON_NOT_EQUAL;
			break;
		case CompFunc::GreaterEqual:
			return D3D11_COMPARISON_GREATER_EQUAL;
			break;
		case CompFunc::Always:
			return D3D11_COMPARISON_ALWAYS;
			break;
		default:
			return D3D11_COMPARISON_LESS;
			break;
		}
	};
	auto glowStencilOpToD3D = [](const StencilOp op) -> D3D11_STENCIL_OP
	{
		switch (op)
		{
		case StencilOp::Keep:
			return D3D11_STENCIL_OP_KEEP;
			break;
		case StencilOp::Zero:
			return D3D11_STENCIL_OP_ZERO;
			break;
		case StencilOp::Replace:
			return D3D11_STENCIL_OP_REPLACE;
			break;
		case StencilOp::IncrementClamp:
			return D3D11_STENCIL_OP_INCR_SAT;
			break;
		case StencilOp::DecrementClamp:
			return D3D11_STENCIL_OP_DECR_SAT;
			break;
		case StencilOp::Invert:
			return D3D11_STENCIL_OP_INVERT;
			break;
		case StencilOp::Increment:
			return D3D11_STENCIL_OP_INCR;
			break;
		case StencilOp::Decrement:
			return D3D11_STENCIL_OP_DECR;
			break;
		default:
			return D3D11_STENCIL_OP_KEEP;
			break;
		}
	};
	auto glowStencilOpDescToD3D = [glowCompFuncToD3D, glowStencilOpToD3D](const StencilOpDesc& op) -> D3D11_DEPTH_STENCILOP_DESC
	{
		D3D11_DEPTH_STENCILOP_DESC result;

		result.StencilFunc = glowCompFuncToD3D(op.comparisonFunc);
		result.StencilDepthFailOp = glowStencilOpToD3D(op.depthFailOp);
		result.StencilFailOp = glowStencilOpToD3D(op.failOp);
		result.StencilPassOp = glowStencilOpToD3D(op.succOp);

		return result;
	};

	d3dDSDesc.DepthEnable = arg.enableDepthTest;
	d3dDSDesc.StencilEnable = arg.enableStencilTest;

	switch (arg.depthWriteMask)
	{
	case DepthStencilDesc::DepthWriteMask::All:
		d3dDSDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		break;
	case DepthStencilDesc::DepthWriteMask::Zero:
		d3dDSDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
		break;
	}

	d3dDSDesc.DepthFunc = glowCompFuncToD3D(arg.depthCompFunc);
	d3dDSDesc.StencilReadMask = arg.stencilReadMask;
	d3dDSDesc.StencilWriteMask = arg.stencilWriteMask;

	d3dDSDesc.FrontFace = glowStencilOpDescToD3D(arg.frontFace);
	d3dDSDesc.BackFace = glowStencilOpDescToD3D(arg.backFace);

	ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();

	HRESULT hr = d3dDevice->CreateDepthStencilState(&d3dDSDesc, &depthStencilState);
	if (FAILED(hr))
	{
		ErrorThrow(true);
	}

	desc = arg;
	hash = desc.getHash();
}

void GLOWE::DirectX11::DepthStencilState::destroy()
{
	if (depthStencilState)
	{
		depthStencilState = nullptr;
		desc = DepthStencilDesc();
		hash = Hash();
	}
}

bool GLOWE::DirectX11::DepthStencilState::checkIsValid() const
{
	return depthStencilState;
}

ID3D11DepthStencilState * GLOWE::DirectX11::DepthStencilState::getDepthStencilState() const
{
	return depthStencilState;
}
