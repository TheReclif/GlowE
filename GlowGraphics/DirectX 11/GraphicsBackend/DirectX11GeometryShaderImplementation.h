#pragma once
#ifndef GLOWE_DIRECTX_11_GEOMETRYSHADERIMPLEMENTATION_INCLUDED
#define GLOWE_DIRECTX_11_GEOMETRYSHADERIMPLEMENTATION_INCLUDED

#include "DirectX11Common.h"

#include "../../Uniform/ShaderImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT GeometryShader
			: public GeometryShaderImpl
		{
		private:
			ComPtr<ID3D11GeometryShader> shader;
			ComPtr<ID3D11Blob> byteCode;
		public:
			GeometryShader();
			virtual ~GeometryShader();

			virtual void compileFromCode(const String& code, const GraphicsDeviceImpl& device, const CompilationData& compilationData = CompilationData()) override;
			virtual void destroy() override;

			virtual bool checkIsCreated() const override;

			ID3DBlob* getBytecode() const;
			ID3D11GeometryShader* getShader() const;

			void fromByteCode(const void* bytes, const UInt32 size, const GraphicsDeviceImpl& device);
		};
	}
}

#endif
