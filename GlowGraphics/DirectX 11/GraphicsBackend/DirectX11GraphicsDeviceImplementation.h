#pragma once
#ifndef GLOW_DIRECTX11_GCIMPL_INCLUDED
#define GLOW_DIRECTX11_GCIMPL_INCLUDED

#include "DirectX11Common.h"

#include "../../../GlowSystem/Utility.h"
#include "../../../GlowSystem/Error.h"
#include "../../../GlowSystem/ThreadPool.h"

#include "../../Uniform/GraphicsDeviceImplementation.h"

#include "DirectX11BasicRendererImplementation.h"
#include "DirectX11RenderTargetImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT GraphicsDevice
			: public GLOWE::GraphicsDeviceImpl
		{
		private:
			ComPtr<ID3D11Device> device;
			/// @brief Curently used GPU's desc.
			DXGI_ADAPTER_DESC adapterDesc;
			BasicRenderer defaultRenderer;

			D3D_FEATURE_LEVEL chosenFeatureLevel;

			GraphicsDeviceSettings usedConfiguration;
		public:
			GraphicsDevice();
			virtual ~GraphicsDevice();

			virtual void create(const GraphicsDeviceSettings& desc) override;
			virtual void destroy() override;

			virtual bool checkIsCreated() const override;

			virtual GraphicsAPIInfo getAPIInfo() const override;

			virtual BasicRendererImpl& getDefaultRenderer() override;
			virtual const BasicRendererImpl& getDefaultRenderer() const override;

			virtual UInt64 queryAvailableMemory() const override;
			virtual UInt64 queryOccupiedMemory() const override;

			ID3D11Device* getDevice() const;
		};
	}
}

#endif
