#pragma once
#ifndef GLOWE_DIRECTX_11_SHADERCOLLECTIONIMPLEMENTATION_INCLUDED
#define GLOWE_DIRECTX_11_SHADERCOLLECTIONIMPLEMENTATION_INCLUDED

#include "DirectX11Common.h"

#include "../../Uniform/ShaderCollectionImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT ShaderCollection
			: public ShaderCollectionImpl
		{
		private:
			ComPtr<ID3D11VertexShader> vertexShader;
			ComPtr<ID3D11PixelShader> pixelShader;
			ComPtr<ID3D11GeometryShader> geometryShader;
			ComPtr<ID3D11ComputeShader> computeShader;
			ComPtr<ID3D11HullShader> hullShader;
			ComPtr<ID3D11DomainShader> domainShader;
			ShaderCollectionDesc usedDesc;
		public:
			ShaderCollection() = default;
			explicit ShaderCollection(const ShaderCollectionDesc& desc);
			virtual ~ShaderCollection();

			virtual void create(const ShaderCollectionDesc& desc) override;
			virtual void destroy() override;

			ID3D11VertexShader* getVertexShader() const;
			ID3D11PixelShader* getPixelShader() const;
			ID3D11GeometryShader* getGeometryShader() const;
			ID3D11ComputeShader* getComputeShader() const;
			ID3D11HullShader* getHullShader() const;
			ID3D11DomainShader* getDomainShader() const;

			virtual const ShaderCollectionDesc& getDesc() const override;
		};
	}
}

#endif
