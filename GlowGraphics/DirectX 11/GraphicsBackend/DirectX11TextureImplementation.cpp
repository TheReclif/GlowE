#include "DirectX11TextureImplementation.h"
#include "DirectX11GraphicsDeviceImplementation.h"

GLOWE::DirectX11::Texture::Texture(Texture && arg) noexcept
	: TextureImpl(exchange(arg.desc, TextureDesc())), texture(std::move(arg.texture)), srv(std::move(arg.srv))
{
}

GLOWE::DirectX11::Texture::~Texture()
{
	destroy();
}

GLOWE::DirectX11::Texture & GLOWE::DirectX11::Texture::operator=(Texture && arg)
{
	texture = std::move(arg.texture);
	std::swap(desc, arg.desc);

	return *this;
}

void GLOWE::DirectX11::Texture::create(const TextureDesc& arg, const GraphicsDeviceImpl& device, const Vector<void*> data)
{
	ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();
	Format formatFromArg = arg.format;
	if (formatFromArg.type != Format::Type::Float && formatFromArg.type != Format::Type::HalfFloat)
	{
		formatFromArg.isNormalized = true;
	}

	if (formatFromArg.elemsCount == 3 && formatFromArg.type != Format::Type::Float)
	{
		// TODO: Expensive conversion to RGBA.
		WarningThrow(true, "Formats with 3 elements will be converted to their 4 elements variants.");
		formatFromArg.elemsCount = 4;
	}

	if (!d3dDevice)
	{
		return;
	}

	shouldBeMipmapsGenerated = arg.generateMipmaps;

	auto translateBinding = [&]() -> UINT
	{
		using Binding = TextureDesc::Binding;

		UINT result = arg.usage != Usage::Staging ? D3D11_BIND_FLAG::D3D11_BIND_SHADER_RESOURCE : 0;

		switch (arg.binding)
		{
		case Binding::DepthStencil:
			result = D3D11_BIND_FLAG::D3D11_BIND_DEPTH_STENCIL;
			break;
		case Binding::RenderTarget:
			result |= D3D11_BIND_FLAG::D3D11_BIND_RENDER_TARGET;
			break;
		case Binding::UnorderedAccess:
			result |= D3D11_BIND_FLAG::D3D11_BIND_UNORDERED_ACCESS;
			break;
		default:
			break;
		}

		return result;
	};
	const auto translateCPUAccess = [&]() -> UINT
	{
		switch (arg.cpuAccess)
		{
		case CPUAccess::Read:
			return D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_READ;
			break;
		case CPUAccess::Write:
			return D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_WRITE;
			break;
		case CPUAccess::ReadWrite:
			return D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_READ | D3D11_CPU_ACCESS_FLAG::D3D11_CPU_ACCESS_WRITE;
			break;
		default:
			return 0;
			break;
		}
	};
	const auto translateUsage = [&]() -> D3D11_USAGE
	{
		switch (arg.usage)
		{
		case Usage::Default:
			return D3D11_USAGE::D3D11_USAGE_DEFAULT;
			break;
		case Usage::Dynamic:
			return D3D11_USAGE::D3D11_USAGE_DYNAMIC;
			break;
		case Usage::Immutable:
			return D3D11_USAGE::D3D11_USAGE_IMMUTABLE;
			break;
		case Usage::Staging:
			return D3D11_USAGE::D3D11_USAGE_STAGING;
			break;
		default:
			return D3D11_USAGE::D3D11_USAGE_DEFAULT;
			break;
		}
	};

	const auto processTex1D = [&, this]
	{
		D3D11_TEXTURE1D_DESC texDesc;

		if (arg.type == TextureDesc::Type::Texture1DArray)
		{
			texDesc.ArraySize = arg.arraySize;
		}
		else
		{
			texDesc.ArraySize = 1;
		}

		texDesc.BindFlags = translateBinding();
		texDesc.Format = getDXFormat(formatFromArg);
		texDesc.Width = arg.width;
		texDesc.CPUAccessFlags = translateCPUAccess();
		texDesc.Usage = translateUsage();

		if (arg.generateMipmaps)
		{
			if (arg.usage == Usage::Immutable)
			{
				WarningThrow(true, u8"Unable to generate mipmaps for immutable texture. Changing usage from Immutable to Default.");
				texDesc.Usage = D3D11_USAGE_DEFAULT;
			}
			texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
			texDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
			texDesc.MipLevels = 0;
		}
		else
		{
			texDesc.MiscFlags = 0;
			texDesc.MipLevels = arg.mipmapsCount;
		}

		HRESULT hr;
		ComPtr<ID3D11Texture1D> texTemp = nullptr;
		const unsigned int mipLevelsCount = texDesc.MipLevels != 0 ? texDesc.MipLevels : calculateMipLevelsCount(texDesc.Width);

		if (data.size())
		{
			Vector<D3D11_SUBRESOURCE_DATA> tempVec(arg.arraySize * (texDesc.MipLevels != 0 ? texDesc.MipLevels : calculateMipLevelsCount(texDesc.Width, 1)));
			const unsigned int ptrCount = std::min(data.size(), tempVec.size());

			for (unsigned int x = 0; x < ptrCount; ++x)
			{
				tempVec[x].pSysMem = data[x];
			}

			// MUST be filled with garbage, otherwise D3D11 throws an error.
			for (unsigned int x = ptrCount; x < tempVec.size(); ++x)
			{
				tempVec[x].pSysMem = data[0];
			}

			hr = d3dDevice->CreateTexture1D(&texDesc, tempVec.data(), &texTemp);
		}
		else
		{
			hr = d3dDevice->CreateTexture1D(&texDesc, nullptr, &texTemp);
		}

		if (FAILED(hr))
		{
			WarningThrow(true, u8"Cannot create Texture (1D).");
			return;
		}

		texture = std::move(texTemp);
		desc = arg;
		desc.format = formatFromArg;
		desc.mipmapsCount = mipLevelsCount;

		if (desc.generateMipmaps && desc.usage == Usage::Immutable)
		{
			desc.usage = Usage::Default;
		}
	};
	const auto processTex2D = [&, this]
	{
		D3D11_TEXTURE2D_DESC texDesc;

		if (arg.type == TextureDesc::Type::Texture2DArray)
		{
			texDesc.ArraySize = arg.arraySize;
		}
		else
		{
			texDesc.ArraySize = 1;
		}

		texDesc.BindFlags = translateBinding();
		texDesc.Format = getDXFormat(formatFromArg);
		texDesc.Width = arg.width;
		texDesc.Height = arg.height;
		texDesc.CPUAccessFlags = translateCPUAccess();
		texDesc.Usage = translateUsage();

		if (arg.generateMipmaps)
		{
			if (arg.usage == Usage::Immutable)
			{
				WarningThrow(true, u8"Unable to generate mipmaps for immutable texture. Changing usage from Immutable to Default.");
				texDesc.Usage = D3D11_USAGE_DEFAULT;
			}
			texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
			texDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
			texDesc.MipLevels = 0;
		}
		else
		{
			texDesc.MiscFlags = 0;
			texDesc.MipLevels = arg.mipmapsCount;
		}

		HRESULT hr;

		if (arg.multisamplingLevel > 0)
		{
			UINT sampleQuality = 0;
			hr = d3dDevice->CheckMultisampleQualityLevels(getDXFormat(arg.format), arg.multisamplingLevel, &sampleQuality);
			if (FAILED(hr))
			{
				texDesc.SampleDesc.Count = 1;
				texDesc.SampleDesc.Quality = 0;
			}
			else
			{
				texDesc.SampleDesc.Count = arg.multisamplingLevel;
				texDesc.SampleDesc.Quality = sampleQuality - 1;
			}
		}
		else
		{
			texDesc.SampleDesc.Count = 1;
			texDesc.SampleDesc.Quality = 0;
		}

		ComPtr<ID3D11Texture2D> texTemp = nullptr;
		const unsigned int mipLevelsCount = texDesc.MipLevels != 0 ? texDesc.MipLevels : calculateMipLevelsCount(texDesc.Width, texDesc.Height);

		if (data.size())
		{
			Vector<D3D11_SUBRESOURCE_DATA> tempVec(arg.arraySize * mipLevelsCount);
			const unsigned int ptrCount = std::min(data.size(), tempVec.size());
			unsigned int currMipmap = 0;
			const UInt2 startSize{ arg.width, arg.height };
			UInt2 currSize(startSize);

			for (unsigned int x = 0; x < ptrCount; ++x)
			{
				tempVec[x].pSysMem = data[x];
				tempVec[x].SysMemPitch = tempVec[x].pSysMem ? currSize[0] * arg.format.getSize() : 0;
				++currMipmap;
				if (currMipmap < mipLevelsCount)
				{
					currSize = getNextMipMapSize(currSize);
				}
				else
				{
					currMipmap = 0;
					currSize = startSize;
				}
			}

			// MUST be filled with garbage, otherwise D3D11 throws an error.
			for (unsigned int x = ptrCount; x < tempVec.size(); ++x)
			{
				tempVec[x].pSysMem = data[0];
				tempVec[x].SysMemPitch = tempVec[x].pSysMem ? currSize[0] * arg.format.getSize() : 0;
				++currMipmap;
				if (currMipmap < mipLevelsCount)
				{
					currSize = getNextMipMapSize(currSize);
				}
				else
				{
					currMipmap = 0;
					currSize = startSize;
				}
			}

			hr = d3dDevice->CreateTexture2D(&texDesc, tempVec.data(), &texTemp);
		}
		else
		{
			hr = d3dDevice->CreateTexture2D(&texDesc, nullptr, &texTemp);
		}

		if (FAILED(hr))
		{
			WarningThrow(true, u8"Cannot create Texture (2D).");
			return;
		}

		texture = std::move(texTemp);
		desc = arg;
		desc.format = formatFromArg;
		desc.mipmapsCount = mipLevelsCount;

		if (desc.generateMipmaps && desc.usage == Usage::Immutable)
		{
			desc.usage = Usage::Default;
		}
	};
	const auto processTex3D = [&, this]
	{
		D3D11_TEXTURE3D_DESC texDesc;

		texDesc.BindFlags = translateBinding();
		texDesc.Format = getDXFormat(formatFromArg);
		texDesc.Width = arg.width;
		texDesc.Height = arg.height;
		texDesc.Depth = arg.depth;
		texDesc.CPUAccessFlags = translateCPUAccess();
		texDesc.Usage = translateUsage();

		if (arg.generateMipmaps)
		{
			if (arg.usage == Usage::Immutable)
			{
				WarningThrow(true, u8"Unable to generate mipmaps for immutable texture. Changing usage from Immutable to Default.");
				texDesc.Usage = D3D11_USAGE_DEFAULT;
			}
			texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
			texDesc.MiscFlags = D3D11_RESOURCE_MISC_GENERATE_MIPS;
			texDesc.MipLevels = 0;
		}
		else
		{
			texDesc.MiscFlags = 0;
			texDesc.MipLevels = arg.mipmapsCount;
		}

		HRESULT hr;
		ComPtr<ID3D11Texture3D> texTemp = nullptr;
		const unsigned int mipLevelsCount = texDesc.MipLevels != 0 ? texDesc.MipLevels : calculateMipLevelsCount(texDesc.Width, texDesc.Height, texDesc.Depth);

		if (data.size())
		{
			Vector<D3D11_SUBRESOURCE_DATA> tempVec(arg.arraySize* mipLevelsCount);
			const unsigned int ptrCount = std::min(data.size(), tempVec.size());
			unsigned int currMipmap = 0;
			const UInt3 startSize{ arg.width, arg.height, arg.depth };
			UInt3 currSize(startSize);

			for (unsigned int x = 0; x < ptrCount; ++x)
			{
				tempVec[x].pSysMem = data[x];
				tempVec[x].SysMemPitch = tempVec[x].pSysMem ? currSize[0] * arg.format.getSize() : 0;
				tempVec[x].SysMemSlicePitch = tempVec[x].pSysMem ? currSize[0] * currSize[1] * arg.format.getSize() : 0;
				++currMipmap;
				if (currMipmap < mipLevelsCount)
				{
					currSize = getNextMipMapSize(currSize);
				}
				else
				{
					currMipmap = 0;
					currSize = startSize;
				}
			}

			// MUST be filled with garbage, otherwise D3D11 throws an error.
			for (unsigned int x = ptrCount; x < tempVec.size(); ++x)
			{
				tempVec[x].pSysMem = data[0];
				tempVec[x].SysMemPitch = tempVec[x].pSysMem ? currSize[0] * arg.format.getSize() : 0;
				tempVec[x].SysMemSlicePitch = tempVec[x].pSysMem ? currSize[0] * currSize[1] * arg.format.getSize() : 0;
				++currMipmap;
				if (currMipmap < mipLevelsCount)
				{
					currSize = getNextMipMapSize(currSize);
				}
				else
				{
					currMipmap = 0;
					currSize = startSize;
				}
			}

			hr = d3dDevice->CreateTexture3D(&texDesc, tempVec.data(), &texTemp);
		}
		else
		{
			hr = d3dDevice->CreateTexture3D(&texDesc, nullptr, &texTemp);
		}

		if (FAILED(hr))
		{
			WarningThrow(true, u8"Cannot create Texture (3D).");
			return;
		}

		texture = std::move(texTemp);
		desc = arg;
		desc.format = formatFromArg;
		desc.mipmapsCount = mipLevelsCount;

		if (desc.generateMipmaps && desc.usage == Usage::Immutable)
		{
			desc.usage = Usage::Default;
		}
	};
	const auto processTexCube = [&, this]
	{
		if (arg.arraySize % 6 != 0)
		{
			return;
		}

		D3D11_TEXTURE2D_DESC texDesc;

		texDesc.ArraySize = arg.arraySize;
		texDesc.BindFlags = translateBinding();
		texDesc.Format = getDXFormat(formatFromArg);
		texDesc.Width = arg.width;
		texDesc.Height = arg.height;
		texDesc.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;
		texDesc.CPUAccessFlags = translateCPUAccess();
		texDesc.Usage = translateUsage();

		if (arg.generateMipmaps)
		{
			if (arg.usage == Usage::Immutable)
			{
				WarningThrow(true, u8"Unable to generate mipmaps for immutable texture. Changing usage from Immutable to Default.");
				texDesc.Usage = D3D11_USAGE_DEFAULT;
			}
			texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
			texDesc.MiscFlags |= D3D11_RESOURCE_MISC_GENERATE_MIPS;
			texDesc.MipLevels = 0;
		}
		else
		{
			texDesc.MipLevels = arg.mipmapsCount;
		}

		HRESULT hr;

		if (arg.multisamplingLevel > 0)
		{
			UINT sampleQuality = 0;
			hr = d3dDevice->CheckMultisampleQualityLevels(getDXFormat(arg.format), arg.multisamplingLevel, &sampleQuality);
			if (FAILED(hr))
			{
				texDesc.SampleDesc.Count = 1;
				texDesc.SampleDesc.Quality = 0;
			}
			else
			{
				texDesc.SampleDesc.Count = arg.multisamplingLevel;
				texDesc.SampleDesc.Quality = sampleQuality - 1;
			}
		}
		else
		{
			texDesc.SampleDesc.Count = 1;
			texDesc.SampleDesc.Quality = 0;
		}

		ComPtr<ID3D11Texture2D> texTemp = nullptr;
		const unsigned int mipLevelsCount = texDesc.MipLevels != 0 ? texDesc.MipLevels : calculateMipLevelsCount(texDesc.Width, texDesc.Height);

		if (data.size())
		{
			Vector<D3D11_SUBRESOURCE_DATA> tempVec(arg.arraySize* mipLevelsCount);
			const unsigned int ptrCount = std::min(data.size(), tempVec.size());
			unsigned int currMipmap = 0;
			const UInt2 startSize{ arg.width, arg.height };
			UInt2 currSize(startSize);

			for (unsigned int x = 0; x < ptrCount; ++x)
			{
				tempVec[x].pSysMem = data[x];
				tempVec[x].SysMemPitch = tempVec[x].pSysMem ? currSize[0] * arg.format.getSize() : 0;
				++currMipmap;
				if (currMipmap < mipLevelsCount)
				{
					currSize = getNextMipMapSize(currSize);
				}
				else
				{
					currMipmap = 0;
					currSize = startSize;
				}
			}

			for (unsigned int x = ptrCount; x < tempVec.size(); ++x)
			{
				tempVec[x].pSysMem = data[0];
				tempVec[x].SysMemPitch = tempVec[x].pSysMem ? currSize[0] * arg.format.getSize() : 0;
				++currMipmap;
				if (currMipmap < mipLevelsCount)
				{
					currSize = getNextMipMapSize(currSize);
				}
				else
				{
					currMipmap = 0;
					currSize = startSize;
				}
			}

			hr = d3dDevice->CreateTexture2D(&texDesc, tempVec.data(), &texTemp);
		}
		else
		{
			hr = d3dDevice->CreateTexture2D(&texDesc, nullptr, &texTemp);
		}

		if (FAILED(hr))
		{
			WarningThrow(true, u8"Cannot create Texture (Cube).");
			return;
		}

		texture = std::move(texTemp);
		desc = arg;
		desc.format = formatFromArg;
		desc.mipmapsCount = mipLevelsCount;

		if (desc.generateMipmaps && desc.usage == Usage::Immutable)
		{
			desc.usage = Usage::Default;
		}
	};

	destroy();

	switch (arg.type)
	{
	case TextureDesc::Type::Texture1D:
	case TextureDesc::Type::Texture1DArray:
		processTex1D();
		break;
	case TextureDesc::Type::Texture2D:
	case TextureDesc::Type::Texture2DArray:
		processTex2D();
		break;
	case TextureDesc::Type::Texture3D:
		processTex3D();
		break;
	case TextureDesc::Type::TextureCube:
		processTexCube();
		break;
	}

	if (arg.binding != TextureDesc::Binding::DepthStencil && arg.usage != Usage::Staging)
	{
		srv.create(*this, device);
	}
}

void GLOWE::DirectX11::Texture::destroy()
{
	if (texture)
	{
		srv.destroy();
		texture = nullptr;
		shouldBeMipmapsGenerated = false;

		desc = TextureDesc();
	}
}

bool GLOWE::DirectX11::Texture::checkIsValid() const
{
	return texture != nullptr;
}

const GLOWE::ShaderResourceViewImpl& GLOWE::DirectX11::Texture::getSRV() const
{
	return srv;
}

ID3D11Resource * GLOWE::DirectX11::Texture::getTexture() const
{
	return texture;
}
