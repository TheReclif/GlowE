#pragma once
#ifndef GLOWE_DIRECTX_11_BUFFERIMPLEMENTATION_INCLUDED
#define GLOWE_DIRECTX_11_BUFFERIMPLEMENTATION_INCLUDED

#include "DirectX11Common.h"

#include "../../Uniform/BufferImplementation.h"
#include "../../Uniform/BasicRendererImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT Buffer
			: public BufferImpl
		{
		private:
			ComPtr<ID3D11Buffer> buffer;
		public:
			Buffer() = default;
			Buffer(const BufferDesc& desc, const unsigned long long buffSize, const GraphicsDeviceImpl& device, const void* data = nullptr);
			virtual ~Buffer();

			virtual void create(const BufferDesc& desc, const unsigned long long buffSize, const GraphicsDeviceImpl& device, const void* data = nullptr) override;
			virtual void destroy() override;

			virtual bool checkIsValid() const override;

			virtual void update(BasicRendererImpl& renderer, const void* data, const unsigned long long dataSize = 0, const unsigned long long offset = 0) const override;

			virtual bool checkIsCreated() const override;

			ID3D11Buffer* getBuffer() const;
		};
	}
}

#endif
