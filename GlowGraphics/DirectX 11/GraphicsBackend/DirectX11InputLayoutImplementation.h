#pragma once
#ifndef GLOWE_GRAPHICS_DIRECTX_11_INPUTLAYOUTIMPLEMENTATION_INCLUDED
#define GLOWE_GRAPHICS_DIRECTX_11_INPUTLAYOUTIMPLEMENTATION_INCLUDED

#include "DirectX11Common.h"

#include "../../Uniform/InputLayoutImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT InputLayout
			: public InputLayoutImpl
		{
		private:
			ComPtr<ID3D11InputLayout> inputLayout;
		public:
			InputLayout() = default;
			InputLayout(const InputLayoutDesc& inputDesc, const GraphicsDeviceImpl& device, const VertexShaderImpl& shader);
			virtual ~InputLayout();

			virtual void create(const InputLayoutDesc& inputDesc, const GraphicsDeviceImpl& device, const VertexShaderImpl& shader) override;
			virtual void destroy() override;

			virtual bool checkIsValid() const override;

			ID3D11InputLayout* getInputLayout() const;
		};
	}
}

#endif
