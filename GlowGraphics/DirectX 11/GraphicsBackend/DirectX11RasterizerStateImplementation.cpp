#include "DirectX11RasterizerStateImplementation.h"
#include "DirectX11GraphicsDeviceImplementation.h"

GLOWE::DirectX11::RasterizerState::RasterizerState()
	: RasterizerStateImpl(), rastState(nullptr)
{
}

GLOWE::DirectX11::RasterizerState::RasterizerState(const RasterizerDesc & rastDesc, const GraphicsDeviceImpl& device)
	: RasterizerStateImpl(), rastState(nullptr)
{
	create(rastDesc, device);
}

GLOWE::DirectX11::RasterizerState::~RasterizerState()
{
	destroy();
}

void GLOWE::DirectX11::RasterizerState::create(const RasterizerDesc & arg, const GraphicsDeviceImpl& device)
{
	destroy();

	D3D11_RASTERIZER_DESC d3dDesc;

	d3dDesc.DepthClipEnable = arg.enableDepthClip;
	d3dDesc.ScissorEnable = arg.enableScissor;
	d3dDesc.MultisampleEnable = arg.enableMultisample;
	d3dDesc.AntialiasedLineEnable = arg.enableAntialiasedLines;
	d3dDesc.FrontCounterClockwise = arg.frontCounterClockwise;

	d3dDesc.DepthBias = arg.depthBias;
	d3dDesc.DepthBiasClamp = arg.depthBiasClamp;
	d3dDesc.SlopeScaledDepthBias = arg.slopeScaledDepthBias;

	switch (arg.cullingMode)
	{
	case RasterizerDesc::CullingMode::None:
		d3dDesc.CullMode = D3D11_CULL_NONE;
		break;
	case RasterizerDesc::CullingMode::Front:
		d3dDesc.CullMode = D3D11_CULL_FRONT;
		break;
	case RasterizerDesc::CullingMode::Back:
		d3dDesc.CullMode = D3D11_CULL_BACK;
		break;
	}

	switch (arg.fillMode)
	{
	case RasterizerDesc::FillMode::Solid:
		d3dDesc.FillMode = D3D11_FILL_SOLID;
		break;
	case RasterizerDesc::FillMode::Wireframe:
		d3dDesc.FillMode = D3D11_FILL_WIREFRAME;
		break;
	}

	ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();

	HRESULT hr = d3dDevice->CreateRasterizerState(&d3dDesc, &rastState);
	if (FAILED(hr))
	{
		ErrorThrow(true);
	}

	desc = arg;
	hash = desc.getHash();
}

void GLOWE::DirectX11::RasterizerState::destroy()
{
	if (rastState)
	{
		rastState = nullptr;
		desc = RasterizerDesc();
		hash = Hash();
	}
}

bool GLOWE::DirectX11::RasterizerState::checkIsValid() const
{
	return rastState;
}

ID3D11RasterizerState * GLOWE::DirectX11::RasterizerState::getRastState() const
{
	return rastState;
}
