#include "DirectX11GraphicsDeviceImplementation.h"
#include "../../Uniform/RenderingMgr.h"

#include <dxgi1_4.h>
#include <VersionHelpers.h>

GLOWE::DirectX11::GraphicsDevice::GraphicsDevice()
	: device(nullptr)
{
}

GLOWE::DirectX11::GraphicsDevice::~GraphicsDevice()
{
}

void GLOWE::DirectX11::GraphicsDevice::create(const GraphicsDeviceSettings & desc)
{
	if (device)
	{
		return;
	}

	UINT threadsCount = getCPUCoresCount() - 1;
	UINT deviceFlags = 0;
#ifdef DEBUG
	deviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	if (!desc.useMultithreadedRendering || threadsCount == 0)
	{
		deviceFlags |= D3D11_CREATE_DEVICE_SINGLETHREADED;
	}

	Vector<D3D_FEATURE_LEVEL> requestedFeatures;
	if (IsWindows10OrGreater())
	{
		requestedFeatures.emplace_back(D3D_FEATURE_LEVEL::D3D_FEATURE_LEVEL_12_1);
		requestedFeatures.emplace_back(D3D_FEATURE_LEVEL::D3D_FEATURE_LEVEL_12_0);
	}
	if (IsWindows7SP1OrGreater())
	{
		requestedFeatures.emplace_back(D3D_FEATURE_LEVEL::D3D_FEATURE_LEVEL_11_1);
	}
	requestedFeatures.emplace_back(D3D_FEATURE_LEVEL::D3D_FEATURE_LEVEL_11_0);
	requestedFeatures.emplace_back(D3D_FEATURE_LEVEL::D3D_FEATURE_LEVEL_10_1);
	requestedFeatures.emplace_back(D3D_FEATURE_LEVEL::D3D_FEATURE_LEVEL_10_0);

	HRESULT result = D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, deviceFlags, requestedFeatures.data(), requestedFeatures.size(), D3D11_SDK_VERSION, &device, &chosenFeatureLevel, nullptr);
	if (FAILED(result))
	{
		if (result != E_INVALIDARG)
		{
			ErrorThrow(true);
			return;
		}
		
		result = D3D11CreateDevice(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, deviceFlags, nullptr, 0, D3D11_SDK_VERSION, &device, &chosenFeatureLevel, nullptr);
		if (FAILED(result))
		{
			ErrorThrow(true);
			return;
		}
	}

	ComPtr<IDXGIDevice> dxgiDevice = nullptr;
	result = device->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&dxgiDevice));
	if (FAILED(result))
	{
		ErrorThrow(true);
		return;
	}

	ComPtr<IDXGIAdapter> dxgiAdapter = nullptr;
	result = dxgiDevice->GetParent(__uuidof(IDXGIAdapter), reinterpret_cast<void**>(&dxgiAdapter));
	if (FAILED(result))
	{
		ErrorThrow(true);
		return;
	}
	result = dxgiAdapter->GetDesc(&adapterDesc);
	if (SUCCEEDED(result))
	{
		logMessage("D3D11Device and immediate context created. Adapter/GPU used: "_str + adapterDesc.Description);
		logMessage("Available dedicated video memory: " + toString(adapterDesc.DedicatedVideoMemory) + " B");
	}
	else
	{
		logMessage("D3D11Device and immediate context created. Adapter/GPU description unavailable.");
	}

	defaultRenderer.create(*this);

	getInstance<RenderingMgr>().initialize(defaultRenderer, desc.useMultithreadedRendering);

	usedConfiguration = desc;
}

void GLOWE::DirectX11::GraphicsDevice::destroy()
{
	if (device)
	{
		defaultRenderer.destroy();
	}
}

bool GLOWE::DirectX11::GraphicsDevice::checkIsCreated() const
{
	return device != nullptr;
}

GLOWE::GraphicsAPIInfo GLOWE::DirectX11::GraphicsDevice::getAPIInfo() const
{
	GraphicsAPIInfo result;
	result.usedAPI = GraphicsAPIInfo::GraphicsAPI::DirectX;
	result.versionFirst = 11;
	result.versionSecond = 0;
	return result;
}

GLOWE::BasicRendererImpl& GLOWE::DirectX11::GraphicsDevice::getDefaultRenderer()
{
	return defaultRenderer;
}

const GLOWE::BasicRendererImpl& GLOWE::DirectX11::GraphicsDevice::getDefaultRenderer() const
{
	return defaultRenderer;
}

GLOWE::UInt64 GLOWE::DirectX11::GraphicsDevice::queryAvailableMemory() const
{
	return adapterDesc.DedicatedVideoMemory;
}

GLOWE::UInt64 GLOWE::DirectX11::GraphicsDevice::queryOccupiedMemory() const
{
	ComPtr<IDXGIDevice> dxgiDevice;
	auto result = device->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&dxgiDevice));
	if (FAILED(result))
	{
		return 0;
	}

	ComPtr<IDXGIAdapter3> dxgiAdapter = nullptr;
	result = dxgiDevice->GetParent(__uuidof(IDXGIAdapter3), reinterpret_cast<void**>(&dxgiAdapter));
	if (FAILED(result))
	{
		return 0;
	}

	DXGI_QUERY_VIDEO_MEMORY_INFO memInfo;
	result = dxgiAdapter->QueryVideoMemoryInfo(0, DXGI_MEMORY_SEGMENT_GROUP_LOCAL, &memInfo);
	if (FAILED(result))
	{
		return 0;
	}

	return memInfo.CurrentUsage;
}

ID3D11Device* GLOWE::DirectX11::GraphicsDevice::getDevice() const
{
	return device.p;
}
