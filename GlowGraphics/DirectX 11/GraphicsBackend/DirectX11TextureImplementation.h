#pragma once
#ifndef GLOWE_DIRECTX_11_TEXTUREIMPLEMENTATION_INCLUDED
#define GLOWE_DIRECTX_11_TEXTUREIMPLEMENTATION_INCLUDED

#include "../../Uniform/TextureImplementation.h"

#include "DirectX11Common.h"

#include "DirectX11ShaderResourceViewImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT Texture
			: public TextureImpl
		{
		private:
			ComPtr<ID3D11Resource> texture;
			ShaderResourceView srv;
		public:
			Texture() = default;
			Texture(Texture&& arg) noexcept;
			virtual ~Texture();

			Texture& operator=(Texture&& arg);

			// Remarks:
			// - For texture arrays data will be interpreted as if it was an array of pointers;
			// - Data size cannot be smaller than width (Texture1D), width * height * format.getSize() (Texture2D) or width * height * depth * format.getSize() (Texture3D).
			// - If generateMipmaps option is ticked, the generation does not occur here. The one responsible for it is BasicRenderer (because it encapsulates ID3D11DeviceContext).
			// - For TextureCube arraySize must be 6.
			virtual void create(const TextureDesc& arg, const GraphicsDeviceImpl& device, const Vector<void*> data = Vector<void*>()) override;
			virtual void destroy() override;

			virtual bool checkIsValid() const override;

			virtual const ShaderResourceViewImpl& getSRV() const override;

			ID3D11Resource* getTexture() const;
		};
	}
}

#endif
