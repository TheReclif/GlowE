#pragma once
#ifndef GLOWE_GRAPHICS_DIRECTX_11_BASICRENDERERIMPLEMENTATION_INCLUDED
#define GLOWE_GRAPHICS_DIRECTX_11_BASICRENDERERIMPLEMENTATION_INCLUDED

#include "DirectX11Common.h"

#include "../../Uniform/BasicRendererImplementation.h"
#include "../../Uniform/BlendStateImplementation.h"
#include "../../Uniform/BufferImplementation.h"
#include "../../Uniform/DepthStencilStateImplementation.h"
#include "../../Uniform/InputLayoutImplementation.h"
#include "../../Uniform/RasterizerStateImplementation.h"
#include "../../Uniform/ShaderCollectionImplementation.h"
#include "../../Uniform/ShaderResourceViewImpl.h"
#include "../../Uniform/RenderTargetImplementation.h"
#include "../../Uniform/SamplerImplementation.h"
#include "../../Uniform/UnorderedAccessViewImpl.h"
#include "../../Uniform/TextureImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT BasicRenderer
			: public BasicRendererImpl
		{
		private:
			ComPtr<ID3D11DeviceContext> devCon;
			const void* currentPixelShader, *currentVertexShader, *currentGeometryShader, *currentComputeShader, *currentHullShader, *currentDomainShader;
			const void* cbuffersSet[16], *currentSRVs[16], *currentVertexBuffers[16], *currentUAVs[16], *currentRTs[8];
			const void* currentBlendState, *currentDSState, *currentRastState;
			const void* currentInputLayout, *currentIndexBuffer;
			unsigned int vertexBuffersCount, SRVsCount, UAVsCount, cbuffersCount, RTsCount;
		private:
			virtual void setPrimitiveTopologyImpl(const PrimitiveTopology topology) override;
			virtual void setScissorRectsImpl(const Rect* const rects, const unsigned int size) override;

			virtual void drawImpl(const unsigned int verticesCount, const unsigned int startingVertex = 0) override;
			virtual void drawIndexedImpl(const unsigned int indicesCount, const unsigned int startingIndex = 0, const unsigned int startingVertex = 0) override;
			virtual void drawInstancedImpl(const unsigned int verticesPerInstance, const unsigned int instancesCount, const unsigned int startingVertex = 0, const unsigned int startingInstanceLoc = 0) override;
			virtual void drawIndexedInstancedImpl(const unsigned int indicesPerInstance, const unsigned int instancesCount, const int baseVertexLocation = 0, const unsigned int startingVertex = 0, const unsigned int startingInstanceLoc = 0) override;
		public:
			BasicRenderer() = default;
			BasicRenderer(const GraphicsDeviceImpl& device);
			virtual ~BasicRenderer();

			virtual void create(const GraphicsDeviceImpl& device) override;
			virtual void destroy() override;

			virtual void customFunction(const std::function<void(BasicRendererImpl&)>& func) override;

			virtual void setBlendState(const BlendStateImpl* state) override;

			virtual void setDepthStencilState(const DepthStencilStateImpl* dss) override;
			virtual void setRasterizerState(const RasterizerStateImpl* rs) override;
			virtual void setInputLayout(const InputLayoutImpl* layout) override;

			virtual void copyBuffers(const BufferImpl* src, const BufferImpl* dst) override;
			virtual void updateBuffer(const BufferImpl* buffer, const void* data, const std::size_t dataSize, const std::size_t offset = 0) override;
			virtual void readBuffer(const BufferImpl* buffer, void* data, const std::size_t dataSize, const std::size_t offset = 0) override;

			virtual void copyTextures(const TextureImpl* src, const TextureImpl* dst) override;
			virtual void copyToTexture(const TextureImpl* src, const TextureImpl* dst, const UInt3& dstPos, const UInt3& srcPos, const UInt3& howMany, const unsigned int srcSubresourceId = 0, const unsigned int dstSubresourceId = 0) override;
			virtual void updateTexture(const TextureImpl* src, const void* data, const UInt3& pos, const UInt3& howMany, const unsigned int dstSubresourceId = 0) override;
			virtual void readTexture(const TextureImpl* src, void* data, const UInt3& pos, const UInt3& howMany, const unsigned int srcSubresourceId = 0) override;

			virtual void setVertexBuffer(const BufferImpl* buffer) override;
			virtual void setVertexBuffers(const Vector<const BufferImpl*>& buffers) override;
			virtual void setVertexBuffers(const BufferImpl** buffers, const unsigned int size) override;

			virtual void setIndexBuffer(const BufferImpl* buffer) override;

			virtual void setShaderCollection(const ShaderCollectionImpl* coll) override;

			virtual void setSRV(const ShaderResourceViewImpl* srv, const unsigned int texUnit, const ShaderType stage) override;
			virtual void setSRV(const ShaderResourceViewImpl** srvArr, const unsigned int firstTexUnit, const unsigned int arraySize, const ShaderType stage) override;

			virtual void setSamplers(const SamplerImpl** arr, const unsigned int startSlot, const unsigned int arraySize, const ShaderType stage) override;

			virtual void setConstantBuffers(const BufferImpl* cb, const unsigned int buffSlot, const ShaderType stage) override;
			virtual void setConstantBuffers(const BufferImpl** cb, const unsigned int buffSlot, const unsigned int arraySize, const ShaderType stage) override;

			virtual void setRenderTarget(const RenderTargetImpl* rt) override;
			virtual void setRenderTargetAndUAVs(const RenderTargetImpl* rt, const Vector<const UnorderedAccessViewImpl*>& uavs, const unsigned int uavStartSlot = 0) override;
			virtual void setRenderTargetAndUAVs(const RenderTargetImpl* rt, const UnorderedAccessViewImpl* const * uavs, const unsigned int arraySize, const unsigned int uavStartSlot = 0) override;
			virtual void setUAVs(const UnorderedAccessViewImpl* const* uavs, const unsigned int arraySize, const unsigned int uavStartSlot = 0) override;

			virtual void setViewport(const Viewport& vp) override;
			virtual void setViewports(const Vector<Viewport>& vpsArray) override;
			virtual void setViewports(const Viewport* vpsArray, const unsigned int size) override;

			virtual void clearColor(const RenderTargetImpl* rt, const Color& refreshCol) override; // TODO: Inconsistent with the base class' signature (optional color argument).
			virtual void clearDepthStencil(const RenderTargetImpl* rt, const float refreshDepth = 1.0f, const UInt8 refreshStencil = 0) override;

			virtual void dispatchImpl(const unsigned int x, const unsigned int y, const unsigned int z) override;

			virtual void generateMipmaps(const ShaderResourceViewImpl* tex) override;

			ID3D11DeviceContext* getContext() const;
		};
	}
}

#endif
