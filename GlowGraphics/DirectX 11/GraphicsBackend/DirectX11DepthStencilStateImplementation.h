#pragma once
#ifndef GLOWE_DIRECTX_11_DEPTHSTENCILIMPLEMENTATION_INCLUDED
#define GLOWE_DIRECTX_11_DEPTHSTENCILIMPLEMENTATION_INCLUDED

#include "DirectX11Common.h"

#include "../../../GlowSystem/Utility.h"

#include "../../Uniform/DepthStencilStateImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT DepthStencilState
			: public DepthStencilStateImpl
		{
		private:
			ComPtr<ID3D11DepthStencilState> depthStencilState;
		public:
			DepthStencilState();
			DepthStencilState(const DepthStencilDesc& arg, const GraphicsDeviceImpl& device);
			virtual ~DepthStencilState();

			virtual void create(const DepthStencilDesc& arg, const GraphicsDeviceImpl& device) override;
			virtual void destroy() override;

			virtual bool checkIsValid() const override;

			ID3D11DepthStencilState* getDepthStencilState() const;
		};
	}
}

#endif
