#include "DirectX11Common.h"

#include "DirectX11GraphicsFactory.h"

DXGI_FORMAT getDXFormat(const GLOWE::Format& format)
{
	using Type = GLOWE::Format::Type;
	using GLOWE::Format;

	switch (format.type)
	{
	case Type::Byte:
		if (format.isNormalized)
		{
			switch (format.elemsCount)
			{
			case 1:
				return DXGI_FORMAT::DXGI_FORMAT_R8_SNORM;
			case 2:
				return DXGI_FORMAT::DXGI_FORMAT_R8G8_SNORM;
			case 3: // There is no DXGI_FORMAT_R8G8B8_SNORM, so GlowE must use bigger (in theory) format.
			case 4:
				return DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_SNORM;
			}
		}
		else
		{
			switch (format.elemsCount)
			{
			case 1:
				return DXGI_FORMAT::DXGI_FORMAT_R8_SINT;
			case 2:
				return DXGI_FORMAT::DXGI_FORMAT_R8G8_SINT;
			case 3: // Same as DXGI_FORMAT_R8G8B8A8_SNORM.
			case 4:
				return DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_SINT;
			}
		}
		break;
	case Type::UnsignedByte:
		if (format.isNormalized)
		{
			switch (format.elemsCount)
			{
			case 1:
				return DXGI_FORMAT::DXGI_FORMAT_R8_UNORM;
			case 2:
				return DXGI_FORMAT::DXGI_FORMAT_R8G8_UNORM;
			case 3: // There is no DXGI_FORMAT_R8G8B8_UNORM, so GlowE must use bigger (in theory) format.
			case 4:
				if (format.isSRGB)
				{
					return DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
				}
				else
				{
					return DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM;
				}
			}
		}
		else
		{
			switch (format.elemsCount)
			{
			case 1:
				return DXGI_FORMAT::DXGI_FORMAT_R8_UINT;
			case 2:
				return DXGI_FORMAT::DXGI_FORMAT_R8G8_UINT;
			case 3: // Same as DXGI_FORMAT_R8G8B8A8_UNORM.
			case 4:
				return DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UINT;
			}
		}
		break;
	case Type::Float:
		// Floats ignore the isNormalized attribute.
		switch (format.elemsCount)
		{
		case 1:
			return DXGI_FORMAT::DXGI_FORMAT_R32_FLOAT;
		case 2:
			return DXGI_FORMAT::DXGI_FORMAT_R32G32_FLOAT;
		case 3:
			return DXGI_FORMAT::DXGI_FORMAT_R32G32B32_FLOAT;
		case 4:
			return DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_FLOAT;
		}
		break;
	case Type::HalfFloat:
		// Halfs (half-floats) ignore the isNormalized attribute.
		switch (format.elemsCount)
		{
		case 1:
			return DXGI_FORMAT::DXGI_FORMAT_R16_FLOAT;
		case 2:
			return DXGI_FORMAT::DXGI_FORMAT_R16G16_FLOAT;
		case 3: // Yet again we must use bigger format.
		case 4:
			return DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_FLOAT;
		}
		break;
	case Type::Int:
		// It may surprise you (it surpised me as well), but ints also ignore the isNormalized attribute.
		switch (format.elemsCount)
		{
		case 1:
			return DXGI_FORMAT::DXGI_FORMAT_R32_SINT;
		case 2:
			return DXGI_FORMAT::DXGI_FORMAT_R32G32_SINT;
		case 3:
			return DXGI_FORMAT::DXGI_FORMAT_R32G32B32_SINT;
		case 4:
			return DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_SINT;
		}
		break;
	case Type::UnsignedInt:
		// UInts also ignore the isNormalized attribute;
		switch (format.elemsCount)
		{
		case 1:
			return DXGI_FORMAT::DXGI_FORMAT_R32_UINT;
		case 2:
			return DXGI_FORMAT::DXGI_FORMAT_R32G32_UINT;
		case 3:
			return DXGI_FORMAT::DXGI_FORMAT_R32G32B32_UINT;
		case 4:
			return DXGI_FORMAT::DXGI_FORMAT_R32G32B32A32_UINT;
		}
		break;
	case Type::Short:
		if (format.isNormalized)
		{
			switch (format.elemsCount)
			{
			case 1:
				return DXGI_FORMAT::DXGI_FORMAT_R16_SNORM;
			case 2:
				return DXGI_FORMAT::DXGI_FORMAT_R16G16_SNORM;
			case 3: // There is no DXGI_FORMAT_R16G16B16_SNORM, so GlowE must use bigger (in theory) format.
			case 4:
				return DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_SNORM;
			}
		}
		else
		{
			switch (format.elemsCount)
			{
			case 1:
				return DXGI_FORMAT::DXGI_FORMAT_R16_SINT;
			case 2:
				return DXGI_FORMAT::DXGI_FORMAT_R16G16_SINT;
			case 3: // Same as DXGI_FORMAT_R16G16B16A16_SNORM.
			case 4:
				return DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_SINT;
			}
		}
		break;
	case Type::UnsignedShort:
		if (format.isNormalized)
		{
			switch (format.elemsCount)
			{
			case 1:
				return DXGI_FORMAT::DXGI_FORMAT_R16_UNORM;
			case 2:
				return DXGI_FORMAT::DXGI_FORMAT_R16G16_UNORM;
			case 3: // There is no DXGI_FORMAT_R16G16B16_UNORM, so GlowE must use bigger (in theory) format.
			case 4:
				return DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_UNORM;
			}
		}
		else
		{
			switch (format.elemsCount)
			{
			case 1:
				return DXGI_FORMAT::DXGI_FORMAT_R16_UINT;
			case 2:
				return DXGI_FORMAT::DXGI_FORMAT_R16G16_UINT;
			case 3: // Same as DXGI_FORMAT_R16G16B16A16_UNORM.
			case 4:
				return DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_UINT;
			}
		}
		break;
	default:
		break;
	}

	switch (format.elemsCount)
	{
	case Format::R11B11G10:
	{
		return DXGI_FORMAT::DXGI_FORMAT_R11G11B10_FLOAT;
	}
	case Format::R10B10G10A2:
	{
		if (format.isNormalized)
		{
			return DXGI_FORMAT::DXGI_FORMAT_R10G10B10A2_UNORM;
		}

		return DXGI_FORMAT::DXGI_FORMAT_R10G10B10A2_UINT;
	}
	case Format::D16:
	{
		return DXGI_FORMAT::DXGI_FORMAT_D16_UNORM;
	}
	case Format::D24S8:
	{
		return DXGI_FORMAT::DXGI_FORMAT_D24_UNORM_S8_UINT;
	}
	case Format::D32:
	{
		return DXGI_FORMAT::DXGI_FORMAT_D32_FLOAT;
	}
	case Format::DXT1:
	{
		return DXGI_FORMAT::DXGI_FORMAT_BC1_UNORM;
	}
	case Format::DXT3:
	{
		return DXGI_FORMAT::DXGI_FORMAT_BC3_UNORM;
	}
	case Format::DXT5:
	{
		return DXGI_FORMAT::DXGI_FORMAT_BC5_UNORM;
	}
	}

	return DXGI_FORMAT::DXGI_FORMAT_UNKNOWN;
}

extern "C" GLOWE::GraphicsFactoryImpl * createFactory()
{
	// new for Deletable will use GlowE's allocator and allow the delete operator to deallocate the memory using GlowE's memory manager.
	return new GLOWE::Deletable<GLOWE::DirectX11::GraphicsFactory>();
}
