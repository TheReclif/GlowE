#include "DirectX11UnorderedAccessViewImplementation.h"
#include "DirectX11BufferImplementation.h"
#include "DirectX11GraphicsDeviceImplementation.h"

#include "../../Uniform/BufferImplementation.h"
#include "../../Uniform/TextureImplementation.h"
#include "../../Uniform/GraphicsDeviceImplementation.h"

GLOWE::DirectX11::UnorderedAccessView::~UnorderedAccessView()
{
    destroy();
}

void GLOWE::DirectX11::UnorderedAccessView::create(const BufferImpl& buff, const GraphicsDeviceImpl& device)
{
    if (buff.getDesc().binds != BufferDesc::Binding::UnorderedAccess)
    {
        WarningThrow(true, u8"Invalid buffer bindings (buff.getDesc().binds != BufferDesc::Binding::UnorderedAccess). Cannot create UAV.");
        return;
    }

    destroy();

    ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();

    if (!d3dDevice)
    {
        WarningThrow(true, String(u8"Cannot create buffer UAV. d3dDevice is nullptr."));
        return;
    }

    const HRESULT hr = d3dDevice->CreateUnorderedAccessView(static_cast<const Buffer&>(buff).getBuffer(), nullptr, &uav);
    if (FAILED(hr))
    {
        WarningThrow(true, String(u8"Cannot create buffer UAV."));
    }
}

void GLOWE::DirectX11::UnorderedAccessView::create(const TextureImpl& tex, const GraphicsDeviceImpl& device)
{
    if (tex.getDesc().binding != TextureDesc::Binding::UnorderedAccess)
    {
        WarningThrow(true, u8"Invalid texture bindings (tex.getDesc().binding != TextureDesc::Binding::UnorderedAccess). Cannot create UAV.");
        return;
    }

    destroy();

    ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();

    if (!d3dDevice)
    {
        WarningThrow(true, String(u8"Cannot create texture UAV. d3dDevice is nullptr."));
        return;
    }

    const HRESULT hr = d3dDevice->CreateUnorderedAccessView(static_cast<const Texture&>(tex).getTexture(), nullptr, &uav);
    if (FAILED(hr))
    {
        WarningThrow(true, String(u8"Cannot create texture UAV."));
    }
}

void GLOWE::DirectX11::UnorderedAccessView::destroy()
{
    uav = nullptr;
}

bool GLOWE::DirectX11::UnorderedAccessView::checkIsValid() const
{
    return uav != nullptr;
}

ID3D11UnorderedAccessView* GLOWE::DirectX11::UnorderedAccessView::getUAV() const
{
    return uav;
}
