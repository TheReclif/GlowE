#pragma once
#ifndef GLOWE_GRAPHICS_DIRECTX_11_UAVIMPLEMENTATION_INCLUDED
#define GLOWE_GRAPHICS_DIRECTX_11_UAVIMPLEMENTATION_INCLUDED

#include "../../Uniform/UnorderedAccessViewImpl.h"

#include "DirectX11Common.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT UnorderedAccessView
			: public UnorderedAccessViewImpl
		{
		private:
			ComPtr<ID3D11UnorderedAccessView> uav;
		public:
			UnorderedAccessView() = default;
			virtual ~UnorderedAccessView();

			virtual void create(const BufferImpl& buff, const GraphicsDeviceImpl& device) override;
			virtual void create(const TextureImpl& tex, const GraphicsDeviceImpl& device) override;

			virtual void destroy() override;

			virtual bool checkIsValid() const override;

			ID3D11UnorderedAccessView* getUAV() const;
		};
	}
}

#endif
