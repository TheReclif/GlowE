#include "DirectX11BlendStateImplementation.h"
#include "DirectX11GraphicsDeviceImplementation.h"

GLOWE::DirectX11::BlendState::BlendState()
	: BlendStateImpl(), blendState(nullptr)
{
}

GLOWE::DirectX11::BlendState::BlendState(const BlendDesc & arg, const GraphicsDeviceImpl& device)
	: BlendStateImpl(), blendState(nullptr)
{
	create(arg, device);
}

GLOWE::DirectX11::BlendState::~BlendState()
{
	destroy();
}

void GLOWE::DirectX11::BlendState::create(const BlendDesc & arg, const GraphicsDeviceImpl& device)
{
	destroy();

	using RTBD = BlendDesc::RenderTargetBlendDesc;
	using BlendOption = RTBD::BlendOption;
	using BlendOperation = RTBD::BlendOperation;

	auto glowBlendOptionToD3D = [](const BlendOption option) -> D3D11_BLEND
	{
		switch (option)
		{
		case BlendOption::Zero:
			return D3D11_BLEND_ZERO;
			break;
		case BlendOption::One:
			return D3D11_BLEND_ONE;
			break;
		case BlendOption::SrcColor:
			return D3D11_BLEND_SRC_COLOR;
			break;
		case BlendOption::InvSrcColor:
			return D3D11_BLEND_INV_SRC_COLOR;
			break;
		case BlendOption::SrcAlpha:
			return D3D11_BLEND_SRC_ALPHA;
			break;
		case BlendOption::InvSrcAlpha:
			return D3D11_BLEND_INV_SRC_ALPHA;
			break;
		case BlendOption::DestAlpha:
			return D3D11_BLEND_DEST_ALPHA;
			break;
		case BlendOption::InvDestAlpha:
			return D3D11_BLEND_INV_DEST_ALPHA;
			break;
		case BlendOption::DestColor:
			return D3D11_BLEND_DEST_COLOR;
			break;
		case BlendOption::InvDestColor:
			return D3D11_BLEND_INV_DEST_COLOR;
			break;
		case BlendOption::SrcAlphaClamp:
			return D3D11_BLEND_SRC_ALPHA_SAT;
			break;
		case BlendOption::BlendFactor:
			return D3D11_BLEND_BLEND_FACTOR;
			break;
		case BlendOption::InvBlendFactor:
			return D3D11_BLEND_INV_BLEND_FACTOR;
			break;
		case BlendOption::Src1Color:
			return D3D11_BLEND_SRC1_COLOR;
			break;
		case BlendOption::InvSrc1Color:
			return D3D11_BLEND_INV_SRC1_COLOR;
			break;
		case BlendOption::Src1Alpha:
			return D3D11_BLEND_SRC1_ALPHA;
			break;
		case BlendOption::InvSrc1Alpha:
			return D3D11_BLEND_INV_SRC1_ALPHA;
			break;
		default:
			return D3D11_BLEND_ONE;
			break;
		}
	};
	auto glowBlendOperationToD3D = [](const BlendOperation operation) -> D3D11_BLEND_OP
	{
		switch (operation)
		{
		case BlendOperation::Add:
			return D3D11_BLEND_OP_ADD;
			break;
		case BlendOperation::Sub:
			return D3D11_BLEND_OP_SUBTRACT;
			break;
		case BlendOperation::RevSub:
			return D3D11_BLEND_OP_REV_SUBTRACT;
			break;
		case BlendOperation::Min:
			return D3D11_BLEND_OP_MIN;
			break;
		case BlendOperation::Max:
			return D3D11_BLEND_OP_MAX;
			break;
		default:
			return D3D11_BLEND_OP_ADD;
			break;
		}
	};

	D3D11_BLEND_DESC d3dBlendDesc;
	d3dBlendDesc.AlphaToCoverageEnable = arg.enableAlphaToCoverage;
	d3dBlendDesc.IndependentBlendEnable = arg.enableIndependentBlend;

	for (UInt x = 0; x < 8; ++x)
	{
		D3D11_RENDER_TARGET_BLEND_DESC& d3dRenderTargetBlendDesc = d3dBlendDesc.RenderTarget[x];
		const RTBD& glowRenderTargetBlendDesc = arg.renderTargetDescs[x];

		d3dRenderTargetBlendDesc.BlendEnable = glowRenderTargetBlendDesc.enableBlend;
		d3dRenderTargetBlendDesc.RenderTargetWriteMask = glowRenderTargetBlendDesc.writeMask;

		d3dRenderTargetBlendDesc.SrcBlend = glowBlendOptionToD3D(glowRenderTargetBlendDesc.srcBlend);
		d3dRenderTargetBlendDesc.DestBlend = glowBlendOptionToD3D(glowRenderTargetBlendDesc.destBlend);
		d3dRenderTargetBlendDesc.BlendOp = glowBlendOperationToD3D(glowRenderTargetBlendDesc.blendOperation);
		d3dRenderTargetBlendDesc.SrcBlendAlpha = glowBlendOptionToD3D(glowRenderTargetBlendDesc.srcBlendAlpha);
		d3dRenderTargetBlendDesc.DestBlendAlpha = glowBlendOptionToD3D(glowRenderTargetBlendDesc.destBlendAlpha);
		d3dRenderTargetBlendDesc.BlendOpAlpha = glowBlendOperationToD3D(glowRenderTargetBlendDesc.blendOperationAlpha);
	}

	ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();

	HRESULT hr = d3dDevice->CreateBlendState(&d3dBlendDesc, &blendState);
	if (FAILED(hr))
	{
		ErrorThrow(true);
	}

	desc = arg;
	hash = arg.getHash();
}

void GLOWE::DirectX11::BlendState::destroy()
{
	if (blendState)
	{
		blendState = nullptr;
		desc = BlendDesc();
		hash = Hash();
	}
}

bool GLOWE::DirectX11::BlendState::checkIsValid() const
{
	return blendState;
}

ID3D11BlendState * GLOWE::DirectX11::BlendState::getBlendState() const
{
	return blendState;
}
