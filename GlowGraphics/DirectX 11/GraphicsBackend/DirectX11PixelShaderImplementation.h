#pragma once
#ifndef GLOWE_DIRECTX_11_PIXELSHADERIMPLEMENTATION_INCLUDED
#define GLOWE_DIRECTX_11_PIXELSHADERIMPLEMENTATION_INCLUDED

#include "DirectX11Common.h"

#include "../../Uniform/ShaderImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT PixelShader
			: public PixelShaderImpl
		{
		private:
			ComPtr<ID3D11PixelShader> shader;
			ComPtr<ID3D11Blob> byteCode;
		public:
			PixelShader();
			virtual ~PixelShader();

			virtual void compileFromCode(const String& code, const GraphicsDeviceImpl& device, const CompilationData& compilationData = CompilationData()) override;
			virtual void destroy() override;

			virtual bool checkIsCreated() const override;

			ID3DBlob* getBytecode() const;
			ID3D11PixelShader* getShader() const;

			void fromByteCode(const void* bytes, const UInt32 size, const GraphicsDeviceImpl& device);
		};
	}
}

#endif
