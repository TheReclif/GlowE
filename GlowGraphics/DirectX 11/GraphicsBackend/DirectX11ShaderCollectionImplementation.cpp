#include "DirectX11ShaderCollectionImplementation.h"

#include "DirectX11ComputeShaderImplementation.h"
#include "DirectX11GeometryShaderImplementation.h"
#include "DirectX11VertexShaderImplementation.h"
#include "DirectX11PixelShaderImplementation.h"

GLOWE::DirectX11::ShaderCollection::ShaderCollection(const ShaderCollectionDesc & desc)
	: ShaderCollection()
{
	create(desc);
}

GLOWE::DirectX11::ShaderCollection::~ShaderCollection()
{
	destroy();
}

void GLOWE::DirectX11::ShaderCollection::create(const ShaderCollectionDesc& desc)
{
	destroy();

	if (desc.vertexShader)
	{
		vertexShader = static_cast<const VertexShader&>(*desc.vertexShader).getShader();
		GlowAssert(vertexShader != nullptr);
	}
	if (desc.computeShader)
	{
		computeShader = static_cast<const ComputeShader&>(*desc.computeShader).getShader();
		GlowAssert(computeShader != nullptr);
	}
	if (desc.pixelShader)
	{
		pixelShader = static_cast<const PixelShader&>(*desc.pixelShader).getShader();
		GlowAssert(pixelShader != nullptr);
	}
	if (desc.geometryShader)
	{
		geometryShader = static_cast<const GeometryShader&>(*desc.geometryShader).getShader();
		GlowAssert(geometryShader != nullptr);
	}

	usedDesc = desc;
	isLinked = true;
}

void GLOWE::DirectX11::ShaderCollection::destroy()
{
	if (isLinked)
	{
		vertexShader = nullptr;
		pixelShader = nullptr;
		geometryShader = nullptr;
		computeShader = nullptr;
		hullShader = nullptr;
		domainShader = nullptr;

		usedDesc = ShaderCollectionDesc();
		isLinked = false;
	}
}

ID3D11VertexShader * GLOWE::DirectX11::ShaderCollection::getVertexShader() const
{
	return vertexShader;
}

ID3D11PixelShader * GLOWE::DirectX11::ShaderCollection::getPixelShader() const
{
	return pixelShader;
}

ID3D11GeometryShader * GLOWE::DirectX11::ShaderCollection::getGeometryShader() const
{
	return geometryShader;
}

ID3D11ComputeShader * GLOWE::DirectX11::ShaderCollection::getComputeShader() const
{
	return computeShader;
}

ID3D11HullShader * GLOWE::DirectX11::ShaderCollection::getHullShader() const
{
	return hullShader;
}

ID3D11DomainShader * GLOWE::DirectX11::ShaderCollection::getDomainShader() const
{
	return domainShader;
}

const GLOWE::ShaderCollectionDesc& GLOWE::DirectX11::ShaderCollection::getDesc() const
{
	return usedDesc;
}
