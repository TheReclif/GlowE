#include "DirectX11SamplerImplementation.h"
#include "DirectX11GraphicsDeviceImplementation.h"

GLOWE::DirectX11::Sampler::Sampler()
	: SamplerImpl(), samplerState(nullptr)
{
}

GLOWE::DirectX11::Sampler::Sampler(const SamplerDesc & arg, const GraphicsDeviceImpl& device)
	: SamplerImpl(), samplerState(nullptr)
{
	create(arg, device);
}

GLOWE::DirectX11::Sampler::~Sampler()
{
	destroy();
}

void GLOWE::DirectX11::Sampler::create(const SamplerDesc& desc, const GraphicsDeviceImpl& device)
{
	destroy();

	D3D11_SAMPLER_DESC d3dDesc;

	using CompFunc = ComparisonFunction;

	d3dDesc.MaxAnisotropy = desc.anisotropyLevel;
	const Color& colRef = desc.borderColor;
	d3dDesc.BorderColor[0] = colRef.rgba[0];
	d3dDesc.BorderColor[1] = colRef.rgba[1];
	d3dDesc.BorderColor[2] = colRef.rgba[2];
	d3dDesc.BorderColor[3] = colRef.rgba[3];
	
	auto addressModeTranslate = [](const SamplerDesc::TextureAddressingMode arg) -> D3D11_TEXTURE_ADDRESS_MODE
	{
		switch (arg)
		{
		case SamplerDesc::TextureAddressingMode::Wrap:
			return D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_WRAP;
			break;
		case SamplerDesc::TextureAddressingMode::Clamp:
			return D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_CLAMP;
			break;
		case SamplerDesc::TextureAddressingMode::Border:
			return D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_BORDER;
			break;
		case SamplerDesc::TextureAddressingMode::Mirror:
			return D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_MIRROR;
			break;
		case SamplerDesc::TextureAddressingMode::MirrorOnce:
			return D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_MIRROR_ONCE;
			break;
		default:
			return D3D11_TEXTURE_ADDRESS_MODE::D3D11_TEXTURE_ADDRESS_WRAP;
			break;
		}
	};
	auto compFuncTranslate = [](const CompFunc arg) -> D3D11_COMPARISON_FUNC
	{
		switch (arg)
		{
		case CompFunc::Never:
			return D3D11_COMPARISON_NEVER;
			break;
		case CompFunc::Less:
			return D3D11_COMPARISON_LESS;
			break;
		case CompFunc::Equal:
			return D3D11_COMPARISON_EQUAL;
			break;
		case CompFunc::LessEqual:
			return D3D11_COMPARISON_LESS_EQUAL;
			break;
		case CompFunc::Greater:
			return D3D11_COMPARISON_GREATER;
			break;
		case CompFunc::NotEqual:
			return D3D11_COMPARISON_NOT_EQUAL;
			break;
		case CompFunc::GreaterEqual:
			return D3D11_COMPARISON_GREATER_EQUAL;
			break;
		case CompFunc::Always:
			return D3D11_COMPARISON_ALWAYS;
			break;
		default:
			return D3D11_COMPARISON_LESS;
			break;
		}
	};
	auto filterTranslate = [&desc]() -> D3D11_FILTER
	{
		constexpr unsigned int compMask = 0x80;
		unsigned int result{};

		switch (desc.mipFilter)
		{
		case SamplerDesc::Filter::Linear:
			switch (desc.magFilter)
			{
			case SamplerDesc::Filter::Linear:
				switch (desc.minFilter)
				{
				case SamplerDesc::Filter::Linear:
					result = D3D11_FILTER::D3D11_FILTER_MIN_MAG_MIP_LINEAR;
					break;
				case SamplerDesc::Filter::Nearest:
					result = D3D11_FILTER::D3D11_FILTER_MIN_POINT_MAG_MIP_LINEAR;
					break;
				}
				break;
			case SamplerDesc::Filter::Nearest:
				switch (desc.minFilter)
				{
				case SamplerDesc::Filter::Linear:
					result = D3D11_FILTER::D3D11_FILTER_MIN_LINEAR_MAG_POINT_MIP_LINEAR;
					break;
				case SamplerDesc::Filter::Nearest:
					result = D3D11_FILTER::D3D11_FILTER_MIN_MAG_POINT_MIP_LINEAR;
					break;
				}
				break;
			}
			break;
		case SamplerDesc::Filter::Nearest:
			switch (desc.magFilter)
			{
			case SamplerDesc::Filter::Linear:
				switch (desc.minFilter)
				{
				case SamplerDesc::Filter::Linear:
					result = D3D11_FILTER::D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;
					break;
				case SamplerDesc::Filter::Nearest:
					result = D3D11_FILTER::D3D11_FILTER_MIN_POINT_MAG_LINEAR_MIP_POINT;
					break;
				}
				break;
			case SamplerDesc::Filter::Nearest:
				switch (desc.minFilter)
				{
				case SamplerDesc::Filter::Linear:
					result = D3D11_FILTER::D3D11_FILTER_MIN_LINEAR_MAG_MIP_POINT;
					break;
				case SamplerDesc::Filter::Nearest:
					result = D3D11_FILTER::D3D11_FILTER_MIN_MAG_MIP_POINT;
					break;
				}
				break;
			}
			break;
		}

		if (desc.anisotropyLevel > 1)
		{
			result = D3D11_FILTER::D3D11_FILTER_ANISOTROPIC;
		}

		if (desc.enableComparison)
		{
			result |= compMask;
		}

		return (D3D11_FILTER)result;
	};

	d3dDesc.AddressU = addressModeTranslate(desc.addressU);
	d3dDesc.AddressV = addressModeTranslate(desc.addressV);
	d3dDesc.AddressW = addressModeTranslate(desc.addressW);

	d3dDesc.ComparisonFunc = compFuncTranslate(desc.compFunc);

	d3dDesc.MaxLOD = desc.maxLOD;
	d3dDesc.MinLOD = desc.minLOD;
	d3dDesc.MipLODBias = desc.mipBiasLOD;

	d3dDesc.Filter = filterTranslate();

	ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();

	HRESULT hr = d3dDevice->CreateSamplerState(&d3dDesc, &samplerState);
	if (FAILED(hr))
	{
		ErrorThrow(true);
	}

	this->desc = desc;
	hash = desc.getHash();
}

void GLOWE::DirectX11::Sampler::destroy()
{
	if (samplerState)
	{
		samplerState = nullptr;
		desc = SamplerDesc();
		hash = Hash();
	}
}

bool GLOWE::DirectX11::Sampler::checkIsValid() const
{
	return samplerState;
}

ID3D11SamplerState * GLOWE::DirectX11::Sampler::getSampler() const
{
	return samplerState;
}
