#pragma once
#ifndef GLOWE_DIRECTX_11_SAMPLERIMPLEMENTATION_INCLUDED
#define GLOWE_DIRECTX_11_SAMPLERIMPLEMENTATION_INCLUDED

#include "DirectX11Common.h"

#include "../../Uniform/SamplerImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT Sampler
			: public SamplerImpl
		{
		private:
			ComPtr<ID3D11SamplerState> samplerState;
		public:
			Sampler();
			Sampler(const SamplerDesc& arg, const GraphicsDeviceImpl& device);
			virtual ~Sampler();

			virtual void create(const SamplerDesc& desc, const GraphicsDeviceImpl& device) override;
			virtual void destroy() override;

			virtual bool checkIsValid() const override;

			ID3D11SamplerState* getSampler() const;
		};
	}
}

#endif

