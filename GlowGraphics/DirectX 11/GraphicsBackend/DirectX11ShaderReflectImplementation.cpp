#include "DirectX11ShaderReflectImplementation.h"

#include "DirectX11ComputeShaderImplementation.h"
#include "DirectX11GeometryShaderImplementation.h"
#include "DirectX11VertexShaderImplementation.h"
#include "DirectX11PixelShaderImplementation.h"

void GLOWE::DirectX11::ShaderReflect::init(ID3DBlob * blob)
{
	if (blob)
	{
		D3D11_SHADER_DESC desc;
		ComPtr<ID3D11ShaderReflection> reflection = nullptr;

		HRESULT hr = D3DReflect(blob->GetBufferPointer(), blob->GetBufferSize(), __uuidof(ID3D11ShaderReflection), (void**)&reflection);
		if (FAILED(hr))
		{
			WarningThrow(true, String(u8"Unable to reflect shader (unable to get shader info)."));
			return;
		}

		hr = reflection->GetDesc(&desc);
		if (FAILED(hr))
		{
			WarningThrow(true, String(u8"Unable to get shader desc."));
			return;
		}

		resources.resize(desc.BoundResources);
		cBuffers.resize(desc.ConstantBuffers);
		nameToElem.clear();
		textureCount = 0;
		bufferCount = 0;
		
		for (unsigned int x = 0; x < desc.BoundResources; ++x)
		{
			reflection->GetResourceBindingDesc(x, &resources[x]);
			const D3D11_SHADER_INPUT_BIND_DESC& currBind = resources[x];
			nameToElem.insert(std::make_pair(String(currBind.Name), currBind.BindPoint));

			if (currBind.Type == D3D_SHADER_INPUT_TYPE::D3D_SIT_STRUCTURED)
			{
				bufferCount += currBind.BindCount;
			}
			else if (currBind.Type == D3D_SHADER_INPUT_TYPE::D3D_SIT_TEXTURE || currBind.Type == D3D_SHADER_INPUT_TYPE::D3D_SIT_TBUFFER)
			{
				textureCount += currBind.BindCount;
			}
		}

		for (unsigned int x = 0; x < desc.ConstantBuffers; ++x)
		{
			ID3D11ShaderReflectionConstantBuffer* temp = reflection->GetConstantBufferByIndex(x);
			if (temp)
			{
				temp->GetDesc(&cBuffers[x]);
				nameToElem.insert(std::make_pair(String(cBuffers[x].Name), x));
			}
		}
	}
}

void GLOWE::DirectX11::ShaderReflect::create(const ComputeShaderImpl & arg)
{
	init(static_cast<const ComputeShader&>(arg).getBytecode());
}

void GLOWE::DirectX11::ShaderReflect::create(const GeometryShaderImpl& arg)
{
	init(static_cast<const GeometryShader&>(arg).getBytecode());
}

void GLOWE::DirectX11::ShaderReflect::create(const PixelShaderImpl & arg)
{
	init(static_cast<const PixelShader&>(arg).getBytecode());
}

void GLOWE::DirectX11::ShaderReflect::create(const VertexShaderImpl & arg)
{
	init(static_cast<const VertexShader&>(arg).getBytecode());
}

unsigned int GLOWE::DirectX11::ShaderReflect::getResourcesCount() const
{
	return resources.size();
}

unsigned int GLOWE::DirectX11::ShaderReflect::getSRVCount() const
{
	return textureCount + bufferCount;
}

unsigned int GLOWE::DirectX11::ShaderReflect::getTexCount() const
{
	return textureCount;
}

unsigned int GLOWE::DirectX11::ShaderReflect::getCBufferCount() const
{
	return cBuffers.size();
}

unsigned int GLOWE::DirectX11::ShaderReflect::getBuffCount() const
{
	return bufferCount;
}

GLOWE::String GLOWE::DirectX11::ShaderReflect::getResourceName(const unsigned int id) const
{
	return resources[id].Name;
}

GLOWE::String GLOWE::DirectX11::ShaderReflect::getCBufferName(const unsigned int id) const
{
	return cBuffers[id].Name;
}

unsigned int GLOWE::DirectX11::ShaderReflect::getResourceID(const String & name) const
{
	return nameToElem.at(name);
}

unsigned int GLOWE::DirectX11::ShaderReflect::getCBufferID(const String & name) const
{
	return nameToElem.at(name);
}
