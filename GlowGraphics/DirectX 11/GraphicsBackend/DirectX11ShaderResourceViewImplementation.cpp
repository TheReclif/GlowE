#include "DirectX11ShaderResourceViewImplementation.h"
#include "DirectX11BufferImplementation.h"
#include "DirectX11TextureImplementation.h"

#include "DirectX11GraphicsDeviceImplementation.h"

GLOWE::DirectX11::ShaderResourceView::~ShaderResourceView()
{
	destroy();
}

void GLOWE::DirectX11::ShaderResourceView::create(const BufferImpl & buff, const GraphicsDeviceImpl& device)
{
	if (buff.getDesc().binds != BufferDesc::Binding::ShaderResource)
	{
		WarningThrow(true, u8"Invalid buffer bindings (buff.getDesc().binds != BufferDesc::Binding::ShaderResource). Cannot create SRV.");
		return;
	}

	destroy();

	ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();
	if (!d3dDevice)
	{
		WarningThrow(true, String(u8"Cannot create buffer SRV. d3dDevice is nullptr."));
		return;
	}

	HRESULT hr = d3dDevice->CreateShaderResourceView(static_cast<const Buffer&>(buff).getBuffer(), nullptr, &srv);
	if (FAILED(hr))
	{
		WarningThrow(true, String(u8"Cannot create buffer SRV."));
		return;
	}
}

void GLOWE::DirectX11::ShaderResourceView::create(const TextureImpl & tex, const GraphicsDeviceImpl& device)
{
	const TextureDesc& texDesc = tex.getDesc();

	if (texDesc.binding == TextureDesc::Binding::DepthStencil)
	{
		WarningThrow(true, String(u8"Unable to create a SRV for Binding::DepthStencil."));
		return;
	}

	destroy();

	ID3D11Device* d3dDevice = static_cast<const DirectX11::GraphicsDevice&>(device).getDevice();
	if (!d3dDevice)
	{
		WarningThrow(true, String(u8"Cannot create texture SRV. d3dDevice is nullptr."));
		return;
	}

	HRESULT hr = d3dDevice->CreateShaderResourceView(static_cast<const Texture&>(tex).getTexture(), nullptr, &srv);
	if (FAILED(hr))
	{
		WarningThrow(true, String(u8"Cannot create texture SRV."));
		return;
	}
}

void GLOWE::DirectX11::ShaderResourceView::destroy()
{
	srv = nullptr;
}

bool GLOWE::DirectX11::ShaderResourceView::checkIsValid() const
{
	return srv != nullptr;
}

ID3D11ShaderResourceView * GLOWE::DirectX11::ShaderResourceView::getSRV() const
{
	return srv;
}
