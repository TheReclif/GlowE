#pragma once
#ifndef GLOWE_DIRECTX11_SWAPCHAIN_INCLUDED
#define GLOWE_DIRECTX11_SWAPCHAIN_INCLUDED

#include "DirectX11GraphicsDeviceImplementation.h"
#include "DirectX11RenderTargetImplementation.h"

#include "../../Uniform/SwapChainImplementation.h"

namespace GLOWE
{
	namespace DirectX11
	{
		class GLOWEDIRECT3D11RENDERBACKEND_EXPORT SwapChain
			: public GLOWE::SwapChain
		{
		private:
			ComPtr<IDXGISwapChain> swapChain = nullptr;
			GraphicsDevice* parentDevice = nullptr;
			WindowHandle associatedWindow;
			Texture defaultDepthStencilTexture;
			RenderTarget defaultRenderTarget;
		private:
			void changeMSAAState(const unsigned int msaaLevel);
			void create(const SwapChainDesc& desc, const WindowHandle window, GraphicsDeviceImpl& device);
		protected:
			virtual void resizeBuffers(const VideoMode& videoMode) override;
		public:
			virtual ~SwapChain() override;

			virtual void create(const SwapChainDesc& desc, const WindowImplementation* const window, GraphicsDeviceImpl& device) override;
			virtual void create(const SwapChainDesc& desc, const Window& window, GraphicsDeviceImpl& device) override;
			virtual void destroy() override;

			virtual void setPresentInterval(const unsigned int vsync) override;

			virtual RenderTargetImpl& getRenderTarget() override;
			virtual const RenderTargetImpl& getRenderTarget() const override;

			virtual void present() const override;

			virtual bool checkIsValid() const override;
		};
	}
}

#endif
