#pragma once
#ifndef GLOWE_LINUX_MOCKS_INCLUDED
#define GLOWE_LINUX_MOCKS_INCLUDED

#include "../Uniform/WindowImplementation.h"
#include "../Uniform/InputImplementation.h"

namespace GLOWE
{
	namespace Linux
	{
		class GLOWE_DLLEXPORT LinuxWindowImplementation
			: public WindowImplementation
		{
		public:
			virtual void create(const VideoMode& size, const String& title, const Int2& position = Int2{ 0,0 }, const WindowMode& wndMode = WindowMode::Windowed) override;
			virtual void destroy() override;
			virtual void resize(const VideoMode& newSize) override;
			virtual void resize(const VideoMode& newSize, const WindowMode& newMode) override;
			virtual VideoMode getSize() const override;
			virtual void setPosition(const Int2& newPos) override;
			virtual Int2 getPosition() const override;
			virtual void setMode(const WindowMode& newMode) override;
			virtual WindowMode getMode() const override;
			virtual String getTitle() const override;
			virtual void setTitle(const String& newTitle) override;
			virtual void setKeyRepeat(bool arg) override;
			virtual void grabCursor() override;
			virtual void ungrabCursor() override;
			virtual void setCursorGrab(bool arg) override;
			virtual void showCursor() override;
			virtual void hideCursor() override;
			virtual void requestFocus() override;
			virtual void processEvents() override;
			virtual bool checkIsKeyRepeatEnabled() const override;
			virtual bool checkIsCreated() const override;
			virtual bool checkHasFocus() const override;
			virtual void setIsResizableImpl() override;
			virtual bool checkIsDuringMoveSize() const override;
			virtual void display() override;
			virtual WindowHandle getWindowHandle() override;
		};

		class GLOWE_DLLEXPORT LinuxInputImplementation
			: public InputImplementation, public Subsystem
		{
		public:
			virtual void updateJoysticks() override;

			virtual Joystick::Description getJoystickDescription(Joystick::JoystickId joystickId) const override;

			virtual bool isKeyDown(Keyboard::Key keycode) const override;

			virtual bool isMouseButtonDown(Mouse::Button buttoncode) const override;

			virtual bool isJoystickButtonDown(Joystick::JoystickId joystickId, Joystick::JoystickId buttonId) const override;

			virtual bool joystickHasAxis(Joystick::JoystickId joystickId, Joystick::Axis axis) const override;

			virtual bool isJoystickConnected(Joystick::JoystickId joystickId) override;

			virtual unsigned int getJoystickButtonsCount(Joystick::JoystickId joystickId) const override;

			virtual Int2 getMousePosition() const override;

			virtual Int2 getMousePosition(Window& relativeTo) const override;

			virtual float getJoystickAxisPosition(Joystick::JoystickId joystickId, Joystick::Axis axis) const override;

			virtual void setMousePosition(const Int2& newPos) override;

			virtual void setMousePosition(const Int2& newPos, Window& relativeTo) override;

			virtual unsigned int getJoysticksCount() const override;

		};
	}
}

#endif
