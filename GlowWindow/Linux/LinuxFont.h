#pragma once
#ifndef LINUX_FONT_INCLUDED
#define LINUX_FONT_INCLUDED

#include "../../GlowSystem/Utility.h"
#if defined(GLOWE_COMPILE_FOR_LINUX)
#include <xcb/xcb.h>
#include <xcb/xcb_aux.h>
#include <xcb/xcb_image.h>
#include <xcb/xcb_atom.h>
#include <xcb/xcb_icccm.h>
#include "../../GlowSystem/String.h"

namespace GLOWE
{
	namespace Linux
	{
		class GLOWE_DLLEXPORT __Font
		{
		private:
			xcb_gcontext_t gc;
			xcb_connection_t* attachedConnection;

			bool isCreated;
		public:
			__Font();
			~__Font();

			void create(const String& fontName, xcb_connection_t* connection, xcb_screen_t* screen, xcb_window_t window);
			void destroy();

			xcb_gcontext_t getGC();
		};
	}
}

#endif

#endif
