#pragma once
#ifndef LINUX_MESSAGEBOX_INCLUDED
#define LINUX_MESSAGEBOX_INCLUDED

#include "../../GlowSystem/Utility.h"
#if defined(GLOWE_COMPILE_FOR_LINUX)

#include "LinuxConnection.h"
#include "LinuxFont.h"

#include "LinuxErrorIcon.h"
#include "LinuxWarningIcon.h"

#include "../Uniform/MessageBox.h"

namespace GLOWE
{
	namespace Linux
	{
		static const struct {
			unsigned int 	 width;
			unsigned int 	 height;
			unsigned int 	 bytes_per_pixel;
			unsigned char	 pixel_data[1 * 30 * 4 + 1];
		} BUTTON_NOT_PRESSED = {
			1, 30, 4,
			"\325\325\325\377\325\325\325\377\325\325\325\377\325\325\325\377\325\325"
			"\325\377\325\325\325\377\325\325\325\377\325\325\325\377\325\325\325\377"
			"\325\325\325\377\325\325\325\377\325\325\325\377\325\325\325\377\325\325"
			"\325\377\325\325\325\377\325\325\325\377\325\325\325\377\325\325\325\377"
			"\325\325\325\377\325\325\325\377\325\325\325\377\325\325\325\377\325\325"
			"\325\377\325\325\325\377\325\325\325\377\325\325\325\377\325\325\325\377"
			"\325\325\325\377\325\325\325\377\325\325\325\377",
		};

		static const struct {
			unsigned int 	 width;
			unsigned int 	 height;
			unsigned int 	 bytes_per_pixel;
			unsigned char	 pixel_data[1 * 30 * 4 + 1];
		} BUTTON_PRESSED = {
			1, 30, 4,
			"\246\246\246\377\246\246\246\377\246\246\246\377\246\246\246\377\246\246"
			"\246\377\246\246\246\377\246\246\246\377\246\246\246\377\246\246\246\377"
			"\246\246\246\377\246\246\246\377\246\246\246\377\246\246\246\377\246\246"
			"\246\377\246\246\246\377\246\246\246\377\246\246\246\377\246\246\246\377"
			"\246\246\246\377\246\246\246\377\246\246\246\377\246\246\246\377\246\246"
			"\246\377\246\246\246\377\246\246\246\377\246\246\246\377\246\246\246\377"
			"\246\246\246\377\246\246\246\377\246\246\246\377",
		};

		struct __LinuxWindowDesc
		{
			unsigned int x, y, width, height;
		};

		class GLOWE_DLLEXPORT __SimpleWindow
		{
		private:
			__Connection* attachedConnection;

			xcb_window_t window;
			xcb_gcontext_t gc;

			bool isCreated;
		public:
			__SimpleWindow();
			~__SimpleWindow();

			void changeTitle(const String& newTitle);

			void create(const String& winName, __LinuxWindowDesc desc, __Connection* connection);
			void destroy();

			xcb_window_t getWindow();
			xcb_gcontext_t getGC();

			bool checkIsCreated() const;
		};

		class GLOWE_DLLEXPORT __MsgBoxInfo
		{
		private:
			__SimpleWindow window;

			static const unsigned int msgBoxWidth = 450;
			static const unsigned int msgBoxHeight = 180;
		public:
			void show(const String& message, const short& type);
		};

		void showMessageBox(const String& text, short style);
	}
}

#endif

#endif
