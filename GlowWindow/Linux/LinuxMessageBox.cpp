#include "LinuxMessageBox.h"

#if defined(GLOWE_COMPILE_FOR_LINUX)
#include "../../GlowSystem/Error.h"
GLOWE::Linux::__SimpleWindow::__SimpleWindow()
	: attachedConnection(nullptr), isCreated(false)
{
}

GLOWE::Linux::__SimpleWindow::~__SimpleWindow()
{
	this->destroy();
}

void GLOWE::Linux::__SimpleWindow::changeTitle(const String & newTitle)
{
	if (isCreated)
	{
		xcb_change_property(attachedConnection->getConnection(), XCB_PROP_MODE_REPLACE, window, XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8, newTitle.getSize(), newTitle.getCharArray());
	}
}

void GLOWE::Linux::__SimpleWindow::create(const String & winName, __LinuxWindowDesc desc, __Connection * connection)
{
	if (isCreated)
	{
		return;
	}

	uint32_t values[] = {
		0xc0c0c0,
		XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_KEY_PRESS |
		XCB_EVENT_MASK_KEY_RELEASE | XCB_EVENT_MASK_BUTTON_PRESS |
		XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_ENTER_WINDOW |
		XCB_EVENT_MASK_LEAVE_WINDOW,
		0, 0, 0, 0
	};

	xcb_screen_t* screen = connection->getScreen();

	attachedConnection = connection;

	window = attachedConnection->generateID();
	gc = attachedConnection->generateID();

	xcb_void_cookie_t cookie;

	cookie = xcb_create_window_checked(attachedConnection->getConnection(), 0, window, screen->root, desc.x, desc.y, desc.width, desc.height,
		1, XCB_WINDOW_CLASS_INPUT_OUTPUT, screen->root_visual, XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK, values);

	if (!testCookie(cookie, connection->getConnection()))
	{
		ErrorThrow(false);
	}

	xcb_create_gc(connection->getConnection(), gc, window, 0, NULL);

	isCreated = true;

	changeTitle(winName);

	xcb_map_window(attachedConnection->getConnection(), window);
	xcb_flush(attachedConnection->getConnection());
}

void GLOWE::Linux::__SimpleWindow::destroy()
{
	if (isCreated)
	{
		xcb_free_gc(attachedConnection->getConnection(), gc);
		xcb_destroy_window(attachedConnection->getConnection(), window);
		isCreated = false;
	}
}

xcb_window_t GLOWE::Linux::__SimpleWindow::getWindow()
{
	return window;
}

xcb_gcontext_t GLOWE::Linux::__SimpleWindow::getGC()
{
	return gc;
}

bool GLOWE::Linux::__SimpleWindow::checkIsCreated() const
{
	return isCreated;
}

void GLOWE::Linux::__MsgBoxInfo::show(const String & message, const short & type)
{
	__Connection c;

	if (!c.connect())
	{
		return;
	}

	uint32_t values[] = {
		0xc0c0c0,
		XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_KEY_PRESS |
		XCB_EVENT_MASK_KEY_RELEASE | XCB_EVENT_MASK_BUTTON_PRESS |
		XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_ENTER_WINDOW |
		XCB_EVENT_MASK_LEAVE_WINDOW,
		0, 0, 0, 0
	};

	String lines[3];
	const String::BasicString& str = message.getString();

	if (str.size()>47)
	{
		lines[0] = str.substr(0, 47);
		if ((str.size() - 47)>47)
		{
			lines[1] = str.substr(47, 47);
			lines[2] = str.substr(94, str.size() - 94);
		}
		else
		{
			lines[1] = str.substr(47, str.size() - 47);
		}
	}
	else
	{
		lines[0] = message;
	}

	xcb_size_hints_t hints;

	xcb_icccm_size_hints_set_max_size(&hints, msgBoxWidth, msgBoxHeight);
	xcb_icccm_size_hints_set_min_size(&hints, msgBoxWidth, msgBoxHeight);

	__LinuxWindowDesc desc;
	desc.x = 100;
	desc.y = 100;
	desc.width = msgBoxWidth;
	desc.height = msgBoxHeight;

	String title;

	switch (type)
	{
	case 0:
		title = u8"Error";
		break;
	case 1:
		title = u8"Warning";
		break;
	case 2:
		title = u8"Notification";
		break;
	default:
		title = u8"MessageBox";
		break;
	}

	window.create(title, desc, &c);

	xcb_window_t button = c.generateID();
	xcb_pixmap_t pm = c.generateID();
	xcb_pixmap_t pm2 = c.generateID();
	xcb_colormap_t cm = c.generateID();
	xcb_alloc_color_reply_t* color1, *color2, *color3;
	xcb_void_cookie_t cookie;
	xcb_generic_event_t* evt;
	int ntype = 0;

	xcb_screen_t* screen = c.getScreen();

	xcb_create_pixmap(c, screen->root_depth,
		pm, window.getWindow(), 1, 30);
	xcb_create_pixmap(c, screen->root_depth, pm2, window.getWindow(), 1, 30);

	cookie = xcb_put_image_checked(c, 2, pm, window.getGC(), 1, 30, 0, 0, 0, screen->root_depth, 30 * 4, (uint8_t*)BUTTON_NOT_PRESSED.pixel_data);

	if (!testCookie(cookie, c))
	{
		ErrorThrow(false);
	}

	cookie = xcb_put_image_checked(c, 2, pm2, window.getGC(), 1, 30, 0, 0, 0, screen->root_depth, 30 * 4, (uint8_t*)BUTTON_PRESSED.pixel_data);

	if (!testCookie(cookie, c))
	{
		ErrorThrow(false);
	}

	values[0] = pm;

	xcb_create_window(c, 0, button,
		window.getWindow(), 182, 125, 75, 30,
		1, XCB_WINDOW_CLASS_INPUT_OUTPUT,
		screen->root_visual,
		XCB_CW_BACK_PIXMAP | XCB_CW_EVENT_MASK,
		values);

	xcb_icccm_set_wm_size_hints(c, window.getWindow(), XCB_ATOM_WM_NORMAL_HINTS, &hints);

	__Font font, font2;

	font.create(u8"9x15", c, screen, button);
	font2.create(u8"7x13", c, screen, button);

	xcb_create_colormap(c, XCB_COLORMAP_ALLOC_NONE, cm, window.getWindow(), screen->root_visual);

	xcb_map_subwindows(c, window.getWindow());
	xcb_map_window(c, window.getWindow());
	color1 = xcb_alloc_color_reply(c, xcb_alloc_color(c, cm, 0xd5 << 8, 0xd5 << 8, 0xd5 << 8), NULL);
	color2 = xcb_alloc_color_reply(c, xcb_alloc_color(c, cm, 0xa6 << 8, 0xa6 << 8, 0xa6 << 8), NULL);
	color3 = xcb_alloc_color_reply(c, xcb_alloc_color(c, cm, 0xc0 << 8, 0xc0 << 8, 0xc0 << 8), NULL);

	xcb_change_gc(c, font2.getGC(), XCB_GC_BACKGROUND, &(color3->pixel));

	xcb_change_gc(c, font.getGC(), XCB_GC_BACKGROUND, &(color1->pixel));
	xcb_flush(c);

	while (evt = xcb_wait_for_event(c))
	{
		ntype = evt->response_type & ~0x80;
		if (ntype == XCB_BUTTON_PRESS)
		{
			xcb_button_press_event_t* event = (xcb_button_press_event_t*)evt;
			if (event->event == button)
			{
				break;
			}
		}
		else
			if (ntype == XCB_ENTER_NOTIFY)
			{
				xcb_enter_notify_event_t* event = (xcb_enter_notify_event_t*)evt;
				uint32_t vals[] = { pm2 };
				if (event->event == button)
				{
					//change the BACK_PIXMAP property into using pm2...
					xcb_change_window_attributes(c, button, XCB_CW_BACK_PIXMAP, vals);
					xcb_clear_area(c, 1, button, 0, 0, 75, 30);
					xcb_change_gc(c, font.getGC(), XCB_GC_BACKGROUND, &(color2->pixel));
					xcb_image_text_8(c, 2, button, font.getGC(), 30, 20, u8"OK");
					xcb_flush(c);
				}
			}
			else
				if (ntype == XCB_LEAVE_NOTIFY)
				{
					xcb_leave_notify_event_t* event = (xcb_leave_notify_event_t*)evt;
					uint32_t vals[] = { pm };
					if (event->event == button)
					{
						//change the BACK_PIXMAP property into using pm2...
						xcb_change_window_attributes(c, button, XCB_CW_BACK_PIXMAP, vals);
						xcb_clear_area(c, 1, button, 0, 0, 75, 30);
						xcb_change_gc(c, font.getGC(), XCB_GC_BACKGROUND, &(color1->pixel));
						xcb_image_text_8(c, 2, button, font.getGC(), 30, 20, u8"OK");
						xcb_flush(c);
					}
				}
				else
					if (ntype == XCB_EXPOSE)
					{
						xcb_expose_event_t* event = (xcb_expose_event_t*)evt;
						if (event->window == button)
						{
							xcb_image_text_8(c, 2, button, font.getGC(), 30, 20, u8"OK");
						}
						else
						{
							switch (type)
							{
							case 0:
								xcb_put_image(c, 2, window.getWindow(), window.getGC(), 64, 64, 25, 25, 0, screen->root_depth, 64 * 64 * 4, (uint8_t*)ERROR_ICON.pixel_data);
								break;
							case 1:
								xcb_put_image(c, 2, window.getWindow(), window.getGC(), 64, 64, 25, 25, 0, screen->root_depth, 64 * 64 * 4, (uint8_t*)WARNING_ICON.pixel_data);
								break;
							case 2:
								break;
							}

							xcb_image_text_8(c, lines[0].getSize(), window.getWindow(), font2.getGC(), 105, 40, lines[0].getCharArray());
							xcb_image_text_8(c, lines[1].getSize(), window.getWindow(), font2.getGC(), 105, 55, lines[1].getCharArray());
							xcb_image_text_8(c, lines[2].getSize(), window.getWindow(), font2.getGC(), 105, 70, lines[2].getCharArray());
						}

						xcb_flush(c);
					}
		free(evt);
	}

	free(color1);
	free(color2);
	free(color3);
	xcb_free_colormap(c, cm);
	xcb_free_pixmap(c, pm);
	xcb_free_pixmap(c, pm2);
	font2.destroy();
	font.destroy();
	window.destroy();
	c.disconnect();
}

void GLOWE::Linux::showMessageBox(const String& text, short style)
{
	__MsgBoxInfo msgBox;
	msgBox.show(text, style);
}

#endif
