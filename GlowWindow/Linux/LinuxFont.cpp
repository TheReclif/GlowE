#include "LinuxFont.h"

#if defined(GLOWE_COMPILE_FOR_LINUX)
#include "../../GlowSystem/Error.h"

GLOWE::Linux::__Font::__Font()
	: gc(0), isCreated(false), attachedConnection(nullptr)
{
}

GLOWE::Linux::__Font::~__Font()
{
	this->destroy();
}

void GLOWE::Linux::__Font::create(const String& fontName, xcb_connection_t* connection, xcb_screen_t* screen, xcb_window_t window)
{
	if (!isCreated)
	{
		xcb_font_t font = xcb_generate_id(connection);
		attachedConnection = connection;

		auto result = xcb_open_font_checked(connection, font, fontName.getSize(), fontName.getCharArray());

		if (!testCookie(result, connection))
		{
			ErrorThrow(false);
		}

		gc = xcb_generate_id(connection);

		uint32_t        mask = XCB_GC_FOREGROUND | XCB_GC_BACKGROUND | XCB_GC_FONT;
		uint32_t        value_list[3] = { screen->black_pixel,
			screen->white_pixel,
			font
		};

		result = xcb_create_gc_checked(connection,
			gc,
			window,
			mask,
			value_list);

		if (!testCookie(result, connection))
		{
			ErrorThrow(false);
		}

		result = xcb_close_font_checked(connection, font);

		if (!testCookie(result, connection))
		{
			ErrorThrow(false);
		}

		isCreated = true;
	}
}

void GLOWE::Linux::__Font::destroy()
{
	if (isCreated)
	{
		xcb_free_gc(attachedConnection, gc);
		isCreated = false;
	}
}

xcb_gcontext_t GLOWE::Linux::__Font::getGC()
{
	return gc;
}

#endif
