#include "LinuxConnection.h"

#if defined(GLOWE_COMPILE_FOR_LINUX)
bool GLOWE::Linux::testCookie(xcb_void_cookie_t cookie, xcb_connection_t* connection)
{
	xcb_generic_error_t* error = xcb_request_check(connection, cookie);

	if (error)
	{
		free(error);
		return false;
	}

	return true;
}

GLOWE::Linux::__Connection::__Connection()
	: c(nullptr)
{
}

GLOWE::Linux::__Connection::~__Connection()
{
	this->disconnect();
}

bool GLOWE::Linux::__Connection::connect()
{
	if (c == nullptr)
	{
		c = xcb_connect(NULL, NULL);

		if (c == nullptr || xcb_connection_has_error(c))
		{
			return false;
		}
	}

	return true;
}

void GLOWE::Linux::__Connection::disconnect()
{
	if (c != nullptr)
	{
		xcb_disconnect(c);
		c = nullptr;
	}
}

uint32_t GLOWE::Linux::__Connection::generateID()
{
	return xcb_generate_id(c);
}

xcb_connection_t*& GLOWE::Linux::__Connection::getConnection()
{
	return c;
}

xcb_screen_t* GLOWE::Linux::__Connection::getScreen()
{
	return xcb_setup_roots_iterator(xcb_get_setup(c)).data;
}

GLOWE::Linux::__Connection::operator xcb_connection_t*()
{
	return c;
}
#endif
