#include "LinuxMocks.h"

void GLOWE::Linux::LinuxWindowImplementation::create(const VideoMode& size, const String& title, const Int2& position, const WindowMode& wndMode)
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::destroy()
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::resize(const VideoMode& newSize)
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::resize(const VideoMode& newSize, const WindowMode& newMode)
{
    throw NotImplementedException();
}

GLOWE::VideoMode GLOWE::Linux::LinuxWindowImplementation::getSize() const
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::setPosition(const Int2& newPos)
{
    throw NotImplementedException();
}

GLOWE::Int2 GLOWE::Linux::LinuxWindowImplementation::getPosition() const
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::setMode(const WindowMode& newMode)
{
    throw NotImplementedException();
}

GLOWE::WindowMode GLOWE::Linux::LinuxWindowImplementation::getMode() const
{
    throw NotImplementedException();
}

GLOWE::String GLOWE::Linux::LinuxWindowImplementation::getTitle() const
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::setTitle(const String& newTitle)
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::setKeyRepeat(bool arg)
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::grabCursor()
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::ungrabCursor()
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::setCursorGrab(bool arg)
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::showCursor()
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::hideCursor()
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::requestFocus()
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::processEvents()
{
    throw NotImplementedException();
}

bool GLOWE::Linux::LinuxWindowImplementation::checkIsKeyRepeatEnabled() const
{
    throw NotImplementedException();
}

bool GLOWE::Linux::LinuxWindowImplementation::checkIsCreated() const
{
    throw NotImplementedException();
}

bool GLOWE::Linux::LinuxWindowImplementation::checkHasFocus() const
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::display()
{
    throw NotImplementedException();
}

GLOWE::WindowHandle GLOWE::Linux::LinuxWindowImplementation::getWindowHandle()
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxWindowImplementation::setIsResizableImpl()
{
    throw NotImplementedException();
}

bool GLOWE::Linux::LinuxWindowImplementation::checkIsDuringMoveSize() const
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxInputImplementation::updateJoysticks()
{
    throw NotImplementedException();
}

GLOWE::Joystick::Description GLOWE::Linux::LinuxInputImplementation::getJoystickDescription(Joystick::JoystickId joystickId) const
{
    throw NotImplementedException();

}

bool GLOWE::Linux::LinuxInputImplementation::isKeyDown(Keyboard::Key keycode) const
{
    throw NotImplementedException();
}

bool GLOWE::Linux::LinuxInputImplementation::isMouseButtonDown(Mouse::Button buttoncode) const
{
    throw NotImplementedException();
}

bool GLOWE::Linux::LinuxInputImplementation::isJoystickButtonDown(Joystick::JoystickId joystickId, Joystick::JoystickId buttonId) const
{
    throw NotImplementedException();
}

bool GLOWE::Linux::LinuxInputImplementation::joystickHasAxis(Joystick::JoystickId joystickId, Joystick::Axis axis) const
{
    throw NotImplementedException();
}

bool GLOWE::Linux::LinuxInputImplementation::isJoystickConnected(Joystick::JoystickId joystickId)
{
    throw NotImplementedException();
}

unsigned int GLOWE::Linux::LinuxInputImplementation::getJoystickButtonsCount(Joystick::JoystickId joystickId) const
{
    throw NotImplementedException();
}

GLOWE::Int2 GLOWE::Linux::LinuxInputImplementation::getMousePosition() const
{
    throw NotImplementedException();
}

GLOWE::Int2 GLOWE::Linux::LinuxInputImplementation::getMousePosition(Window& relativeTo) const
{
    throw NotImplementedException();
}

float GLOWE::Linux::LinuxInputImplementation::getJoystickAxisPosition(Joystick::JoystickId joystickId, Joystick::Axis axis) const
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxInputImplementation::setMousePosition(const Int2& newPos)
{
    throw NotImplementedException();
}

void GLOWE::Linux::LinuxInputImplementation::setMousePosition(const Int2& newPos, Window& relativeTo)
{
    throw NotImplementedException();
}

unsigned int GLOWE::Linux::LinuxInputImplementation::getJoysticksCount() const
{
    throw NotImplementedException();
}
