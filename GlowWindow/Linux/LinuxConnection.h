#pragma once
#ifndef LINUX_CONNECTION_INCLUDED
#define LINUX_CONNECTION_INCLUDED

#include "../../GlowSystem/Utility.h"
#if defined(GLOWE_COMPILE_FOR_LINUX)
#include <xcb/xcb.h>
#include <xcb/xcb_aux.h>
#include <xcb/xcb_image.h>
#include <xcb/xcb_atom.h>
#include <xcb/xcb_icccm.h>

namespace GLOWE
{
	namespace Linux
	{
		bool testCookie(xcb_void_cookie_t cookie, xcb_connection_t* connection);

		class GLOWE_DLLEXPORT __Connection
		{
		private:
			xcb_connection_t* c;
		public:
			__Connection();
			~__Connection();

			bool connect();
			void disconnect();

			uint32_t generateID();

			xcb_connection_t*& getConnection();
			xcb_screen_t* getScreen();

			operator xcb_connection_t*();
		};
	}
}

#endif

#endif
