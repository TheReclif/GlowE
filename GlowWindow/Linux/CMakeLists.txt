target_sources(GlowWindow
	PRIVATE
		LinuxConnection.cpp
		LinuxFont.cpp
		LinuxMessageBox.cpp
		LinuxMocks.cpp
#	PUBLIC
#		LinuxConnection.h
#		LinuxErrorIcon.h
#		LinuxFont.h
#		LinuxMessageBox.h
#		LinuxWarningIcon.h
#		LinuxMocks.h
)
install(DIRECTORY "." DESTINATION include/GlowWindow/Linux FILES_MATCHING PATTERN "*.h")