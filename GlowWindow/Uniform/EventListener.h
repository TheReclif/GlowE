#pragma once
#ifndef EVENTLISTENER_INCLUDED
#define EVENTLISTENER_INCLUDED

#include "Event.h"

namespace GLOWE
{
	class WindowImplementation;

	class GLOWWINDOW_EXPORT EventCallback
	{
	public:
		virtual ~EventCallback() = default;

		virtual void proccessEvent(const WindowImplementation* const window, const Event& event) = 0;
	};

	class GLOWWINDOW_EXPORT EventListener
	{
	private:
		UniquePtr<EventCallback> eventHandler;
	public:
		EventListener();
		EventListener(UniquePtr<EventCallback>&& callback);

		void onEvent(const WindowImplementation* const window, const Event& msg);

		void setEventHandler(UniquePtr<EventCallback>&& callback);
	};

	class GLOWWINDOW_EXPORT EventListenersMgr
		: public Subsystem, public NoCopy
	{
	private:
		Vector<EventListener> listeners;
	public:
		// Returns index of added listener
		unsigned int addListener(EventListener&& listener);
		unsigned int addListener(UniquePtr<EventCallback>&& callback);
		void removeListener(const unsigned int id);

		void onEvent(const WindowImplementation* const window, const Event& msg);
	};
}

#endif