#include "EventListener.h"

GLOWE::EventListener::EventListener()
	: eventHandler(nullptr)
{
}

GLOWE::EventListener::EventListener(UniquePtr<EventCallback>&& callback)
	: eventHandler(std::move(callback))
{
}

void GLOWE::EventListener::onEvent(const WindowImplementation* const window, const Event& msg)
{
	if (eventHandler)
	{
		eventHandler->proccessEvent(window, msg);
	}
}

void GLOWE::EventListener::setEventHandler(UniquePtr<EventCallback>&& callback)
{
	eventHandler = std::move(callback);
}

unsigned int GLOWE::EventListenersMgr::addListener(EventListener&& listener)
{
	listeners.emplace_back(std::move(listener));

	return listeners.size() - 1;
}

unsigned int GLOWE::EventListenersMgr::addListener(UniquePtr<EventCallback>&& callback)
{
	return addListener(EventListener(std::move(callback)));
}

void GLOWE::EventListenersMgr::removeListener(const unsigned int id)
{
	if (id >= listeners.size())
	{
		return;
	}

	listeners.erase(std::next(listeners.begin(), id));
}

void GLOWE::EventListenersMgr::onEvent(const WindowImplementation* const window, const Event& msg)
{
	for (auto& x : listeners)
	{
		x.onEvent(window, msg);
	}
}
