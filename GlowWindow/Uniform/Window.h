#pragma once
#ifndef GLOWE_UNIFORM_WINDOW_INCLUDED
#define GLOWE_UNIFORM_WINDOW_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "VideoMode.h"
#include "../../GlowSystem/SettingsMgr.h"
#include "../../GlowSystem/MemoryMgr.h"
#include "../../GlowSystem/Time.h"
#include "../../GlowSystem/String.h"
#include "../../GlowSystem/Color.h"

#include "WndHandle.h"
#include "InputMgr.h"
#include "EventMgr.h"
#include "CurrentWindowImpl.h"

namespace GLOWE
{
	extern Color windowBackgroundColor;

	class GLOWWINDOW_EXPORT Window
		: public NoCopy
	{
	private:
		CurrentWindowImplementation winImpl;
	private:
		class GLOWWINDOW_EXPORT WindowEventCallback
			: public EventCallback
		{
		private:
			Window* wnd;
		public:
			WindowEventCallback() = delete;
			WindowEventCallback(Window* arg);
			virtual ~WindowEventCallback() = default;

			virtual void proccessEvent(const WindowImplementation* const window, const Event& event) override;
		};
	protected:
		virtual void onResize(const size_t newHeight, const size_t newWidth);
	public:
		Window() = default;
		Window(Window&& arg) noexcept;
		virtual ~Window();
		
		void create(const VideoMode& size, const String& title, const Int2& position = Int2{ WindowImplementation::AutoCenter, WindowImplementation::AutoCenter }, const WindowMode& wndMode = WindowMode::Default);
		void open(const VideoMode& size, const String& title, const Int2& position = Int2{ WindowImplementation::AutoCenter, WindowImplementation::AutoCenter }, const WindowMode& wndMode = WindowMode::Default)
		{
			create(size, title, position, wndMode);
		}

		void destroy();
		void close()
		{
			destroy();
		}

		void resize(const VideoMode& newSize);
		void resize(const VideoMode& newSize, const WindowMode& newMode);

		VideoMode getSize() const;
		void setPosition(const Int2& newPos);
		Int2 getPosition() const;
		void setMode(const WindowMode& newMode);
		WindowMode getMode() const;
		String getTitle() const;
		void setTitle(const String& newTitle);
		void setKeyRepeat(bool arg);
		void grabCursor();
		void ungrabCursor();
		void setCursorGrab(bool arg);
		void showCursor();
		void hideCursor();
		void requestFocus();
		void setIsResizable(const bool arg);

		void waitForEvent();
		bool peekEvents();

		bool checkIsKeyRepeatEnabled() const;
		bool checkIsCreated() const;
		bool checkIsResizable() const;
		bool hasFocus() const;

		virtual void display();

		WindowHandle getWindowHandle() const;

		void waitForAnyEvent();

		friend class EventMgr;
	};

	GLOWWINDOW_EXPORT bool operator==(const Window& wnd, const WindowHandle& handle);	    
	GLOWWINDOW_EXPORT bool operator!=(const Window& wnd, const WindowHandle& handle);	    
	GLOWWINDOW_EXPORT bool operator==(const WindowHandle& handle, const Window& wnd);   
	GLOWWINDOW_EXPORT bool operator!=(const WindowHandle& handle, const Window& wnd);
}

#endif
