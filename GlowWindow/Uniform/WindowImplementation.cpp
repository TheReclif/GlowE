#include "WindowImplementation.h"

GLOWE::WindowImplementation::WindowImplementation()
	: windowMode(WindowMode::Default), isCreated(false), isResizable(false), context(nullptr)
{
}

GLOWE::WindowImplementation::WindowImplementation(WindowImplementation&& arg) noexcept
	: windowMode(arg.windowMode), isCreated(exchange(arg.isCreated, false)), isResizable(arg.isResizable), context(exchange(arg.context, nullptr))
{
}

void GLOWE::WindowImplementation::setIsResizable(const bool arg)
{
	isResizable = arg;
	setIsResizableImpl();
}

bool GLOWE::WindowImplementation::checkIsResizable() const
{
	return isResizable;
}
