#pragma once
#ifndef GLOWE_UNIFORM_INPUTMGR_INCLUDED
#define GLOWE_UNIFORM_INPUTMGR_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "Joystick.h"

#ifdef GLOWE_COMPILE_FOR_WINDOWS
#include "../Windows/WindowsInputImplementation.h"
#include "../Windows/WindowsWindowImplementation.h"
#elif defined(GLOWE_COMPILE_FOR_LINUX)
//#include "../Linux/LinuxInputImplementation.h"
//#include "../Linux/LinuxWindowImplementation.h"
#include "../Linux/LinuxMocks.h"
#elif defined(GLOWE_COMPILE_FOR_MAC)
//#include "../Mac/MacInputImplementation.h"
//#include "../Mac/MacWindowImplementation.h"
#else
#error Target OS not specified.
#endif

namespace GLOWE
{
#if defined(GLOWE_COMPILE_FOR_WINDOWS)
	using InputMgr = Windows::WindowsInputImplementation;
#elif defined(GLOWE_COMPILE_FOR_LINUX)
	using InputMgr = Linux::LinuxInputImplementation;
#endif
}

#endif
