#include "Mouse.h"
#include "InputMgr.h"

bool GLOWE::Mouse::isMouseButtonPressed(Button buttonCode)
{
	return getInstance<InputMgr>().isMouseButtonDown(buttonCode);
}

GLOWE::Int2 GLOWE::Mouse::getMousePosition(bool global)
{
	return getInstance<InputMgr>().getMousePosition(global);
}

GLOWE::Int2 GLOWE::Mouse::getMousePosition(Window & relativeTo)
{
	return getInstance<InputMgr>().getMousePosition(relativeTo);
}

void GLOWE::Mouse::setMousePosition(const Int2& newPos, bool global)
{
	getInstance<InputMgr>().setMousePosition(newPos, global);
}

void GLOWE::Mouse::setMousePosition(const Int2& newPos, Window& relativeTo)
{
	getInstance<InputMgr>().setMousePosition(newPos, relativeTo);
}
