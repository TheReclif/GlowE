#include "MessageBox.h"

void GLOWE::messageBox(const String& text, MsgBoxType style)
{
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	Windows::showMessageBox(text, style);
#elif defined(GLOWE_COMPILE_FOR_LINUX) && !defined(GLOWE_SKIP_XCB)
	Linux::showMessageBox(text, style);
#elif defined(GLOWE_COMPILE_FOR_MAC)
	Mac::showMessageBox(text, style);
#endif
}
