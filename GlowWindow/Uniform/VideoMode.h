#pragma once
#ifndef GLOWE_UNIFORM_VIDEOMODE_INCLUDED
#define GLOWE_UNIFORM_VIDEOMODE_INCLUDED

#include "glowwindow_export.h"

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/String.h"

namespace GLOWE
{
	class GLOWWINDOW_EXPORT VideoMode
	{
	public:
		enum : unsigned int
		{
			Keep = ~static_cast<unsigned int>(0)
		};
		unsigned int x, y, bpp, refreshRate;

		VideoMode();
		VideoMode(const unsigned int width, const unsigned int height, const unsigned int bpp, const unsigned int refresh = 0);
		~VideoMode() = default;

		void setMode(const unsigned int width, const unsigned int height, const unsigned int bpp, const unsigned int refresh = Keep);

		OStringStream getVideoModeAsSString() const;

		static VideoMode getMonitorVideoMode();
		static void getAvailableVideoModes(Vector<VideoMode>& vecOut);
	};

	GLOWWINDOW_EXPORT bool operator==(const VideoMode left, const VideoMode right);
	GLOWWINDOW_EXPORT bool operator!=(const VideoMode left, const VideoMode right);
	GLOWWINDOW_EXPORT bool operator<(const VideoMode left, const VideoMode right);
	GLOWWINDOW_EXPORT bool operator>(const VideoMode left, const VideoMode right);
	GLOWWINDOW_EXPORT bool operator<=(const VideoMode left, const VideoMode right);
	GLOWWINDOW_EXPORT bool operator>=(const VideoMode left, const VideoMode right);
}

#endif
