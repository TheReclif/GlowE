#include "Joystick.h"
#include "InputMgr.h"

bool GLOWE::Joystick::isConnected(JoystickId id)
{
	return getInstance<InputMgr>().isJoystickConnected(id);
}

bool GLOWE::Joystick::hasAxis(JoystickId joystickId, Axis axis)
{
	return getInstance<InputMgr>().joystickHasAxis(joystickId, axis);
}

bool GLOWE::Joystick::isButtonDown(Joystick::JoystickId joystickId, Joystick::JoystickId buttonId)
{
	return getInstance<InputMgr>().isJoystickButtonDown(joystickId, buttonId);
}

unsigned int GLOWE::Joystick::getButtonsCount(JoystickId id)
{
	return getInstance<InputMgr>().getJoystickButtonsCount(id);
}

float GLOWE::Joystick::getAxisPosition(JoystickId joystickId, Axis axis)
{
	return getInstance<InputMgr>().getJoystickAxisPosition(joystickId, axis);
}

unsigned int GLOWE::Joystick::getJoysticksCount()
{
	return getInstance<InputMgr>().getJoysticksCount();
}

GLOWE::Joystick::Description GLOWE::Joystick::getDesc(JoystickId joystickId)
{
	return getInstance<InputMgr>().getJoystickDescription(joystickId);
}

GLOWE::Joystick::Capabilities::Capabilities()
	: buttonCount(0), supportedAxes()
{
	for (bool & x : Joystick::Capabilities::supportedAxes)
	{
		x = false;
	}
}

GLOWE::Joystick::Status::Status()
	: isConnected(false)
{
	for (float & x : axesPos)
	{
		x = 0.0f;
	}

	for (bool & x : buttonsStatus)
	{
		x = false;
	}
}
