#include "InputImplementation.h"

void GLOWE::InputImplementation::_initForEngine(Window& wnd)
{
	if (windowEngine == nullptr)
	{
		windowEngine = &wnd;
	}
}

void GLOWE::InputImplementation::_cleanUpForEngine()
{
	windowEngine = nullptr;
}

bool GLOWE::InputImplementation::checkIsEngineInitialized() const
{
	return windowEngine != nullptr;
}
