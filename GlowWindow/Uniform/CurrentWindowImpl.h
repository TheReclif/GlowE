#pragma once
#ifndef GLOWE_UNIFORM_CURRENTWINDOWIMPL_INCLUDED
#define GLOWE_UNIFORM_CURRENTWINDOWIMPL_INCLUDED

#include "../../GlowSystem/MemoryMgr.h"

#include "WindowImplementation.h"

#if defined(GLOWE_COMPILE_FOR_WINDOWS)
#include "../Windows/WindowsWindowImplementation.h"
#elif defined(GLOWE_COMPILE_FOR_LINUX)
//#include "../Linux/LinuxWindowImplementation.h"
#include "../Linux/LinuxMocks.h"
#elif defined(GLOWE_COMPILE_FOR_MAC)
#include "../Mac/MacWindowImplementation.h"
#else
#error OS not defined or not supported.
#endif

namespace GLOWE
{
#if defined(GLOWE_COMPILE_FOR_WINDOWS)
	using CurrentWindowImplementation = Windows::WindowsWindowImplementation;
#elif defined(GLOWE_COMPILE_FOR_LINUX)
	using CurrentWindowImplementation = Linux::LinuxWindowImplementation;
#endif
}

#endif
