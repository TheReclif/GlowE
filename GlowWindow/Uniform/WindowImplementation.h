#pragma once
#ifndef GLOWE_WINDOWIMPLEMENTATION_INCLUDED
#define GLOWE_WINDOWIMPLEMENTATION_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowMath/Math.h"
#include "../../GlowMath/VectorClasses.h"

#include "EventListener.h"
#include "WndHandle.h"
#include "VideoMode.h"

namespace GLOWE
{
	enum class WindowMode
	{
		Windowed = 0,
		Default = Windowed,
		Fullscreen,
		TrueFullscreen
	};

	class GraphicsDeviceImpl;

	class GLOWWINDOW_EXPORT WindowImplementation
		: public NoCopy
	{
	protected:
		WindowMode windowMode;

		bool isCreated, isResizable;

		GraphicsDeviceImpl* context;
	protected:
		virtual void setIsResizableImpl() = 0;
	public:
		enum : int
		{
			/// @brief Used as position variable to indicate that the window should be center at the screen center.
			AutoCenter = std::numeric_limits<int>::min()
		};
	public:
		WindowImplementation();
		WindowImplementation(WindowImplementation&& arg) noexcept;
		virtual ~WindowImplementation() = default;

		virtual void create(const VideoMode& size, const String& title, const Int2& position = Int2{ AutoCenter, AutoCenter }, const WindowMode& wndMode = WindowMode::Windowed) = 0;
		virtual void destroy() = 0;

		virtual void resize(const VideoMode& newSize) = 0;
		virtual void resize(const VideoMode& newSize, const WindowMode& newMode) = 0;

		virtual VideoMode getSize() const = 0;
		virtual void setPosition(const Int2& newPos) = 0;
		virtual Int2 getPosition() const = 0;
		virtual void setMode(const WindowMode& newMode) = 0;
		virtual WindowMode getMode() const = 0;
		virtual String getTitle() const = 0;
		virtual void setTitle(const String& newTitle) = 0;
		virtual void setKeyRepeat(bool arg) = 0;
		virtual void grabCursor() = 0;
		virtual void ungrabCursor() = 0;
		virtual void setCursorGrab(bool arg) = 0;
		virtual void showCursor() = 0;
		virtual void hideCursor() = 0;
		virtual void requestFocus() = 0;
		void setIsResizable(const bool arg);

		virtual void processEvents() = 0;

		virtual bool checkIsDuringMoveSize() const = 0;
		virtual bool checkIsKeyRepeatEnabled() const = 0;
		virtual bool checkIsCreated() const = 0;
		virtual bool checkHasFocus() const = 0;
		bool checkIsResizable() const;

		virtual void display() = 0;

		virtual WindowHandle getWindowHandle() const = 0;
	};
}

#endif