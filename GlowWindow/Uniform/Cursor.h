#pragma once
#ifndef GLOWE_WINDOW_CURSOR_INCLUDED
#define GLOWE_WINDOW_CURSOR_INCLUDED

#include "glowwindow_export.h"

#include "../../GlowSystem/OneInstance.h"
#include "../../GlowSystem/Color.h"

namespace GLOWE
{
	class GLOWWINDOW_EXPORT Cursor
		: public Subsystem, public Lockable
	{
	public:
		enum class CursorType
		{
			Arrow,
			Cross,
			Hand,
			Help,
			Beam,
			No,
			SizeAll,
			SizeNESW,
			SizeNS,
			SizeNWSE,
			SizeWE,
			UpArrow,
			Wait
		};
	private:
		UInt2 dim, hotSpot;
		Vector<Color> cursorPixels;
#ifdef GLOWE_COMPILE_FOR_WINDOWS
		void* cachedCursor = nullptr;
		bool updateCachedCursor = false;
#endif
	public:
		virtual ~Cursor();

		void setCursor(const Color* pixels, const UInt2 size, const UInt2 hotspot);
		void setCursor(const CursorType cursor);

		bool _onSetCursor();
	};
}

#endif
