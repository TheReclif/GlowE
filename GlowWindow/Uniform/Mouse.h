#pragma once
#ifndef GLOW_UNIFORM_MOUSE_INCLUDED
#define GLOW_UNIFORM_MOUSE_INCLUDED

#include "glowwindow_export.h"

#include "../../GlowSystem/Utility.h"
#include "../../GlowMath/VectorClasses.h"

namespace GLOWE
{
	class Window;

	class GLOWWINDOW_EXPORT Mouse
	{
	private:
		Mouse();
	public:
		enum class Button
		{
			Left = 0,
			Right,
			Middle,
			XButton1,
			XButton2,
			ButtonCount
		};

		enum class Wheel
		{
			Horizontal = 0, 
			Vertical
		};

		static bool isMouseButtonPressed(Button buttonCode);
		static Int2 getMousePosition(bool global = false);
		static Int2 getMousePosition(Window& relativeTo);
		static void setMousePosition(const Int2& newPos, bool global = false);
		static void setMousePosition(const Int2& newPos, Window& relativeTo);
	};
}

#endif
