#pragma once
#ifndef GLOWE_UNIFORM_INPUTIMPL_INCLUDED
#define GLOWE_UNIFORM_INPUTIMPL_INCLUDED

#include "Keyboard.h"
#include "Mouse.h"
#include "Joystick.h"

#include "../../GlowSystem/NumberStorage.h"

namespace GLOWE
{
	class GLOWWINDOW_EXPORT InputImplementation
	{
	protected:
		Window* windowEngine;
	public:
		InputImplementation() : windowEngine(nullptr) {}
		virtual ~InputImplementation() = default;

		void _initForEngine(Window& wnd);
		void _cleanUpForEngine();
		bool checkIsEngineInitialized() const; // If true, all functions that have "Window& relative" variants their no-relative variants will work with Window from Engine.

		virtual void updateJoysticks() = 0;

		virtual Joystick::Description getJoystickDescription(Joystick::JoystickId joystickId) const = 0;

		virtual bool isKeyDown(Keyboard::Key keycode) const = 0;
		virtual bool isMouseButtonDown(Mouse::Button buttoncode) const = 0;
		virtual bool isJoystickButtonDown(Joystick::JoystickId joystickId, Joystick::JoystickId buttonId) const = 0;
		virtual bool joystickHasAxis(Joystick::JoystickId joystickId, Joystick::Axis axis) const = 0;
		virtual bool isJoystickConnected(Joystick::JoystickId joystickId) = 0;

		virtual unsigned int getJoystickButtonsCount(Joystick::JoystickId joystickId) const = 0;

		virtual Int2 getMousePosition(bool global = false) const = 0;
		virtual Int2 getMousePosition(Window& relativeTo) const = 0;

		virtual float getJoystickAxisPosition(Joystick::JoystickId joystickId, Joystick::Axis axis) const = 0;

		virtual void setMousePosition(const Int2& newPos, bool global = false) = 0;
		virtual void setMousePosition(const Int2& newPos, Window& relativeTo) = 0;

		virtual unsigned int getJoysticksCount() const = 0;
	};
}

#endif
