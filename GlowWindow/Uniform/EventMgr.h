#pragma once
#ifndef UNIFORM_EVENTMGR_INCLUDED
#define UNIFORM_EVENTMGR_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/MemoryMgr.h"

#include "Event.h"

namespace GLOWE
{
	class EventListenersMgr;
	class WindowImplementation;
	class Window;

	class GLOWWINDOW_EXPORT EventMgr
		: public Subsystem, public NoCopy
	{
	private:
		Map<const WindowImplementation*, Deque<Event>> eventsQueue;

		EventListenersMgr& listeners;
	public:
		EventMgr();

		Event getEvent(const WindowImplementation* const window) const;
		Event getEvent(const Window& window) const;
		void popEvent(const WindowImplementation* const window, Event& out);
		void popEvent(const Window& window, Event& out);
		void pushEvent(const WindowImplementation* const window, const Event& event);
		void pushEvent(const Window& window, const Event& event);
		void clear(const WindowImplementation* const window = nullptr);
		void clear(const Window& window);

		Deque<Event>& getEventsQueue(const WindowImplementation* const window);
		Deque<Event>& getEventsQueue(const Window& window);

		bool checkIsEmpty(const WindowImplementation* const window) const;
		bool checkIsEmpty(const Window& window) const;
	};
}

#endif // UNIFORM_EVENTMGR_INCLUDED
