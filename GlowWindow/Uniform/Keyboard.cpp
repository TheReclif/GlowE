#include "Keyboard.h"
#include "InputMgr.h"

bool GLOWE::Keyboard::isKeyDown(Key keycode)
{
	return OneInstance<InputMgr>::instance().isKeyDown(keycode);
}
