#include "VideoMode.h"

#ifdef GLOWE_COMPILE_FOR_WINDOWS
#include "../Windows/WindowsVideoMode.h"
#elif defined(GLOWE_COMPILE_FOR_LINUX)
//#include "../Linux/LinuxVideoMode.h"
#elif defined(GLOWE_COMPILE_FOR_MAC)
#include "../Mac/MacVideoMode.h"
#else
#error Target OS not specified.
#endif

// TODO: Replace Linux mocks.

GLOWE::VideoMode::VideoMode()
	: x(0), y(0), bpp(0), refreshRate(0)
{
}

GLOWE::VideoMode::VideoMode(const unsigned int ax, const unsigned int ay, const unsigned int az, const unsigned int refresh)
	: x(ax), y(ay), bpp(az), refreshRate(refresh)
{
}

void GLOWE::VideoMode::setMode(const unsigned int ax, const unsigned int ay, const unsigned int az, const unsigned int refresh)
{
	x = ax;
	y = ay;
	bpp = az;
	if (refresh != Keep)
	{
		refreshRate = refresh;
	}
}

GLOWE::OStringStream GLOWE::VideoMode::getVideoModeAsSString() const
{
	OStringStream a;
	a << u8"(" << x << u8"," << y << u8"," << bpp << "," << refreshRate << u8")";
	return std::move(a);
}

GLOWE::VideoMode GLOWE::VideoMode::getMonitorVideoMode()
{
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	return Windows::getMonitorVideoMode();
#elif defined(GLOWE_COMPILE_FOR_LINUX)
	//return Linux::getMonitorVideoMode();
	return VideoMode();
#elif defined(GLOWE_COMPILE_FOR_MAC)
	return Mac::getMonitorVideoMode();
#else
#error Target OS not specified.
#endif
}

void GLOWE::VideoMode::getAvailableVideoModes(Vector<VideoMode>& vecOut)
{
	vecOut.clear();
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	Windows::getAvailableVideoModes(vecOut);
#elif defined(GLOWE_COMPILE_FOR_LINUX)
	//Linux::getAvailableVideoModes(vecOut);
#elif defined(GLOWE_COMPILE_FOR_MAC)
	Mac::getAvailableVideoModes(vecOut);
#else
#error Target OS not specified.
#endif
}

bool GLOWE::operator==(const VideoMode left, const VideoMode right)
{
	return (left.x == right.x) &&
		(left.y == right.y) &&
		(left.bpp == right.bpp) &&
		(left.refreshRate == right.refreshRate);
}

bool GLOWE::operator!=(const VideoMode left, const VideoMode right)
{
	return !(left == right);
}

bool GLOWE::operator<(const VideoMode left, const VideoMode right)
{
	if (left.bpp == right.bpp)
	{
		if (left.x == right.x)
		{
			if (left.refreshRate == right.refreshRate)
			{
				return left.y < right.y;
			}
			else
			{
				return left.refreshRate < right.refreshRate;
			}
		}
		else
		{
			return left.x < right.x;
		}
	}
	else
	{
		return left.bpp < right.bpp;
	}
}

bool GLOWE::operator>(const VideoMode left, const VideoMode right)
{
	return right < left;
}

bool GLOWE::operator<=(const VideoMode left, const VideoMode right)
{
	return !(left > right);
}

bool GLOWE::operator>=(const VideoMode left, const VideoMode right)
{
	return !(left < right);
}
