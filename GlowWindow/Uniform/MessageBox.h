#pragma once
#ifndef GLOW_MESSAGEBOX_INCLUDED
#define GLOW_MESSAGEBOX_INCLUDED

#include "glowwindow_export.h"

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/String.h"

#ifdef GLOWE_COMPILE_FOR_WINDOWS
#include "../Windows/WindowsMessageBox.h"
#elif defined(GLOWE_COMPILE_FOR_LINUX)
#include "../Linux/LinuxMessageBox.h"
#elif defined(GLOWE_COMPILE_FOR_MAC)
#include "../Mac/MacMessageBox.h"
#else
#error Target OS not specified.
#endif

namespace GLOWE
{
	enum MsgBoxType
	{
		MsgBoxError = 0,
		MsgBoxWarning = 1,
		MsgBoxInfo = 2
	};

	GLOWWINDOW_EXPORT void messageBox(const String& text, MsgBoxType style);
}

#endif