#pragma once
#ifndef GLOW_UNIFORM_WNDHANDLE_INCLUDED
#define GLOW_UNIFORM_WNDHANDLE_INCLUDED

#include "../../GlowSystem/Utility.h"

namespace GLOWE
{
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	using WindowHandle =  HWND;
#elif defined(GLOWE_COMPILE_FOR_LINUX)
#ifndef GLOWE_SKIP_XCB
	using WindowHandle = xcb_window_t;
#else
	using WindowHandle = void*;
#endif
#elif defined(GLOWE_COMPILE_FOR_MAC)
	using WindowHandle = void*;
#else
#error Target OS not defined.
#endif
}

#endif
