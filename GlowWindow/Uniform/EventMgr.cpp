#include "EventMgr.h"
#include "EventListener.h"
#include "Window.h"

GLOWE::EventMgr::EventMgr()
	: listeners(getInstance<EventListenersMgr>())
{
}

GLOWE::Event GLOWE::EventMgr::getEvent(const WindowImplementation* const window) const
{
	if (!eventsQueue.empty())
	{
		return eventsQueue.at(window).front();
	}

	return Event();
}

GLOWE::Event GLOWE::EventMgr::getEvent(const Window& window) const
{
	return getEvent(&window.winImpl);
}

void GLOWE::EventMgr::popEvent(const WindowImplementation* const window, Event& out)
{
	auto& que = eventsQueue.at(window);
	if (!que.empty())
	{
		out = que.front();
		listeners.onEvent(window, out);
		que.pop_front();
	}
}

void GLOWE::EventMgr::popEvent(const Window& window, Event& out)
{
	popEvent(&window.winImpl, out);
}

void GLOWE::EventMgr::pushEvent(const WindowImplementation* const window, const Event & event)
{
	eventsQueue[window].push_back(event);
}

void GLOWE::EventMgr::pushEvent(const Window& window, const Event& event)
{
	pushEvent(&window.winImpl, event);
}

void GLOWE::EventMgr::clear(const WindowImplementation* const window)
{
	// TODO: Is it even needed here? Rethink the whole "listeners" feature.
	for (auto& x : eventsQueue)
	{
		if (window == nullptr || window == x.first)
		{
			for (const auto& y : x.second)
			{
				listeners.onEvent(x.first, y);
			}
			x.second.clear();
		}
	}
}

void GLOWE::EventMgr::clear(const Window& window)
{
	clear(&window.winImpl);
}

GLOWE::Deque<GLOWE::Event>& GLOWE::EventMgr::getEventsQueue(const WindowImplementation* const window)
{
	return eventsQueue[window];
}

GLOWE::Deque<GLOWE::Event>& GLOWE::EventMgr::getEventsQueue(const Window& window)
{
	return getEventsQueue(&window.winImpl);
}

bool GLOWE::EventMgr::checkIsEmpty(const WindowImplementation* const window) const
{
	const auto& temp = eventsQueue.find(window);
	if (temp != eventsQueue.end())
	{
		return temp->second.empty();
	}
	return true;
}

bool GLOWE::EventMgr::checkIsEmpty(const Window& window) const
{
	return checkIsEmpty(&window.winImpl);
}
