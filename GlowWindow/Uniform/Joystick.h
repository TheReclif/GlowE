#pragma once
#ifndef GLOW_UNIFORM_JOYSTICK_INCLUDED
#define GLOW_UNIFORM_JOYSTICK_INCLUDED

#include "glowwindow_export.h"

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/String.h"

namespace GLOWE
{
	class GLOWWINDOW_EXPORT Joystick
	{
	private:
		Joystick();
	public:
		static constexpr unsigned int supportedJoysticks = 8;
		static constexpr unsigned int supportedButtons = 32;
		static constexpr unsigned int supportedAxes = 8;
	public:
		using JoystickId = unsigned int;

		enum class Axis
		{
			X,
			Y,
			Z,
			R,
			U,
			V,
			PovX, 
			PovY
		};

		class GLOWWINDOW_EXPORT Description
		{
		public:
			Description() = default;

			String name;

			JoystickId vendorId, productId;
		};

		class GLOWWINDOW_EXPORT Capabilities
		{
		public:
			Capabilities();

			unsigned int buttonCount;
			bool supportedAxes[Joystick::supportedAxes];
		};

		class GLOWWINDOW_EXPORT Status
		{
		public:
			Status();

			bool isConnected;
			float axesPos[Joystick::supportedAxes];
			bool buttonsStatus[Joystick::supportedButtons];
		};
	public:
		static bool isConnected(JoystickId id);
		static bool hasAxis(JoystickId joystickId, Axis axis);
		static bool isButtonDown(Joystick::JoystickId joystickId, Joystick::JoystickId buttonId);

		static unsigned int getButtonsCount(JoystickId id);

		static float getAxisPosition(JoystickId joystickId, Axis axis);

		static unsigned int getJoysticksCount();

		static Description getDesc(JoystickId joystickId);
	};
}

#endif
