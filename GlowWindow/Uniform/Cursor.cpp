#include "Cursor.h"

#include <bitset>

#include <Windows.h>

#ifdef GLOWE_COMPILE_FOR_WINDOWS
LPCWSTR cursorEnumToIntResource(const GLOWE::Cursor::CursorType cursor)
{
	using Type = GLOWE::Cursor::CursorType;
	switch (cursor)
	{
	case Type::Arrow:
		return IDC_ARROW;
	case Type::Beam:
		return IDC_IBEAM;
	case Type::Cross:
		return IDC_CROSS;
	case Type::Hand:
		return IDC_HAND;
	case Type::Help:
		return IDC_HELP;
	case Type::No:
		return IDC_NO;
	case Type::SizeAll:
		return IDC_SIZEALL;
	case Type::SizeNESW:
		return IDC_SIZENESW;
	case Type::SizeNS:
		return IDC_SIZENS;
	case Type::SizeNWSE:
		return IDC_SIZENWSE;
	case Type::SizeWE:
		return IDC_SIZEWE;
	case Type::UpArrow:
		return IDC_UPARROW;
	case Type::Wait:
		return IDC_WAIT;
	default:
		throw GLOWE::Exception("Invalid enum value");
	}
}
#endif

GLOWE::Cursor::~Cursor()
{
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	if (cachedCursor)
	{
		DestroyCursor(static_cast<HCURSOR>(cachedCursor));
	}
#endif
}

void GLOWE::Cursor::setCursor(const Color* pixels, const UInt2 size, const UInt2 hotspot)
{
	dim = size;
	hotSpot = hotspot;
	cursorPixels.assign(pixels, pixels + (size[0] * size[1]));
	updateCachedCursor = true;
}

void GLOWE::Cursor::setCursor(const CursorType cursor)
{
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	if (cachedCursor)
	{
		DestroyCursor(static_cast<HCURSOR>(cachedCursor));
	}
	updateCachedCursor = false;
	cachedCursor = LoadCursorW(nullptr, cursorEnumToIntResource(cursor));
#endif
}

bool GLOWE::Cursor::_onSetCursor()
{
#ifdef GLOWE_COMPILE_FOR_WINDOWS
	if (!updateCachedCursor)
	{
		if (cachedCursor)
		{
			SetCursor(static_cast<HICON>(cachedCursor));
		}
		return cachedCursor;
	}
	if (cachedCursor)
	{
		DestroyIcon(static_cast<HICON>(cachedCursor));
		cachedCursor = nullptr;
	}
	
	const auto width = dim[0], height = dim[1];
	const auto alignment = (width % (alignof(DWORD) * 8) != 0) ? (alignof(DWORD) * 8) - (width % (alignof(DWORD) * 8)) : 0;
	Vector<COLORREF> pixels(cursorPixels.size());
	Vector<BYTE> andMaskBuffer;
	unsigned int counter = 0;
	andMaskBuffer.resize((cursorPixels.size() + alignment * height) / 8, 0);
	const auto emplaceBit = [&](const unsigned int bit)
	{
		andMaskBuffer[counter / 8] |= (bit << (counter % 8));
		++counter;
	};
	for (unsigned int y = 0; y < height; ++y)
	{
		for (unsigned int x = 0; x < width; ++x)
		{
			const auto currentIndex = (height * y) + x;
			const auto currentPixel = cursorPixels[currentIndex];
			if (currentPixel.a < 0.8f)
			{
				emplaceBit(1);
				pixels[currentIndex] = RGB(0, 0, 0);
			}
			else
			{
				emplaceBit(0);
				pixels[currentIndex] = RGB(
					static_cast<BYTE>(std::floor(currentPixel.r >= 1.0f ? 255 : currentPixel.r * 256.0f)),
					static_cast<BYTE>(std::floor(currentPixel.g >= 1.0f ? 255 : currentPixel.g * 256.0f)),
					static_cast<BYTE>(std::floor(currentPixel.b >= 1.0f ? 255 : currentPixel.b * 256.0f)));
			}
		}

		for (unsigned int x = 0; x < alignment; ++x)
		{
			emplaceBit(0);
		}
	}

	HBITMAP andMask = CreateBitmap(width, height, 1, 1, andMaskBuffer.data()), xorMask = CreateBitmap(width, height, 3, 24, pixels.data());
	if (andMask == nullptr)
	{
		throw StringException("Invalid Cursor: AND mask: " + toString(GetLastError()));
	}
	if (xorMask == nullptr)
	{
		DeleteObject(andMask);
		throw StringException("Invalid Cursor: XOR mask: " + toString(GetLastError()));
	}

	ICONINFO iconInfo = { 0 };
	iconInfo.fIcon = FALSE;
	iconInfo.hbmMask = andMask;
	iconInfo.hbmColor = xorMask;
	iconInfo.xHotspot = hotSpot[0];
	iconInfo.yHotspot = hotSpot[1];

	cachedCursor = CreateIconIndirect(&iconInfo);
	DeleteObject(andMask);
	DeleteObject(xorMask);

	if (cachedCursor == nullptr)
	{
		throw StringException("Invalid Cursor: CreateIconIndirect failed: " + toString(GetLastError()));
	}
	SetCursor(static_cast<HICON>(cachedCursor));

	return true;
#else
#error Platform unsupported by the Cursor API.
#endif
}
