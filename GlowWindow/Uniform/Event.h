#pragma once
#ifndef GLOW_UNIFORM_EVENT_INCLUDED
#define GLOW_UNIFORM_EVENT_INCLUDED

#include "Mouse.h"
#include "Keyboard.h"
#include "Joystick.h"
#include "WndHandle.h"

namespace GLOWE
{
	struct GLOWWINDOW_EXPORT Event
	{
		enum class Type
		{
			Closed = 0,
			Resized,
			LostFocus,
			GainedFocus,
			TextEntered,
			KeyPressed,
			KeyReleased,
			MouseWheelScrolled,
			MouseButtonPressed,
			MouseButtonReleased,
			MouseMoved,
			MouseEntered,
			MouseLeft,
			JoystickButtonPressed,
			JoystickButtonReleased,
			JoystickMoved,
			JoystickConnected,
			JoystickDisconnected,
			EnterSizeMove,
			ExitSizeMove,
			Count
		};

		struct GLOWWINDOW_EXPORT ResizeInfo
		{
			size_t newWidth, newHeight;
		};

		struct GLOWWINDOW_EXPORT TextEnteredInfo
		{
			char32_t characterWritten;
		};

		struct GLOWWINDOW_EXPORT MouseWheelScrollInfo
		{
			float delta;
			Mouse::Wheel wheel;
		};

		struct GLOWWINDOW_EXPORT MouseMoveInfo
		{
			int x, y;
		};

		struct GLOWWINDOW_EXPORT MouseButtonInfo
		{
			Mouse::Button button;
			int x, y;
		};

		struct GLOWWINDOW_EXPORT KeyInfo
		{
			Keyboard::Key key;
			bool isAltPressed, isShiftPressed, isControlPressed, isSystemPressed;
		};

		struct GLOWWINDOW_EXPORT JoystickMoveInfo
		{
			unsigned int  joystickIndex;
			Joystick::Axis axis;
			float axisPosition;
		};

		struct GLOWWINDOW_EXPORT JoystickConnectInfo
		{
			unsigned int joystickIndex;
		};

		struct GLOWWINDOW_EXPORT JoystickButtonInfo
		{
			unsigned int joystickIndex, buttonIndex;
		};

		/// @brief Event info for EnterSizeMove and ExitSizeMove
		struct GLOWWINDOW_EXPORT SizeMoveInfo
		{
			int top, left, bottom, right;
		};

		union
		{
			ResizeInfo resizeInfo;
			TextEnteredInfo textEnteredInfo;
			MouseWheelScrollInfo mouseWheelScrollInfo;
			MouseMoveInfo mouseMoveInfo;
			MouseButtonInfo mouseButtonInfo;
			KeyInfo keyInfo;
			JoystickMoveInfo joystickMoveInfo;
			JoystickConnectInfo joystickConnectInfo;
			JoystickButtonInfo joystickButtonInfo;
			SizeMoveInfo sizeMoveInfo;
		};
	
		Type type;
		WindowHandle eventWindow;
	public:
		Event() noexcept;
		
		bool operator==(const Type other) const noexcept
		{
			return type == other;
		}

		bool operator!=(const Type other) const noexcept
		{
			return type != other;
		}

		operator Type() const noexcept
		{
			return type;
		}
	};
}

#endif
