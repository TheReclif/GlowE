#include "Window.h"

GLOWE::Color GLOWE::windowBackgroundColor = GLOWE::Color(10, 75, 175);
//10.0f, 75.0f, 175.0f

GLOWE::VideoMode GLOWE::Window::getSize() const
{
	return winImpl.getSize();
}

void GLOWE::Window::setPosition(const Int2& newPos)
{
	winImpl.setPosition(newPos);
}

GLOWE::Int2 GLOWE::Window::getPosition() const
{
	return winImpl.getPosition();
}

void GLOWE::Window::setMode(const WindowMode& newMode)
{
	winImpl.setMode(newMode);
}

GLOWE::WindowMode GLOWE::Window::getMode() const
{
	return winImpl.getMode();
}

GLOWE::String GLOWE::Window::getTitle() const
{
	return winImpl.getTitle();
}

void GLOWE::Window::setTitle(const String& newTitle)
{
	winImpl.setTitle(newTitle);
}

void GLOWE::Window::setKeyRepeat(bool arg)
{
	winImpl.setKeyRepeat(arg);
}

void GLOWE::Window::grabCursor()
{
	winImpl.grabCursor();
}

void GLOWE::Window::ungrabCursor()
{
	winImpl.ungrabCursor();
}

void GLOWE::Window::setCursorGrab(bool arg)
{
	winImpl.setCursorGrab(arg);
}

void GLOWE::Window::showCursor()
{
	winImpl.showCursor();
}

void GLOWE::Window::hideCursor()
{
	winImpl.hideCursor();
}

void GLOWE::Window::requestFocus()
{
	winImpl.requestFocus();
}

void GLOWE::Window::setIsResizable(const bool arg)
{
	winImpl.setIsResizable(arg);
}

void GLOWE::Window::waitForEvent()
{
	static EventMgr& eventMgr = getInstance<EventMgr>();
	static InputMgr& inputMgr = getInstance<InputMgr>();

	winImpl.processEvents();
	inputMgr.updateJoysticks();

	while (eventMgr.checkIsEmpty(&winImpl))
	{
		Clock::waitForGivenTime(Time::fromMilliseconds(5));
		winImpl.processEvents();
		inputMgr.updateJoysticks();
	}
}

bool GLOWE::Window::peekEvents()
{
	static EventMgr& eventMgr = getInstance<EventMgr>();
	static InputMgr& inputMgr = getInstance<InputMgr>();

	winImpl.processEvents();
	inputMgr.updateJoysticks();

	return !eventMgr.checkIsEmpty(&winImpl);
}

bool GLOWE::Window::checkIsKeyRepeatEnabled() const
{
	return winImpl.checkIsKeyRepeatEnabled();
}

bool GLOWE::Window::checkIsCreated() const
{
	return winImpl.checkIsCreated();
}

bool GLOWE::Window::checkIsResizable() const
{
	return winImpl.checkIsResizable();
}

bool GLOWE::Window::hasFocus() const
{
	return winImpl.checkHasFocus();
}

void GLOWE::Window::display()
{
}

GLOWE::WindowHandle GLOWE::Window::getWindowHandle() const
{
	return winImpl.getWindowHandle();
}

bool GLOWE::operator==(const Window& wnd, const WindowHandle& handle)
{
	return wnd.getWindowHandle() == handle;
}

bool GLOWE::operator!=(const Window& wnd, const WindowHandle& handle)
{
	return wnd.getWindowHandle() != handle;
}

bool GLOWE::operator==(const WindowHandle& handle, const Window& wnd)
{
	return wnd.getWindowHandle() == handle;
}

bool GLOWE::operator!=(const WindowHandle& handle, const Window& wnd)
{
	return wnd.getWindowHandle() != handle;
}

void GLOWE::Window::waitForAnyEvent()
{
	winImpl.waitForAnyEvent();
}

void GLOWE::Window::onResize(const size_t newHeight, const size_t newWidth)
{
}

GLOWE::Window::Window(Window&& arg) noexcept
	: winImpl(std::move(arg.winImpl))
{
}

GLOWE::Window::~Window()
{
	destroy();
}

void GLOWE::Window::create(const VideoMode& size, const String& title, const Int2& position, const WindowMode& wndMode)
{
	VideoMode trueSize = size;
	if (size.x < 8 || size.y < 8)
	{
		trueSize.x = trueSize.y = 8; // Choose a reasonable default window size.
		WarningThrow(false, u8"Window and buffer size is too small. Taking 8 as a reasonable default instead.");
	}
	getInstance<EventListenersMgr>().addListener(makeUniquePtr<WindowEventCallback>(this));
	winImpl.create(trueSize, title, position, wndMode);
}

void GLOWE::Window::destroy()
{
	winImpl.destroy();
}

void GLOWE::Window::resize(const VideoMode& newSize)
{
	winImpl.resize(newSize);
}

void GLOWE::Window::resize(const VideoMode& newSize, const WindowMode& newMode)
{
	winImpl.resize(newSize, newMode);
}

GLOWE::Window::WindowEventCallback::WindowEventCallback(Window* arg)
	: wnd(arg)
{
}

void GLOWE::Window::WindowEventCallback::proccessEvent(const WindowImplementation* const window, const Event& event)
{
	if (window == &wnd->winImpl && event == Event::Type::Resized)
	{
		wnd->onResize(event.resizeInfo.newHeight, event.resizeInfo.newWidth);
	}
}
