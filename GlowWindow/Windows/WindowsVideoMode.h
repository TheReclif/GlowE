#pragma once
#ifndef WINDOWS_VIDEOMODE_INCLUDED
#define WINDOWS_VIDEOMODE_INCLUDED

#include "../../GlowSystem/Utility.h"

#ifdef GLOWE_COMPILE_FOR_WINDOWS
#include "../Uniform/VideoMode.h"

namespace GLOWE
{
	namespace Windows
	{
		VideoMode getMonitorVideoMode();
		void getAvailableVideoModes(Vector<VideoMode>& vecOut);
	}
}
#endif

#endif
