#include "WindowsVideoMode.h"

GLOWE::VideoMode GLOWE::Windows::getMonitorVideoMode()
{
	DEVMODE devmode;

	devmode.dmSize = sizeof(devmode);
	EnumDisplaySettingsW(nullptr, ENUM_CURRENT_SETTINGS, &devmode);

	return VideoMode(devmode.dmPelsWidth, devmode.dmPelsHeight, devmode.dmBitsPerPel, devmode.dmDisplayFrequency);
}

void GLOWE::Windows::getAvailableVideoModes(Vector<VideoMode>& vecOut)
{
	vecOut.clear();

	DEVMODE devmode;
	VideoMode temp;

	devmode.dmSize = sizeof(devmode);
	for (int x = 0; EnumDisplaySettingsW(nullptr, x, &devmode); ++x)
	{
		temp = VideoMode(devmode.dmPelsWidth, devmode.dmPelsHeight, devmode.dmBitsPerPel, devmode.dmDisplayFrequency);

		if (std::find(vecOut.begin(), vecOut.end(), temp) == vecOut.end())
		{
			vecOut.push_back(temp);
		}
	}
}
