#include "WindowsWindowImplementation.h"
#include "../Uniform/InputMgr.h"
#include "../Uniform/Window.h"
#include "../Uniform/Cursor.h"

#include <windowsx.h>

#ifndef XBUTTON1
#define XBUTTON1 0x0001
#endif

#ifndef XBUTTON2
#define XBUTTON2 0x0002
#endif

#ifndef WM_MOUSEHWHEEL
#define WM_MOUSEHWHEEL 0x020E
#endif

#ifndef MAPVK_VK_TO_VSC
#define MAPVK_VK_TO_VSC (0)
#endif

static GLOWE::Atomic<std::size_t> windowsInExistence = 0;

int GLOWE::Windows::WindowsWindowImplementation::nCmdShow = SW_SHOW;

void getMonitorCenterPos(const int width, const int height, GLOWE::Int2& pos)
{
	RECT rect{};
	GetClientRect(GetDesktopWindow(), &rect);
	pos[0] = (rect.right / 2) - (width / 2);
	pos[1] = (rect.bottom / 2) - (height / 2);
}

bool GLOWE::Windows::WindowsWindowImplementation::checkIsDuringMoveSize() const
{
	return isDuringMoveSize;
}

bool GLOWE::Windows::WindowsWindowImplementation::checkIsKeyRepeatEnabled() const
{
	return keyRepeatEnabled;
}

bool GLOWE::Windows::WindowsWindowImplementation::checkIsCreated() const
{
	return isCreated;
}

bool GLOWE::Windows::WindowsWindowImplementation::checkHasFocus() const
{
	return hasFocus;
}

void GLOWE::Windows::WindowsWindowImplementation::display()
{
	UpdateWindow(window);
}

GLOWE::WindowHandle GLOWE::Windows::WindowsWindowImplementation::getWindowHandle() const
{
	return window;
}

LRESULT CALLBACK GLOWE::Windows::WindowsWindowImplementation::windowMainWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static String::Utf16Char firstCharPart = 0;
	static bool wasLostFocusFullscreen = false;
	static VideoMode lastMode;

	Event ev;

	if (msg == WM_CREATE)
	{
		const LONG_PTR tempPtr = reinterpret_cast<LONG_PTR>(reinterpret_cast<CREATESTRUCT*>(lParam)->lpCreateParams);
		SetWindowLongPtr(hWnd, GWLP_USERDATA, tempPtr);
	}

	Windows::WindowsWindowImplementation* windowImpl = hWnd ? reinterpret_cast<Windows::WindowsWindowImplementation*>(GetWindowLongPtr(hWnd, GWLP_USERDATA)) : nullptr;

	if (windowImpl)
	{
		ev.eventWindow = hWnd;

		switch (msg)
		{
		case WM_DESTROY:
		{
			windowImpl->destroyCleanup();
		}
		break;
		case WM_CLOSE:
		{
			ev.type = Event::Type::Closed;
			windowImpl->eventQueue.push(ev);
		}
		break;
		case WM_SETFOCUS:
		{
			windowImpl->setCursorGrab(windowImpl->isCursorGrabbed);

			if (wasLostFocusFullscreen)
			{
				windowImpl->resize(lastMode, WindowMode::TrueFullscreen);
				wasLostFocusFullscreen = false;
			}
			windowImpl->hasFocus = true;

			ev.type = Event::Type::GainedFocus;
			windowImpl->eventQueue.push(ev);
		}
		break;
		case WM_SETCURSOR:
		{
			if ((LOWORD(lParam) == HTCLIENT) && getInstance<Cursor>()._onSetCursor())
			{
				return TRUE;
			}
			else
			{
				return DefWindowProc(hWnd, msg, wParam, lParam);
			}
		}
		break;
		case WM_KILLFOCUS:
		{
			windowImpl->ungrabCursor();

			if (windowImpl->getMode() == WindowMode::TrueFullscreen)
			{
				lastMode = windowImpl->getSize();
				windowImpl->resize(windowImpl->getSize(), WindowMode::Windowed);
				wasLostFocusFullscreen = true;
			}
			windowImpl->hasFocus = false;

			ev.type = Event::Type::LostFocus;
			windowImpl->eventQueue.push(ev);
		}
		break;
		case WM_GETMINMAXINFO:
		{
			MINMAXINFO* temp = (MINMAXINFO*)lParam;
			temp->ptMaxTrackSize.x = 50000;
			temp->ptMaxTrackSize.y = 50000;
		}
		break;
		case WM_ENTERSIZEMOVE:
		{
			// TODO: Modal size-move loop here causes the DefWindowProc to not return.
			// See:
			// https://github.com/glfw/glfw/pull/1426
			// https://microsoft.public.win32.programmer.ui.narkive.com/MzflQf5e/message-pumping-and-moving-sizing-modal-loops
			// https://chromium.googlesource.com/chromium/src/+/master/ui/views/win/hwnd_message_handler.cc#3187
			// TODO (Probable sollution): Create and manage window on an another, dedicated thread and pump the engine's message queue asynchronously. Change PeekMessage to GetMessage to better utilise the dedicated thread.
			windowImpl->isDuringMoveSize = true;
			windowImpl->rememberedWindowDragSize = VideoMode(0, 0, 0);
			RECT rect;
			GetWindowRect(hWnd, &rect);
			ev.type = Event::Type::EnterSizeMove;
			ev.sizeMoveInfo.top = rect.top;
			ev.sizeMoveInfo.left = rect.left;
			ev.sizeMoveInfo.bottom = rect.bottom;
			ev.sizeMoveInfo.right = rect.right;
			windowImpl->eventQueue.push(ev);
		}
		break;
		case WM_EXITSIZEMOVE:
		{
			windowImpl->isDuringMoveSize = false;

			RECT rect;
			GetWindowRect(hWnd, &rect);
			ev.type = Event::Type::ExitSizeMove;
			ev.sizeMoveInfo.top = rect.top;
			ev.sizeMoveInfo.left = rect.left;
			ev.sizeMoveInfo.bottom = rect.bottom;
			ev.sizeMoveInfo.right = rect.right;
			windowImpl->eventQueue.push(ev);

			if (windowImpl->rememberedWindowDragSize.x != 0 && windowImpl->rememberedWindowDragSize.y != 0)
			{
				ev.type = Event::Type::Resized;
				ev.resizeInfo.newWidth = windowImpl->rememberedWindowDragSize.x;
				ev.resizeInfo.newHeight = windowImpl->rememberedWindowDragSize.y;

				windowImpl->eventQueue.push(ev);
			}
		}
		break;
		case WM_SIZE:
		if (!windowImpl->isDuringMoveSize)
		{
			ev.type = Event::Type::Resized;
			ev.resizeInfo.newWidth = static_cast<size_t>(LOWORD(lParam));
			ev.resizeInfo.newHeight = static_cast<size_t>(HIWORD(lParam));
			
			windowImpl->eventQueue.push(ev);
		}
		else
		{
			windowImpl->rememberedWindowDragSize = VideoMode(static_cast<size_t>(LOWORD(lParam)), static_cast<size_t>(HIWORD(lParam)), 32);
		}
		break;
		case WM_CHAR:
			if (windowImpl->keyRepeatEnabled || ((lParam & ((1U << 16U) - 1U)) <= 1))
			{
				String::Utf32Char character{ static_cast<String::Utf32Char>(wParam) };

				if ((character >= 0xD800) && (character <= 0xDBFF))
				{
					firstCharPart = static_cast<String::Utf16Char>(character);
				}
				else
				{
					if ((character >= 0xDC00) && (character <= 0xDFFF))
					{
						const String::Utf16Char temp[2] = { firstCharPart, String::Utf16Char{ static_cast<String::Utf16Char>(character) } };
						String::Utf16String tempStr16(static_cast<const String::Utf16Char*>(temp), 2);
						String::Utf32String tempResult;
						Utf<32>::fromUtf16(tempStr16, tempResult);
						character = tempResult.front();
						firstCharPart = 0;
					}

					if (isPrintable(character))
					{
						ev.type = Event::Type::TextEntered;

						ev.textEnteredInfo.characterWritten = character;
						
						windowImpl->eventQueue.push(ev);
					}
				}
			}
			break;
		case WM_KEYDOWN:
		case WM_SYSKEYDOWN:
			if (windowImpl->keyRepeatEnabled || (HIWORD(lParam) & KF_REPEAT) == 0)
			{
				ev.type = Event::Type::KeyPressed;

				ev.keyInfo.key = virtualKeyToKeyCode(lParam, wParam);
				ev.keyInfo.isAltPressed = HIWORD(GetAsyncKeyState(VK_MENU));
				ev.keyInfo.isControlPressed = HIWORD(GetAsyncKeyState(VK_CONTROL));
				ev.keyInfo.isShiftPressed = HIWORD(GetAsyncKeyState(VK_SHIFT));
				ev.keyInfo.isSystemPressed = HIWORD(GetAsyncKeyState(VK_LWIN)) || HIWORD(GetAsyncKeyState(VK_RWIN));
				
				windowImpl->eventQueue.push(ev);
			}
			break;
		case WM_KEYUP:
		case WM_SYSKEYUP:
		{
			ev.type = Event::Type::KeyReleased;

			ev.keyInfo.key = virtualKeyToKeyCode(lParam, wParam);
			ev.keyInfo.isAltPressed = HIWORD(GetAsyncKeyState(VK_MENU));
			ev.keyInfo.isControlPressed = HIWORD(GetAsyncKeyState(VK_CONTROL));
			ev.keyInfo.isShiftPressed = HIWORD(GetAsyncKeyState(VK_SHIFT));
			ev.keyInfo.isSystemPressed = HIWORD(GetAsyncKeyState(VK_LWIN)) || HIWORD(GetAsyncKeyState(VK_RWIN));
			
			windowImpl->eventQueue.push(ev);
		}
		break;
		case WM_MOUSEWHEEL:
		{
			const std::int16_t delta{ GET_WHEEL_DELTA_WPARAM(wParam) };

			ev.type = Event::Type::MouseWheelScrolled;

			ev.mouseWheelScrollInfo.wheel = GLOWE::Mouse::Wheel::Vertical;
			ev.mouseWheelScrollInfo.delta = static_cast<float>(delta) / 120.0f;
			
			windowImpl->eventQueue.push(ev);
		}
		break;
		case WM_MOUSEHWHEEL:
		{
			const int16_t delta{ GET_WHEEL_DELTA_WPARAM(wParam) };

			ev.type = Event::Type::MouseWheelScrolled;

			ev.mouseWheelScrollInfo.wheel = GLOWE::Mouse::Wheel::Horizontal;
			ev.mouseWheelScrollInfo.delta = -static_cast<float>(delta) / 120.0f;
			
			windowImpl->eventQueue.push(ev);
		}
		break;
		case WM_LBUTTONDOWN:
		{
			ev.type = Event::Type::MouseButtonPressed;
			ev.mouseButtonInfo.button = Mouse::Button::Left;
			ev.mouseButtonInfo.x = GET_X_LPARAM(lParam);
			ev.mouseButtonInfo.y = GET_Y_LPARAM(lParam);
			
			windowImpl->eventQueue.push(ev);
			SetFocus(hWnd); // Added here so that the window can be refocused when the user clicks on it.
		}
		break;
		case WM_LBUTTONUP:
		{
			ev.type = Event::Type::MouseButtonReleased;
			ev.mouseButtonInfo.button = Mouse::Button::Left;
			ev.mouseButtonInfo.x = GET_X_LPARAM(lParam);
			ev.mouseButtonInfo.y = GET_Y_LPARAM(lParam);
			
			windowImpl->eventQueue.push(ev);
		}
		break;
		case WM_RBUTTONDOWN:
		{
			ev.type = Event::Type::MouseButtonPressed;
			ev.mouseButtonInfo.button = Mouse::Button::Right;
			ev.mouseButtonInfo.x = GET_X_LPARAM(lParam);
			ev.mouseButtonInfo.y = GET_Y_LPARAM(lParam);
			
			windowImpl->eventQueue.push(ev);
		}
		break;
		case WM_RBUTTONUP:
		{
			ev.type = Event::Type::MouseButtonReleased;
			ev.mouseButtonInfo.button = Mouse::Button::Right;
			ev.mouseButtonInfo.x = GET_X_LPARAM(lParam);
			ev.mouseButtonInfo.y = GET_Y_LPARAM(lParam);
			
			windowImpl->eventQueue.push(ev);
		}
		break;
		case WM_MBUTTONDOWN:
		{
			ev.type = Event::Type::MouseButtonPressed;
			ev.mouseButtonInfo.button = Mouse::Button::Middle;
			ev.mouseButtonInfo.x = GET_X_LPARAM(lParam);
			ev.mouseButtonInfo.y = GET_Y_LPARAM(lParam);
			
			windowImpl->eventQueue.push(ev);
		}
		break;
		case WM_MBUTTONUP:
		{
			ev.type = Event::Type::MouseButtonReleased;
			ev.mouseButtonInfo.button = Mouse::Button::Middle;
			ev.mouseButtonInfo.x = GET_X_LPARAM(lParam);
			ev.mouseButtonInfo.y = GET_Y_LPARAM(lParam);
			
			windowImpl->eventQueue.push(ev);
		}
		break;
		case WM_XBUTTONDOWN:
		{
			ev.type = Event::Type::MouseButtonPressed;
			ev.mouseButtonInfo.button = HIWORD(wParam) == XBUTTON1 ? Mouse::Button::XButton1 : Mouse::Button::XButton2;
			ev.mouseButtonInfo.x = GET_X_LPARAM(lParam);
			ev.mouseButtonInfo.y = GET_Y_LPARAM(lParam);
			
			windowImpl->eventQueue.push(ev);
		}
		break;
		case WM_XBUTTONUP:
		{
			ev.type = Event::Type::MouseButtonReleased;
			ev.mouseButtonInfo.button = HIWORD(wParam) == XBUTTON1 ? Mouse::Button::XButton1 : Mouse::Button::XButton2;
			ev.mouseButtonInfo.x = GET_X_LPARAM(lParam);
			ev.mouseButtonInfo.y = GET_Y_LPARAM(lParam);

			windowImpl->eventQueue.push(ev);
		}
		break;
		case WM_MOUSELEAVE:
			if (windowImpl->mouseIn)
			{
				windowImpl->mouseIn = false;
				ev.type = Event::Type::MouseLeft;
				windowImpl->eventQueue.push(ev);
			}
			break;
		case WM_MOUSEMOVE:
		{
			const int x = GET_X_LPARAM(lParam);
			const int y = GET_Y_LPARAM(lParam);

			ev.mouseMoveInfo.x = x;
			ev.mouseMoveInfo.y = y;
			
			if ((wParam & (MK_LBUTTON | MK_MBUTTON | MK_RBUTTON | MK_XBUTTON1 | MK_XBUTTON2)) == 0)
			{
				if (GetCapture() == hWnd)
				{
					ReleaseCapture();
				}
				else
				if (GetCapture() != hWnd)
				{
					SetCapture(hWnd);
				}
			}

			RECT area;
			GetClientRect(hWnd, &area);

			if ((x < area.left) || (x > area.right) || (y < area.top) || (y > area.bottom))
			{
				if (windowImpl->mouseIn)
				{
					windowImpl->mouseIn = false;

					windowImpl->setMouseTracking(false);
					ev.type = Event::Type::MouseLeft;
					windowImpl->eventQueue.push(ev);
				}
			}
			else
			{
				if (!windowImpl->mouseIn)
				{
					windowImpl->mouseIn = true;

					windowImpl->setMouseTracking(true);
					ev.type = Event::Type::MouseEntered;
					windowImpl->eventQueue.push(ev);
				}
			}

			ev.type = Event::Type::MouseMoved;
			windowImpl->eventQueue.push(ev);
		}
		break;
		case WM_DEVICECHANGE:
			if (wParam == DBT_DEVNODES_CHANGED)
			{
				getInstance<InputMgr>().updateJoysticks();
			}
			break;
		default:
			return DefWindowProc(hWnd, msg, wParam, lParam);
		}
	}
	else
	if (hWnd)
	{
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}

	return TRUE;
}

void GLOWE::Windows::WindowsWindowImplementation::initializeWindowClass()
{
	if (windowsInExistence++ == 0)
	{
		WNDCLASSEXW classDescriptor{};
		ZeroMemory(&classDescriptor, sizeof(classDescriptor));

		{
			const COLORREF color = RGB(windowBackgroundColor.rgba[0], windowBackgroundColor.rgba[1], windowBackgroundColor.rgba[2]);
			classDescriptor.hbrBackground = CreateSolidBrush(color);
		}
		
		classDescriptor.cbSize = sizeof(classDescriptor);
		classDescriptor.hInstance = GetModuleHandle(nullptr);
		classDescriptor.cbWndExtra = 0;
		classDescriptor.lpszClassName = className;
		classDescriptor.lpfnWndProc = windowMainWndProc;
		classDescriptor.lpszMenuName = nullptr;
		classDescriptor.cbClsExtra = 0;
		classDescriptor.hIcon = LoadIcon(classDescriptor.hInstance, IDI_APPLICATION);
		classDescriptor.hIconSm = LoadIcon(classDescriptor.hInstance, IDI_APPLICATION);
		classDescriptor.hCursor = LoadCursor(nullptr, IDC_ARROW);
		classDescriptor.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

		if (!RegisterClassExW(&classDescriptor))
		{
			ErrorThrow(false);
			return;
		}
	}
}

void GLOWE::Windows::WindowsWindowImplementation::setMouseTracking(bool arg)
{
	TRACKMOUSEEVENT tempEvent;
	tempEvent.cbSize = sizeof(tempEvent);
	tempEvent.hwndTrack = window;
	tempEvent.dwHoverTime = HOVER_DEFAULT;
	tempEvent.dwFlags = arg ? TME_LEAVE : TME_CANCEL;
	TrackMouseEvent(&tempEvent);
}

void GLOWE::Windows::WindowsWindowImplementation::destroyCleanup()
{
	if (getMode() == WindowMode::TrueFullscreen)
	{
		ChangeDisplaySettings(nullptr, 0);
	}

	setMouseTracking(false);

	showCursor();

	ReleaseCapture();
}

void GLOWE::Windows::WindowsWindowImplementation::waitForAnyEvent()
{
	// TODO: Async message queue invalidated the concept of waiting for any events to process.
}

void GLOWE::Windows::WindowsWindowImplementation::setInitialWindowState(const int & arg)
{
	nCmdShow = arg;
}

void GLOWE::Windows::WindowsWindowImplementation::goFullscreen()
{
	const VideoMode size = VideoMode::getMonitorVideoMode();

	switch (windowMode)
	{
	case WindowMode::Windowed:
		SetWindowLong(window, GWL_STYLE, WS_VISIBLE | WS_POPUP); //Reset window style to default windowed style

		SetWindowPos(window, HWND_TOP, 0, 0, size.x, size.y, SWP_SHOWWINDOW | SWP_FRAMECHANGED);
		break;
	case WindowMode::Fullscreen:
		return;
	case WindowMode::TrueFullscreen:
		ChangeDisplaySettings(nullptr, 0); //Disable full screen status

		SetWindowLong(window, GWL_STYLE, WS_VISIBLE | WS_POPUP); //Reset window style to default windowed style

		SetWindowPos(window, HWND_TOP, 0, 0, size.x, size.y, SWP_SHOWWINDOW | SWP_FRAMECHANGED);
		break;
	}

	windowMode = WindowMode::Fullscreen;
}

void GLOWE::Windows::WindowsWindowImplementation::goWindowed(const VideoMode & size, const Int2 & position)
{
	VideoMode siz = size;

	if (size.x == 0 || size.y == 0)
	{
		siz = VideoMode::getMonitorVideoMode();
	}
	Int2 finalPos = position;
	if (position[0] == AutoCenter || position[1] == AutoCenter)
	{
		getMonitorCenterPos(siz.x, siz.y, finalPos);
	}

	RECT transformedSize{};
	switch (windowMode)
	{
	case WindowMode::Windowed:
		return;
	case WindowMode::Fullscreen:
		SetWindowLong(window, GWL_STYLE, WS_VISIBLE | WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU | (isResizable ? WS_SIZEBOX : 0)); //Reset window style to default windowed style

		transformedSize.top = transformedSize.left = 0;
		transformedSize.right = siz.x;
		transformedSize.bottom = siz.y;

		AdjustWindowRect(&transformedSize, GetWindowLong(window, GWL_STYLE), FALSE);

		SetWindowPos(window, HWND_TOP, finalPos[0], finalPos[1], transformedSize.right - transformedSize.left, transformedSize.bottom - transformedSize.top, SWP_SHOWWINDOW | SWP_FRAMECHANGED);

		break;
	case WindowMode::TrueFullscreen:
		ChangeDisplaySettings(nullptr, 0); //Disable full screen status

		SetWindowLong(window, GWL_STYLE, WS_VISIBLE | WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU | (isResizable ? WS_SIZEBOX : 0)); //Reset window style to default windowed style

		transformedSize.top = transformedSize.left = 0;
		transformedSize.right = siz.x;
		transformedSize.bottom = siz.y;

		AdjustWindowRect(&transformedSize, GetWindowLong(window, GWL_STYLE), FALSE);

		SetWindowPos(window, HWND_TOP, finalPos[0], finalPos[1], transformedSize.right - transformedSize.left, transformedSize.bottom - transformedSize.top, SWP_SHOWWINDOW | SWP_FRAMECHANGED);
		break;
	}

	windowMode = WindowMode::Windowed;
}

void GLOWE::Windows::WindowsWindowImplementation::goTrueFullscreen(const VideoMode & size)
{
	VideoMode siz = size;
	Vector<VideoMode> vecVideoMode;
	DEVMODE devmode;

	VideoMode::getAvailableVideoModes(vecVideoMode);

	ZeroMemory(&devmode, sizeof(devmode));

	devmode.dmSize = sizeof(devmode);
	devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

	if (size.x == 0 || size.y == 0 || (std::find(vecVideoMode.begin(), vecVideoMode.end(), size))==vecVideoMode.end())
	{
		siz = VideoMode::getMonitorVideoMode();
	}

	devmode.dmBitsPerPel = siz.bpp;
	devmode.dmPelsWidth = siz.x;
	devmode.dmPelsHeight = siz.y;

	switch (windowMode)
	{
	case WindowMode::Windowed:
	case WindowMode::Fullscreen:
		if (ChangeDisplaySettingsW(&devmode, CDS_FULLSCREEN) == DISP_CHANGE_SUCCESSFUL)
		{
			SetWindowLong(window, GWL_STYLE, WS_VISIBLE | WS_POPUP); //Reset window style to default windowed style

			SetWindowPos(window, HWND_TOP, 0, 0, siz.x, siz.y, SWP_SHOWWINDOW | SWP_FRAMECHANGED);
		}

		break;
	case WindowMode::TrueFullscreen:
		return;
	}

	windowMode = WindowMode::TrueFullscreen;
}

void GLOWE::Windows::WindowsWindowImplementation::setIsResizableImpl()
{
	if (windowMode != WindowMode::Windowed)
	{
		return;
	}

	auto currentStyle = GetWindowLong(window, GWL_STYLE);
	if (isResizable)
	{
		currentStyle |= WS_SIZEBOX;
	}
	else
	{
		currentStyle &= ~static_cast<decltype(currentStyle)>(WS_SIZEBOX);
	}
	SetWindowLong(window, GWL_STYLE, currentStyle);
}

GLOWE::Windows::WindowsWindowImplementation::WindowsWindowImplementation()
	: window(nullptr), className(L"GlowWindowClass"), keyRepeatEnabled(false), mouseIn(false),
	isCursorVisible(true), isDuringMoveSize(false), isCursorGrabbed(false)
{
}

GLOWE::Windows::WindowsWindowImplementation::WindowsWindowImplementation(WindowsWindowImplementation&& arg) noexcept
	: window(arg.window), rememberedWindowDragSize(arg.rememberedWindowDragSize), className(arg.className), keyRepeatEnabled(arg.keyRepeatEnabled), mouseIn(arg.mouseIn), isCursorVisible(arg.isCursorVisible), hasFocus(arg.hasFocus), isDuringMoveSize(arg.isDuringMoveSize), isCursorGrabbed(arg.isCursorGrabbed)
{
}

GLOWE::Windows::WindowsWindowImplementation::~WindowsWindowImplementation()
{
	WindowsWindowImplementation::destroy();
}

void GLOWE::Windows::WindowsWindowImplementation::create(const VideoMode & size, const String& title, const Int2 & position, const WindowMode & wndMode)
{
	if (!isCreated)
	{
		initializeWindowClass();

		DWORD style = 0;
		VideoMode siz = size;
		Int2 pos = position;

		switch (wndMode)
		{
		case WindowMode::Windowed:
			style = WS_VISIBLE;
			style |= WS_CAPTION;
			style |= WS_MINIMIZEBOX;
			style |= WS_SYSMENU;
			break;
		case WindowMode::TrueFullscreen:
		case WindowMode::Fullscreen:
			style = WS_VISIBLE;
			style |= WS_POPUP;
			break;
		}

		if (siz.x == 0 || siz.y == 0)
		{
			HMONITOR monitor = MonitorFromWindow(HWND_DESKTOP, MONITOR_DEFAULTTONEAREST);
			MONITORINFO mi = { sizeof(mi) };

			if (!GetMonitorInfoW(monitor, &mi))
			{
				WarningThrow(false, String(u8"Unable to get monitor info. Using default window: 800x600."));
				siz.x = 800;
				siz.y = 600;

				pos = Int2{ 200, 200 };

				style = WS_VISIBLE;
				style |= WS_CAPTION;
				style |= WS_MINIMIZEBOX;
				style |= WS_SYSMENU;
			}
			else
			{
				siz.x = mi.rcWork.right - mi.rcWork.left;
				siz.y = mi.rcWork.bottom - mi.rcWork.top;
			}
		}

		RECT transformedSize;
		transformedSize.top = transformedSize.left = 0;
		transformedSize.right = siz.x;
		transformedSize.bottom = siz.y;

		AdjustWindowRect(&transformedSize, style, FALSE);

		const Int2 finalSize{ transformedSize.right - transformedSize.left, transformedSize.bottom - transformedSize.top };
		Int2 finalPos = pos;
		if (pos[0] == AutoCenter || pos[1] == AutoCenter)
		{
			getMonitorCenterPos(finalSize[0], finalSize[1], finalPos);
		}

		Promise<void> windowErrorSignal;
		const auto future = windowErrorSignal.get_future();
		dedicatedThread.beginThread([this, finalPos, finalSize, style, title, wndMode, siz, pos, &windowErrorSignal](Thread::Pointer threadPtr)
			{
				shouldEnd = false;
				{
					const auto threadName = ("\"" + title + "\" Window Message Thread").toWide();
					threadPtr->setThreadDescription(threadName.c_str());
				}
				getInstance<ConcurrentGC>().attachThread();
				window = CreateWindowExW(0, className, title.toWide().c_str(), style, finalPos[0], finalPos[1], finalSize[0], finalSize[1], nullptr, nullptr, GetModuleHandleW(nullptr), this);

				if (!window)
				{
					windowErrorSignal.set_exception(std::make_exception_ptr(StringException("Window creation failed. GetLastError: " + toString(GetLastError()))));
					return;
				}

				windowMode = wndMode;

				switch (wndMode)
				{
				case WindowMode::Windowed:
					goWindowed(siz, pos);
					break;
				case WindowMode::TrueFullscreen:
					goTrueFullscreen(siz);
					break;
				case WindowMode::Fullscreen:
					goFullscreen();
					break;
				default:
					break;
				}

				ShowWindow(window, nCmdShow);
				UpdateWindow(window);

				isCreated = true;

				windowErrorSignal.set_value();

				threadMessagePump();

				// TODO: close/destroy
				// Done? Test it out
				goWindowed(VideoMode(800, 600, 32));
				DestroyWindow(window);
				isCreated = false;
				window = nullptr;
				windowMode = WindowMode::Default;
				if (--windowsInExistence == 0)
				{
					UnregisterClassW(className, GetModuleHandle(nullptr));
				}
			});
		
		future.wait();
	}
}

void GLOWE::Windows::WindowsWindowImplementation::threadMessagePump()
{
	MSG msg;
	BOOL ret;
	while (!shouldEnd && (ret = GetMessageW(&msg, window, 0, 0)) != 0)
	{
		if (ret == -1)
		{
			// TODO: Raise a soft error of some sort. Or not.
			return;
		}
		else
		{
			TranslateMessage(&msg);
			DispatchMessageW(&msg);
		}
	}
}

void GLOWE::Windows::WindowsWindowImplementation::destroy()
{
	if (checkIsCreated())
	{
		isCreated = false;
		shouldEnd = true;
		PostMessageW(window, WM_NULL, 0, 0);
		dedicatedThread.stopThread();
	}
}

void GLOWE::Windows::WindowsWindowImplementation::resize(const VideoMode & newSize)
{
	switch (windowMode)
	{
	case WindowMode::Fullscreen:
		break;
	case WindowMode::TrueFullscreen:
		goWindowed(VideoMode(800, 600, 32));
		goTrueFullscreen(newSize);
		break;
	case WindowMode::Windowed:
		{
			RECT transformedSize;

			transformedSize.top = transformedSize.left = 0;
			transformedSize.right = newSize.x;
			transformedSize.bottom = newSize.y;

			AdjustWindowRect(&transformedSize, GetWindowLong(window, GWL_STYLE), FALSE);
			SetWindowPos(window, nullptr, 0, 0, transformedSize.right - transformedSize.left, transformedSize.bottom - transformedSize.top, SWP_NOMOVE | SWP_SHOWWINDOW);
		}
		break;
	}
}

void GLOWE::Windows::WindowsWindowImplementation::resize(const VideoMode & newSize, const WindowMode & newMode)
{
	switch (newMode)
	{
	case WindowMode::Windowed:
		goWindowed(newSize);
		break;
	case WindowMode::Fullscreen:
		goFullscreen();
		break;
	case WindowMode::TrueFullscreen:
		goTrueFullscreen(newSize);
		break;
	}
}

GLOWE::VideoMode GLOWE::Windows::WindowsWindowImplementation::getSize() const
{
	RECT rect;
	GetClientRect(window, &rect);
	return VideoMode(rect.right, rect.bottom, 32);
}

void GLOWE::Windows::WindowsWindowImplementation::setPosition(const Int2 & newPos)
{
	Int2 finalPos = newPos;
	if (newPos[0] == AutoCenter || newPos[1] == AutoCenter)
	{
		const auto videoMode = getSize();
		getMonitorCenterPos(videoMode.x, videoMode.y, finalPos);
	}

	SetWindowPos(window, HWND_TOP, finalPos[0], finalPos[1], 0, 0, SWP_NOSIZE);
}

GLOWE::Int2 GLOWE::Windows::WindowsWindowImplementation::getPosition() const
{
	RECT rect;
	GetWindowRect(window, &rect);
	return Int2{ rect.left, rect.top };
}

void GLOWE::Windows::WindowsWindowImplementation::setMode(const WindowMode & newMode)
{
	switch (newMode)
	{
	case WindowMode::Windowed:
		goWindowed(VideoMode(0, 0, 32));
		break;
	case WindowMode::Fullscreen:
		goFullscreen();
		break;
	case WindowMode::TrueFullscreen:
		goTrueFullscreen(VideoMode(0, 0, 32));
		break;
	}
}

GLOWE::WindowMode GLOWE::Windows::WindowsWindowImplementation::getMode() const
{
	return windowMode;
}

GLOWE::String GLOWE::Windows::WindowsWindowImplementation::getTitle() const
{
	wchar_t result[512] = L"";
	GetWindowTextW(window, static_cast<wchar_t*>(result), 512);
	return String(static_cast<wchar_t*>(result));
}

void GLOWE::Windows::WindowsWindowImplementation::setTitle(const String & newTitle)
{
	String::WideString temp;
	Utf<8>::toWide(newTitle.getString(), temp);
	SetWindowTextW(window, temp.c_str());
}

void GLOWE::Windows::WindowsWindowImplementation::setKeyRepeat(bool arg)
{
	keyRepeatEnabled = arg;
}

void GLOWE::Windows::WindowsWindowImplementation::grabCursor()
{
	RECT area;
	GetClientRect(window, &area);
	MapWindowPoints(window, nullptr, reinterpret_cast<LPPOINT>(&area), 2);
	ClipCursor(&area);
}

void GLOWE::Windows::WindowsWindowImplementation::ungrabCursor()
{
	ClipCursor(nullptr);
}

void GLOWE::Windows::WindowsWindowImplementation::setCursorGrab(bool arg)
{
	if (arg)
	{
		grabCursor();
	}
	else
	{
		ungrabCursor();
	}

	isCursorGrabbed = arg;
}

void GLOWE::Windows::WindowsWindowImplementation::showCursor()
{
	if (!isCursorVisible)
	{
		isCursorVisible = true;
		ShowCursor(true);
	}
}

void GLOWE::Windows::WindowsWindowImplementation::hideCursor()
{
	if (isCursorVisible)
	{
		isCursorVisible = false;
		ShowCursor(false);
	}
}

void GLOWE::Windows::WindowsWindowImplementation::requestFocus()
{
	const DWORD thisProcessID = GetWindowThreadProcessId(window, nullptr);
	const DWORD foregroundWindowProcessID = GetWindowThreadProcessId(GetForegroundWindow(), nullptr);

	if (thisProcessID == foregroundWindowProcessID)
	{
		SetForegroundWindow(window);
	}
	else
	{
		FLASHWINFO flashInfo;
		flashInfo.cbSize = sizeof(flashInfo);
		flashInfo.dwFlags = FLASHW_TRAY;
		flashInfo.dwTimeout = 0;
		flashInfo.hwnd = window;
		flashInfo.uCount = 3;

		FlashWindowEx(&flashInfo);
	}
}

void GLOWE::Windows::WindowsWindowImplementation::processEvents()
{
	EventMgr& eventMgr = getInstance<EventMgr>();

	Event ev;
	while (eventQueue.pop(ev))
	{
		eventMgr.pushEvent(this, ev);
	}

	/*
	MSG msg;
	while (PeekMessage(&msg, window, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}*/
}
