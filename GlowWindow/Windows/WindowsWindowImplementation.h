#pragma once
#ifndef WINDOWS_WINDOWIMPLEMENTATION_INCLUDED
#define WINDOWS_WINDOWIMPLEMENTATION_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/Error.h"
#include "../../GlowSystem/String.h"
#ifdef GLOWE_COMPILE_FOR_WINDOWS
#include "../Uniform/WindowImplementation.h"
#include "../Uniform/WndHandle.h"
#include "../Uniform/EventMgr.h"

#if defined(free) && defined(_MSC_VER)
#pragma push_macro("free")
#undef free
#include <cds/container/basket_queue.h>
#include <cds/container/rwqueue.h>
#pragma pop_macro("free")
#else
#include <cds/container/basket_queue.h>
#include <cds/container/rwqueue.h>
#endif

namespace GLOWE
{
	namespace Windows
	{
		class GLOWWINDOW_EXPORT WindowsWindowImplementation
			: public WindowImplementation
		{
		private:
			WindowHandle window;
			VideoMode rememberedWindowDragSize;

			const wchar_t* className;
			
			bool keyRepeatEnabled;
			bool mouseIn;
			bool isCursorVisible;
			bool hasFocus;
			bool isDuringMoveSize;

			bool isCursorGrabbed;

			static int nCmdShow;

			Thread dedicatedThread;
			cds::container::RWQueue<Event> eventQueue;
			Atomic<bool> shouldEnd;
		private:
			void goFullscreen();
			void goWindowed(const VideoMode& size, const Int2& position = Int2{ AutoCenter, AutoCenter });
			void goTrueFullscreen(const VideoMode& size);

			void threadMessagePump();

			virtual void setIsResizableImpl() override;
		public:
			WindowsWindowImplementation();
			WindowsWindowImplementation(WindowsWindowImplementation&& arg) noexcept;
			virtual ~WindowsWindowImplementation();
			
			virtual void create(const VideoMode& size, const String& title, const Int2& position = Int2{ AutoCenter, AutoCenter }, const WindowMode& wndMode = WindowMode::Windowed) override;
			virtual void destroy() override;

			virtual void resize(const VideoMode& newSize) override;
			virtual void resize(const VideoMode& newSize, const WindowMode& newMode) override;

			virtual VideoMode getSize() const override;
			virtual void setPosition(const Int2& newPos) override;
			virtual Int2 getPosition() const override;
			virtual void setMode(const WindowMode& newMode) override;
			virtual WindowMode getMode() const override;
			virtual String getTitle() const override;
			virtual void setTitle(const String& newTitle) override;
			virtual void setKeyRepeat(bool arg) override;
			virtual void grabCursor() override;
			virtual void ungrabCursor() override;
			virtual void setCursorGrab(bool arg) override;
			virtual void showCursor() override;
			virtual void hideCursor() override;
			virtual void requestFocus() override;

			virtual void processEvents() override;

			virtual bool checkIsDuringMoveSize() const override;
			virtual bool checkIsKeyRepeatEnabled() const override;
			virtual bool checkIsCreated() const override;
			virtual bool checkHasFocus() const override;

			virtual void display() override;

			virtual WindowHandle getWindowHandle() const override;

			static LRESULT CALLBACK windowMainWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

			void initializeWindowClass();
			void setMouseTracking(bool arg);
			void destroyCleanup();

			void setCustomCallback(std::function<LRESULT(HWND, UINT, WPARAM, LPARAM, bool&)>&& callback);

			void waitForAnyEvent();

			static void setInitialWindowState(const int& arg);
		};
	}
}
#endif

#endif
