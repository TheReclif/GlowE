#include "WindowsInputImplementation.h"
#include "../Uniform/Window.h"

namespace GLOWE
{
	namespace JoystickHelpers
	{
		String getErrorString(DWORD errorCode);
		String getDeviceName(GLOWE::Joystick::JoystickId index, JOYCAPS caps);

		String getErrorString(DWORD errorCode)
		{
			PWSTR buffer = nullptr;

			if (FormatMessage(FORMAT_MESSAGE_MAX_WIDTH_MASK | FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, nullptr, errorCode, 0, buffer, 0, nullptr) == 0)
			{
				return u8"Unknown error";
			}

			String result(buffer);

			LocalFree(buffer);

			return result;
		}

		String getDeviceName(GLOWE::Joystick::JoystickId index, JOYCAPS caps)
		{
			LONG res;
			HKEY currentKey, rootKey;
			String subkey = REGSTR_PATH_JOYCONFIG;

			subkey += u8"\\";
			subkey += caps.szRegKey;
			subkey += u8"\\";
			subkey += REGSTR_KEY_JOYCURR;

			rootKey = HKEY_CURRENT_USER;

			res = RegOpenKeyEx(rootKey, Utf<8>::toWide(subkey.getString()).c_str(), NULL, KEY_READ, &currentKey);

			if (res != ERROR_SUCCESS)
			{
				rootKey = HKEY_LOCAL_MACHINE;
				res = RegOpenKeyEx(rootKey, Utf<8>::toWide(subkey.getString()).c_str(), NULL, KEY_READ, &currentKey);

				if (res != ERROR_SUCCESS)
				{
					WarningThrow(false, u8"Opening registry for joystick " + toString(index) + u8" failed.");
					return String(u8"Unknown joystick");
				}
			}

			subkey = u8"Joystick";
			subkey += toString(index + 1);
			subkey += REGSTR_VAL_JOYOEMNAME;

			wchar_t tempData[256];
			DWORD dataSize = sizeof(tempData);

			res = RegQueryValueEx(currentKey, Utf<8>::toWide(subkey.getString()).c_str(), nullptr, nullptr, (LPBYTE)tempData, &dataSize);
			RegCloseKey(currentKey);

			if (res != ERROR_SUCCESS)
			{
				WarningThrow(false, u8"Querying registry key for joystick " + toString(index) + u8" failed.");
				return String(u8"Unknown joystick");
			}

			subkey = REGSTR_PATH_JOYOEM;
			subkey += L"\\";
			subkey += String(String::WideString(tempData, dataSize / sizeof(wchar_t)));
			//subkey.getString().append(tempData, dataSize / sizeof(wchar_t));

			res = RegOpenKeyEx(rootKey, Utf<8>::toWide(subkey.getString()).c_str(), 0, KEY_READ, &currentKey);

			if (res != ERROR_SUCCESS)
			{
				WarningThrow(false, u8"Opening registry for joystick " + toString(index) + u8" failed.");
				return String(u8"Unknown joystick");
			}

			dataSize = sizeof(tempData);
			res = RegQueryValueEx(currentKey, REGSTR_VAL_JOYOEMNAME, nullptr, nullptr, (LPBYTE)tempData, &dataSize);
			RegCloseKey(currentKey);

			if (res != ERROR_SUCCESS)
			{
				WarningThrow(false, u8"Querying registry key for joystick " + toString(index) + u8" failed.");
				return String(u8"Unknown joystick");
			}

			tempData[255] = L'\0';

			return String(tempData);
		}
	}
}

GLOWE::Windows::WindowsInputImplementation::WindowsInputImplementation()
	: lazyUpdates(false), connectionRefreshDelay(GLOWE::Time::fromMilliseconds(500))
{
	updateJoysticksConnections();
}

void GLOWE::Windows::WindowsInputImplementation::updateJoysticks()
{
	for (unsigned int x = 0; x < Joystick::supportedJoysticks; ++x)
	{
		WindowsInputImplementation::JoystickInfo& info = joystickInfos[x];

		if (info.status.isConnected)
		{
			info.status = info.update();

			if (!info.status.isConnected)
			{
				info.close();
				info.caps = Joystick::Capabilities();
				info.desc = Joystick::Description();
				info.status = Joystick::Status();
			}
		}
		else
		{
			if (WindowsInputImplementation::isJoystickConnected(x))
			{
				if (info.open(x))
				{
					info.updateCapabilities();
					info.status = info.update();
				}
			}
		}
	}
}

void GLOWE::Windows::WindowsInputImplementation::updateJoysticksConnections()
{
	JOYINFOEX joyinfo;
	joyinfo.dwSize = sizeof(JOYINFOEX);
	joyinfo.dwFlags = 0;

	for (unsigned int x = 0; x < GLOWE::Joystick::supportedJoysticks; ++x)
	{
		connectionCaches[x].isConnected = joyGetPosEx(JOYSTICKID1 + x, &joyinfo) == JOYERR_NOERROR;
		connectionCaches[x].timer.reset();
	}
}

GLOWE::Joystick::Description GLOWE::Windows::WindowsInputImplementation::getJoystickDescription(Joystick::JoystickId joystickId) const
{
	if (joystickId < 8)
	{
		return joystickInfos[joystickId].desc;
	}

	WarningThrow(false, u8"Only 8 joysticks supported. Array index out of bounds.");

	return Joystick::Description();
}

bool GLOWE::Windows::WindowsInputImplementation::isKeyDown(Keyboard::Key keycode) const
{
	return (GetAsyncKeyState(keyCodeToVirtualKey(keycode)) & 0x8000) != 0;
}

bool GLOWE::Windows::WindowsInputImplementation::isMouseButtonDown(Mouse::Button buttoncode) const
{
	int temp = 0;

	switch (buttoncode)
	{
	case Mouse::Button::Left:
		temp = GetSystemMetrics(SM_SWAPBUTTON) ? VK_RBUTTON : VK_LBUTTON;
		break;
	case Mouse::Button::Right:
		temp = GetSystemMetrics(SM_SWAPBUTTON) ? VK_LBUTTON : VK_RBUTTON;
		break;
	case Mouse::Button::Middle:
		temp = VK_MBUTTON;  
		break;
	case Mouse::Button::XButton1: 
		temp = VK_XBUTTON1;
		break;
	case Mouse::Button::XButton2: 
		temp = VK_XBUTTON2; 
		break;
	default:
		temp = 0;
		break;
	}

	return (GetAsyncKeyState(temp) & 0x8000) != 0;
}

bool GLOWE::Windows::WindowsInputImplementation::isJoystickButtonDown(Joystick::JoystickId joystickId, Joystick::JoystickId buttonId) const
{
	return joystickInfos[joystickId].status.buttonsStatus[buttonId];
}

bool GLOWE::Windows::WindowsInputImplementation::joystickHasAxis(Joystick::JoystickId joystickId, Joystick::Axis axis) const
{
	return joystickInfos[joystickId].caps.supportedAxes[static_cast<unsigned int>(axis)];
}

bool GLOWE::Windows::WindowsInputImplementation::isJoystickConnected(Joystick::JoystickId joystickId)
{
	JoystickConnectionCache& connCache = connectionCaches[joystickId];
	if (!lazyUpdates&&connCache.timer.getElapsedTime()>connectionRefreshDelay)
	{
		JOYINFOEX joyInfo;

		joyInfo.dwSize = sizeof(JOYINFOEX);
		joyInfo.dwFlags = 0;

		connCache.isConnected = joyGetPosEx(JOYSTICKID1 + joystickId, &joyInfo) == JOYERR_NOERROR;
		connCache.timer.reset();
	}

	return connCache.isConnected;
}

unsigned int GLOWE::Windows::WindowsInputImplementation::getJoystickButtonsCount(Joystick::JoystickId joystickId) const
{
	return joystickInfos[joystickId].caps.buttonCount;
}

GLOWE::Int2 GLOWE::Windows::WindowsInputImplementation::getMousePosition(bool global) const
{
	if (!global && checkIsEngineInitialized())
	{
		return getMousePosition(*windowEngine);
	}
	POINT temp;
	GetCursorPos(&temp);
	return Int2{ temp.x, temp.y };
}

GLOWE::Int2 GLOWE::Windows::WindowsInputImplementation::getMousePosition(Window & relativeTo) const
{
	WindowHandle wnd = relativeTo.getWindowHandle();

	if (wnd)
	{
		POINT temp;
		GetCursorPos(&temp);
		ScreenToClient(wnd, &temp);
		return Int2{ temp.x, temp.y };
	}

	return Int2();
}

float GLOWE::Windows::WindowsInputImplementation::getJoystickAxisPosition(Joystick::JoystickId joystickId, Joystick::Axis axis) const
{
	return joystickInfos[joystickId].status.axesPos[static_cast<unsigned int>(axis)];
}

void GLOWE::Windows::WindowsInputImplementation::setMousePosition(const Int2& newPos, bool global)
{
	if (!global && checkIsEngineInitialized())
	{
		setMousePosition(newPos, *windowEngine);
		return;
	}
	SetCursorPos(newPos[0], newPos[1]);
}

void GLOWE::Windows::WindowsInputImplementation::setMousePosition(const Int2& newPos, Window & relativeTo)
{
	WindowHandle wnd = relativeTo.getWindowHandle();
	if (wnd)
	{
		POINT temp = { newPos[0], newPos[1] };
		ScreenToClient(wnd, &temp);
		SetCursorPos(temp.x, temp.y);
	}
}

unsigned int GLOWE::Windows::WindowsInputImplementation::getJoysticksCount() const
{
	unsigned int result = 0;
	for (const auto & joystickInfo : joystickInfos)
	{
		if (joystickInfo.status.isConnected)
		{
			result++;
		}
	}

	return result;
}

GLOWE::Windows::WindowsInputImplementation::JoystickConnectionCache::JoystickConnectionCache()
	: isConnected(false)
{
}

GLOWE::Windows::WindowsInputImplementation::JoystickInfo::JoystickInfo()
	: index(0)
{
}

GLOWE::Windows::WindowsInputImplementation::JoystickInfo::~JoystickInfo()
{
	close();
}

bool GLOWE::Windows::WindowsInputImplementation::JoystickInfo::open(Joystick::JoystickId joyIndex)
{
	index = JOYSTICKID1 + joyIndex;

	bool result = joyGetDevCaps(index, &joycaps, sizeof(joycaps)) == JOYERR_NOERROR;

	if (result)
	{
		desc.name = JoystickHelpers::getDeviceName(index, joycaps);
		desc.vendorId = joycaps.wMid;
		desc.productId = joycaps.wPid;
	}

	return result;
}

void GLOWE::Windows::WindowsInputImplementation::JoystickInfo::close()
{
}

GLOWE::Joystick::Status GLOWE::Windows::WindowsInputImplementation::JoystickInfo::update()
{
	Joystick::Status result;

	JOYINFOEX joyInfo;

	joyInfo.dwFlags = JOY_RETURNX | JOY_RETURNY | JOY_RETURNZ | JOY_RETURNR | JOY_RETURNU | JOY_RETURNV | JOY_RETURNBUTTONS;
	joyInfo.dwSize = sizeof(JOYINFOEX);
	joyInfo.dwFlags |= (joycaps.wCaps & JOYCAPS_POVCTS) ? JOY_RETURNPOVCTS : JOY_RETURNPOV;

	if (joyGetPosEx(index, &joyInfo) == JOYERR_NOERROR)
	{
		result.isConnected = true;

		result.axesPos[static_cast<unsigned int>(Joystick::Axis::X)] = (joyInfo.dwXpos - (joycaps.wXmax + joycaps.wXmin) / 2.0f) * 200.0f / (joycaps.wXmax - joycaps.wXmin);
		result.axesPos[static_cast<unsigned int>(Joystick::Axis::Y)] = (joyInfo.dwYpos - (joycaps.wYmax + joycaps.wYmin) / 2.0f) * 200.0f / (joycaps.wYmax - joycaps.wYmin);
		result.axesPos[static_cast<unsigned int>(Joystick::Axis::Z)] = (joyInfo.dwZpos - (joycaps.wZmax + joycaps.wZmin) / 2.0f) * 200.0f / (joycaps.wZmax - joycaps.wZmin);
		result.axesPos[static_cast<unsigned int>(Joystick::Axis::R)] = (joyInfo.dwRpos - (joycaps.wRmax + joycaps.wRmin) / 2.0f) * 200.0f / (joycaps.wRmax - joycaps.wRmin);
		result.axesPos[static_cast<unsigned int>(Joystick::Axis::U)] = (joyInfo.dwUpos - (joycaps.wUmax + joycaps.wUmin) / 2.0f) * 200.0f / (joycaps.wUmax - joycaps.wUmin);
		result.axesPos[static_cast<unsigned int>(Joystick::Axis::V)] = (joyInfo.dwVpos - (joycaps.wVmax + joycaps.wVmin) / 2.0f) * 200.0f / (joycaps.wVmax - joycaps.wVmin);

		if (joyInfo.dwPOV != 0xFFFF)
		{
			float angle = joyInfo.dwPOV / 18000.f * MathHelper::Pi;
			result.axesPos[static_cast<unsigned int>(Joystick::Axis::PovX)] = std::sin(angle) * 100.0f;
			result.axesPos[static_cast<unsigned int>(Joystick::Axis::PovY)] = std::cos(angle) * 100.0f;

		}
		else
		{
			result.axesPos[static_cast<unsigned int>(Joystick::Axis::PovX)] = 0.0f;
			result.axesPos[static_cast<unsigned int>(Joystick::Axis::PovY)] = 0.0f;
		}

		for (unsigned int x = 0; x < Joystick::supportedButtons; ++x)
		{
			result.buttonsStatus[x] = (joyInfo.dwButtons & (1 << x)) != 0;
		}
	}

	return result;
}

void GLOWE::Windows::WindowsInputImplementation::JoystickInfo::updateCapabilities()
{
	caps.buttonCount = joycaps.wMaxButtons;
	if (caps.buttonCount > Joystick::supportedButtons)
	{
		caps.buttonCount = Joystick::supportedButtons;
	}

	caps.supportedAxes[static_cast<unsigned int>(Joystick::Axis::X)] = true;
	caps.supportedAxes[static_cast<unsigned int>(Joystick::Axis::Y)] = true;
	caps.supportedAxes[static_cast<unsigned int>(Joystick::Axis::Z)] = (joycaps.wCaps&JOYCAPS_HASZ) != 0;
	caps.supportedAxes[static_cast<unsigned int>(Joystick::Axis::R)] = (joycaps.wCaps&JOYCAPS_HASR) != 0;
	caps.supportedAxes[static_cast<unsigned int>(Joystick::Axis::U)] = (joycaps.wCaps&JOYCAPS_HASU) != 0;
	caps.supportedAxes[static_cast<unsigned int>(Joystick::Axis::V)] = (joycaps.wCaps&JOYCAPS_HASV) != 0;
	caps.supportedAxes[static_cast<unsigned int>(Joystick::Axis::PovX)] = (joycaps.wCaps&JOYCAPS_HASPOV) != 0;
	caps.supportedAxes[static_cast<unsigned int>(Joystick::Axis::PovY)] = caps.supportedAxes[static_cast<unsigned int>(Joystick::Axis::PovX)];
}

GLOWE::Keyboard::Key GLOWE::Windows::virtualKeyToKeyCode(LPARAM lParam, WPARAM wParam)
{
	switch (wParam)
	{
	case VK_SHIFT:
	{
		static UINT isLShift = MapVirtualKeyW(VK_LSHIFT, MAPVK_VK_TO_VSC);

		UINT scancode = static_cast<UINT>((lParam & (0xFF << 16)) >> 16);

		return scancode == isLShift ? Keyboard::Key::LShift : Keyboard::Key::RShift;
	}

	case VK_MENU: return (HIWORD(lParam) & KF_EXTENDED) ? Keyboard::Key::RAlt : Keyboard::Key::LAlt;

	case VK_CONTROL: return (HIWORD(lParam) & KF_EXTENDED) ? Keyboard::Key::RControl : Keyboard::Key::LControl;

	case VK_LWIN:       return Keyboard::Key::LSystem;
	case VK_RWIN:       return Keyboard::Key::RSystem;
	case VK_APPS:       return Keyboard::Key::Menu;
	case VK_OEM_1:      return Keyboard::Key::SemiColon;
	case VK_OEM_2:      return Keyboard::Key::Slash;
	case VK_OEM_PLUS:   return Keyboard::Key::Equal;
	case VK_OEM_MINUS:  return Keyboard::Key::Dash;
	case VK_OEM_4:      return Keyboard::Key::LBracket;
	case VK_OEM_6:      return Keyboard::Key::RBracket;
	case VK_OEM_COMMA:  return Keyboard::Key::Comma;
	case VK_OEM_PERIOD: return Keyboard::Key::Period;
	case VK_OEM_7:      return Keyboard::Key::Quote;
	case VK_OEM_5:      return Keyboard::Key::BackSlash;
	case VK_OEM_3:      return Keyboard::Key::Tilde;
	case VK_ESCAPE:     return Keyboard::Key::Escape;
	case VK_SPACE:      return Keyboard::Key::Space;
	case VK_RETURN:     return Keyboard::Key::Return;
	case VK_BACK:       return Keyboard::Key::BackSpace;
	case VK_TAB:        return Keyboard::Key::Tab;
	case VK_PRIOR:      return Keyboard::Key::PageUp;
	case VK_NEXT:       return Keyboard::Key::PageDown;
	case VK_END:        return Keyboard::Key::End;
	case VK_HOME:       return Keyboard::Key::Home;
	case VK_INSERT:     return Keyboard::Key::Insert;
	case VK_DELETE:     return Keyboard::Key::Delete;
	case VK_ADD:        return Keyboard::Key::Add;
	case VK_SUBTRACT:   return Keyboard::Key::Subtract;
	case VK_MULTIPLY:   return Keyboard::Key::Multiply;
	case VK_DIVIDE:     return Keyboard::Key::Divide;
	case VK_PAUSE:      return Keyboard::Key::Pause;
	case VK_F1:         return Keyboard::Key::F1;
	case VK_F2:         return Keyboard::Key::F2;
	case VK_F3:         return Keyboard::Key::F3;
	case VK_F4:         return Keyboard::Key::F4;
	case VK_F5:         return Keyboard::Key::F5;
	case VK_F6:         return Keyboard::Key::F6;
	case VK_F7:         return Keyboard::Key::F7;
	case VK_F8:         return Keyboard::Key::F8;
	case VK_F9:         return Keyboard::Key::F9;
	case VK_F10:        return Keyboard::Key::F10;
	case VK_F11:        return Keyboard::Key::F11;
	case VK_F12:        return Keyboard::Key::F12;
	case VK_F13:        return Keyboard::Key::F13;
	case VK_F14:        return Keyboard::Key::F14;
	case VK_F15:        return Keyboard::Key::F15;
	case VK_LEFT:       return Keyboard::Key::Left;
	case VK_RIGHT:      return Keyboard::Key::Right;
	case VK_UP:         return Keyboard::Key::Up;
	case VK_DOWN:       return Keyboard::Key::Down;
	case VK_NUMPAD0:    return Keyboard::Key::Numpad0;
	case VK_NUMPAD1:    return Keyboard::Key::Numpad1;
	case VK_NUMPAD2:    return Keyboard::Key::Numpad2;
	case VK_NUMPAD3:    return Keyboard::Key::Numpad3;
	case VK_NUMPAD4:    return Keyboard::Key::Numpad4;
	case VK_NUMPAD5:    return Keyboard::Key::Numpad5;
	case VK_NUMPAD6:    return Keyboard::Key::Numpad6;
	case VK_NUMPAD7:    return Keyboard::Key::Numpad7;
	case VK_NUMPAD8:    return Keyboard::Key::Numpad8;
	case VK_NUMPAD9:    return Keyboard::Key::Numpad9;
	case 'A':           return Keyboard::Key::A;
	case 'Z':           return Keyboard::Key::Z;
	case 'E':           return Keyboard::Key::E;
	case 'R':           return Keyboard::Key::R;
	case 'T':           return Keyboard::Key::T;
	case 'Y':           return Keyboard::Key::Y;
	case 'U':           return Keyboard::Key::U;
	case 'I':           return Keyboard::Key::I;
	case 'O':           return Keyboard::Key::O;
	case 'P':           return Keyboard::Key::P;
	case 'Q':           return Keyboard::Key::Q;
	case 'S':           return Keyboard::Key::S;
	case 'D':           return Keyboard::Key::D;
	case 'F':           return Keyboard::Key::F;
	case 'G':           return Keyboard::Key::G;
	case 'H':           return Keyboard::Key::H;
	case 'J':           return Keyboard::Key::J;
	case 'K':           return Keyboard::Key::K;
	case 'L':           return Keyboard::Key::L;
	case 'M':           return Keyboard::Key::M;
	case 'W':           return Keyboard::Key::W;
	case 'X':           return Keyboard::Key::X;
	case 'C':           return Keyboard::Key::C;
	case 'V':           return Keyboard::Key::V;
	case 'B':           return Keyboard::Key::B;
	case 'N':           return Keyboard::Key::N;
	case '0':           return Keyboard::Key::Num0;
	case '1':           return Keyboard::Key::Num1;
	case '2':           return Keyboard::Key::Num2;
	case '3':           return Keyboard::Key::Num3;
	case '4':           return Keyboard::Key::Num4;
	case '5':           return Keyboard::Key::Num5;
	case '6':           return Keyboard::Key::Num6;
	case '7':           return Keyboard::Key::Num7;
	case '8':           return Keyboard::Key::Num8;
	case '9':           return Keyboard::Key::Num9;
	}

	return Keyboard::Key::Unknown;
}

int GLOWE::Windows::keyCodeToVirtualKey(Keyboard::Key arg)
{
	switch (arg)
	{
	case Keyboard::Key::A:          return 'A';           break;
	case Keyboard::Key::B:          return 'B';           break;
	case Keyboard::Key::C:          return 'C';           break;
	case Keyboard::Key::D:          return 'D';           break;
	case Keyboard::Key::E:          return 'E';           break;
	case Keyboard::Key::F:          return 'F';           break;
	case Keyboard::Key::G:          return 'G';           break;
	case Keyboard::Key::H:          return 'H';           break;
	case Keyboard::Key::I:          return 'I';           break;
	case Keyboard::Key::J:          return 'J';           break;
	case Keyboard::Key::K:          return 'K';           break;
	case Keyboard::Key::L:          return 'L';           break;
	case Keyboard::Key::M:          return 'M';           break;
	case Keyboard::Key::N:          return 'N';           break;
	case Keyboard::Key::O:          return 'O';           break;
	case Keyboard::Key::P:          return 'P';           break;
	case Keyboard::Key::Q:          return 'Q';           break;
	case Keyboard::Key::R:          return 'R';           break;
	case Keyboard::Key::S:          return 'S';           break;
	case Keyboard::Key::T:          return 'T';           break;
	case Keyboard::Key::U:          return 'U';           break;
	case Keyboard::Key::V:          return 'V';           break;
	case Keyboard::Key::W:          return 'W';           break;
	case Keyboard::Key::X:          return 'X';           break;
	case Keyboard::Key::Y:          return 'Y';           break;
	case Keyboard::Key::Z:          return 'Z';           break;
	case Keyboard::Key::Num0:       return '0';           break;
	case Keyboard::Key::Num1:       return '1';           break;
	case Keyboard::Key::Num2:       return '2';           break;
	case Keyboard::Key::Num3:       return '3';           break;
	case Keyboard::Key::Num4:       return '4';           break;
	case Keyboard::Key::Num5:       return '5';           break;
	case Keyboard::Key::Num6:       return '6';           break;
	case Keyboard::Key::Num7:       return '7';           break;
	case Keyboard::Key::Num8:       return '8';           break;
	case Keyboard::Key::Num9:       return '9';           break;
	case Keyboard::Key::Escape:     return VK_ESCAPE;     break;
	case Keyboard::Key::LControl:   return VK_LCONTROL;   break;
	case Keyboard::Key::LShift:     return VK_LSHIFT;     break;
	case Keyboard::Key::LAlt:       return VK_LMENU;      break;
	case Keyboard::Key::LSystem:    return VK_LWIN;       break;
	case Keyboard::Key::RControl:   return VK_RCONTROL;   break;
	case Keyboard::Key::RShift:     return VK_RSHIFT;     break;
	case Keyboard::Key::RAlt:       return VK_RMENU;      break;
	case Keyboard::Key::RSystem:    return VK_RWIN;       break;
	case Keyboard::Key::Menu:       return VK_APPS;       break;
	case Keyboard::Key::LBracket:   return VK_OEM_4;      break;
	case Keyboard::Key::RBracket:   return VK_OEM_6;      break;
	case Keyboard::Key::SemiColon:  return VK_OEM_1;      break;
	case Keyboard::Key::Comma:      return VK_OEM_COMMA;  break;
	case Keyboard::Key::Period:     return VK_OEM_PERIOD; break;
	case Keyboard::Key::Quote:      return VK_OEM_7;      break;
	case Keyboard::Key::Slash:      return VK_OEM_2;      break;
	case Keyboard::Key::BackSlash:  return VK_OEM_5;      break;
	case Keyboard::Key::Tilde:      return VK_OEM_3;      break;
	case Keyboard::Key::Equal:      return VK_OEM_PLUS;   break;
	case Keyboard::Key::Dash:       return VK_OEM_MINUS;  break;
	case Keyboard::Key::Space:      return VK_SPACE;      break;
	case Keyboard::Key::Return:     return VK_RETURN;     break;
	case Keyboard::Key::BackSpace:  return VK_BACK;       break;
	case Keyboard::Key::Tab:        return VK_TAB;        break;
	case Keyboard::Key::PageUp:     return VK_PRIOR;      break;
	case Keyboard::Key::PageDown:   return VK_NEXT;       break;
	case Keyboard::Key::End:        return VK_END;        break;
	case Keyboard::Key::Home:       return VK_HOME;       break;
	case Keyboard::Key::Insert:     return VK_INSERT;     break;
	case Keyboard::Key::Delete:     return VK_DELETE;     break;
	case Keyboard::Key::Add:        return VK_ADD;        break;
	case Keyboard::Key::Subtract:   return VK_SUBTRACT;   break;
	case Keyboard::Key::Multiply:   return VK_MULTIPLY;   break;
	case Keyboard::Key::Divide:     return VK_DIVIDE;     break;
	case Keyboard::Key::Left:       return VK_LEFT;       break;
	case Keyboard::Key::Right:      return VK_RIGHT;      break;
	case Keyboard::Key::Up:         return VK_UP;         break;
	case Keyboard::Key::Down:       return VK_DOWN;       break;
	case Keyboard::Key::Numpad0:    return VK_NUMPAD0;    break;
	case Keyboard::Key::Numpad1:    return VK_NUMPAD1;    break;
	case Keyboard::Key::Numpad2:    return VK_NUMPAD2;    break;
	case Keyboard::Key::Numpad3:    return VK_NUMPAD3;    break;
	case Keyboard::Key::Numpad4:    return VK_NUMPAD4;    break;
	case Keyboard::Key::Numpad5:    return VK_NUMPAD5;    break;
	case Keyboard::Key::Numpad6:    return VK_NUMPAD6;    break;
	case Keyboard::Key::Numpad7:    return VK_NUMPAD7;    break;
	case Keyboard::Key::Numpad8:    return VK_NUMPAD8;    break;
	case Keyboard::Key::Numpad9:    return VK_NUMPAD9;    break;
	case Keyboard::Key::F1:         return VK_F1;         break;
	case Keyboard::Key::F2:         return VK_F2;         break;
	case Keyboard::Key::F3:         return VK_F3;         break;
	case Keyboard::Key::F4:         return VK_F4;         break;
	case Keyboard::Key::F5:         return VK_F5;         break;
	case Keyboard::Key::F6:         return VK_F6;         break;
	case Keyboard::Key::F7:         return VK_F7;         break;
	case Keyboard::Key::F8:         return VK_F8;         break;
	case Keyboard::Key::F9:         return VK_F9;         break;
	case Keyboard::Key::F10:        return VK_F10;        break;
	case Keyboard::Key::F11:        return VK_F11;        break;
	case Keyboard::Key::F12:        return VK_F12;        break;
	case Keyboard::Key::F13:        return VK_F13;        break;
	case Keyboard::Key::F14:        return VK_F14;        break;
	case Keyboard::Key::F15:        return VK_F15;        break;
	case Keyboard::Key::Pause:      return VK_PAUSE;      break;
	default:				   return 0;
	}
}
