#pragma once
#ifndef GLOWE_WINDOWS_INPUTIMPL_INCLUDED
#define GLOWE_WINDOWS_INPUTIMPL_INCLUDED

#include "../../GlowSystem/Utility.h"
#ifdef GLOWE_COMPILE_FOR_WINDOWS
#include "../Uniform/InputImplementation.h"
#include "../../GlowSystem/Time.h"
#include "../../GlowSystem/String.h"

namespace GLOWE
{
	namespace Windows
	{
		Keyboard::Key virtualKeyToKeyCode(LPARAM lParam, WPARAM wParam);
		int keyCodeToVirtualKey(Keyboard::Key arg);

		class GLOWWINDOW_EXPORT WindowsInputImplementation final
			: public InputImplementation, public Subsystem
		{
		private:
			class GLOWWINDOW_EXPORT JoystickConnectionCache
			{
			public:
				bool isConnected;
				GLOWE::Clock timer;
			public:
				JoystickConnectionCache();
			};

			class GLOWWINDOW_EXPORT JoystickInfo
			{
			public:
				Joystick::Capabilities caps;
				Joystick::Description desc;
				Joystick::JoystickId index;
				Joystick::Status status;
				JOYCAPS joycaps;

				JoystickInfo();
				~JoystickInfo();

				bool open(Joystick::JoystickId joyIndex);
				void close();

				Joystick::Status update();
				void updateCapabilities();
			};

			bool lazyUpdates;

			const GLOWE::Time connectionRefreshDelay;

			JoystickConnectionCache connectionCaches[GLOWE::Joystick::supportedJoysticks];

			JoystickInfo joystickInfos[GLOWE::Joystick::supportedJoysticks];
		public:
			WindowsInputImplementation();
			virtual ~WindowsInputImplementation() = default;

			virtual void updateJoysticks() override;
			void updateJoysticksConnections();

			virtual Joystick::Description getJoystickDescription(Joystick::JoystickId joystickId) const override;

			virtual bool isKeyDown(Keyboard::Key keycode) const override;
			virtual bool isMouseButtonDown(Mouse::Button buttoncode) const override;

			virtual bool isJoystickButtonDown(Joystick::JoystickId joystickId, Joystick::JoystickId buttonId) const override;
			virtual bool joystickHasAxis(Joystick::JoystickId joystickId, Joystick::Axis axis) const override;
			virtual bool isJoystickConnected(Joystick::JoystickId joystickId) override;

			virtual unsigned int getJoystickButtonsCount(Joystick::JoystickId joystickId) const override;

			virtual Int2 getMousePosition(bool global = false) const override;
			virtual Int2 getMousePosition(Window& relativeTo) const override;

			virtual float getJoystickAxisPosition(Joystick::JoystickId joystickId, Joystick::Axis axis) const override;

			virtual void setMousePosition(const Int2& newPos, bool global = false) override;
			virtual void setMousePosition(const Int2& newPos, Window& relativeTo) override;

			virtual unsigned int getJoysticksCount() const override;
		};
	}
}

#endif

#endif
