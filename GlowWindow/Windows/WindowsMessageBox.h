#pragma once
#ifndef WINDOWS_MESSAGEBOX_INCLUDED
#define WINDOWS_MESSAGEBOX_INCLUDED

#include "../../GlowSystem/Utility.h"
#include "../../GlowSystem/String.h"
#include "../../GlowSystem/UtfStringConverter.h"

#ifdef GLOWE_COMPILE_FOR_WINDOWS

#include "../Uniform/MessageBox.h"

namespace GLOWE
{
	namespace Windows
	{
		void showMessageBox(const String& text, short style);
	}
}

#endif

#endif
