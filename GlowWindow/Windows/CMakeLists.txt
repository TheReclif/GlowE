target_sources(GlowWindow
	PRIVATE
		WindowsInputImplementation.cpp
		WindowsMessageBox.cpp
		WindowsVideoMode.cpp
		WindowsWindowImplementation.cpp
#	PUBLIC
#		WindowsInputImplementation.h
#		WindowsMessageBox.h
#		WindowsVideoMode.h
#		WindowsWindowImplementation.h
)
install(DIRECTORY "." DESTINATION include/GlowWindow/Windows FILES_MATCHING PATTERN "*.h")