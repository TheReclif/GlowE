#include "WindowsMessageBox.h"

#ifdef GLOWE_COMPILE_FOR_WINDOWS
void GLOWE::Windows::showMessageBox(const String& text, short style)
{
	UINT _style = MB_OK;
	String title = u8"";

	switch (style)
	{
	case 0:
		_style |= MB_ICONERROR;
		title = u8"Error";
		break;
	case 1:
		_style |= MB_ICONASTERISK;
		title = u8"Warning";
		break;
	default:
		_style |= MB_ICONINFORMATION;
		title = u8"Notification";
		break;
	}

	MessageBoxW(GetDesktopWindow(), Utf<8>::toWide(text.getString()).c_str(), Utf<8>::toWide(title.getString()).c_str(), _style);
}
#endif