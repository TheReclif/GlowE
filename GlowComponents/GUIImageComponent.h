#pragma once
#ifndef GLOWE_GUIIMAGECOMPONENT_INCLUDED
#define GLOWE_GUIIMAGECOMPONENT_INCLUDED

#include "GUIBaseComponent.h"

#include "../GlowEngine/GUI/Sprite.h"

#include "../GlowEngine/Material.h"

#include "../GlowGraphics/Uniform/Geometry.h"

namespace GLOWE
{
	namespace Components
	{
		/// @brief GUI image component. Can show any renderable texture, including the ones bind as render targets.
		class GLOWENGINE_EXPORT GUIImage
			: public GUIBase
		{
		private:
			Material usedMaterial;
			ResourceHandle<Geometry> guiPlane;
			Float4x4 modelMat;
			int texPos, materialCBufferPos, colorPos;
			Sprite sprite;
		protected:
			virtual void renderImpl(BasicRendererImpl& renderer, MaterialEnvironment& matEnv) override;
			virtual void updateImpl() override;
		public:
			/// @brief The minimal constructor.
			/// @param newParent Component's parent.
			GUIImage(GameObject* const newParent);
			/// @brief The minimal constructor. Also, automatically finds a suitable GUI container.
			/// @param newParent Component's parent.
			/// @param  Default constructed object of AutoFindParentOnStart.
			GUIImage(GameObject* const newParent, const AutoFindParentOnStart&);
			virtual ~GUIImage() = default;

			GlowSerializeComponent(GUIImage, GlowBases());

			virtual void processEvent(const GameEvent&) override {};

			virtual void onActivate() override;
			virtual void onDeactivate() override;
			
			/// @brief Sets the used image. May not work with custom shaders (when texture "Diffuse Texture" is unavailable).
			/// @param texture New texture.
			/// @param newTextureRect Pass the new texture region if you want to change it.
			void setImage(const ResourceHandle<TextureImpl>& texture, const GUIRect& newTextureRect = GUIRect());
			/// @brief Sets the image's color. May not work with custom shaders (when "Color" field in "Material" constant buffer is unavailable).
			/// @param col New color.
			void setColor(const Color& col);

			/// @brief Returns a reference to the used material.
			/// @return Current material.
			Material& getMaterial();

			Sprite& getSprite();
		};

		GlowInitComponentFactoryReflection(GUIImage);
	}
}

#endif
