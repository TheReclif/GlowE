#pragma once
#ifndef GLOWE_COMPONENTS_TRANSFORM_INCLUDED
#define GLOWE_COMPONENTS_TRANSFORM_INCLUDED

#include "../GlowEngine/Component.h"

#include "../GlowMath/Transform.h"

namespace GLOWE
{
	namespace Components
	{
		/// @brief Transform(ation) component. It's required by all of the game objects. It stores and manages the objects' position (also called translation), rotation and scale. It also stores cached already computed transformation matrices.
		class GLOWENGINE_EXPORT Transform
			: public Component
		{
		private:
			using Mat3x3 = const Matrix3x3 &;
			using Mat4x4 = const Matrix4x4 &;
			using Vec3 = const Vector3F &;
			using Vec4 = const Vector4F &;
			using Quat = const Quaternion &;
			using RotMat = const RotationMatrix &;
		private:
			// Stores the local transformations.
			::GLOWE::Transform transform;
			Float4x4 composedMatrix;
			bool isDirty;

			Float3 worldTranslation, worldScale;
			Float4 worldRotation;
		public:
			/// @brief Default-initializes the component with given game object as its parent.
			/// @param newParent Pointer to component's parent.
			Transform(GameObject* const newParent);

			//GlowSerializeCustomAccessors(GlowField(worldTranslation), GlowField(worldScale), GlowField(worldRotation));
			GlowSerializeComponentCustomAccessors(Transform, GlowBases());

			virtual Map<String, UniquePtr<BaseAccessor>> getAccessors() override;

			/// @brief Sets the scale.
			/// @param newScale Scale.
			void setScale(Vec3 newScale);
			/// @brief Sets the scale.
			/// @param newScale Scale.
			void setScale(const Float3& newScale);

			/// @brief Sets the translation.
			/// @param newTranslation Translation.
			void setTranslation(Vec3 newTranslation);
			/// @brief Sets the translation.
			/// @param newTranslation Translation.
			void setTranslation(const Float3& newTranslation);

			/// @brief Sets the rotation from a quaternion.
			/// @param newRotation Rotation as quaternion.
			void setRotation(Quat newRotation);
			/// @brief Sets the rotation from a rotation matrix.
			/// @param newRotation Rotation as rotation matrix.
			void setRotation(RotMat newRotation);
			/// @brief Sets the rotation from Euler angles. Each component represents rotation in radians in a corresponding axis.
			/// @param angles Rotation as Euler angles.
			void setRotation(const Float3& angles);
			/// @brief Sets the rotation from a quaternion. Keep in mind that the w component is first here.
			/// @param newRotation Rotation as quaternion, passed as its components in Float4 in the following order: { w, x, y, z }.
			void setRotation(const Float4& newRotation);

			/// @brief Scales the transform by a given factor. Example: transform with scale of { 1, 2, 3 } scaled by { 3, 2, 1 } will become { 3, 4, 3 }.
			/// @param arg Scaling factor.
			void scale(Vec3 arg);
			/// @brief Scales the transform by a given factor. Example: transform with scale of { 1, 2, 3 } scaled by { 3, 2, 1 } will become { 3, 4, 3 }.
			/// @param arg Scaling factor.
			void scale(const Float3& arg);

			/// @brief Translates the transform by a given amount. Example: transform with a translation of { 1, 1, 1 } translated by { 1, 2, 3 } will become { 2, 3, 4 }.
			/// @param arg Translation amount.
			void translate(Vec3 arg);
			/// @brief Translates the transform by a given amount. Example: transform with a translation of { 1, 1, 1 } translated by { 1, 2, 3 } will become { 2, 3, 4 }.
			/// @param arg Translation amount.
			void translate(const Float3& arg);

			/// @brief Rotates the transform using a given quaternion.
			/// @param arg Rotation amount as quaternion.
			void rotate(Quat arg);
			/// @brief Rotates the transform using a given rotation matrix. First it's converted to quaternion and then passed to rotate(Quat) method.
			/// @param arg Rotation amount as rotation matrix.
			void rotate(RotMat arg);
			/// @brief Rotates the transform using a given set of Euler angles in radians. First they're converted to quaternion and then passed to rotate(Quat) method.
			/// @param arg Rotation amount as Euler angles in radians.
			void rotate(const Float3& arg);
			/// @brief Rotates the transform using a given quaternion. Keep in mind that the w component is first here.
			/// @param arg Rotation amount as quaternion, passed as its components in Float4 in the following order: { w, x, y, z }.
			void rotate(const Float4& arg);

			/// @brief Returns the transform's local scale.
			/// @return Scale.
			Float3 getScale() const;
			/// @brief Returns the transform's local translation.
			/// @return Translation.
			Float3 getTranslation() const;
			/// @brief Returns the transform's local rotation as components of quaternion in the following order: { w, x, y, z }.
			/// @return Rotation as quaternion components.
			Float4 getRotation() const;

			/// @brief Returns the transform's world scale, that is with applied parent's scale.
			/// @return World scale.
			Float3 getWorldScale();
			/// @brief Returns the transform's world translation, that is with applied parent's translation.
			/// @return World translation.
			Float3 getWorldTranslation();
			/// @brief Returns the transform's world rotation (that is with applied parent's rotation) as components of quaternion in the following order: { w, x, y, z }.
			/// @return World rotation as quaternion components.
			Float4 getWorldRotation();

			/// @brief Returns normalized forward vector.
			/// @return Normalized forward vector.
			Float3 getForward();
			/// @brief Returns normalized right vector.
			/// @return Normalized right vector.
			Float3 getRight();
			/// @brief Returns normalized up vector.
			/// @return Normalized up vector.
			Float3 getUp();

			/// @brief Returns the composed transformation matrix.
			/// @return Transformation matrix.
			Float4x4 getMatrix();
			/// @brief Applies all of the accumulated changes to the transform and composes the transformation matrix.
			void updateMatrix();

			/// @brief Returns the underlaying mathematical transform object.
			/// @return Mathematical transform.
			const ::GLOWE::Transform& getTransform() const;

			/// @brief Checks if either the transform or its parent are dirty, i.e. needs matrix update.
			/// @return true if transform or the parent is dirty, false otherwise.
			bool checkIsDirty() const;

			virtual void processEvent(const GameEvent& event) override;

			virtual void onFirstActivate() override;

			// Actually it makes no sense to disable/enable a Transform.
			virtual void onActivate() override {};
			virtual void onDeactivate() override {};
		};

		GlowInitComponentFactoryReflection(Transform);
	}
}

#endif
