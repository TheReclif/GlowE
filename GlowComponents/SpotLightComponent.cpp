#include "SpotLightComponent.h"
#include "TransformComponent.h"
#include "../GlowEngine/GameObject.h"

void GLOWE::Components::SpotLight::updateLight()
{
	if (needToUpdate)
	{
		auto transform = gameObject->getTransform();
		lightInfo.position = transform->getWorldTranslation();
		lightInfo.direction = transform->getForward();
		spotLightHandle = getInstance<LightingMgr>().updateSpotLight(spotLightHandle, lightInfo);
		needToUpdate = false;
	}
}

GLOWE::Components::SpotLight::SpotLight(GameObject* const newParent)
	: Component(newParent), lightInfo{ Color::white, Color::white, Color::white, Float3(), 0.0f, Float3(), 0.0f, Float3{1.0f, 1.0f, 1.0f}, 1000.0f }, spotLightHandle(), lightHandle(), needToUpdate(true)
{
}

void GLOWE::Components::SpotLight::setAmbient(const Color& newAmbient)
{
	lightInfo.ambient = newAmbient;
	needToUpdate = true;
}

void GLOWE::Components::SpotLight::setDiffuse(const Color& newDiffuse)
{
	lightInfo.diffuse = newDiffuse;
	needToUpdate = true;
}

void GLOWE::Components::SpotLight::setSpecular(const Color& newSpecular)
{
	lightInfo.specular = newSpecular;
	needToUpdate = true;
}

void GLOWE::Components::SpotLight::setAttenuation(const Float3& newAtten)
{
	lightInfo.attenuation = newAtten;
	needToUpdate = true;
}

void GLOWE::Components::SpotLight::setAttenuation(const Vector3F& newAtten)
{
	lightInfo.attenuation = newAtten.toFloat3();
	needToUpdate = true;
}

void GLOWE::Components::SpotLight::setRange(const float newRange)
{
	lightInfo.range = newRange;
	needToUpdate = true;
}

void GLOWE::Components::SpotLight::setInnerCutOff(const float newSpot)
{
	lightInfo.innerCutOff = std::cos(newSpot);
	needToUpdate = true;
}

void GLOWE::Components::SpotLight::setOuterCutOff(const float newSpot)
{
	lightInfo.outerCutOff = std::cos(newSpot);
	lightInfo.outerCutOffAngle = newSpot;
	needToUpdate = true;
}

void GLOWE::Components::SpotLight::setImportance(const float newImportance)
{
	lightInfo.importance = newImportance;
	needToUpdate = true;
}

void GLOWE::Components::SpotLight::processEvent(const GameEvent& event)
{
	if (event == GameEvent::Type::TransformUpdated)
	{
		needToUpdate = true;
	}
}

void GLOWE::Components::SpotLight::onActivate()
{
	spotLightHandle = getInstance<LightingMgr>().addSpotLight(lightInfo);
	lightHandle = getInstance<LightingMgr>().registerLight(this);
}

void GLOWE::Components::SpotLight::onDeactivate()
{
	getInstance<LightingMgr>().removeSpotLight(spotLightHandle);
	getInstance<LightingMgr>().unregisterLight(lightHandle);
}
