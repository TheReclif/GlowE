#include "GUITextFieldComponent.h"

#include "../GlowGraphics/Uniform/GraphicsFactory.h"

#include "../GlowEngine/GUI/GUIContainer.h"
#include "../GlowEngine/Engine.h"
#include "../GlowEngine/GameObject.h"

void GLOWE::Components::GUITextField::resizeBuffer(const unsigned int newElementsCount, const void* data)
{
	const unsigned int newSize = newElementsCount * sizeof(InstanceData);

	GraphicsDeviceImpl& device = getInstance<Engine>().getGraphicsContext();
	const BufferDesc tempDesc = instanceBuffer->getDesc();
	instanceBuffer->destroy();
	instanceBuffer->create(tempDesc, newSize, device, data);
	renderedBuffers.replaceBuffer(1, instanceBuffer.get());
	bufferCapacity = newElementsCount;
}

GLOWE::Components::GUITextField::GUITextField(GameObject* const newParent)
	: GUIBase(newParent), usedMaterial(), guiPlane(getInstance<ResourceMgr>().load<Geometry>("GlowCore/BasicGeometry/GUIPlane.gmsh", "GUIPlane")), usedFont(getInstance<ResourceMgr>().load<Font>("GlowCore/BasicFonts/Arial.gfont", "Arial Regular")), instanceBuffer(createGraphicsObject<BufferImpl>()), bufferCapacity(5), fontSize(12), texturePos(), text(), renderedBuffers(), setColorFunc(), fontMetrics(), lineBreaks(1, 0)
{
	GraphicsDeviceImpl& device = getInstance<Engine>().getGraphicsContext();

	text.reserve(5);
	BufferDesc buffDesc;
	buffDesc.binds = BufferDesc::Binding::VertexBuffer;
	buffDesc.cpuAccess = CPUAccess::Write;
	buffDesc.usage = Usage::Dynamic;
	buffDesc.layout.addElement(Format(Format::Type::Float, 2)); // pos.
	buffDesc.layout.addElement(Format(Format::Type::Float, 2)); // size.
	buffDesc.layout.addElement(Format(Format::Type::Float, 2)); // atlasPos.
	instanceBuffer->create(buffDesc, sizeof(InstanceData) * text.getCapacity(), device);

	InputLayoutDesc inputLayoutDesc;
	inputLayoutDesc.addInstanceElement("Pos", Format(Format::Type::Float, 2));
	inputLayoutDesc.addInstanceElement("CharSize", Format(Format::Type::Float, 2));
	inputLayoutDesc.addInstanceElement("AtlasPos", Format(Format::Type::Float, 2));
	renderedBuffers.addBuffer(std::dynamic_pointer_cast<GeometryBuffers>(guiPlane->buffers));
	renderedBuffers.addBuffer(instanceBuffer.get(), inputLayoutDesc);
	renderedBuffers.setIndicesBuffer(std::dynamic_pointer_cast<GeometryBuffers>(guiPlane->buffers));
	
	auto effect = loadResource<Effect>("GlowCore/Shaders/StandardTextShader.geff", "Effect");
	Material::createFromEffect(usedMaterial, device, effect, "DefaultText");
	const int tempPos = usedMaterial.getTexturePosition("Diffuse Texture");
	if (tempPos < 0)
	{
		WarningThrow(false, "Unable to get Diffuse Texture position.");
	}
	else
	{
		texturePos = tempPos;
	}
	updateSetColorFunc();
	setColor(Color::black);

	effect = loadResource<Effect>("GlowCore/Shaders/StandardGUIShader.geff", "Effect");
	Material::createFromEffect(selectionMaterial, device, effect, "Default", "GUI");
	materialSelectionCBuffer = selectionMaterial.getCBufferPosition("Material");
	if (materialSelectionCBuffer >= 0)
	{
		selectionColorPos = selectionMaterial.getCBufferFieldPosition(materialSelectionCBuffer, "Color");
		setSelectionColor(Color::from255RGBA(51, 153, 255));
	}
	selectionMaterial.setTexture(0, loadResource<TextureImpl>("GlowCore/Textures/PlainColorTexture.gtex", ""));

	Guard _(*usedFont);
	fontMetrics = usedFont->getFontMetrics(fontSize);
}

GLOWE::Components::GUITextField::GUITextField(GameObject* const newParent, const AutoFindParentOnStart&)
	: GUITextField(newParent)
{
	autoFindContainer(gameObject);
}

void GLOWE::Components::GUITextField::setFontSize(const unsigned int newFontSize)
{
	fontSize = newFontSize;
	{
		Guard _(*usedFont);
		fontMetrics = usedFont->getFontMetrics(newFontSize);
	}
	isTextDirty = true;
	scheduleRecalc();
}

void GLOWE::Components::GUITextField::setText(const String& newText)
{
	text = newText;
	isTextDirty = true;
	scheduleRecalc();
}

void GLOWE::Components::GUITextField::setLanguage(const String& bcp47Language)
{
	language = bcp47Language;
	isTextDirty = true;
	scheduleRecalc();
}

void GLOWE::Components::GUITextField::setUseSimpleTab(const bool useSimpleTab)
{
	useSimpleTabs = useSimpleTab;
	isTextDirty = true;
	scheduleRecalc();
}

void GLOWE::Components::GUITextField::setTabulationStopDistance(const float newDistance)
{
	tabulationStopDistance = newDistance;
	isTextDirty = true;
	scheduleRecalc();
}

void GLOWE::Components::GUITextField::setHorizontalAlignment(const TextHorizontalAlignment alignment)
{
	horizontalAlignment = alignment;
	isTextDirty = true;
	scheduleRecalc();
}

void GLOWE::Components::GUITextField::setVerticalAlignment(const TextVerticalAlignment alignment)
{
	verticalAlignment = alignment;
	isTextDirty = true;
	scheduleRecalc();
}

bool GLOWE::Components::GUITextField::setColor(const Color& newColor)
{
	if (setColorFunc)
	{
		setColorFunc(newColor);
		markForRedraw();
		return true;
	}
	return false;
}

void GLOWE::Components::GUITextField::setSelectionColor(const Color& newColor)
{
	if (selectionColorPos >= 0)
	{
		selectionMaterial.modifyCBuffer<Color>(materialSelectionCBuffer, selectionColorPos, newColor);
		if (selectionEnd != selectionBegin)
		{
			markForRedraw();
		}
	}
}

void GLOWE::Components::GUITextField::updateSetColorFunc(const std::function<void(const Color&)>& newFunc)
{
	if (newFunc)
	{
		setColorFunc = newFunc;
	}
	else
	{
		const int cbufferPos = usedMaterial.getCBufferPosition("Material");
		if (cbufferPos >= 0)
		{
			const int colorPos = usedMaterial.getCBufferFieldPosition(cbufferPos, "Color");
			setColorFunc = [this, cbufferPos, colorPos](const Color& newColor)
			{
				usedMaterial.modifyCBuffer<Color>(cbufferPos, colorPos, newColor);
			};
		}
	}
}

void GLOWE::Components::GUITextField::setSelection(const unsigned int selectionBegin, const unsigned int selectionEnd)
{
	if (selectionEnd >= selectionBegin)
	{
		if (this->selectionBegin != selectionBegin || this->selectionEnd != selectionEnd)
		{
			this->selectionBegin = selectionBegin;
			this->selectionEnd = selectionEnd;
			markForRedraw();
		}
	}
	else
	{
		this->selectionBegin = this->selectionEnd = 0;
	}
}

void GLOWE::Components::GUITextField::setCanBeSelected(const bool canBeSelected)
{
	(void)canBeSelected;
	throw NotImplementedException();
}

const GLOWE::Font::Metrics& GLOWE::Components::GUITextField::getFontMetrics() const
{
	return fontMetrics;
}

unsigned int GLOWE::Components::GUITextField::getFontSize() const
{
	return fontSize;
}

GLOWE::String GLOWE::Components::GUITextField::getText() const
{
	return text;
}

GLOWE::Material& GLOWE::Components::GUITextField::getMaterial()
{
	return usedMaterial;
}

const GLOWE::Vector<GLOWE::Components::GUITextField::TextGlyph>& GLOWE::Components::GUITextField::getGlyphs() const
{
	return textGlyphs;
}

unsigned int GLOWE::Components::GUITextField::getLinePosition(const unsigned int whichLine) const
{
	return lineBreaks.at(whichLine);
}

unsigned int GLOWE::Components::GUITextField::getLineCount() const
{
	return lineBreaks.size();
}

int GLOWE::Components::GUITextField::localPosToGlyphPos(const Vector2F pos, const SelectionDirection caretPos) const
{
	int whichLine = static_cast<int>(pos.getY()) / fontSize;
	if (pos.getY() < 0)
	{
		whichLine = 0;
	}
	else
	if (pos.getY() >= (getLineCount() * fontSize))
	{
		whichLine = getLineCount() - 1;
	}

	const auto prevLineBreak = whichLine > 0 ? lineBreaks[whichLine - 1] : 0;
	const float parentX = getGlobalGUIRect().pos[0];
	//const auto nextLineBreak = ((whichLine + 1) < lineBreaks.size()) ? lineBreaks[whichLine + 1] : textGlyphs.size();
	for (unsigned int x = prevLineBreak; x < lineBreaks[whichLine]; ++x)
	{
		if (textGlyphs[x].glyph == nullptr || !isGraphical(textGlyphs[x].character))
		{
			continue;
		}
		const float glyphWidth = textGlyphs[x].glyph->size[0], glyphX = textGlyphs[x].pos[0] - parentX;
		if (pos.getX() < ((glyphWidth * 0.6f) + glyphX))
		{
			return x;
		}
	}

	if (lineBreaks[whichLine] != textGlyphs.size())
	{
		return lineBreaks[whichLine];
	}

	return lineBreaks[whichLine];
}

int GLOWE::Components::GUITextField::textPosToLinePos(const unsigned int textPos) const
{
	const auto it = std::upper_bound(lineBreaks.begin(), lineBreaks.end(), textPos > 0 ? textPos - 1 : 0);
	if (it != lineBreaks.end())
	{
		return std::distance(lineBreaks.begin(), it);
	}
	return -1;
}

void GLOWE::Components::GUITextField::setOnGlyphsChangedCallback(const std::function<void(const GUITextField&)>& callback)
{
	onGlyphsChangedCallback = callback;
}

void GLOWE::Components::GUITextField::drawLine(const unsigned int textBegin, const unsigned int textEnd, const unsigned int textLine, BasicRendererImpl& renderer, MaterialEnvironment& matEnv)
{
	if (textBegin >= textEnd)
	{
		return;
	}
	GLOWE::Transform tempTrans;
	const auto firstCharPos = textGlyphs.at(textBegin).pos;
	Float2 lastCharPos;
	if (textEnd < textGlyphs.size())
	{
		lastCharPos = textGlyphs.at(textEnd).pos;
	}
	else
	{
		// Only valid when textEnd == textGlyphs.size()
		const auto& tempGlyph = textGlyphs.at(textEnd - 1);
		lastCharPos = tempGlyph.pos;
		if (tempGlyph.glyph)
		{
			lastCharPos[0] += tempGlyph.glyph->size[0];
		}
	}
	tempTrans.setTranslation(Vector3F(firstCharPos[0], getGlobalGUIRect().y + (textLine * fontSize), 0));
	tempTrans.setScale(Vector3F(lastCharPos[0] - firstCharPos[0], fontMetrics.height, 0));
	matEnv.setVariable<Float4x4>("World View Proj", Matrix4x4(matEnv.getVariable<Float4x4>("View Proj")) * tempTrans.calculateMatrix());
	selectionMaterial.applyEnvironment(matEnv);
	selectionMaterial.apply(0, Hash(), renderer);
	guiPlane->draw(renderer);
}

void GLOWE::Components::GUITextField::onRecalcDone()
{
	// Previously regenerateGlyphs was called in renderImpl, but it collided with GUIInputText and its render task that run in parallel.
	constexpr auto empiricalBestCmdListCapacity = 26;
	CommandList renderer(empiricalBestCmdListCapacity);
	regenerateGlyphs(renderer);
	getInstance<RenderingMgr>().addCommandList(std::move(renderer));
}

void GLOWE::Components::GUITextField::regenerateGlyphs(BasicRendererImpl& renderer)
{
	if (!isTextDirty)
	{
		return;
	}

	isTextDirty = false;

	textGlyphs.clear();
	lineBreaks.clear();
	memBuffer.clear();

	if (text.isEmpty())
	{
		lineBreaks.emplace_back(0);
		return;
	}

	auto& font = *usedFont;
	Guard fontGuard(font);

	const auto rect = getGlobalGUIRect();
	const auto spaceWidth = font.getGlyph(fontSize, U' ', renderer).advance[0];
	const auto finalTabStopDistance = spaceWidth * tabulationStopDistance;

	const Utf32String utf32Text = text.toUtf32();
	
	auto shapedGlyphs = font.layoutText(utf32Text, language, fontSize);

	Vector<int> lineOffsets;
	unsigned int currentLineBreak = 0;

	const auto alignLine = [&](const float lineWidth)
	{
		int offsetToApply = 0;
		switch (horizontalAlignment)
		{
		case TextHorizontalAlignment::Justified:
			// fallthrough
		case TextHorizontalAlignment::Left:
			break;
		case TextHorizontalAlignment::Right:
			offsetToApply = static_cast<int>(rect.width - lineWidth);
			break;
		case TextHorizontalAlignment::Middle:
			offsetToApply = static_cast<int>((rect.width - lineWidth) / 2.0f);
			break;
		}
		
		lineOffsets.emplace_back(offsetToApply);
	};

	float verticalOffset;
	// Perform (simple) line breaking pass.
	// TODO: Justification.
	{
		unsigned int lastLineBreakPoint = 0, rollingIndex = 0;
		float currentWidth = 0, currentWordWidth = 0, lastLineBreakSpaceWidth = 0;
		for (const auto& shapedGlyph : shapedGlyphs)
		{
			if (isLineBreakPoint(utf32Text[rollingIndex]))
			{
				lastLineBreakPoint = rollingIndex;
				currentWordWidth = 0;

				switch (utf32Text[rollingIndex])
				{
				case U'\n':
					alignLine(currentWidth);
					lineBreaks.emplace_back(rollingIndex);
					currentWidth = 0;
					lastLineBreakSpaceWidth = 0;
					break;
				case U'\t':
				{
					const auto tabSpaceWidth = useSimpleTabs ? finalTabStopDistance : finalTabStopDistance - std::fmod(currentWidth, finalTabStopDistance);
					currentWidth += tabSpaceWidth;
					lastLineBreakSpaceWidth = tabSpaceWidth;
				}
				break;
				case U' ':
					currentWidth += shapedGlyph.advance[0];
					lastLineBreakSpaceWidth = shapedGlyph.advance[0];
					break;
				default:
					break;
				}
			}
			else
			{
				currentWidth += shapedGlyph.advance[0];
				currentWordWidth += shapedGlyph.advance[0];
				if (currentWidth > rect.width)
				{
					alignLine(currentWidth - currentWordWidth - lastLineBreakSpaceWidth);
					lineBreaks.emplace_back(lastLineBreakPoint);
					currentWidth = currentWordWidth;
				}
			}

			++rollingIndex;
		}
		alignLine(currentWidth);
		lineBreaks.emplace_back(rollingIndex);

		const float currentHeight = lineBreaks.size() * fontSize;
		// Vertical alignment.
		switch (verticalAlignment)
		{
		case TextVerticalAlignment::Down:
			verticalOffset = rect.height - currentHeight;
			break;
		case TextVerticalAlignment::Middle:
			verticalOffset = (rect.height - currentHeight) / 2.0f;
			break;
		case TextVerticalAlignment::Up:
			// fallthrough
		default:
			verticalOffset = 0;
			break;
		}
	}
	
	Vector2F penPos(rect.x + lineOffsets[0], rect.y + fontSize + fontMetrics.descender + verticalOffset);
	//Vector2F penPos(rect.x + lineOffsets[0], rect.y + verticalOffset);
	unsigned int rollingIndex = 0;
	// Perform the actual text layout.
	for (const auto& shapedGlyph : shapedGlyphs)
	{
		textGlyphs.emplace_back();
		auto& current = *std::prev(textGlyphs.end());
		current.character = utf32Text[shapedGlyph.cluster];
		current.cluster = shapedGlyph.cluster;
		current.glyph = &font.getGlyph(fontSize, shapedGlyph, renderer);
		current.pos = (penPos + Vector2F(shapedGlyph.offset[0], shapedGlyph.offset[1]) + Vector2F(static_cast<float>(current.glyph->translation[0]), static_cast<float>(current.glyph->translation[1]))).toFloat2();

		penPos += Vector2F(shapedGlyph.advance[0], shapedGlyph.advance[1]);

		if (rollingIndex == lineBreaks[currentLineBreak])
		{
			++currentLineBreak;
			penPos.setX(rect.x + lineOffsets[currentLineBreak]);
			penPos += Vector2F(0, fontSize);
		}
		
		switch (current.character)
		{
		case U'\t':
		{
			// Reverse undef glyph advance.
			penPos -= Vector2F(shapedGlyph.advance[0], shapedGlyph.advance[1]);
			const auto tabSpaceWidth = useSimpleTabs ? finalTabStopDistance : finalTabStopDistance - std::fmod(penPos[0] - rect.x, finalTabStopDistance);
			penPos += Vector2F(tabSpaceWidth, 0);
		}
		break;
		case U'\n':
			break;
		case U' ':
			break;
		default:
		{
			memBuffer.emplace_back();
			auto& currentBufferElement = *std::prev(memBuffer.end());
			currentBufferElement.pos = current.pos;
			currentBufferElement.atlasPos = { static_cast<float>(current.glyph->subtexInAtlas.pos[0]), static_cast<float>(current.glyph->subtexInAtlas.pos[1]) };
			currentBufferElement.size = { static_cast<float>(current.glyph->subtexInAtlas.size[0]), static_cast<float>(current.glyph->subtexInAtlas.size[1]) };
		}
		break;
		}
		
		++rollingIndex;
	}
}

void GLOWE::Components::GUITextField::renderImpl(BasicRendererImpl& renderer, MaterialEnvironment& matEnv)
{
	if (selectionBegin < selectionEnd && selectionEnd <= textGlyphs.size())
	{
		const auto beginLine = textPosToLinePos(selectionBegin), endLine = textPosToLinePos(selectionEnd);
		if (beginLine >= 0 && endLine >= 0)
		{
			renderer.setPrimitiveTopology(BasicRendererImpl::PrimitiveTopology::TriangleList);
			guiPlane->apply(renderer, getInstance<Engine>().getGraphicsContext(), selectionMaterial.getShaderCollection(0, Hash()));
			if (beginLine != endLine)
			{
				for (unsigned int x = beginLine + 1; x < endLine; ++x)
				{
					const auto firstChar = lineBreaks[x] + 1, lastChar = lineBreaks[x + 1];
					drawLine(firstChar, lastChar, x, renderer, matEnv);
				}

				drawLine(selectionBegin, lineBreaks[beginLine], beginLine, renderer, matEnv);
				drawLine(lineBreaks[endLine - 1] + 1, selectionEnd, endLine, renderer, matEnv);
			}
			else
			{
				drawLine(selectionBegin, selectionEnd, beginLine, renderer, matEnv);
			}
		}
	}
	else
	{
		selectionBegin = selectionEnd = 0;
	}

	if (bufferCapacity < textGlyphs.size())
	{
		resizeBuffer(textGlyphs.capacity(), memBuffer.data());
	}
	else
	{
		renderer.updateBuffer(instanceBuffer.get(), memBuffer.data(), memBuffer.size() * sizeof(InstanceData));
	}

	if (!textGlyphs.empty())
	{
		for (const auto& x : textGlyphs)
		{
			if (x.glyph && x.glyph->atlas)
			{
				usedMaterial.setTexture(texturePos, x.glyph->atlas->getTexture());
				break;
			}
		}
	}

	if (onGlyphsChangedCallback)
	{
		onGlyphsChangedCallback(*this);
	}

	const auto& passes = usedMaterial.getPasses();

	if (!passes.empty())
	{
		renderer.setPrimitiveTopology(BasicRendererImpl::PrimitiveTopology::TriangleList);
		renderedBuffers.apply(renderer, getInstance<Engine>().getGraphicsContext(), usedMaterial.getShaderCollection(passes.begin()->first, Hash()));
		usedMaterial.applyEnvironment(matEnv);

		for (const auto& pass : passes)
		{
			usedMaterial.apply(pass.first, Hash(), renderer);
			guiPlane->drawInstanced(memBuffer.size(), renderer);
		}
	}
	else
	{
		WarningThrow(false, "Unable to render. passes.size() == 0. GameObject: " + getGameObject()->getName() + ".");
	}
}
