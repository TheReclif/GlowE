#pragma once
#ifndef GLOWE_SPOTLIGHTCOMPONENT_INCLUDED
#define GLOWE_SPOTLIGHTCOMPONENT_INCLUDED

#include "../GlowEngine/Component.h"

#include "../GlowSystem/Color.h"

#include "../GlowGraphics/Uniform/LightingMgr.h"

namespace GLOWE
{
	namespace Components
	{
		/// @brief Spot light component. Spot light is a light shaped like a cone. It's described using range, color, attenuation vector, inner cut off angle and outer cut off angle. Its direction is driven by the game object's Transform.
		class GLOWENGINE_EXPORT SpotLight
			: public Component, public LightingMgr::LightBase
		{
		private:
			GLOWE::SpotLight lightInfo;
			LightingMgr::SpotLightHandle spotLightHandle;
			LightingMgr::RegisteredLightHandle lightHandle;
			bool needToUpdate;
		private:
			virtual void updateLight() override;
		public:
			/// @brief Default-initializes the component with given game object as its parent.
			/// @param newParent Pointer to component's parent.
			SpotLight(GameObject* const newParent);

			GlowSerializeComponent(SpotLight, GlowBases(), GlowField(lightInfo));

			/// @brief Sets the light's ambient color.
			/// @param newAmbient New ambient.
			void setAmbient(const Color& newAmbient);
			/// @brief Sets the light's diffuse color.
			/// @param newDiffuse New diffuse.
			void setDiffuse(const Color& newDiffuse);
			/// @brief Sets the light's specular color.
			/// @param newSpecular New specular.
			void setSpecular(const Color& newSpecular);
			/// @brief Sets the attenuation vector. The first component is const attenuation, the second one is linear attenuation and the third one is quadratic attenuation.
			/// @param newAtten Attenuation.
			void setAttenuation(const Float3& newAtten);
			/// @brief Sets the attenuation vector. The first component is const attenuation, the second one is linear attenuation and the third one is quadratic attenuation.
			/// @param newAtten Attenuation.
			void setAttenuation(const Vector3F& newAtten);
			/// @brief Sets the light's range.
			/// @param newRange Range.
			void setRange(const float newRange);
			/// @brief Sets the inner cut off angle in radians. It's measured from the direction axis and specifies which part of the cone will get fully lit.
			/// @param newSpot Inner cut off angle, in radians (degrees * PI / 180).
			void setInnerCutOff(const float newSpot);
			/// @brief Sets the outer cut off angle in radians. It's measured from the direction axis and specifies where the cone ends (it's basically a half of the angle at the cone's vertex).
			/// @param newSpot Outer cut off angle, in radians (degrees * PI / 180).
			void setOuterCutOff(const float newSpot);
			/// @brief Sets the light importance. When more than 8 lights are present in the object's range, they're sorted using the following formula: distance * (1.0f / (light.importance / 1000.0f)) (the bigger the importance the better for the light). Importance defaults to 1000.
			/// @param newImportance New importance.
			void setImportance(const float newImportance);

			virtual void processEvent(const GameEvent& event) override;

			virtual void onFirstActivate() override {};

			// Actually it makes no sense to disable/enable a Transform.
			virtual void onActivate() override;
			virtual void onDeactivate() override;
		};

		GlowInitComponentFactoryReflection(SpotLight);
	}
}

#endif
