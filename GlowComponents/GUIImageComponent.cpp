#include "GUIImageComponent.h"

#include "../GlowMath/Transform.h"

#include "../GlowEngine/GUI/GUIContainer.h"
#include "../GlowEngine/Engine.h"
#include "../GlowEngine/GameObject.h"

GLOWE::Components::GUIImage::GUIImage(GameObject* const newParent)
	: GUIBase(newParent), usedMaterial(), guiPlane(loadResource<Geometry>("GlowCore/BasicGeometry/GUIPlane.gmsh", "GUIPlane")), colorPos(-1)
{
	auto effect = loadResource<Effect>("GlowCore/Shaders/StandardGUIShader.geff", "Effect");
	Material::createFromEffect(usedMaterial, getInstance<Engine>().getGraphicsContext(), effect, "Default", "GUI");
	texPos = usedMaterial.getTexturePosition("Diffuse Texture");
	setImage(loadResource<TextureImpl>("GlowCore/Textures/GUIElementShape.gtex", ""));
	sprite.setBorder(Float4{ 5.34, 5.34, 5.34, 5.34 });
	materialCBufferPos = usedMaterial.getCBufferPosition("Material");
	if (materialCBufferPos >= 0)
	{
		colorPos = usedMaterial.getCBufferFieldPosition(materialCBufferPos, "Color");
		setColor(Color::white);
	}
}

GLOWE::Components::GUIImage::GUIImage(GameObject* const newParent, const AutoFindParentOnStart&)
	: GUIImage(newParent)
{
	autoFindContainer(gameObject);
}

void GLOWE::Components::GUIImage::updateImpl()
{
	using GLOWE::Transform;
	Transform tempTransform;
	const auto& rect = getGlobalGUIRect();
	tempTransform.setTranslation(Float3{ rect.x, rect.y, 0.0f });
	tempTransform.setScale(Float3{ rect.width, rect.height, 1.0f });
	modelMat = tempTransform.calculateMatrix();
}

void GLOWE::Components::GUIImage::renderImpl(BasicRendererImpl& renderer, MaterialEnvironment& matEnv)
{
	matEnv.setVariable<Float4x4>("World View Proj", Matrix4x4(matEnv.getVariable<Float4x4>("View Proj")) * modelMat);

	sprite.apply(matEnv, this);
	usedMaterial.applyEnvironment(matEnv);

	if (texPos >= 0)
	{
		// We use the non-owning overload of the setTexture method to optimize it (sprite has the ownership anyway).
		usedMaterial.setTexture(texPos, sprite.getTexture().getResource());
	}

	const auto& passes = usedMaterial.getPasses();

	if (passes.size())
	{
		renderer.setPrimitiveTopology(BasicRendererImpl::PrimitiveTopology::TriangleList);
		// We must assume that every pass takes the same vertex and instance input and has the same vertex and instance layout.
		guiPlane->apply(renderer, getInstance<Engine>().getGraphicsContext(), usedMaterial.getShaderCollection(passes.begin()->first, Hash()));

		for (const auto& pass : passes)
		{
			usedMaterial.apply(pass.first, Hash(), renderer);
			guiPlane->draw(renderer);
		}
	}
	else
	{
		WarningThrow(false, "Unable to render. passes.size() == 0. GameObject: " + getGameObject()->getName() + ".");
	}
}

void GLOWE::Components::GUIImage::onActivate()
{
	// Empty.
}

void GLOWE::Components::GUIImage::onDeactivate()
{
	// Empty.
}

void GLOWE::Components::GUIImage::setImage(const ResourceHandle<TextureImpl>& texture, const GUIRect& newTextureRect)
{
	sprite.setTexture(texture, newTextureRect);
	markForRedraw();
}

void GLOWE::Components::GUIImage::setColor(const Color& col)
{
	if (colorPos >= 0)
	{
		usedMaterial.modifyCBuffer<Color>(materialCBufferPos, colorPos, col);
		markForRedraw();
	}
}

GLOWE::Material& GLOWE::Components::GUIImage::getMaterial()
{
	return usedMaterial;
}

GLOWE::Sprite& GLOWE::Components::GUIImage::getSprite()
{
	return sprite;
}
