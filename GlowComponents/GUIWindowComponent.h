#pragma once
#ifndef GLOWE_COMPONENTS_GUIWINDOW_INCLUDED
#define GLOWE_COMPONENTS_GUIWINDOW_INCLUDED

#include "../GlowGraphics/Uniform/Geometry.h"

#include "../GlowEngine/Component.h"
#include "../GlowEngine/Material.h"

#include "../GlowEngine/GUI/GUIContainer.h"

#include "GUIButtonComponent.h"

namespace GLOWE
{
	namespace Components
	{
		/// @brief GUI window component. A window consists of a client area (it has its own GUI container), a background image (for default it's a plain color) and optionally a titlebar (if title is enabled) and a close button. All of these are implemented as pointers to owned components.
		class GLOWENGINE_EXPORT GUIWindow
			: public GLOWE::GUIContainer, public Component
		{
		private:
			GUIButton* closeButton;
			GUIImage* background, *titlebarBackground;
			GUITextField* titlebar;
			GUIContainer clientArea;
			GUIContainer::ConstraintHandle clientAreaUpConstraint;
		public:
			/// @brief Minimal constructor.
			/// @param newParent Component's parent.
			GUIWindow(GameObject* const newParent);
			/// @brief The minimal constructor. Also, automatically finds a suitable GUI container.
			/// @param newParent Component's parent.
			/// @param  Default constructed object of AutoFindParentOnStart.
			GUIWindow(GameObject* const newParent, const AutoFindParentOnStart&);
			virtual ~GUIWindow();

			GlowSerializeComponent(GUIWindow, GlowBases());

			/// @brief Adds a close button and a titlebar if there isn't one. Will not work if the window already has a close button. It will add the components to the parenting game object.
			void addCloseButton();
			/// @brief Adds a titlebar and sets the window's title.
			/// @param title Window's title. Defaults to "GlowWindow".
			void addTitlebar(const String& title = u8"GlowWindow");

			// To add elements to the window, use this method first to get its client area (treating GUIWindow as GUIContainer may lead to drawing over the window's elements like titlebar).

			/// @brief Returns the client area's GUI container. Add GUI elements that needs to be contained in the window there.
			/// @return Pointer to client area's GUI container.
			GUIContainer* getClientArea();
			/// @brief Returns the client area's GUI container. Add GUI elements that needs to be contained in the window there.
			/// @return Pointer to client area's GUI container.
			const GUIContainer* getClientArea() const;
			GUIImage* getBackgroundComponent();

			/// @brief Sets the background's texture.
			/// @param texture Background texture.
			void setBackground(const ResourceHandle<TextureImpl>& texture);
			/// @brief Sets the background's color.
			/// @param col Background's color.
			void setBackgroundColor(const Color& col);

			/// @brief Sets the window's title.
			/// @param newTitle Window title.
			void setTitle(const String& newTitle);

			virtual void processEvent(const GameEvent& event) override {};

			virtual void onFirstActivate() override;

			virtual void onActivate() override {};
			virtual void onDeactivate() override {};

			virtual bool checkIsActiveInGUI() const override;
		};

		GlowInitComponentFactoryReflection(GUIWindow);
	}
}

#endif
