#include "RenderableComponentBase.h"

GLOWE::Components::RenderableBase::RenderableBase(GameObject* const newParent)
	: Component(newParent), Renderable(), Cullable()
{
}
