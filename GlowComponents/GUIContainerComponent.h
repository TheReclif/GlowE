#pragma once
#ifndef GLOWE_COMPONENTS_GUICONTAINER_INCLUDED
#define GLOWE_COMPONENTS_GUICONTAINER_INCLUDED

#include "../GlowEngine/Component.h"
#include "../GlowEngine/GUI/GUIContainer.h"

namespace GLOWE
{
	namespace Components
	{
		/// @brief GUI container component. Allows the container to exist since the GLOWE::GUIContainer is not a component and, therefore, cannot exist on its own in the game object.
		class GLOWENGINE_EXPORT GUIContainer
			: public Component, public GLOWE::GUIContainer
		{
		public:
			/// @brief The minimal constructor.
			/// @param newParent Component's parent.
			GUIContainer(GameObject* const newParent);

			GlowSerializeComponent(GUIContainer, GlowBases());

			virtual void processEvent(const GameEvent& event) override {};

			virtual void onFirstActivate() override;

			virtual void onActivate() override;
			virtual void onDeactivate() override;

			virtual bool checkIsActiveInGUI() const override;
		};

		GlowInitComponentFactoryReflection(GUIContainer);
	}
}

#endif
