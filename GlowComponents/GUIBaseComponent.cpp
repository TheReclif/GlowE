#include "GUIBaseComponent.h"
#include "../GlowEngine/GameObject.h"

GLOWE::Components::GUIBase::GUIBase(GameObject* const newParent)
	: Component(newParent), GUIElement()
{
}

void GLOWE::Components::GUIBase::processGUIEvent(const GameEvent::GUIEvent& event)
{
	if (gameObject)
	{
		GameEvent tempEvent;
		tempEvent.type = GameEvent::Type::GUI;
		tempEvent.info.guiEvent = event;
		gameObject->sendEvent(tempEvent, this);
	}
}

void GLOWE::Components::GUIBase::onFirstActivate()
{
	autoFindContainer(gameObject);
}

void GLOWE::Components::GUIBase::onActivate()
{
	GuiMutexLock guard(guiMutex);
	scheduleRecalc();
}

void GLOWE::Components::GUIBase::onDeactivate()
{
	GuiMutexLock guard(guiMutex);
	scheduleRecalc();
}

bool GLOWE::Components::GUIBase::checkIsActiveInGUI() const
{
	return checkIsActive();
}
