#include "GUIScroll.h"
#include "../GlowEngine/GameObject.h"

void GLOWE::Components::GUIScroll::setupConstraints()
{
	GUIConstraint constraint{};

	const auto parent = this;
	constraint.type = isVertical ? GUIConstraint::Type::FixedLeft : GUIConstraint::Type::FixedUp;
	constraint.fixedData.distance = 0;
	constraint.fixedData.whichEdge = GUIConstraint::Edge::LeftUp;
	constraint.fixedData.whichElement = this;
	parent->addConstraint(scrollButton, constraint);
	parent->addConstraint(background, constraint);
	upLeftButton->addConstraintToThis(GUIConstraint(isVertical ? GUIConstraint::Type::FixedLeft : GUIConstraint::Type::FixedUp, 4, this, GUIConstraint::Edge::LeftUp));
	bottomRightButton->addConstraintToThis(GUIConstraint(isVertical ? GUIConstraint::Type::FixedLeft : GUIConstraint::Type::FixedUp, 4, this, GUIConstraint::Edge::LeftUp));

	constraint.type = isVertical ? GUIConstraint::Type::FixedRight : GUIConstraint::Type::FixedDown;
	constraint.fixedData.whichEdge = GUIConstraint::Edge::RightDown;
	parent->addConstraint(scrollButton, constraint);
	parent->addConstraint(background, constraint);
	upLeftButton->addConstraintToThis(GUIConstraint(isVertical ? GUIConstraint::Type::FixedRight : GUIConstraint::Type::FixedDown, 4, this, GUIConstraint::Edge::RightDown));
	bottomRightButton->addConstraintToThis(GUIConstraint(isVertical ? GUIConstraint::Type::FixedRight : GUIConstraint::Type::FixedDown, 4, this, GUIConstraint::Edge::RightDown));

	constraint.type = isVertical ? GUIConstraint::Type::FixedDown : GUIConstraint::Type::FixedRight;
	constraint.fixedData.whichEdge = GUIConstraint::Edge::RightDown;
	bottomRightButton->addConstraintToThis(GUIConstraint(isVertical ? GUIConstraint::Type::FixedDown : GUIConstraint::Type::FixedRight, 4, this, GUIConstraint::Edge::RightDown));

	constraint.type = isVertical ? GUIConstraint::Type::FixedUp : GUIConstraint::Type::FixedLeft;
	constraint.fixedData.whichEdge = GUIConstraint::Edge::LeftUp;
	upLeftButton->addConstraintToThis(GUIConstraint(isVertical ? GUIConstraint::Type::FixedUp : GUIConstraint::Type::FixedLeft, 4, this, GUIConstraint::Edge::LeftUp));

	constraint.type = isVertical ? GUIConstraint::Type::FixedUp : GUIConstraint::Type::FixedLeft;
	constraint.fixedData.whichEdge = GUIConstraint::Edge::LeftUp;
	parent->addConstraint(background, constraint);

	constraint.type = isVertical ? GUIConstraint::Type::FixedDown : GUIConstraint::Type::FixedRight;
	constraint.fixedData.whichEdge = GUIConstraint::Edge::RightDown;
	parent->addConstraint(background, constraint);

	// Scroll button pos update.
	const GUIRect upLeftButtonRect = upLeftButton->getLocalGUIRect();
	if (isVertical)
	{
		scrollButton->setLocalGUIRect(GUIRect({ 0, upLeftButtonRect.y + upLeftButtonRect.height }, { 30, 30 }));
	}
	else
	{
		scrollButton->setLocalGUIRect(GUIRect({ upLeftButtonRect.x + upLeftButtonRect.width, 0 }, { 30, 30 }));
	}
}

bool GLOWE::Components::GUIScroll::onAfterElementsUpdate()
{
	if (!linearLayout)
	{
		changeInPixels = currentButtonPos = 0;
		return false;
	}
	const auto& topLeftRect = upLeftButton->getLocalGUIRect();
	const float barSize = calculateBarSize();
	const float finalScrollPos = clamp((currentButtonPos * barSize) + changeInPixels, 0.0f, barSize), in01ScrollPos = finalScrollPos / barSize;
	linearLayout->setOffset(-(in01ScrollPos * linearLayout->getContentSize()));
	if (isVertical)
	{
		scrollButton->setLocalPosition({ 0, finalScrollPos + topLeftRect.height });
	}
	else
	{
		scrollButton->setLocalPosition({ finalScrollPos + topLeftRect.width, 0 });
	}

	currentButtonPos = in01ScrollPos;
	changeInPixels = 0;

	return true;
}

void GLOWE::Components::GUIScroll::onFirstActivate()
{
	autoFindContainer(gameObject);
}

GLOWE::Components::GUIScroll::GUIScroll(GameObject* const newParent, const bool vertical)
	: ScriptBehaviour(newParent), isVertical(vertical), isMouseInside(false), wasMouseClicked(false), isMouseScrollEnabled(true), upLeftButton(nullptr), bottomRightButton(nullptr), scrollButton(nullptr), background(nullptr), linearLayout(nullptr), onScrollMovedCallback(nullptr), currentButtonPos(0), changeInPixels(0)
{
	autoFindContainer(gameObject->getParent());

	background = &newParent->addComponent<GUIImage>();
	background->setParent(this);

	upLeftButton = &newParent->addComponent<GUIButton>();
	upLeftButton->setParent(this);
	upLeftButton->setLocalGUIRect(GUIRect({}, { 20, 25 }));
	upLeftButton->setOnClickCallback([this](GUIButton&)
	{
		moveScrollBy(-25);
	});
	GUIImage* const upLeftArrowImage = upLeftButton->getBackgroundImage();
	bottomRightButton = &newParent->addComponent<GUIButton>();
	bottomRightButton->setParent(this);
	bottomRightButton->setLocalGUIRect(GUIRect({}, { 20, 25 }));
	bottomRightButton->setOnClickCallback([this](GUIButton&)
	{
		moveScrollBy(25);
	});
	GUIImage* const downRightArrowImage = bottomRightButton->getBackgroundImage();
	scrollButton = &newParent->addComponent<GUIButton>();
	scrollButton->setParent(this);
	scrollButton->setBaseColor(Color::grey);

	scrollButton->setOnEnterCallback([this](GUIButton&)
	{
		isMouseInside = true;
	});
	scrollButton->setOnLeaveCallback([this](GUIButton&)
	{
		isMouseInside = false;
	});

	if (isVertical)
	{
		upLeftArrowImage->setImage(loadResource<TextureImpl>("GlowCore/Textures/ArrowUp.gtex", ""));
		downRightArrowImage->setImage(loadResource<TextureImpl>("GlowCore/Textures/ArrowDown.gtex", ""));
	}
	else
	{
		upLeftArrowImage->setImage(getInstance<ResourceMgr>().load<TextureImpl>("GlowCore/Textures/ArrowLeft.gtex", ""));
		downRightArrowImage->setImage(getInstance<ResourceMgr>().load<TextureImpl>("GlowCore/Textures/ArrowRight.gtex", ""));
	}

	upLeftArrowImage->getSprite().setBorder(Float4{ 0, 0, 0, 0 });
	downRightArrowImage->getSprite().setBorder(Float4{ 0, 0, 0, 0 });

	setupConstraints();
	background->placeAtFront();
}

void GLOWE::Components::GUIScroll::setParent(GUIContainer* const newParent)
{
	if (getParent() == newParent)
	{
		return;
	}
	GUIElement::setParent(newParent);
	if (newParent)
	{
		setupConstraints();
	}
}

GLOWE::GUILinearLayout* GLOWE::Components::GUIScroll::getLayout() const
{
	return linearLayout;
}

void GLOWE::Components::GUIScroll::setManagedLayout(GLOWE::GUILinearLayout* const layout)
{
	if (layout && (layout->getParent() != getParent()))
	{
		throw GUIException("Layout must have the same parent as the scroll");
	}
	linearLayout = layout;
	if (linearLayout)
	{
		linearLayout->setOffsetChangedCallback([this](const float)
		{
			if (onScrollMovedCallback)
			{
				onScrollMovedCallback();
			}
		});
		linearLayout->addConstraintToThis(GUIConstraint(this));
	}
	scheduleRecalc();
}

void GLOWE::Components::GUIScroll::setMouseScroll(const bool enableMouseScroll)
{
	isMouseScrollEnabled = enableMouseScroll;
}

void GLOWE::Components::GUIScroll::autoFindGUIContainer()
{
	autoFindContainer(gameObject);
}

void GLOWE::Components::GUIScroll::setOnScrollMovedCallback(const std::function<void()>& func)
{
	onScrollMovedCallback = func;
}

void GLOWE::Components::GUIScroll::placeAtBack()
{
	GUIElement::placeAtBack();
	background->placeAtBack();
	upLeftButton->placeAtBack();
	bottomRightButton->placeAtBack();
	scrollButton->placeAtBack();
}

void GLOWE::Components::GUIScroll::placeAtFront()
{
	scrollButton->placeAtFront();
	bottomRightButton->placeAtFront();
	upLeftButton->placeAtFront();
	background->placeAtFront();
	GUIElement::placeAtFront();
}

void GLOWE::Components::GUIScroll::placeBefore(GUIElement* const beforeWhat)
{
	GUIElement::placeBefore(beforeWhat);
	background->placeBefore(beforeWhat);
	upLeftButton->placeBefore(beforeWhat);
	bottomRightButton->placeBefore(beforeWhat);
	scrollButton->placeBefore(beforeWhat);
}

float GLOWE::Components::GUIScroll::calculateBarSize() const
{
	const auto& topLeftRect = upLeftButton->getLocalGUIRect(), & bottomRightRect = bottomRightButton->getLocalGUIRect();
	const float barSize =
		isVertical ?
		(bottomRightRect.y - topLeftRect.y - topLeftRect.height - scrollButton->getLocalGUIRect().height)
		:
		(bottomRightRect.x - topLeftRect.x - topLeftRect.width - scrollButton->getLocalGUIRect().width);
	return barSize;
}

void GLOWE::Components::GUIScroll::processEvent(const GameEvent& event)
{
	if (event != GameEvent::Type::System)
	{
		return;
	}

	const Event& sysEvent = event.info.systemEvent;
	switch (sysEvent)
	{
	case Event::Type::MouseButtonPressed:
		if (isMouseInside)
		{
			lastMousePos = Float2{ static_cast<float>(sysEvent.mouseButtonInfo.x), static_cast<float>(sysEvent.mouseButtonInfo.y) };
			wasMouseClicked = true;
		}
		break;
	case Event::Type::MouseButtonReleased:
		wasMouseClicked = false;
		break;
	case Event::Type::MouseMoved:
		if (wasMouseClicked)
		{
			GuiMutexLock lock(guiMutex);
			const Vector2F currentMousePos(static_cast<float>(sysEvent.mouseMoveInfo.x), static_cast<float>(sysEvent.mouseMoveInfo.y)), lastPos(lastMousePos), diff = currentMousePos - lastPos;
			lastMousePos = currentMousePos.toFloat2();
			const float scalarDiff = diff[isVertical ? 1 : 0];

			moveScrollBy(scalarDiff);
		}
		break;
	case Event::Type::MouseWheelScrolled:
		if (isMouseScrollEnabled)
		{
			GuiMutexLock lock(guiMutex);
			if (isVertical)
			{
				if (sysEvent.mouseWheelScrollInfo.wheel == Mouse::Wheel::Vertical)
				{
					moveScrollBy(-5.0f * sysEvent.mouseWheelScrollInfo.delta);
				}
			}
			else
			{
				if (sysEvent.mouseWheelScrollInfo.wheel == Mouse::Wheel::Horizontal)
				{
					moveScrollBy(-5.0f * sysEvent.mouseWheelScrollInfo.delta);
				}
			}
		}
		break;
	}
}

// amount - pixels.
void GLOWE::Components::GUIScroll::moveScrollBy(const float amount)
{
	changeInPixels += amount;
	scheduleRecalc();
}

void GLOWE::Components::GUIScroll::moveScrollTo(const float in01Pos)
{
	currentButtonPos = in01Pos;
	scheduleRecalc();
}
