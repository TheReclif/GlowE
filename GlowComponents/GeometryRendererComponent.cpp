#include "GeometryRendererComponent.h"
#include "TransformComponent.h"
#include "../GlowGraphics/Uniform/LightingMgr.h"
#include "../GlowEngine/Engine.h"
#include "../GlowEngine/GameObject.h"

void GLOWE::Components::GeometryRenderer::render(BasicRendererImpl& renderer, MaterialEnvironment& matEnv)
{
	if (!material || !geometry)
	{
		WarningThrow(false, "Trying to render invalid GeometryRenderer (material or geometry not set).");
		return;
	}

	const Matrix4x4 worldMat = gameObject->getTransform()->getMatrix();

	matEnv.setVariable<Float4x4>("World", worldMat);
	matEnv.setVariable<Float4x4>("World Inv Transpose", worldMat.inverse().transpose());
	matEnv.setVariable<Float4x4>("World View Proj", Matrix4x4(matEnv.getVariableByConstRef<Float4x4>("View Proj")) * worldMat);
	if (bestDirLights.size())
	{
		matEnv.setVariable("Dir Lights", bestDirLights.front(), sizeof(DirectionalLight) * bestDirLights.size());
	}
	if (bestPointLights.size())
	{
		matEnv.setVariable("Point Lights", bestPointLights.front(), sizeof(PointLight) * bestPointLights.size());
	}
	if (bestSpotLights.size())
	{
		matEnv.setVariable("Spot Lights", bestSpotLights.front(), sizeof(SpotLight) * bestSpotLights.size());
	}

	material->applyEnvironment(matEnv);

	const auto& passes = material->getPasses();
	if (!passes.empty())
	{
		Hash variant = getVariantHash();

		renderer.setPrimitiveTopology(usedTopology);
		// We must assume that every pass takes the same vertex and instance input and has the same vertex and instance layout.
		geometry->apply(renderer, getInstance<Engine>().getGraphicsContext(), material->getShaderCollection(passes.begin()->first, variant));

		for (const auto& pass : passes)
		{
			material->apply(pass.first, variant, renderer);
			geometry->draw(renderer);
		}
	}
	else
	{
		WarningThrow(false, "Unable to render. passes.size() == 0. GameObject: " + getGameObject()->getName() + ".");
	}
}

GLOWE::Hash GLOWE::Components::GeometryRenderer::recalculateVariantHashCustom()
{
	if (material && material->checkIsValid())
	{
		const auto& tags = material->getTags();
		String toCreateHash;
		toCreateHash.reserve(20);

		LightingMgr& lightMgr = getInstance<LightingMgr>();

		for (const auto& x : tags)
		{
			using Type = GSILShader::Technique::Tag::Type;

			switch (x.type)
			{
			case Type::IsTexturePresent:
				toCreateHash += material->checkIsTexturePresent(x.isTexturePresentInfo.texID) ? '1' : '0';
				break;
			case Type::SupportsDirectionalLights:
				toCreateHash += toString(bestDirLights.size());
				break;
			case Type::SupportsPointLights:
				toCreateHash += toString(bestPointLights.size());
				break;
			case Type::SupportsSpotLights:
				toCreateHash += toString(bestSpotLights.size());
				break;
			}

			toCreateHash += '|';
		}

		if (toCreateHash.getSize() > 0)
		{
			toCreateHash.erase(toCreateHash.getSize() - 1); // Erase the last '|'.
		}

		return Hash(toCreateHash);
	}

	return Hash();
}

GLOWE::Material* GLOWE::Components::GeometryRenderer::getMaterialPtr() const
{
	return &(*material);
}

bool GLOWE::Components::GeometryRenderer::isExcludedFromRenderingCustom(const RenderingEngine::RenderPass& renderPass) const
{
	return !gameObject->checkIsActive() || !getCullTestResult() || renderPass.notRendered.count(gameObject->getLayer()) == 1;
	//return false; // FOR DEBUG ONLY.
}

bool GLOWE::Components::GeometryRenderer::performCulling(const Frustum& frustum)
{
	if (needToUpdateBounds)
	{
		worldBounds = geometry->localAABB.transform(gameObject->getTransform()->getMatrix());
		needToUpdateBounds = false;
	}

	const bool isInside = frustum.isAABBInside(worldBounds);

	if (isInside)
	{
		// Update lights.
		LightingMgr& lightMgr = getInstance<LightingMgr>();

		bestDirLights = lightMgr.getBestDirLights();
		bestPointLights = lightMgr.getBestPointLights(worldBounds);
		bestSpotLights = lightMgr.getBestSpotLights(worldBounds);
		recalculateVariantHash();
	}

	return isInside;
}

GLOWE::Components::GeometryRenderer::GeometryRenderer(GameObject* const newParent)
	: RenderableBase(newParent), variantToChoose(), material(), geometry(), worldBounds(), needToUpdateBounds(), bestDirLights(), bestPointLights(), bestSpotLights(), usedTopology(BasicRendererImpl::PrimitiveTopology::TriangleList)
{
	Renderable::init();
}

GLOWE::Components::GeometryRenderer::~GeometryRenderer()
{
	onDeactivate(); // Just in case.
	Renderable::cleanUp();
}

void GLOWE::Components::GeometryRenderer::setGeometry(const ResourceHandle<Geometry>& newGeometry)
{
	geometry = newGeometry;
	needToUpdateBounds = true;
}

const GLOWE::ResourceHandle<GLOWE::Geometry>& GLOWE::Components::GeometryRenderer::getGeometry() const
{
	return geometry;
}

void GLOWE::Components::GeometryRenderer::setMaterial(const ResourceHandle<Material>& newMaterial)
{
	Renderable::cleanUp();
	material = newMaterial;
	Renderable::init();
}

const GLOWE::ResourceHandle<GLOWE::Material>& GLOWE::Components::GeometryRenderer::getMaterial() const
{
	return material;
}

void GLOWE::Components::GeometryRenderer::setPrimitiveTopology(const BasicRendererImpl::PrimitiveTopology newTopo)
{
	usedTopology = newTopo;
}

void GLOWE::Components::GeometryRenderer::onActivate()
{
	Renderable::setActive(true); // TODO
	onCullableActivate();
}

void GLOWE::Components::GeometryRenderer::onDeactivate()
{
	Renderable::setActive(false); // TODO
	onCullableDeactivate();
}
