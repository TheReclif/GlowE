#include "GUITopComponent.h"
#include "../GlowEngine/Engine.h"
#include "../GlowGraphics/Uniform/GraphicsFactory.h"

void GLOWE::Components::GUITop::addToRenderingEngine()
{
	if (wasAddedToRenderEngine)
	{
		return;
	}

	RenderingEngine::RenderPass::Callbacks callbacks;
	RenderingEngine::RenderPass::Settings settings;

	settings.needsLightsCulling = settings.needsRenderablesCulling = false;

	populateRenderCallbacks(callbacks);

	renderingEngineHandle = getInstance<RenderingEngine>().addRenderPass(*renderTarget, { /* Render overriden, vector unused. */ }, engineRenderPassId, callbacks, settings);
	matEnv = &renderingEngineHandle->second.matEnv;
	wasAddedToRenderEngine = true;
}

void GLOWE::Components::GUITop::removeFromRenderingEngine()
{
	if (wasAddedToRenderEngine)
	{
		getInstance<RenderingEngine>().removeRenderPass(renderingEngineHandle);
		wasAddedToRenderEngine = false;
	}
}

void GLOWE::Components::GUITop::resizeRT(const UInt2 size, const Format& templateFormat)
{
	Format formatToUse = templateFormat;
	if (target->checkIsValid())
	{
		formatToUse = target->getDesc().format;
	}

	ownedRenderTarget->destroy();
	target->destroy();
	dsv->destroy();

	TextureDesc desc;
	desc.arraySize = 1;
	desc.binding = TextureDesc::Binding::RenderTarget;
	desc.cpuAccess = CPUAccess::None;
	desc.depth = 1;
	desc.format = formatToUse;
	desc.generateMipmaps = false;
	desc.width = size[0];
	desc.height = size[1];
	desc.mipmapsCount = 1;
	desc.multisamplingLevel = 1;
	desc.type = TextureDesc::Type::Texture2D;
	desc.usage = Usage::Default;

	const auto& gc = engine.getGraphicsContext();
	target->create(desc, gc);

	desc.binding = TextureDesc::Binding::DepthStencil;
	desc.format = Format(Format::Type::Custom, Format::D24S8);

	dsv->create(desc, gc);
	ownedRenderTarget->create(target, dsv, gc);

	removeFromRenderingEngine();
	addToRenderingEngine();

	const int texPos = usedMaterial.getTexturePosition("Diffuse Texture");
	if (texPos >= 0)
	{
		usedMaterial.setTexture(texPos, target.get());
	}
	else
	{
		throw GUIException("\"Diffuse Texture\" not present in the material");
	}
}

GLOWE::Components::GUITop::GUITop(GameObject* const newParent)
	: ScriptBehaviour(newParent), shouldProcessEvents(false), renderTarget(), target(createGraphicsObject<TextureImpl>()), dsv(createGraphicsObject<TextureImpl>()), clearColor(Color::black), engineRenderPassId(1), wasAddedToRenderEngine(false), matEnv(nullptr), guiPlane(loadResource<Geometry>("GlowCore/BasicGeometry/GUIPlane.gmsh", "GUIPlane")), ownedRenderTarget(createGraphicsObject<RenderTargetImpl>()), shouldClearTarget(false)
{
	auto effect = loadResource<Effect>("GlowCore/Shaders/StandardGUIShader.geff", "Effect");
	Material::createFromEffect(usedMaterial, getInstance<Engine>().getGraphicsContext(), effect, "NoStencil", "GUI");
	const int materialCbufferPos = usedMaterial.getCBufferPosition("Material"), colorPos = usedMaterial.getCBufferFieldPosition(materialCbufferPos, "Color");
	usedMaterial.modifyCBuffer<Color>(materialCbufferPos, colorPos, Color::white);
}

GLOWE::Components::GUITop::~GUITop()
{
	removeFromRenderingEngine();
}

void GLOWE::Components::GUITop::updateImpl()
{
	updateRect();
	const auto& rect = getGlobalGUIRect();
	//cameraMat.orthographicProjection(rect.width, rect.height);
	cameraMat.orthographicProjectionOffScreen(0, rect.width, 0, rect.height);
	matEnv->setVariable<Float4x4>(Hash(u8"View Proj"), cameraMat.getProj());
}

void GLOWE::Components::GUITop::setNotOwnedRenderTarget(RenderTargetImpl* const newTarget, const Format& rtFormat)
{
	renderTarget = newTarget;
	GUIRect tempRect;
	tempRect.x = tempRect.y = 0.0f;
	tempRect.size = renderTarget->getSize();
	setGlobalGUIRect(tempRect);

	resizeRT(UInt2{ static_cast<unsigned int>(tempRect.width), static_cast<unsigned int>(tempRect.height) }, rtFormat);
}

void GLOWE::Components::GUITop::processEvent(const GameEvent& gameEvent)
{
	if (shouldProcessEvents)
	{
		using GType = GameEvent::Type;
		using EType = Event::Type;

		switch (gameEvent)
		{
		case GType::System:
			{
				const Event& event = gameEvent.info.systemEvent;
				GameEvent::GUIEvent tempGUIEvent{};
				switch (event)
				{
				case EType::Resized:
				{
					GUIRect tempRect;
					tempRect.x = tempRect.y = 0;
					tempRect.width = static_cast<float>(event.resizeInfo.newWidth);
					tempRect.height = static_cast<float>(event.resizeInfo.newHeight);
					setGlobalGUIRect(tempRect);
					for (const auto& x : elementsOrder)
					{
						if (x->checkIsActiveInGUI())
						{
							x->scheduleRecalc();
						}
					}
				}
				break;
				case EType::MouseButtonPressed:
				{
					tempGUIEvent.type = GameEvent::GUIEvent::Type::MouseButtonDownInside;
					tempGUIEvent.mouseButtonInfo = event.mouseButtonInfo;

					for (auto& elem : elementsOrder)
					{
						if (elem->checkIsActiveInGUI() && elem->getGlobalGUIRect().isPointInside(Float2{ static_cast<float>(event.mouseButtonInfo.x), static_cast<float>(event.mouseButtonInfo.y) }))
						{
							tempGUIEvent.whichElement = elem;
							elem->processGUIEvent(tempGUIEvent);
						}
					}
				}
				break;
				case EType::MouseButtonReleased:
				{
					tempGUIEvent.type = GameEvent::GUIEvent::Type::MouseButtonUpInside;
					tempGUIEvent.mouseButtonInfo = event.mouseButtonInfo;

					for (auto& elem : elementsOrder)
					{
						if (elem->checkIsActiveInGUI() && elem->getGlobalGUIRect().isPointInside(Float2{ static_cast<float>(event.mouseButtonInfo.x), static_cast<float>(event.mouseButtonInfo.y) }))
						{
							tempGUIEvent.whichElement = elem;
							elem->processGUIEvent(tempGUIEvent);
						}
					}
				}
				break;
				case EType::MouseMoved:
				{
					tempGUIEvent.type = GameEvent::GUIEvent::Type::MouseMovedInside;
					tempGUIEvent.mouseMoveInfo = event.mouseMoveInfo;

					for (auto& elem : elementsOrder)
					{
						if (!elem->checkIsActiveInGUI())
						{
							continue;
						}

						bool testResult = elem->getGlobalGUIRect().isPointInside(Float2{ static_cast<float>(event.mouseMoveInfo.x), static_cast<float>(event.mouseMoveInfo.y) });

						if (testResult)
						{
							if (elementsWithMouseInside.count(elem) == 0)
							{
								elementsWithMouseInside.emplace(elem);
								testResult = false;

								GameEvent::GUIEvent tempEvent;
								tempEvent.type = GameEvent::GUIEvent::Type::MouseEntered;
								tempEvent.whichElement = elem;
								tempEvent.mouseMoveInfo = event.mouseMoveInfo;

								elem->processGUIEvent(tempEvent);
							}
						}
						else
						{
							const auto elemIt = elementsWithMouseInside.find(elem);
							if (elemIt != elementsWithMouseInside.end())
							{
								elementsWithMouseInside.erase(elemIt);

								GameEvent::GUIEvent tempEvent;
								tempEvent.type = GameEvent::GUIEvent::Type::MouseLeft;
								tempEvent.whichElement = elem;
								tempEvent.mouseMoveInfo = event.mouseMoveInfo;

								elem->processGUIEvent(tempEvent);
							}
						}

						if (testResult)
						{
							tempGUIEvent.whichElement = elem;
							elem->processGUIEvent(tempGUIEvent);
						}
					}
				}
				break;
				default:
				break;
				}
			}
			break;
		}
	}
}

void GLOWE::Components::GUITop::onScriptActivated()
{
	addToRenderingEngine();
}

void GLOWE::Components::GUITop::onScriptDeactivated()
{
	removeFromRenderingEngine();
}

void GLOWE::Components::GUITop::forceUpdate()
{
	GUIContainer::update();
}

void GLOWE::Components::GUITop::setClearColor(const Color& newClearColor)
{
	clearColor = newClearColor;
}

void GLOWE::Components::GUITop::setShouldClearRT(const bool shouldClear)
{
	shouldClearTarget = shouldClear;
}

bool GLOWE::Components::GUITop::checkIfProcessesEvents() const
{
	return shouldProcessEvents;
}

void GLOWE::Components::GUITop::setEventProcessing(const bool arg)
{
	shouldProcessEvents = arg;
}

void GLOWE::Components::GUITop::renderImpl(BasicRendererImpl& renderer, MaterialEnvironment& matEnvironment)
{
	GUIContainer::renderImpl(renderer, matEnvironment);
}

void GLOWE::Components::GUITop::onRectUpdated()
{
	const auto& localRect = getLocalGUIRect();
	viewport.topX = viewport.topY = 0.0f;
	if (viewport.width != localRect.width || viewport.height != localRect.height)
	{
		viewport.width = localRect.width;
		viewport.height = localRect.height;
		if (target->checkIsValid() && (target->getDesc().width != localRect.width || target->getDesc().height != localRect.height))
		{
			resizeRT(UInt2{ static_cast<unsigned int>(localRect.width), static_cast<unsigned int>(localRect.height) }, Format(Format::Type::Byte, 4, true));
		}
	}
}

void GLOWE::Components::GUITop::populateRenderCallbacks(RenderingEngine::RenderPass::Callbacks& callbacks)
{
	callbacks.clearTargetFunc = [this](BasicRendererImpl& renderer)
	{
		if (!elementsToRedraw.empty())
		{
			renderer.clearDepthStencil(ownedRenderTarget.get());
		}
		if (shouldClearTarget)
		{
			renderer.clearColor(renderTarget, clearColor);
		}
	};
	callbacks.getViewportFunc = [this]() -> const Viewport&
	{
		updateRect();
		return viewport;
	};
	callbacks.renderOverrideGetCommandListsCount = [this]() -> unsigned int
	{
		return elementsOrder.size();
	};
	callbacks.renderOverrideSingleThread = [this](BasicRendererImpl& renderer)
	{
		//render(renderer, *matEnv);
		throw NotImplementedException();
	};
	callbacks.renderOverrideMultiThreaded = [this](ThreadPool& threads, Vector<ThreadPool::TaskResult<void>>& results, Deque<CommandList>& cmdLists)
	{
		if (!elementsToRedraw.empty())
		{
			cmdLists.emplace_back();
			auto& list = cmdLists.back();
			list.setSRV(nullptr, 0, ShaderType::PixelShader);
			list.setRenderTarget(ownedRenderTarget.get());
			if (!invalidatedElements.empty())
			{
				// Draw masks at their old locations.
				processElementsToDraw();
				drawMask(list, *matEnv);

				GUIContainer::update();
				if (getLastUpdateFrame() == getInstance<Engine>().getCurrentFrameId())
				{
					updateImpl();
				}

				onUpdateEnd();
			}

			// Draw masks at their new locations.
			processElementsToDraw();
			drawMask(list, *matEnv);
			results.emplace_back();

			results.reserve(elementsOrder.size() + results.size());

			for (const auto& x : elementsOrder)
			{
				const auto it = elementsToRedraw.find(x);
				if (it != elementsToRedraw.end() && (it->second & GUIContainer::Element))
				{
					const auto temp = x->getGlobalGUIRect().calculateIntersection(getGlobalGUIRect());
					if (temp.width > 0 && temp.height > 0)
					{
						const auto scissorRect = temp.toRect();
						cmdLists.emplace_back();
						results.emplace_back(threads.addTask([x, scissorRect, this](CommandList& result)
							{
								thread_local MaterialEnvironment localMatEnv;
								localMatEnv = *matEnv;
								result.setScissorRect(scissorRect);
								x->render(result, localMatEnv);
							}, std::ref(cmdLists.back())));
					}
				}
			}

			elementsToRedraw.clear();

			const auto templateSize = getGlobalGUIRect().getScaledSize();
			GLOWE::Transform tempTransform;
			tempTransform.setScale(Float3{ static_cast<float>(templateSize[0]), static_cast<float>(templateSize[1]), 0 });
			matEnv->setVariable<Float4x4>("World View Proj", Matrix4x4(matEnv->getVariable<Float4x4>("View Proj")) * tempTransform.calculateMatrix());
		}

		cmdLists.emplace_back();
		auto& list = cmdLists.back();

		const auto templateSize = getGlobalGUIRect().getScaledSize();
		Rect rect;
		rect.topLeft = { 0, 0 };
		rect.bottomRight = { static_cast<int>(templateSize[0]), static_cast<int>(templateSize[1]) };
		list.setScissorRect(rect);

		list.setRenderTarget(renderTarget);
		usedMaterial.applyEnvironment(*matEnv);
		usedMaterial.apply(0, Hash(), list);
		guiPlane->apply(list, engine.getGraphicsContext(), usedMaterial.getShaderCollection(0, Hash()));
		list.setPrimitiveTopology(BasicRendererImpl::PrimitiveTopology::TriangleList);
		guiPlane->draw(list);
		results.emplace_back();
	};
}
