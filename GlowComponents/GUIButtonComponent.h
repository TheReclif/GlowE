#pragma once
#ifndef GLOWE_COMPONENTS_GUIBUTTON_INCLUDED
#define GLOWE_COMPONENTS_GUIBUTTON_INCLUDED

#include "../GlowEngine/GUI/GUIContainer.h"

#include "GUIImageComponent.h"
#include "GUITextFieldComponent.h"
#include "ScriptBehaviour.h"

namespace GLOWE
{
	namespace Components
	{
		// Yep, it's a script. It's easier this way.
	
		/// @brief GUI button component. Made as a script because it simplifies some details of the implementation.
		class GLOWENGINE_EXPORT GUIButton
			: public ScriptBehaviour, public GLOWE::GUIContainer
		{
		private:
			enum class MouseState
			{
				Outside,
				Hovering,
				Clicked
			};
		private:
			MouseState mouseState;
			Color baseColor, highlightColor, pressedColor, lastColor;
			Time lerpTime, timer;

			bool wasClickedInside, isDuringLerp;
			std::function<void(const Color&)> setColorFunc;
			std::function<void(GUIButton&)> onClickCallback, onEnterCallback, onLeaveCallback;
			GUIImage* image;
			GUITextField* text;
		private:
			void onGUIEvent(const GameEvent::GUIEvent& event);
			void setColor(const Color& col);
			void setupTextConstraints();
			void setLastColor();
		protected:
			// In this function, force update on all depending components.
			//virtual void onRecalcSchedule() override;
			virtual bool checkIsActiveInGUI() const override;
		public:
			/// @brief The minimal constructor.
			/// @param newParent Component's parent.
			GUIButton(GameObject* const newParent);
			virtual ~GUIButton();

			GlowSerializeScript(GUIButton, GlowBases(), GlowField(baseColor), GlowField(highlightColor), GlowField(pressedColor), GlowField(lerpTime));

			/// @brief Automatically finds a fitting GUI container by moving up in the game object's hierarchy. Will not work if the component already has a parent (i.e. GUI container).
			void autoFindGUIContainer();

			/// @brief Sets the base button color.
			/// @param col New base color.
			void setBaseColor(const Color& col);
			/// @brief Sets the highlight color. Shown when the user hovers the cursor over the button, but does not click the mouse button.
			/// @param col New highlight color.
			void setHighlightColor(const Color& col);
			/// @brief Sets the pressed color. Shown when the user clicks the button.
			/// @param col New pressed color.
			void setPressedColor(const Color& col);

			/// @brief Sets the lerping (color transition) time.
			/// @param newTime New transition time.
			void setLerpingTime(const Time& newTime);

			/// @brief Returns the background image's GUI image (a component).
			/// @return Pointer to the background's GUIImage.
			GUIImage* getBackgroundImage() const;
			/// @brief Returns the text's GUI text field.
			/// @return Pointer to the text's GUITextField.
			GUITextField* getTextField() const;

			/// @brief Returns the button's text.
			/// @return Button's text.
			String getText() const;
			/// @brief Sets the button's text.
			/// @param newText Button's text.
			void setText(const String& newText);

			/// @brief Sets the font size.
			/// @param newSize New font size.
			void setFontSize(const unsigned int newSize);

			virtual void onFirstActivate() override;

			//virtual void onActivate() override;
			virtual void onScriptDeactivated() override;

			//virtual void processEvent(const GameEvent& event) override;
			virtual void processGUIEvent(const GameEvent::GUIEvent& event) override;
			virtual void update() override;

			/// @brief Sets the on click callback. Called every time the button is successfully clicked.
			/// @param callback Callback. A function that returns nothing and its only parameter is a reference to the affected button.
			void setOnClickCallback(const std::function<void(GUIButton&)>& callback);
			/// @brief Sets the on enter callback. Called every time the cursor enters the button.
			/// @param callback Callback. A function that returns nothing and its only parameter is a reference to the affected button.
			void setOnEnterCallback(const std::function<void(GUIButton&)>& callback);
			/// @brief Sets the on leave callback. Called every time the cursor leaves the button.
			/// @param callback Callback. A function that returns nothing and its only parameter is a reference to the affected button.
			void setOnLeaveCallback(const std::function<void(GUIButton&)>& callback);
			/// @brief Sets the set color function. Use when the material does not contain a "Color" field or it doesn't correspond to the button's color.
			/// @param colorFunc Set color function.
			void setCustomSetColorFunction(const std::function<void(const Color&)>& colorFunc);
		};

		GlowInitComponentFactoryReflection(GUIButton);
	}
}

#endif
