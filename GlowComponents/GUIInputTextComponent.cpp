#include "GUIInputTextComponent.h"
#include "GUITextFieldComponent.h"

#include "../GlowEngine/GUI/GUIContainer.h"

#include "../GlowWindow/Uniform/Cursor.h"

void GLOWE::Components::GUIInputField::updateCaretHighlight(Utf32String&& newString)
{
	textString = std::move(newString);
	const auto strSize = textString.size();
	switch (whatActive)
	{
	case WhatActive::Highlight:
		if (highlightStartPos > strSize)
		{
			highlightStartPos = strSize;
		}
#if __cplusplus >= 201703L
		[[fallthrough]];
#endif
	case WhatActive::Caret:
		if (caretHighlightPos > strSize)
		{
			caretHighlightPos = strSize;
		}
		break;
	default:
		break;
	}

	if (caretHighlightPos == highlightStartPos)
	{
		whatActive = WhatActive::Caret;
	}
}

unsigned int GLOWE::Components::GUIInputField::worldPosToTextPos(const Vector2F& worldPos) const
{
	if (managedText == nullptr)
	{
		throw Exception("managedText == nullptr");
	}

	const auto res = managedText->localPosToGlyphPos(worldPos - managedText->getGlobalGUIRect().pos);
	return res < 0 ? 0 : res;
}

void GLOWE::Components::GUIInputField::renderImpl(BasicRendererImpl& renderer, MaterialEnvironment& matEnv)
{
	if (managedText == nullptr || whatActive == WhatActive::Nothing)
	{
		return;
	}

	const auto& passes = material.getPasses();
	if (passes.empty())
	{
		return;
	}
	renderer.setPrimitiveTopology(BasicRendererImpl::PrimitiveTopology::TriangleList);

	const auto& GLOW_RESTRICT(glyphs) = managedText->getGlyphs();
	const auto fontSize = managedText->getFontMetrics().ascender;

	if (timer < (blinkTime / 2.f))
	{
		const auto linePos = (!glyphs.empty()) ? managedText->textPosToLinePos(caretHighlightPos) : 0;
		if (linePos < 0)
		{
			return;
		}
		const float caretY = getGlobalGUIRect().y + linePos * fontSize;
		GLOWE::Transform tempTransform;
		if (caretHighlightPos < textString.size())
		{
			tempTransform.setTranslation(Vector3F(glyphs[caretHighlightPos].pos[0] - glyphs[caretHighlightPos].glyph->translation[0], caretY, 0));
		}
		else
		{
			if (!glyphs.empty() && glyphs.back().character != U'\n')
			{
				const auto& glyph = glyphs.back();
				tempTransform.setTranslation(Vector3F(glyph.pos[0] + (glyph.glyph != nullptr ? glyph.glyph->size[0] : 0), caretY, 0));
			}
			else
			{
				tempTransform.setTranslation(Vector3F(getGlobalGUIRect().x, caretY, 0));
			}
		}
		tempTransform.setScale(Vector3F(caretWidth, fontSize, 1));
		matEnv.setVariable<Float4x4>("World View Proj", Matrix4x4(matEnv.getVariable<Float4x4>("View Proj")) * tempTransform.calculateMatrix());
		material.applyEnvironment(matEnv);
		guiPlane->apply(renderer, engine.getGraphicsContext(), material.getShaderCollection(passes.begin()->first, Hash()));

		for (const auto& pass : passes)
		{
			material.apply(pass.first, Hash(), renderer);
			guiPlane->draw(renderer);
		}
	}
}

GLOWE::Components::GUIInputField::GUIInputField(GameObject* const newParent)
	: ScriptBehaviour(newParent), guiPlane(loadResource<Geometry>("GlowCore/BasicGeometry/GUIPlane.gmsh", "GUIPlane"))
{
	auto effect = loadResource<Effect>("GlowCore/Shaders/StandardGUIShader.geff", "Effect");
	Material::createFromEffect(material, getInstance<Engine>().getGraphicsContext(), effect, "Default", "GUI");
	material.setTexture(0, loadResource<TextureImpl>("GlowCore/Textures/PlainColorTexture.gtex", ""));

	materialCaretCBuffer = material.getCBufferPosition("Material");
	if (materialCaretCBuffer >= 0)
	{
		caretColorPos = material.getCBufferFieldPosition(materialCaretCBuffer, "Color");
		setCaretColor(Color::from255RGBA(224, 204, 251));
	}
}

void GLOWE::Components::GUIInputField::setValidator(const std::function<bool(Char32)>& validatorFunc)
{
	validatorFunction = validatorFunc;
}

void GLOWE::Components::GUIInputField::setCaretColor(const Color& newColor)
{
	if (materialCaretCBuffer < 0 || caretColorPos < 0)
	{
		return;
	}

	material.modifyCBuffer<Color>(materialCaretCBuffer, caretColorPos, newColor);

	if (whatActive == WhatActive::Caret && managedText)
	{
		markForRedraw();
	}
}

void GLOWE::Components::GUIInputField::setHighlightColor(const Color& newColor)
{
	if (managedText)
	{
		managedText->setSelectionColor(newColor);
	}
}

void GLOWE::Components::GUIInputField::setCaretWidth(const float width)
{
	if (caretWidth != width)
	{
		caretWidth = width;
		if (whatActive == WhatActive::Caret && managedText)
		{
			markForRedraw();
		}
	}
}

void GLOWE::Components::GUIInputField::setManagedText(GUITextField* const text)
{
	if (managedText == text)
	{
		return;
	}

	managedText = text;
	if (!managedText)
	{
		return;
	}

	orphan();
	managedText->getParent()->addElementBefore(this, managedText);
	managedText->placeBefore(this);
	managedText->setOnGlyphsChangedCallback([this](const GUITextField& text)
	{
		// TODO: Check if the lock is needed.
		//ComponentLock _(text), _2(*this);
		auto str = text.getText().toUtf32();
		if (textString != str)
		{
			updateCaretHighlight(std::move(str));
		}
	});
	stretchToElement(managedText);
	textString = managedText->getText().toUtf32();
}

void GLOWE::Components::GUIInputField::setMultilineInputAllowed(const bool multilineAllowed)
{
	allowsMultilineInput = multilineAllowed;
}

void GLOWE::Components::GUIInputField::processGUIEvent(const GameEvent::GUIEvent& event)
{
	if (!managedText)
	{
		return;
	}

	using T = GameEvent::GUIEvent::Type;
	switch (event.type)
	{
	case T::MouseEntered:
		mouseInside = true;
		getInstance<Cursor>().setCursor(Cursor::CursorType::Beam);
		break;
	case T::MouseLeft:
		mouseInside = false;
		getInstance<Cursor>().setCursor(Cursor::CursorType::Arrow);
		break;
	case T::MouseButtonDownInside:
		mouseDown = true;
		timer = 0;
		highlightStartPos = caretHighlightPos = worldPosToTextPos(Vector2F(static_cast<float>(event.mouseButtonInfo.x), static_cast<float>(event.mouseButtonInfo.y)));
		whatActive = WhatActive::Caret;
		{
			GuiMutexLock _(guiMutex);
			managedText->setSelection(0, 0);
			markForRedraw();
		}
		break;
	case T::MouseMovedInside:
		if (mouseDown)
		{
			timer = 0;
			const auto temp = worldPosToTextPos(Vector2F(static_cast<float>(event.mouseMoveInfo.x), static_cast<float>(event.mouseMoveInfo.y)));
			if (temp != caretHighlightPos)
			{
				caretHighlightPos = temp;
				if (highlightStartPos == caretHighlightPos)
				{
					whatActive = WhatActive::Caret;
				}
				else
				{
					whatActive = WhatActive::Highlight;
				}
				GuiMutexLock _(guiMutex);
				if (caretHighlightPos > highlightStartPos)
				{
					managedText->setSelection(highlightStartPos, caretHighlightPos);
				}
				else
				{
					managedText->setSelection(caretHighlightPos, highlightStartPos);
				}
				markForRedraw();
			}
		}
		break;
	default:
		break;
	}
}

void GLOWE::Components::GUIInputField::processEvent(const GameEvent& event)
{
	if (!managedText || event != GameEvent::Type::System)
	{
		return;
	}

	const Event& sysEvent = event.info.systemEvent;
	switch (sysEvent)
	{
	case Event::Type::TextEntered:
		{
			if (mouseDown || whatActive == WhatActive::Nothing)
			{
				return;
			}
			const auto charEntered = sysEvent.textEnteredInfo.characterWritten;
			if (validatorFunction && !validatorFunction(charEntered))
			{
				return;
			}

			switch (whatActive)
			{
			case WhatActive::Caret:
				{
					textString.insert(caretHighlightPos++, 1, charEntered);
					timer = 0;
					GuiMutexLock _(guiMutex);
					managedText->setText(textString);
				}
				break;
			case WhatActive::Highlight:
				{
					if (caretHighlightPos >= highlightStartPos)
					{
						textString.replace(highlightStartPos, caretHighlightPos - highlightStartPos, &charEntered, 1);
					}
					else
					{
						textString.replace(caretHighlightPos, highlightStartPos - caretHighlightPos, &charEntered, 1);
					}
					caretHighlightPos = highlightStartPos + 1;
					timer = 0;
					whatActive = WhatActive::Caret;
					GuiMutexLock _(guiMutex);
					managedText->setSelection(0, 0);
					managedText->setText(textString);
				}
				break;
			}
		}
		break;
	case Event::Type::KeyPressed:
		if (mouseDown)
		{
			break;
		}
		switch (whatActive)
		{
		case WhatActive::Caret:
			switch (sysEvent.keyInfo.key)
			{
			case Keyboard::Key::Delete:
				if (caretHighlightPos >= 0 && !textString.empty())
				{
					textString.erase(caretHighlightPos, 1);
					timer = 0;
					GuiMutexLock _(guiMutex);
					managedText->setText(textString);
				}
				break;
			case Keyboard::Key::BackSpace:
				if (caretHighlightPos > 0)
				{
					textString.erase(--caretHighlightPos, 1);
					timer = 0;
					GuiMutexLock _(guiMutex);
					managedText->setText(textString);
				}
				break;
			case Keyboard::Key::Left:
				if (caretHighlightPos > 0)
				{
					--caretHighlightPos;
					timer = 0;
					GuiMutexLock _(guiMutex);
					markForRedraw();
				}
				break;
			case Keyboard::Key::Right:
				if (caretHighlightPos < textString.size())
				{
					++caretHighlightPos;
					timer = 0;
					GuiMutexLock _(guiMutex);
					markForRedraw();
				}
				break;
			case Keyboard::Key::Return:
				if (allowsMultilineInput)
				{
					textString.insert(caretHighlightPos++, 1, U'\n');
					timer = 0;
					GuiMutexLock _(guiMutex);
					managedText->setText(textString);
				}
				break;
			}
			break;
		case WhatActive::Highlight:
			switch (sysEvent.keyInfo.key)
			{
			case Keyboard::Key::Delete:
			case Keyboard::Key::BackSpace:
				if (caretHighlightPos > highlightStartPos)
				{
					textString.erase(highlightStartPos, caretHighlightPos - highlightStartPos);
					caretHighlightPos = highlightStartPos;
					timer = 0;
					whatActive = WhatActive::Caret;
					GuiMutexLock _(guiMutex);
					managedText->setSelection(0, 0);
					managedText->setText(textString);
				}
				break;
			case Keyboard::Key::Left:
				caretHighlightPos = highlightStartPos;
			case Keyboard::Key::Right:
				{
					timer = 0;
					whatActive = WhatActive::Caret;
					GuiMutexLock _(guiMutex);
					managedText->setSelection(0, 0);
				}
				break;
			case Keyboard::Key::Return:
				if (allowsMultilineInput)
				{
					if (caretHighlightPos >= highlightStartPos)
					{
						textString.replace(highlightStartPos, caretHighlightPos - highlightStartPos, U"\n", 1);
					}
					else
					{
						textString.replace(caretHighlightPos, highlightStartPos - caretHighlightPos, U"\n", 1);
					}
					caretHighlightPos = highlightStartPos + 1;
					timer = 0;
					whatActive = WhatActive::Caret;
					GuiMutexLock _(guiMutex);
					managedText->setSelection(0, 0);
					managedText->setText(textString);
				}
				break;
			}
			break;
		default:
			break;
		}
		break;
	case Event::Type::MouseButtonReleased:
		if (sysEvent.mouseButtonInfo.button == Mouse::Button::Left)
		{
			mouseDown = false;
		}
		break;
	case Event::Type::MouseButtonPressed:
		if (!mouseInside)
		{
			if (whatActive != WhatActive::Nothing)
			{
				whatActive = WhatActive::Nothing;
				GuiMutexLock _(guiMutex);
				markForRedraw();
			}
		}
		break;
	default:
		break;
	}
}

void GLOWE::Components::GUIInputField::update()
{
	if (whatActive == WhatActive::Nothing)
	{
		return;
	}

	float newTimer = timer + engine.getDeltaTime().asSeconds();
	if (newTimer > blinkTime)
	{
		newTimer -= blinkTime;
		timer = newTimer;
		GuiMutexLock _(guiMutex);
		scheduleRecalc();
		return;
	}
	
	if (newTimer > (blinkTime / 2.f))
	{
		GuiMutexLock _(guiMutex);
		scheduleRecalc();
	}

	timer = newTimer;
}
