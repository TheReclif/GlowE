#pragma once
#ifndef GLOWE_POINTLIGHTCOMPONENT_INCLUDED
#define GLOWE_POINTLIGHTCOMPONENT_INCLUDED

#include "../GlowEngine/Component.h"

#include "../GlowSystem/Color.h"

#include "../GlowGraphics/Uniform/LightingMgr.h"

namespace GLOWE
{
	namespace Components
	{
		/// @brief Point light component. A point light is a type of light that has a position, range and it attenues over distance. It propagates spherically. It's described with position, range, attenuation vector and importance.
		class GLOWENGINE_EXPORT PointLight
			: public Component, public LightingMgr::LightBase
		{
		private:
			GLOWE::PointLight lightInfo;
			LightingMgr::PointLightHandle pointLightHandle;
			LightingMgr::RegisteredLightHandle lightHandle;
			bool needToUpdate;
		private:
			virtual void updateLight() override;
		public:
			/// @brief Minimal constructor.
			/// @param newParent Component's parent.
			PointLight(GameObject* const newParent);

			GlowSerializeComponent(PointLight, GlowBases(), GlowField(lightInfo));

			/// @brief Sets the light's ambient color.
			/// @param newAmbient New ambient.
			void setAmbient(const Color& newAmbient);
			/// @brief Sets the light's diffuse color.
			/// @param newDiffuse New diffuse.
			void setDiffuse(const Color& newDiffuse);
			/// @brief Sets the light's specular color.
			/// @param newSpecular New specular.
			void setSpecular(const Color& newSpecular);
			/// @brief Sets the attenuation vector. The first component is const attenuation, the second one is linear attenuation and the third one is quadratic attenuation.
			/// @param newAtten Attenuation.
			void setAttenuation(const Float3& newAtten);
			/// @brief Sets the attenuation vector. The first component is const attenuation, the second one is linear attenuation and the third one is quadratic attenuation.
			/// @param newAtten Attenuation.
			void setAttenuation(const Vector3F& newAtten);
			/// @brief Sets the light's range.
			/// @param newRange Range.
			/// @param autoSetAttenuation If true, the attenuation will be set accordingly to the following formula: Linear = 4.5 / LightRange, Quadratic = 75.0 / LightRange ^ 2 (taken from Ogre's website).
			void setRange(const float newRange, const bool autoSetAttenuation = true);
			/// @brief Sets the light importance. When more than 8 lights are present in the object's range, they're sorted using the following formula: distance * (1.0f / (light.importance / 1000.0f)) (the bigger the importance the better for the light). Importance defaults to 1000.
			/// @param newImportance New importance.
			void setImportance(const float newImportance);

			virtual void processEvent(const GameEvent& event) override;

			virtual void onFirstActivate() override {};

			// Actually it makes no sense to disable/enable a Transform.
			virtual void onActivate() override;
			virtual void onDeactivate() override;
		};

		GlowInitComponentFactoryReflection(PointLight);
	}
}

#endif
