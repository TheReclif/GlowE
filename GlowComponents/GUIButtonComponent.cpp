#include "GUIButtonComponent.h"
#include "../GlowEngine/GUI/GUIContainer.h"
#include "../GlowEngine/Engine.h"
#include "../GlowEngine/GameObject.h"

void GLOWE::Components::GUIButton::onGUIEvent(const GameEvent::GUIEvent& event)
{
	using GUIEvent = GameEvent::GUIEvent;

	switch (event.type)
	{
	case GUIEvent::Type::MouseEntered:
		wasClickedInside = false;
		setLastColor();
		mouseState = MouseState::Hovering;
		isDuringLerp = true;
		timer = (lerpTime - timer);
		//setColor(highlightColor);
		if (onEnterCallback)
		{
			onEnterCallback(*this);
		}
		break;
	case GUIEvent::Type::MouseLeft:
		//setColor(baseColor);
		setLastColor();
		isDuringLerp = true;
		timer = (lerpTime - timer);
		mouseState = MouseState::Outside;
		if (onLeaveCallback)
		{
			onLeaveCallback(*this);
		}
		break;
	case GUIEvent::Type::MouseButtonDownInside:
		setLastColor();
		isDuringLerp = true;
		timer = (lerpTime - timer);
		mouseState = MouseState::Clicked;
		wasClickedInside = true;
		//setColor(pressedColor);
		break;
	case GUIEvent::Type::MouseButtonUpInside:
		setLastColor();
		isDuringLerp = true;
		timer = (lerpTime - timer);
		mouseState = MouseState::Hovering;
		//setColor(highlightColor);
		if (wasClickedInside && onClickCallback)
		{
			onClickCallback(*this);
		}
		wasClickedInside = false;
		break;
	}
}

void GLOWE::Components::GUIButton::setColor(const Color& col)
{
	if (setColorFunc)
	{
		setColorFunc(col);
	}
}

void GLOWE::Components::GUIButton::setupTextConstraints()
{
	image->stretchToParent();
	text->stretchToParent(5.0f);
	/*text->addConstraintToThis(GUIConstraint(GUIConstraint::Type::FixedUp, 5, this, GUIConstraint::Edge::LeftUp));
	text->addConstraintToThis(GUIConstraint(GUIConstraint::Type::FixedLeft, 5, this, GUIConstraint::Edge::LeftUp));
	text->addConstraintToThis(GUIConstraint(GUIConstraint::Type::FixedDown, 0, this, GUIConstraint::Edge::RightDown));
	text->addConstraintToThis(GUIConstraint(GUIConstraint::Type::FixedRight, 0, this, GUIConstraint::Edge::RightDown));*/
}

void GLOWE::Components::GUIButton::setLastColor()
{
	switch (mouseState)
	{
	case MouseState::Clicked:
		lastColor = pressedColor;
		break;
	case MouseState::Hovering:
		lastColor = highlightColor;
		break;
	case MouseState::Outside:
		lastColor = baseColor;
		break;
	}
}

bool GLOWE::Components::GUIButton::checkIsActiveInGUI() const
{
	return checkIsActive();
}

GLOWE::Components::GUIButton::GUIButton(GameObject* const newParent)
	: ScriptBehaviour(newParent), mouseState(MouseState::Outside), image(nullptr), text(nullptr), baseColor(Color::white), highlightColor(Color::grey), pressedColor(Color::from255RGBA(104, 104, 104)), lerpTime(Time::fromSeconds(0.3f)), timer(lerpTime), wasClickedInside(false), isDuringLerp(false), setColorFunc([this](const Color& col)
{
	image->setColor(col);
}), onClickCallback(), onEnterCallback(), onLeaveCallback()
{
	autoFindGUIContainer();
	image = &gameObject->addComponent<GUIImage>();
	text = &gameObject->addComponent<GUITextField>();
	image->setParent(this);
	text->setParent(this);

	const int texPos = image->getMaterial().getTexturePosition("Diffuse Texture");
	if (texPos >= 0)
	{
		image->getMaterial().setTexture(texPos, getInstance<ResourceMgr>().load<TextureImpl>("GlowCore/Textures/PlainColorTexture.gtex", ""));
	}
	setupTextConstraints();

	setColor(baseColor);
}

GLOWE::Components::GUIButton::~GUIButton()
{
	gameObject->deleteComponent(text);
	gameObject->deleteComponent(image);
}

void GLOWE::Components::GUIButton::autoFindGUIContainer()
{
	autoFindContainer(gameObject);
}

void GLOWE::Components::GUIButton::setBaseColor(const Color& col)
{
	baseColor = col;
	if (mouseState == MouseState::Outside)
	{
		setColor(col);
	}
}

void GLOWE::Components::GUIButton::setHighlightColor(const Color& col)
{
	highlightColor = col;
	if (mouseState == MouseState::Hovering)
	{
		setColor(col);
	}
}

void GLOWE::Components::GUIButton::setPressedColor(const Color& col)
{
	pressedColor = col;
	if (mouseState == MouseState::Clicked)
	{
		setColor(col);
	}
}

void GLOWE::Components::GUIButton::setLerpingTime(const Time& newTime)
{
	lerpTime = newTime;
}

GLOWE::Components::GUIImage* GLOWE::Components::GUIButton::getBackgroundImage() const
{
	return image;
}

GLOWE::Components::GUITextField* GLOWE::Components::GUIButton::getTextField() const
{
	return text;
}

GLOWE::String GLOWE::Components::GUIButton::getText() const
{
	if (text)
	{
		return text->getText();
	}

	return String();
}

void GLOWE::Components::GUIButton::setText(const String& newText)
{
	if (text)
	{
		text->setText(newText);
	}
}

void GLOWE::Components::GUIButton::setFontSize(const unsigned int newSize)
{
	if (text)
	{
		text->setFontSize(newSize);
	}
}

void GLOWE::Components::GUIButton::onFirstActivate()
{
	autoFindContainer(gameObject);
}

void GLOWE::Components::GUIButton::onScriptDeactivated()
{
	timer = lerpTime;
	mouseState = MouseState::Outside;
	GuiMutexLock lock(guiMutex);
	setColor(baseColor);
}

void GLOWE::Components::GUIButton::processGUIEvent(const GameEvent::GUIEvent& event)
{
	// The comment below is out of date, but I will keep it if it does cause glitches.
	// Delete this and you'll get beautiful GUI graphical glitches.
	//if (checkIsActive())
	GuiMutexLock lock(guiMutex);
	onGUIEvent(event);
}

void GLOWE::Components::GUIButton::update()
{
	if (!isDuringLerp)
	{
		return;
	}
	timer += engine.getDeltaTime();
	const float interpolationT = clamp(timer.asSeconds() / lerpTime.asSeconds(), 0.0f, 1.0f);
	if (timer >= lerpTime)
	{
		isDuringLerp = false;
	}

	Color finalColor;
	switch (mouseState)
	{
	case MouseState::Outside:
		finalColor = MathHelper::lerp(lastColor, baseColor, interpolationT);
		break;
	case MouseState::Clicked:
		finalColor = MathHelper::lerp(lastColor, pressedColor, interpolationT);
		break;
	case MouseState::Hovering:
		finalColor = MathHelper::lerp(lastColor, highlightColor, interpolationT);
		break;
	}

	GuiMutexLock lock(guiMutex);
	setColor(finalColor);
}

void GLOWE::Components::GUIButton::setOnClickCallback(const std::function<void(GUIButton&)>& callback)
{
	onClickCallback = callback;
}

void GLOWE::Components::GUIButton::setOnEnterCallback(const std::function<void(GUIButton&)>& callback)
{
	onEnterCallback = callback;
}

void GLOWE::Components::GUIButton::setOnLeaveCallback(const std::function<void(GUIButton&)>& callback)
{
	onLeaveCallback = callback;
}

void GLOWE::Components::GUIButton::setCustomSetColorFunction(const std::function<void(const Color&)>& colorFunc)
{
	setColorFunc = colorFunc;
}
