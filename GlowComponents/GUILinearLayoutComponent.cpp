#include "GUILinearLayoutComponent.h"

GLOWE::Components::GUILinearLayout::GUILinearLayout(GameObject* const newParent)
	: Component(newParent), GLOWE::GUILinearLayout()
{
}

void GLOWE::Components::GUILinearLayout::onFirstActivate()
{
	autoFindContainer(gameObject);
}

bool GLOWE::Components::GUILinearLayout::checkIsActiveInGUI() const
{
	return checkIsActive();
}
