#pragma once
#ifndef GLOWE_COMPONENTS_CAMERACOMPONENT_INCLUDED
#define GLOWE_COMPONENTS_CAMERACOMPONENT_INCLUDED

#include "../GlowEngine/Component.h"
#include "../GlowEngine/RenderingEngine.h"

#include "../GlowMath/CameraMath.h"
#include "../GlowMath/Frustum.h"

#include "../GlowGraphics/Uniform/RenderTargetImplementation.h"

namespace GLOWE
{
	namespace Components
	{
		class Camera;
	}

	/// @brief Class for managing cameras and their render pass handles.
	class GLOWENGINE_EXPORT CameraMgr
		: public Subsystem
	{
	private:
		List<Pair<Components::Camera*, RenderingEngine::RenderPassHandle>> cameras;
		Components::Camera* mainCamera;
	private:
		static RenderingEngine::RenderPassHandle registerPass(Components::Camera* camera);
	public:
		using CameraHandle = List<Pair<Components::Camera*, RenderingEngine::RenderPassHandle>>::iterator;
	public:
		/// @brief Register a camera.
		/// @param camera A pointer to the camera to be registered.
		/// @return Registered internal camera handle.
		CameraHandle registerCamera(Components::Camera* camera);
		/// @brief Unregisters camera.
		/// @param camera Camera handle to unregister.
		void unregisterCamera(const CameraHandle& camera);
		/// @brief Causes the camera's registered render pass to be unregistered and registered again.
		/// @param camera Camera to "reregister".
		void resetCameraRenderPass(const CameraHandle& camera);

		/// @brief Returns the current main camera (usually the first registered).
		/// @return A pointer to the current camera.
		Components::Camera* getMainCamera() const;
		/// @brief Sets the current main camera.
		/// @param newMainCamera New main camera pointer.
		void setMainCamera(Components::Camera* newMainCamera);
	};

	namespace Components
	{
		/// @brief A rendering camera component. It registers its own rendering pass and has its own projection matrix. Can use owned render target or the one passed by the user.
		class GLOWENGINE_EXPORT Camera
			: public Component
		{
		public:
			/// @brief Tells the camera to use the default render target (screen display for PC, consoles, etc.).
			struct CreateDefaultTag {};

			/// @brief Type of projection matrix. Other means that the user passed their own matrix.
			enum class ProjectionType
			{
				Other = 0,
				Orthographic,
				Perspective
			};

			/// @brief Type of clear to use. May be combined by using bitwise OR and readed by using bitwise AND.
			struct ClearType
			{
				/// @brief No clearing.
				static constexpr unsigned char None = 0;
				/// @brief Clears the depth stencil view.
				static constexpr unsigned char DepthStencil = 1 << 0;
				/// @brief Use the color to clear the render target.
				static constexpr unsigned char Color = 1 << 1;
				/// @brief Use the skybox to clear the render target.
				static constexpr unsigned char Skybox = 1 << 2;

				ClearType() = delete;
			};
		private:
			CameraMath cameraMath;
			Frustum frustum;
			bool needToUpdateFrustum, needToUpdateView, isOwningRenderTarget;
			UniquePtr<RenderTargetImpl> renderTarget; // May be owned or not.
			CameraMgr::CameraHandle cameraHandle;
			Vector<SharedPtr<TextureImpl>> targets;
			SharedPtr<TextureImpl> dsv;

			// Serialization variables.
			unsigned char clearType;
			Color clearColor;
			ProjectionType projType;
			float fov, orthoScale; // Used only in deserialization.
			Viewport viewport;
			bool isDefault;
			Vector<TextureDesc> renderTargetDescs;
			TextureDesc dsvDesc;
			int engineRenderPassId;
			Vector<unsigned int> notRendered;
		private:
			void create(const Vector<TextureDesc>& targetsDescs, const TextureDesc& dsvDesc);
			void destroy();
			void setupDefaultProjection();
		public:
			/// @brief The minimal constructor.
			/// @param newParent Component's parent.
			Camera(GameObject* const newParent);

			virtual ~Camera();

			GlowSerializeComponent(Camera, GlowBases(), GlowField(clearType), GlowField(clearColor), GlowField(projType), GlowField(fov), GlowField(viewport), GlowField(isDefault), GlowField(dsvDesc), GlowField(engineRenderPassId), GlowField(notRendered), GlowField(renderTargetDescs), GlowField(orthoScale));

			virtual void processEvent(const GameEvent& event) override;

			virtual void onFirstActivate() override {};

			virtual void onActivate() override;
			virtual void onDeactivate() override;

			virtual void onAfterDeserialize() override;

			/// @brief Sets the clear color. May not work if the clear type's Color field is not set.
			/// @param col New clear color as DX format (in range [0, 1]).
			void setClearColor(const Color& col);
			/// @brief Sets the clear type.
			/// @param newClearType A bit combination of ClearType values being a new clear type mask.
			void setClearType(const unsigned char newClearType);

			/// @brief Clears the render target according to the set clear type.
			/// @param renderer Graphics renderer.
			void clearRenderTarget(BasicRendererImpl& renderer);

			/// @brief Sets the projection matrix to perspective with given parameters.
			/// @param width Width of the viewport.
			/// @param height Height of the viewport.
			/// @param farF Frustum far plane position. Any fragment/pixel with depth greater than that will be discarded during the Z test.
			/// @param nearF Frustum near plane position. Any fragment/pixel with depth less than that will be discarded during the Z test.
			void setPerspective(const float width, const float height, const float farF = 1000.0f, const float nearF = 0.001f);
			/// @brief Sets the projection matrix to perspective with given parameters. Uses field of view angle to set up the projection.
			/// @param fov Horizontal field of view angle, in radians.
			/// @param farF Frustum far plane position. Any fragment/pixel with depth greater than that will be discarded during the Z test.
			/// @param nearF Frustum near plane position. Any fragment/pixel with depth less than that will be discarded during the Z test.
			/// @param aspectRatio Viewport aspect ratio (width/height).
			void setPerspectiveFov(const float fov, const float farF = 1000.0f, const float nearF = 0.001f, const float aspectRatio = 0.0f);

			/// @brief Sets up a default orthographic projection. The width and height are taken from the render target. TODO: Add optional parameters farF and nearF.
			void setOrthographic();
			/// @brief Sets the projection matrix to orthographic with given parameters.
			/// @param width Width of the viewport.
			/// @param height Height of the viewport.
			/// @param farF Frustum far plane position. Any fragment/pixel with depth greater than that will be discarded during the Z test.
			/// @param nearF Frustum near plane position. Any fragment/pixel with depth less than that will be discarded during the Z test.
			void setOrthographic(const float width, const float height, const float farF = 1000.0f, const float nearF = 0.001f);
			void setOrthographicOffScreen(const float left, const float right, const float top, const float bottom, const float farF = 1000.0f, const float nearF = 0.001f);

			/// @brief Sets a custom, user-provided projection matrix.
			/// @param newMat New projection matrix.
			void setCustomProjectionMatrix(const Float4x4& newMat);

			/// @brief Sets the list of not rendered renderables.
			/// <remarks>
			/// Will call onDeactivate and onActivate to update its render pass. Use sparingly.
			/// </remarks>
			/// @param whatNotToRender New not rendered list.
			void setNotRendered(const Vector<unsigned int>& whatNotToRender);
			/// @brief Sets the render pass order.
			/// <remarks>
			/// Will call onDeactivate and onActivate to update its render pass. Use sparingly.
			/// </remarks>
			/// @param newId New render pass order.
			void setRenderPassId(const int newId);
			/// @brief Sets the viewport.
			/// @param newVp New viewport.
			void setViewport(const Viewport& newVp);

			/// @brief Updates the view matrix using the transformation matrix from the GameObject's Transform component.
			void updateViewFromTransform();

			/// @brief Resize the render target reusing its texture descs (if owned).
			/// @param newDimensions New texture dimensions (width, height, depth). Use 1 for unused dimensions.
			void resize(const UInt3& newDimensions);
			/// @brief Recreates the render target using provided texture descs (if owned).
			/// @param targetsDescs Render target texture descs.
			/// @param dsvDesc Depth stencil texture desc.
			void recreateRenderTarget(const Vector<TextureDesc>& targetsDescs, const TextureDesc& dsvDesc);

			/// @brief Returns the projection type.
			/// @return Projection type.
			ProjectionType getProjectionType() const;
			/// @brief If any changes were made to the projection or view matrix, the frustrum is updated before returning.
			/// @return Frustum (updated if necessary).
			const Frustum& getFrustum(); // Cannot be const because of the frustum update.
			/// @brief Returns the render target's size.
			/// @return Render target's size.
			Float2 getSize() const;
			/// @brief Returns the aspect ratio of the render target.
			/// @return Aspect ratio of the render target.
			float getAspectRatio() const;
			/// @brief If any changes were made to the view matrix, it's updated before returning.
			/// @return View matrix (updated if necessary).
			Matrix4x4 getCameraViewMatrix();
			/// @brief Returns the projection matrix.
			/// @return Projection matrix.
			Matrix4x4 getCameraProjMatrix() const;
			/// @brief Returns the list of not rendered renderable types.
			/// @return List of not rendered renderables
			Vector<unsigned int> getNotRendered() const;
			/// @brief Returns the render pass order.
			/// @return Render pass order.
			int getRenderPassId() const;
			/// @brief Returns a pointer to the render target.
			/// @return Pointer to the render target.
			RenderTargetImpl* getRenderTarget() const;
			/// @brief Returns the viewport.
			/// @return Viewport.
			const Viewport& getViewport() const;
			/// @brief Returns the depth stencil view.
			/// @return Depth stencil view.
			const SharedPtr<TextureImpl>& getDsv() const;
			/// @brief Returns a reference to target textures.
			/// @return Reference to target textures. Will be empty if the render target is not owned by the camera.
			const Vector<SharedPtr<TextureImpl>>& getTargets() const;

			/// @brief Checks whether the camera is owning its render target.
			/// @return true if owns, false otherwise.
			bool checkIsOwningRenderTarget() const;
		};

		GlowInitComponentFactoryReflection(Camera);
	}
}

#endif
