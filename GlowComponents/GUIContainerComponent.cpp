#include "GUIContainerComponent.h"

GLOWE::Components::GUIContainer::GUIContainer(GameObject* const newParent)
	: Component(newParent), GLOWE::GUIContainer()
{
}

void GLOWE::Components::GUIContainer::onFirstActivate()
{
	autoFindContainer(gameObject);
}

void GLOWE::Components::GUIContainer::onActivate()
{
	GuiMutexLock guard(guiMutex);
	scheduleRecalc();
}

void GLOWE::Components::GUIContainer::onDeactivate()
{
	GuiMutexLock guard(guiMutex);
	scheduleRecalc();
}

bool GLOWE::Components::GUIContainer::checkIsActiveInGUI() const
{
	return checkIsActive();
}
