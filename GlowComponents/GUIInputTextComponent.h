#pragma once
#ifndef GLOWE_GUIINPUTFIELDCOMPONENT_INCLUDED
#define GLOWE_GUIINPUTFIELDCOMPONENT_INCLUDED

#include "ScriptBehaviour.h"

#include "../GlowEngine/GUI/GUIElement.h"
#include "../GlowEngine/Material.h"

namespace GLOWE
{
	namespace Components
	{
		class GUITextField;

		class GLOWENGINE_EXPORT GUIInputField
			: public ScriptBehaviour, public GUIElement
		{
		private:
			enum class WhatActive
			{
				Nothing,
				Caret,
				Highlight
			};
		private:
			GUITextField* managedText = nullptr;
			WhatActive whatActive = WhatActive::Nothing;
			bool mouseDown = false, mouseInside = false, allowsMultilineInput = false;
			float caretWidth = 1.5f, timer = 0, blinkTime = 1.f;
			unsigned int caretHighlightPos = 0, highlightStartPos = 0;
			int materialCaretCBuffer = -1, caretColorPos = -1;
			Material material;
			Utf32String textString;
			ResourceHandle<Geometry> guiPlane;
			std::function<bool(Char32)> validatorFunction;
		private:
			void updateCaretHighlight(Utf32String&& newString);
			unsigned int worldPosToTextPos(const Vector2F& worldPos) const;
		protected:
			virtual void renderImpl(BasicRendererImpl& renderer, MaterialEnvironment& matEnv) override;
		public:
			GUIInputField(GameObject* const newParent);

			void setValidator(const std::function<bool(Char32)>& validatorFunc);
			void setCaretColor(const Color& newColor);
			void setHighlightColor(const Color& newColor);
			void setCaretWidth(const float width);

			void setManagedText(GUITextField* const text);
			void setMultilineInputAllowed(const bool multilineAllowed);

			virtual void processGUIEvent(const GameEvent::GUIEvent& event) override;
			virtual void processEvent(const GameEvent& event) override;

			virtual void update() override;

			GlowSerializeScript(GUIInputField, GlowBases());
		};
	}
}

GlowInitScriptReflection(GUIInputField);

#endif
