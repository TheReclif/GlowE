#pragma once
#ifndef GLOWE_GUIBASECOMPONENT_INCLUDED
#define GLOWE_GUIBASECOMPONENT_INCLUDED

#include "../GlowEngine/GUI/GUIElement.h"
#include "../GlowEngine/Component.h"

namespace GLOWE
{
	namespace Components
	{
		/// @brief Bases class for GUI components. Properly propagates GUI events to the game object.
		class GLOWENGINE_EXPORT GUIBase
			: public Component, public GUIElement
		{
		public:
			/// @brief The minimal constructor.
			/// @param newParent Component's parent.
			GUIBase(GameObject* const newParent);

			virtual void processGUIEvent(const GameEvent::GUIEvent& event) override;

			virtual void onFirstActivate() override;
			virtual void onActivate() override;
			virtual void onDeactivate() override;

			virtual bool checkIsActiveInGUI() const override;
		};
	}
}

#endif
