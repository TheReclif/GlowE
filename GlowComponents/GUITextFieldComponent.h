#pragma once
#ifndef GLOWE_COMPONENTS_TEXTFIELD_INCLUDED
#define GLOWE_COMPONENTS_TEXTFIELD_INCLUDED

#include "GUIBaseComponent.h"

#include "../GlowGraphics/Uniform/Geometry.h"

#include "../GlowEngine/Material.h"
#include "../GlowEngine/GUI/Text.h"

namespace GLOWE
{
	namespace Components
	{
		/// @brief GUI text field. Displays text. It does not handle events such as keyboard input.
		class GLOWENGINE_EXPORT GUITextField
			: public GUIBase
		{
		public:
			enum class SelectionDirection
			{
				None,
				Forward,
				Backward
			};

			enum class TextHorizontalAlignment
			{
				Left,
				Middle,
				Right,
				Justified
			};

			enum class TextVerticalAlignment
			{
				Up,
				Middle,
				Down
			};
		private:
			struct TextGlyph
			{
				const Glyph* glyph = nullptr;
				String::Utf32Char character = 0;
				int memBuffPos = -1;
				Float2 pos = { 0.0f, 0.0f };
				unsigned int cluster;
			};

			struct InstanceData
			{
				Float2 pos = { 0.0f, 0.0f }, size = { 0.0f, 0.0f }, atlasPos = { 0.0f, 0.0f };
			};
		private:
			ResourceHandle<Geometry> guiPlane;
			ComplexGeometryBuffers renderedBuffers;
			UniquePtr<BufferImpl> instanceBuffer;
			ResourceHandle<Font> usedFont;
			Material usedMaterial, selectionMaterial;
			Vector<TextGlyph> textGlyphs;
			Vector<InstanceData> memBuffer;
			Vector<unsigned int> lineBreaks;
			String text, language = "en";
			TextHorizontalAlignment horizontalAlignment = TextHorizontalAlignment::Left;
			TextVerticalAlignment verticalAlignment = TextVerticalAlignment::Up;
			bool isTextDirty = false, processSelection = false, useSimpleTabs = false;
			unsigned int fontSize, texturePos, bufferCapacity, selectionBegin = 0, selectionEnd = 0;
			int materialSelectionCBuffer = -1, selectionColorPos = -1;
			float tabulationStopDistance = 4;
			Font::Metrics fontMetrics;
			std::function<void(const Color&)> setColorFunc;
			std::function<void(const GUITextField&)> onGlyphsChangedCallback;
		private:
			void resizeBuffer(const unsigned int newElementsCount, const void* data);
			void drawLine(const unsigned int textBegin, const unsigned int textEnd, const unsigned int textLine, BasicRendererImpl& renderer, MaterialEnvironment& matEnv);
			virtual void onRecalcDone() override;
		protected:
			void regenerateGlyphs(BasicRendererImpl& renderer);

			virtual void renderImpl(BasicRendererImpl& renderer, MaterialEnvironment& matEnv) override;
		public:
			/// @brief Minimal constructor.
			/// @param newParent Component's parent.
			GUITextField(GameObject* const newParent);
			/// @brief The minimal constructor. Also, automatically finds a suitable GUI container.
			/// @param newParent Component's parent.
			GUITextField(GameObject* const newParent, const AutoFindParentOnStart&);

			GlowSerializeComponent(GUITextField, GlowBases(), GlowField(text), GlowField(fontSize), GlowField(texturePos), GlowField(bufferCapacity));

			/// @brief Sets the font size.
			/// @param newFontSize Font size.
			void setFontSize(const unsigned int newFontSize);
			/// @brief Sets the text string.
			/// @param newText Text.
			void setText(const String& newText);
			/// @brief Sets the language used to shape the rendered text.
			/// @param bcp47Language BCP 47 language tag, e.g. "en" for English, "pl" for Polish.
			void setLanguage(const String& bcp47Language);
			/// @brief Sets the usage of simple tabs. Simple tabs are just multiple spaces, while advanced tabs move the pen to the next tab stop.
			/// @param useSimpleTab True for simple tabs, false for complex tabs.
			void setUseSimpleTab(const bool useSimpleTab);
			/// @brief Sets the tab stop distance. For simple tabs it's the amount of spaces for a tab. For complex tabs it's the distance between tab stops.
			/// @param newDistance New tabs top.
			void setTabulationStopDistance(const float newDistance);
			/// @brief Sets the text alignment method.
			/// @param alignment New text alignment.
			void setHorizontalAlignment(const TextHorizontalAlignment alignment);
			/// @brief Sets the text alignment method.
			/// @param alignment New text alignment.
			void setVerticalAlignment(const TextVerticalAlignment alignment);

			/// @brief Sets the text's color.
			/// @param newColor Text color.
			/// @return true if it was able to change the color, false otherwise.
			bool setColor(const Color& newColor);
			void setSelectionColor(const Color& newColor);
			/// @brief Sets the set color function. Call without any arguments to auto find "Color" field in "Material" constant buffer (may not work with custom shaders without this field).
			/// @param newFunc New set color function. If it equals nullptr, the function is determined automatically by finding the Color field. Defaults to nullptr.
			void updateSetColorFunc(const std::function<void(const Color&)>& newFunc = nullptr); // May be null to auto detect Color field in Material constant buffer.

			void setSelection(const unsigned int selectionBegin, const unsigned int selectionEnd);
			void setCanBeSelected(const bool canBeSelected);
			
			/// @brief Returns the font metrics.
			/// @return Font metrics.
			const Font::Metrics& getFontMetrics() const;
			/// @brief Returns the font size.
			/// @return Font size.
			unsigned int getFontSize() const;
			/// @brief Returns the text string.
			/// @return Text.
			String getText() const;
			/// @brief Returns the used material.
			/// @return Material.
			Material& getMaterial();
			/// @brief Returns current glyph info vector.
			/// @return Glyphs info.
			const Vector<TextGlyph>& getGlyphs() const;
			/// @brief Returns the position in the glyph array of the first character of the given line or throws an exception if there is no such line.
			/// @param whichLine Line number from 0 to number of lines exclusively.
			/// @return First character position of the line in the glyph array
			unsigned int getLinePosition(const unsigned int whichLine) const;
			/// @brief Returns the lines count (a line must contain at least one character after its line break).
			/// @return Lines count
			unsigned int getLineCount() const;
			/// @brief Calculates which glyph corresponds to the given position. Can also calculate caret position by changing the second parameter. Complexity O(n), but n is the amount of glyphs in one line.
			/// @param pos Position relative to the text field
			/// @param caretPos Optional parameter telling the method which half-part of the glyph should be unaccounted for that glyph when performing intersection testing
			/// @return Glyph pos or -1 if the pos is above/before or getGlyphs().size()+1 if the pos is below/after all of the glyphs
			int localPosToGlyphPos(const Vector2F pos, const SelectionDirection caretPos = SelectionDirection::None) const;
			/// @brief Calculates the line position of the character at the given position
			/// @param textPos Character position
			/// @return Line position or -1 if the character position is out of bounds
			int textPosToLinePos(const unsigned int textPos) const;

			void setOnGlyphsChangedCallback(const std::function<void(const GUITextField&)>& callback);

			virtual void processGUIEvent(const GameEvent::GUIEvent& event) override {};
			virtual void processEvent(const GameEvent&) override {};

			virtual void onActivate() override {};
			virtual void onDeactivate() override {};
		};

		GlowInitComponentFactoryReflection(GUITextField);
	}
}

#endif
