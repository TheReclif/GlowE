#include "TransformComponent.h"
#include "../GlowEngine/GameObject.h"

GLOWE::Components::Transform::Transform(GameObject* const newParent)
	: Component(newParent), transform(), composedMatrix(Matrix4x4::getIdentity()), isDirty(false)
{
}

GLOWE::Map<GLOWE::String, GLOWE::UniquePtr<GLOWE::BaseAccessor>> GLOWE::Components::Transform::getAccessors()
{
	Map<String, UniquePtr<BaseAccessor>> result;

	result.emplace(u8"rotation", makeUniquePtr<ArrayOfSimplesAccessor<float>>([]() -> UInt64 { return 3; }, nullptr,
		[this]() -> Vector<float>
	{
		const Vector3F eulerAngles = Quaternion(transform.getRotation()).asEulerAngles();
		return { eulerAngles[0], eulerAngles[1], eulerAngles[2] };
	},
		[this](const UInt64 elemId) -> float
	{
		return Quaternion(transform.getRotation()).asEulerAngles()[elemId];
	},
		[this](const float* const arr, const UInt64 arrSize)
	{
		Float3 rot;
		for (unsigned int x = 0; x < std::min(static_cast<UInt64>(3), arrSize); ++x)
		{
			rot[x] = arr[x];
		}
		setRotation(rot);
	},
		[this](const float& elem, const UInt64 whichElem)
	{
		if (whichElem < 3)
		{
			Vector3F eulerAngles = Quaternion(transform.getRotation()).asEulerAngles();
			eulerAngles[whichElem] = elem;
			setRotation(eulerAngles.toFloat3());
		}
	},
		[this]() -> Hash
	{
		const Quaternion quat = transform.getRotation();
		return Hash(reinterpret_cast<const char* const>(&quat), sizeof(Quaternion));
	}));
	result.emplace(u8"translation", makeUniquePtr<ArrayOfSimplesAccessor<float>>([]() -> UInt64 { return 3; }, nullptr,
		[this]() -> Vector<float>
	{
		const Vector3F transl = transform.getTranslation();
		return { transl[0], transl[1], transl[2] };
	},
		[this](const UInt64 elemId) -> float
	{
		return transform.getTranslation()[elemId];
	},
		[this](const float* const arr, const UInt64 arrSize)
	{
		Float3 transl;
		for (unsigned int x = 0; x < std::min(static_cast<UInt64>(3), arrSize); ++x)
		{
			transl[x] = arr[x];
		}
		setTranslation(transl);
	},
		[this](const float& elem, const UInt64 whichElem)
	{
		if (whichElem < 3)
		{
			Vector3F transl = transform.getTranslation();
			transl[whichElem] = elem;
			setTranslation(transl);
		}
	},
		[this]() -> Hash
	{
		const Float3 vec = transform.getTranslation();
		return Hash(reinterpret_cast<const char* const>(&vec), sizeof(Float3));
	}));
	result.emplace(u8"scale", makeUniquePtr<ArrayOfSimplesAccessor<float>>([]() -> UInt64 { return 3; }, nullptr,
		[this]() -> Vector<float>
	{
		const Vector3F scal = transform.getScale();
		return { scal[0], scal[1], scal[2] };
	},
		[this](const UInt64 elemId) -> float
	{
		return transform.getScale()[elemId];
	},
		[this](const float* const arr, const UInt64 arrSize)
	{
		Float3 scal;
		for (unsigned int x = 0; x < std::min(static_cast<UInt64>(3), arrSize); ++x)
		{
			scal[x] = arr[x];
		}
		setScale(scal);
	},
		[this](const float& elem, const UInt64 whichElem)
	{
		if (whichElem < 3)
		{
			Vector3F scal = transform.getScale();
			scal[whichElem] = elem;
			setScale(scal);
		}
	},
		[this]() -> Hash
	{
		const Float3 vec = transform.getScale();
		return Hash(reinterpret_cast<const char* const>(&vec), sizeof(Float3));
	}));

	return result;
}

void GLOWE::Components::Transform::setScale(Vec3 newScale)
{
	isDirty = true;
	transform.setScale(newScale);
}

void GLOWE::Components::Transform::setScale(const Float3& newScale)
{
	isDirty = true;
	transform.setScale(newScale);
}

void GLOWE::Components::Transform::setTranslation(Vec3 newTranslation)
{
	isDirty = true;
	transform.setTranslation(newTranslation);
}

void GLOWE::Components::Transform::setTranslation(const Float3& newTranslation)
{
	isDirty = true;
	transform.setTranslation(newTranslation);
}

void GLOWE::Components::Transform::setRotation(Quat newRotation)
{
	isDirty = true;
	transform.setRotation(newRotation);
}

void GLOWE::Components::Transform::setRotation(RotMat newRotation)
{
	isDirty = true;
	transform.setRotation(newRotation);
}

void GLOWE::Components::Transform::setRotation(const Float3& angles)
{
	isDirty = true;
	transform.setRotation(angles);
}

void GLOWE::Components::Transform::setRotation(const Float4& newRotation)
{
	isDirty = true;
	transform.setRotation(newRotation);
}

void GLOWE::Components::Transform::scale(Vec3 arg)
{
	isDirty = true;
	transform.scale(arg);
}

void GLOWE::Components::Transform::scale(const Float3& arg)
{
	isDirty = true;
	transform.scale(arg);
}

void GLOWE::Components::Transform::translate(Vec3 arg)
{
	isDirty = true;
	transform.translate(arg);
}

void GLOWE::Components::Transform::translate(const Float3& arg)
{
	isDirty = true;
	transform.translate(arg);
}

void GLOWE::Components::Transform::rotate(Quat newRotation)
{
	isDirty = true;
	transform.rotate(newRotation);
}

void GLOWE::Components::Transform::rotate(RotMat newRotation)
{
	isDirty = true;
	transform.rotate(newRotation);
}

void GLOWE::Components::Transform::rotate(const Float3& angles)
{
	isDirty = true;
	transform.rotate(angles);
}

void GLOWE::Components::Transform::rotate(const Float4& newRotation)
{
	isDirty = true;
	transform.rotate(newRotation);
}

GLOWE::Float3 GLOWE::Components::Transform::getScale() const
{
	return transform.getScale();
}

GLOWE::Float3 GLOWE::Components::Transform::getTranslation() const
{
	return transform.getTranslation();
}

GLOWE::Float4 GLOWE::Components::Transform::getRotation() const
{
	return transform.getRotation();
}

GLOWE::Float3 GLOWE::Components::Transform::getWorldScale()
{
	updateMatrix();
	return worldScale;
}

GLOWE::Float3 GLOWE::Components::Transform::getWorldTranslation()
{
	updateMatrix();
	return worldTranslation;
}

GLOWE::Float4 GLOWE::Components::Transform::getWorldRotation()
{
	updateMatrix();
	return worldRotation;
}

GLOWE::Float3 GLOWE::Components::Transform::getForward()
{
	return Quaternion(getWorldRotation()).transformPoint(VecHelpers::forward).toFloat3();
}

GLOWE::Float3 GLOWE::Components::Transform::getRight()
{
	return Quaternion(getWorldRotation()).transformPoint(VecHelpers::right).toFloat3();
}

GLOWE::Float3 GLOWE::Components::Transform::getUp()
{
	return Quaternion(getWorldRotation()).transformPoint(VecHelpers::up).toFloat3();
}

GLOWE::Float4x4 GLOWE::Components::Transform::getMatrix()
{
	updateMatrix();

	return composedMatrix;
}

void GLOWE::Components::Transform::updateMatrix()
{
	if (checkIsDirty())
	{
		isDirty = false;
		Matrix4x4 temp = transform.calculateMatrix();

		Vector3F tempTranslation, tempScale(1.0f, 1.0f, 1.0f);
		Quaternion tempRot(Quaternion::getIdentity());

		if (gameObject->parent)
		{
			auto parentTransform = gameObject->parent->getTransform();
			temp *= parentTransform->getMatrix();

			tempTranslation = Vector3F(parentTransform->getWorldTranslation());
			tempScale = Vector3F(parentTransform->getWorldScale());
			tempRot = parentTransform->getWorldRotation();
		}

		composedMatrix = temp;
		worldTranslation = (tempTranslation + transform.getTranslation()).toFloat3();
		worldScale = (tempScale * transform.getScale()).toFloat3();
		worldRotation = (tempRot * Quaternion(transform.getRotation()).normalize()).toFloat4();

		gameObject->sendEventToChildrenRecursive(GameEvent(this), this);
	}
}

const::GLOWE::Transform& GLOWE::Components::Transform::getTransform() const
{
	return transform;
}

bool GLOWE::Components::Transform::checkIsDirty() const
{
	return isDirty || (gameObject->parent ? gameObject->parent->getTransform()->checkIsDirty() : false);
}

void GLOWE::Components::Transform::processEvent(const GameEvent& event)
{
	if (event == GameEvent::Type::TransformUpdated && event.info.transform != this) // Ignore if sent by me.
	{
		isDirty = true;
	}
}

void GLOWE::Components::Transform::onFirstActivate()
{
	isDirty = true;
}
