#include "CameraComponent.h"
#include "TransformComponent.h"

#include "../GlowGraphics/Uniform/GraphicsFactory.h"

#include "../GlowEngine/GameObject.h"
#include "../GlowEngine/Engine.h"

void GLOWE::Components::Camera::create(const Vector<TextureDesc>& targetsDescs, const TextureDesc& dsvDesc)
{
	destroy();

	const GraphicsDeviceImpl& device = getInstance<Engine>().getGraphicsContext();

	renderTarget = createGraphicsObject<RenderTargetImpl>();
	
	targets.reserve(targetsDescs.size());

	UniquePtr<TextureImpl> tex;
	for (const auto& desc : targetsDescs)
	{
		tex = createGraphicsObject<TextureImpl>();
		tex->create(desc, device);
		targets.emplace_back(std::move(tex));
	}

	tex = createGraphicsObject<TextureImpl>();
	tex->create(dsvDesc, device);
	dsv = std::move(tex);

	renderTarget->create(targets, dsv, device);

	Viewport vp;
	vp.topX = vp.topY = 0.0f;
	vp.minDepth = 0.0f;
	vp.maxDepth = 1.0f;
	vp.width = renderTarget->getSize()[0];
	vp.height = renderTarget->getSize()[1];
	setViewport(vp);
}

void GLOWE::Components::Camera::destroy()
{
	if (!isOwningRenderTarget && renderTarget)
	{
		// We don't want to deallocate a not-owned render target.
		renderTarget.release();
	}
}

void GLOWE::Components::Camera::setupDefaultProjection()
{
	setPerspectiveFov(MathHelper::convertToRadians(80.0f));
}

GLOWE::Components::Camera::Camera(GameObject* const newParent)
	: Component(newParent), cameraMath(), frustum(), needToUpdateFrustum(false), needToUpdateView(false), isOwningRenderTarget(false), renderTarget(), cameraHandle(), notRendered(), engineRenderPassId(0), targets(), dsv(), clearColor(Color(0.0f, 135.0f, 135.0f, 255.0f)), clearType(ClearType::Color), projType(ProjectionType::Perspective), fov(80.0f), orthoScale(1.0f)
{
}

GLOWE::Components::Camera::~Camera()
{
	destroy();
}

void GLOWE::Components::Camera::processEvent(const GameEvent& event)
{
	if (event == GameEvent::Type::TransformUpdated)
	{
		needToUpdateFrustum = needToUpdateView = true;
	}
}

void GLOWE::Components::Camera::onActivate()
{
	cameraHandle = getInstance<CameraMgr>().registerCamera(this);
}

void GLOWE::Components::Camera::onDeactivate()
{
	getInstance<CameraMgr>().unregisterCamera(cameraHandle);
	cameraHandle = CameraMgr::CameraHandle();
}

void GLOWE::Components::Camera::onAfterDeserialize()
{
	if (isDefault)
	{
		auto& defaultRt = getInstance<Engine>().getWindow().getSwapChain().getRenderTarget();
		if (renderTarget.get() != &defaultRt)
		{
			destroy();
			renderTarget.reset(&defaultRt);
			isOwningRenderTarget = false;
			Viewport vp;
			vp.topX = vp.topY = 0.0f;
			vp.minDepth = 0.0f;
			vp.maxDepth = 1.0f;
			const auto tempSize = defaultRt.getSize();
			vp.width = tempSize[0];
			vp.height = tempSize[1];
			setViewport(vp);
		}
	}
	else
	{
		// TODO
		// It's done I think.
		create(renderTargetDescs, dsvDesc);
	}

	switch (projType)
	{
	case ProjectionType::Perspective:
		setPerspectiveFov(MathHelper::convertToRadians(fov));
		break;
	case ProjectionType::Orthographic:
		setOrthographic(viewport.width * orthoScale, viewport.height * orthoScale);
		break;
	default:
		break;
	}

	// If the camera is active, i.e. it's already registered, we need to reset the rendering engine binding so that the correct render target is used.
	if (checkIsActive())
	{
		getInstance<CameraMgr>().resetCameraRenderPass(cameraHandle);
	}
}

void GLOWE::Components::Camera::setClearColor(const Color& col)
{
	clearColor = col;
}

void GLOWE::Components::Camera::setClearType(const unsigned char newClearType)
{
	clearType = newClearType;
}

void GLOWE::Components::Camera::clearRenderTarget(BasicRendererImpl& renderer)
{
	if (clearType & ClearType::DepthStencil)
	{
		renderer.clearDepthStencil(renderTarget.get());
	}

	if (clearType & ClearType::Color)
	{
		renderer.clearColor(renderTarget.get(), clearColor);
	}

	if (clearType & ClearType::Skybox)
	{
		/// \todo { Implement universal skybox rendering (possibly provide something like setCustomSkyboxRenderer function for more flexibility). }
	}

	/*
	switch (clearType)
	{
	case GLOWE::Components::Camera::ClearType::Color:
		renderer.clearColor(renderTarget, clearColor);
	case GLOWE::Components::Camera::ClearType::DepthStencil:
		renderer.clearDepthStencil(renderTarget);
		break;
	case GLOWE::Components::Camera::ClearType::Skybox:
		break;
	default:
		break;
	}
	*/
}

void GLOWE::Components::Camera::setPerspective(const float width, const float height, const float farF, const float nearF)
{
	projType = ProjectionType::Perspective;
	needToUpdateFrustum = true;
	cameraMath.perspectiveProjection(width, height, farF, nearF);
}

void GLOWE::Components::Camera::setPerspectiveFov(const float fov, const float farF, const float nearF, const float aspectRatio)
{
	projType = ProjectionType::Perspective;
	float tempAR = aspectRatio;
	if (tempAR == 0.0f)
	{
		tempAR = getAspectRatio();
	}

	needToUpdateFrustum = true;
	cameraMath.perspectiveProjectionFov(fov, tempAR, farF, nearF);
}

void GLOWE::Components::Camera::setOrthographic()
{
	projType = ProjectionType::Orthographic;
	Float2 temp = getSize();

	needToUpdateFrustum = true;
	cameraMath.orthographicProjection(temp[0], temp[1]);
}

void GLOWE::Components::Camera::setOrthographic(const float width, const float height, const float farF, const float nearF)
{
	projType = ProjectionType::Orthographic;
	needToUpdateFrustum = true;
	cameraMath.orthographicProjection(width, height, farF, nearF);
}

void GLOWE::Components::Camera::setOrthographicOffScreen(const float left, const float right, const float top, const float bottom, const float farF, const float nearF)
{
	projType = ProjectionType::Orthographic;
	needToUpdateFrustum = true;
	cameraMath.orthographicProjectionOffScreen(left, right, top, bottom, farF, nearF);
}

void GLOWE::Components::Camera::setCustomProjectionMatrix(const Float4x4& newMat)
{
	needToUpdateFrustum = true;
	projType = ProjectionType::Other;
	cameraMath.setProj(newMat);
}

void GLOWE::Components::Camera::setNotRendered(const Vector<unsigned int>& whatNotToRender)
{
	onDeactivate();
	notRendered = whatNotToRender;
	onActivate();
}

void GLOWE::Components::Camera::setRenderPassId(const int newId)
{
	onDeactivate();
	engineRenderPassId = newId;
	onActivate();
}

void GLOWE::Components::Camera::setViewport(const Viewport& newVp)
{
	viewport = newVp;
}

void GLOWE::Components::Camera::updateViewFromTransform()
{
	cameraMath.fromTransform(gameObject->getTransform()->getTransform());
}

void GLOWE::Components::Camera::resize(const UInt3& newDimensions)
{
	if (!isOwningRenderTarget)
	{
		return;
	}

	for (auto& x : renderTargetDescs)
	{
		x.width = newDimensions[0];
		x.height = newDimensions[1];
		x.depth = newDimensions[2];
	}

	dsvDesc.width = newDimensions[0];
	dsvDesc.height = newDimensions[1];
	dsvDesc.depth = newDimensions[2];

	renderTarget->destroy();

	const GraphicsDeviceImpl& device = getInstance<Engine>().getGraphicsContext();
	for (unsigned int x = 0; x < targets.size(); ++x)
	{
		targets[x]->destroy();
		targets[x]->create(renderTargetDescs[x], device);
	}
	dsv->destroy();
	dsv->create(dsvDesc, device);

	renderTarget->create(targets, dsv, device);
}

void GLOWE::Components::Camera::recreateRenderTarget(const Vector<TextureDesc>& targetsDescs, const TextureDesc& dsvDesc)
{
	if (!isOwningRenderTarget)
	{
		return;
	}

	const GraphicsDeviceImpl& device = getInstance<Engine>().getGraphicsContext();
	targets.resize(targetsDescs.size());
	for (unsigned int x = 0; x < targets.size(); ++x)
	{
		if (targets[x])
		{
			targets[x]->destroy();
		}
		else
		{
			targets[x] = getInstance<GraphicsFactoryImpl>().createTexture();
		}
		targets[x]->create(renderTargetDescs[x], device);
	}
	dsv->destroy();
	dsv->create(dsvDesc, device);

	renderTarget->create(targets, dsv, device);
}

GLOWE::Components::Camera::ProjectionType GLOWE::Components::Camera::getProjectionType() const
{
	return projType;
}

const GLOWE::Frustum& GLOWE::Components::Camera::getFrustum()
{
	gameObject->getTransform()->updateMatrix();
	if (needToUpdateView)
	{
		updateViewFromTransform();
		needToUpdateView = false;
	}
	if (needToUpdateFrustum)
	{
		frustum.fromMatrix(cameraMath.getProj() * cameraMath.getView());
		needToUpdateFrustum = false;
	}

	return frustum;
}

GLOWE::Float2 GLOWE::Components::Camera::getSize() const
{
	return renderTarget ? renderTarget->getSize() : Float2{ 800.0f, 600.0f };
}

float GLOWE::Components::Camera::getAspectRatio() const
{
	Float2 temp = getSize();
	return temp[0] / temp[1];
}

GLOWE::Matrix4x4 GLOWE::Components::Camera::getCameraViewMatrix()
{
	gameObject->getTransform()->updateMatrix();
	if (needToUpdateView)
	{
		updateViewFromTransform();
		needToUpdateView = false;
	}
	return cameraMath.getView();
}

GLOWE::Matrix4x4 GLOWE::Components::Camera::getCameraProjMatrix() const
{
	return cameraMath.getProj();
}

GLOWE::Vector<unsigned int> GLOWE::Components::Camera::getNotRendered() const
{
	return notRendered;
}

int GLOWE::Components::Camera::getRenderPassId() const
{
	return engineRenderPassId;
}

GLOWE::RenderTargetImpl* GLOWE::Components::Camera::getRenderTarget() const
{
	return renderTarget.get();
}

const GLOWE::Viewport& GLOWE::Components::Camera::getViewport() const
{
	return viewport;
}

const GLOWE::SharedPtr<GLOWE::TextureImpl>& GLOWE::Components::Camera::getDsv() const
{
	return dsv;
}

const GLOWE::Vector<GLOWE::SharedPtr<GLOWE::TextureImpl>>& GLOWE::Components::Camera::getTargets() const
{
	return targets;
}

bool GLOWE::Components::Camera::checkIsOwningRenderTarget() const
{
	return isOwningRenderTarget;
}

GLOWE::RenderingEngine::RenderPassHandle GLOWE::CameraMgr::registerPass(Components::Camera* camera)
{
	RenderingEngine::RenderPass::Callbacks callbacks;
	callbacks.clearTargetFunc = [camera](BasicRendererImpl& renderer)
	{
		camera->clearRenderTarget(renderer);
	};
	callbacks.getFrustumFunc = [camera]() -> const Frustum&
	{
		return camera->getFrustum();
	};
	callbacks.getViewportFunc = [camera]() -> const Viewport&
	{
		return camera->getViewport();
	};
	callbacks.prepareMatEnvFunc = [camera](MaterialEnvironment& matEnv)
	{
		const auto matProj = camera->getCameraProjMatrix();
		const auto matView = camera->getCameraViewMatrix();
		matEnv.setVariable<Float4x4>(Hash(u8"Proj"), matProj);
		matEnv.setVariable<Float4x4>(Hash(u8"View"), matView);
		matEnv.setVariable<Float4x4>(Hash(u8"View Proj"), matProj * matView); // matProj * matView
		matEnv.setVariable<Float3>(Hash(u8"Eye Pos"), camera->getGameObject()->getTransform()->getWorldTranslation());
	};

	return getInstance<RenderingEngine>().addRenderPass(*camera->getRenderTarget(), camera->getNotRendered(), camera->getRenderPassId(), callbacks);
}

GLOWE::CameraMgr::CameraHandle GLOWE::CameraMgr::registerCamera(Components::Camera* camera)
{
	Pair<Components::Camera*, RenderingEngine::RenderPassHandle> pair;

	pair.first = camera;
	pair.second = registerPass(camera);

	cameras.emplace_back(pair);
	if (mainCamera == nullptr)
	{
		mainCamera = camera;
	}
	return std::prev(cameras.end());
}

void GLOWE::CameraMgr::unregisterCamera(const CameraHandle& camera)
{
	const Components::Camera* const temp = camera->first;
	getInstance<RenderingEngine>().removeRenderPass(camera->second);
	cameras.erase(camera);
	if (mainCamera == temp)
	{
		if (cameras.size() > 0)
		{
			mainCamera = cameras.front().first;
		}
		else
		{
			mainCamera = nullptr;
		}
	}
}

void GLOWE::CameraMgr::resetCameraRenderPass(const CameraHandle& camera)
{
	auto& renderMgr = getInstance<RenderingEngine>();
	renderMgr.removeRenderPass(camera->second);
	camera->second = registerPass(camera->first);
}

GLOWE::Components::Camera* GLOWE::CameraMgr::getMainCamera() const
{
	return mainCamera;
}

void GLOWE::CameraMgr::setMainCamera(Components::Camera* newMainCamera)
{
	mainCamera = newMainCamera;
}
