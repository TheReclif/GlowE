#pragma once
#ifndef GLOWE_COMPONENTS_GUITOP_INCLUDED
#define GLOWE_COMPONENTS_GUITOP_INCLUDED

#include "ScriptBehaviour.h"

#include "../GlowMath/CameraMath.h"

#include "../GlowEngine/GUI/GUIContainer.h"
#include "../GlowEngine/RenderingEngine.h"

#include "../GlowGraphics/Uniform/RenderTargetImplementation.h"
#include "../GlowGraphics/Uniform/TextureImplementation.h"

namespace GLOWE
{
	namespace Components
	{
		/// @brief GUI top component. It's supposed to work like a canvas. It manages and owns the rendering process for all of its children.
		class GLOWENGINE_EXPORT GUITop
			: public GLOWE::GUIContainer, public ScriptBehaviour
		{
		private:
			bool shouldProcessEvents, shouldClearTarget;
			RenderTargetImpl* renderTarget;
			UniquePtr<RenderTargetImpl> ownedRenderTarget;
			SharedPtr<TextureImpl> target, dsv;
			Material usedMaterial;
			ResourceHandle<Geometry> guiPlane;

			RenderingEngine::RenderPassHandle renderingEngineHandle;
			int engineRenderPassId;
			bool wasAddedToRenderEngine;
			Color clearColor;
			Viewport viewport;
			CameraMath cameraMat;
			MaterialEnvironment* matEnv;
		private:
			void addToRenderingEngine();
			void removeFromRenderingEngine();
			void resizeRT(const UInt2 size, const Format& templateFormat);
		protected:
			virtual void updateImpl() override;
			virtual void renderImpl(BasicRendererImpl& renderer, MaterialEnvironment& matEnv) override;
			virtual void onRectUpdated() override;
			virtual void populateRenderCallbacks(RenderingEngine::RenderPass::Callbacks& callbacks);
		public:
			/// @brief Minimal constructor.
			/// @param newParent Component's parent.
			GUITop(GameObject* const newParent);
			~GUITop();

			GlowSerializeScript(GUITop, GlowBases(), GlowField(engineRenderPassId), GlowField(clearColor));// , GlowField(viewport));

			/// @brief Sets the non owned render target. Will return immediately if owns target.
			/// @param newTarget New non owned render target.
			void setNotOwnedRenderTarget(RenderTargetImpl* const newTarget, const Format& rtFormat = Format(Format::Type::Byte, 4, true));

			virtual void processEvent(const GameEvent& event) override;

			virtual void onScriptActivated() override;
			virtual void onScriptDeactivated() override;

			/// @brief Forces any pending updates to take place now.
			void forceUpdate();

			/// @brief Sets the clear color.
			/// @param newClearColor New clear color.
			void setClearColor(const Color& newClearColor);
			/// @brief Enables/disables target render target clearing.
			/// @param shouldClear True to enable, false to disable the render target clearing
			void setShouldClearRT(const bool shouldClear);

			/// @brief Returns whether the GUITop processes events.
			/// @return true if processes events, false otherwise.
			bool checkIfProcessesEvents() const;
			/// @brief Enables or disables event processing. When disabled, the processGUIEvent method will not propagate events to its children.
			/// @param arg Boolean indicating whether to process events.
			void setEventProcessing(const bool arg);
		};

		GlowInitComponentFactoryReflection(GUITop);
	}
}

#endif
