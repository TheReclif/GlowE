#pragma once
#ifndef GLOWE_COMPONENTS_GEOMETRYRENDERERCOMPONENT_INCLUDED
#define GLOWE_COMPONENTS_GEOMETRYRENDERERCOMPONENT_INCLUDED

#include "RenderableComponentBase.h"

#include "../GlowEngine/Material.h"

#include "../GlowGraphics/Uniform/Geometry.h"
#include "../GlowGraphics/Uniform/LightStructures.h"

#include "../GlowMath/AxisAlignedBoundingBox.h"

namespace GLOWE
{
	namespace Components
	{
		class GLOWENGINE_EXPORT GeometryRenderer
			: public RenderableBase
		{
		private:
			Hash variantToChoose;
			ResourceHandle<Material> material;
			ResourceHandle<Geometry> geometry;
			AABB worldBounds;
			bool needToUpdateBounds;
			Vector<const DirectionalLight*> bestDirLights;
			Vector<const PointLight*> bestPointLights;
			Vector<const SpotLight*> bestSpotLights;
			BasicRendererImpl::PrimitiveTopology usedTopology;
		private:
			virtual bool isExcludedFromRenderingCustom(const RenderingEngine::RenderPass& renderPass) const override;
			virtual Hash recalculateVariantHashCustom() override;
		private:
			virtual bool performCulling(const Frustum& frustum) override;
		public:
			/// @brief The minimal constructor.
			/// @param newParent Component's parent.
			GeometryRenderer(GameObject* const newParent);
			virtual ~GeometryRenderer();

			GlowSerializeComponent(GeometryRenderer, GlowBases(), GlowField(material), GlowField(geometry));

			/// @brief Sets the used mesh.
			/// @param newGeometry New mesh.
			void setGeometry(const ResourceHandle<Geometry>& newGeometry);
			/// @brief Returns the current mesh.
			/// @return Current mesh.
			const ResourceHandle<Geometry>& getGeometry() const;

			/// @brief Sets the used material. Call at least once as the material is not default initialized to the default material.
			/// @param newMaterial New material.
			void setMaterial(const ResourceHandle<Material>& newMaterial);
			/// @brief Returns the current material.
			/// @return Current material.
			const ResourceHandle<Material>& getMaterial() const;

			void setPrimitiveTopology(const BasicRendererImpl::PrimitiveTopology newTopo);

			// Component's methods.
			virtual void onFirstActivate() override {};

			virtual void onActivate() override;
			virtual void onDeactivate() override;

			virtual void processEvent(const GameEvent& event) override {};

			virtual void render(BasicRendererImpl& renderer, MaterialEnvironment& matEnv) override;

			virtual Material* getMaterialPtr() const override;
		};

		GlowInitComponentFactoryReflection(GeometryRenderer);
	}
}

#endif
