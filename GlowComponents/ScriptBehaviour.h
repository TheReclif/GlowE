#pragma once
#ifndef GLOWE_COMPONENTS_SCRIPTBEHAVIOUR_INCLUDED
#define GLOWE_COMPONENTS_SCRIPTBEHAVIOUR_INCLUDED

#include "../GlowScripting/BaseScriptBehaviour.h"

#include "../GlowEngine/ScriptMgr.h"
#include "../GlowEngine/Component.h"

/// @brief Used to autogenerate script reflection methods. Can be used instead of a general macro GlowSerializeScript (eg. to use GlowSerializeComponentCustomAccessors).
#define GlowScriptRefl() \
virtual bool checkIsAsyncUpdated() const noexcept override { return GLOWE::Hidden::checkIfScriptIsAsyncUpdated(this); } \
virtual bool checkIsProcessingEvents() const noexcept override { return GLOWE::Hidden::checkIfScriptProcessesEvents(this); } \
virtual bool checkIsUpdated() const noexcept override { return GLOWE::Hidden::checkIfScriptIsSyncUpdated(this); }

/// @brief A general macro used to serialize a script. Also, autogenerates script reflection methods.
#define GlowSerializeScript(ScriptName, bases, ...) \
GlowSerializeComponent(ScriptName, bases, __VA_ARGS__) \
GlowScriptRefl()

namespace GLOWE
{
	class Engine;

	namespace Components
	{
		/// @brief Base script class. Every script has to derive from it.
		class GLOWENGINE_EXPORT ScriptBehaviour
			: public Component, public BaseScriptBehaviour
		{
		protected:
			Engine& engine;
		private:
			ScriptMgr::ScriptHandle scriptHandle;
		public:
			/// @brief Default-initializes the component with given game object as its parent.
			/// @param newParent Pointer to component's parent.
			ScriptBehaviour(GameObject* const newParent);
			virtual ~ScriptBehaviour();

			/// @brief Update function. Called asynchronously every frame. Use ComponentLocks to modify components' values in there. To enable asynchronous update, place GlowScriptEnableAsyncUpdate in your class' body.
			virtual void update() {};
			/// @brief Update function called synchronously after all of the asynchronous updates have been finished. You don't need to use ComponentLocks here since the function is called on one thread.
			virtual void postUpdateSync() {};

			/// @brief Event processing method. Called asynchronously for every event and every script. Use ComponentLocks to modify components' values in there. To enable asynchronous event processing, place GlowScriptEnableProcessEvent in your class' body.
			/// @param event Processed event.
			virtual void processEvent(const GameEvent& event) override {};

			/// @brief Called synchronously once at the first activation of the component.
			virtual void onFirstActivate() override {};

			/// @brief Called synchronously after the object became active.
			virtual void onActivate() override final;
			/// @brief Called synchronously after the object became inactive.
			virtual void onDeactivate() override final;

			/// @brief Called synchronously after the object became active.
			virtual void onScriptActivated() {};
			/// @brief Called synchronously after the object became inactive.
			virtual void onScriptDeactivated() {};

			/// @brief Overriden automatically by GlowScriptRefl or GlowSerializeScript.
			/// @return true if implements asynchronous event processing, false otherwise.
			virtual bool checkIsProcessingEvents() const noexcept = 0;
			/// @brief Overriden automatically by GlowScriptRefl or GlowSerializeScript.
			/// @return true if implements its own asynchronous update, false otherwise.
			virtual bool checkIsAsyncUpdated() const noexcept = 0;
			/// @brief Overriden automatically by GlowScriptRefl or GlowSerializeScript.
			/// @return true if implements its own update, false otherwise.
			virtual bool checkIsUpdated() const noexcept = 0;
		};
	}

	namespace Hidden
	{
		inline bool checkIfScriptIsAsyncUpdated(...)
		{
			return false;
		}

		template<class T>
		inline auto checkIfScriptIsAsyncUpdated(const T*) -> decltype(&T::update, true)
		{
			return typeid(&T::update) != typeid(&GLOWE::Components::ScriptBehaviour::update);
		}

		inline bool checkIfScriptIsSyncUpdated(...)
		{
			return false;
		}

		template<class T>
		inline auto checkIfScriptIsSyncUpdated(const T*) -> decltype(&T::postUpdateSync, true)
		{
			return typeid(&T::postUpdateSync) != typeid(&GLOWE::Components::ScriptBehaviour::postUpdateSync);
		}

		inline bool checkIfScriptProcessesEvents(...)
		{
			return false;
		}

		template<class T>
		inline auto checkIfScriptProcessesEvents(const T*) -> decltype(&T::processEvent, true)
		{
			return typeid(&T::processEvent) != typeid(&GLOWE::Components::ScriptBehaviour::processEvent);
		}
	}
}

#endif
