#pragma once
#ifndef GLOWE_COMPONENTS_RENDERABLECOMPONENTBASE_INCLUDED
#define GLOWE_COMPONENTS_RENDERABLECOMPONENTBASE_INCLUDED

#include "../GlowEngine/Component.h"
#include "../GlowEngine/Renderable.h"

#include "../GlowGraphics/Uniform/Cullable.h"

namespace GLOWE
{
	namespace Components
	{
		/// @brief Base class for all renderable components.
		class GLOWENGINE_EXPORT RenderableBase
			: public Component, public GLOWE::Renderable, public GLOWE::Cullable
		{
		public:
			/// @brief Default-initializes the component with given game object as its parent.
			/// @param newParent Pointer to component's parent.
			RenderableBase(GameObject* const newParent);

			virtual ~RenderableBase() = default;
		};
	}
}

#endif
