#include "PointLightComponent.h"
#include "TransformComponent.h"
#include "../GlowEngine/GameObject.h"

void GLOWE::Components::PointLight::updateLight()
{
	if (needToUpdate)
	{
		auto transform = gameObject->getTransform();
		lightInfo.position = transform->getWorldTranslation();
		pointLightHandle = getInstance<LightingMgr>().updatePointLight(pointLightHandle, lightInfo);
		needToUpdate = false;
	}
}

GLOWE::Components::PointLight::PointLight(GameObject* const newParent)
	: Component(newParent), lightInfo{ Color::white, Color::white, Color::white, Float3(), 0.0f, Float3{1.0f, 1.0f, 1.0f}, 1000.0f }, pointLightHandle(), lightHandle(), needToUpdate(true)
{
}

void GLOWE::Components::PointLight::setAmbient(const Color& newAmbient)
{
	lightInfo.ambient = newAmbient;
	needToUpdate = true;
}

void GLOWE::Components::PointLight::setDiffuse(const Color& newDiffuse)
{
	lightInfo.diffuse = newDiffuse;
	needToUpdate = true;
}

void GLOWE::Components::PointLight::setSpecular(const Color& newSpecular)
{
	lightInfo.specular = newSpecular;
	needToUpdate = true;
}

void GLOWE::Components::PointLight::setAttenuation(const Float3& newAtten)
{
	lightInfo.attenuation = newAtten;
	needToUpdate = true;
}

void GLOWE::Components::PointLight::setAttenuation(const Vector3F& newAtten)
{
	lightInfo.attenuation = newAtten.toFloat3();
	needToUpdate = true;
}

void GLOWE::Components::PointLight::setRange(const float newRange, const bool autoSetAttenuation)
{
	lightInfo.range = newRange;
	if (autoSetAttenuation)
	{
		lightInfo.attenuation = Float3{ 1.0f, 4.5f / newRange, 75.0f / (newRange * newRange) };
	}
	needToUpdate = true;
}

void GLOWE::Components::PointLight::setImportance(const float newImportance)
{
	lightInfo.importance = newImportance;
	needToUpdate = true;
}

void GLOWE::Components::PointLight::processEvent(const GameEvent& event)
{
	if (event == GameEvent::Type::TransformUpdated)
	{
		needToUpdate = true;
	}
}

void GLOWE::Components::PointLight::onActivate()
{
	pointLightHandle = getInstance<LightingMgr>().addPointLight(lightInfo);
	lightHandle = getInstance<LightingMgr>().registerLight(this);
}

void GLOWE::Components::PointLight::onDeactivate()
{
	getInstance<LightingMgr>().removePointLight(pointLightHandle);
	getInstance<LightingMgr>().unregisterLight(lightHandle);
}
