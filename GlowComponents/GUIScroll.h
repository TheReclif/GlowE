#pragma once
#ifndef GLOWE_COMPONENTS_GUISCROLL_INCLUDED
#define GLOWE_COMPONENTS_GUISCROLL_INCLUDED

#include "../GlowEngine/Component.h"
#include "../GlowEngine/GUI/GUIElement.h"
#include "../GlowEngine/GUI/GUILinearLayout.h"

#include "GUIButtonComponent.h"
#include "GUIImageComponent.h"
#include "ScriptBehaviour.h"

namespace GLOWE
{
	namespace Components
	{
		/// @brief GUI scroll component. Works horizontally and vertically.
		class GLOWENGINE_EXPORT GUIScroll
			: public ScriptBehaviour, public GLOWE::GUIContainer
		{
		private:
			GUIButton* upLeftButton, *bottomRightButton, *scrollButton;
			GUIImage* background;
			GUILinearLayout* linearLayout;
			Float2 lastMousePos;
#ifdef _DEBUG
			bool isMouseInside, wasMouseClicked, isVertical, isMouseScrollEnabled;
#else
			struct
			{
				bool isMouseInside : 1, wasMouseClicked : 1, isVertical : 1, isMouseScrollEnabled : 1;
			};
#endif
			float currentButtonPos, changeInPixels;
			std::function<void()> onScrollMovedCallback;
		private:
			void setupConstraints();

			virtual bool onAfterElementsUpdate() override;
		public:
			/// @brief Minimal constructor. Scroll's direction must be set here (it can be done only once, here).
			/// @param newParent Component's parent.
			/// @param vertical true if the scroll is vertical, false otherwise. Defaults to true.
			GUIScroll(GameObject* const newParent, const bool vertical = true);

			// TODO
			//GlowSerializeScript(GlowField(elementToPosition), GlowField(bottomElement));
			GlowSerializeScript(GUIScroll, GlowBases());

			virtual void setParent(GUIContainer* const newParent) override;

			/// @brief If any changes were made to the linear layout, it's updated, then it's returned.
			/// @return Reference to the linear layout.
			GUILinearLayout* getLayout() const;
			/// @brief Sets the managed linear layout. The layout must have the same parent as the scroll.
			/// @param layout Managed layout.
			void setManagedLayout(GLOWE::GUILinearLayout* const layout);
			/// @brief Enables or disables support for mouse scroll. Defaults to true.
			/// @param enableMouseScroll true to enable, false otherwise.
			void setMouseScroll(const bool enableMouseScroll);

			/// @brief Finds a suitable GUI container parent by moving up in the game object's hierarchy.
			void autoFindGUIContainer();

			/// @brief Moves the scroll by given amount of pixels/units and updates the content's position.
			/// @param amount Translation in pixels.
			void moveScrollBy(const float amount);
			/// @brief ABANDONED: Moves the managed content by given amount of pixels/units and updates the scroll button's position.
			/// @param amount Translation in pixels.
			//void moveContentBy(const float amount);
			/// @brief Moves the scroll button to a given relative position in [0, 1] range.
			/// @param in01Pos Position in [0, 1] range. 0 means the top edge, 1 means the bottom edge.
			void moveScrollTo(const float in01Pos);
			/// @brief ABANDONED: Moves the content to a given relative position in [0, 1] range.
			/// @param in01Pos Position in [0, 1] range. 0 means the top edge, 1 means the bottom edge.
			//void moveContentTo(const float in01Pos);

			void setOnScrollMovedCallback(const std::function<void()>& func);

			virtual void placeAtBack() override;
			virtual void placeAtFront() override;
			virtual void placeBefore(GUIElement* const beforeWhat) override;

			/// @brief Calculates the scroll bar size (height for vertical, width for horizontal).
			/// @return Bar size.
			float calculateBarSize() const;

			virtual void processEvent(const GameEvent& event) override;

			virtual void onFirstActivate() override;
		};

		GlowInitComponentFactoryReflection(GUIScroll);
	}
}

#endif
