#pragma once
#ifndef GLOWE_COMPONENTS_GUILINEARLAYOUT_INCLUDED
#define GLOWE_COMPONENTS_GUILINEARLAYOUT_INCLUDED

#include "../GlowEngine/Component.h"
#include "../GlowEngine/GUI/GUILinearLayout.h"

namespace GLOWE
{
	namespace Components
	{
		/// @brief Component that places all of its children in a list-like order and manner.
		class GLOWENGINE_EXPORT GUILinearLayout
			: public GLOWE::GUILinearLayout, public Component
		{
		public:
			GUILinearLayout(GameObject* const newParent);

			GlowSerializeComponent(GUILinearLayout, GlowBases());

			virtual void processEvent(const GameEvent& event) override {};

			virtual void onFirstActivate() override;

			virtual void onActivate() override {};
			virtual void onDeactivate() override {};

			virtual bool checkIsActiveInGUI() const override;
		};

		GlowInitComponentFactoryReflection(GUILinearLayout);
	}
}

#endif
