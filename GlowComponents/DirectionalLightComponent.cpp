#include "DirectionalLightComponent.h"
#include "TransformComponent.h"
#include "../GlowEngine/GameObject.h"

void GLOWE::Components::DirectionalLight::updateLight()
{
	if (needToUpdate)
	{
		auto transform = gameObject->getTransform();
		const Matrix4x4 mat = transform->getMatrix();
		const Float4 tempResult = (mat * Vector4F(VecHelpers::forward[0], VecHelpers::forward[1], VecHelpers::forward[2], 0.0f)).toFloat4();
		lightInfo.direction = Float3{ tempResult[0], tempResult[1], tempResult[2] };
		dirLightHandle = getInstance<LightingMgr>().updateDirectionalLight(dirLightHandle, lightInfo);
		needToUpdate = false;
	}
}

GLOWE::Components::DirectionalLight::DirectionalLight(GameObject* const newParent)
	: Component(newParent), lightInfo{ Color::white, Color::white, Color::white, 1000.0f }, dirLightHandle(), lightHandle(), needToUpdate(true)
{
}

void GLOWE::Components::DirectionalLight::setAmbient(const Color& newAmbient)
{
	lightInfo.ambient = newAmbient;
	needToUpdate = true;
}

void GLOWE::Components::DirectionalLight::setDiffuse(const Color& newDiffuse)
{
	lightInfo.diffuse = newDiffuse;
	needToUpdate = true;
}

void GLOWE::Components::DirectionalLight::setSpecular(const Color& newSpecular)
{
	lightInfo.specular = newSpecular;
	needToUpdate = true;
}

void GLOWE::Components::DirectionalLight::setImportance(const float newImportance)
{
	lightInfo.importance = newImportance;
	needToUpdate = true;
}

void GLOWE::Components::DirectionalLight::processEvent(const GameEvent& event)
{
	if (event == GameEvent::Type::TransformUpdated)
	{
		needToUpdate = true;
	}
}

void GLOWE::Components::DirectionalLight::onActivate()
{
	dirLightHandle = getInstance<LightingMgr>().addDirectionalLight(lightInfo);
	lightHandle = getInstance<LightingMgr>().registerLight(this);
}

void GLOWE::Components::DirectionalLight::onDeactivate()
{
	getInstance<LightingMgr>().removeDirectionalLight(dirLightHandle);
	getInstance<LightingMgr>().unregisterLight(lightHandle);
}
