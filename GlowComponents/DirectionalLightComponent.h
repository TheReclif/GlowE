#pragma once
#ifndef GLOWE_DIRLIGHTCOMPONENT_INCLUDED
#define GLOWE_DIRLIGHTCOMPONENT_INCLUDED

#include "../GlowEngine/Component.h"

#include "../GlowSystem/Color.h"

#include "../GlowGraphics/Uniform/LightingMgr.h"

namespace GLOWE
{
	namespace Components
	{
		/// @brief Directional light component. Directional light is a light that has an infinite range and no attenuation. It's described only by its color (ambient, specular and diffuse), importance (used only in forward shading for chosing which lights to chose when more than 16 are present at once) and direction (it's placed in infinity). Its direction is driven by the game object's Transform.
		class GLOWENGINE_EXPORT DirectionalLight
			: public Component, public LightingMgr::LightBase
		{
		private:
			GLOWE::DirectionalLight lightInfo;
			LightingMgr::DirectionalLightHandle dirLightHandle;
			LightingMgr::RegisteredLightHandle lightHandle;
			bool needToUpdate;
		private:
			virtual void updateLight() override;
		public:
			/// @brief Default-initializes the component with given game object as its parent.
			/// @param newParent Pointer to component's parent.
			DirectionalLight(GameObject* const newParent);

			GlowSerializeComponent(DirectionalLight, GlowBases(), GlowField(lightInfo));

			/// @brief Sets the light's ambient color.
			/// @param newAmbient New ambient.
			void setAmbient(const Color& newAmbient);
			/// @brief Sets the light's diffuse color.
			/// @param newDiffuse New diffuse.
			void setDiffuse(const Color& newDiffuse);
			/// @brief Sets the light's specular color.
			/// @param newSpecular New specular.
			void setSpecular(const Color& newSpecular);
			/// @brief Sets the light importance. When more than 8 directional lights are present in the object's range, they're sorted using the following formula: 1.0f / (light.importance / 1000.0f) (the bigger the importance the better for the light). Importance defaults to 1000.
			/// @param newImportance New importance.
			void setImportance(const float newImportance);

			virtual void processEvent(const GameEvent& event) override;

			virtual void onFirstActivate() override {};

			// Actually it makes no sense to disable/enable a Transform.
			virtual void onActivate() override;
			virtual void onDeactivate() override;
		};

		GlowInitComponentFactoryReflection(DirectionalLight);
	}
}

#endif
