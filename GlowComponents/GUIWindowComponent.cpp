#include "GUIWindowComponent.h"

#include "../GlowGraphics/Uniform/TextureImplementation.h"

#include "../GlowEngine/Engine.h"
#include "../GlowEngine/GameObject.h"

GLOWE::Components::GUIWindow::GUIWindow(GameObject* const newParent)
	: Component(newParent), clientArea(), titlebar(nullptr), closeButton(nullptr), titlebarBackground(nullptr)
{
	background = &gameObject->addComponent<GUIImage>();
	background->setParent(this);
	background->stretchToParent();
	background->setColor(Color::from255RGBA(35, 55, 67, 255));

	addElementBack(&clientArea);

	GUIConstraint constraint;
	constraint.type = GUIConstraint::Type::FixedLeft;
	constraint.fixedData.distance = 5.34f;
	constraint.fixedData.whichEdge = GUIConstraint::Edge::LeftUp;
	constraint.fixedData.whichElement = this;
	addConstraint(&clientArea, constraint);

	constraint.type = GUIConstraint::Type::FixedUp;
	clientAreaUpConstraint = addConstraint(&clientArea, constraint);

	constraint.type = GUIConstraint::Type::FixedRight;
	constraint.fixedData.whichEdge = GUIConstraint::Edge::RightDown;
	addConstraint(&clientArea, constraint);

	constraint.type = GUIConstraint::Type::FixedDown;
	addConstraint(&clientArea, constraint);
}

GLOWE::Components::GUIWindow::GUIWindow(GameObject* const newParent, const AutoFindParentOnStart&)
	: GUIWindow(newParent)
{
	autoFindContainer(gameObject);
}

GLOWE::Components::GUIWindow::~GUIWindow()
{
	if (closeButton)
	{
		gameObject->deleteComponent(closeButton);
	}
	if (titlebarBackground)
	{
		gameObject->deleteComponent(titlebarBackground);
	}
	if (titlebar)
	{
		gameObject->deleteComponent(titlebar);
	}
}

void GLOWE::Components::GUIWindow::addCloseButton()
{
	if (closeButton)
	{
		return;
	}

	// Just in case.
	addTitlebar();

	closeButton = &gameObject->addComponent<GUIButton>();
	GUIImage* const image = closeButton->getBackgroundImage();
	Material& mat = image->getMaterial();
	const int matTexPos = mat.getTexturePosition("Diffuse Texture");

	// Should always be positive, but we check just in case.
	if (matTexPos >= 0)
	{
		mat.setTexture(matTexPos, loadResource<TextureImpl>("GlowCore/Textures/WindowCloseIcon.gtex", ""));
	}
	else
	{
		WarningThrow(false, "Unable to set window's close button texture (Diffuse Texture not present in the GUI shader).");
	}

	closeButton->setBaseColor(Color::grey);
	closeButton->setHighlightColor(Color::red);
	closeButton->setPressedColor(Color::from255RGBA(255, 51, 51));
	closeButton->setOnClickCallback([](Components::GUIButton& button)
	{
		button.getGameObject()->deleteComponent(&button);
	});

	GUIConstraint constraint;
	constraint.type = GUIConstraint::Type::VerticalRatio;
	constraint.ratioData.leftUpPart = constraint.ratioData.rightBottomPart = 1.0f;
	constraint.ratioData.leftUp = titlebarBackground;
	constraint.ratioData.rightBottom = titlebarBackground;
	constraint.ratioData.whichEdgeLeftUp = GUIConstraint::Edge::LeftUp;
	constraint.ratioData.whichEdgeRightBottom = GUIConstraint::Edge::RightDown;
	image->getParent()->addConstraint(image, constraint);

	constraint.type = GUIConstraint::Type::FixedRight;
	constraint.fixedData.distance = 7.5f;
	constraint.fixedData.whichElement = titlebarBackground;
	constraint.fixedData.whichEdge = GUIConstraint::Edge::RightDown;
	image->getParent()->addConstraint(image, constraint);

	closeButton->setLocalGUIRect(GUIRect({ 0, 0 }, { 20, 20 }));
}

void GLOWE::Components::GUIWindow::addTitlebar(const String& newTitle)
{
	if (titlebar)
	{
		return;
	}

	titlebarBackground = &gameObject->addComponent<GUIImage>();
	Material& mat = titlebarBackground->getMaterial();
	const int cbufferPos = mat.getCBufferPosition("Material");
	if (cbufferPos >= 0)
	{
		const int colorPos = mat.getCBufferFieldPosition(cbufferPos, "Color");
		if (colorPos >= 0)
		{
			mat.modifyCBuffer<Color>(cbufferPos, colorPos, Color::from255RGBA(62, 96, 117));
		}
	}
	const int texPos = mat.getTexturePosition("Diffuse Texture");
	if (texPos >= 0)
	{
		mat.setTexture(texPos, getInstance<ResourceMgr>().load<TextureImpl>("GlowCore/Textures/PlainColorTexture.gtex", ""));
	}
	titlebar = &gameObject->addComponent<GUITextField>();

	GUIConstraint constraint;
	constraint.type = GUIConstraint::Type::FixedUp;
	constraint.fixedData.distance = 0;
	constraint.fixedData.whichElement = this;
	constraint.fixedData.whichEdge = GUIConstraint::Edge::LeftUp;
	titlebarBackground->getParent()->addConstraint(titlebarBackground, constraint);

	constraint.type = GUIConstraint::Type::FixedLeft;
	titlebarBackground->getParent()->addConstraint(titlebarBackground, constraint);

	constraint.type = GUIConstraint::Type::FixedRight;
	constraint.fixedData.whichEdge = GUIConstraint::Edge::RightDown;
	titlebarBackground->getParent()->addConstraint(titlebarBackground, constraint);
	titlebarBackground->setLocalGUIRect(GUIRect({ 0, 0 }, { 30, 30 }));

	constraint.fixedData.distance = 7.5f;

	constraint.type = GUIConstraint::Type::FixedLeft;
	constraint.fixedData.whichEdge = GUIConstraint::Edge::LeftUp;
	constraint.fixedData.whichElement = titlebarBackground;
	titlebar->getParent()->addConstraint(titlebar, constraint);

	constraint.type = GUIConstraint::Type::FixedUp;
	titlebar->getParent()->addConstraint(titlebar, constraint);

	constraint.type = GUIConstraint::Type::FixedRight;
	constraint.fixedData.whichEdge = GUIConstraint::Edge::RightDown;
	titlebar->getParent()->addConstraint(titlebar, constraint);

	constraint.type = GUIConstraint::Type::FixedDown;
	titlebar->getParent()->addConstraint(titlebar, constraint);

	removeConstraint(&clientArea, clientAreaUpConstraint);
	constraint.type = GUIConstraint::Type::FixedUp;
	constraint.fixedData.distance = 5.34f;
	constraint.fixedData.whichEdge = GUIConstraint::Edge::RightDown;
	constraint.fixedData.whichElement = titlebarBackground;
	clientAreaUpConstraint = clientArea.getParent()->addConstraint(&clientArea, constraint);

	titlebar->setText(newTitle);
}

GLOWE::GUIContainer* GLOWE::Components::GUIWindow::getClientArea()
{
	return &clientArea;
}

const GLOWE::GUIContainer* GLOWE::Components::GUIWindow::getClientArea() const
{
	return &clientArea;
}

GLOWE::Components::GUIImage* GLOWE::Components::GUIWindow::getBackgroundComponent()
{
	return background;
}

void GLOWE::Components::GUIWindow::setBackground(const ResourceHandle<TextureImpl>& texture)
{
	background->setImage(texture);
}

void GLOWE::Components::GUIWindow::setBackgroundColor(const Color& col)
{
	background->setColor(col);
}

void GLOWE::Components::GUIWindow::setTitle(const String& newTitle)
{
	if (titlebar)
	{
		titlebar->setText(newTitle);
	}
}

void GLOWE::Components::GUIWindow::onFirstActivate()
{
	autoFindContainer(gameObject);
}

bool GLOWE::Components::GUIWindow::checkIsActiveInGUI() const
{
	return checkIsActive();
}
