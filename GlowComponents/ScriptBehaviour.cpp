#include "ScriptBehaviour.h"

#include "../GlowEngine/Engine.h"
#include "../GlowEngine/ScriptMgr.h"

GLOWE::Components::ScriptBehaviour::ScriptBehaviour(GameObject* const newParent)
	: Component(newParent), engine(getInstance<Engine>()), scriptHandle()
{
}

GLOWE::Components::ScriptBehaviour::~ScriptBehaviour()
{
	getInstance<ScriptMgr>().unregisterScript(scriptHandle);
}

void GLOWE::Components::ScriptBehaviour::onActivate()
{
	scriptHandle = getInstance<ScriptMgr>().registerScript(this);
	onScriptActivated();
}

void GLOWE::Components::ScriptBehaviour::onDeactivate()
{
	onScriptDeactivated();
	getInstance<ScriptMgr>().unregisterScript(scriptHandle);
}
