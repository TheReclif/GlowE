
// Generated from F:/Dokumenty/GlowE/antlr4/GSILParser.g4 by ANTLR 4.13.0

#pragma once


#include "antlr4-runtime.h"


namespace GSIL {


class  GSILParser : public antlr4::Parser {
public:
  enum {
    WhiteSpace = 1, MultiLineComment = 2, SingleLineComment = 3, CodeSnippetPrefix = 4, 
    CBuffer = 5, Struct = 6, InOut = 7, Out = 8, GeometryShaderInputSpecifier = 9, 
    SVSpecifier = 10, EngineSupplied = 11, Hidden = 12, Sampler = 13, BlendState = 14, 
    DepthStencilState = 15, RasterizerState = 16, PropertySpecifier = 17, 
    Instanced = 18, TextureType = 19, UAVType = 20, TechniqueGroup = 21, 
    Technique = 22, Pass = 23, CompileShader = 24, Tag = 25, Threads = 26, 
    EarlyZ = 27, MaxVertexCount = 28, InterpolationModeSpecifier = 29, Uniform = 30, 
    ShaderName = 31, VectorType = 32, Matrix = 33, DMatrix = 34, False = 35, 
    True = 36, Break = 37, Continue = 38, Discard = 39, Do = 40, If = 41, 
    Switch = 42, While = 43, Case = 44, Default = 45, For = 46, Else = 47, 
    Return = 48, StorageClass = 49, TypeModifier = 50, LeftParen = 51, RightParen = 52, 
    LeftBracket = 53, RightBracket = 54, LeftBrace = 55, RightBrace = 56, 
    Less = 57, LessEqual = 58, Greater = 59, GreaterEqual = 60, LeftShift = 61, 
    RightShift = 62, Plus = 63, PlusPlus = 64, Minus = 65, MinusMinus = 66, 
    Star = 67, Div = 68, Mod = 69, And = 70, Or = 71, AndAnd = 72, OrOr = 73, 
    Caret = 74, Not = 75, Tilde = 76, Question = 77, Colon = 78, ColonColon = 79, 
    Semi = 80, Comma = 81, Assign = 82, StarAssign = 83, DivAssign = 84, 
    ModAssign = 85, PlusAssign = 86, MinusAssign = 87, LeftShiftAssign = 88, 
    RightShiftAssign = 89, AndAssign = 90, XorAssign = 91, OrAssign = 92, 
    Equal = 93, NotEqual = 94, Dot = 95, IntegralLiteral = 96, FloatLiteral = 97, 
    StringLiteral = 98, Identifier = 99
  };

  enum {
    RuleLineSep = 0, RuleVariableType = 1, RuleVariableArrayPart = 2, RuleSimpleVariableDefinition = 3, 
    RulePropertyColorValue = 4, RulePropertyDefinition = 5, RuleStatePropertyName = 6, 
    RuleStatePropertyValue = 7, RuleStatePropertySetter = 8, RuleStateDefinition = 9, 
    RuleMaxVertexCount = 10, RuleThreads = 11, RuleAttribute = 12, RuleAttributeDefinition = 13, 
    RuleShaderInputAccessSpecifier = 14, RuleRasterizerStateDefinition = 15, 
    RuleBlendStateDefinition = 16, RuleSamplerDefinition = 17, RuleDepthStencilStateDefinition = 18, 
    RuleTextureDefinition = 19, RuleUavUnderlayingType = 20, RuleUavDefinition = 21, 
    RuleStateNames = 22, RuleCompileShaderReplacement = 23, RuleCompileShader = 24, 
    RulePassShaderKeyValuePair = 25, RulePassStateKeyValuePair = 26, RulePassKeyValuePair = 27, 
    RuleTagValue = 28, RuleTagDefinition = 29, RulePassDefinition = 30, 
    RuleTechniqueDefinition = 31, RuleTechniqueGroupDefinition = 32, RuleStructSpecifier = 33, 
    RuleFieldSpecifier = 34, RuleStructField = 35, RuleStructDefinition = 36, 
    RuleCbufferDefinition = 37, RuleValidInFunctionDef = 38, RuleBlockContent = 39, 
    RuleBlock = 40, RuleFunctionParameter = 41, RuleCommaFunctionParameter = 42, 
    RuleFunctionParameterList = 43, RuleFunctionDefinition = 44, RuleTopLevelDeclaration = 45, 
    RuleCompilationUnit = 46
  };

  explicit GSILParser(antlr4::TokenStream *input);

  GSILParser(antlr4::TokenStream *input, const antlr4::atn::ParserATNSimulatorOptions &options);

  ~GSILParser() override;

  std::string getGrammarFileName() const override;

  const antlr4::atn::ATN& getATN() const override;

  const std::vector<std::string>& getRuleNames() const override;

  const antlr4::dfa::Vocabulary& getVocabulary() const override;

  antlr4::atn::SerializedATNView getSerializedATN() const override;


  class LineSepContext;
  class VariableTypeContext;
  class VariableArrayPartContext;
  class SimpleVariableDefinitionContext;
  class PropertyColorValueContext;
  class PropertyDefinitionContext;
  class StatePropertyNameContext;
  class StatePropertyValueContext;
  class StatePropertySetterContext;
  class StateDefinitionContext;
  class MaxVertexCountContext;
  class ThreadsContext;
  class AttributeContext;
  class AttributeDefinitionContext;
  class ShaderInputAccessSpecifierContext;
  class RasterizerStateDefinitionContext;
  class BlendStateDefinitionContext;
  class SamplerDefinitionContext;
  class DepthStencilStateDefinitionContext;
  class TextureDefinitionContext;
  class UavUnderlayingTypeContext;
  class UavDefinitionContext;
  class StateNamesContext;
  class CompileShaderReplacementContext;
  class CompileShaderContext;
  class PassShaderKeyValuePairContext;
  class PassStateKeyValuePairContext;
  class PassKeyValuePairContext;
  class TagValueContext;
  class TagDefinitionContext;
  class PassDefinitionContext;
  class TechniqueDefinitionContext;
  class TechniqueGroupDefinitionContext;
  class StructSpecifierContext;
  class FieldSpecifierContext;
  class StructFieldContext;
  class StructDefinitionContext;
  class CbufferDefinitionContext;
  class ValidInFunctionDefContext;
  class BlockContentContext;
  class BlockContext;
  class FunctionParameterContext;
  class CommaFunctionParameterContext;
  class FunctionParameterListContext;
  class FunctionDefinitionContext;
  class TopLevelDeclarationContext;
  class CompilationUnitContext; 

  class  LineSepContext : public antlr4::ParserRuleContext {
  public:
    LineSepContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Semi();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  LineSepContext* lineSep();

  class  VariableTypeContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *Type = nullptr;
    VariableTypeContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Identifier();
    antlr4::tree::TerminalNode *VectorType();
    antlr4::tree::TerminalNode *Matrix();
    antlr4::tree::TerminalNode *DMatrix();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  VariableTypeContext* variableType();

  class  VariableArrayPartContext : public antlr4::ParserRuleContext {
  public:
    VariableArrayPartContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LeftBracket();
    antlr4::tree::TerminalNode *IntegralLiteral();
    antlr4::tree::TerminalNode *RightBracket();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  VariableArrayPartContext* variableArrayPart();

  class  SimpleVariableDefinitionContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *typemodifierToken = nullptr;
    std::vector<antlr4::Token *> TypeModifiers;
    GSILParser::VariableTypeContext *Type = nullptr;
    antlr4::Token *Name = nullptr;
    GSILParser::VariableArrayPartContext *variableArrayPartContext = nullptr;
    std::vector<VariableArrayPartContext *> ArrayParts;
    SimpleVariableDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    VariableTypeContext *variableType();
    antlr4::tree::TerminalNode *Identifier();
    std::vector<antlr4::tree::TerminalNode *> TypeModifier();
    antlr4::tree::TerminalNode* TypeModifier(size_t i);
    std::vector<VariableArrayPartContext *> variableArrayPart();
    VariableArrayPartContext* variableArrayPart(size_t i);


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  SimpleVariableDefinitionContext* simpleVariableDefinition();

  class  PropertyColorValueContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *Red = nullptr;
    antlr4::Token *Green = nullptr;
    antlr4::Token *Blue = nullptr;
    antlr4::Token *Alpha = nullptr;
    PropertyColorValueContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<antlr4::tree::TerminalNode *> Colon();
    antlr4::tree::TerminalNode* Colon(size_t i);
    std::vector<antlr4::tree::TerminalNode *> IntegralLiteral();
    antlr4::tree::TerminalNode* IntegralLiteral(size_t i);


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  PropertyColorValueContext* propertyColorValue();

  class  PropertyDefinitionContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *EditorName = nullptr;
    PropertyDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Colon();
    antlr4::tree::TerminalNode *PropertySpecifier();
    antlr4::tree::TerminalNode *LeftParen();
    antlr4::tree::TerminalNode *RightParen();
    antlr4::tree::TerminalNode *StringLiteral();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  PropertyDefinitionContext* propertyDefinition();

  class  StatePropertyNameContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *Name = nullptr;
    StatePropertyNameContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Identifier();
    VariableArrayPartContext *variableArrayPart();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  StatePropertyNameContext* statePropertyName();

  class  StatePropertyValueContext : public antlr4::ParserRuleContext {
  public:
    StatePropertyValueContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Identifier();
    antlr4::tree::TerminalNode *IntegralLiteral();
    antlr4::tree::TerminalNode *False();
    antlr4::tree::TerminalNode *True();
    PropertyColorValueContext *propertyColorValue();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  StatePropertyValueContext* statePropertyValue();

  class  StatePropertySetterContext : public antlr4::ParserRuleContext {
  public:
    GSILParser::StatePropertyNameContext *Property = nullptr;
    GSILParser::StatePropertyValueContext *Value = nullptr;
    StatePropertySetterContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Assign();
    LineSepContext *lineSep();
    StatePropertyNameContext *statePropertyName();
    StatePropertyValueContext *statePropertyValue();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  StatePropertySetterContext* statePropertySetter();

  class  StateDefinitionContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *Name = nullptr;
    GSILParser::StatePropertySetterContext *statePropertySetterContext = nullptr;
    std::vector<StatePropertySetterContext *> Properties;
    StateDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LeftBrace();
    antlr4::tree::TerminalNode *RightBrace();
    LineSepContext *lineSep();
    antlr4::tree::TerminalNode *Identifier();
    std::vector<StatePropertySetterContext *> statePropertySetter();
    StatePropertySetterContext* statePropertySetter(size_t i);


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  StateDefinitionContext* stateDefinition();

  class  MaxVertexCountContext : public antlr4::ParserRuleContext {
  public:
    MaxVertexCountContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *MaxVertexCount();
    antlr4::tree::TerminalNode *LeftParen();
    antlr4::tree::TerminalNode *IntegralLiteral();
    antlr4::tree::TerminalNode *RightParen();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  MaxVertexCountContext* maxVertexCount();

  class  ThreadsContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *X = nullptr;
    antlr4::Token *Y = nullptr;
    antlr4::Token *Z = nullptr;
    ThreadsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Threads();
    antlr4::tree::TerminalNode *LeftParen();
    std::vector<antlr4::tree::TerminalNode *> Comma();
    antlr4::tree::TerminalNode* Comma(size_t i);
    antlr4::tree::TerminalNode *RightParen();
    std::vector<antlr4::tree::TerminalNode *> IntegralLiteral();
    antlr4::tree::TerminalNode* IntegralLiteral(size_t i);


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ThreadsContext* threads();

  class  AttributeContext : public antlr4::ParserRuleContext {
  public:
    AttributeContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *EarlyZ();
    MaxVertexCountContext *maxVertexCount();
    ThreadsContext *threads();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  AttributeContext* attribute();

  class  AttributeDefinitionContext : public antlr4::ParserRuleContext {
  public:
    GSILParser::AttributeContext *Attribute = nullptr;
    AttributeDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LeftBracket();
    antlr4::tree::TerminalNode *RightBracket();
    AttributeContext *attribute();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  AttributeDefinitionContext* attributeDefinition();

  class  ShaderInputAccessSpecifierContext : public antlr4::ParserRuleContext {
  public:
    ShaderInputAccessSpecifierContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Hidden();
    antlr4::tree::TerminalNode *EngineSupplied();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ShaderInputAccessSpecifierContext* shaderInputAccessSpecifier();

  class  RasterizerStateDefinitionContext : public antlr4::ParserRuleContext {
  public:
    GSILParser::StateDefinitionContext *Def = nullptr;
    RasterizerStateDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *RasterizerState();
    StateDefinitionContext *stateDefinition();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  RasterizerStateDefinitionContext* rasterizerStateDefinition();

  class  BlendStateDefinitionContext : public antlr4::ParserRuleContext {
  public:
    GSILParser::StateDefinitionContext *Def = nullptr;
    BlendStateDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *BlendState();
    StateDefinitionContext *stateDefinition();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  BlendStateDefinitionContext* blendStateDefinition();

  class  SamplerDefinitionContext : public antlr4::ParserRuleContext {
  public:
    GSILParser::StateDefinitionContext *Def = nullptr;
    SamplerDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Sampler();
    StateDefinitionContext *stateDefinition();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  SamplerDefinitionContext* samplerDefinition();

  class  DepthStencilStateDefinitionContext : public antlr4::ParserRuleContext {
  public:
    GSILParser::StateDefinitionContext *Def = nullptr;
    DepthStencilStateDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *DepthStencilState();
    StateDefinitionContext *stateDefinition();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  DepthStencilStateDefinitionContext* depthStencilStateDefinition();

  class  TextureDefinitionContext : public antlr4::ParserRuleContext {
  public:
    GSILParser::ShaderInputAccessSpecifierContext *InputSpecifier = nullptr;
    antlr4::Token *Name = nullptr;
    antlr4::Token *SamplerName = nullptr;
    TextureDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *TextureType();
    antlr4::tree::TerminalNode *LeftParen();
    antlr4::tree::TerminalNode *RightParen();
    LineSepContext *lineSep();
    std::vector<antlr4::tree::TerminalNode *> Identifier();
    antlr4::tree::TerminalNode* Identifier(size_t i);
    PropertyDefinitionContext *propertyDefinition();
    ShaderInputAccessSpecifierContext *shaderInputAccessSpecifier();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  TextureDefinitionContext* textureDefinition();

  class  UavUnderlayingTypeContext : public antlr4::ParserRuleContext {
  public:
    UavUnderlayingTypeContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *VectorType();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  UavUnderlayingTypeContext* uavUnderlayingType();

  class  UavDefinitionContext : public antlr4::ParserRuleContext {
  public:
    GSILParser::ShaderInputAccessSpecifierContext *InputSpecifier = nullptr;
    GSILParser::UavUnderlayingTypeContext *Type = nullptr;
    antlr4::Token *Name = nullptr;
    UavDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *UAVType();
    antlr4::tree::TerminalNode *Less();
    antlr4::tree::TerminalNode *Greater();
    LineSepContext *lineSep();
    UavUnderlayingTypeContext *uavUnderlayingType();
    antlr4::tree::TerminalNode *Identifier();
    PropertyDefinitionContext *propertyDefinition();
    ShaderInputAccessSpecifierContext *shaderInputAccessSpecifier();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  UavDefinitionContext* uavDefinition();

  class  StateNamesContext : public antlr4::ParserRuleContext {
  public:
    StateNamesContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *BlendState();
    antlr4::tree::TerminalNode *RasterizerState();
    antlr4::tree::TerminalNode *DepthStencilState();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  StateNamesContext* stateNames();

  class  CompileShaderReplacementContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *Replacement = nullptr;
    CompileShaderReplacementContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Comma();
    antlr4::tree::TerminalNode *IntegralLiteral();
    antlr4::tree::TerminalNode *FloatLiteral();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CompileShaderReplacementContext* compileShaderReplacement();

  class  CompileShaderContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *Version = nullptr;
    antlr4::Token *MainFunc = nullptr;
    GSILParser::CompileShaderReplacementContext *compileShaderReplacementContext = nullptr;
    std::vector<CompileShaderReplacementContext *> Replacements;
    CompileShaderContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *CompileShader();
    antlr4::tree::TerminalNode *LeftParen();
    antlr4::tree::TerminalNode *Comma();
    antlr4::tree::TerminalNode *RightParen();
    antlr4::tree::TerminalNode *FloatLiteral();
    antlr4::tree::TerminalNode *Identifier();
    std::vector<CompileShaderReplacementContext *> compileShaderReplacement();
    CompileShaderReplacementContext* compileShaderReplacement(size_t i);


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CompileShaderContext* compileShader();

  class  PassShaderKeyValuePairContext : public antlr4::ParserRuleContext {
  public:
    PassShaderKeyValuePairContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *ShaderName();
    antlr4::tree::TerminalNode *Assign();
    CompileShaderContext *compileShader();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  PassShaderKeyValuePairContext* passShaderKeyValuePair();

  class  PassStateKeyValuePairContext : public antlr4::ParserRuleContext {
  public:
    PassStateKeyValuePairContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    StateNamesContext *stateNames();
    antlr4::tree::TerminalNode *Assign();
    antlr4::tree::TerminalNode *Identifier();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  PassStateKeyValuePairContext* passStateKeyValuePair();

  class  PassKeyValuePairContext : public antlr4::ParserRuleContext {
  public:
    PassKeyValuePairContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    LineSepContext *lineSep();
    PassShaderKeyValuePairContext *passShaderKeyValuePair();
    PassStateKeyValuePairContext *passStateKeyValuePair();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  PassKeyValuePairContext* passKeyValuePair();

  class  TagValueContext : public antlr4::ParserRuleContext {
  public:
    TagValueContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Identifier();
    antlr4::tree::TerminalNode *IntegralLiteral();
    antlr4::tree::TerminalNode *FloatLiteral();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  TagValueContext* tagValue();

  class  TagDefinitionContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *Key = nullptr;
    GSILParser::TagValueContext *Value = nullptr;
    TagDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Tag();
    antlr4::tree::TerminalNode *LeftParen();
    antlr4::tree::TerminalNode *Colon();
    antlr4::tree::TerminalNode *RightParen();
    LineSepContext *lineSep();
    antlr4::tree::TerminalNode *Identifier();
    TagValueContext *tagValue();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  TagDefinitionContext* tagDefinition();

  class  PassDefinitionContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *Id = nullptr;
    GSILParser::PassKeyValuePairContext *passKeyValuePairContext = nullptr;
    std::vector<PassKeyValuePairContext *> Values;
    PassDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Pass();
    antlr4::tree::TerminalNode *LeftBrace();
    antlr4::tree::TerminalNode *RightBrace();
    LineSepContext *lineSep();
    antlr4::tree::TerminalNode *IntegralLiteral();
    std::vector<PassKeyValuePairContext *> passKeyValuePair();
    PassKeyValuePairContext* passKeyValuePair(size_t i);


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  PassDefinitionContext* passDefinition();

  class  TechniqueDefinitionContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *Name = nullptr;
    GSILParser::TagDefinitionContext *tagDefinitionContext = nullptr;
    std::vector<TagDefinitionContext *> Tags;
    GSILParser::PassDefinitionContext *passDefinitionContext = nullptr;
    std::vector<PassDefinitionContext *> Passes;
    TechniqueDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Technique();
    antlr4::tree::TerminalNode *LeftBrace();
    antlr4::tree::TerminalNode *RightBrace();
    LineSepContext *lineSep();
    antlr4::tree::TerminalNode *Identifier();
    std::vector<TagDefinitionContext *> tagDefinition();
    TagDefinitionContext* tagDefinition(size_t i);
    std::vector<PassDefinitionContext *> passDefinition();
    PassDefinitionContext* passDefinition(size_t i);


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  TechniqueDefinitionContext* techniqueDefinition();

  class  TechniqueGroupDefinitionContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *Name = nullptr;
    GSILParser::TechniqueDefinitionContext *techniqueDefinitionContext = nullptr;
    std::vector<TechniqueDefinitionContext *> Techniques;
    TechniqueGroupDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *TechniqueGroup();
    antlr4::tree::TerminalNode *LeftBrace();
    antlr4::tree::TerminalNode *RightBrace();
    LineSepContext *lineSep();
    antlr4::tree::TerminalNode *Identifier();
    std::vector<TechniqueDefinitionContext *> techniqueDefinition();
    TechniqueDefinitionContext* techniqueDefinition(size_t i);


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  TechniqueGroupDefinitionContext* techniqueGroupDefinition();

  class  StructSpecifierContext : public antlr4::ParserRuleContext {
  public:
    StructSpecifierContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *InOut();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  StructSpecifierContext* structSpecifier();

  class  FieldSpecifierContext : public antlr4::ParserRuleContext {
  public:
    FieldSpecifierContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *SVSpecifier();
    antlr4::tree::TerminalNode *Instanced();
    ShaderInputAccessSpecifierContext *shaderInputAccessSpecifier();
    antlr4::tree::TerminalNode *InterpolationModeSpecifier();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FieldSpecifierContext* fieldSpecifier();

  class  StructFieldContext : public antlr4::ParserRuleContext {
  public:
    GSILParser::FieldSpecifierContext *fieldSpecifierContext = nullptr;
    std::vector<FieldSpecifierContext *> InputSpecifier;
    StructFieldContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    SimpleVariableDefinitionContext *simpleVariableDefinition();
    LineSepContext *lineSep();
    PropertyDefinitionContext *propertyDefinition();
    std::vector<FieldSpecifierContext *> fieldSpecifier();
    FieldSpecifierContext* fieldSpecifier(size_t i);


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  StructFieldContext* structField();

  class  StructDefinitionContext : public antlr4::ParserRuleContext {
  public:
    GSILParser::StructSpecifierContext *Specifiers = nullptr;
    antlr4::Token *Name = nullptr;
    GSILParser::StructFieldContext *structFieldContext = nullptr;
    std::vector<StructFieldContext *> Fields;
    StructDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Struct();
    antlr4::tree::TerminalNode *LeftBrace();
    antlr4::tree::TerminalNode *RightBrace();
    LineSepContext *lineSep();
    antlr4::tree::TerminalNode *Identifier();
    StructSpecifierContext *structSpecifier();
    std::vector<StructFieldContext *> structField();
    StructFieldContext* structField(size_t i);


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  StructDefinitionContext* structDefinition();

  class  CbufferDefinitionContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *Name = nullptr;
    GSILParser::StructFieldContext *structFieldContext = nullptr;
    std::vector<StructFieldContext *> Fields;
    CbufferDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *CBuffer();
    antlr4::tree::TerminalNode *LeftBrace();
    antlr4::tree::TerminalNode *RightBrace();
    LineSepContext *lineSep();
    antlr4::tree::TerminalNode *Identifier();
    std::vector<StructFieldContext *> structField();
    StructFieldContext* structField(size_t i);


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CbufferDefinitionContext* cbufferDefinition();

  class  ValidInFunctionDefContext : public antlr4::ParserRuleContext {
  public:
    ValidInFunctionDefContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *RightBrace();
    antlr4::tree::TerminalNode *CBuffer();
    antlr4::tree::TerminalNode *RasterizerState();
    antlr4::tree::TerminalNode *BlendState();
    antlr4::tree::TerminalNode *DepthStencilState();
    antlr4::tree::TerminalNode *Sampler();
    antlr4::tree::TerminalNode *CompileShader();
    antlr4::tree::TerminalNode *Struct();
    antlr4::tree::TerminalNode *Tag();
    antlr4::tree::TerminalNode *Pass();
    antlr4::tree::TerminalNode *UAVType();
    antlr4::tree::TerminalNode *TextureType();
    antlr4::tree::TerminalNode *Hidden();
    antlr4::tree::TerminalNode *EngineSupplied();
    antlr4::tree::TerminalNode *Technique();
    antlr4::tree::TerminalNode *TechniqueGroup();
    antlr4::tree::TerminalNode *Uniform();
    antlr4::tree::TerminalNode *ShaderName();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  ValidInFunctionDefContext* validInFunctionDef();

  class  BlockContentContext : public antlr4::ParserRuleContext {
  public:
    BlockContentContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<BlockContext *> block();
    BlockContext* block(size_t i);
    std::vector<ValidInFunctionDefContext *> validInFunctionDef();
    ValidInFunctionDefContext* validInFunctionDef(size_t i);


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  BlockContentContext* blockContent();

  class  BlockContext : public antlr4::ParserRuleContext {
  public:
    GSILParser::BlockContentContext *Content = nullptr;
    BlockContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LeftBrace();
    antlr4::tree::TerminalNode *RightBrace();
    BlockContentContext *blockContent();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  BlockContext* block();

  class  FunctionParameterContext : public antlr4::ParserRuleContext {
  public:
    antlr4::Token *IsUniform = nullptr;
    GSILParser::VariableTypeContext *Type = nullptr;
    antlr4::Token *Name = nullptr;
    GSILParser::VariableArrayPartContext *variableArrayPartContext = nullptr;
    std::vector<VariableArrayPartContext *> ArrayPart;
    FunctionParameterContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    VariableTypeContext *variableType();
    antlr4::tree::TerminalNode *Identifier();
    antlr4::tree::TerminalNode *Uniform();
    std::vector<VariableArrayPartContext *> variableArrayPart();
    VariableArrayPartContext* variableArrayPart(size_t i);


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FunctionParameterContext* functionParameter();

  class  CommaFunctionParameterContext : public antlr4::ParserRuleContext {
  public:
    GSILParser::FunctionParameterContext *Parameter = nullptr;
    CommaFunctionParameterContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *Comma();
    FunctionParameterContext *functionParameter();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CommaFunctionParameterContext* commaFunctionParameter();

  class  FunctionParameterListContext : public antlr4::ParserRuleContext {
  public:
    GSILParser::FunctionParameterContext *FirstParameter = nullptr;
    GSILParser::CommaFunctionParameterContext *commaFunctionParameterContext = nullptr;
    std::vector<CommaFunctionParameterContext *> MoreParameters;
    FunctionParameterListContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    FunctionParameterContext *functionParameter();
    std::vector<CommaFunctionParameterContext *> commaFunctionParameter();
    CommaFunctionParameterContext* commaFunctionParameter(size_t i);


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FunctionParameterListContext* functionParameterList();

  class  FunctionDefinitionContext : public antlr4::ParserRuleContext {
  public:
    GSILParser::VariableTypeContext *ReturnType = nullptr;
    GSILParser::AttributeDefinitionContext *attributeDefinitionContext = nullptr;
    std::vector<AttributeDefinitionContext *> Attributes;
    antlr4::Token *Name = nullptr;
    GSILParser::FunctionParameterListContext *Parameters = nullptr;
    GSILParser::BlockContext *Code = nullptr;
    FunctionDefinitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *LeftParen();
    antlr4::tree::TerminalNode *RightParen();
    VariableTypeContext *variableType();
    antlr4::tree::TerminalNode *Identifier();
    BlockContext *block();
    std::vector<AttributeDefinitionContext *> attributeDefinition();
    AttributeDefinitionContext* attributeDefinition(size_t i);
    FunctionParameterListContext *functionParameterList();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  FunctionDefinitionContext* functionDefinition();

  class  TopLevelDeclarationContext : public antlr4::ParserRuleContext {
  public:
    TopLevelDeclarationContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    RasterizerStateDefinitionContext *rasterizerStateDefinition();
    BlendStateDefinitionContext *blendStateDefinition();
    SamplerDefinitionContext *samplerDefinition();
    DepthStencilStateDefinitionContext *depthStencilStateDefinition();
    TextureDefinitionContext *textureDefinition();
    UavDefinitionContext *uavDefinition();
    TechniqueGroupDefinitionContext *techniqueGroupDefinition();
    TechniqueDefinitionContext *techniqueDefinition();
    StructDefinitionContext *structDefinition();
    CbufferDefinitionContext *cbufferDefinition();
    FunctionDefinitionContext *functionDefinition();


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  TopLevelDeclarationContext* topLevelDeclaration();

  class  CompilationUnitContext : public antlr4::ParserRuleContext {
  public:
    GSILParser::TopLevelDeclarationContext *topLevelDeclarationContext = nullptr;
    std::vector<TopLevelDeclarationContext *> Declarations;
    antlr4::Token *EndOfFileToken = nullptr;
    CompilationUnitContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *EOF();
    std::vector<TopLevelDeclarationContext *> topLevelDeclaration();
    TopLevelDeclarationContext* topLevelDeclaration(size_t i);


    virtual std::any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CompilationUnitContext* compilationUnit();


  // By default the static state used to implement the parser is lazily initialized during the first
  // call to the constructor. You can call this function if you wish to initialize the static state
  // ahead of time.
  static void initialize();

private:
};

}  // namespace GSIL
