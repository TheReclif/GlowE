
// Generated from F:/Dokumenty/GlowE/antlr4/GSILParser.g4 by ANTLR 4.13.0


#include "GSILParserVisitor.h"

#include "GSILParser.h"


using namespace antlrcpp;
using namespace GSIL;

using namespace antlr4;

namespace {

struct GSILParserStaticData final {
  GSILParserStaticData(std::vector<std::string> ruleNames,
                        std::vector<std::string> literalNames,
                        std::vector<std::string> symbolicNames)
      : ruleNames(std::move(ruleNames)), literalNames(std::move(literalNames)),
        symbolicNames(std::move(symbolicNames)),
        vocabulary(this->literalNames, this->symbolicNames) {}

  GSILParserStaticData(const GSILParserStaticData&) = delete;
  GSILParserStaticData(GSILParserStaticData&&) = delete;
  GSILParserStaticData& operator=(const GSILParserStaticData&) = delete;
  GSILParserStaticData& operator=(GSILParserStaticData&&) = delete;

  std::vector<antlr4::dfa::DFA> decisionToDFA;
  antlr4::atn::PredictionContextCache sharedContextCache;
  const std::vector<std::string> ruleNames;
  const std::vector<std::string> literalNames;
  const std::vector<std::string> symbolicNames;
  const antlr4::dfa::Vocabulary vocabulary;
  antlr4::atn::SerializedATNView serializedATN;
  std::unique_ptr<antlr4::atn::ATN> atn;
};

::antlr4::internal::OnceFlag gsilparserParserOnceFlag;
#if ANTLR4_USE_THREAD_LOCAL_CACHE
static thread_local
#endif
GSILParserStaticData *gsilparserParserStaticData = nullptr;

void gsilparserParserInitialize() {
#if ANTLR4_USE_THREAD_LOCAL_CACHE
  if (gsilparserParserStaticData != nullptr) {
    return;
  }
#else
  assert(gsilparserParserStaticData == nullptr);
#endif
  auto staticData = std::make_unique<GSILParserStaticData>(
    std::vector<std::string>{
      "lineSep", "variableType", "variableArrayPart", "simpleVariableDefinition", 
      "propertyColorValue", "propertyDefinition", "statePropertyName", "statePropertyValue", 
      "statePropertySetter", "stateDefinition", "maxVertexCount", "threads", 
      "attribute", "attributeDefinition", "shaderInputAccessSpecifier", 
      "rasterizerStateDefinition", "blendStateDefinition", "samplerDefinition", 
      "depthStencilStateDefinition", "textureDefinition", "uavUnderlayingType", 
      "uavDefinition", "stateNames", "compileShaderReplacement", "compileShader", 
      "passShaderKeyValuePair", "passStateKeyValuePair", "passKeyValuePair", 
      "tagValue", "tagDefinition", "passDefinition", "techniqueDefinition", 
      "techniqueGroupDefinition", "structSpecifier", "fieldSpecifier", "structField", 
      "structDefinition", "cbufferDefinition", "validInFunctionDef", "blockContent", 
      "block", "functionParameter", "commaFunctionParameter", "functionParameterList", 
      "functionDefinition", "topLevelDeclaration", "compilationUnit"
    },
    std::vector<std::string>{
      "", "", "", "", "", "'CBuffer'", "'Struct'", "'InOut'", "'Out'", "", 
      "'SV'", "'EngineSupplied'", "'Hidden'", "'Sampler'", "", "", "", "'Property'", 
      "'Instanced'", "", "", "'TechniqueGroup'", "'Technique'", "'Pass'", 
      "'CompileShader'", "'Tag'", "'Threads'", "'EarlyZ'", "'MaxVertexCount'", 
      "", "'Uniform'", "", "", "", "", "'false'", "'true'", "'break'", "'continue'", 
      "'discard'", "'do'", "'if'", "'switch'", "'while'", "'case'", "'default'", 
      "'for'", "'else'", "'return'", "", "", "'('", "')'", "'['", "']'", 
      "'{'", "'}'", "'<'", "'<='", "'>'", "'>='", "'<<'", "'>>'", "'+'", 
      "'++'", "'-'", "'--'", "'*'", "'/'", "'%'", "'&'", "'|'", "'&&'", 
      "'||'", "'^'", "'!'", "'~'", "'\\u003F'", "':'", "'::'", "';'", "','", 
      "'='", "'*='", "'/='", "'%='", "'+='", "'-='", "'<<='", "'>>='", "'&='", 
      "'^='", "'|='", "'=='", "'!='", "'.'"
    },
    std::vector<std::string>{
      "", "WhiteSpace", "MultiLineComment", "SingleLineComment", "CodeSnippetPrefix", 
      "CBuffer", "Struct", "InOut", "Out", "GeometryShaderInputSpecifier", 
      "SVSpecifier", "EngineSupplied", "Hidden", "Sampler", "BlendState", 
      "DepthStencilState", "RasterizerState", "PropertySpecifier", "Instanced", 
      "TextureType", "UAVType", "TechniqueGroup", "Technique", "Pass", "CompileShader", 
      "Tag", "Threads", "EarlyZ", "MaxVertexCount", "InterpolationModeSpecifier", 
      "Uniform", "ShaderName", "VectorType", "Matrix", "DMatrix", "False", 
      "True", "Break", "Continue", "Discard", "Do", "If", "Switch", "While", 
      "Case", "Default", "For", "Else", "Return", "StorageClass", "TypeModifier", 
      "LeftParen", "RightParen", "LeftBracket", "RightBracket", "LeftBrace", 
      "RightBrace", "Less", "LessEqual", "Greater", "GreaterEqual", "LeftShift", 
      "RightShift", "Plus", "PlusPlus", "Minus", "MinusMinus", "Star", "Div", 
      "Mod", "And", "Or", "AndAnd", "OrOr", "Caret", "Not", "Tilde", "Question", 
      "Colon", "ColonColon", "Semi", "Comma", "Assign", "StarAssign", "DivAssign", 
      "ModAssign", "PlusAssign", "MinusAssign", "LeftShiftAssign", "RightShiftAssign", 
      "AndAssign", "XorAssign", "OrAssign", "Equal", "NotEqual", "Dot", 
      "IntegralLiteral", "FloatLiteral", "StringLiteral", "Identifier"
    }
  );
  static const int32_t serializedATNSegment[] = {
  	4,1,99,424,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,6,2,
  	7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,2,12,7,12,2,13,7,13,2,14,7,
  	14,2,15,7,15,2,16,7,16,2,17,7,17,2,18,7,18,2,19,7,19,2,20,7,20,2,21,7,
  	21,2,22,7,22,2,23,7,23,2,24,7,24,2,25,7,25,2,26,7,26,2,27,7,27,2,28,7,
  	28,2,29,7,29,2,30,7,30,2,31,7,31,2,32,7,32,2,33,7,33,2,34,7,34,2,35,7,
  	35,2,36,7,36,2,37,7,37,2,38,7,38,2,39,7,39,2,40,7,40,2,41,7,41,2,42,7,
  	42,2,43,7,43,2,44,7,44,2,45,7,45,2,46,7,46,1,0,1,0,1,1,1,1,1,2,1,2,1,
  	2,1,2,1,3,5,3,104,8,3,10,3,12,3,107,9,3,1,3,1,3,1,3,5,3,112,8,3,10,3,
  	12,3,115,9,3,1,4,1,4,1,4,1,4,1,4,1,4,1,4,3,4,124,8,4,1,5,1,5,1,5,1,5,
  	1,5,1,5,1,6,1,6,3,6,134,8,6,1,7,1,7,1,7,1,7,1,7,3,7,141,8,7,1,8,1,8,1,
  	8,1,8,1,8,1,9,1,9,1,9,5,9,151,8,9,10,9,12,9,154,9,9,1,9,1,9,1,9,1,10,
  	1,10,1,10,1,10,1,10,1,11,1,11,1,11,1,11,1,11,1,11,1,11,1,11,1,11,1,12,
  	1,12,1,12,3,12,176,8,12,1,13,1,13,1,13,1,13,1,14,1,14,1,15,1,15,1,15,
  	1,16,1,16,1,16,1,17,1,17,1,17,1,18,1,18,1,18,1,19,3,19,197,8,19,1,19,
  	1,19,1,19,1,19,1,19,1,19,3,19,205,8,19,1,19,1,19,1,20,1,20,1,21,3,21,
  	212,8,21,1,21,1,21,1,21,1,21,1,21,1,21,3,21,220,8,21,1,21,1,21,1,22,1,
  	22,1,23,1,23,1,23,1,24,1,24,1,24,1,24,1,24,1,24,5,24,235,8,24,10,24,12,
  	24,238,9,24,1,24,1,24,1,25,1,25,1,25,1,25,1,26,1,26,1,26,1,26,1,27,1,
  	27,3,27,252,8,27,1,27,1,27,1,28,1,28,1,29,1,29,1,29,1,29,1,29,1,29,1,
  	29,1,29,1,30,1,30,1,30,1,30,5,30,270,8,30,10,30,12,30,273,9,30,1,30,1,
  	30,1,30,1,31,1,31,1,31,1,31,5,31,282,8,31,10,31,12,31,285,9,31,1,31,5,
  	31,288,8,31,10,31,12,31,291,9,31,1,31,1,31,1,31,1,32,1,32,1,32,1,32,4,
  	32,300,8,32,11,32,12,32,301,1,32,1,32,1,32,1,33,1,33,1,34,1,34,1,34,1,
  	34,3,34,313,8,34,1,35,5,35,316,8,35,10,35,12,35,319,9,35,1,35,1,35,3,
  	35,323,8,35,1,35,1,35,1,36,1,36,3,36,329,8,36,1,36,1,36,1,36,5,36,334,
  	8,36,10,36,12,36,337,9,36,1,36,1,36,1,36,1,37,1,37,1,37,1,37,5,37,346,
  	8,37,10,37,12,37,349,9,37,1,37,1,37,1,37,1,38,1,38,1,39,1,39,5,39,358,
  	8,39,10,39,12,39,361,9,39,1,40,1,40,1,40,1,40,1,41,3,41,368,8,41,1,41,
  	1,41,1,41,5,41,373,8,41,10,41,12,41,376,9,41,1,42,1,42,1,42,1,43,1,43,
  	5,43,383,8,43,10,43,12,43,386,9,43,1,44,1,44,5,44,390,8,44,10,44,12,44,
  	393,9,44,1,44,1,44,1,44,3,44,398,8,44,1,44,1,44,1,44,1,45,1,45,1,45,1,
  	45,1,45,1,45,1,45,1,45,1,45,1,45,1,45,3,45,414,8,45,1,46,5,46,417,8,46,
  	10,46,12,46,420,9,46,1,46,1,46,1,46,0,0,47,0,2,4,6,8,10,12,14,16,18,20,
  	22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,64,66,
  	68,70,72,74,76,78,80,82,84,86,88,90,92,0,6,2,0,32,34,99,99,1,0,11,12,
  	1,0,14,16,1,0,96,97,2,0,96,97,99,99,5,0,5,6,11,16,19,25,30,31,56,56,423,
  	0,94,1,0,0,0,2,96,1,0,0,0,4,98,1,0,0,0,6,105,1,0,0,0,8,116,1,0,0,0,10,
  	125,1,0,0,0,12,131,1,0,0,0,14,140,1,0,0,0,16,142,1,0,0,0,18,147,1,0,0,
  	0,20,158,1,0,0,0,22,163,1,0,0,0,24,175,1,0,0,0,26,177,1,0,0,0,28,181,
  	1,0,0,0,30,183,1,0,0,0,32,186,1,0,0,0,34,189,1,0,0,0,36,192,1,0,0,0,38,
  	196,1,0,0,0,40,208,1,0,0,0,42,211,1,0,0,0,44,223,1,0,0,0,46,225,1,0,0,
  	0,48,228,1,0,0,0,50,241,1,0,0,0,52,245,1,0,0,0,54,251,1,0,0,0,56,255,
  	1,0,0,0,58,257,1,0,0,0,60,265,1,0,0,0,62,277,1,0,0,0,64,295,1,0,0,0,66,
  	306,1,0,0,0,68,312,1,0,0,0,70,317,1,0,0,0,72,326,1,0,0,0,74,341,1,0,0,
  	0,76,353,1,0,0,0,78,359,1,0,0,0,80,362,1,0,0,0,82,367,1,0,0,0,84,377,
  	1,0,0,0,86,380,1,0,0,0,88,387,1,0,0,0,90,413,1,0,0,0,92,418,1,0,0,0,94,
  	95,5,80,0,0,95,1,1,0,0,0,96,97,7,0,0,0,97,3,1,0,0,0,98,99,5,53,0,0,99,
  	100,5,96,0,0,100,101,5,54,0,0,101,5,1,0,0,0,102,104,5,50,0,0,103,102,
  	1,0,0,0,104,107,1,0,0,0,105,103,1,0,0,0,105,106,1,0,0,0,106,108,1,0,0,
  	0,107,105,1,0,0,0,108,109,3,2,1,0,109,113,5,99,0,0,110,112,3,4,2,0,111,
  	110,1,0,0,0,112,115,1,0,0,0,113,111,1,0,0,0,113,114,1,0,0,0,114,7,1,0,
  	0,0,115,113,1,0,0,0,116,117,5,96,0,0,117,118,5,78,0,0,118,119,5,96,0,
  	0,119,120,5,78,0,0,120,123,5,96,0,0,121,122,5,78,0,0,122,124,5,96,0,0,
  	123,121,1,0,0,0,123,124,1,0,0,0,124,9,1,0,0,0,125,126,5,78,0,0,126,127,
  	5,17,0,0,127,128,5,51,0,0,128,129,5,98,0,0,129,130,5,52,0,0,130,11,1,
  	0,0,0,131,133,5,99,0,0,132,134,3,4,2,0,133,132,1,0,0,0,133,134,1,0,0,
  	0,134,13,1,0,0,0,135,141,5,99,0,0,136,141,5,96,0,0,137,141,5,35,0,0,138,
  	141,5,36,0,0,139,141,3,8,4,0,140,135,1,0,0,0,140,136,1,0,0,0,140,137,
  	1,0,0,0,140,138,1,0,0,0,140,139,1,0,0,0,141,15,1,0,0,0,142,143,3,12,6,
  	0,143,144,5,82,0,0,144,145,3,14,7,0,145,146,3,0,0,0,146,17,1,0,0,0,147,
  	148,5,99,0,0,148,152,5,55,0,0,149,151,3,16,8,0,150,149,1,0,0,0,151,154,
  	1,0,0,0,152,150,1,0,0,0,152,153,1,0,0,0,153,155,1,0,0,0,154,152,1,0,0,
  	0,155,156,5,56,0,0,156,157,3,0,0,0,157,19,1,0,0,0,158,159,5,28,0,0,159,
  	160,5,51,0,0,160,161,5,96,0,0,161,162,5,52,0,0,162,21,1,0,0,0,163,164,
  	5,26,0,0,164,165,5,51,0,0,165,166,5,96,0,0,166,167,5,81,0,0,167,168,5,
  	96,0,0,168,169,5,81,0,0,169,170,5,96,0,0,170,171,5,52,0,0,171,23,1,0,
  	0,0,172,176,5,27,0,0,173,176,3,20,10,0,174,176,3,22,11,0,175,172,1,0,
  	0,0,175,173,1,0,0,0,175,174,1,0,0,0,176,25,1,0,0,0,177,178,5,53,0,0,178,
  	179,3,24,12,0,179,180,5,54,0,0,180,27,1,0,0,0,181,182,7,1,0,0,182,29,
  	1,0,0,0,183,184,5,16,0,0,184,185,3,18,9,0,185,31,1,0,0,0,186,187,5,14,
  	0,0,187,188,3,18,9,0,188,33,1,0,0,0,189,190,5,13,0,0,190,191,3,18,9,0,
  	191,35,1,0,0,0,192,193,5,15,0,0,193,194,3,18,9,0,194,37,1,0,0,0,195,197,
  	3,28,14,0,196,195,1,0,0,0,196,197,1,0,0,0,197,198,1,0,0,0,198,199,5,19,
  	0,0,199,200,5,99,0,0,200,201,5,51,0,0,201,202,5,99,0,0,202,204,5,52,0,
  	0,203,205,3,10,5,0,204,203,1,0,0,0,204,205,1,0,0,0,205,206,1,0,0,0,206,
  	207,3,0,0,0,207,39,1,0,0,0,208,209,5,32,0,0,209,41,1,0,0,0,210,212,3,
  	28,14,0,211,210,1,0,0,0,211,212,1,0,0,0,212,213,1,0,0,0,213,214,5,20,
  	0,0,214,215,5,57,0,0,215,216,3,40,20,0,216,217,5,59,0,0,217,219,5,99,
  	0,0,218,220,3,10,5,0,219,218,1,0,0,0,219,220,1,0,0,0,220,221,1,0,0,0,
  	221,222,3,0,0,0,222,43,1,0,0,0,223,224,7,2,0,0,224,45,1,0,0,0,225,226,
  	5,81,0,0,226,227,7,3,0,0,227,47,1,0,0,0,228,229,5,24,0,0,229,230,5,51,
  	0,0,230,231,5,97,0,0,231,232,5,81,0,0,232,236,5,99,0,0,233,235,3,46,23,
  	0,234,233,1,0,0,0,235,238,1,0,0,0,236,234,1,0,0,0,236,237,1,0,0,0,237,
  	239,1,0,0,0,238,236,1,0,0,0,239,240,5,52,0,0,240,49,1,0,0,0,241,242,5,
  	31,0,0,242,243,5,82,0,0,243,244,3,48,24,0,244,51,1,0,0,0,245,246,3,44,
  	22,0,246,247,5,82,0,0,247,248,5,99,0,0,248,53,1,0,0,0,249,252,3,50,25,
  	0,250,252,3,52,26,0,251,249,1,0,0,0,251,250,1,0,0,0,252,253,1,0,0,0,253,
  	254,3,0,0,0,254,55,1,0,0,0,255,256,7,4,0,0,256,57,1,0,0,0,257,258,5,25,
  	0,0,258,259,5,51,0,0,259,260,5,99,0,0,260,261,5,78,0,0,261,262,3,56,28,
  	0,262,263,5,52,0,0,263,264,3,0,0,0,264,59,1,0,0,0,265,266,5,23,0,0,266,
  	267,5,96,0,0,267,271,5,55,0,0,268,270,3,54,27,0,269,268,1,0,0,0,270,273,
  	1,0,0,0,271,269,1,0,0,0,271,272,1,0,0,0,272,274,1,0,0,0,273,271,1,0,0,
  	0,274,275,5,56,0,0,275,276,3,0,0,0,276,61,1,0,0,0,277,278,5,22,0,0,278,
  	279,5,99,0,0,279,283,5,55,0,0,280,282,3,58,29,0,281,280,1,0,0,0,282,285,
  	1,0,0,0,283,281,1,0,0,0,283,284,1,0,0,0,284,289,1,0,0,0,285,283,1,0,0,
  	0,286,288,3,60,30,0,287,286,1,0,0,0,288,291,1,0,0,0,289,287,1,0,0,0,289,
  	290,1,0,0,0,290,292,1,0,0,0,291,289,1,0,0,0,292,293,5,56,0,0,293,294,
  	3,0,0,0,294,63,1,0,0,0,295,296,5,21,0,0,296,297,5,99,0,0,297,299,5,55,
  	0,0,298,300,3,62,31,0,299,298,1,0,0,0,300,301,1,0,0,0,301,299,1,0,0,0,
  	301,302,1,0,0,0,302,303,1,0,0,0,303,304,5,56,0,0,304,305,3,0,0,0,305,
  	65,1,0,0,0,306,307,5,7,0,0,307,67,1,0,0,0,308,313,5,10,0,0,309,313,5,
  	18,0,0,310,313,3,28,14,0,311,313,5,29,0,0,312,308,1,0,0,0,312,309,1,0,
  	0,0,312,310,1,0,0,0,312,311,1,0,0,0,313,69,1,0,0,0,314,316,3,68,34,0,
  	315,314,1,0,0,0,316,319,1,0,0,0,317,315,1,0,0,0,317,318,1,0,0,0,318,320,
  	1,0,0,0,319,317,1,0,0,0,320,322,3,6,3,0,321,323,3,10,5,0,322,321,1,0,
  	0,0,322,323,1,0,0,0,323,324,1,0,0,0,324,325,3,0,0,0,325,71,1,0,0,0,326,
  	328,5,6,0,0,327,329,3,66,33,0,328,327,1,0,0,0,328,329,1,0,0,0,329,330,
  	1,0,0,0,330,331,5,99,0,0,331,335,5,55,0,0,332,334,3,70,35,0,333,332,1,
  	0,0,0,334,337,1,0,0,0,335,333,1,0,0,0,335,336,1,0,0,0,336,338,1,0,0,0,
  	337,335,1,0,0,0,338,339,5,56,0,0,339,340,3,0,0,0,340,73,1,0,0,0,341,342,
  	5,5,0,0,342,343,5,99,0,0,343,347,5,55,0,0,344,346,3,70,35,0,345,344,1,
  	0,0,0,346,349,1,0,0,0,347,345,1,0,0,0,347,348,1,0,0,0,348,350,1,0,0,0,
  	349,347,1,0,0,0,350,351,5,56,0,0,351,352,3,0,0,0,352,75,1,0,0,0,353,354,
  	8,5,0,0,354,77,1,0,0,0,355,358,3,80,40,0,356,358,3,76,38,0,357,355,1,
  	0,0,0,357,356,1,0,0,0,358,361,1,0,0,0,359,357,1,0,0,0,359,360,1,0,0,0,
  	360,79,1,0,0,0,361,359,1,0,0,0,362,363,5,55,0,0,363,364,3,78,39,0,364,
  	365,5,56,0,0,365,81,1,0,0,0,366,368,5,30,0,0,367,366,1,0,0,0,367,368,
  	1,0,0,0,368,369,1,0,0,0,369,370,3,2,1,0,370,374,5,99,0,0,371,373,3,4,
  	2,0,372,371,1,0,0,0,373,376,1,0,0,0,374,372,1,0,0,0,374,375,1,0,0,0,375,
  	83,1,0,0,0,376,374,1,0,0,0,377,378,5,81,0,0,378,379,3,82,41,0,379,85,
  	1,0,0,0,380,384,3,82,41,0,381,383,3,84,42,0,382,381,1,0,0,0,383,386,1,
  	0,0,0,384,382,1,0,0,0,384,385,1,0,0,0,385,87,1,0,0,0,386,384,1,0,0,0,
  	387,391,3,2,1,0,388,390,3,26,13,0,389,388,1,0,0,0,390,393,1,0,0,0,391,
  	389,1,0,0,0,391,392,1,0,0,0,392,394,1,0,0,0,393,391,1,0,0,0,394,395,5,
  	99,0,0,395,397,5,51,0,0,396,398,3,86,43,0,397,396,1,0,0,0,397,398,1,0,
  	0,0,398,399,1,0,0,0,399,400,5,52,0,0,400,401,3,80,40,0,401,89,1,0,0,0,
  	402,414,3,30,15,0,403,414,3,32,16,0,404,414,3,34,17,0,405,414,3,36,18,
  	0,406,414,3,38,19,0,407,414,3,42,21,0,408,414,3,64,32,0,409,414,3,62,
  	31,0,410,414,3,72,36,0,411,414,3,74,37,0,412,414,3,88,44,0,413,402,1,
  	0,0,0,413,403,1,0,0,0,413,404,1,0,0,0,413,405,1,0,0,0,413,406,1,0,0,0,
  	413,407,1,0,0,0,413,408,1,0,0,0,413,409,1,0,0,0,413,410,1,0,0,0,413,411,
  	1,0,0,0,413,412,1,0,0,0,414,91,1,0,0,0,415,417,3,90,45,0,416,415,1,0,
  	0,0,417,420,1,0,0,0,418,416,1,0,0,0,418,419,1,0,0,0,419,421,1,0,0,0,420,
  	418,1,0,0,0,421,422,5,0,0,1,422,93,1,0,0,0,32,105,113,123,133,140,152,
  	175,196,204,211,219,236,251,271,283,289,301,312,317,322,328,335,347,357,
  	359,367,374,384,391,397,413,418
  };
  staticData->serializedATN = antlr4::atn::SerializedATNView(serializedATNSegment, sizeof(serializedATNSegment) / sizeof(serializedATNSegment[0]));

  antlr4::atn::ATNDeserializer deserializer;
  staticData->atn = deserializer.deserialize(staticData->serializedATN);

  const size_t count = staticData->atn->getNumberOfDecisions();
  staticData->decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    staticData->decisionToDFA.emplace_back(staticData->atn->getDecisionState(i), i);
  }
  gsilparserParserStaticData = staticData.release();
}

}

GSILParser::GSILParser(TokenStream *input) : GSILParser(input, antlr4::atn::ParserATNSimulatorOptions()) {}

GSILParser::GSILParser(TokenStream *input, const antlr4::atn::ParserATNSimulatorOptions &options) : Parser(input) {
  GSILParser::initialize();
  _interpreter = new atn::ParserATNSimulator(this, *gsilparserParserStaticData->atn, gsilparserParserStaticData->decisionToDFA, gsilparserParserStaticData->sharedContextCache, options);
}

GSILParser::~GSILParser() {
  delete _interpreter;
}

const atn::ATN& GSILParser::getATN() const {
  return *gsilparserParserStaticData->atn;
}

std::string GSILParser::getGrammarFileName() const {
  return "GSILParser.g4";
}

const std::vector<std::string>& GSILParser::getRuleNames() const {
  return gsilparserParserStaticData->ruleNames;
}

const dfa::Vocabulary& GSILParser::getVocabulary() const {
  return gsilparserParserStaticData->vocabulary;
}

antlr4::atn::SerializedATNView GSILParser::getSerializedATN() const {
  return gsilparserParserStaticData->serializedATN;
}


//----------------- LineSepContext ------------------------------------------------------------------

GSILParser::LineSepContext::LineSepContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::LineSepContext::Semi() {
  return getToken(GSILParser::Semi, 0);
}


size_t GSILParser::LineSepContext::getRuleIndex() const {
  return GSILParser::RuleLineSep;
}


std::any GSILParser::LineSepContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitLineSep(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::LineSepContext* GSILParser::lineSep() {
  LineSepContext *_localctx = _tracker.createInstance<LineSepContext>(_ctx, getState());
  enterRule(_localctx, 0, GSILParser::RuleLineSep);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(94);
    match(GSILParser::Semi);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableTypeContext ------------------------------------------------------------------

GSILParser::VariableTypeContext::VariableTypeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::VariableTypeContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}

tree::TerminalNode* GSILParser::VariableTypeContext::VectorType() {
  return getToken(GSILParser::VectorType, 0);
}

tree::TerminalNode* GSILParser::VariableTypeContext::Matrix() {
  return getToken(GSILParser::Matrix, 0);
}

tree::TerminalNode* GSILParser::VariableTypeContext::DMatrix() {
  return getToken(GSILParser::DMatrix, 0);
}


size_t GSILParser::VariableTypeContext::getRuleIndex() const {
  return GSILParser::RuleVariableType;
}


std::any GSILParser::VariableTypeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitVariableType(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::VariableTypeContext* GSILParser::variableType() {
  VariableTypeContext *_localctx = _tracker.createInstance<VariableTypeContext>(_ctx, getState());
  enterRule(_localctx, 2, GSILParser::RuleVariableType);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(96);
    antlrcpp::downCast<VariableTypeContext *>(_localctx)->Type = _input->LT(1);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & 30064771072) != 0) || _la == GSILParser::Identifier)) {
      antlrcpp::downCast<VariableTypeContext *>(_localctx)->Type = _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableArrayPartContext ------------------------------------------------------------------

GSILParser::VariableArrayPartContext::VariableArrayPartContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::VariableArrayPartContext::LeftBracket() {
  return getToken(GSILParser::LeftBracket, 0);
}

tree::TerminalNode* GSILParser::VariableArrayPartContext::IntegralLiteral() {
  return getToken(GSILParser::IntegralLiteral, 0);
}

tree::TerminalNode* GSILParser::VariableArrayPartContext::RightBracket() {
  return getToken(GSILParser::RightBracket, 0);
}


size_t GSILParser::VariableArrayPartContext::getRuleIndex() const {
  return GSILParser::RuleVariableArrayPart;
}


std::any GSILParser::VariableArrayPartContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitVariableArrayPart(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::VariableArrayPartContext* GSILParser::variableArrayPart() {
  VariableArrayPartContext *_localctx = _tracker.createInstance<VariableArrayPartContext>(_ctx, getState());
  enterRule(_localctx, 4, GSILParser::RuleVariableArrayPart);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(98);
    match(GSILParser::LeftBracket);
    setState(99);
    match(GSILParser::IntegralLiteral);
    setState(100);
    match(GSILParser::RightBracket);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SimpleVariableDefinitionContext ------------------------------------------------------------------

GSILParser::SimpleVariableDefinitionContext::SimpleVariableDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GSILParser::VariableTypeContext* GSILParser::SimpleVariableDefinitionContext::variableType() {
  return getRuleContext<GSILParser::VariableTypeContext>(0);
}

tree::TerminalNode* GSILParser::SimpleVariableDefinitionContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}

std::vector<tree::TerminalNode *> GSILParser::SimpleVariableDefinitionContext::TypeModifier() {
  return getTokens(GSILParser::TypeModifier);
}

tree::TerminalNode* GSILParser::SimpleVariableDefinitionContext::TypeModifier(size_t i) {
  return getToken(GSILParser::TypeModifier, i);
}

std::vector<GSILParser::VariableArrayPartContext *> GSILParser::SimpleVariableDefinitionContext::variableArrayPart() {
  return getRuleContexts<GSILParser::VariableArrayPartContext>();
}

GSILParser::VariableArrayPartContext* GSILParser::SimpleVariableDefinitionContext::variableArrayPart(size_t i) {
  return getRuleContext<GSILParser::VariableArrayPartContext>(i);
}


size_t GSILParser::SimpleVariableDefinitionContext::getRuleIndex() const {
  return GSILParser::RuleSimpleVariableDefinition;
}


std::any GSILParser::SimpleVariableDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitSimpleVariableDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::SimpleVariableDefinitionContext* GSILParser::simpleVariableDefinition() {
  SimpleVariableDefinitionContext *_localctx = _tracker.createInstance<SimpleVariableDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 6, GSILParser::RuleSimpleVariableDefinition);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(105);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == GSILParser::TypeModifier) {
      setState(102);
      antlrcpp::downCast<SimpleVariableDefinitionContext *>(_localctx)->typemodifierToken = match(GSILParser::TypeModifier);
      antlrcpp::downCast<SimpleVariableDefinitionContext *>(_localctx)->TypeModifiers.push_back(antlrcpp::downCast<SimpleVariableDefinitionContext *>(_localctx)->typemodifierToken);
      setState(107);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(108);
    antlrcpp::downCast<SimpleVariableDefinitionContext *>(_localctx)->Type = variableType();
    setState(109);
    antlrcpp::downCast<SimpleVariableDefinitionContext *>(_localctx)->Name = match(GSILParser::Identifier);
    setState(113);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == GSILParser::LeftBracket) {
      setState(110);
      antlrcpp::downCast<SimpleVariableDefinitionContext *>(_localctx)->variableArrayPartContext = variableArrayPart();
      antlrcpp::downCast<SimpleVariableDefinitionContext *>(_localctx)->ArrayParts.push_back(antlrcpp::downCast<SimpleVariableDefinitionContext *>(_localctx)->variableArrayPartContext);
      setState(115);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PropertyColorValueContext ------------------------------------------------------------------

GSILParser::PropertyColorValueContext::PropertyColorValueContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> GSILParser::PropertyColorValueContext::Colon() {
  return getTokens(GSILParser::Colon);
}

tree::TerminalNode* GSILParser::PropertyColorValueContext::Colon(size_t i) {
  return getToken(GSILParser::Colon, i);
}

std::vector<tree::TerminalNode *> GSILParser::PropertyColorValueContext::IntegralLiteral() {
  return getTokens(GSILParser::IntegralLiteral);
}

tree::TerminalNode* GSILParser::PropertyColorValueContext::IntegralLiteral(size_t i) {
  return getToken(GSILParser::IntegralLiteral, i);
}


size_t GSILParser::PropertyColorValueContext::getRuleIndex() const {
  return GSILParser::RulePropertyColorValue;
}


std::any GSILParser::PropertyColorValueContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitPropertyColorValue(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::PropertyColorValueContext* GSILParser::propertyColorValue() {
  PropertyColorValueContext *_localctx = _tracker.createInstance<PropertyColorValueContext>(_ctx, getState());
  enterRule(_localctx, 8, GSILParser::RulePropertyColorValue);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(116);
    antlrcpp::downCast<PropertyColorValueContext *>(_localctx)->Red = match(GSILParser::IntegralLiteral);
    setState(117);
    match(GSILParser::Colon);
    setState(118);
    antlrcpp::downCast<PropertyColorValueContext *>(_localctx)->Green = match(GSILParser::IntegralLiteral);
    setState(119);
    match(GSILParser::Colon);
    setState(120);
    antlrcpp::downCast<PropertyColorValueContext *>(_localctx)->Blue = match(GSILParser::IntegralLiteral);
    setState(123);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == GSILParser::Colon) {
      setState(121);
      match(GSILParser::Colon);
      setState(122);
      antlrcpp::downCast<PropertyColorValueContext *>(_localctx)->Alpha = match(GSILParser::IntegralLiteral);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PropertyDefinitionContext ------------------------------------------------------------------

GSILParser::PropertyDefinitionContext::PropertyDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::PropertyDefinitionContext::Colon() {
  return getToken(GSILParser::Colon, 0);
}

tree::TerminalNode* GSILParser::PropertyDefinitionContext::PropertySpecifier() {
  return getToken(GSILParser::PropertySpecifier, 0);
}

tree::TerminalNode* GSILParser::PropertyDefinitionContext::LeftParen() {
  return getToken(GSILParser::LeftParen, 0);
}

tree::TerminalNode* GSILParser::PropertyDefinitionContext::RightParen() {
  return getToken(GSILParser::RightParen, 0);
}

tree::TerminalNode* GSILParser::PropertyDefinitionContext::StringLiteral() {
  return getToken(GSILParser::StringLiteral, 0);
}


size_t GSILParser::PropertyDefinitionContext::getRuleIndex() const {
  return GSILParser::RulePropertyDefinition;
}


std::any GSILParser::PropertyDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitPropertyDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::PropertyDefinitionContext* GSILParser::propertyDefinition() {
  PropertyDefinitionContext *_localctx = _tracker.createInstance<PropertyDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 10, GSILParser::RulePropertyDefinition);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(125);
    match(GSILParser::Colon);
    setState(126);
    match(GSILParser::PropertySpecifier);
    setState(127);
    match(GSILParser::LeftParen);
    setState(128);
    antlrcpp::downCast<PropertyDefinitionContext *>(_localctx)->EditorName = match(GSILParser::StringLiteral);
    setState(129);
    match(GSILParser::RightParen);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatePropertyNameContext ------------------------------------------------------------------

GSILParser::StatePropertyNameContext::StatePropertyNameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::StatePropertyNameContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}

GSILParser::VariableArrayPartContext* GSILParser::StatePropertyNameContext::variableArrayPart() {
  return getRuleContext<GSILParser::VariableArrayPartContext>(0);
}


size_t GSILParser::StatePropertyNameContext::getRuleIndex() const {
  return GSILParser::RuleStatePropertyName;
}


std::any GSILParser::StatePropertyNameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitStatePropertyName(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::StatePropertyNameContext* GSILParser::statePropertyName() {
  StatePropertyNameContext *_localctx = _tracker.createInstance<StatePropertyNameContext>(_ctx, getState());
  enterRule(_localctx, 12, GSILParser::RuleStatePropertyName);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(131);
    antlrcpp::downCast<StatePropertyNameContext *>(_localctx)->Name = match(GSILParser::Identifier);
    setState(133);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == GSILParser::LeftBracket) {
      setState(132);
      variableArrayPart();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatePropertyValueContext ------------------------------------------------------------------

GSILParser::StatePropertyValueContext::StatePropertyValueContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::StatePropertyValueContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}

tree::TerminalNode* GSILParser::StatePropertyValueContext::IntegralLiteral() {
  return getToken(GSILParser::IntegralLiteral, 0);
}

tree::TerminalNode* GSILParser::StatePropertyValueContext::False() {
  return getToken(GSILParser::False, 0);
}

tree::TerminalNode* GSILParser::StatePropertyValueContext::True() {
  return getToken(GSILParser::True, 0);
}

GSILParser::PropertyColorValueContext* GSILParser::StatePropertyValueContext::propertyColorValue() {
  return getRuleContext<GSILParser::PropertyColorValueContext>(0);
}


size_t GSILParser::StatePropertyValueContext::getRuleIndex() const {
  return GSILParser::RuleStatePropertyValue;
}


std::any GSILParser::StatePropertyValueContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitStatePropertyValue(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::StatePropertyValueContext* GSILParser::statePropertyValue() {
  StatePropertyValueContext *_localctx = _tracker.createInstance<StatePropertyValueContext>(_ctx, getState());
  enterRule(_localctx, 14, GSILParser::RuleStatePropertyValue);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(140);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 4, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(135);
      match(GSILParser::Identifier);
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(136);
      match(GSILParser::IntegralLiteral);
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(137);
      match(GSILParser::False);
      break;
    }

    case 4: {
      enterOuterAlt(_localctx, 4);
      setState(138);
      match(GSILParser::True);
      break;
    }

    case 5: {
      enterOuterAlt(_localctx, 5);
      setState(139);
      propertyColorValue();
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatePropertySetterContext ------------------------------------------------------------------

GSILParser::StatePropertySetterContext::StatePropertySetterContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::StatePropertySetterContext::Assign() {
  return getToken(GSILParser::Assign, 0);
}

GSILParser::LineSepContext* GSILParser::StatePropertySetterContext::lineSep() {
  return getRuleContext<GSILParser::LineSepContext>(0);
}

GSILParser::StatePropertyNameContext* GSILParser::StatePropertySetterContext::statePropertyName() {
  return getRuleContext<GSILParser::StatePropertyNameContext>(0);
}

GSILParser::StatePropertyValueContext* GSILParser::StatePropertySetterContext::statePropertyValue() {
  return getRuleContext<GSILParser::StatePropertyValueContext>(0);
}


size_t GSILParser::StatePropertySetterContext::getRuleIndex() const {
  return GSILParser::RuleStatePropertySetter;
}


std::any GSILParser::StatePropertySetterContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitStatePropertySetter(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::StatePropertySetterContext* GSILParser::statePropertySetter() {
  StatePropertySetterContext *_localctx = _tracker.createInstance<StatePropertySetterContext>(_ctx, getState());
  enterRule(_localctx, 16, GSILParser::RuleStatePropertySetter);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(142);
    antlrcpp::downCast<StatePropertySetterContext *>(_localctx)->Property = statePropertyName();
    setState(143);
    match(GSILParser::Assign);
    setState(144);
    antlrcpp::downCast<StatePropertySetterContext *>(_localctx)->Value = statePropertyValue();
    setState(145);
    lineSep();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StateDefinitionContext ------------------------------------------------------------------

GSILParser::StateDefinitionContext::StateDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::StateDefinitionContext::LeftBrace() {
  return getToken(GSILParser::LeftBrace, 0);
}

tree::TerminalNode* GSILParser::StateDefinitionContext::RightBrace() {
  return getToken(GSILParser::RightBrace, 0);
}

GSILParser::LineSepContext* GSILParser::StateDefinitionContext::lineSep() {
  return getRuleContext<GSILParser::LineSepContext>(0);
}

tree::TerminalNode* GSILParser::StateDefinitionContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}

std::vector<GSILParser::StatePropertySetterContext *> GSILParser::StateDefinitionContext::statePropertySetter() {
  return getRuleContexts<GSILParser::StatePropertySetterContext>();
}

GSILParser::StatePropertySetterContext* GSILParser::StateDefinitionContext::statePropertySetter(size_t i) {
  return getRuleContext<GSILParser::StatePropertySetterContext>(i);
}


size_t GSILParser::StateDefinitionContext::getRuleIndex() const {
  return GSILParser::RuleStateDefinition;
}


std::any GSILParser::StateDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitStateDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::StateDefinitionContext* GSILParser::stateDefinition() {
  StateDefinitionContext *_localctx = _tracker.createInstance<StateDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 18, GSILParser::RuleStateDefinition);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(147);
    antlrcpp::downCast<StateDefinitionContext *>(_localctx)->Name = match(GSILParser::Identifier);
    setState(148);
    match(GSILParser::LeftBrace);
    setState(152);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == GSILParser::Identifier) {
      setState(149);
      antlrcpp::downCast<StateDefinitionContext *>(_localctx)->statePropertySetterContext = statePropertySetter();
      antlrcpp::downCast<StateDefinitionContext *>(_localctx)->Properties.push_back(antlrcpp::downCast<StateDefinitionContext *>(_localctx)->statePropertySetterContext);
      setState(154);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(155);
    match(GSILParser::RightBrace);
    setState(156);
    lineSep();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MaxVertexCountContext ------------------------------------------------------------------

GSILParser::MaxVertexCountContext::MaxVertexCountContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::MaxVertexCountContext::MaxVertexCount() {
  return getToken(GSILParser::MaxVertexCount, 0);
}

tree::TerminalNode* GSILParser::MaxVertexCountContext::LeftParen() {
  return getToken(GSILParser::LeftParen, 0);
}

tree::TerminalNode* GSILParser::MaxVertexCountContext::IntegralLiteral() {
  return getToken(GSILParser::IntegralLiteral, 0);
}

tree::TerminalNode* GSILParser::MaxVertexCountContext::RightParen() {
  return getToken(GSILParser::RightParen, 0);
}


size_t GSILParser::MaxVertexCountContext::getRuleIndex() const {
  return GSILParser::RuleMaxVertexCount;
}


std::any GSILParser::MaxVertexCountContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitMaxVertexCount(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::MaxVertexCountContext* GSILParser::maxVertexCount() {
  MaxVertexCountContext *_localctx = _tracker.createInstance<MaxVertexCountContext>(_ctx, getState());
  enterRule(_localctx, 20, GSILParser::RuleMaxVertexCount);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(158);
    match(GSILParser::MaxVertexCount);
    setState(159);
    match(GSILParser::LeftParen);
    setState(160);
    match(GSILParser::IntegralLiteral);
    setState(161);
    match(GSILParser::RightParen);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ThreadsContext ------------------------------------------------------------------

GSILParser::ThreadsContext::ThreadsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::ThreadsContext::Threads() {
  return getToken(GSILParser::Threads, 0);
}

tree::TerminalNode* GSILParser::ThreadsContext::LeftParen() {
  return getToken(GSILParser::LeftParen, 0);
}

std::vector<tree::TerminalNode *> GSILParser::ThreadsContext::Comma() {
  return getTokens(GSILParser::Comma);
}

tree::TerminalNode* GSILParser::ThreadsContext::Comma(size_t i) {
  return getToken(GSILParser::Comma, i);
}

tree::TerminalNode* GSILParser::ThreadsContext::RightParen() {
  return getToken(GSILParser::RightParen, 0);
}

std::vector<tree::TerminalNode *> GSILParser::ThreadsContext::IntegralLiteral() {
  return getTokens(GSILParser::IntegralLiteral);
}

tree::TerminalNode* GSILParser::ThreadsContext::IntegralLiteral(size_t i) {
  return getToken(GSILParser::IntegralLiteral, i);
}


size_t GSILParser::ThreadsContext::getRuleIndex() const {
  return GSILParser::RuleThreads;
}


std::any GSILParser::ThreadsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitThreads(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::ThreadsContext* GSILParser::threads() {
  ThreadsContext *_localctx = _tracker.createInstance<ThreadsContext>(_ctx, getState());
  enterRule(_localctx, 22, GSILParser::RuleThreads);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(163);
    match(GSILParser::Threads);
    setState(164);
    match(GSILParser::LeftParen);
    setState(165);
    antlrcpp::downCast<ThreadsContext *>(_localctx)->X = match(GSILParser::IntegralLiteral);
    setState(166);
    match(GSILParser::Comma);
    setState(167);
    antlrcpp::downCast<ThreadsContext *>(_localctx)->Y = match(GSILParser::IntegralLiteral);
    setState(168);
    match(GSILParser::Comma);
    setState(169);
    antlrcpp::downCast<ThreadsContext *>(_localctx)->Z = match(GSILParser::IntegralLiteral);
    setState(170);
    match(GSILParser::RightParen);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AttributeContext ------------------------------------------------------------------

GSILParser::AttributeContext::AttributeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::AttributeContext::EarlyZ() {
  return getToken(GSILParser::EarlyZ, 0);
}

GSILParser::MaxVertexCountContext* GSILParser::AttributeContext::maxVertexCount() {
  return getRuleContext<GSILParser::MaxVertexCountContext>(0);
}

GSILParser::ThreadsContext* GSILParser::AttributeContext::threads() {
  return getRuleContext<GSILParser::ThreadsContext>(0);
}


size_t GSILParser::AttributeContext::getRuleIndex() const {
  return GSILParser::RuleAttribute;
}


std::any GSILParser::AttributeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitAttribute(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::AttributeContext* GSILParser::attribute() {
  AttributeContext *_localctx = _tracker.createInstance<AttributeContext>(_ctx, getState());
  enterRule(_localctx, 24, GSILParser::RuleAttribute);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(175);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case GSILParser::EarlyZ: {
        enterOuterAlt(_localctx, 1);
        setState(172);
        match(GSILParser::EarlyZ);
        break;
      }

      case GSILParser::MaxVertexCount: {
        enterOuterAlt(_localctx, 2);
        setState(173);
        maxVertexCount();
        break;
      }

      case GSILParser::Threads: {
        enterOuterAlt(_localctx, 3);
        setState(174);
        threads();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AttributeDefinitionContext ------------------------------------------------------------------

GSILParser::AttributeDefinitionContext::AttributeDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::AttributeDefinitionContext::LeftBracket() {
  return getToken(GSILParser::LeftBracket, 0);
}

tree::TerminalNode* GSILParser::AttributeDefinitionContext::RightBracket() {
  return getToken(GSILParser::RightBracket, 0);
}

GSILParser::AttributeContext* GSILParser::AttributeDefinitionContext::attribute() {
  return getRuleContext<GSILParser::AttributeContext>(0);
}


size_t GSILParser::AttributeDefinitionContext::getRuleIndex() const {
  return GSILParser::RuleAttributeDefinition;
}


std::any GSILParser::AttributeDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitAttributeDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::AttributeDefinitionContext* GSILParser::attributeDefinition() {
  AttributeDefinitionContext *_localctx = _tracker.createInstance<AttributeDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 26, GSILParser::RuleAttributeDefinition);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(177);
    match(GSILParser::LeftBracket);
    setState(178);
    antlrcpp::downCast<AttributeDefinitionContext *>(_localctx)->Attribute = attribute();
    setState(179);
    match(GSILParser::RightBracket);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ShaderInputAccessSpecifierContext ------------------------------------------------------------------

GSILParser::ShaderInputAccessSpecifierContext::ShaderInputAccessSpecifierContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::ShaderInputAccessSpecifierContext::Hidden() {
  return getToken(GSILParser::Hidden, 0);
}

tree::TerminalNode* GSILParser::ShaderInputAccessSpecifierContext::EngineSupplied() {
  return getToken(GSILParser::EngineSupplied, 0);
}


size_t GSILParser::ShaderInputAccessSpecifierContext::getRuleIndex() const {
  return GSILParser::RuleShaderInputAccessSpecifier;
}


std::any GSILParser::ShaderInputAccessSpecifierContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitShaderInputAccessSpecifier(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::ShaderInputAccessSpecifierContext* GSILParser::shaderInputAccessSpecifier() {
  ShaderInputAccessSpecifierContext *_localctx = _tracker.createInstance<ShaderInputAccessSpecifierContext>(_ctx, getState());
  enterRule(_localctx, 28, GSILParser::RuleShaderInputAccessSpecifier);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(181);
    _la = _input->LA(1);
    if (!(_la == GSILParser::EngineSupplied

    || _la == GSILParser::Hidden)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RasterizerStateDefinitionContext ------------------------------------------------------------------

GSILParser::RasterizerStateDefinitionContext::RasterizerStateDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::RasterizerStateDefinitionContext::RasterizerState() {
  return getToken(GSILParser::RasterizerState, 0);
}

GSILParser::StateDefinitionContext* GSILParser::RasterizerStateDefinitionContext::stateDefinition() {
  return getRuleContext<GSILParser::StateDefinitionContext>(0);
}


size_t GSILParser::RasterizerStateDefinitionContext::getRuleIndex() const {
  return GSILParser::RuleRasterizerStateDefinition;
}


std::any GSILParser::RasterizerStateDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitRasterizerStateDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::RasterizerStateDefinitionContext* GSILParser::rasterizerStateDefinition() {
  RasterizerStateDefinitionContext *_localctx = _tracker.createInstance<RasterizerStateDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 30, GSILParser::RuleRasterizerStateDefinition);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(183);
    match(GSILParser::RasterizerState);
    setState(184);
    antlrcpp::downCast<RasterizerStateDefinitionContext *>(_localctx)->Def = stateDefinition();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BlendStateDefinitionContext ------------------------------------------------------------------

GSILParser::BlendStateDefinitionContext::BlendStateDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::BlendStateDefinitionContext::BlendState() {
  return getToken(GSILParser::BlendState, 0);
}

GSILParser::StateDefinitionContext* GSILParser::BlendStateDefinitionContext::stateDefinition() {
  return getRuleContext<GSILParser::StateDefinitionContext>(0);
}


size_t GSILParser::BlendStateDefinitionContext::getRuleIndex() const {
  return GSILParser::RuleBlendStateDefinition;
}


std::any GSILParser::BlendStateDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitBlendStateDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::BlendStateDefinitionContext* GSILParser::blendStateDefinition() {
  BlendStateDefinitionContext *_localctx = _tracker.createInstance<BlendStateDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 32, GSILParser::RuleBlendStateDefinition);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(186);
    match(GSILParser::BlendState);
    setState(187);
    antlrcpp::downCast<BlendStateDefinitionContext *>(_localctx)->Def = stateDefinition();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SamplerDefinitionContext ------------------------------------------------------------------

GSILParser::SamplerDefinitionContext::SamplerDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::SamplerDefinitionContext::Sampler() {
  return getToken(GSILParser::Sampler, 0);
}

GSILParser::StateDefinitionContext* GSILParser::SamplerDefinitionContext::stateDefinition() {
  return getRuleContext<GSILParser::StateDefinitionContext>(0);
}


size_t GSILParser::SamplerDefinitionContext::getRuleIndex() const {
  return GSILParser::RuleSamplerDefinition;
}


std::any GSILParser::SamplerDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitSamplerDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::SamplerDefinitionContext* GSILParser::samplerDefinition() {
  SamplerDefinitionContext *_localctx = _tracker.createInstance<SamplerDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 34, GSILParser::RuleSamplerDefinition);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(189);
    match(GSILParser::Sampler);
    setState(190);
    antlrcpp::downCast<SamplerDefinitionContext *>(_localctx)->Def = stateDefinition();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DepthStencilStateDefinitionContext ------------------------------------------------------------------

GSILParser::DepthStencilStateDefinitionContext::DepthStencilStateDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::DepthStencilStateDefinitionContext::DepthStencilState() {
  return getToken(GSILParser::DepthStencilState, 0);
}

GSILParser::StateDefinitionContext* GSILParser::DepthStencilStateDefinitionContext::stateDefinition() {
  return getRuleContext<GSILParser::StateDefinitionContext>(0);
}


size_t GSILParser::DepthStencilStateDefinitionContext::getRuleIndex() const {
  return GSILParser::RuleDepthStencilStateDefinition;
}


std::any GSILParser::DepthStencilStateDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitDepthStencilStateDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::DepthStencilStateDefinitionContext* GSILParser::depthStencilStateDefinition() {
  DepthStencilStateDefinitionContext *_localctx = _tracker.createInstance<DepthStencilStateDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 36, GSILParser::RuleDepthStencilStateDefinition);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(192);
    match(GSILParser::DepthStencilState);
    setState(193);
    antlrcpp::downCast<DepthStencilStateDefinitionContext *>(_localctx)->Def = stateDefinition();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TextureDefinitionContext ------------------------------------------------------------------

GSILParser::TextureDefinitionContext::TextureDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::TextureDefinitionContext::TextureType() {
  return getToken(GSILParser::TextureType, 0);
}

tree::TerminalNode* GSILParser::TextureDefinitionContext::LeftParen() {
  return getToken(GSILParser::LeftParen, 0);
}

tree::TerminalNode* GSILParser::TextureDefinitionContext::RightParen() {
  return getToken(GSILParser::RightParen, 0);
}

GSILParser::LineSepContext* GSILParser::TextureDefinitionContext::lineSep() {
  return getRuleContext<GSILParser::LineSepContext>(0);
}

std::vector<tree::TerminalNode *> GSILParser::TextureDefinitionContext::Identifier() {
  return getTokens(GSILParser::Identifier);
}

tree::TerminalNode* GSILParser::TextureDefinitionContext::Identifier(size_t i) {
  return getToken(GSILParser::Identifier, i);
}

GSILParser::PropertyDefinitionContext* GSILParser::TextureDefinitionContext::propertyDefinition() {
  return getRuleContext<GSILParser::PropertyDefinitionContext>(0);
}

GSILParser::ShaderInputAccessSpecifierContext* GSILParser::TextureDefinitionContext::shaderInputAccessSpecifier() {
  return getRuleContext<GSILParser::ShaderInputAccessSpecifierContext>(0);
}


size_t GSILParser::TextureDefinitionContext::getRuleIndex() const {
  return GSILParser::RuleTextureDefinition;
}


std::any GSILParser::TextureDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitTextureDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::TextureDefinitionContext* GSILParser::textureDefinition() {
  TextureDefinitionContext *_localctx = _tracker.createInstance<TextureDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 38, GSILParser::RuleTextureDefinition);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(196);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == GSILParser::EngineSupplied

    || _la == GSILParser::Hidden) {
      setState(195);
      antlrcpp::downCast<TextureDefinitionContext *>(_localctx)->InputSpecifier = shaderInputAccessSpecifier();
    }
    setState(198);
    match(GSILParser::TextureType);
    setState(199);
    antlrcpp::downCast<TextureDefinitionContext *>(_localctx)->Name = match(GSILParser::Identifier);
    setState(200);
    match(GSILParser::LeftParen);
    setState(201);
    antlrcpp::downCast<TextureDefinitionContext *>(_localctx)->SamplerName = match(GSILParser::Identifier);
    setState(202);
    match(GSILParser::RightParen);
    setState(204);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == GSILParser::Colon) {
      setState(203);
      propertyDefinition();
    }
    setState(206);
    lineSep();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- UavUnderlayingTypeContext ------------------------------------------------------------------

GSILParser::UavUnderlayingTypeContext::UavUnderlayingTypeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::UavUnderlayingTypeContext::VectorType() {
  return getToken(GSILParser::VectorType, 0);
}


size_t GSILParser::UavUnderlayingTypeContext::getRuleIndex() const {
  return GSILParser::RuleUavUnderlayingType;
}


std::any GSILParser::UavUnderlayingTypeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitUavUnderlayingType(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::UavUnderlayingTypeContext* GSILParser::uavUnderlayingType() {
  UavUnderlayingTypeContext *_localctx = _tracker.createInstance<UavUnderlayingTypeContext>(_ctx, getState());
  enterRule(_localctx, 40, GSILParser::RuleUavUnderlayingType);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(208);
    match(GSILParser::VectorType);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- UavDefinitionContext ------------------------------------------------------------------

GSILParser::UavDefinitionContext::UavDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::UavDefinitionContext::UAVType() {
  return getToken(GSILParser::UAVType, 0);
}

tree::TerminalNode* GSILParser::UavDefinitionContext::Less() {
  return getToken(GSILParser::Less, 0);
}

tree::TerminalNode* GSILParser::UavDefinitionContext::Greater() {
  return getToken(GSILParser::Greater, 0);
}

GSILParser::LineSepContext* GSILParser::UavDefinitionContext::lineSep() {
  return getRuleContext<GSILParser::LineSepContext>(0);
}

GSILParser::UavUnderlayingTypeContext* GSILParser::UavDefinitionContext::uavUnderlayingType() {
  return getRuleContext<GSILParser::UavUnderlayingTypeContext>(0);
}

tree::TerminalNode* GSILParser::UavDefinitionContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}

GSILParser::PropertyDefinitionContext* GSILParser::UavDefinitionContext::propertyDefinition() {
  return getRuleContext<GSILParser::PropertyDefinitionContext>(0);
}

GSILParser::ShaderInputAccessSpecifierContext* GSILParser::UavDefinitionContext::shaderInputAccessSpecifier() {
  return getRuleContext<GSILParser::ShaderInputAccessSpecifierContext>(0);
}


size_t GSILParser::UavDefinitionContext::getRuleIndex() const {
  return GSILParser::RuleUavDefinition;
}


std::any GSILParser::UavDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitUavDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::UavDefinitionContext* GSILParser::uavDefinition() {
  UavDefinitionContext *_localctx = _tracker.createInstance<UavDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 42, GSILParser::RuleUavDefinition);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(211);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == GSILParser::EngineSupplied

    || _la == GSILParser::Hidden) {
      setState(210);
      antlrcpp::downCast<UavDefinitionContext *>(_localctx)->InputSpecifier = shaderInputAccessSpecifier();
    }
    setState(213);
    match(GSILParser::UAVType);
    setState(214);
    match(GSILParser::Less);
    setState(215);
    antlrcpp::downCast<UavDefinitionContext *>(_localctx)->Type = uavUnderlayingType();
    setState(216);
    match(GSILParser::Greater);
    setState(217);
    antlrcpp::downCast<UavDefinitionContext *>(_localctx)->Name = match(GSILParser::Identifier);
    setState(219);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == GSILParser::Colon) {
      setState(218);
      propertyDefinition();
    }
    setState(221);
    lineSep();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StateNamesContext ------------------------------------------------------------------

GSILParser::StateNamesContext::StateNamesContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::StateNamesContext::BlendState() {
  return getToken(GSILParser::BlendState, 0);
}

tree::TerminalNode* GSILParser::StateNamesContext::RasterizerState() {
  return getToken(GSILParser::RasterizerState, 0);
}

tree::TerminalNode* GSILParser::StateNamesContext::DepthStencilState() {
  return getToken(GSILParser::DepthStencilState, 0);
}


size_t GSILParser::StateNamesContext::getRuleIndex() const {
  return GSILParser::RuleStateNames;
}


std::any GSILParser::StateNamesContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitStateNames(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::StateNamesContext* GSILParser::stateNames() {
  StateNamesContext *_localctx = _tracker.createInstance<StateNamesContext>(_ctx, getState());
  enterRule(_localctx, 44, GSILParser::RuleStateNames);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(223);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & 114688) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CompileShaderReplacementContext ------------------------------------------------------------------

GSILParser::CompileShaderReplacementContext::CompileShaderReplacementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::CompileShaderReplacementContext::Comma() {
  return getToken(GSILParser::Comma, 0);
}

tree::TerminalNode* GSILParser::CompileShaderReplacementContext::IntegralLiteral() {
  return getToken(GSILParser::IntegralLiteral, 0);
}

tree::TerminalNode* GSILParser::CompileShaderReplacementContext::FloatLiteral() {
  return getToken(GSILParser::FloatLiteral, 0);
}


size_t GSILParser::CompileShaderReplacementContext::getRuleIndex() const {
  return GSILParser::RuleCompileShaderReplacement;
}


std::any GSILParser::CompileShaderReplacementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitCompileShaderReplacement(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::CompileShaderReplacementContext* GSILParser::compileShaderReplacement() {
  CompileShaderReplacementContext *_localctx = _tracker.createInstance<CompileShaderReplacementContext>(_ctx, getState());
  enterRule(_localctx, 46, GSILParser::RuleCompileShaderReplacement);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(225);
    match(GSILParser::Comma);
    setState(226);
    antlrcpp::downCast<CompileShaderReplacementContext *>(_localctx)->Replacement = _input->LT(1);
    _la = _input->LA(1);
    if (!(_la == GSILParser::IntegralLiteral

    || _la == GSILParser::FloatLiteral)) {
      antlrcpp::downCast<CompileShaderReplacementContext *>(_localctx)->Replacement = _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CompileShaderContext ------------------------------------------------------------------

GSILParser::CompileShaderContext::CompileShaderContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::CompileShaderContext::CompileShader() {
  return getToken(GSILParser::CompileShader, 0);
}

tree::TerminalNode* GSILParser::CompileShaderContext::LeftParen() {
  return getToken(GSILParser::LeftParen, 0);
}

tree::TerminalNode* GSILParser::CompileShaderContext::Comma() {
  return getToken(GSILParser::Comma, 0);
}

tree::TerminalNode* GSILParser::CompileShaderContext::RightParen() {
  return getToken(GSILParser::RightParen, 0);
}

tree::TerminalNode* GSILParser::CompileShaderContext::FloatLiteral() {
  return getToken(GSILParser::FloatLiteral, 0);
}

tree::TerminalNode* GSILParser::CompileShaderContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}

std::vector<GSILParser::CompileShaderReplacementContext *> GSILParser::CompileShaderContext::compileShaderReplacement() {
  return getRuleContexts<GSILParser::CompileShaderReplacementContext>();
}

GSILParser::CompileShaderReplacementContext* GSILParser::CompileShaderContext::compileShaderReplacement(size_t i) {
  return getRuleContext<GSILParser::CompileShaderReplacementContext>(i);
}


size_t GSILParser::CompileShaderContext::getRuleIndex() const {
  return GSILParser::RuleCompileShader;
}


std::any GSILParser::CompileShaderContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitCompileShader(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::CompileShaderContext* GSILParser::compileShader() {
  CompileShaderContext *_localctx = _tracker.createInstance<CompileShaderContext>(_ctx, getState());
  enterRule(_localctx, 48, GSILParser::RuleCompileShader);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(228);
    match(GSILParser::CompileShader);
    setState(229);
    match(GSILParser::LeftParen);
    setState(230);
    antlrcpp::downCast<CompileShaderContext *>(_localctx)->Version = match(GSILParser::FloatLiteral);
    setState(231);
    match(GSILParser::Comma);
    setState(232);
    antlrcpp::downCast<CompileShaderContext *>(_localctx)->MainFunc = match(GSILParser::Identifier);
    setState(236);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == GSILParser::Comma) {
      setState(233);
      antlrcpp::downCast<CompileShaderContext *>(_localctx)->compileShaderReplacementContext = compileShaderReplacement();
      antlrcpp::downCast<CompileShaderContext *>(_localctx)->Replacements.push_back(antlrcpp::downCast<CompileShaderContext *>(_localctx)->compileShaderReplacementContext);
      setState(238);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(239);
    match(GSILParser::RightParen);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PassShaderKeyValuePairContext ------------------------------------------------------------------

GSILParser::PassShaderKeyValuePairContext::PassShaderKeyValuePairContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::PassShaderKeyValuePairContext::ShaderName() {
  return getToken(GSILParser::ShaderName, 0);
}

tree::TerminalNode* GSILParser::PassShaderKeyValuePairContext::Assign() {
  return getToken(GSILParser::Assign, 0);
}

GSILParser::CompileShaderContext* GSILParser::PassShaderKeyValuePairContext::compileShader() {
  return getRuleContext<GSILParser::CompileShaderContext>(0);
}


size_t GSILParser::PassShaderKeyValuePairContext::getRuleIndex() const {
  return GSILParser::RulePassShaderKeyValuePair;
}


std::any GSILParser::PassShaderKeyValuePairContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitPassShaderKeyValuePair(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::PassShaderKeyValuePairContext* GSILParser::passShaderKeyValuePair() {
  PassShaderKeyValuePairContext *_localctx = _tracker.createInstance<PassShaderKeyValuePairContext>(_ctx, getState());
  enterRule(_localctx, 50, GSILParser::RulePassShaderKeyValuePair);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(241);
    match(GSILParser::ShaderName);
    setState(242);
    match(GSILParser::Assign);
    setState(243);
    compileShader();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PassStateKeyValuePairContext ------------------------------------------------------------------

GSILParser::PassStateKeyValuePairContext::PassStateKeyValuePairContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GSILParser::StateNamesContext* GSILParser::PassStateKeyValuePairContext::stateNames() {
  return getRuleContext<GSILParser::StateNamesContext>(0);
}

tree::TerminalNode* GSILParser::PassStateKeyValuePairContext::Assign() {
  return getToken(GSILParser::Assign, 0);
}

tree::TerminalNode* GSILParser::PassStateKeyValuePairContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}


size_t GSILParser::PassStateKeyValuePairContext::getRuleIndex() const {
  return GSILParser::RulePassStateKeyValuePair;
}


std::any GSILParser::PassStateKeyValuePairContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitPassStateKeyValuePair(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::PassStateKeyValuePairContext* GSILParser::passStateKeyValuePair() {
  PassStateKeyValuePairContext *_localctx = _tracker.createInstance<PassStateKeyValuePairContext>(_ctx, getState());
  enterRule(_localctx, 52, GSILParser::RulePassStateKeyValuePair);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(245);
    stateNames();
    setState(246);
    match(GSILParser::Assign);
    setState(247);
    match(GSILParser::Identifier);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PassKeyValuePairContext ------------------------------------------------------------------

GSILParser::PassKeyValuePairContext::PassKeyValuePairContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GSILParser::LineSepContext* GSILParser::PassKeyValuePairContext::lineSep() {
  return getRuleContext<GSILParser::LineSepContext>(0);
}

GSILParser::PassShaderKeyValuePairContext* GSILParser::PassKeyValuePairContext::passShaderKeyValuePair() {
  return getRuleContext<GSILParser::PassShaderKeyValuePairContext>(0);
}

GSILParser::PassStateKeyValuePairContext* GSILParser::PassKeyValuePairContext::passStateKeyValuePair() {
  return getRuleContext<GSILParser::PassStateKeyValuePairContext>(0);
}


size_t GSILParser::PassKeyValuePairContext::getRuleIndex() const {
  return GSILParser::RulePassKeyValuePair;
}


std::any GSILParser::PassKeyValuePairContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitPassKeyValuePair(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::PassKeyValuePairContext* GSILParser::passKeyValuePair() {
  PassKeyValuePairContext *_localctx = _tracker.createInstance<PassKeyValuePairContext>(_ctx, getState());
  enterRule(_localctx, 54, GSILParser::RulePassKeyValuePair);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(251);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case GSILParser::ShaderName: {
        setState(249);
        passShaderKeyValuePair();
        break;
      }

      case GSILParser::BlendState:
      case GSILParser::DepthStencilState:
      case GSILParser::RasterizerState: {
        setState(250);
        passStateKeyValuePair();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    setState(253);
    lineSep();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TagValueContext ------------------------------------------------------------------

GSILParser::TagValueContext::TagValueContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::TagValueContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}

tree::TerminalNode* GSILParser::TagValueContext::IntegralLiteral() {
  return getToken(GSILParser::IntegralLiteral, 0);
}

tree::TerminalNode* GSILParser::TagValueContext::FloatLiteral() {
  return getToken(GSILParser::FloatLiteral, 0);
}


size_t GSILParser::TagValueContext::getRuleIndex() const {
  return GSILParser::RuleTagValue;
}


std::any GSILParser::TagValueContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitTagValue(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::TagValueContext* GSILParser::tagValue() {
  TagValueContext *_localctx = _tracker.createInstance<TagValueContext>(_ctx, getState());
  enterRule(_localctx, 56, GSILParser::RuleTagValue);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(255);
    _la = _input->LA(1);
    if (!(((((_la - 96) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 96)) & 11) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TagDefinitionContext ------------------------------------------------------------------

GSILParser::TagDefinitionContext::TagDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::TagDefinitionContext::Tag() {
  return getToken(GSILParser::Tag, 0);
}

tree::TerminalNode* GSILParser::TagDefinitionContext::LeftParen() {
  return getToken(GSILParser::LeftParen, 0);
}

tree::TerminalNode* GSILParser::TagDefinitionContext::Colon() {
  return getToken(GSILParser::Colon, 0);
}

tree::TerminalNode* GSILParser::TagDefinitionContext::RightParen() {
  return getToken(GSILParser::RightParen, 0);
}

GSILParser::LineSepContext* GSILParser::TagDefinitionContext::lineSep() {
  return getRuleContext<GSILParser::LineSepContext>(0);
}

tree::TerminalNode* GSILParser::TagDefinitionContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}

GSILParser::TagValueContext* GSILParser::TagDefinitionContext::tagValue() {
  return getRuleContext<GSILParser::TagValueContext>(0);
}


size_t GSILParser::TagDefinitionContext::getRuleIndex() const {
  return GSILParser::RuleTagDefinition;
}


std::any GSILParser::TagDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitTagDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::TagDefinitionContext* GSILParser::tagDefinition() {
  TagDefinitionContext *_localctx = _tracker.createInstance<TagDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 58, GSILParser::RuleTagDefinition);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(257);
    match(GSILParser::Tag);
    setState(258);
    match(GSILParser::LeftParen);
    setState(259);
    antlrcpp::downCast<TagDefinitionContext *>(_localctx)->Key = match(GSILParser::Identifier);
    setState(260);
    match(GSILParser::Colon);
    setState(261);
    antlrcpp::downCast<TagDefinitionContext *>(_localctx)->Value = tagValue();
    setState(262);
    match(GSILParser::RightParen);
    setState(263);
    lineSep();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PassDefinitionContext ------------------------------------------------------------------

GSILParser::PassDefinitionContext::PassDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::PassDefinitionContext::Pass() {
  return getToken(GSILParser::Pass, 0);
}

tree::TerminalNode* GSILParser::PassDefinitionContext::LeftBrace() {
  return getToken(GSILParser::LeftBrace, 0);
}

tree::TerminalNode* GSILParser::PassDefinitionContext::RightBrace() {
  return getToken(GSILParser::RightBrace, 0);
}

GSILParser::LineSepContext* GSILParser::PassDefinitionContext::lineSep() {
  return getRuleContext<GSILParser::LineSepContext>(0);
}

tree::TerminalNode* GSILParser::PassDefinitionContext::IntegralLiteral() {
  return getToken(GSILParser::IntegralLiteral, 0);
}

std::vector<GSILParser::PassKeyValuePairContext *> GSILParser::PassDefinitionContext::passKeyValuePair() {
  return getRuleContexts<GSILParser::PassKeyValuePairContext>();
}

GSILParser::PassKeyValuePairContext* GSILParser::PassDefinitionContext::passKeyValuePair(size_t i) {
  return getRuleContext<GSILParser::PassKeyValuePairContext>(i);
}


size_t GSILParser::PassDefinitionContext::getRuleIndex() const {
  return GSILParser::RulePassDefinition;
}


std::any GSILParser::PassDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitPassDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::PassDefinitionContext* GSILParser::passDefinition() {
  PassDefinitionContext *_localctx = _tracker.createInstance<PassDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 60, GSILParser::RulePassDefinition);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(265);
    match(GSILParser::Pass);
    setState(266);
    antlrcpp::downCast<PassDefinitionContext *>(_localctx)->Id = match(GSILParser::IntegralLiteral);
    setState(267);
    match(GSILParser::LeftBrace);
    setState(271);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & 2147598336) != 0)) {
      setState(268);
      antlrcpp::downCast<PassDefinitionContext *>(_localctx)->passKeyValuePairContext = passKeyValuePair();
      antlrcpp::downCast<PassDefinitionContext *>(_localctx)->Values.push_back(antlrcpp::downCast<PassDefinitionContext *>(_localctx)->passKeyValuePairContext);
      setState(273);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(274);
    match(GSILParser::RightBrace);
    setState(275);
    lineSep();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TechniqueDefinitionContext ------------------------------------------------------------------

GSILParser::TechniqueDefinitionContext::TechniqueDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::TechniqueDefinitionContext::Technique() {
  return getToken(GSILParser::Technique, 0);
}

tree::TerminalNode* GSILParser::TechniqueDefinitionContext::LeftBrace() {
  return getToken(GSILParser::LeftBrace, 0);
}

tree::TerminalNode* GSILParser::TechniqueDefinitionContext::RightBrace() {
  return getToken(GSILParser::RightBrace, 0);
}

GSILParser::LineSepContext* GSILParser::TechniqueDefinitionContext::lineSep() {
  return getRuleContext<GSILParser::LineSepContext>(0);
}

tree::TerminalNode* GSILParser::TechniqueDefinitionContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}

std::vector<GSILParser::TagDefinitionContext *> GSILParser::TechniqueDefinitionContext::tagDefinition() {
  return getRuleContexts<GSILParser::TagDefinitionContext>();
}

GSILParser::TagDefinitionContext* GSILParser::TechniqueDefinitionContext::tagDefinition(size_t i) {
  return getRuleContext<GSILParser::TagDefinitionContext>(i);
}

std::vector<GSILParser::PassDefinitionContext *> GSILParser::TechniqueDefinitionContext::passDefinition() {
  return getRuleContexts<GSILParser::PassDefinitionContext>();
}

GSILParser::PassDefinitionContext* GSILParser::TechniqueDefinitionContext::passDefinition(size_t i) {
  return getRuleContext<GSILParser::PassDefinitionContext>(i);
}


size_t GSILParser::TechniqueDefinitionContext::getRuleIndex() const {
  return GSILParser::RuleTechniqueDefinition;
}


std::any GSILParser::TechniqueDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitTechniqueDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::TechniqueDefinitionContext* GSILParser::techniqueDefinition() {
  TechniqueDefinitionContext *_localctx = _tracker.createInstance<TechniqueDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 62, GSILParser::RuleTechniqueDefinition);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(277);
    match(GSILParser::Technique);
    setState(278);
    antlrcpp::downCast<TechniqueDefinitionContext *>(_localctx)->Name = match(GSILParser::Identifier);
    setState(279);
    match(GSILParser::LeftBrace);
    setState(283);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == GSILParser::Tag) {
      setState(280);
      antlrcpp::downCast<TechniqueDefinitionContext *>(_localctx)->tagDefinitionContext = tagDefinition();
      antlrcpp::downCast<TechniqueDefinitionContext *>(_localctx)->Tags.push_back(antlrcpp::downCast<TechniqueDefinitionContext *>(_localctx)->tagDefinitionContext);
      setState(285);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(289);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == GSILParser::Pass) {
      setState(286);
      antlrcpp::downCast<TechniqueDefinitionContext *>(_localctx)->passDefinitionContext = passDefinition();
      antlrcpp::downCast<TechniqueDefinitionContext *>(_localctx)->Passes.push_back(antlrcpp::downCast<TechniqueDefinitionContext *>(_localctx)->passDefinitionContext);
      setState(291);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(292);
    match(GSILParser::RightBrace);
    setState(293);
    lineSep();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TechniqueGroupDefinitionContext ------------------------------------------------------------------

GSILParser::TechniqueGroupDefinitionContext::TechniqueGroupDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::TechniqueGroupDefinitionContext::TechniqueGroup() {
  return getToken(GSILParser::TechniqueGroup, 0);
}

tree::TerminalNode* GSILParser::TechniqueGroupDefinitionContext::LeftBrace() {
  return getToken(GSILParser::LeftBrace, 0);
}

tree::TerminalNode* GSILParser::TechniqueGroupDefinitionContext::RightBrace() {
  return getToken(GSILParser::RightBrace, 0);
}

GSILParser::LineSepContext* GSILParser::TechniqueGroupDefinitionContext::lineSep() {
  return getRuleContext<GSILParser::LineSepContext>(0);
}

tree::TerminalNode* GSILParser::TechniqueGroupDefinitionContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}

std::vector<GSILParser::TechniqueDefinitionContext *> GSILParser::TechniqueGroupDefinitionContext::techniqueDefinition() {
  return getRuleContexts<GSILParser::TechniqueDefinitionContext>();
}

GSILParser::TechniqueDefinitionContext* GSILParser::TechniqueGroupDefinitionContext::techniqueDefinition(size_t i) {
  return getRuleContext<GSILParser::TechniqueDefinitionContext>(i);
}


size_t GSILParser::TechniqueGroupDefinitionContext::getRuleIndex() const {
  return GSILParser::RuleTechniqueGroupDefinition;
}


std::any GSILParser::TechniqueGroupDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitTechniqueGroupDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::TechniqueGroupDefinitionContext* GSILParser::techniqueGroupDefinition() {
  TechniqueGroupDefinitionContext *_localctx = _tracker.createInstance<TechniqueGroupDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 64, GSILParser::RuleTechniqueGroupDefinition);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(295);
    match(GSILParser::TechniqueGroup);
    setState(296);
    antlrcpp::downCast<TechniqueGroupDefinitionContext *>(_localctx)->Name = match(GSILParser::Identifier);
    setState(297);
    match(GSILParser::LeftBrace);
    setState(299); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(298);
      antlrcpp::downCast<TechniqueGroupDefinitionContext *>(_localctx)->techniqueDefinitionContext = techniqueDefinition();
      antlrcpp::downCast<TechniqueGroupDefinitionContext *>(_localctx)->Techniques.push_back(antlrcpp::downCast<TechniqueGroupDefinitionContext *>(_localctx)->techniqueDefinitionContext);
      setState(301); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == GSILParser::Technique);
    setState(303);
    match(GSILParser::RightBrace);
    setState(304);
    lineSep();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StructSpecifierContext ------------------------------------------------------------------

GSILParser::StructSpecifierContext::StructSpecifierContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::StructSpecifierContext::InOut() {
  return getToken(GSILParser::InOut, 0);
}


size_t GSILParser::StructSpecifierContext::getRuleIndex() const {
  return GSILParser::RuleStructSpecifier;
}


std::any GSILParser::StructSpecifierContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitStructSpecifier(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::StructSpecifierContext* GSILParser::structSpecifier() {
  StructSpecifierContext *_localctx = _tracker.createInstance<StructSpecifierContext>(_ctx, getState());
  enterRule(_localctx, 66, GSILParser::RuleStructSpecifier);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(306);
    match(GSILParser::InOut);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FieldSpecifierContext ------------------------------------------------------------------

GSILParser::FieldSpecifierContext::FieldSpecifierContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::FieldSpecifierContext::SVSpecifier() {
  return getToken(GSILParser::SVSpecifier, 0);
}

tree::TerminalNode* GSILParser::FieldSpecifierContext::Instanced() {
  return getToken(GSILParser::Instanced, 0);
}

GSILParser::ShaderInputAccessSpecifierContext* GSILParser::FieldSpecifierContext::shaderInputAccessSpecifier() {
  return getRuleContext<GSILParser::ShaderInputAccessSpecifierContext>(0);
}

tree::TerminalNode* GSILParser::FieldSpecifierContext::InterpolationModeSpecifier() {
  return getToken(GSILParser::InterpolationModeSpecifier, 0);
}


size_t GSILParser::FieldSpecifierContext::getRuleIndex() const {
  return GSILParser::RuleFieldSpecifier;
}


std::any GSILParser::FieldSpecifierContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitFieldSpecifier(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::FieldSpecifierContext* GSILParser::fieldSpecifier() {
  FieldSpecifierContext *_localctx = _tracker.createInstance<FieldSpecifierContext>(_ctx, getState());
  enterRule(_localctx, 68, GSILParser::RuleFieldSpecifier);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(312);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case GSILParser::SVSpecifier: {
        enterOuterAlt(_localctx, 1);
        setState(308);
        match(GSILParser::SVSpecifier);
        break;
      }

      case GSILParser::Instanced: {
        enterOuterAlt(_localctx, 2);
        setState(309);
        match(GSILParser::Instanced);
        break;
      }

      case GSILParser::EngineSupplied:
      case GSILParser::Hidden: {
        enterOuterAlt(_localctx, 3);
        setState(310);
        shaderInputAccessSpecifier();
        break;
      }

      case GSILParser::InterpolationModeSpecifier: {
        enterOuterAlt(_localctx, 4);
        setState(311);
        match(GSILParser::InterpolationModeSpecifier);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StructFieldContext ------------------------------------------------------------------

GSILParser::StructFieldContext::StructFieldContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GSILParser::SimpleVariableDefinitionContext* GSILParser::StructFieldContext::simpleVariableDefinition() {
  return getRuleContext<GSILParser::SimpleVariableDefinitionContext>(0);
}

GSILParser::LineSepContext* GSILParser::StructFieldContext::lineSep() {
  return getRuleContext<GSILParser::LineSepContext>(0);
}

GSILParser::PropertyDefinitionContext* GSILParser::StructFieldContext::propertyDefinition() {
  return getRuleContext<GSILParser::PropertyDefinitionContext>(0);
}

std::vector<GSILParser::FieldSpecifierContext *> GSILParser::StructFieldContext::fieldSpecifier() {
  return getRuleContexts<GSILParser::FieldSpecifierContext>();
}

GSILParser::FieldSpecifierContext* GSILParser::StructFieldContext::fieldSpecifier(size_t i) {
  return getRuleContext<GSILParser::FieldSpecifierContext>(i);
}


size_t GSILParser::StructFieldContext::getRuleIndex() const {
  return GSILParser::RuleStructField;
}


std::any GSILParser::StructFieldContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitStructField(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::StructFieldContext* GSILParser::structField() {
  StructFieldContext *_localctx = _tracker.createInstance<StructFieldContext>(_ctx, getState());
  enterRule(_localctx, 70, GSILParser::RuleStructField);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(317);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & 537140224) != 0)) {
      setState(314);
      antlrcpp::downCast<StructFieldContext *>(_localctx)->fieldSpecifierContext = fieldSpecifier();
      antlrcpp::downCast<StructFieldContext *>(_localctx)->InputSpecifier.push_back(antlrcpp::downCast<StructFieldContext *>(_localctx)->fieldSpecifierContext);
      setState(319);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(320);
    simpleVariableDefinition();
    setState(322);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == GSILParser::Colon) {
      setState(321);
      propertyDefinition();
    }
    setState(324);
    lineSep();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StructDefinitionContext ------------------------------------------------------------------

GSILParser::StructDefinitionContext::StructDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::StructDefinitionContext::Struct() {
  return getToken(GSILParser::Struct, 0);
}

tree::TerminalNode* GSILParser::StructDefinitionContext::LeftBrace() {
  return getToken(GSILParser::LeftBrace, 0);
}

tree::TerminalNode* GSILParser::StructDefinitionContext::RightBrace() {
  return getToken(GSILParser::RightBrace, 0);
}

GSILParser::LineSepContext* GSILParser::StructDefinitionContext::lineSep() {
  return getRuleContext<GSILParser::LineSepContext>(0);
}

tree::TerminalNode* GSILParser::StructDefinitionContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}

GSILParser::StructSpecifierContext* GSILParser::StructDefinitionContext::structSpecifier() {
  return getRuleContext<GSILParser::StructSpecifierContext>(0);
}

std::vector<GSILParser::StructFieldContext *> GSILParser::StructDefinitionContext::structField() {
  return getRuleContexts<GSILParser::StructFieldContext>();
}

GSILParser::StructFieldContext* GSILParser::StructDefinitionContext::structField(size_t i) {
  return getRuleContext<GSILParser::StructFieldContext>(i);
}


size_t GSILParser::StructDefinitionContext::getRuleIndex() const {
  return GSILParser::RuleStructDefinition;
}


std::any GSILParser::StructDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitStructDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::StructDefinitionContext* GSILParser::structDefinition() {
  StructDefinitionContext *_localctx = _tracker.createInstance<StructDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 72, GSILParser::RuleStructDefinition);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(326);
    match(GSILParser::Struct);
    setState(328);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == GSILParser::InOut) {
      setState(327);
      antlrcpp::downCast<StructDefinitionContext *>(_localctx)->Specifiers = structSpecifier();
    }
    setState(330);
    antlrcpp::downCast<StructDefinitionContext *>(_localctx)->Name = match(GSILParser::Identifier);
    setState(331);
    match(GSILParser::LeftBrace);
    setState(335);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & 1125930508753920) != 0) || _la == GSILParser::Identifier) {
      setState(332);
      antlrcpp::downCast<StructDefinitionContext *>(_localctx)->structFieldContext = structField();
      antlrcpp::downCast<StructDefinitionContext *>(_localctx)->Fields.push_back(antlrcpp::downCast<StructDefinitionContext *>(_localctx)->structFieldContext);
      setState(337);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(338);
    match(GSILParser::RightBrace);
    setState(339);
    lineSep();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CbufferDefinitionContext ------------------------------------------------------------------

GSILParser::CbufferDefinitionContext::CbufferDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::CbufferDefinitionContext::CBuffer() {
  return getToken(GSILParser::CBuffer, 0);
}

tree::TerminalNode* GSILParser::CbufferDefinitionContext::LeftBrace() {
  return getToken(GSILParser::LeftBrace, 0);
}

tree::TerminalNode* GSILParser::CbufferDefinitionContext::RightBrace() {
  return getToken(GSILParser::RightBrace, 0);
}

GSILParser::LineSepContext* GSILParser::CbufferDefinitionContext::lineSep() {
  return getRuleContext<GSILParser::LineSepContext>(0);
}

tree::TerminalNode* GSILParser::CbufferDefinitionContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}

std::vector<GSILParser::StructFieldContext *> GSILParser::CbufferDefinitionContext::structField() {
  return getRuleContexts<GSILParser::StructFieldContext>();
}

GSILParser::StructFieldContext* GSILParser::CbufferDefinitionContext::structField(size_t i) {
  return getRuleContext<GSILParser::StructFieldContext>(i);
}


size_t GSILParser::CbufferDefinitionContext::getRuleIndex() const {
  return GSILParser::RuleCbufferDefinition;
}


std::any GSILParser::CbufferDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitCbufferDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::CbufferDefinitionContext* GSILParser::cbufferDefinition() {
  CbufferDefinitionContext *_localctx = _tracker.createInstance<CbufferDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 74, GSILParser::RuleCbufferDefinition);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(341);
    match(GSILParser::CBuffer);
    setState(342);
    antlrcpp::downCast<CbufferDefinitionContext *>(_localctx)->Name = match(GSILParser::Identifier);
    setState(343);
    match(GSILParser::LeftBrace);
    setState(347);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & 1125930508753920) != 0) || _la == GSILParser::Identifier) {
      setState(344);
      antlrcpp::downCast<CbufferDefinitionContext *>(_localctx)->structFieldContext = structField();
      antlrcpp::downCast<CbufferDefinitionContext *>(_localctx)->Fields.push_back(antlrcpp::downCast<CbufferDefinitionContext *>(_localctx)->structFieldContext);
      setState(349);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(350);
    match(GSILParser::RightBrace);
    setState(351);
    lineSep();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ValidInFunctionDefContext ------------------------------------------------------------------

GSILParser::ValidInFunctionDefContext::ValidInFunctionDefContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::RightBrace() {
  return getToken(GSILParser::RightBrace, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::CBuffer() {
  return getToken(GSILParser::CBuffer, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::RasterizerState() {
  return getToken(GSILParser::RasterizerState, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::BlendState() {
  return getToken(GSILParser::BlendState, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::DepthStencilState() {
  return getToken(GSILParser::DepthStencilState, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::Sampler() {
  return getToken(GSILParser::Sampler, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::CompileShader() {
  return getToken(GSILParser::CompileShader, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::Struct() {
  return getToken(GSILParser::Struct, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::Tag() {
  return getToken(GSILParser::Tag, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::Pass() {
  return getToken(GSILParser::Pass, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::UAVType() {
  return getToken(GSILParser::UAVType, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::TextureType() {
  return getToken(GSILParser::TextureType, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::Hidden() {
  return getToken(GSILParser::Hidden, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::EngineSupplied() {
  return getToken(GSILParser::EngineSupplied, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::Technique() {
  return getToken(GSILParser::Technique, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::TechniqueGroup() {
  return getToken(GSILParser::TechniqueGroup, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::Uniform() {
  return getToken(GSILParser::Uniform, 0);
}

tree::TerminalNode* GSILParser::ValidInFunctionDefContext::ShaderName() {
  return getToken(GSILParser::ShaderName, 0);
}


size_t GSILParser::ValidInFunctionDefContext::getRuleIndex() const {
  return GSILParser::RuleValidInFunctionDef;
}


std::any GSILParser::ValidInFunctionDefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitValidInFunctionDef(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::ValidInFunctionDefContext* GSILParser::validInFunctionDef() {
  ValidInFunctionDefContext *_localctx = _tracker.createInstance<ValidInFunctionDefContext>(_ctx, getState());
  enterRule(_localctx, 76, GSILParser::RuleValidInFunctionDef);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(353);
    _la = _input->LA(1);
    if (_la == 0 || _la == Token::EOF || ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & 72057597325867104) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BlockContentContext ------------------------------------------------------------------

GSILParser::BlockContentContext::BlockContentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<GSILParser::BlockContext *> GSILParser::BlockContentContext::block() {
  return getRuleContexts<GSILParser::BlockContext>();
}

GSILParser::BlockContext* GSILParser::BlockContentContext::block(size_t i) {
  return getRuleContext<GSILParser::BlockContext>(i);
}

std::vector<GSILParser::ValidInFunctionDefContext *> GSILParser::BlockContentContext::validInFunctionDef() {
  return getRuleContexts<GSILParser::ValidInFunctionDefContext>();
}

GSILParser::ValidInFunctionDefContext* GSILParser::BlockContentContext::validInFunctionDef(size_t i) {
  return getRuleContext<GSILParser::ValidInFunctionDefContext>(i);
}


size_t GSILParser::BlockContentContext::getRuleIndex() const {
  return GSILParser::RuleBlockContent;
}


std::any GSILParser::BlockContentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitBlockContent(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::BlockContentContext* GSILParser::blockContent() {
  BlockContentContext *_localctx = _tracker.createInstance<BlockContentContext>(_ctx, getState());
  enterRule(_localctx, 78, GSILParser::RuleBlockContent);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(359);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & -72057597325867106) != 0) || ((((_la - 64) & ~ 0x3fULL) == 0) &&
      ((1ULL << (_la - 64)) & 68719476735) != 0)) {
      setState(357);
      _errHandler->sync(this);
      switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 23, _ctx)) {
      case 1: {
        setState(355);
        block();
        break;
      }

      case 2: {
        setState(356);
        validInFunctionDef();
        break;
      }

      default:
        break;
      }
      setState(361);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BlockContext ------------------------------------------------------------------

GSILParser::BlockContext::BlockContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::BlockContext::LeftBrace() {
  return getToken(GSILParser::LeftBrace, 0);
}

tree::TerminalNode* GSILParser::BlockContext::RightBrace() {
  return getToken(GSILParser::RightBrace, 0);
}

GSILParser::BlockContentContext* GSILParser::BlockContext::blockContent() {
  return getRuleContext<GSILParser::BlockContentContext>(0);
}


size_t GSILParser::BlockContext::getRuleIndex() const {
  return GSILParser::RuleBlock;
}


std::any GSILParser::BlockContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitBlock(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::BlockContext* GSILParser::block() {
  BlockContext *_localctx = _tracker.createInstance<BlockContext>(_ctx, getState());
  enterRule(_localctx, 80, GSILParser::RuleBlock);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(362);
    match(GSILParser::LeftBrace);
    setState(363);
    antlrcpp::downCast<BlockContext *>(_localctx)->Content = blockContent();
    setState(364);
    match(GSILParser::RightBrace);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionParameterContext ------------------------------------------------------------------

GSILParser::FunctionParameterContext::FunctionParameterContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GSILParser::VariableTypeContext* GSILParser::FunctionParameterContext::variableType() {
  return getRuleContext<GSILParser::VariableTypeContext>(0);
}

tree::TerminalNode* GSILParser::FunctionParameterContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}

tree::TerminalNode* GSILParser::FunctionParameterContext::Uniform() {
  return getToken(GSILParser::Uniform, 0);
}

std::vector<GSILParser::VariableArrayPartContext *> GSILParser::FunctionParameterContext::variableArrayPart() {
  return getRuleContexts<GSILParser::VariableArrayPartContext>();
}

GSILParser::VariableArrayPartContext* GSILParser::FunctionParameterContext::variableArrayPart(size_t i) {
  return getRuleContext<GSILParser::VariableArrayPartContext>(i);
}


size_t GSILParser::FunctionParameterContext::getRuleIndex() const {
  return GSILParser::RuleFunctionParameter;
}


std::any GSILParser::FunctionParameterContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitFunctionParameter(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::FunctionParameterContext* GSILParser::functionParameter() {
  FunctionParameterContext *_localctx = _tracker.createInstance<FunctionParameterContext>(_ctx, getState());
  enterRule(_localctx, 82, GSILParser::RuleFunctionParameter);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(367);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == GSILParser::Uniform) {
      setState(366);
      antlrcpp::downCast<FunctionParameterContext *>(_localctx)->IsUniform = match(GSILParser::Uniform);
    }
    setState(369);
    antlrcpp::downCast<FunctionParameterContext *>(_localctx)->Type = variableType();
    setState(370);
    antlrcpp::downCast<FunctionParameterContext *>(_localctx)->Name = match(GSILParser::Identifier);
    setState(374);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == GSILParser::LeftBracket) {
      setState(371);
      antlrcpp::downCast<FunctionParameterContext *>(_localctx)->variableArrayPartContext = variableArrayPart();
      antlrcpp::downCast<FunctionParameterContext *>(_localctx)->ArrayPart.push_back(antlrcpp::downCast<FunctionParameterContext *>(_localctx)->variableArrayPartContext);
      setState(376);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CommaFunctionParameterContext ------------------------------------------------------------------

GSILParser::CommaFunctionParameterContext::CommaFunctionParameterContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::CommaFunctionParameterContext::Comma() {
  return getToken(GSILParser::Comma, 0);
}

GSILParser::FunctionParameterContext* GSILParser::CommaFunctionParameterContext::functionParameter() {
  return getRuleContext<GSILParser::FunctionParameterContext>(0);
}


size_t GSILParser::CommaFunctionParameterContext::getRuleIndex() const {
  return GSILParser::RuleCommaFunctionParameter;
}


std::any GSILParser::CommaFunctionParameterContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitCommaFunctionParameter(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::CommaFunctionParameterContext* GSILParser::commaFunctionParameter() {
  CommaFunctionParameterContext *_localctx = _tracker.createInstance<CommaFunctionParameterContext>(_ctx, getState());
  enterRule(_localctx, 84, GSILParser::RuleCommaFunctionParameter);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(377);
    match(GSILParser::Comma);
    setState(378);
    antlrcpp::downCast<CommaFunctionParameterContext *>(_localctx)->Parameter = functionParameter();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionParameterListContext ------------------------------------------------------------------

GSILParser::FunctionParameterListContext::FunctionParameterListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GSILParser::FunctionParameterContext* GSILParser::FunctionParameterListContext::functionParameter() {
  return getRuleContext<GSILParser::FunctionParameterContext>(0);
}

std::vector<GSILParser::CommaFunctionParameterContext *> GSILParser::FunctionParameterListContext::commaFunctionParameter() {
  return getRuleContexts<GSILParser::CommaFunctionParameterContext>();
}

GSILParser::CommaFunctionParameterContext* GSILParser::FunctionParameterListContext::commaFunctionParameter(size_t i) {
  return getRuleContext<GSILParser::CommaFunctionParameterContext>(i);
}


size_t GSILParser::FunctionParameterListContext::getRuleIndex() const {
  return GSILParser::RuleFunctionParameterList;
}


std::any GSILParser::FunctionParameterListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitFunctionParameterList(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::FunctionParameterListContext* GSILParser::functionParameterList() {
  FunctionParameterListContext *_localctx = _tracker.createInstance<FunctionParameterListContext>(_ctx, getState());
  enterRule(_localctx, 86, GSILParser::RuleFunctionParameterList);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(380);
    antlrcpp::downCast<FunctionParameterListContext *>(_localctx)->FirstParameter = functionParameter();
    setState(384);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == GSILParser::Comma) {
      setState(381);
      antlrcpp::downCast<FunctionParameterListContext *>(_localctx)->commaFunctionParameterContext = commaFunctionParameter();
      antlrcpp::downCast<FunctionParameterListContext *>(_localctx)->MoreParameters.push_back(antlrcpp::downCast<FunctionParameterListContext *>(_localctx)->commaFunctionParameterContext);
      setState(386);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionDefinitionContext ------------------------------------------------------------------

GSILParser::FunctionDefinitionContext::FunctionDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::FunctionDefinitionContext::LeftParen() {
  return getToken(GSILParser::LeftParen, 0);
}

tree::TerminalNode* GSILParser::FunctionDefinitionContext::RightParen() {
  return getToken(GSILParser::RightParen, 0);
}

GSILParser::VariableTypeContext* GSILParser::FunctionDefinitionContext::variableType() {
  return getRuleContext<GSILParser::VariableTypeContext>(0);
}

tree::TerminalNode* GSILParser::FunctionDefinitionContext::Identifier() {
  return getToken(GSILParser::Identifier, 0);
}

GSILParser::BlockContext* GSILParser::FunctionDefinitionContext::block() {
  return getRuleContext<GSILParser::BlockContext>(0);
}

std::vector<GSILParser::AttributeDefinitionContext *> GSILParser::FunctionDefinitionContext::attributeDefinition() {
  return getRuleContexts<GSILParser::AttributeDefinitionContext>();
}

GSILParser::AttributeDefinitionContext* GSILParser::FunctionDefinitionContext::attributeDefinition(size_t i) {
  return getRuleContext<GSILParser::AttributeDefinitionContext>(i);
}

GSILParser::FunctionParameterListContext* GSILParser::FunctionDefinitionContext::functionParameterList() {
  return getRuleContext<GSILParser::FunctionParameterListContext>(0);
}


size_t GSILParser::FunctionDefinitionContext::getRuleIndex() const {
  return GSILParser::RuleFunctionDefinition;
}


std::any GSILParser::FunctionDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitFunctionDefinition(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::FunctionDefinitionContext* GSILParser::functionDefinition() {
  FunctionDefinitionContext *_localctx = _tracker.createInstance<FunctionDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 88, GSILParser::RuleFunctionDefinition);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(387);
    antlrcpp::downCast<FunctionDefinitionContext *>(_localctx)->ReturnType = variableType();
    setState(391);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == GSILParser::LeftBracket) {
      setState(388);
      antlrcpp::downCast<FunctionDefinitionContext *>(_localctx)->attributeDefinitionContext = attributeDefinition();
      antlrcpp::downCast<FunctionDefinitionContext *>(_localctx)->Attributes.push_back(antlrcpp::downCast<FunctionDefinitionContext *>(_localctx)->attributeDefinitionContext);
      setState(393);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(394);
    antlrcpp::downCast<FunctionDefinitionContext *>(_localctx)->Name = match(GSILParser::Identifier);
    setState(395);
    match(GSILParser::LeftParen);
    setState(397);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & 31138512896) != 0) || _la == GSILParser::Identifier) {
      setState(396);
      antlrcpp::downCast<FunctionDefinitionContext *>(_localctx)->Parameters = functionParameterList();
    }
    setState(399);
    match(GSILParser::RightParen);
    setState(400);
    antlrcpp::downCast<FunctionDefinitionContext *>(_localctx)->Code = block();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TopLevelDeclarationContext ------------------------------------------------------------------

GSILParser::TopLevelDeclarationContext::TopLevelDeclarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

GSILParser::RasterizerStateDefinitionContext* GSILParser::TopLevelDeclarationContext::rasterizerStateDefinition() {
  return getRuleContext<GSILParser::RasterizerStateDefinitionContext>(0);
}

GSILParser::BlendStateDefinitionContext* GSILParser::TopLevelDeclarationContext::blendStateDefinition() {
  return getRuleContext<GSILParser::BlendStateDefinitionContext>(0);
}

GSILParser::SamplerDefinitionContext* GSILParser::TopLevelDeclarationContext::samplerDefinition() {
  return getRuleContext<GSILParser::SamplerDefinitionContext>(0);
}

GSILParser::DepthStencilStateDefinitionContext* GSILParser::TopLevelDeclarationContext::depthStencilStateDefinition() {
  return getRuleContext<GSILParser::DepthStencilStateDefinitionContext>(0);
}

GSILParser::TextureDefinitionContext* GSILParser::TopLevelDeclarationContext::textureDefinition() {
  return getRuleContext<GSILParser::TextureDefinitionContext>(0);
}

GSILParser::UavDefinitionContext* GSILParser::TopLevelDeclarationContext::uavDefinition() {
  return getRuleContext<GSILParser::UavDefinitionContext>(0);
}

GSILParser::TechniqueGroupDefinitionContext* GSILParser::TopLevelDeclarationContext::techniqueGroupDefinition() {
  return getRuleContext<GSILParser::TechniqueGroupDefinitionContext>(0);
}

GSILParser::TechniqueDefinitionContext* GSILParser::TopLevelDeclarationContext::techniqueDefinition() {
  return getRuleContext<GSILParser::TechniqueDefinitionContext>(0);
}

GSILParser::StructDefinitionContext* GSILParser::TopLevelDeclarationContext::structDefinition() {
  return getRuleContext<GSILParser::StructDefinitionContext>(0);
}

GSILParser::CbufferDefinitionContext* GSILParser::TopLevelDeclarationContext::cbufferDefinition() {
  return getRuleContext<GSILParser::CbufferDefinitionContext>(0);
}

GSILParser::FunctionDefinitionContext* GSILParser::TopLevelDeclarationContext::functionDefinition() {
  return getRuleContext<GSILParser::FunctionDefinitionContext>(0);
}


size_t GSILParser::TopLevelDeclarationContext::getRuleIndex() const {
  return GSILParser::RuleTopLevelDeclaration;
}


std::any GSILParser::TopLevelDeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitTopLevelDeclaration(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::TopLevelDeclarationContext* GSILParser::topLevelDeclaration() {
  TopLevelDeclarationContext *_localctx = _tracker.createInstance<TopLevelDeclarationContext>(_ctx, getState());
  enterRule(_localctx, 90, GSILParser::RuleTopLevelDeclaration);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(413);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 30, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(402);
      rasterizerStateDefinition();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(403);
      blendStateDefinition();
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(404);
      samplerDefinition();
      break;
    }

    case 4: {
      enterOuterAlt(_localctx, 4);
      setState(405);
      depthStencilStateDefinition();
      break;
    }

    case 5: {
      enterOuterAlt(_localctx, 5);
      setState(406);
      textureDefinition();
      break;
    }

    case 6: {
      enterOuterAlt(_localctx, 6);
      setState(407);
      uavDefinition();
      break;
    }

    case 7: {
      enterOuterAlt(_localctx, 7);
      setState(408);
      techniqueGroupDefinition();
      break;
    }

    case 8: {
      enterOuterAlt(_localctx, 8);
      setState(409);
      techniqueDefinition();
      break;
    }

    case 9: {
      enterOuterAlt(_localctx, 9);
      setState(410);
      structDefinition();
      break;
    }

    case 10: {
      enterOuterAlt(_localctx, 10);
      setState(411);
      cbufferDefinition();
      break;
    }

    case 11: {
      enterOuterAlt(_localctx, 11);
      setState(412);
      functionDefinition();
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CompilationUnitContext ------------------------------------------------------------------

GSILParser::CompilationUnitContext::CompilationUnitContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* GSILParser::CompilationUnitContext::EOF() {
  return getToken(GSILParser::EOF, 0);
}

std::vector<GSILParser::TopLevelDeclarationContext *> GSILParser::CompilationUnitContext::topLevelDeclaration() {
  return getRuleContexts<GSILParser::TopLevelDeclarationContext>();
}

GSILParser::TopLevelDeclarationContext* GSILParser::CompilationUnitContext::topLevelDeclaration(size_t i) {
  return getRuleContext<GSILParser::TopLevelDeclarationContext>(i);
}


size_t GSILParser::CompilationUnitContext::getRuleIndex() const {
  return GSILParser::RuleCompilationUnit;
}


std::any GSILParser::CompilationUnitContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<GSILParserVisitor*>(visitor))
    return parserVisitor->visitCompilationUnit(this);
  else
    return visitor->visitChildren(this);
}

GSILParser::CompilationUnitContext* GSILParser::compilationUnit() {
  CompilationUnitContext *_localctx = _tracker.createInstance<CompilationUnitContext>(_ctx, getState());
  enterRule(_localctx, 92, GSILParser::RuleCompilationUnit);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(418);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & 30072764512) != 0) || _la == GSILParser::Identifier) {
      setState(415);
      antlrcpp::downCast<CompilationUnitContext *>(_localctx)->topLevelDeclarationContext = topLevelDeclaration();
      antlrcpp::downCast<CompilationUnitContext *>(_localctx)->Declarations.push_back(antlrcpp::downCast<CompilationUnitContext *>(_localctx)->topLevelDeclarationContext);
      setState(420);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(421);
    antlrcpp::downCast<CompilationUnitContext *>(_localctx)->EndOfFileToken = match(GSILParser::EOF);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

void GSILParser::initialize() {
#if ANTLR4_USE_THREAD_LOCAL_CACHE
  gsilparserParserInitialize();
#else
  ::antlr4::internal::call_once(gsilparserParserOnceFlag, gsilparserParserInitialize);
#endif
}
