
// Generated from .\GSILParser.g4 by ANTLR 4.11.1

#pragma once


#include "antlr4-runtime.h"
#include "GSILParser.h"


/**
 * This interface defines an abstract listener for a parse tree produced by GSILParser.
 */
class  GSILParserListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterLineSep(GSILParser::LineSepContext *ctx) = 0;
  virtual void exitLineSep(GSILParser::LineSepContext *ctx) = 0;

  virtual void enterVariableArrayPart(GSILParser::VariableArrayPartContext *ctx) = 0;
  virtual void exitVariableArrayPart(GSILParser::VariableArrayPartContext *ctx) = 0;

  virtual void enterSimpleVariableDefinition(GSILParser::SimpleVariableDefinitionContext *ctx) = 0;
  virtual void exitSimpleVariableDefinition(GSILParser::SimpleVariableDefinitionContext *ctx) = 0;

  virtual void enterPropertyDefinition(GSILParser::PropertyDefinitionContext *ctx) = 0;
  virtual void exitPropertyDefinition(GSILParser::PropertyDefinitionContext *ctx) = 0;

  virtual void enterStatePropertyName(GSILParser::StatePropertyNameContext *ctx) = 0;
  virtual void exitStatePropertyName(GSILParser::StatePropertyNameContext *ctx) = 0;

  virtual void enterStatePropertySetter(GSILParser::StatePropertySetterContext *ctx) = 0;
  virtual void exitStatePropertySetter(GSILParser::StatePropertySetterContext *ctx) = 0;

  virtual void enterStateDefinition(GSILParser::StateDefinitionContext *ctx) = 0;
  virtual void exitStateDefinition(GSILParser::StateDefinitionContext *ctx) = 0;

  virtual void enterMaxVertexCount(GSILParser::MaxVertexCountContext *ctx) = 0;
  virtual void exitMaxVertexCount(GSILParser::MaxVertexCountContext *ctx) = 0;

  virtual void enterThreads(GSILParser::ThreadsContext *ctx) = 0;
  virtual void exitThreads(GSILParser::ThreadsContext *ctx) = 0;

  virtual void enterAttribute(GSILParser::AttributeContext *ctx) = 0;
  virtual void exitAttribute(GSILParser::AttributeContext *ctx) = 0;

  virtual void enterCommaAttribute(GSILParser::CommaAttributeContext *ctx) = 0;
  virtual void exitCommaAttribute(GSILParser::CommaAttributeContext *ctx) = 0;

  virtual void enterAttributeCommaList(GSILParser::AttributeCommaListContext *ctx) = 0;
  virtual void exitAttributeCommaList(GSILParser::AttributeCommaListContext *ctx) = 0;

  virtual void enterAttributeDefinition(GSILParser::AttributeDefinitionContext *ctx) = 0;
  virtual void exitAttributeDefinition(GSILParser::AttributeDefinitionContext *ctx) = 0;

  virtual void enterShaderInputAccessSpecifier(GSILParser::ShaderInputAccessSpecifierContext *ctx) = 0;
  virtual void exitShaderInputAccessSpecifier(GSILParser::ShaderInputAccessSpecifierContext *ctx) = 0;

  virtual void enterRasterizerStateDefinition(GSILParser::RasterizerStateDefinitionContext *ctx) = 0;
  virtual void exitRasterizerStateDefinition(GSILParser::RasterizerStateDefinitionContext *ctx) = 0;

  virtual void enterBlendStateDefinition(GSILParser::BlendStateDefinitionContext *ctx) = 0;
  virtual void exitBlendStateDefinition(GSILParser::BlendStateDefinitionContext *ctx) = 0;

  virtual void enterSamplerDefinition(GSILParser::SamplerDefinitionContext *ctx) = 0;
  virtual void exitSamplerDefinition(GSILParser::SamplerDefinitionContext *ctx) = 0;

  virtual void enterDepthStencilStateDefinition(GSILParser::DepthStencilStateDefinitionContext *ctx) = 0;
  virtual void exitDepthStencilStateDefinition(GSILParser::DepthStencilStateDefinitionContext *ctx) = 0;

  virtual void enterTextureDefinition(GSILParser::TextureDefinitionContext *ctx) = 0;
  virtual void exitTextureDefinition(GSILParser::TextureDefinitionContext *ctx) = 0;

  virtual void enterUavDefinition(GSILParser::UavDefinitionContext *ctx) = 0;
  virtual void exitUavDefinition(GSILParser::UavDefinitionContext *ctx) = 0;

  virtual void enterStateNames(GSILParser::StateNamesContext *ctx) = 0;
  virtual void exitStateNames(GSILParser::StateNamesContext *ctx) = 0;

  virtual void enterCompileShader(GSILParser::CompileShaderContext *ctx) = 0;
  virtual void exitCompileShader(GSILParser::CompileShaderContext *ctx) = 0;

  virtual void enterPassShaderKeyValuePair(GSILParser::PassShaderKeyValuePairContext *ctx) = 0;
  virtual void exitPassShaderKeyValuePair(GSILParser::PassShaderKeyValuePairContext *ctx) = 0;

  virtual void enterPassStateKeyValuePair(GSILParser::PassStateKeyValuePairContext *ctx) = 0;
  virtual void exitPassStateKeyValuePair(GSILParser::PassStateKeyValuePairContext *ctx) = 0;

  virtual void enterPassKeyValuePair(GSILParser::PassKeyValuePairContext *ctx) = 0;
  virtual void exitPassKeyValuePair(GSILParser::PassKeyValuePairContext *ctx) = 0;

  virtual void enterTagValue(GSILParser::TagValueContext *ctx) = 0;
  virtual void exitTagValue(GSILParser::TagValueContext *ctx) = 0;

  virtual void enterTagDefinition(GSILParser::TagDefinitionContext *ctx) = 0;
  virtual void exitTagDefinition(GSILParser::TagDefinitionContext *ctx) = 0;

  virtual void enterPassDefinition(GSILParser::PassDefinitionContext *ctx) = 0;
  virtual void exitPassDefinition(GSILParser::PassDefinitionContext *ctx) = 0;

  virtual void enterTechniqueDefinition(GSILParser::TechniqueDefinitionContext *ctx) = 0;
  virtual void exitTechniqueDefinition(GSILParser::TechniqueDefinitionContext *ctx) = 0;

  virtual void enterTechniqueGroupDefinition(GSILParser::TechniqueGroupDefinitionContext *ctx) = 0;
  virtual void exitTechniqueGroupDefinition(GSILParser::TechniqueGroupDefinitionContext *ctx) = 0;

  virtual void enterStructSpecifier(GSILParser::StructSpecifierContext *ctx) = 0;
  virtual void exitStructSpecifier(GSILParser::StructSpecifierContext *ctx) = 0;

  virtual void enterFieldSpecifier(GSILParser::FieldSpecifierContext *ctx) = 0;
  virtual void exitFieldSpecifier(GSILParser::FieldSpecifierContext *ctx) = 0;

  virtual void enterStructField(GSILParser::StructFieldContext *ctx) = 0;
  virtual void exitStructField(GSILParser::StructFieldContext *ctx) = 0;

  virtual void enterStructDefinition(GSILParser::StructDefinitionContext *ctx) = 0;
  virtual void exitStructDefinition(GSILParser::StructDefinitionContext *ctx) = 0;

  virtual void enterCbufferDefinition(GSILParser::CbufferDefinitionContext *ctx) = 0;
  virtual void exitCbufferDefinition(GSILParser::CbufferDefinitionContext *ctx) = 0;

  virtual void enterValidInFunctionDef(GSILParser::ValidInFunctionDefContext *ctx) = 0;
  virtual void exitValidInFunctionDef(GSILParser::ValidInFunctionDefContext *ctx) = 0;

  virtual void enterBlock(GSILParser::BlockContext *ctx) = 0;
  virtual void exitBlock(GSILParser::BlockContext *ctx) = 0;

  virtual void enterFunctionParameter(GSILParser::FunctionParameterContext *ctx) = 0;
  virtual void exitFunctionParameter(GSILParser::FunctionParameterContext *ctx) = 0;

  virtual void enterCommaFunctionParameter(GSILParser::CommaFunctionParameterContext *ctx) = 0;
  virtual void exitCommaFunctionParameter(GSILParser::CommaFunctionParameterContext *ctx) = 0;

  virtual void enterFunctionParameterList(GSILParser::FunctionParameterListContext *ctx) = 0;
  virtual void exitFunctionParameterList(GSILParser::FunctionParameterListContext *ctx) = 0;

  virtual void enterFunctionDefinition(GSILParser::FunctionDefinitionContext *ctx) = 0;
  virtual void exitFunctionDefinition(GSILParser::FunctionDefinitionContext *ctx) = 0;

  virtual void enterTopLevelDeclaration(GSILParser::TopLevelDeclarationContext *ctx) = 0;
  virtual void exitTopLevelDeclaration(GSILParser::TopLevelDeclarationContext *ctx) = 0;

  virtual void enterCompilationUnit(GSILParser::CompilationUnitContext *ctx) = 0;
  virtual void exitCompilationUnit(GSILParser::CompilationUnitContext *ctx) = 0;


};

