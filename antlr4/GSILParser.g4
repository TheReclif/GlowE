parser grammar GSILParser;
options
{
    tokenVocab = GSILLexer;
}

lineSep: Semi;

variableType: Type=(Identifier | VectorType | Matrix | DMatrix);
variableArrayPart: LeftBracket IntegralLiteral RightBracket;
simpleVariableDefinition: TypeModifiers+=TypeModifier* Type=variableType Name=Identifier ArrayParts+=variableArrayPart*;

propertyColorValue: Red=IntegralLiteral Colon Green=IntegralLiteral Colon Blue=IntegralLiteral (Colon Alpha=IntegralLiteral)?;
propertyDefinition: Colon PropertySpecifier LeftParen EditorName=StringLiteral RightParen;
statePropertyName: Name=Identifier variableArrayPart?;
statePropertyValue: Identifier | IntegralLiteral | False | True | propertyColorValue;
statePropertySetter: Property=statePropertyName Assign Value=statePropertyValue lineSep;
stateDefinition: Name=Identifier LeftBrace Properties+=statePropertySetter* RightBrace lineSep;

maxVertexCount: MaxVertexCount LeftParen IntegralLiteral RightParen;
threads: Threads LeftParen X=IntegralLiteral Comma Y=IntegralLiteral Comma Z=IntegralLiteral RightParen;

attribute: EarlyZ | maxVertexCount | threads;
attributeDefinition: LeftBracket Attribute=attribute RightBracket;
shaderInputAccessSpecifier: Hidden | EngineSupplied;

rasterizerStateDefinition: RasterizerState Def=stateDefinition;
blendStateDefinition: BlendState Def=stateDefinition;
samplerDefinition: Sampler Def=stateDefinition;
depthStencilStateDefinition: DepthStencilState Def=stateDefinition;
textureDefinition: InputSpecifier=shaderInputAccessSpecifier? TextureType Name=Identifier LeftParen SamplerName=Identifier RightParen propertyDefinition? lineSep;
uavUnderlayingType: VectorType;
uavDefinition: InputSpecifier=shaderInputAccessSpecifier? UAVType Less Type=uavUnderlayingType Greater Name=Identifier propertyDefinition? lineSep;

// Techniques.
stateNames: BlendState | RasterizerState | DepthStencilState;
compileShaderReplacement: Comma Replacement=(IntegralLiteral | FloatLiteral);
compileShader: CompileShader LeftParen Version=FloatLiteral Comma MainFunc=Identifier Replacements+=compileShaderReplacement* RightParen;

passShaderKeyValuePair: ShaderName Assign compileShader;
passStateKeyValuePair: stateNames Assign Identifier;
passKeyValuePair: (passShaderKeyValuePair | passStateKeyValuePair) lineSep;

tagValue: Identifier | IntegralLiteral | FloatLiteral;
tagDefinition: Tag LeftParen Key=Identifier Colon Value=tagValue RightParen lineSep;

passDefinition: Pass Id=IntegralLiteral LeftBrace Values+=passKeyValuePair* RightBrace lineSep;

techniqueDefinition: Technique Name=Identifier LeftBrace Tags+=tagDefinition* Passes+=passDefinition* RightBrace lineSep;
techniqueGroupDefinition: TechniqueGroup Name=Identifier LeftBrace Techniques+=techniqueDefinition+ RightBrace lineSep;

structSpecifier: InOut;
fieldSpecifier: SVSpecifier | Instanced | shaderInputAccessSpecifier | InterpolationModeSpecifier;
structField: InputSpecifier+=fieldSpecifier* simpleVariableDefinition propertyDefinition? lineSep;
structDefinition: Struct Specifiers=structSpecifier? Name=Identifier LeftBrace Fields+=structField* RightBrace lineSep;

cbufferDefinition: CBuffer Name=Identifier LeftBrace Fields+=structField* RightBrace lineSep;

validInFunctionDef: ~(RightBrace | CBuffer | RasterizerState | BlendState | DepthStencilState | Sampler | CompileShader | Struct | Tag | Pass | UAVType | TextureType | Hidden | EngineSupplied | Technique | TechniqueGroup | Uniform | ShaderName);
blockContent: (block | validInFunctionDef)*;
block: LeftBrace Content=blockContent RightBrace;
functionParameter: IsUniform=Uniform? Type=variableType Name=Identifier ArrayPart+=variableArrayPart*;
commaFunctionParameter: Comma Parameter=functionParameter;
functionParameterList: FirstParameter=functionParameter MoreParameters+=commaFunctionParameter*;
functionDefinition: ReturnType=variableType Attributes+=attributeDefinition* Name=Identifier LeftParen Parameters=functionParameterList? RightParen Code=block;

topLevelDeclaration: rasterizerStateDefinition | blendStateDefinition | samplerDefinition | depthStencilStateDefinition | textureDefinition | uavDefinition | techniqueGroupDefinition | techniqueDefinition | structDefinition | cbufferDefinition | functionDefinition;

compilationUnit: Declarations+=topLevelDeclaration* EndOfFileToken=EOF;
