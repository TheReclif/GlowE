
// Generated from F:/Dokumenty/GlowE/antlr4/GSILParser.g4 by ANTLR 4.13.0

#pragma once


#include "antlr4-runtime.h"
#include "GSILParser.h"


namespace GSIL {

/**
 * This class defines an abstract visitor for a parse tree
 * produced by GSILParser.
 */
class  GSILParserVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by GSILParser.
   */
    virtual std::any visitLineSep(GSILParser::LineSepContext *context) = 0;

    virtual std::any visitVariableType(GSILParser::VariableTypeContext *context) = 0;

    virtual std::any visitVariableArrayPart(GSILParser::VariableArrayPartContext *context) = 0;

    virtual std::any visitSimpleVariableDefinition(GSILParser::SimpleVariableDefinitionContext *context) = 0;

    virtual std::any visitPropertyColorValue(GSILParser::PropertyColorValueContext *context) = 0;

    virtual std::any visitPropertyDefinition(GSILParser::PropertyDefinitionContext *context) = 0;

    virtual std::any visitStatePropertyName(GSILParser::StatePropertyNameContext *context) = 0;

    virtual std::any visitStatePropertyValue(GSILParser::StatePropertyValueContext *context) = 0;

    virtual std::any visitStatePropertySetter(GSILParser::StatePropertySetterContext *context) = 0;

    virtual std::any visitStateDefinition(GSILParser::StateDefinitionContext *context) = 0;

    virtual std::any visitMaxVertexCount(GSILParser::MaxVertexCountContext *context) = 0;

    virtual std::any visitThreads(GSILParser::ThreadsContext *context) = 0;

    virtual std::any visitAttribute(GSILParser::AttributeContext *context) = 0;

    virtual std::any visitAttributeDefinition(GSILParser::AttributeDefinitionContext *context) = 0;

    virtual std::any visitShaderInputAccessSpecifier(GSILParser::ShaderInputAccessSpecifierContext *context) = 0;

    virtual std::any visitRasterizerStateDefinition(GSILParser::RasterizerStateDefinitionContext *context) = 0;

    virtual std::any visitBlendStateDefinition(GSILParser::BlendStateDefinitionContext *context) = 0;

    virtual std::any visitSamplerDefinition(GSILParser::SamplerDefinitionContext *context) = 0;

    virtual std::any visitDepthStencilStateDefinition(GSILParser::DepthStencilStateDefinitionContext *context) = 0;

    virtual std::any visitTextureDefinition(GSILParser::TextureDefinitionContext *context) = 0;

    virtual std::any visitUavUnderlayingType(GSILParser::UavUnderlayingTypeContext *context) = 0;

    virtual std::any visitUavDefinition(GSILParser::UavDefinitionContext *context) = 0;

    virtual std::any visitStateNames(GSILParser::StateNamesContext *context) = 0;

    virtual std::any visitCompileShaderReplacement(GSILParser::CompileShaderReplacementContext *context) = 0;

    virtual std::any visitCompileShader(GSILParser::CompileShaderContext *context) = 0;

    virtual std::any visitPassShaderKeyValuePair(GSILParser::PassShaderKeyValuePairContext *context) = 0;

    virtual std::any visitPassStateKeyValuePair(GSILParser::PassStateKeyValuePairContext *context) = 0;

    virtual std::any visitPassKeyValuePair(GSILParser::PassKeyValuePairContext *context) = 0;

    virtual std::any visitTagValue(GSILParser::TagValueContext *context) = 0;

    virtual std::any visitTagDefinition(GSILParser::TagDefinitionContext *context) = 0;

    virtual std::any visitPassDefinition(GSILParser::PassDefinitionContext *context) = 0;

    virtual std::any visitTechniqueDefinition(GSILParser::TechniqueDefinitionContext *context) = 0;

    virtual std::any visitTechniqueGroupDefinition(GSILParser::TechniqueGroupDefinitionContext *context) = 0;

    virtual std::any visitStructSpecifier(GSILParser::StructSpecifierContext *context) = 0;

    virtual std::any visitFieldSpecifier(GSILParser::FieldSpecifierContext *context) = 0;

    virtual std::any visitStructField(GSILParser::StructFieldContext *context) = 0;

    virtual std::any visitStructDefinition(GSILParser::StructDefinitionContext *context) = 0;

    virtual std::any visitCbufferDefinition(GSILParser::CbufferDefinitionContext *context) = 0;

    virtual std::any visitValidInFunctionDef(GSILParser::ValidInFunctionDefContext *context) = 0;

    virtual std::any visitBlockContent(GSILParser::BlockContentContext *context) = 0;

    virtual std::any visitBlock(GSILParser::BlockContext *context) = 0;

    virtual std::any visitFunctionParameter(GSILParser::FunctionParameterContext *context) = 0;

    virtual std::any visitCommaFunctionParameter(GSILParser::CommaFunctionParameterContext *context) = 0;

    virtual std::any visitFunctionParameterList(GSILParser::FunctionParameterListContext *context) = 0;

    virtual std::any visitFunctionDefinition(GSILParser::FunctionDefinitionContext *context) = 0;

    virtual std::any visitTopLevelDeclaration(GSILParser::TopLevelDeclarationContext *context) = 0;

    virtual std::any visitCompilationUnit(GSILParser::CompilationUnitContext *context) = 0;


};

}  // namespace GSIL
