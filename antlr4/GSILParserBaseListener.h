
// Generated from .\GSILParser.g4 by ANTLR 4.11.1

#pragma once


#include "antlr4-runtime.h"
#include "GSILParserListener.h"


/**
 * This class provides an empty implementation of GSILParserListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class  GSILParserBaseListener : public GSILParserListener {
public:

  virtual void enterLineSep(GSILParser::LineSepContext * /*ctx*/) override { }
  virtual void exitLineSep(GSILParser::LineSepContext * /*ctx*/) override { }

  virtual void enterVariableArrayPart(GSILParser::VariableArrayPartContext * /*ctx*/) override { }
  virtual void exitVariableArrayPart(GSILParser::VariableArrayPartContext * /*ctx*/) override { }

  virtual void enterSimpleVariableDefinition(GSILParser::SimpleVariableDefinitionContext * /*ctx*/) override { }
  virtual void exitSimpleVariableDefinition(GSILParser::SimpleVariableDefinitionContext * /*ctx*/) override { }

  virtual void enterPropertyDefinition(GSILParser::PropertyDefinitionContext * /*ctx*/) override { }
  virtual void exitPropertyDefinition(GSILParser::PropertyDefinitionContext * /*ctx*/) override { }

  virtual void enterStatePropertyName(GSILParser::StatePropertyNameContext * /*ctx*/) override { }
  virtual void exitStatePropertyName(GSILParser::StatePropertyNameContext * /*ctx*/) override { }

  virtual void enterStatePropertySetter(GSILParser::StatePropertySetterContext * /*ctx*/) override { }
  virtual void exitStatePropertySetter(GSILParser::StatePropertySetterContext * /*ctx*/) override { }

  virtual void enterStateDefinition(GSILParser::StateDefinitionContext * /*ctx*/) override { }
  virtual void exitStateDefinition(GSILParser::StateDefinitionContext * /*ctx*/) override { }

  virtual void enterMaxVertexCount(GSILParser::MaxVertexCountContext * /*ctx*/) override { }
  virtual void exitMaxVertexCount(GSILParser::MaxVertexCountContext * /*ctx*/) override { }

  virtual void enterThreads(GSILParser::ThreadsContext * /*ctx*/) override { }
  virtual void exitThreads(GSILParser::ThreadsContext * /*ctx*/) override { }

  virtual void enterAttribute(GSILParser::AttributeContext * /*ctx*/) override { }
  virtual void exitAttribute(GSILParser::AttributeContext * /*ctx*/) override { }

  virtual void enterCommaAttribute(GSILParser::CommaAttributeContext * /*ctx*/) override { }
  virtual void exitCommaAttribute(GSILParser::CommaAttributeContext * /*ctx*/) override { }

  virtual void enterAttributeCommaList(GSILParser::AttributeCommaListContext * /*ctx*/) override { }
  virtual void exitAttributeCommaList(GSILParser::AttributeCommaListContext * /*ctx*/) override { }

  virtual void enterAttributeDefinition(GSILParser::AttributeDefinitionContext * /*ctx*/) override { }
  virtual void exitAttributeDefinition(GSILParser::AttributeDefinitionContext * /*ctx*/) override { }

  virtual void enterShaderInputAccessSpecifier(GSILParser::ShaderInputAccessSpecifierContext * /*ctx*/) override { }
  virtual void exitShaderInputAccessSpecifier(GSILParser::ShaderInputAccessSpecifierContext * /*ctx*/) override { }

  virtual void enterRasterizerStateDefinition(GSILParser::RasterizerStateDefinitionContext * /*ctx*/) override { }
  virtual void exitRasterizerStateDefinition(GSILParser::RasterizerStateDefinitionContext * /*ctx*/) override { }

  virtual void enterBlendStateDefinition(GSILParser::BlendStateDefinitionContext * /*ctx*/) override { }
  virtual void exitBlendStateDefinition(GSILParser::BlendStateDefinitionContext * /*ctx*/) override { }

  virtual void enterSamplerDefinition(GSILParser::SamplerDefinitionContext * /*ctx*/) override { }
  virtual void exitSamplerDefinition(GSILParser::SamplerDefinitionContext * /*ctx*/) override { }

  virtual void enterDepthStencilStateDefinition(GSILParser::DepthStencilStateDefinitionContext * /*ctx*/) override { }
  virtual void exitDepthStencilStateDefinition(GSILParser::DepthStencilStateDefinitionContext * /*ctx*/) override { }

  virtual void enterTextureDefinition(GSILParser::TextureDefinitionContext * /*ctx*/) override { }
  virtual void exitTextureDefinition(GSILParser::TextureDefinitionContext * /*ctx*/) override { }

  virtual void enterUavDefinition(GSILParser::UavDefinitionContext * /*ctx*/) override { }
  virtual void exitUavDefinition(GSILParser::UavDefinitionContext * /*ctx*/) override { }

  virtual void enterStateNames(GSILParser::StateNamesContext * /*ctx*/) override { }
  virtual void exitStateNames(GSILParser::StateNamesContext * /*ctx*/) override { }

  virtual void enterCompileShader(GSILParser::CompileShaderContext * /*ctx*/) override { }
  virtual void exitCompileShader(GSILParser::CompileShaderContext * /*ctx*/) override { }

  virtual void enterPassShaderKeyValuePair(GSILParser::PassShaderKeyValuePairContext * /*ctx*/) override { }
  virtual void exitPassShaderKeyValuePair(GSILParser::PassShaderKeyValuePairContext * /*ctx*/) override { }

  virtual void enterPassStateKeyValuePair(GSILParser::PassStateKeyValuePairContext * /*ctx*/) override { }
  virtual void exitPassStateKeyValuePair(GSILParser::PassStateKeyValuePairContext * /*ctx*/) override { }

  virtual void enterPassKeyValuePair(GSILParser::PassKeyValuePairContext * /*ctx*/) override { }
  virtual void exitPassKeyValuePair(GSILParser::PassKeyValuePairContext * /*ctx*/) override { }

  virtual void enterTagValue(GSILParser::TagValueContext * /*ctx*/) override { }
  virtual void exitTagValue(GSILParser::TagValueContext * /*ctx*/) override { }

  virtual void enterTagDefinition(GSILParser::TagDefinitionContext * /*ctx*/) override { }
  virtual void exitTagDefinition(GSILParser::TagDefinitionContext * /*ctx*/) override { }

  virtual void enterPassDefinition(GSILParser::PassDefinitionContext * /*ctx*/) override { }
  virtual void exitPassDefinition(GSILParser::PassDefinitionContext * /*ctx*/) override { }

  virtual void enterTechniqueDefinition(GSILParser::TechniqueDefinitionContext * /*ctx*/) override { }
  virtual void exitTechniqueDefinition(GSILParser::TechniqueDefinitionContext * /*ctx*/) override { }

  virtual void enterTechniqueGroupDefinition(GSILParser::TechniqueGroupDefinitionContext * /*ctx*/) override { }
  virtual void exitTechniqueGroupDefinition(GSILParser::TechniqueGroupDefinitionContext * /*ctx*/) override { }

  virtual void enterStructSpecifier(GSILParser::StructSpecifierContext * /*ctx*/) override { }
  virtual void exitStructSpecifier(GSILParser::StructSpecifierContext * /*ctx*/) override { }

  virtual void enterFieldSpecifier(GSILParser::FieldSpecifierContext * /*ctx*/) override { }
  virtual void exitFieldSpecifier(GSILParser::FieldSpecifierContext * /*ctx*/) override { }

  virtual void enterStructField(GSILParser::StructFieldContext * /*ctx*/) override { }
  virtual void exitStructField(GSILParser::StructFieldContext * /*ctx*/) override { }

  virtual void enterStructDefinition(GSILParser::StructDefinitionContext * /*ctx*/) override { }
  virtual void exitStructDefinition(GSILParser::StructDefinitionContext * /*ctx*/) override { }

  virtual void enterCbufferDefinition(GSILParser::CbufferDefinitionContext * /*ctx*/) override { }
  virtual void exitCbufferDefinition(GSILParser::CbufferDefinitionContext * /*ctx*/) override { }

  virtual void enterValidInFunctionDef(GSILParser::ValidInFunctionDefContext * /*ctx*/) override { }
  virtual void exitValidInFunctionDef(GSILParser::ValidInFunctionDefContext * /*ctx*/) override { }

  virtual void enterBlock(GSILParser::BlockContext * /*ctx*/) override { }
  virtual void exitBlock(GSILParser::BlockContext * /*ctx*/) override { }

  virtual void enterFunctionParameter(GSILParser::FunctionParameterContext * /*ctx*/) override { }
  virtual void exitFunctionParameter(GSILParser::FunctionParameterContext * /*ctx*/) override { }

  virtual void enterCommaFunctionParameter(GSILParser::CommaFunctionParameterContext * /*ctx*/) override { }
  virtual void exitCommaFunctionParameter(GSILParser::CommaFunctionParameterContext * /*ctx*/) override { }

  virtual void enterFunctionParameterList(GSILParser::FunctionParameterListContext * /*ctx*/) override { }
  virtual void exitFunctionParameterList(GSILParser::FunctionParameterListContext * /*ctx*/) override { }

  virtual void enterFunctionDefinition(GSILParser::FunctionDefinitionContext * /*ctx*/) override { }
  virtual void exitFunctionDefinition(GSILParser::FunctionDefinitionContext * /*ctx*/) override { }

  virtual void enterTopLevelDeclaration(GSILParser::TopLevelDeclarationContext * /*ctx*/) override { }
  virtual void exitTopLevelDeclaration(GSILParser::TopLevelDeclarationContext * /*ctx*/) override { }

  virtual void enterCompilationUnit(GSILParser::CompilationUnitContext * /*ctx*/) override { }
  virtual void exitCompilationUnit(GSILParser::CompilationUnitContext * /*ctx*/) override { }


  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

};

