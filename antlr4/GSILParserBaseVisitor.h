
// Generated from F:/Dokumenty/GlowE/antlr4/GSILParser.g4 by ANTLR 4.13.0

#pragma once


#include "antlr4-runtime.h"
#include "GSILParserVisitor.h"


namespace GSIL {

/**
 * This class provides an empty implementation of GSILParserVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  GSILParserBaseVisitor : public GSILParserVisitor {
public:

  virtual std::any visitLineSep(GSILParser::LineSepContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitVariableType(GSILParser::VariableTypeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitVariableArrayPart(GSILParser::VariableArrayPartContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitSimpleVariableDefinition(GSILParser::SimpleVariableDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitPropertyColorValue(GSILParser::PropertyColorValueContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitPropertyDefinition(GSILParser::PropertyDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitStatePropertyName(GSILParser::StatePropertyNameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitStatePropertyValue(GSILParser::StatePropertyValueContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitStatePropertySetter(GSILParser::StatePropertySetterContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitStateDefinition(GSILParser::StateDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitMaxVertexCount(GSILParser::MaxVertexCountContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitThreads(GSILParser::ThreadsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitAttribute(GSILParser::AttributeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitAttributeDefinition(GSILParser::AttributeDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitShaderInputAccessSpecifier(GSILParser::ShaderInputAccessSpecifierContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitRasterizerStateDefinition(GSILParser::RasterizerStateDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitBlendStateDefinition(GSILParser::BlendStateDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitSamplerDefinition(GSILParser::SamplerDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitDepthStencilStateDefinition(GSILParser::DepthStencilStateDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitTextureDefinition(GSILParser::TextureDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitUavUnderlayingType(GSILParser::UavUnderlayingTypeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitUavDefinition(GSILParser::UavDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitStateNames(GSILParser::StateNamesContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitCompileShaderReplacement(GSILParser::CompileShaderReplacementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitCompileShader(GSILParser::CompileShaderContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitPassShaderKeyValuePair(GSILParser::PassShaderKeyValuePairContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitPassStateKeyValuePair(GSILParser::PassStateKeyValuePairContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitPassKeyValuePair(GSILParser::PassKeyValuePairContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitTagValue(GSILParser::TagValueContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitTagDefinition(GSILParser::TagDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitPassDefinition(GSILParser::PassDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitTechniqueDefinition(GSILParser::TechniqueDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitTechniqueGroupDefinition(GSILParser::TechniqueGroupDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitStructSpecifier(GSILParser::StructSpecifierContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitFieldSpecifier(GSILParser::FieldSpecifierContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitStructField(GSILParser::StructFieldContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitStructDefinition(GSILParser::StructDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitCbufferDefinition(GSILParser::CbufferDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitValidInFunctionDef(GSILParser::ValidInFunctionDefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitBlockContent(GSILParser::BlockContentContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitBlock(GSILParser::BlockContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitFunctionParameter(GSILParser::FunctionParameterContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitCommaFunctionParameter(GSILParser::CommaFunctionParameterContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitFunctionParameterList(GSILParser::FunctionParameterListContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitFunctionDefinition(GSILParser::FunctionDefinitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitTopLevelDeclaration(GSILParser::TopLevelDeclarationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual std::any visitCompilationUnit(GSILParser::CompilationUnitContext *ctx) override {
    return visitChildren(ctx);
  }


};

}  // namespace GSIL
