lexer grammar GSILLexer;

WhiteSpace: [ \t\r\n]+ -> skip;
MultiLineComment: '/*' .*? '*/' -> skip;
SingleLineComment: '//' ~ [\r\n]* -> skip;

fragment Letter: [A-Za-z_];
fragment AlphaNumLetter: [A-Za-z_0-9];
fragment Digit: [0-9];
fragment HexDigit: [A-Fa-f0-9];
fragment NonDigit: [a-zA-Z_];

// Keywords.
// GSIL specific.
CodeSnippetPrefix: 'HLSL' | 'GLSL' | 'MSL';
CBuffer: 'CBuffer';
Struct: 'Struct';
InOut: 'InOut';
Out: 'Out';
GeometryShaderInputSpecifier: 'Point' | 'Line' | 'LineAdj' | 'Triangle' | 'TriangleAdj';
SVSpecifier: 'SV';
EngineSupplied: 'EngineSupplied';
Hidden: 'Hidden';
Sampler: 'Sampler';
BlendState: 'BlendState' | 'BS';
DepthStencilState: 'DepthStencilState' | 'DSS';
RasterizerState: 'RasterizerState' | 'RS';
PropertySpecifier: 'Property';
Instanced: 'Instanced';
TextureType: 'Texture' ('1D' | '2D' | '3D' | 'Cube' | '1DArray' | '2DArray' | '1DMultiSample' | '2DMultiSample');
UAVType: 'RW' (TextureType | 'Buffer');
TechniqueGroup: 'TechniqueGroup';
Technique: 'Technique';
Pass: 'Pass';
CompileShader: 'CompileShader';
Tag: 'Tag';
Threads: 'Threads';
EarlyZ: 'EarlyZ';
MaxVertexCount: 'MaxVertexCount';
InterpolationModeSpecifier: 'NoInterpolation' | 'NoPerspective' | 'Smooth';
Uniform: 'Uniform';
fragment ShaderType: 'Vertex' | 'Pixel' | 'Compute' | 'Geometry' | 'Hull' | 'Domain';
ShaderName: ShaderType 'Shader';
fragment Bool: 'Bool';
fragment Float: 'Float';
fragment Int: 'Int';
fragment UInt: 'UInt';
fragment Double: 'Double';
VectorType: (Bool | Float | Int | UInt | Double) Digit?;
Matrix: ('Matrix' | 'Mat') Digit 'x' Digit;
DMatrix: ('DMatrix' | 'DMat') Digit 'x' Digit;
// Common.
False: 'false';
True: 'true';
Break: 'break';
Continue: 'continue';
Discard: 'discard';
Do: 'do';
If: 'if';
Switch: 'switch';
While: 'while';
Case: 'case';
Default: 'default';
For: 'for';
Else: 'else';
Return: 'return';
// Variable definition.
StorageClass: 'extern' | 'nointerpolation' | 'precise' | 'shared' | 'groupshared' | 'static' | 'uniform' | 'volatile';
TypeModifier: 'const' | 'row_major' | 'column_major';

// Operators.
LeftParen : '(';
RightParen : ')';
LeftBracket : '[';
RightBracket : ']';
LeftBrace : '{';
RightBrace : '}';

Less : '<';
LessEqual : '<=';
Greater : '>';
GreaterEqual : '>=';
LeftShift : '<<';
RightShift : '>>';

Plus : '+';
PlusPlus : '++';
Minus : '-';
MinusMinus : '--';
Star : '*';
Div : '/';
Mod : '%';

And : '&';
Or : '|';
AndAnd : '&&';
OrOr : '||';
Caret : '^';
Not : '!';
Tilde : '~';

Question : '?';
Colon : ':';
ColonColon : '::';
Semi : ';';
Comma : ',';

Assign : '=';
// '*=' | '/=' | '%=' | '+=' | '-=' | '<<=' | '>>=' | '&=' | '^=' | '|='
StarAssign : '*=';
DivAssign : '/=';
ModAssign : '%=';
PlusAssign : '+=';
MinusAssign : '-=';
LeftShiftAssign : '<<=';
RightShiftAssign : '>>=';
AndAssign : '&=';
XorAssign : '^=';
OrAssign : '|=';

Equal : '==';
NotEqual : '!=';

Dot : '.';

// Literals.
fragment IntegerSuffix: [uUlL];
fragment DecimalIntegralLiteral: Digit+;
fragment HexadecimalIntegralLiteral : ('0x' | '0X') HexDigit+;

IntegralLiteral: DecimalIntegralLiteral | HexadecimalIntegralLiteral;

fragment FracConstant: DecimalIntegralLiteral? Dot DecimalIntegralLiteral | Dot DecimalIntegralLiteral;
fragment ExponentPart: ('e' | 'E') ('+' | '-')? DecimalIntegralLiteral;
fragment FloatSuffix: [hHfFlL];

FloatLiteral: FracConstant ExponentPart? FloatSuffix? | DecimalIntegralLiteral ExponentPart FloatSuffix?;

fragment EscapeSequence: '\\' ['"?abfnrtv\\];
fragment SChar: ~["\\\r\n] | EscapeSequence;
StringLiteral: '"' SChar* '"';

// Others.
Identifier: Letter AlphaNumLetter*;
