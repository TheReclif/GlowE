
// Generated from F:/Dokumenty/GlowE/antlr4/GSILLexer.g4 by ANTLR 4.13.0

#pragma once


#include "antlr4-runtime.h"


namespace GSIL {


class  GSILLexer : public antlr4::Lexer {
public:
  enum {
    WhiteSpace = 1, MultiLineComment = 2, SingleLineComment = 3, CodeSnippetPrefix = 4, 
    CBuffer = 5, Struct = 6, InOut = 7, Out = 8, GeometryShaderInputSpecifier = 9, 
    SVSpecifier = 10, EngineSupplied = 11, Hidden = 12, Sampler = 13, BlendState = 14, 
    DepthStencilState = 15, RasterizerState = 16, PropertySpecifier = 17, 
    Instanced = 18, TextureType = 19, UAVType = 20, TechniqueGroup = 21, 
    Technique = 22, Pass = 23, CompileShader = 24, Tag = 25, Threads = 26, 
    EarlyZ = 27, MaxVertexCount = 28, InterpolationModeSpecifier = 29, Uniform = 30, 
    ShaderName = 31, VectorType = 32, Matrix = 33, DMatrix = 34, False = 35, 
    True = 36, Break = 37, Continue = 38, Discard = 39, Do = 40, If = 41, 
    Switch = 42, While = 43, Case = 44, Default = 45, For = 46, Else = 47, 
    Return = 48, StorageClass = 49, TypeModifier = 50, LeftParen = 51, RightParen = 52, 
    LeftBracket = 53, RightBracket = 54, LeftBrace = 55, RightBrace = 56, 
    Less = 57, LessEqual = 58, Greater = 59, GreaterEqual = 60, LeftShift = 61, 
    RightShift = 62, Plus = 63, PlusPlus = 64, Minus = 65, MinusMinus = 66, 
    Star = 67, Div = 68, Mod = 69, And = 70, Or = 71, AndAnd = 72, OrOr = 73, 
    Caret = 74, Not = 75, Tilde = 76, Question = 77, Colon = 78, ColonColon = 79, 
    Semi = 80, Comma = 81, Assign = 82, StarAssign = 83, DivAssign = 84, 
    ModAssign = 85, PlusAssign = 86, MinusAssign = 87, LeftShiftAssign = 88, 
    RightShiftAssign = 89, AndAssign = 90, XorAssign = 91, OrAssign = 92, 
    Equal = 93, NotEqual = 94, Dot = 95, IntegralLiteral = 96, FloatLiteral = 97, 
    StringLiteral = 98, Identifier = 99
  };

  explicit GSILLexer(antlr4::CharStream *input);

  ~GSILLexer() override;


  std::string getGrammarFileName() const override;

  const std::vector<std::string>& getRuleNames() const override;

  const std::vector<std::string>& getChannelNames() const override;

  const std::vector<std::string>& getModeNames() const override;

  const antlr4::dfa::Vocabulary& getVocabulary() const override;

  antlr4::atn::SerializedATNView getSerializedATN() const override;

  const antlr4::atn::ATN& getATN() const override;

  // By default the static state used to implement the lexer is lazily initialized during the first
  // call to the constructor. You can call this function if you wish to initialize the static state
  // ahead of time.
  static void initialize();

private:

  // Individual action functions triggered by action() above.

  // Individual semantic predicate functions triggered by sempred() above.

};

}  // namespace GSIL
