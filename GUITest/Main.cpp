#include <GlowEngine/Engine.h>
#include <GlowEngine/Scene.h>
#include <GlowEngine/GameObject.h>

#include <GlowSystem/Debug.h>

#include <GlowComponents/GUITopComponent.h>
#include <GlowComponents/GUIImageComponent.h>
#include <GlowComponents/GUIContainerComponent.h>
#include <GlowComponents/GUILinearLayoutComponent.h>
#include <GlowComponents/GUIScroll.h>
#include <GlowComponents/GUIInputTextComponent.h>
#include <GlowComponents/GUIButtonComponent.h>
#include <GlowComponents/GUIWindowComponent.h>

#include <GlowGraphics/Uniform/TextureLoader.h>

#include <GlowAppFramework/AppFramework.h>

#include <GlowComponents/ScriptBehaviour.h>

using namespace GLOWE;

HWND getWindowUnderMouse(HWND whichWindow);

Int2 getRectSize(const Event::SizeMoveInfo& rect)
{
	return { rect.right - rect.left, rect.bottom - rect.top };
}

class DockableWindow
	: public GLOWE::Components::ScriptBehaviour
{
public:
	GraphicsWindow wnd;
	Event::SizeMoveInfo windowRect;
	Components::GUIButton& button;

	inline DockableWindow(GLOWE::GameObject* const newParent)
		: ScriptBehaviour(newParent), button(GameObject::createGameObject("Child", gameObject).addComponent<Components::GUIButton>())
	{
		SwapChainDesc swapChainDesc;
		swapChainDesc.size = VideoMode(200, 100, 32, 144);

		wnd.open(engine.getGraphicsContext(), swapChainDesc.size, "Test drop", swapChainDesc);
		debugPrintln("Tool wnd: " + toString(wnd.getWindowHandle()));

		button.setText("Omkno");
		button.getTextField()->setHorizontalAlignment(Components::GUITextField::TextHorizontalAlignment::Middle);
		button.getTextField()->setVerticalAlignment(Components::GUITextField::TextVerticalAlignment::Middle);
		button.deactivate();
	}

	// Uncomment these lines to enable asynchronous script update.
	//virtual void update() override;

	// Uncomment these lines to enable the script to receive events.
	//virtual void processEvent(const GLOWE::GameEvent& event) override;

	virtual void postUpdateSync() override
	{
		if (wnd.peekEvents())
		{
			wchar_t buff[512] = L"";
			for (const auto& event : getInstance<EventMgr>().getEventsQueue(wnd))
			{
				switch (event.type)
				{
				case Event::Type::Closed:
					wnd.close();
					break;
				case Event::Type::EnterSizeMove:
					windowRect = event.sizeMoveInfo;
					SetWindowLong(wnd.getWindowHandle(), GWL_EXSTYLE, GetWindowLong(wnd.getWindowHandle(), GWL_EXSTYLE) | WS_EX_LAYERED);
					SetLayeredWindowAttributes(wnd.getWindowHandle(), 0, 123, LWA_ALPHA);
					break;
				case Event::Type::ExitSizeMove:
				{
					Event::SizeMoveInfo nowRect = event.sizeMoveInfo;
					auto sizeThen = getRectSize(windowRect), sizeNow = getRectSize(nowRect);
					if ((nowRect.top != windowRect.top || nowRect.left != windowRect.left)
						&& sizeThen[0] == sizeNow[0]
						&& sizeThen[1] == sizeNow[1])
					{
						const auto handle = getWindowUnderMouse(wnd.getWindowHandle());
						GetWindowTextW(handle, buff, 512);
						debugPrintln(buff);
						debugPrintln(toString(handle));
						if (engine.getWindow() == handle)
						{
							debugPrintln("Mouse entered the main engine window");
							const auto wndSize = wnd.getSize();
							wnd.close();
							BringWindowToTop(engine.getWindow().getWindowHandle());
							auto pos = Mouse::getMousePosition(engine.getWindow());
							button.activate();
							button.setGlobalPosition({ float(pos[0]), float(pos[1]) });
							button.setLocalSize({ float(wndSize.x), float(wndSize.y) });
							button.setOnClickCallback([this](Components::GUIButton& button)
								{
									SwapChainDesc swapChainDesc;
									swapChainDesc.size = VideoMode(200, 100, 32, 144);

									auto pos = Mouse::getMousePosition(true);
									wnd.open(engine.getGraphicsContext(), swapChainDesc.size, "Test drop", swapChainDesc, pos);
									debugPrintln("Tool wnd: " + toString(wnd.getWindowHandle()));

									// TODO: mutex deadlock (exception thrown instead)
									button.deactivate();
								});
						
							break;
						}
					}
					SetWindowLong(wnd.getWindowHandle(), GWL_EXSTYLE, GetWindowLong(wnd.getWindowHandle(), GWL_EXSTYLE) & ~WS_EX_LAYERED);
					RedrawWindow(wnd.getWindowHandle(), NULL, NULL, RDW_ERASE | RDW_INVALIDATE | RDW_FRAME | RDW_ALLCHILDREN);
				}
					break;
				}
				
			}
			getInstance<EventMgr>().clear(wnd);
		}

		wnd.display();
	}

	// Place GlowSerialize instructions here.
	GlowSerializeScript(DockableWindow, GlowBases());
};

GlowInitScriptReflection(DockableWindow);

HWND getWindowUnderThisAt(Int2 point, HWND whichWindowUnder)
{
	RECT rect;
	POINT pt{ point[0], point[1] };

	for (HWND wnd = GetWindow(whichWindowUnder, GW_HWNDNEXT); wnd != nullptr; wnd = GetWindow(wnd, GW_HWNDNEXT))
	{
		if (wnd == whichWindowUnder || GetWindowRect(wnd, &rect) == 0 || IsWindowEnabled(wnd) == 0 || IsWindowVisible(wnd) == 0)
		{
			continue;
		}

		if (PtInRect(&rect, pt))
		{
			return wnd;
		}
	}

	return nullptr;
}

HWND getWindowUnderMouse(HWND whichWindow)
{
	return getWindowUnderThisAt(Mouse::getMousePosition(true), whichWindow);
}

GlowWindowApplication()
{
	using namespace GLOWE;

	auto& engine = GLOWE::getInstance<GLOWE::Engine>();
	engine.setCallbackAfterInitialize([](GLOWE::Engine& engine)
	{
		debugPrintln("Main wnd: " + toString(engine.getWindow().getWindowHandle()));

		auto& gc = engine.getGraphicsContext();

		engine.setKeyRepeat(true);

		Scene::createEmptyScene("MainScene");
		auto& topObject = GameObject::createGameObject("GUI Top");
		
		auto top = &topObject.addComponent<Components::GUITop>();
		top->setNotOwnedRenderTarget(&engine.getWindow().getSwapChain().getRenderTarget());
		top->setEventProcessing(true);
		top->setShouldClearRT(true);
		top->setClearColor(Color::from255RGBA(200, 250, 230));
		engine.setShouldPresentThisFramePredicate([top]() -> bool
			{
				return top->checkIsAnythingToRedraw();
			});

		GameObject::createGameObject("DockableWindow", &topObject).addComponent<DockableWindow>();
		GameObject::createGameObject("DockableWindow2", &topObject).addComponent<DockableWindow>();
	});
	engine.startLoop();
}
