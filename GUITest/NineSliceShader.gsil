RasterizerState RastState
{
	EnableScissor = true;
	EnableDepthClip = false;
};

BlendState AdditiveBlend
{
	EnableBlend[0] = true;
	SrcBlend[0] = SrcAlpha;
	DestBlend[0] = InvSrcAlpha;
	BlendOperation[0] = Add;
	SrcBlendAlpha[0] = One;
	DestBlendAlpha[0] = InvSrcAlpha;
	BlendOperationAlpha[0] = Add;
	WriteMask[0] = 15;
};

DepthStencilState DepthDisabled
{
	EnableDepthTest = false;
	EnableStencilTest = true;
	FrontFaceComparisonFunc = Equal;
	FrontFaceSuccOp = Keep;
	FrontFaceFailOp = Keep;
	BackFaceComparisonFunc = Never;
};

DepthStencilState DepthStencilDisabled
{
	EnableDepthTest = false;
	EnableStencilTest = false;
};

Sampler DiffuseSampler
{
	MinFilter = Linear;
	MagFilter = Linear;
	MipFilter = Linear;
};

Texture2D spriteTexture(DiffuseSampler) : Property("Sprite");

CBuffer PerObject
{
	EngineSupplied Matrix4x4 worldViewProj;
};

CBuffer PerSprite
{
	// Sprite border in UV space (borderPx / dimensionPx).
	EngineSupplied Float4 border; // Left, Right, Top, Bottom
	// Border in the rect context in UV space (borderPx / rectDimensionPx).
	EngineSupplied Float4 windowBorder;
};

Struct InOut VSIn
{
	Float3 position;
	Float2 texCoord;
};

Struct InOut VSOut
{
	SV Float4 position;
	Float2 texCoord;
};

Struct InOut PSOut
{
	SV Float4 target;
};

Float map(Float value, Float originalMin, Float originalMax, Float newMin, Float newMax)
{
	return (value - originalMin) / (originalMax - originalMin) * (newMax - newMin) + newMin;
}

Float processAxis(Float coord, Float2 textureBorder, Float2 windowBorder)
{
	if (coord < windowBorder.x)
	{
		return map(coord, 0, windowBorder.x, 0, textureBorder.x);
	}
	if (coord < (1 - windowBorder.y))
	{
		return map(coord, windowBorder.x, 1 - windowBorder.y, textureBorder.x, 1 - textureBorder.y);
	}
	return map(coord, 1 - windowBorder.y, 1, 1 - textureBorder.y, 1);
}

VSOut VSMain(VSIn input)
{
	VSOut output;

	output.position = mul(PerObject.worldViewProj, Float4(input.position, 1.0f) * Float4(1.0f, -1.0f, 1.0f, 1.0f));
	output.position.z = 0.0f;
	output.texCoord = input.texCoord;
	
	return output;
}

PSOut PSMain(VSOut input)
{
	PSOut output;
	
	Float2 texCoord = Float2
	(
	processAxis(input.texCoord.x, PerSprite.border.xy, PerSprite.windowBorder.xy),
	processAxis(input.texCoord.y, PerSprite.border.zw, PerSprite.windowBorder.zw)
	);
	output.target = spriteTexture.sample(texCoord);
	//output.target = Float4(texCoord.xy, 0, 1);
	
	return output;
}

TechniqueGroup GUI
{
	Technique NineSlice
	{
		Pass 0
		{
			VertexShader = CompileShader(5.0, VSMain);
			PixelShader = CompileShader(5.0, PSMain);
			DepthStencilState = DepthDisabled;
			RasterizerState = RastState;
			BlendState = AdditiveBlend;
		};
	};
	
	Technique NineSliceNoStencil
	{
		Pass 0
		{
			VertexShader = CompileShader(5.0, VSMain);
			PixelShader = CompileShader(5.0, PSMain);
			DepthStencilState = DepthStencilDisabled;
			RasterizerState = RastState;
			BlendState = AdditiveBlend;
		};
	};
};
