#pragma once
#ifndef GLOWE_APPFRAMEWORK_INCLUDED
#define GLOWE_APPFRAMEWORK_INCLUDED

#include "../GlowSystem/Utility.h"
#include "../GlowSystem/String.h"

#define GlowCmdApplication() \
void GlowAppMain(const GLOWE::Vector<GLOWE::String>&); \
 \
int main(int argc, char** argv) \
{ \
	using namespace GLOWE; \
	Vector<String> args; \
	 \
	for(unsigned int x = 0; x < argc; ++x) \
	{ \
		args.push_back(String(argv[x])); \
	} \
	 \
	GlowAppMain(args); \
	 \
	return 0; \
} \
void GlowAppMain(const GLOWE::Vector<GLOWE::String>& arguments)

#ifdef GLOWE_COMPILE_FOR_WINDOWS
#ifdef __GNUC__
#define GlowMainDecl() int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
#else
#define GlowMainDecl() int __stdcall wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nShowCmd)
#endif
#define GlowWindowApplication() \
void GlowAppMain(const GLOWE::Vector<GLOWE::String>&); \
 \
GlowMainDecl() \
{ \
	int argc = 0; \
	LPWSTR tempCmdLine = GetCommandLineW(); \
	LPWSTR* argvRaw = CommandLineToArgvW(tempCmdLine, &argc); \
	 \
	GLOWE::Windows::WindowsWindowImplementation::setInitialWindowState(nShowCmd); \
	GLOWE::Vector<GLOWE::String> args; \
	GLOWE::getInstance<GLOWE::ConcurrentGC>().attachThread(); \
	 \
	try \
	{ \
		for (int x = 0; x < argc; ++x) \
		{ \
			args.push_back(argvRaw[x]); \
		} \
	} \
	catch (...) { LocalFree(argvRaw); return -1; } \
	LocalFree(argvRaw); \
	 \
	GlowAppMain(args); \
	 \
	return 0; \
} \
void GlowAppMain(const GLOWE::Vector<GLOWE::String>& arguments)
#else
#define GlowWindowApplication() \
void GlowAppMain(const GLOWE::Vector<GLOWE::String>&); \
 \
int main(int argc, char** argv) \
{ \
	using namespace GLOWE; \
	Vector<String> args; \
	GLOWE::getInstance<GLOWE::ConcurrentGC>().attachThread(); \
	 \
	for(unsigned int x = 0; x < argc; ++x) \
	{ \
		args.push_back(String(argv[x])); \
	} \
	 \
	GlowAppMain(args); \
	 \
	return 0; \
} \
void GlowAppMain(const GLOWE::Vector<GLOWE::String>& arguments)
#endif

#endif
